Hamartia Error Suite
====================

** Work still in progress! **

Repository Summary
------------------
### Branches
- *master* : Main release branch, versions will be tagged by release
- *develop* : Development branch, development versions, release candidates, etc.

### Issues
Please look at and add to the list of issues/tasks. These tools are still in development and is not
mature in terms of testing.

### External Projects
If external projects are included, use `git subtree` to add them to the repository (currently done).
This allows for commits to be brought in from the private (or public) directories and for
pull-requests to be made to the external repos by doing a `git push`. See [this
tutorial](https://medium.com/@v/git-subtrees-a-tutorial-6ff568381844#.yaw4wzfsc) for more
information.

Overview
--------
The Hamartia Error Suite provides a suit of applications including a common interface for fault
injection and detection.  While the current implementation is currently focused on
*instruction-level* injection, the overall interface could be extended in the future to support
different levels of injection. In general the API library provides:

- Cross-language interfaces (swig)
	- Instruction information
	- Injection statistics
	- Triggers (in progress)
- Generic Injector
- Error managers (for handling C++ or Python error/detector models)
- Unified error/detector model interface
- Basic fault utilities
- Helper functions

Tools and Projects
------------------
### Hamartia Error API (/api)
A C++/Python interface for defining injectors and error/detectors models. Allows for models to be
run across different injectors.

### Hamartia Pin Injector (/injectors/pin_injector)
A [Pin-based](https://software.intel.com/en-us/articles/pin-a-dynamic-binary-instrumentation-tool)
error injector (mainly focused on arithmetic instructions currently).

### Hamartia RTL Injector (/injectors/rtl_injector)
A RTL (gate-level) injector (or model) (mainly focused on arithmetic instructions).

### Installation Guide
http://lph.ece.utexas.edu/users/hamartia/install

### Documentation (/mkdocs)
Documentation and the accompanying website is generated via mkdocs. 
