The core API specifies interfaces and common utility functions for error injection and detection tools. 
This allows for:

  * Error models to be used by injectors
  * Detector models to be used by injectors
  * Common utility methods to be reused

A majority of the interface will be defined using SWIG, which provides
interfaces for C++ and Python (possiblity of other languages) to be generated.

### Features

  * C++/Python helper methods for interface
  * Error manager for launching error/detector models (C++ or Python)
  * Interface for generic injection and detection (currently instruction-level only)
    + Error injection context  

