High-fidelity detectors refer to those used by the RTL gate-level injector.

See the [Checker Template](../rtl/checker.md) page of RTL injector for details.
