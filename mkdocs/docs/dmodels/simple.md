We currently implement arithmetic residuce checkers with one and two moduli.
Note that residue checkers only protect integer `ADD`, `SUB`, and `MUL`
instructions.

### Available simple detector models:
> `X` and `Y` are positive integers (usaually 3, 5, or 7).

- MOD`X`: residue checker with one modulo  
- MOD`X`_`Y`:  residue checker with two moduli

