## Installation

This installation guide is for Linux users. 
List of tested platform: 

  * OpenSUSE 42.3 
  * Ubuntu 16.04 (see issues below)

---------------------
### Obtain Source Code

Hamartia suite currently consists of three major components: Error/Detector API, an instruction-level injector, and a set of gate-level fault models. We have wrapped them into a single repository.

  * `git clone https://bitbucket.org/lph_tools/hamartia_suite`
  * The root folder will be `HAMARTIA_ROOT` below

---------------------
### Dependencies

#### Pin (A dynamic binary instrumentation tool)

  * Download Pin (version 71313) from [here][Pin71313]
  * The root folder after untar will be `PIN_HOME` below
      
> Note that we are using an older Pin version because the newer versions use its own C runtime which complicates integration with our current toolchain.
#### Linux packages

  * OpenSUSE 42.3

    `sudo zypper install scons libelf-devel swig python-devel iverilog graphviz-devel yaml-cpp-devel`

  * Ubuntu 16.04

    `sudo apt-get install scons libelf-dev swig python-dev iverilog libboost-dev libyaml-cpp-dev graphviz graphviz-dev libgraphviz-dev`

    > The default gcc version comes with Ubuntu16.04 is 5.4 but Pin requires an older ABI version. Thus, we use gcc/g++ 4.9 instead. See [here][gccSwitch] for how to set a switch between gcc versions.

  * For documentation only: 
    + doxygen

#### Python packages
Tested with Python2 (>= 2.7.12), and `pip` is used to install packages.

  * Essential: 
    + `pip install --user pyyaml mako paramiko subprocess32 futures`
  * For RTL model only: 
    + All: `pip install --user -I pyverilog==0.9.3`
    + OpenSUSE: `pip install --user pygraphviz`
    + Ubuntu: `pip install --user pygraphviz --install-option="--include-path=/usr/include/graphviz" --install-option="--library-path=/usr/lib/graphviz/"`
  * For documentation only: 
    + `pip install --user mkdocs mkdocs-material sphinx sphinxcontrib-napoleon`
    + `pip install --user git+https://github.com/pieterdavid/mkdocs-doxygen-plugin`

---------------------
### Setting Environment Variables
Environment variables are used to bridge components within this suite.

  * `export PIN_HOME=<pin root directory after untar>`
  * `export HAMARTIA_DIR=<HAMARTIA_ROOT>/api` 
  * `export PIN_INJECTOR_DIR=<HAMARTIA_ROOT>/injectors/pin-injector`
  * For RTL model only:
    + `export HAMARTIA_ERROR_PATH=<HAMARTIA_ROOT>/injectors/rtl-injector/src/model.py`

---------------------
### Build
`scons` is used to build the API and the instruction-level injector.

  * `cd $HAMARTIA_DIR && scons -Q debug=1`
  * `cd $PIN_INJECTOR_DIR && scons -Q debug=1`

---------------------
### Example Run
#### Simple Error Models
The following injects an error to the output operand of the 100th dynamic arithmetic instruction of the `ls` application by randomly flipping a single bit. You might observe that `ls` crashes because of this error injected.

  * `cd $PIN_INJECTOR_DIR/bin`
  * `./injector.sh -i 100 -- /bin/ls` 

#### RTL Error Model 
This example injects an error to the same instruction as above but using the RTL error model.
Note that on Ubuntu16.04 invoking the RTL model from the instruction-level injector doesn't work (see below).

  * `cd <HAMARTIA_ROOT>/injectors/rtl-injector/rtl_componets/configs`  
  * modify the absolute paths in the nangate.yaml file to your local absolute paths
  * `export RTL_ERR_CONFIG=<HAMARTIA_ROOT>/injectors/rtl-injector/rtl_componets/configs/nangate.yaml`
  * `cd $PIN_INJECTOR_DIR/bin`
  * `./injector.sh -e RTL -q $RTL_ERR_CONFIG -i 100 -- /bin/ls`

---------------------
### Issues

  * On Ubuntu16.04, RTL error model works as a standalone injector (see instructions in the RTL repository). However, when invoked by the Pin instruction-level injector, it runs into an error in pygraphviz saying `.../: undefined symbols: PyObject...`. We haven't figured out if this is due to the pygraphviz on Ubuntu or due to some bug in our C++/Python interface.
 
---------------------
[Pin71313]: http://software.intel.com/sites/landingpage/pintool/downloads/pin-2.14-71313-gcc.4.4.7-linux.tar.gz 
[gccSwitch]: https://codeyarns.com/2015/02/26/how-to-switch-gcc-version-using-update-alternatives/
