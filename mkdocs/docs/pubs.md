If you use Hamartia for your research, we would appreciate a citation to our SC18 paper in any publications you produce.

```
@INPROCEEDINGS { chang2018hamartia,
    AUTHOR = { Chun-Kai Chang and Sangkug Lym and Nicholas Kelly and Michael B. Sullivan and Mattan Erez },
    TITLE = { Evaluating and Accelerating High-Fidelity Error Injection for HPC },
    BOOKTITLE = { to appear in the Proceedings of the ACM/IEEE International Conference on High-Performance Computing, Networking, Storage, and Analysis (SC18) },
    YEAR = { 2018 },
    MONTH = { November },
    ADDRESS = { Dallas, TX },
    MYCAT = { conference },
    PDF = { /sc18_hamartia.pdf },
}
```

## Publications using Hamartia
- Chun-Kai Chang, Wenqi Yin, and Mattan Erez, "Assessing The Impact of Timing Errors on HPC Applications," In the Proceedings of the ACM/IEEE International Conference on High-Performance Computing, Networking, Storage, and Analysis (SC). Denver, CO. November, 2019.
- Chun-Kai Chang, Guanpeng Li, Mattan Erez, "Evaluating Compiler IR-Level Selective Instruction Duplication with Realistic Hardware Error," Workshop on Fault Tolerance for HPC at eXtreme Scale (FTXS). Denver, CO. November, 2019
- Chun-Kai Chang, Mattan Erez, "A High-Fidelity, Low-Overhead and Open-Source Timing Error Injector," Workshop on Silicon Errors in Logic–System Effects (SELSE). Palo Alto, CA. March, 2019.
- Chun-Kai Chang, Sangkug Lym, Nicholas Kelly, Michael B. Sullivan, Mattan Erez, "Evaluating and Accelerating High-Fidelity Error Injection for HPC," In Proceedings of The International Conference for High Performance Computing, Networking, Storage, and Analysis (SC). Dallas, TX. November, 2018.
- Sullivan, Michael B., et al. "SwapCodes: Error Codes for Hardware-Software Cooperative GPU Pipeline Error Detection." In Proceedings of the 51st Annual IEEE/ACM International Symposium on Microarchitecture (MICRO). IEEE, 2018.
- Omer Subasi, Chun-Kai Chang, Mattan Erez, S. Krishnamoorthy, "Characterizing the Impact of Soft Errors Affecting Floating-point ALUs using RTL-level Fault Injection," In Proceedings of the 47th International Conference on Parallel Processing (ICPP), p. 59. ACM. Eugene, OR. August, 2018.
- Chun-Kai Chang, Sangkug Lym, Nicholas Kelly, Michael B. Sullivan, Mattan Erez, "Hamartia: A Fast and Accurate Error Injection Framework," Workshop on Silicon Errors in Logic–System Effects (SELSE). Boston, MA. April, 2018. **Best of SELSE (invited to present at DSN 2018)**
