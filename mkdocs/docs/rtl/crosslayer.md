Higher-level injectors fail to accurately model all sorts of hardware errors 
To improve error fidelity of higher-level injectors without sacrificing 
their speed, Hamartia API allows this RTL gate-level injector to be launched 
by instruction-level injectors. 

### Prerequisites
The high-level injector must prepare for an `error_context` as the input 
to this RTL gate-level injector. The best way to achieve this is by inheriting 
your injector from the ``Injector`` class of Hamartia API. We assume the 
readers understand the Hamartia API before launching this injector from 
instruction-level injectors.

### Steps

1. Prepare an error config
    An error config specifies everything needed for RTL gate-level simulation. It consists of the following fields: 
      * `cache`: a directory path where inetermediate data will be saved to improve peformance
      * `dirs`: directory paths that contain the gate-level netlists or cell libraries
      * `cell_libs`: Cell library upon which the netlists based
      * `modules`: A list of information related to the netlists (e.g. circuit module name, input/output size, etc.)

    > See `rtl_component/configs/nangate.yaml` for example error config. Modify those paths to your local paths. 

2. Specify *RTL* as the error model name and the *error_config* path to the high-level injector's constructor.
    The error config path is what you created in Step 1.

3. Set `$HAMARTIA_ERROR_PATH` to `src/model.py`. This will allow Hamartia API to call this gate-level injector.  
4. Launch your injector. The error context will then contain the injection results of this gate-level injector.


### Invoke with Hamartia's Instruction-level Injector 

`$PIN_INJECTOR_DIR/bin/injector.sh -e RTL -q <config_file> -i <instruction>`


