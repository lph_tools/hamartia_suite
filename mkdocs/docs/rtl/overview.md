This is a RTL gate-level injector that injects particle-strike-like faults into 
synthesized gate-level netlists in Verilog . Using the Hamartia Error API, 
it can be used as an Error Model with an instruction-level injector to inject 
high-fidelity errors into applications. Additionally, it can also be run as 
a standalone injector.

### Features
  * Support arbitrary gate-level Verilog
  * FLIP, STUCKAT0, and STUCKAT1 faults
  * Inject multiple errors (of a single type)
  * Inject errors only at the output of certain modules (e.g. Flip-flops)
  * Module/group mapping file (YAML) for libraries (so actual module names are not needed)
  * Support for pipelined or clocked logic
  * Support for checker functions to determine filtered/masked errors
  * Caches intermediate structures for quick parsing/running
  * Invokable by higher-level injectors via Hamartia API

