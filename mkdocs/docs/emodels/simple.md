
### Available simple error models:
> `X` is a number in {1,2,4,8}

- RANDBIT`X` - Inject random bit flips (`X` erroneous bits)
- RANDOM - Replace the value with a random number.
- RANDOM_LH - Replace the value with a random number (limited to the lower half-word).
- BURST`X` - Inject a bursty error (burst length of `X` bits)
- MANTB`X` - Inject a bursty error into the mantissa (FP values only)
- MANTR`X` - Inject random bit flips into the mantissa (FP values only)



