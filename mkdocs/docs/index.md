![logo](img/hamartia.png)
## An Error Injection and Detection Suite
Wonder how resilience your applications against potential hardware errors?

Hamartia is an error injection and detection suite designed for evaluating
the resilience of applications against hardware errors. It features a core 
API, an instruction-level error injector, and a set of high-fidelity 
error models and detector models. The main aim is to allow for a unified 
interface for (currently instruction-level) injectors. 

The tool is available [here][repo].

[repo]: https://bitbucket.org/lph_tools/hamartia_suite 
