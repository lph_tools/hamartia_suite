This is another high-fidelity injector that models timing errors due to transient voltage droops.
Using the Hamartia Error API, it can be used as an Error Model with an instruction-level injector to inject 
high-fidelity errors into applications. 

### Features
  * Support arbitrary gate-level Verilog
  * Knobs for controling operating condition via a configuration file (YAML)
  * Invokable by higher-level injectors via Hamartia API

See our SELSE 2019 paper for implementation details.
