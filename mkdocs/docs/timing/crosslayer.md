The general usage is similar to the RTL gate-level injector that models particle strikes.
However, the error config and the environment variable `$HAMARTIA_ERROR_PATH` are different. 

### Steps

1. Prepare an error config
    An error config specifies everything needed for RTL gate-level simulation. It consists of the following fields: 
      * `cache`: A folder where inetermediate data will be saved to improve peformance
      * `cell_libs`: Cell library upon which the netlists based
      * `liberties`: Available operating conditions in `.lib` format 
      * `netlist_dirs`: A folder that contains all post-synthesis verilog files 
      * `open_timer_path`: A path to the executable of OpenTimer (`external/OpenTimer`).
      * `fault`: Characteristics of the faults to model
        * `cycle_time`: Clock cycle time in nano seconds
        * `type`: Category of timing errors
        * `index`: Index in the category list
        * `duration`: Duration of the timing error

    > See `rtl_component/configs/timing/dw_saed32.yaml` for example error config. Modify those paths to your local paths. 

2. Specify *TIMING_RTL* as the error model name and the *error_config* path to the high-level injector's constructor.
    The error config path is what you created in Step 1.

3. Set `$HAMARTIA_ERROR_PATH` to `src/timing_model.py`.
4. Launch your injector. The error context will then contain the injection results of this injector.


### Invoke with Hamartia's Instruction-level Injector 

`$PIN_INJECTOR_DIR/bin/injector.sh -e TIMING_RTL -q <config_file> -i <instruction>`


