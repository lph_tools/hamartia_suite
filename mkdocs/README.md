The documentation is generated using [MkDocs](https://www.mkdocs.org/). 

In addition to `mkdocs`, you also need the [doxygen plugin](https://github.com/mkdocs/mkdocs/wiki/MkDocs-Plugins).

To preview the website, run `mkdocs serve` in this folder to generate a local URL.

To build the website, run `mkdocs build` in this folder. The outcome is saved in `site`.
