.. RTL Error Injector documentation master file, created by
   sphinx-quickstart on Thu Mar 26 14:27:53 2015.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. _checker:

Checker Templates
=================

Checker (Mako) templates are used to include error checking logic into the error simulation.
This function/logic effectively catches or masks errors which were not logically masked.
For instance, an example could be arithmetic residue checking. The checker is implemented
as a function in the verilog simulation. It returns 0 if an error was **not** caught and 1
if the error was detected. Additionally, for each detected error, the *filtered* count is
incremented so the coverage of the error detection method/unit can be determined.

Built-in Options/Arguments
--------------------------
- **inputs** [list]: list of inputs

  - name [str]: input name
  - width [int]: input width
  - value [int]: input value
  - datatype [str]: input datatype ("d", "h", "b")

- **outputs** [list]: list of outputs

  - name [str]: output name
  - width [int]: output width

- **options** [object/dict]: parsed JSON structure from command-line

Basic Template
--------------
Checker templates use the `Mako <http://www.makotemplates.org/>`_ syntax, which is based off
of python. A typical checker template will contain:

- Page header

  - Including which arguments are used/required

- Return statement (``checker = ...;``)

  - Verilog statements

- Other template functionality

  - Possibly additional options/arguments

.. code-block:: python

    <%page args="options, inputs, outputs"/>
        checker = !(
    % for i, m in enumerate(options.mods):
    % 	if i == 0:
            ((${options.a} % ${m} + ${options.b} % ${m}) % ${m} == ${options.z} % ${m})
    % 	else:
            && ((${options.a} % ${m} + ${options.b} % ${m}) % ${m} == ${options.z} % ${m})
    % 	endif
    % endfor
        );

In this case, the command-line options object is being used to specify which inputs/outputs the
template operates on (``options.a``, ``options.b``, and ``options.z``). Additionally, different 
modulus's are specified in a list (``options.mods``). Using these parameters/options, a Verilog
boolean statement is formed to check the additions of the modulus's against the output. Remember,
the template is parsed and run as **Verilog**!

To run such a checker template, a command such as this could be run::

        $ ./inject.py --path $RTL_DIR --lib $CELL_LIBRARY --src add_4bit.v --top Add_syn --num 1 --fault FLIP --input 3 A --input 6 B --output Z --checker adder_mod.mako '{"a": "A", "b": "B", "z": "Z", "mods": [3, 5]}'
