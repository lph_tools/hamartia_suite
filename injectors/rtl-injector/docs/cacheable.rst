cacheable module
================

.. automodule:: cacheable
    :members:
    :undoc-members:
    :show-inheritance:
