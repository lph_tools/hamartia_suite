.. RTL Error Injector documentation master file, created by
   sphinx-quickstart on Thu Mar 26 14:27:53 2015.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

RTL Error Injector Documentation
==============================================

An RTL error injector used for injecting different types of of single event upsets (SEUs) into 
an existing RTL module. The module is then simulated with provided inputs until a non-masked
error is reached. Additionally, a "checker" function/template can be provided to simulate
error-checking to filter errors that would be caught. The results at the end of the simulation
return the number of iterations, number of filtered errors, and the correct/incorrect outputs.

Features
--------
- Source RTL uses Gate-level verilog (cell-library can be normal)
- FLIP, STUCKAT0, and STUCKAT1 faults
- Inject multiple errors (of a single type)
- Inject errors only at the output of certain modules (e.g. FF)
- Module/group mapping file (YAML) for libraries (so actual module names are not needed)
- Support for pipelined or clocked logic
- Support for checker functions to determine filtered/masked errors (Mako template)
- Caches intermediate structures for quick parsing/running (using pickle)
- Invoke by higher-level injectors via Hamartia API


Cross-Layer Injection
---------------------
    This RTL gate-level injector can also be launched by higher-level injectors through Hamartia API.
    See :ref:`cross_layer` page for more details.

 
Examples
--------

Basic RTL Module
""""""""""""""""

    4-bit Adder. Command::

        $ ./inject.py --path $RTL_DIR --lib $CELL_LIBRARY --src add_4bit.v --top Add_syn --num 1 --fault FLIP --input 3 A --input 6 B --output Z

Configurable Residue Checker
""""""""""""""""""""""""""""

    Checker template, residue arithmetic error detection (adder_mod.mako):

    .. code-block:: python

        <%page args="options, inputs, outputs"/>
            checker = !(
        % for i, m in enumerate(options.mods):
        % 	if i == 0:
                ((${options.a} % ${m} + ${options.b} % ${m}) % ${m} == ${options.z} % ${m})
        % 	else:
                && ((${options.a} % ${m} + ${options.b} % ${m}) % ${m} == ${options.z} % ${m})
        % 	endif
        % endfor
            );

    Command::

        $ ./inject.py --path $RTL_DIR --lib $CELL_LIBRARY --src add_4bit.v --top Add_syn --num 1 --fault FLIP --input 3 A --input 6 B --output Z --checker adder_mod.mako '{"a": "A", "b": "B", "z": "Z", "mods": [3, 5]}'

Pipelined Logic
"""""""""""""""

    4-bit, 2-stage Multiplier. Command::

	    $ ./inject.py --path $RTL_DIR --lib $CELL_LIBRARY --src mult_pipe_2stage_4bit.v --top Mult_Pipe_2stage_syn --num 1 --fault FLIP --input 3 A --input 6 B --output Z --clk CLK 2

Output Module Injection
"""""""""""""""""""""""

    Only inject errors after all latches/FFs in 2-stage Multiplier. Command::

	    $ ./inject.py --path $RTL_DIR --lib $CELL_LIBRARY --src mult_pipe_2stage_4bit.v --top Mult_Pipe_2stage_syn --num 1 --fault FLIP --input 3 A --input 6 B --output Z --clk CLK 2 --module DFF_X1


See Also
--------
    For more information/parameters::

        $ ./inject.py --help

    Also, check the ``tests/all.bats`` file for further run cases.

Indices and Modules
===================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

Topics
======

.. toctree::
   :titlesonly:
   :maxdepth: 3

   checker
   cross_layer 
