
.. _cross_layer:


Cross-Layer Injection
=====================
    Higher-level injectors fail to accurately model all sorts of hardware errors since hardware faults
    would be masked before propagating to structures visible to higher-level injectors. To improve 
    accuracy of higher-level injectors without sacrificing their speed, Hamartia API allows this RTL
    gate-level injector to be launched by higher-level injectors. The **error context** is sent to
    this injector from the high-level injector.

Prerequisites
-------------
    The high-level injector must prepare for an **error context** as the input to this RTL gate-level
    injector. The best way to achieve this is by inheriting your injector from the ``Injector`` class
    of Hamartia API; otherwise, the user has to register this gate-level injector by himself. We assume 
    the readers understand the Hamartia API before launching this injector from higher-level injectors.

Steps
-----
    We assume your high-level injector inherits the ``Injector`` class in Hamartia API.

    1. Prepare an error config
        An error config specifies everything needed for RTL gate-level simulation. It consists of the following fields: 
            - *Cache*: Cache is a directory path where inetermediate data will be saved to improve peformance.
            - *Dirs*: Directory path that contains the gate-level netlists.
            - *Cell Libs*: Cell library required to resolve naming of the netlists.
            - *Modules*: A list of information related to the netlists (e.g. circuit module name, input/output size, etc.)
        .. note:: See `rtl_component/configs/nangate.yaml` as a template error config. Modify the paths to your local paths. *Cache* can be any path directory. *Dirs* and *Cell Libs* should be set to the corresponding paths of your machine if you will use gate-level netlists shipped with this injector.

    2. Pass **RTL** as the error model name and the **error config** path to the high-level injector's constructor.
        The error config path is the path to your error config file created in Step 1.

    3. Set the environmental variable *HAMARTIA_ERROR_PATH* to `src/model.py`. 

        $ export HAMARTIA_ERROR_PATH=<this_repo>/src/model.py

        This will allow Hamartia API to call this gate-level injector.  
    4. Launch your injector as usual. The error context will then contain the injection results of this gate-level injector.

See also
--------
    *Error Models* section of Hamartia API documentation.

