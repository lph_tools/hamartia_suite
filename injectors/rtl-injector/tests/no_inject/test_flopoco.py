"""
    Test script of FloPoCo circuits

    Basically, we want to make sure the synthesized circuits functions correctly
    even without injection. Since we only need to test each circuit only once,
    we mark the circuit into the checklist (a file) to avoid re-checking.
"""

import pytest
import os
import sys
import json

from common import STAL_FLDR
from common import out_json
from common import build, prepare_input_csv, simulate, cleanup

UTIL_FLDR=os.path.join(STAL_FLDR,"util")
sys.path.append(UTIL_FLDR)
from fp_convert import fp2uint_flopoco, uint2fp_flopoco

CHECK_LIST='flopoco_checklist.json'
LIB=os.path.join(STAL_FLDR, "config_gen", "flopoco.yaml")

@pytest.fixture(scope="module")
def checklist():
    if not os.path.exists(CHECK_LIST):
        with open(CHECK_LIST, "wb") as of:
            init = {'test_name':'passed_or_not',}
            json.dump(init, of, indent=4) 

    with open(CHECK_LIST) as f:
        checklist = json.load(f)
        assert checklist
        yield checklist

    # teardown code: save checklist and cleanup
    with open(CHECK_LIST, "wb") as of:
        json.dump(checklist, of, indent=4) 
    
    cleanup()

def test_fp_add_32b(checklist):
    funcname = sys._getframe().f_code.co_name
    test_data = {
            "in_port_name" : ['X', 'Y'],
            "in_port_val" : [0.1, 0.1],
            "expected_out" : {'Radd': 0.2},
            "in_data_type" : 'DT_FLOAT',
            "out_data_type" : 'DT_FLOAT',
    }        
    core_test(checklist, funcname, test_data)

def test_fp_sub_32b(checklist):
    funcname = sys._getframe().f_code.co_name
    test_data = {
            "in_port_name" : ['X', 'Y'],
            "in_port_val" : [0.1, 0.1],
            "expected_out" : {'Rsub': 0.0},
            "in_data_type" : 'DT_FLOAT',
            "out_data_type" : 'DT_FLOAT',
    }        
    core_test(checklist, funcname, test_data)
   
def test_fp_add_64b(checklist):
    funcname = sys._getframe().f_code.co_name
    test_data = {
            "in_port_name" : ['X', 'Y'],
            "in_port_val" : [0.1, 0.1],
            "expected_out" : {'Radd': 0.2},
            "in_data_type" : 'DT_DOUBLE',
            "out_data_type" : 'DT_DOUBLE',
    }        
    core_test(checklist, funcname, test_data)

def test_fp_sub_64b(checklist):
    funcname = sys._getframe().f_code.co_name
    test_data = {
            "in_port_name" : ['X', 'Y'],
            "in_port_val" : [0.1, 0.1],
            "expected_out" : {'Rsub': 0.0},
            "in_data_type" : 'DT_DOUBLE',
            "out_data_type" : 'DT_DOUBLE',
    }        
    core_test(checklist, funcname, test_data)

def test_fp_mult_32b(checklist):
    funcname = sys._getframe().f_code.co_name
    test_data = {
            "in_port_name" : ['X', 'Y'],
            "in_port_val" : [0.1, 0.1],
            "expected_out" : {'R': 0.01},
            "in_data_type" : 'DT_FLOAT',
            "out_data_type" : 'DT_FLOAT',
    }        
    core_test(checklist, funcname, test_data)

def test_fp_mult_64b(checklist):
    funcname = sys._getframe().f_code.co_name
    test_data = {
            "in_port_name" : ['X', 'Y'],
            "in_port_val" : [0.1, 0.1],
            "expected_out" : {'R': 0.01},
            "in_data_type" : 'DT_DOUBLE',
            "out_data_type" : 'DT_DOUBLE',
    }        
    core_test(checklist, funcname, test_data)

def test_fp_div_32b(checklist):
    funcname = sys._getframe().f_code.co_name
    test_data = {
            "in_port_name" : ['X', 'Y'],
            "in_port_val" : [0.1, 0.1],
            "expected_out" : {'R': 1.0},
            "in_data_type" : 'DT_FLOAT',
            "out_data_type" : 'DT_FLOAT',
    }        
    core_test(checklist, funcname, test_data)

def test_fp_div_64b(checklist):
    funcname = sys._getframe().f_code.co_name
    test_data = {
            "in_port_name" : ['X', 'Y'],
            "in_port_val" : [0.1, 0.1],
            "expected_out" : {'R': 1.0},
            "in_data_type" : 'DT_DOUBLE',
            "out_data_type" : 'DT_DOUBLE',
    }        
    core_test(checklist, funcname, test_data)

def test_fp_sqrt_32b(checklist):
    funcname = sys._getframe().f_code.co_name
    test_data = {
            "in_port_name" : ['X'],
            "in_port_val" : [0.01],
            "expected_out" : {'R': 0.1},
            "in_data_type" : 'DT_FLOAT',
            "out_data_type" : 'DT_FLOAT',
    }        
    core_test(checklist, funcname, test_data)

def test_fp_sqrt_64b(checklist):
    funcname = sys._getframe().f_code.co_name
    test_data = {
            "in_port_name" : ['X'],
            "in_port_val" : [0.01],
            "expected_out" : {'R': 0.1},
            "in_data_type" : 'DT_DOUBLE',
            "out_data_type" : 'DT_DOUBLE',
    }        
    core_test(checklist, funcname, test_data)


###############################################################################
# Internals
###############################################################################
def build_sample(data_type, v):
    return {'data_type' : data_type, 'inputs' : [{'value': v}] }        

def encode_val(val_list, data_type):

    encoded = []
    for v in val_list:
        sample = build_sample(data_type, v)
        fp2uint_flopoco(sample)
        encoded.append(sample['inputs'][0]['value'])
    return encoded

def check(expect_val, data_type):
    def is_close(v1, v2):
        return abs(v1 - v2) < 1e-6

    with open(out_json) as f:
        results = json.load(f)
        assert results, "Empty simulation output!"
        outputs = results["outputs"]
        for name, val in outputs.items():
            out_sample = build_sample(data_type, val)
            uint2fp_flopoco(out_sample)
            assert is_close(expect_val[name], out_sample['inputs'][0]['value'])
    return True    

def core_test(checklist, funcname, test_data):
    if funcname in checklist: # already tested
        return 
    build(funcname, LIB)
    port_name, port_val = (test_data["in_port_name"], test_data["in_port_val"])
    port_val = encode_val(port_val, test_data["in_data_type"])
    prepare_input_csv(port_name, port_val)
    simulate()
    passed = check(test_data["expected_out"], test_data["out_data_type"])
    if passed:
        checklist[funcname] = 'PASS'


