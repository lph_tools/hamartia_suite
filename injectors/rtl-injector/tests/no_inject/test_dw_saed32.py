import pytest
import os
import sys
import json
from random import randint

from common import STAL_FLDR
from common import out_json
from common import build, prepare_input_csv, simulate, cleanup

LIB=os.path.join(STAL_FLDR, "config_gen", "dw_saed32.yaml")

UTIL_FLDR=os.path.join(STAL_FLDR,"util")
sys.path.append(UTIL_FLDR)
from fp_convert import fp2uint, uint2fp

@pytest.fixture(scope="function")
def teardown():
    yield []
    cleanup()

def get_diff_bits(n1, n2):
    # convert to binary string and strip '0b'
    n1_bin, n2_bin = (bin(n1)[2:], bin(n2)[2:]) 
    maxl = max(len(n1_bin), len(n2_bin))
    # zero-filling to the same length and then reverse
    n1_bin, n2_bin = (n1_bin.zfill(maxl)[::-1], n2_bin.zfill(maxl)[::-1])
    return [i for i in range(maxl) if n1_bin[i] != n2_bin[i]]

def test_add_64b_cla(teardown):
    def get_rand_val(x=0, y=100):
        return randint(x,y)

    def get_expected_out(inputs):
        return inputs[0] + inputs[1]   

    funcname = sys._getframe().f_code.co_name
    inputs1, inputs2 = ([get_rand_val(), get_rand_val()], [get_rand_val(), get_rand_val()])
    output1, output2 = (get_expected_out(inputs1), get_expected_out(inputs2))
    output_name = 'SUM_inst'
    exp_tn = ["{}[{}]".format(output_name, b) for b in get_diff_bits(output1, output2)]
    test_data = {
            "in_port_name" : ['inst_A', 'inst_B'],
            "in_port_val" : [inputs1, inputs2],
            "expected_out" : {output_name: output2},
            "in_data_type" : 'DT_INT',
            "out_data_type" : 'DT_INT',
            "expected_toggled_nets" : exp_tn,
    }        

    # extra fields to put in the config
    extra = { 'dump_vcd' : 'yes', 'verilog_defs' : 'functional' }

    core_test(funcname, test_data, extra)

def test_add_32b_cla(teardown):
    def get_rand_val(x=0, y=100):
        return randint(x,y)

    def get_expected_out(inputs):
        return inputs[0] + inputs[1]   

    funcname = sys._getframe().f_code.co_name
    inputs1, inputs2 = ([get_rand_val(), get_rand_val()], [get_rand_val(), get_rand_val()])
    output1, output2 = (get_expected_out(inputs1), get_expected_out(inputs2))
    output_name = 'SUM_inst'
    exp_tn = ["{}[{}]".format(output_name, b) for b in get_diff_bits(output1, output2)]
    test_data = {
            "in_port_name" : ['inst_A', 'inst_B'],
            "in_port_val" : [inputs1, inputs2],
            "expected_out" : {output_name: output2},
            "in_data_type" : 'DT_INT',
            "out_data_type" : 'DT_INT',
            "expected_toggled_nets" : exp_tn,
    }        

    # extra fields to put in the config
    extra = { 'dump_vcd' : 'yes', 'verilog_defs' : 'functional' }

    core_test(funcname, test_data, extra)


def test_fp_add_64b_3_stages(teardown):
    funcname = sys._getframe().f_code.co_name
    #inputs1, inputs2 = ([get_rand_val(), get_rand_val()], [get_rand_val(), get_rand_val()])
    #output1, output2 = (get_expected_out(inputs1), get_expected_out(inputs2))
    num_pipe_regs = 3
    output_name = 'z_inst'
    #exp_tn = ["{}[{}]".format(output_name, b) for b in get_diff_bits(output1, output2)]
    test_data = {
            "in_port_name" : ['inst_a', 'inst_b'],
            "in_port_val" : [0.1, 0.2],
            "expected_out" : {output_name: 0.3},
            "in_data_type" : 'DT_DOUBLE',
            "out_data_type" : 'DT_DOUBLE',
            "expected_toggled_nets" : 0,
    }        

    # extra fields to put in the config
    extra = { 'dump_vcd' : 'yes', 'verilog_defs' : 'functional' }

    core_test_pipeline(funcname, test_data, num_pipe_regs, extra)

def test_fp_div_32b(teardown):
    funcname = sys._getframe().f_code.co_name
    output_name = 'z_inst'
    test_data = {
            "in_port_name" : ['inst_a', 'inst_b'],
            "in_port_val" : [0.6, 0.2],
            "expected_out" : {output_name: 3},
            "in_data_type" : 'DT_FLOAT',
            "out_data_type" : 'DT_FLOAT',
            "expected_toggled_nets" : 0,
    }        

    # extra fields to put in the config
    extra = { 'dump_vcd' : 'yes', 'verilog_defs' : 'functional' }

    core_test(funcname, test_data, extra)

def test_fp_div_64b(teardown):
    funcname = sys._getframe().f_code.co_name
    output_name = 'z_inst'
    test_data = {
            "in_port_name" : ['inst_a', 'inst_b'],
            "in_port_val" : [0.6, 2],
            "expected_out" : {output_name: 0.3},
            "in_data_type" : 'DT_DOUBLE',
            "out_data_type" : 'DT_DOUBLE',
            "expected_toggled_nets" : 0,
    }        

    # extra fields to put in the config
    extra = { 'dump_vcd' : 'yes', 'verilog_defs' : 'functional' }

    core_test(funcname, test_data, extra)

def test_fp_add_32b_2_stages(teardown):
    funcname = sys._getframe().f_code.co_name
    num_pipe_regs = 2
    output_name = 'z_inst'
    test_data = {
            "in_port_name" : ['inst_a', 'inst_b'],
            "in_port_val" : [0.1, 0.2],
            "expected_out" : {output_name: 0.3},
            "in_data_type" : 'DT_FLOAT',
            "out_data_type" : 'DT_FLOAT',
            "expected_toggled_nets" : 0,
    }        

    # extra fields to put in the config
    extra = { 'dump_vcd' : 'yes', 'verilog_defs' : 'functional' }

    core_test_pipeline(funcname, test_data, num_pipe_regs, extra)

def test_fp_add_64b_2_stages(teardown):
    funcname = sys._getframe().f_code.co_name
    num_pipe_regs = 2
    output_name = 'z_inst'
    test_data = {
            "in_port_name" : ['inst_a', 'inst_b'],
            "in_port_val" : [0.1, 0.2],
            "expected_out" : {output_name: 0.3},
            "in_data_type" : 'DT_DOUBLE',
            "out_data_type" : 'DT_DOUBLE',
            "expected_toggled_nets" : 0,
    }        

    # extra fields to put in the config
    extra = { 'dump_vcd' : 'yes', 'verilog_defs' : 'functional' }

    core_test_pipeline(funcname, test_data, num_pipe_regs, extra)

def test_fp_mult_32b_2_stages(teardown):
    funcname = sys._getframe().f_code.co_name
    num_pipe_regs = 2
    output_name = 'z_inst'
    test_data = {
            "in_port_name" : ['inst_a', 'inst_b'],
            "in_port_val" : [0.1, 0.2],
            "expected_out" : {output_name: 0.02},
            "in_data_type" : 'DT_FLOAT',
            "out_data_type" : 'DT_FLOAT',
            "expected_toggled_nets" : 0,
    }        

    # extra fields to put in the config
    extra = { 'dump_vcd' : 'yes', 'verilog_defs' : 'functional' }

    core_test_pipeline(funcname, test_data, num_pipe_regs, extra)

def test_fp_mult_64b_2_stages(teardown):
    funcname = sys._getframe().f_code.co_name
    num_pipe_regs = 2
    output_name = 'z_inst'
    test_data = {
            "in_port_name" : ['inst_a', 'inst_b'],
            "in_port_val" : [0.1, 0.2],
            "expected_out" : {output_name: 0.02},
            "in_data_type" : 'DT_DOUBLE',
            "out_data_type" : 'DT_DOUBLE',
            "expected_toggled_nets" : 0,
    }        

    # extra fields to put in the config
    extra = { 'dump_vcd' : 'yes', 'verilog_defs' : 'functional' }

    core_test_pipeline(funcname, test_data, num_pipe_regs, extra)

def test_mult_32b_2_stages(teardown):
    funcname = sys._getframe().f_code.co_name
    num_pipe_regs = 2
    output_name = 'PRODUCT_inst'
    test_data = {
            "in_port_name" : ['inst_A', 'inst_B'],
            "in_port_val" : [11, 9],
            "expected_out" : {output_name: 99},
            "in_data_type" : 'DT_INT',
            "out_data_type" : 'DT_INT',
            "expected_toggled_nets" : 0,
    }        

    # extra fields to put in the config
    extra = { 'dump_vcd' : 'yes', 'verilog_defs' : 'functional' }

    core_test_pipeline(funcname, test_data, num_pipe_regs, extra)

def test_mult_64b_2_stages(teardown):
    funcname = sys._getframe().f_code.co_name
    num_pipe_regs = 2
    output_name = 'PRODUCT_inst'
    test_data = {
            "in_port_name" : ['inst_A', 'inst_B'],
            "in_port_val" : [11, 9],
            "expected_out" : {output_name: 99},
            "in_data_type" : 'DT_INT',
            "out_data_type" : 'DT_INT',
            "expected_toggled_nets" : 0,
    }        

    # extra fields to put in the config
    extra = { 'dump_vcd' : 'yes', 'verilog_defs' : 'functional' }

    core_test_pipeline(funcname, test_data, num_pipe_regs, extra)



###############################################################################
# Internals
###############################################################################
def build_sample(data_type, v):
    return {'data_type' : data_type, 'inputs' : [{'value': v}] }

def encode_val(val_list, data_type):

    encoded = []
    for v in val_list:
        sample = build_sample(data_type, v)
        fp2uint(sample)
        encoded.append(sample['inputs'][0]['value'])
    return encoded

def check(expect_val, data_type, exp_tn=[]):
    def is_close(v1, v2):
        return abs(v1 - v2) < 1e-6

    with open(out_json) as f:
        results = json.load(f)
        assert results, "Empty simulation output!"
        outputs = results["outputs"]
        for name, val in outputs.items():
            out_sample = build_sample(data_type, val)
            uint2fp(out_sample)
            val = out_sample['inputs'][0]['value']
            golden = expect_val[name]
            assert is_close(golden, val), "{} should be {} but we get {} instead".format(name, golden, val)
        if exp_tn:
            tn = results["toggled_nets"]
            for i in exp_tn:
                assert i in tn, "{} should be toggled but not".format(i)

    return True    

def core_test(funcname, test_data, config_extra={}):
    build(funcname, LIB, config_extra)
    port_name, port_val = (test_data["in_port_name"], test_data["in_port_val"])
    port_val = encode_val(port_val, test_data["in_data_type"])
    prepare_input_csv(port_name, port_val, multi_inputs=isinstance(port_val[0], list))
    simulate()
    check(test_data["expected_out"], test_data["out_data_type"], test_data["expected_toggled_nets"])

def get_outputs():
    with open(out_json) as f:
        results = json.load(f)
        assert results, "Empty simulation output!"
        pairs = results["outputs"].items()
        port_name, port_val = zip(*pairs)[0], zip(*pairs)[1]
        return (port_name, port_val)

def core_test_pipeline(funcname, test_data, num_pipe_regs, config_extra={}):
    base_module_name = funcname.split('test_')[1]
    # prepare input of 1st stage
    port_name, port_val = test_data["in_port_name"], test_data["in_port_val"]
    port_val = encode_val(port_val, test_data["in_data_type"])
    prepare_input_csv(port_name, port_val, multi_inputs=isinstance(port_val[0], list))

    for i in range(num_pipe_regs+1):
        module_name = "{}_{}".format(base_module_name, i)
        build(module_name, LIB, config_extra)
        simulate()
        # prepare input for next stage
        port_name, port_val = get_outputs()
        prepare_input_csv(port_name, port_val)

    check(test_data["expected_out"], test_data["out_data_type"], test_data["expected_toggled_nets"])


