import os
import csv
import sys
import subprocess

# Folder paths
TEST_FLDR = os.path.dirname(os.path.realpath(__file__))
TEST_PARENT_FLDR = os.path.abspath(os.path.join(TEST_FLDR, os.pardir)) # no_inject folder
RTL_ROOT_FLDR = os.path.abspath(os.path.join(TEST_PARENT_FLDR, os.pardir)) # rtl_err_inj
SRC_FLDR = os.path.abspath(os.path.join(RTL_ROOT_FLDR, 'src'))
STAL_FLDR = os.path.abspath(os.path.join(RTL_ROOT_FLDR, 'stand_alone'))

LAUNCHER_PATH = os.path.join(RTL_ROOT_FLDR, "src/verify.py")

CONFIG_GEN_FLDR=os.path.join(STAL_FLDR,"config_gen")
sys.path.append(CONFIG_GEN_FLDR)
import config_gen

# Temporary files
config = os.path.join(TEST_FLDR, "test.yaml")
in_csv = os.path.join(TEST_FLDR, "test.csv")
out_json = os.path.join(TEST_FLDR, "test.json")


def build(tf, lib, extra={}):
    # Generate config based on test function name
    module_name = tf.split('test_')[1] if tf.startswith('test_') else tf
    config_gen.run(name=module_name, lib=lib, out=config, extra=extra) 

def prepare_input_csv(port_name, port_val, multi_inputs=False): 
    with open(in_csv, "wf") as f:
        cw = csv.writer(f)
        cw.writerow(port_name)
        if multi_inputs:
            for v in port_val:
                cw.writerow(v)
        else:    
            cw.writerow(port_val)

def simulate():
    cmd = [LAUNCHER_PATH, '-c', config, '-i', in_csv, '-o', out_json, '-d', '1']
    child_process = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    out, err = child_process.communicate()

def cleanup():
    to_remove = (config, in_csv, out_json)
    for f in to_remove:
        if os.path.exists(f):
            os.remove(f)    
