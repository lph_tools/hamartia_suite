"""
    Test script of ECC circuits

    Basically, we want to make sure the synthesized circuits functions correctly
    even without injection. Since we only need to test each circuit only once,
    we mark the circuit into the checklist (a file) to avoid re-checking.
"""

import pytest
import os
import sys
import json

from common import TEST_FLDR, STAL_FLDR, LAUNCHER_PATH
from common import config, in_csv, out_json
from common import prepare_input_csv, simulate, cleanup

CONFIG_GEN_FLDR=os.path.join(STAL_FLDR,"config_gen")
sys.path.append(CONFIG_GEN_FLDR)
import config_gen

CHECK_LIST='ecc_checklist.json'
LIB = os.path.join(CONFIG_GEN_FLDR, 'dw.yaml')

@pytest.fixture(scope="module")
def checklist():
    if not os.path.exists(CHECK_LIST):
        with open(CHECK_LIST, "wb") as of:
            init = {'test_name':'passed_or_not',}
            json.dump(init, of, indent=4) 

    with open(CHECK_LIST) as f:
        checklist = json.load(f)
        assert checklist
        yield checklist

    # teardown code: save checklist and cleanup
    with open(CHECK_LIST, "wb") as of:
        json.dump(checklist, of, indent=4) 

    cleanup()
 
def test_secdec_64_8(checklist):

    def run_encode(test_data):
        result = run(test_data)
        return result['wr_checkbits']

    def run_decode(test_data):    
        result = run(test_data)
        return result['rd_error'], result['real_bad_rd_error'], result['rd_data_sys']
        
    funcname = sys._getframe().f_code.co_name
    if funcname in checklist: # already tested
        return 
    val = 0x12345678
    # encoding
    test_data = {
            "module_name" : "secdec_encode_64_8",
            "in_port_name" : ['wr_data_sys'],
            "in_port_val" : [val],
            "in_data_type" : 'DT_INT',
            "out_data_type" : 'DT_INT',
    }        
    checkbits = run_encode(test_data)
    
    # decoding error-free data
    test_data = {
            "module_name" : "secdec_decode_64_8",
            "in_port_name" : ['rd_data_mem', 'rd_checkbits'],
            "in_port_val" : [val, checkbits],
            "in_data_type" : 'DT_INT',
            "out_data_type" : 'DT_INT',
    } 
    rd_err, multi_err, data_out = run_decode(test_data)
    assert rd_err == 0 and multi_err == 0 and data_out == val

    # decoding single-bit-error data (i.e., shouble be corrected)
    err_val = val ^ 1
    test_data = {
            "module_name" : "secdec_decode_64_8",
            "in_port_name" : ['rd_data_mem', 'rd_checkbits'],
            "in_port_val" : [err_val, checkbits],
            "in_data_type" : 'DT_INT',
            "out_data_type" : 'DT_INT',
    }
    rd_err, multi_err, data_out = run_decode(test_data)
    assert rd_err == 1 and multi_err == 0 and data_out == val

    # decoding double-bit-error data (i.e., shouble be detected)
    err_val = val ^ 3
    test_data = {
            "module_name" : "secdec_decode_64_8",
            "in_port_name" : ['rd_data_mem', 'rd_checkbits'],
            "in_port_val" : [err_val, checkbits],
            "in_data_type" : 'DT_INT',
            "out_data_type" : 'DT_INT',
    }
    rd_err, multi_err, data_out = run_decode(test_data)
    assert rd_err == 1 and multi_err == 1 and data_out == err_val

    checklist[funcname] = 'PASS'
    
###############################################################################
# Internals
###############################################################################
def build(module_name):
    config_gen.run(name=module_name, lib=LIB, out=config) 

def encode_val(val_list, data_type):
    def build_sample(data_type, v):
        return {'data_type' : data_type, 'inputs' : [{'value': v}] }

    encoded = []
    for v in val_list:
        sample = build_sample(data_type, v)
        encoded.append(sample['inputs'][0]['value'])
    return encoded

def get_results():
    with open(out_json) as f:
        results = json.load(f)
        assert results, "Empty simulation output!"
        return results['outputs']

def run(test_data):
    build(test_data["module_name"])
    port_name, port_val = (test_data["in_port_name"], test_data["in_port_val"])
    port_val = encode_val(port_val, test_data["in_data_type"])
    prepare_input_csv(port_name, port_val)
    simulate()
    return get_results()

