# Runs all testing scripts

RT=${BATS_TEST_DIRNAME}/../

pushd_nostd () {
    command pushd "$@" > /dev/null
}

popd_nostd () {
    command popd "$@" > /dev/null
}

setup() {
    # change CWD to a temporary directory 
    pushd_nostd ${BATS_TMPDIR}
    # register the tool root as our base folder 
}

function printout {
    echo " = OUTPUT = "
    echo "$output"
}

teardown() {
    if [ -e "$CACHE_DIR" ] 
    then
        rm -r $CACHE_DIR
    fi
    # go back to wherever we started 
    popd_nostd
}

#################################################################################################
# Basic running tests
#################################################################################################

# Adder 32-bit - Basic faults

@test "Adder32, FLIP, 1" {
    run ${RT}/src/stand_alone.py -c ${RT}/stand_alone/add_64b.yaml -i ${RT}/stand_alone/add_64b_inputs.csv
    printout
    [ "$status" -eq 0 ]     # check exit code
    NUM_LINES=`cat ./stand_alone_out.csv | wc -l`
    [ "$NUM_LINES" -gt 0 ]  # some file is produced
}

#################################################################################################
# Legacy Inject.py tests
#################################################################################################

RTL_DIR="${RT}/rtl_components/gate_verilog/old"
CELL_LIBRARY="../../libs/NangateOpenCellLibrary.v"
CACHE_DIR=cache

# Adder 4-bit - Basic faults

@test "Adder4, FLIP, 1" {
    run ${RT}/src/inject.py -v -p $RTL_DIR -l $CELL_LIBRARY -s add_4bit.v -t Add_syn -n 1 -f FLIP -i 3 A -i 6 B -o Z
    printout
    [ "$status" -eq 0 ]
}

@test "Adder4, FLIP, 4" {
    run ${RT}/src/inject.py -v -p $RTL_DIR -l $CELL_LIBRARY -s add_4bit.v -t Add_syn -n 4 -f FLIP -i 3 A -i 6 B -o Z
    printout
    [ "$status" -eq 0 ]
}

@test "Adder4, STUCKAT0, 1" {
    run ${RT}/src/inject.py -v -p $RTL_DIR -l $CELL_LIBRARY -s add_4bit.v -t Add_syn -n 1 -f STUCKAT0 -i 3 A -i 6 B -o Z
    printout
    [ "$status" -eq 0 ]
}

@test "Adder4, STUCKAT0, 4" {
    run ${RT}/src/inject.py -v -p $RTL_DIR -l $CELL_LIBRARY -s add_4bit.v -t Add_syn -n 4 -f STUCKAT0 -i 3 A -i 6 B -o Z
    printout
    [ "$status" -eq 0 ]
}

@test "Adder4, STUCKAT1, 1" {
    run ${RT}/src/inject.py -v -p $RTL_DIR -l $CELL_LIBRARY -s add_4bit.v -t Add_syn -n 1 -f STUCKAT1 -i 3 A -i 6 B -o Z
    printout
    [ "$status" -eq 0 ]
}

@test "Adder4, STUCKAT1, 4" {
    run ${RT}/src/inject.py -v -p $RTL_DIR -l $CELL_LIBRARY -s add_4bit.v -t Add_syn -n 4 -f STUCKAT1 -i 3 A -i 6 B -o Z
    printout
    [ "$status" -eq 0 ]
}

# Multiplier 4-bit - Basic faults

@test "Multiplier4, FLIP, 1" {
    run ${RT}/src/inject.py -v -p $RTL_DIR -l $CELL_LIBRARY -s mult_4bit.v -t Mult_syn -n 1 -f FLIP -i 3 A -i 6 B -o Z
    printout
    [ "$status" -eq 0 ]
}

@test "Multiplier4, STUCKAT0, 1" {
    run ${RT}/src/inject.py -v -p $RTL_DIR -l $CELL_LIBRARY -s mult_4bit.v -t Mult_syn -n 1 -f STUCKAT0 -i 3 A -i 6 B -o Z
    printout
    [ "$status" -eq 0 ]
}

@test "Multiplier4, STUCKAT1, 1" {
    run ${RT}/src/inject.py -v -p $RTL_DIR -l $CELL_LIBRARY -s mult_4bit.v -t Mult_syn -n 1 -f STUCKAT1 -i 3 A -i 6 B -o Z
    printout
    [ "$status" -eq 0 ]
}

# Adder 4-bit - Output module

@test "Adder4, FLIP, module INV_X4" {
    run ${RT}/src/inject.py -v -p $RTL_DIR -l $CELL_LIBRARY -s add_4bit.v -t Add_syn -n 1 -f FLIP -i 3 A -i 6 B -o Z -x INV_X4
    printout
    [ "$status" -eq 0 ]
}

@test "Adder4, FLIP, module NAND2_X2" {
    run ${RT}/src/inject.py -v -p $RTL_DIR -l $CELL_LIBRARY -s add_4bit.v -t Add_syn -n 1 -f FLIP -i 3 A -i 6 B -o Z -x NAND2_X2
    printout
    [ "$status" -eq 0 ]
}

# Multiplier 4-bit - Output module

@test "Multiplier4, FLIP, module INV_X4" {
    run ${RT}/src/inject.py -v -p $RTL_DIR -l $CELL_LIBRARY -s mult_4bit.v -t Mult_syn -n 1 -f FLIP -i 3 A -i 6 B -o Z -x INV_X4
    printout
    [ "$status" -eq 0 ]
}

@test "Multiplier4, FLIP, module NAND2_X2" {
    run ${RT}/src/inject.py -v -p $RTL_DIR -l $CELL_LIBRARY -s mult_4bit.v -t Mult_syn -n 1 -f FLIP -i 3 A -i 6 B -o Z -x NAND2_X2
    printout
    [ "$status" -eq 0 ]
}

# Multiplier 2-stage, 4-bit - Clocking

@test "Multiplier4, FLIP, 2-stage" {
    run ${RT}/src/inject.py -v -p $RTL_DIR -l $CELL_LIBRARY -s mult_pipe_2stage_4bit.v -t Mult_Pipe_2stage_syn -n 1 -f FLIP -i 3 A -i 6 B -o Z -k CLK 2
    printout
    [ "$status" -eq 0 ]
}

@test "Multiplier4, STUCKAT0, 2-stage" {
    run ${RT}/src/inject.py -v -p $RTL_DIR -l $CELL_LIBRARY -s mult_pipe_2stage_4bit.v -t Mult_Pipe_2stage_syn -n 1 -f STUCKAT0 -i 3 A -i 6 B -o Z -k CLK 2
    printout
    [ "$status" -eq 0 ]
}

@test "Multiplier4, STUCKAT1, 2-stage" {
    run ${RT}/src/inject.py -v -p $RTL_DIR -l $CELL_LIBRARY -s mult_pipe_2stage_4bit.v -t Mult_Pipe_2stage_syn -n 1 -f STUCKAT1 -i 3 A -i 6 B -o Z -k CLK 2
    printout
    [ "$status" -eq 0 ]
}

# Adder 4-bit - Checker function

@test "Adder4, FLIP, MOD Checker (2)" {
    run ${RT}/src/inject.py -v -p $RTL_DIR -l $CELL_LIBRARY -s add_4bit.v -t Add_syn -n 1 -f FLIP -i 3 A -i 6 B -o Z -e templates/adder_mod.mako '{"a": "A", "b": "B", "z": "Z", "mods": [2]}'
    printout
    [ "$status" -eq 0 ]
}

@test "Adder4, FLIP, MOD Checker (3, 5)" {
    run ${RT}/src/inject.py -v -p $RTL_DIR -l $CELL_LIBRARY -s add_4bit.v -t Add_syn -n 1 -f FLIP -i 3 A -i 6 B -o Z -e templates/adder_mod.mako '{"a": "A", "b": "B", "z": "Z", "mods": [3, 5]}'
    printout
    [ "$status" -eq 0 ]
}
