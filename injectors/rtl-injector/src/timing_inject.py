import os, subprocess, re
from collections import defaultdict

# Templating
from mako_helper import to_mako
from mako.template import Template
from mako.lookup import TemplateLookup

# Paths
SCRIPT_DIR = os.path.dirname(os.path.realpath(__file__))
CONF_TEMPLATE = "templates/ot_conf.mako" # OT stands for OpenTimer
SDC_TEMPLATE = "templates/ot_sdc.mako"

# File Names
OT_CONF = "ot.conf"
OT_SDC = "ot.sdc" # timing constraints

def generate_constraints(env, cycle_time, clk_port, clk2q_ns=0.130, setup_time_ns=0.03, load_fF=0.052):
    # TODO: generic setup time and output load 
    # now using average values of DFFX2_RVT in saed32nm
    with open(os.path.join(SCRIPT_DIR, SDC_TEMPLATE), 'r') as tbf:
        lookup_paths = TemplateLookup(directories=[".", SCRIPT_DIR])
        tb_template = Template(tbf.read(), lookup=lookup_paths)

        # Render template
        out = tb_template.render(cycle_time=cycle_time,
                                 clk_port=clk_port,
                                 clk2q=str(clk2q_ns),
                                 setup_time=str(setup_time_ns),
                                 out_load=str(load_fF))

        # Write testbench
        with open(os.path.join(env, OT_SDC), 'w') as of:
            of.write(out)

def generate_config(env, liberty, rtl_src, non_tg_nets, tgt_pins, tgt_trans):
    with open(os.path.join(SCRIPT_DIR, CONF_TEMPLATE), 'r') as tbf:
        lookup_paths = TemplateLookup(directories=[".", SCRIPT_DIR])
        tb_template = Template(tbf.read(), lookup=lookup_paths)

        # make pin information
        pin_info = []
        for i, n in enumerate(tgt_pins):
            info = {}
            info['name'] = n
            info['trans'] = tgt_trans[i]
            pin_info.append(info)

        # Render template
        out = tb_template.render(liberty_path=liberty,
                                 rtl_src_path=rtl_src,
                                 sdc_path=os.path.join(env, OT_SDC),
                                 non_tg_nets=non_tg_nets,
                                 pin_info=to_mako(pin_info))

        # Write testbench
        with open(os.path.join(env, OT_CONF), 'w') as of:
            of.write(out)

def run(env, sta_path):
    with open(os.path.join(env, OT_CONF)) as f:
        arg = [sta_path]
        sta = subprocess.Popen(arg, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE, cwd=env)
        (out, err) = sta.communicate(input=f.read())
    return out

def parse_slacks(results):
    if not results: ## no timing violation
        return None

    lines = results.strip().split('\n')
    out = {}
    for l in lines:
        name_slack = l.split(':')
        name, slack = name_slack[0].strip(), name_slack[1].strip()
        out[name] = float(slack)
    return out

def timing_analysis(env, sta_path, liberty, rtl_src, cycle_time, clk_port, non_tg_nets, tgt_pins, tgt_trans):
    """
    Run timing analysis to obtain slacks of the specified output pins

    Args:
        env          (str): path to store intermediate files
        sta_path     (str): path to the STA tool
        liberty      (str): liberty file path
        rtl_src      (str): RTL gate-level netlist path
        cycle_time   (str): clock cycle time 
        clk_port     (str): clock port name
        non_tg_nets  (list): list of non-toggled nets from Verilog simulation
        tgt_pins     (list): list of target pin names
        tgt_trans    (list): list of target pin transitions

    Returns:
        slacks       (list[float]): timing slacks of specified output pins
    """

    ## prepare input for STA
    generate_constraints(env, cycle_time, clk_port)
    generate_config(env, liberty, rtl_src, non_tg_nets, tgt_pins, tgt_trans)

    ## run STA
    results = run(env, sta_path)

    slacks = parse_slacks(results)
    return slacks

def get_fault_masks(outputs, err_pins):
    """
    Get fault masks for each output port

    Args:
        outputs     (list): output port name of the module
        err_pins    (list): output pin names with timing viloation

    Returns:
        masks   (dict): bit-flip masks for each output port
    """
    def get_pin_index(pin):
        pattern = '\S+\[(\d+)\]' # match against array notation
        r = re.match(pattern, pin)
        if r:
            return int(r.group(1)) # group 1 is the index
        else:
            return 0

    masks = defaultdict(int)
    for name in outputs:
        my_pins = [get_pin_index(p) for p in err_pins if p.startswith(name)]
        for p in my_pins:
            masks[name] |= (1 << p)

    return masks

def inject_output(out_vals, fault_masks):
    """
    Flip output values based on fault masks

    Args:
        out_vals    (dict): output port names to error-free values
        fault_masks (dict): output port names to pin-level fault masks
    
    Returns:
        new_vals    (dict): modified output values
    """
    new_vals = {}
    for name, val in out_vals.iteritems():
        new_vals[name] = val ^ fault_masks[name] 

    return new_vals
