#!/usr/bin/python2.7

"""

  A py.test File for math_helpers.py.

  Usage: py.test from anywhere

"""

import sys
import os
import collections
import types

###############################################################################
# Fix the path
###############################################################################

TEST_ROOT_FLDR = os.path.dirname(os.path.realpath(__file__))
TEST_PARENT_FLDR = os.path.abspath(os.path.join(TEST_ROOT_FLDR, os.pardir))
sys.path.append(TEST_PARENT_FLDR)

###############################################################################
# Test-specific
###############################################################################

import pytest
import math_helpers as tmod      # target module


@pytest.fixture
def shared_long(scope="session"):
    """ Create a long value. """
    global TGT_LONG
    TGT_LONG = 12892198372198652673
    assert isinstance(TGT_LONG, long)


def test_tobin(shared_long):
    """ Test tobin functionality. """

    # test type, value, width
    res = tmod.tobin(10, 9)
    assert isinstance(res, basestring)  # NOTE: not Python 3 compatible
    assert res == "000001010"

    # test longs
    res = tmod.tobin(TGT_LONG, 64)
    assert isinstance(res, basestring)  # NOTE: not Python 3 compatible
    gold = "1011001011101010010010100001110100011100110001010001101100000001"
    assert res == gold

    # test negatives
    with pytest.raises(ValueError):
        tmod.tobin(-1, 9)

    # test too short
    with pytest.raises(ValueError):
        tmod.tobin(10, 2)


def test_todec():
    """ Test tobin functionality. """

    # test type, value, width
    res = tmod.todec(10, 9)
    assert isinstance(res, basestring)  # NOTE: not Python 3 compatible
    assert res == "000000010"

    # test longs
    res = tmod.todec(TGT_LONG, 20)
    assert isinstance(res, basestring)  # NOTE: not Python 3 compatible
    gold = "12892198372198652673"
    assert res == gold

    # test negatives
    with pytest.raises(ValueError):
        tmod.todec(-1, 9)

    # test too short
    with pytest.raises(ValueError):
        tmod.todec(10, 1)


def test_tohex():
    """ Test tohex functionality. """

    # test type, value, width
    res = tmod.tohex(10, 9)
    assert isinstance(res, basestring)  # NOTE: not Python 3 compatible
    assert res == "00000000a"

    # test longs
    res = tmod.tohex(TGT_LONG, 16)
    assert isinstance(res, basestring)  # NOTE: not Python 3 compatible
    gold = "b2ea4a1d1cc51b01"
    assert res == gold

    # test negatives
    with pytest.raises(ValueError):
        tmod.tohex(-1, 9)

    # test too short
    with pytest.raises(ValueError):
        print tmod.tohex(17, 1)


def test_bintohex():
    """ Test bintohex functionality. """

    # test type, value, width
    res = tmod.bintohex("1010", 1)
    assert isinstance(res, basestring)  # NOTE: not Python 3 compatible
    assert res == "a"

    # test negatives
    with pytest.raises(ValueError):
        tmod.bintohex("1010", -1)

    # test too short
    with pytest.raises(ValueError):
        tmod.bintohex("11010", 1)


def test_bintoint():
    """ Test bintoint functionality. """

    # test type, value
    res = tmod.bintoint("1010")
    assert isinstance(res, int)
    assert res == 10


def test_hextoint():
    """ Test hextoint functionality. """

    # test type, value, width
    res = tmod.hextoint("0A")
    assert isinstance(res, int)
    assert res == 10
