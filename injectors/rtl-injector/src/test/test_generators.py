#!/usr/bin/python2.7

"""

  A py.test File for generators.py.

  Usage: py.test from anywhere

"""

import sys
import os
import collections
import types

###############################################################################
# Fix the path
###############################################################################

TEST_ROOT_FLDR = os.path.dirname(os.path.realpath(__file__))
TEST_PARENT_FLDR = os.path.abspath(os.path.join(TEST_ROOT_FLDR, os.pardir))
sys.path.append(TEST_PARENT_FLDR)

###############################################################################
# Test-specific
###############################################################################

import pytest
import generators as tmod      # target module
import random


@pytest.fixture
def fix_rng(scope="session"):
    """ Seed the RNG. """
    random.seed(1)  # a fixed RNG seed


def test_unique_random_faults_l(fix_rng):
    """ Test the random fault generator. """
    num_nets = 5

    # test type
    assert isinstance(tmod.unique_random_faults_l(num_nets, 1), list)

    # test lengths
    assert len(tmod.unique_random_faults_l(num_nets, 1)) == 5
    assert len(tmod.unique_random_faults_l(num_nets, 2)) == 10
    assert len(tmod.unique_random_faults_l(num_nets, 5)) == 1

    # test values
    assert tmod.unique_random_faults_l(num_nets, 1) == [(1,), (2,), (0,),
                                                        (3,), (4,)]

    # test uniqueness
    unique_num_nets = 10
    for test_subset in xrange(1, unique_num_nets+1):
        all_seen = set()
        sample_combos = [tuple(x) for x in
                         tmod.unique_random_faults_l(unique_num_nets,
                                                     test_subset)]
        for unique_combo in sample_combos:
            assert unique_combo not in all_seen
            all_seen.add(unique_combo)

    # corner cases
    # NOTE: is this too weird for num_faults=0?
    assert tmod.unique_random_faults_l(num_nets, 0) == [()]
    assert tmod.unique_random_faults_l(num_nets, 6) == []

    # test non-sensical lengths
    with pytest.raises(ValueError):
        tmod.unique_random_faults_l(num_nets, -1)


def isgenerator(iterable):
    """ Generalized test for a generator type.

    http://stackoverflow.com/a/10644028

    """
    return hasattr(iterable, '__iter__') and not hasattr(iterable, '__len__')


def test_repeatfunc(fix_rng):
    """ Test the basic repeatfunc generator. """

    def tst_func():
        """ A function to test. """
        return 1

    def tst_func_2(arg):
        """ A second function to test. """
        return arg

    # test type
    assert isgenerator(tmod.repeatfunc(tst_func, 2))

    # test length
    assert len(list(tmod.repeatfunc(tst_func, 2))) == 2

    # test values
    assert sum(list(tmod.repeatfunc(tst_func, 2))) == 2
    assert sum(list(tmod.repeatfunc(tst_func_2, 2, 2))) == 4

    # corner cases
    assert list(tmod.repeatfunc(tst_func, 0)) == []

    # test non-sensical lengths
    with pytest.raises(ValueError):
        list(tmod.repeatfunc(tst_func, -1))


def test_unique_sample(fix_rng):
    """ Test the unique sample generator. """

    input_iter = xrange(20)

    # test type
    assert isgenerator(tmod.unique_sample([], 0))

    # test length
    assert len(list(tmod.unique_sample(input_iter, 10))) == 10
    assert len(list(tmod.unique_sample(input_iter, 15))) == 15

    # test uniqueness
    for test_subset in xrange(21):
        all_seen = set()
        for unique_num in tmod.unique_sample(input_iter, test_subset):
            assert unique_num not in all_seen
            all_seen.add(unique_num)

    # corner cases
    assert list(tmod.unique_sample([], 0)) == []
    assert list(tmod.unique_sample([], 1)) == []

    # test non-sensical lengths
    with pytest.raises(ValueError):
        tmod.unique_sample([], -1)


def test_unique_randints(fix_rng):
    """ Test the unique random integer tuple generator. """

    (lower_bound, upper_bound) = (10, 20)

    # test type
    assert isgenerator(tmod.unique_randints(lower_bound, upper_bound, 0))
    assert isgenerator(tmod.unique_randints(lower_bound, upper_bound, 10))

    # test length
    assert len(list(tmod.unique_randints(lower_bound, upper_bound, 0))) == 0
    assert len(list(tmod.unique_randints(lower_bound, upper_bound, 5))) == 5

    # test uniqueness
    all_seen = set()
    for unique_num in tmod.unique_randints(lower_bound, upper_bound, 9):
        assert unique_num not in all_seen
        all_seen.add(unique_num)

    # test non-sensical lengths
    with pytest.raises(ValueError):
        list(tmod.unique_randints(0, -1, 1))
    with pytest.raises(ValueError):
        list(tmod.unique_randints(0, 0, 2))


def test_unique_random_faults_g(fix_rng):
    """ Test the random fault generator. """
    num_nets = 5

    # test type
    assert isgenerator(tmod.unique_random_faults_g(num_nets, 1))

    # test lengths
    assert len(list(tmod.unique_random_faults_g(num_nets, 1))) == 5
    assert len(list(tmod.unique_random_faults_g(num_nets, 2))) == 10
    assert len(list(tmod.unique_random_faults_g(num_nets, 5))) == 1

    # test values
    sample_combos = [tuple(x) for x in tmod.unique_random_faults_g(num_nets, 1)]
    assert sample_combos == [(3,), (1,), (5,), (0,), (4,)]
    # NOTE: order differs from unique_random_faults_l

    # test uniqueness
    unique_num_nets = 10
    for test_subset in xrange(1, unique_num_nets+1):
        all_seen = set()
        sample_combos = [tuple(x) for x in
                         tmod.unique_random_faults_g(unique_num_nets,
                                                     test_subset)]
        for unique_combo in sample_combos:
            assert unique_combo not in all_seen
            all_seen.add(unique_combo)
    # for unique_num in tmod.unique_randints(lower_bound, upper_bound, 9):
    #     assert unique_num not in all_seen
    #     all_seen.add(unique_num)

    # corner cases
    # consistent with unique_random_faults_l
    empty_sample = [tuple(x) for x in tmod.unique_random_faults_g(num_nets, 0)]
    assert empty_sample == [()]
    over_sample = [tuple(x) for x in tmod.unique_random_faults_g(num_nets, 6)]
    assert over_sample == []

    # test non-sensical lengths
    with pytest.raises(ValueError):
        list(tmod.unique_random_faults_g(num_nets, -1))


def test_unique_random_hex_faults(fix_rng):
    """ Test the random hex fault string generator. """
    num_nets = 5

    # test type and values
    tgen = tmod.unique_random_hex_faults(num_nets, 1)
    assert isgenerator(tgen)
    for tgt_tup in tgen:
        # assert isinstance(tgt_str, basestring)  # NOTE: Not Python 3 compatible
        tgt_str = tgt_tup[1]
        for tgt_char in tgt_str:
            assert tgt_char in "0123456789abcdef"

    # test lengths
    assert len(list(tmod.unique_random_hex_faults(num_nets, 1))) == 5
    assert len(list(tmod.unique_random_hex_faults(num_nets, 2))) == 10
    assert len(list(tmod.unique_random_hex_faults(num_nets, 5))) == 1

    # test non-sensical lengths
    with pytest.raises(ValueError):
        list(tmod.unique_random_hex_faults(num_nets, -1))
