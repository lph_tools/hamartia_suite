'''
   Original Author: Donald 'Paddy' McCarthy (C) 24 March 2008

   Copyright  2013  Gordon McGregor

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

   Assumptions:
    1. Gate-level Verilog netlist without pipelining (i.e., latency = 1 cycle)
    2. Simulation spans only 2 cycles
'''

from __future__ import with_statement
from itertools import dropwhile, takewhile, izip
from collections import defaultdict

class PhaseEnum:
  ENTER_INIT_SECT = 0x2F12AFE  # cycle 0
  ENTER_CHNG_SECT = 0x2341414  # cycle 1

class Utility:
  @staticmethod  
  def zfill_and_reverse(s1, s2):
    """ Zero-fill two strings to the same length and then reverse them """  
    maxl = max(len(s1), len(s2))
    return (s1.zfill(maxl)[::-1], s2.zfill(maxl)[::-1])
  
  @staticmethod
  def get_diffs(init, now):
    return [i for i in range(len(init)) if init[i] != now[i]]
   
class VCDParser(object):
  def __init__(self, init_time=0, change_time=2):
    keyword2handler = {
      # declaration_keyword ::=
      "$comment":        self.drop_declaration,
      "$date":           self.save_declaration,
      "$enddefinitions": self.vcd_enddefinitions,
      "$scope":          self.vcd_scope,
      "$timescale":      self.save_declaration,
      "$upscope":        self.vcd_upscope,
      "$var":            self.vcd_var,
      "$version":        self.save_declaration,
      # simulation_keyword ::=
      "$dumpall":        self.vcd_dumpall,
      "$dumpoff":        self.vcd_dumpoff,
      "$dumpon":         self.vcd_dumpon,
      "$dumpvars":       self.vcd_dumpvars,
      "$end":            self.vcd_end,
      }
    self.keyword_handlers = defaultdict(self.parse_error, keyword2handler)
    
    self.init_time = init_time 
    self.change_time = change_time
    self.scope = []
    self.idcode2references = defaultdict(list)
    self.wire_init = {} # initial values of array of wires 
    self.changes = {} # nets with transition
    self.xmr_cache = {}
    self.enddefinitions = False
    self.section_tracker = 0
    self.module_wires = defaultdict(list)
    

  def parse(self, vcdfile):

    with open(vcdfile) as f:
      tokeniser = (word for line in f for word in line.split() if word)
      for count,token in enumerate(tokeniser):
        if not self.enddefinitions:
          self.keyword_handlers[token](tokeniser, token)
        else: # enter the Value Change Section
          c, rest = token[0], token[1:]
          if c == '$':
            # skip $dump* tokens and $end tokens in sim section
            continue
          elif c == '#':
            if int(rest) == self.init_time:
              self.section_tracker = PhaseEnum.ENTER_INIT_SECT
            elif int(rest) == self.change_time:
              self.section_tracker = PhaseEnum.ENTER_CHNG_SECT
          elif c in '01xXzZ':
            self.scalar_value_change(value=c, id=rest)
          elif c in 'bBrR':
            self.vector_value_change(format=c.lower(), number=rest, id=tokeniser.next())
          else:
            raise "Don't understand: {} After {} words".format(token, count)

  def get_toggled_nets(self):
    get_transition = lambda x: "fall" if str(x) == "0" else "rise"

    nets = []
    transitions = []
    for c, v in self.changes.items():
        ref = self.get_xmr(c)
        if isinstance(v, tuple): # array notation
          init, now = Utility.zfill_and_reverse(self.wire_init[c][1], v[1])
          bits = Utility.get_diffs(init, now)
          nets.extend(["{}[{}]".format(ref, b) for b in bits])
          transitions.extend([get_transition(now[b]) for b in bits])
        else:
          nets.append(ref)
          transitions.append(get_transition(v))
    return (nets, transitions)

  def get_xmr(self, id):
    """Given an ID, generate the hierarchical reference"""
    if id in self.xmr_cache:
        return self.xmr_cache[id]

    (type, size, refs) = self.idcode2references[id][0]
    xmr = ":".join([v for (k, v) in refs])
    self.xmr_cache[id] = xmr
    return xmr 

  def get_wires_of(self, module_name):
    key = ('module', module_name)
    return self.module_wires.get(key, []) 

  ########################
  # End of public methods
  ########################
  def parse_error(self, tokeniser, keyword):
    raise "Don't understand keyword: " + keyword

  def drop_declaration(self, tokeniser, keyword):
    dropwhile(lambda x: x != "$end", tokeniser).next()
  
  def save_declaration(self, tokeniser, keyword):
    self.__setattr__(keyword.lstrip('$'),
                    " ".join( takewhile(lambda x: x != "$end", tokeniser)) )

  def vcd_enddefinitions(self, tokeniser, keyword):
    self.enddefinitions = True
    self.drop_declaration(tokeniser, keyword)

  def vcd_scope(self, tokeniser, keyword):
    self.scope.append( tuple(takewhile(lambda x: x != "$end", tokeniser)))

  def vcd_upscope(self, tokeniser, keyword):
    self.scope.pop()
    tokeniser.next()

  def vcd_var(self, tokeniser, keyword):
    var_type, size, identifier_code, reference = tuple(takewhile(lambda x: x != "$end", tokeniser))[:4]
    if reference.startswith('\\'): # disgard '\' for Verilog escape identifiers
        reference = reference[1:]
    if var_type == 'wire':
      self.add_module_wires(reference, int(size))    
    reference = self.scope + [('var', reference)]
    self.idcode2references[identifier_code].append( (var_type, size, reference))

  def vcd_dumpall(self, tokeniser, keyword): pass
  def vcd_dumpoff(self, tokeniser, keyword): pass
  def vcd_dumpon(self, tokeniser, keyword): pass
  def vcd_dumpvars(self, tokeniser, keyword): pass

  def vcd_end(self, tokeniser, keyword):
    if not self.enddefinitions:
      self.parse_error(tokeniser, keyword)
       
  def scalar_value_change(self, value, id):
    if self.section_tracker == PhaseEnum.ENTER_CHNG_SECT:
      self.changes[id] = value

  def vector_value_change(self, format, number, id):
    if self.section_tracker == PhaseEnum.ENTER_INIT_SECT:
      self.wire_init[id] = (format, number)
    elif self.section_tracker == PhaseEnum.ENTER_CHNG_SECT:
      self.changes[id] = (format, number)

  def add_module_wires(self, wire_name, wire_size):
    mod = self.scope[-1] 
    if(wire_size > 1):
      wires = ["{}[{}]".format(wire_name, bit) for bit in range(wire_size)]  
      self.module_wires[mod].extend(wires)
    else:    
      self.module_wires[mod].append(wire_name)


