#! /usr/bin/env python
"""
An RTL error injector used for injecting different types of of single event upsets (SEUs) into
an existing RTL module. The module is then simulated with provided inputs until a non-masked
error is reached. Additionally, a "checker" function/template can be provided to simulate
error-checking to filter errors that would be caught. The results at the end of the simulation
return the number of iterations, number of filtered errors, and the correct/incorrect outputs.

Features
========
- Source RTL uses Gate-level verilog (cell-library can be normal)
- FLIP, STUCKAT0, and STUCKAT1 faults
- Inject multiple errors (of a single type)
- Inject errors only at the output of a certain module (e.g. FF)
- Support for pipelined or clocked logic
- Support for checker functions to determine filtered/masked errors (Mako template)
- Caches intermediate structures for quick parsing/running (using pickle)
- Provides information on final/injected error and all/attempted injections

Examples
========
    Basic 4-bit adder::

        $ ./src/inject.py -v -p $RTL_DIR -l $CELL_LIBRARY -s add_4bit.v -t Add_syn -n 1 -f FLIP -i 3 A -i 6 B -o Z

See Also:
    For more information/parameters::

        $ ./inject.py --help

Attributes
==========
Attributes:
    IVERILOG    (str): iverilog command (used for simulation)
    OUT_V       (str): output (including injection logic) verilog filename
    TB_TEMPLATE (str): testbench template path (relative to script directory)
    TB_V        (str): testbench filename
    ERROR_SEQ   (str): error sequence RTL wire name

Methods
=======
"""

# Base imports
import os, sys, re, random, tempfile, subprocess, json, shutil, copy, argparse
from math import factorial

# Paths
SCRIPT_DIR = os.path.dirname(os.path.realpath(__file__))
IVERILOG = "iverilog"
TB_TEMPLATE = "templates/tb.mako"

# Filenames
TB_V = "tb.v"
OUT_V = "out.v"
IN_PIPE_NAME = "in_pipe.txt"
OUT_PIPE_NAME = "out_pipe.txt"

# RTL
ERROR_SEQ = "err_seq"

# TODO: remove?
PRIMITIVES = ["and", "nand", "or", "nor,", "xor", "xnor"]
INSERT_AT_OUTPUT = True

# Lib Paths
sys.path.insert(0, SCRIPT_DIR)

# YAML
from yaml import load as yaml_load
from yaml import safe_dump as yaml_dump

# Verilog Parser
import pyverilog.vparser.ast as ast
from pyverilog.ast_code_generator.codegen import ASTCodeGenerator
import verilog_parser as parser

# Templating
from mako_helper import to_mako
from mako.template import Template
from mako.lookup import TemplateLookup

# Caching
from cacheable import Cacheable

# Generators
import generators

# Globals
verbose = False

# TODO: add debug switch (for logging)
# TODO: better documentation!

def vprint(string):
    """
    Verbose print (only printed with the -v/--verbose switch)

    Args:
        string (str): string to print
    """
    if verbose:
        sys.stderr.write(str(string) + "\n")
        sys.stderr.flush()

def parse_cell_library(path, module_io):
    """
    Parse the RTL cell-library provided. Basicially extract the different cells and their I/O.

    Args:
        path        (str): cell library path
        module_io   (dict): dict of all modules parsed (in a format similar to: parse_modules)
    """

    # Parsing regex (module and input/output respectively)
    module_re = re.compile(r"module (.*?)\(")
    io_re = re.compile(r"(input|output).*?([\w0-9_]+)[;, ]+")

    # Open cell library
    with open(path, 'r') as cells:
        mod = ""
        record = False
        for l in cells:
            # FIXME: could have issues with inner module defs (do they exist?)
            if 'module' in l:
                if 'endmodule' in l:
                    # End module
                    record = False
                else:
                    # Start module
                    m = re.match(module_re, l)
                    mod = m.group(1).strip()
                    module_io[mod] = {'ins': 0, 'lib': True, 'io': {}}
                    record = True
            elif record:
                # Record inputs/outputs
                for io in re.finditer(io_re, l):
                    # Assume width 1
                    # FIXME: include width!
                    module_io[mod]['io'][io.group(2)] = (io.group(1), 1) # group(2): io name; group(1): 'input' or 'output'


# Parse module info (io, wires, etc)
def parse_modules(node, module_io, parents=[]):
    """
    Parse the module information (recursively). Basically extract modules and their I/O, wires, connection info.

    Args:
        node        (object): current node object (AST object)
        module_io   (dict): dict of all modules parsed
        parents     (list): list of parent nodes/objects [optional]

    module_io format::

        {
            '$module_name': {
                'def': module_def,              # Module Def (AST node)
                'ins': instances,               # Number of instances
                'lib': cell_lib,                # Is Cell library (bool)
                'io': {
                    '$io_name': 'input|output'  # I/O name and input/output
                },
                'in_wires': {                   # Wires into the module/instance
                    '$wire_name': [
                        [
                            wire_node,          # AST node for wire/pointer/partselect
                            parent_node,        # Parent node (module instance)
                            port_node,          # Port node (ast.Port)
                            index_in_concat     # Index in wire concat (i.e. { line[1:0], wire, 1'b0 })
                        ]
                    ]
                },
                'out_wires': {                  # Wires out of the module
                    '$wire_name': [
                        wire_node,              # AST node for wire/pointer/partselect
                        parent_node,            # Parent node (module instance)
                        port_node,              # Port node (ast.Port)
                        index_in_concat         # Index in wire concat (i.e. { line[1:0], wire, 1'b0 })
                    ]
                }
            }
        }
    """

    parent = None
    if parents:
        parent = parents[0]

    # Found module, add to list
    if isinstance(node, ast.ModuleDef):
        module_io[node.name] = {'def': node, 'ins': 0, 'lib': False, 'io': {}, 'in_wires': {}, 'out_wires': {}}

    # Recursively parse children
    if node.children():
        for c in node.children():
            parse_modules(c, module_io, [node] + parents)

    # Found I/O, add to list for module
    if isinstance(node, ast.Variable):
        for p in parents:
            if isinstance(p, ast.ModuleDef):
                # input, output, wire, reg
                node_type = type(node).__name__.lower()
                module_io[p.name]['io'][node.name] = (node_type, int(node.width.msb.value)-int(node.width.lsb.value)+1)
                break;

    # Found net/wire usage, add to list
    if isinstance(node, ast.PortArg) and isinstance(parent, ast.Instance):
        ports = [node.argname]
        # If a concat of wires, add each one
        if isinstance(node.argname, ast.Concat):
            ports = node.argname.list

        # Record I/O
        for pi, port in enumerate(ports):
            wire_name = ''
            # FIXME: do I need to record width? If everything ends at a gate, shouldn't it only be one wire?
            if isinstance(port, ast.Identifier):
                # Regular wire
                wire_name = port.name
            elif isinstance(port, ast.Pointer):
                # Pointer (i.e. wire[x])
                wire_name = port.var.name + ' [' + str(port.ptr) + ']'
            elif isinstance(port, ast.Partselect):
                # Range (i.e. wire[x:y])
                wire_name = port.var.name + ' [' + str(port.msb) + ':' + str(port.lsb) + ']'
            elif not isinstance(port, ast.IntConst):
                # Unknown/unparsed port (skip)
                print "UNKNOWN TYPE:",type(port),port.__dict__
                continue

            # Wires
            # TODO: add primatives to lib?
            if parent.module in module_io and module_io[parent.module]['lib'] and module_io[parent.module]['io'][node.portname][0] in ['input', 'inout']:
                # Input into terminal module (cell library)
                for p in parents:
                    if isinstance(p, ast.ModuleDef):
                        if not wire_name in module_io[p.name]['in_wires']:
                            module_io[p.name]['in_wires'][wire_name] = []
                        module_io[p.name]['in_wires'][wire_name].append([node, parent, port, pi])
            elif parent.module in module_io and module_io[parent.module]['lib'] and module_io[parent.module]['io'][node.portname][0] in ['inout', 'output']:
                # Output from terminal module (for selecting wires)
                for p in parents:
                    if isinstance(p, ast.ModuleDef):
                        module_io[p.name]['out_wires'][wire_name] = [node, parent, port, pi]

def rename_wire(port, wire):
    """
    Rename wire based upon convention. In this case add "\\__" to the front.

    Args:
        port (object): AST portarg (could be different types)
        wire (str): wire to rename

    Returns:
        str: renamed wire
    """

    if wire.startswith("\\"):
        wire = wire[1:]
    wire = re.sub(r"\s", "", wire)
    return r"\__" + wire + " "

# Inject error logic
def inject_error_mappings(node, top, parents, parent_wires, path, module_io, mapping, fault_type, targets):
    """
    Inject error signals and error logic

    Looks at the output of "terminal" (gate-level) modules and inserts error logic and a select line.
    Error select (ERROR_SEQ) are propagated to each module defintion and connected to each module instance,
    as well as connected to error logic. At the end, the resulting top-level module contains the error
    sequence as input which is used to select which error "cells" are active.

    Args:
        node            (object): current node object (AST object)
        top             (str): top level module name
        parents         (list): list of parent nodes/objects
        parent_wires    (list): list of wires existing in the parent module (used to propegate wires)
        path            (str): current RTL path (instance names)
        module_io       (dict): dict of all modules parsed
        mapping         (list): error signal/bit to RTL signal mapping
        fault_type      (str): fault type (e.g. FLIP)
        targets         (list): name of modules to inject logic at (None for all)

    Returns:
        list: list of wires with injected logic at that node
    """

    # Quick parent reference
    parent = None
    if parents:
        parent = parents[0]

    # Associated wires
    children_wires = []
    if isinstance(node, ast.ModuleDef):
        parent_wires = []

    # Recursively parse children
    if node.children():
        for c in node.children():
            # Only parse from top module (or an instance from it)
            # TODO: better way?
            if (isinstance(node, ast.ModuleDef) and (node.name == top or isinstance(parent, ast.Instance))) or not isinstance(node, ast.ModuleDef):
                w = inject_error_mappings(c, top, [node] + parents, parent_wires, path, module_io, mapping, fault_type, targets)
                children_wires.extend(w)
                if isinstance(node, ast.ModuleDef):
                    parent_wires.extend(w)
            elif isinstance(node, ast.ModuleDef):
                return []

    # Found module instance, follow it
    if isinstance(node, ast.Instance):
        # Add instance
        #print node.name, node.module
        try:
            module_io[node.module]['ins'] += 1
        except:
            print node.module, node.name, path, [x.__dict__ for x in parents]
            raise Exception('sdsd')
        # Follow instance by looking into module def
        if node.module in module_io and module_io[node.module] and not module_io[node.module]['lib']:
            module_wires = inject_error_mappings(module_io[node.module]['def'], top, [node] + parents, parent_wires, path + [node.name], module_io, mapping, fault_type, targets)
            # Add module wires
            if len(module_wires) > 0:
                err_inputs = ast.Partselect(ast.Identifier(ERROR_SEQ), ast.IntConst(len(parent_wires)+len(module_wires)-1), ast.IntConst(len(parent_wires)))
                node.portlist = [x for x in node.portlist] + [ast.PortArg(ERROR_SEQ, err_inputs)]

            children_wires.extend(module_wires)

    # Found module, process wires
    # FIXME: issue, if output module is in another module, output goes to another instance
    if isinstance(node, ast.ModuleDef):
        #print node.name
        if not 'wires' in module_io[node.name]:
            wires = []
            mod_wires = []
            mod_logic = []
            if INSERT_AT_OUTPUT:
                # Insert error injection at output (rename output wire)
                for (wire, w_inst) in module_io[node.name]['out_wires'].iteritems():
                    w_name = wire
                    (port, inst, portarg, pi) = w_inst
                    # If an targets is given, and the instance module doesn't match, skip
                    if targets and inst.module not in targets:
                        continue

                    # Update output
                    w_name = rename_wire(portarg, wire)
                    if isinstance(port.argname, ast.Concat):
                        port.argname.list[pi] = ast.Identifier(w_name)
                    else:
                        port.argname = ast.Identifier(w_name)
                    # Generate signal path
                    wf_path = '.'.join([x + ' ' if x.startswith('\\') else x for x in path + [wire]])
                    wt_path = '.'.join([x + ' ' if x.startswith('\\') else x for x in path + [w_name]])
                    mapping.append((wf_path, str(inst.module), wt_path))

                    # AST update
                    bits = [""]
                    if isinstance(portarg, ast.Partselect):
                        bits = [" ["+str(x)+"]" for x in range(int(portarg.lsb), int(portarg.msb)+1)]
                        mod_wires.append(ast.Wire(w_name, ast.Width(portarg.msb, portarg.lsb)))
                    else:
                        mod_wires.append(ast.Wire(w_name))
                    # Insert error logic
                    for b in bits:
                        wires.append(w_name + b)
                        err_logic_sig = ERROR_SEQ + "[" + str(len(children_wires)+len(wires)-1) + "]"
                        if fault_type == "FLIP":
                            # FLIP fault
                            mod_logic.append(inject_gate("xor", [w_name+b, err_logic_sig], [wire]))
                        elif fault_type == "STUCKAT1":
                            # STUCKAT1 fault
                            mod_logic.append(inject_gate("or", [w_name+b, err_logic_sig], [wire]))
                        elif fault_type == "STUCKAT0":
                            # STUCKAT0 fault
                            mod_logic.append(inject_gate("and", [w_name+b, "~"+err_logic_sig], [wire]))
            else:
                # TODO: remove?
                # Insert error injection at inputs (rename input wires)
                # - Good if you want to inject at reg or something you can't rename?
                for (wire, w_inst) in module_io[node.name]['in_wires'].iteritems():
                    w_name = wire
                    if targets and (wire not in module_io[node.name]['out_wires'] or module_io[node.name]['out_wires'][wire][1].module not in targets):
                        continue

                    # Update inputs
                    for (port, parent) in w_inst:
                        w_name = rename_wire(port.argname, wire)
                        port.argname = ast.Identifier(w_name)
                    wires.append(w_name)
                    # FIXME: update or remove
                    wf_path = '.'.join([x + ' ' if x.startswith('\\') else x for x in path + [wire]])
                    wt_path = '.'.join([x + ' ' if x.startswith('\\') else x for x in path + [w_name]])
                    mapping.append((wf_path, "", wt_path))

                    # AST update
                    mod_wires.append(ast.Wire(w_name))
                    err_logic_sig = ERROR_SEQ + "[" + str(len(children_wires)+len(wires)-1) + "]"
                    if fault_type == "FLIP":
                        mod_logic.append(inject_gate("xor", [wire, err_logic_sig], [w_name]))
                    elif fault_type == "STUCKAT1":
                        mod_logic.append(inject_gate("or", [wire, err_logic_sig], [w_name]))
                    elif fault_type == "STUCKAT0":
                        mod_logic.append(inject_gate("and", [wire, "~"+err_logic_sig], [w_name]))

            # Error inputs
            if len(wires)+len(children_wires) > 0:
                if node.portlist.ports and isinstance(node.portlist.ports[0], ast.Port):
                    # I/O wires method
                    mod_wires.append(ast.Input(ERROR_SEQ, ast.Width(ast.IntConst(len(wires)+len(children_wires)-1), ast.IntConst(0))))
                    node.portlist.ports = [x for x in node.portlist.ports] + [ast.Port(ERROR_SEQ, None, None)]
                else:
                    err_inputs = ast.Input(ERROR_SEQ, ast.Width(ast.IntConst(len(wires)+len(children_wires)-1), ast.IntConst(0)))
                    node.portlist.ports = [x for x in node.portlist.ports] + [ast.Ioport(err_inputs)]

                node.items = mod_wires + [x for x in node.items] + mod_logic
            module_io[node.name]['wires'] = wires

        children_wires.extend(module_io[node.name]['wires'])

    return children_wires

def inject_gate(gate, inputs, outputs):
    """
    Generic inject gate

    Construct a gate (ast.Instance) with specified inputs and outputs

    Args:
        gate    (str): the name of the gate to create
        inputs  (list): a list of inputs (wire names)
        outputs (list): a list of outputs (wire names)

    Returns:
        object: ast.InstanceList which contains new gate
    """

    port_list = []

    # FIXME: add Partselect!!!
    for out in outputs:
        if not out.startswith("\\") and "[" in out:
            m = re.match(r"([\w_]+) ?\[(\d+)\]", out)
            port_list.append(ast.PortArg(None, ast.Pointer(ast.Identifier(m.group(1)), ast.IntConst(m.group(2)))))
        else:
            port_list.append(ast.PortArg(None, ast.Identifier(out)))
    for inp in inputs:
        if not inp.startswith("\\") and "[" in inp:
            m = re.match(r"([\w_\~]+) ?\[(\d+)\]", inp)
            port_list.append(ast.PortArg(None, ast.Pointer(ast.Identifier(m.group(1)), ast.IntConst(m.group(2)))))
        else:
            port_list.append(ast.PortArg(None, ast.Identifier(inp)))

    # HAS to be in InstanceList!
    ins = ast.Instance(gate, "", port_list, [])
    return ast.InstanceList(gate, [], [ins])

def create_env(dir=None):
    """
    Create simulation environment (temp directory)

    Args:
        dir (str): dir for environment [optional]

    Returns:
        str: temporary or specified directory path
        bool: the env will be saved or not
    """
    save = False
    if dir:
        #if os.path.isdir(dir):
        #    os.makedirs(dir)
        save = True
        return (dir, save)
    tmp = tempfile.mkdtemp()
    return (tmp, save)

def emit_verilog_ast(ast_root):
    codegen = ASTCodeGenerator()
    return codegen.visit(ast_root)

def save_verilog_ast(out, path):
    """
    Save the AST as verilog

    Args:
        ast_root    (object): AST structure
        path        (str): Path to save at
    """

    with open(path, 'w') as outf:
        outf.write(out)

def generate_testbench(env, top_module, inputs, outputs, module_io_mapping, error_mapping, clk_port, clk_stages, checker_file, checker_options, error_num=1, debug=False):
    """
    Generate the testbench file

    Args:
        env                 (str): path of environment (to copy files)
        top_module          (str): name of the top-level module
        module_io_mapping   (dict): mapping of ports for the top-level module (for widths)
        error_mapping       (list): mapping of error signals
        clk_port            (str): name of CLK port
        clk_stages          (int): number of stages for pipelined logic
        checker_file        (str): path to checker template
        checker_options     (dict): parsed JSON dict of options for the checker template
        error_num           (int): number of errors to inject unmasked by the circuit
    """

    # Open template
    with open(os.path.join(SCRIPT_DIR, TB_TEMPLATE), 'r') as tbf:
        lookup_paths = TemplateLookup(directories=[".", SCRIPT_DIR])
        tb_template = Template(tbf.read(), lookup=lookup_paths)
        # Template data
        data = { 'top_module': top_module, 'inputs': [], 'outputs': []}

        # Format inputs (data from input file)
        for port, value in inputs.iteritems():
            io_data = {}
            io_data['name'] = port
            io_data['width'] = module_io_mapping[port][1]

            # Datatypes
            if str(value).startswith('0b'):
                io_data['datatype'] = 'b';
                io_data['value'] = value[2:] # strip '0b'
            elif str(value).startswith('0x'):
                io_data['datatype'] = 'h';
                io_data['value'] = value[2:] # strip '0x'
            else:
                io_data['datatype'] = 'd';
                io_data['value'] = value 

            data['inputs'].append(io_data)

        # Format outputs
        for port, value in outputs.iteritems():
            io_data = {}
            io_data['name'] = port
            io_data['width'] = module_io_mapping[port][1]

            data['outputs'].append(io_data)

        # Render template
        out = tb_template.render(err_name=ERROR_SEQ,
                                 in_pipe_name=IN_PIPE_NAME,
                                 out_pipe_name=OUT_PIPE_NAME,
                                 err_len=len(error_mapping),
                                 err_num=error_num,
                                 clk_port=clk_port,
                                 clk_stages=clk_stages,
                                 checker_file=checker_file,
                                 checker_options=to_mako(checker_options),
                                 top_module=data['top_module'],
                                 inputs=to_mako(data['inputs']),
                                 outputs=to_mako(data['outputs']),
                                 mapping=error_mapping.get(),
                                 debug=debug)

        # Write testbench
        with open(os.path.join(env, TB_V), 'w') as of:
            of.write(out)

## Simulate
#
#  @param input_proto Input protobuf (object)
#  @param rtl_module_proto RTL Module protobuf (object)
def simulate(env, faults, cell_library_path, wire_mapping):
    """
    Compile and Simulate verilog testbench

    The generated testbench and RTL module are synthesized and compiled. Pipes are created for input
    and output to the testbench. The function then feeds the testbench random error sequences which
    are simulated and evaluated. The results are then returned to the script in a JSON format.

    Args:
        env                 (str): path of environment (to copy files)
        faults              (str): number of faults to inject
        cell_library_path   (str): path to cell library
        wire_mapping        (list): list of error signal mappings

    Returns:
        dict: parsed JSON output from the testbench
    """

    # Combine
    arg_comb = [IVERILOG, "-E", "-o"]
    arg_comb.append(TB_V+"o")
    arg_comb.append(TB_V)
    arg_comb.append(OUT_V)
    arg_comb.append(os.path.abspath(cell_library_path))

    vprint("  > " + ' '.join(arg_comb))
    comb = subprocess.Popen(arg_comb, stdout=subprocess.PIPE, stderr=subprocess.PIPE, cwd=env)
    (out, err) = comb.communicate()

    # Compile
    arg_comp = [IVERILOG, "-o", TB_V+"pp", "-s", "tb", TB_V+"o"]

    vprint("  > " + ' '.join(arg_comp))
    comp = subprocess.Popen(arg_comp, stdout=subprocess.PIPE, stderr=subprocess.PIPE, cwd=env)
    (out, err) = comp.communicate()

    # Simulate
    sim = subprocess.Popen(["./"+TB_V+"pp"], stdout=subprocess.PIPE, stderr=subprocess.PIPE, cwd=env)

    # Feed in data
    in_pipe_name = os.path.join(env, IN_PIPE_NAME)
    injected_fault_ids = generators.feed_pipe(in_pipe_name, len(wire_mapping), faults)

    # Get results
    (out, err) = sim.communicate()
    out_pipe_name = os.path.join(env, OUT_PIPE_NAME)
    opipe = os.open(out_pipe_name, os.O_RDONLY)

    # Read from pipe
    result = ""
    rd = os.read(opipe, 1024)
    while rd:
        result += rd
        rd = os.read(opipe, 1024)
    os.close(opipe)
    os.remove(out_pipe_name)
    vprint("  > " + str(result))

    # Parse JSON results and return
    json_data = None
    try:
        json_data = json.loads(result)
    except ValueError as e:
        print result
        raise Exception(str(e))
    return (json_data, injected_fault_ids)

def startup(lib, src, top, fault="FLIP", num=1, targets=[], path="./", cache="cache"):
    """
    Starts up the injector, but does not simulate

    Basically used for caching preliminary structures to speed up simulation and execution.

    Args:
        lib     (str): cell library path
        src     (str): RTL module path
        top     (str): top-level module name
        fault   (str): fault type (e.g. FLIP) [optional]
        num     (int): number of errors to inject [optional]
        targets (list): output modules to inject error logic at [optional]
        path    (str): directory for RTL files [optional]
        cache   (str): directory for storing cache files [optional]
    """
    vprint("Caching " + src + "...")
    run(lib, src, top, fault, num, targets, path=path, cache=cache, run_sim=False)

def run(lib, src, top, fault="FLIP", num=1, targets=[], out_groups=[], inputs={}, outputs={}, clk_port=None, clk_stages=0, checker_file=None, checker_options=None, group_mapping={}, path="./", dir=None, cache="cache", run_sim=True, info=False, debug=False):
    """
    General run function

    Basically the Front-end for the RTL injector. Runs all other functions and is responsible
    for accepting inputs from the command line or external calls.

    Args:
        lib             (str): cell library path
        src             (str): RTL module path
        top             (str): top-level module name
        fault           (str): fault type (e.g. FLIP) [optional]
        num             (int): number of errors to inject [optional]
        targets         (list): output modules to inject error logic at [optional]
        out_groups      (list): modules to display
        inputs          (dict): input ports/values [optional]
        outputs         (dict): output ports (0 value) [optional]
        clk_port        (str): CLK port name [optional]
        clk_stages      (int): number of stages in pipelined logic [optional]
        checker_file    (str): path to MAKO checker template [optional]
        checker_options (dict): parsed JSON options for MAKO template [optional]
        group_mapping   (dict): parsed YAML group/module mapping
        path            (str): directory for RTL files [optional]
        dir             (str): directory path for generated verilog files [optional]
        cache           (str): directory for storing cache files [optional]
        run_sim         (bool): whether to run the simulation or not
        info            (bool): whether to display RTL statistics/info
    """

    # Fix Nonetypes
    if not out_groups:
        out_groups = []
    if not group_mapping:
        group_mapping = {}

    # Replace keywords/groups in targets
    new_targets = []
    if targets:
        for t in targets:
            if t in group_mapping:
                new_targets.extend(group_mapping[t])
            else:
                new_targets.append(t)
    targets = new_targets

    # Setup caches
    cache_src = os.path.splitext(os.path.basename(src))[0]
    if Cacheable.setupPath(cache, cache_src) and not run_sim:
        return

    # Parse AST gen (shortcut)
    emitted_ast = Cacheable("emitted_ast", value_deps=[top, src, lib, targets, fault])

    # Parse RTL source (module)
    rtl_module_path = os.path.join(path, src)
    if not os.path.isfile(rtl_module_path):
        raise Exception("RTL file (" + rtl_module_path + ") does not exist!")
    vobj = Cacheable("init_ast", value_deps=[top, src])

    # Cell Library
    cell_lib_path = os.path.join(path, lib)
    if not os.path.isfile(cell_lib_path):
        raise Exception("Cell Library \"%s\" does not exist!" % cell_lib_path)
    module_io = Cacheable("module_io", value_deps=[top, lib], cache_deps=[vobj], group="refs")
    if not module_io.isValid():
        if not vobj.isValid():
            vprint("Parse module...")
            vobj.set(parser.parse([rtl_module_path], table_output_dir=SCRIPT_DIR))
        else:
            vprint("Retrieved module from cache...")

        parse_cell_library(cell_lib_path, module_io)
        parse_modules(vobj, module_io)
        vprint("Parsed cell lib...")
    else:
        vprint("Retrieved module from cache...")
        vprint("Retrieved cell lib from cache...")


    # Mapping and Injection
    mapping = Cacheable("wire_mapping", value_deps=[top, targets, fault])
    final_vobj = Cacheable("final_ast", value_deps=[top, targets, fault], cache_deps=[vobj], group="refs")
    vobj.end()
    if not mapping.isValid() or (not emitted_ast.isValid() and not final_vobj.isValid()):
        final_vobj.set(vobj.get())
        mapping.set([])
        inject_error_mappings(final_vobj, top, [], [], [], module_io, mapping, fault, targets)
        vprint("Injecting error logic...")
    else:
        vprint("Retrieved error logic from cache...")
    
    assert len(mapping), "[RTL Error] Failed to inject error logic. Probably due to no '{}' in the circuit '{}'".format(targets, top)

    # Emit verilog ast
    if not emitted_ast.isValid():
        emitted_ast.set(emit_verilog_ast(final_vobj))
        vprint("Emitting ast...")

    # TODO: Do other processing?
    if not run_sim:
        Cacheable.cleanup()
        return

    # TODO: cache generated files (e.g. testbench)?

    # Create env
    (env, save_env) = create_env(dir)
    if save_env:
        vprint("Using environment directory (" + env + ")...")
    else:
        vprint("Creating tmp directory (" + env + ")...")

    # Create tmp dir and save AST
    save_verilog_ast(emitted_ast.get(), os.path.join(env, OUT_V))
    vprint("Save RTL...")

    # Testbench
    generate_testbench(env, top, inputs, outputs, module_io[top]['io'], mapping, clk_port, clk_stages, checker_file, checker_options, 1, debug)
    vprint("Generate testbench...")

    # Simulate
    (results, injected_fault_ids) = simulate(env, num, cell_lib_path, mapping)
    vprint("Simulate...")

    # Dump Synthesized RTL for future post-processing (e.g. computing masking factor)
    shutil.copy(os.path.join(env, TB_V+"o"), os.getcwd())
    vprint("Dump synthesized RTL to " + os.getcwd() + " ...")

    # Clean up
    if not save_env:
        shutil.rmtree(env)
        vprint("Cleanup...")

    # Cleanup cache
    Cacheable.cleanup()

    # Add additional results info
    results['total_combinations'] = factorial(len(mapping)) / (factorial(num) * factorial(len(mapping) - num))
    iterations = results['iterations']

    module_group_mapping = {}
    for group, mods in group_mapping.iteritems():
        for m in mods:
            module_group_mapping[m] = group

    if not results['err']:
        injected_fault_ids = injected_fault_ids[0:iterations]

        # Injected fault info
        if iterations <= len(injected_fault_ids):
            ewires = []
            emodules = []
            for err in injected_fault_ids[-1]:
                (ewire, emodule) = mapping[err][:2]
                ewires.append(ewire)
                emodules.append(emodule)
            results['injection'] = { 'wires': ewires, 'modules': emodules }

        # Create list of error counts (grouped)
        egroups = { 'OTHER' : 0 }
        if out_groups or group_mapping:
            for g in out_groups + group_mapping.keys():
                egroups[g] = 0
        for faults in injected_fault_ids:
            for err in faults:
                (ewire, emodule) = mapping[err][:2]
                if emodule in module_group_mapping:
                    emodule = module_group_mapping[emodule]
                elif not out_groups or emodule not in out_groups:
                    emodule = "OTHER"
                egroups[emodule] += 1

        results['iteration_groups'] = egroups

    # Add RTL statistics/info
    if info:
        # Cell counts
        lib_module_count = { 'OTHER' : 0 }
        if out_groups or group_mapping:
            for g in out_groups + group_mapping.keys():
                lib_module_count[g] = 0
        for mod in module_io.keys():
            if not module_io[mod]['lib']:
                continue
            grp = mod
            if mod in module_group_mapping:
                grp = module_group_mapping[mod]
            elif mod not in out_groups:
                grp = "OTHER"
            lib_module_count[grp] += module_io[mod]['ins']
        results['cell_counts'] = lib_module_count

    # Results!
    return results

def print_results(results, pretty=False):
    """
    Pretty printer for results

    Args:
        results (dict): dict of results
        pretty  (bool): pretty printing
    """

    output = ""
    if pretty:
        output = yaml_dump(results, indent=2, encoding='utf-8', default_flow_style=False,
                           allow_unicode=True, explicit_start=True, explicit_end=True)
    else:
        output = str(results)

    print output

def parse_checker_args(args):
    """
    Parse checker args

    Args:
        args (list): [0] checker file, [1] checker options

    Returns:
        tuple: [0] parsed checker file, [1] parsed checker options dict
    """

    checker_file = None
    checker_options = None
    if args:
        checker_file = args[0]
        checker_options = args[1]
        if checker_options:
            checker_options = json.loads(checker_options)

    return (checker_file, checker_options)

def parse_clk_args(args):
    """
    Parse clk args

    Args:
        args (list): [0] clk port, [1] clk stages

    Returns:
        tuple: [0] parsed clk port, [1] parsed clk stages
    """

    clk_port = None
    clk_stages = 0
    if args:
        clk_port = args[0]
        clk_stages = args[1]
        if not clk_stages:
            clk_port = None

    return (clk_port, clk_stages)

def parse_group_mapping(mapping_filename, lib_path, dir_path = "./"):
    """
    Parse group mappings

    Args:
        mapping_filename (str): group mapping filename
        lib_path         (str): cell library path
        dir_path         (str): RTL directory path

    Returns:
        dict: group mappings
    """
    group_mappings = {}
    if not dir_path:
        dir_path = "./"
    if not mapping_filename:
        mapping_filename = os.path.splitext(os.path.join(dir_path, lib_path))[0] + ".yaml"
    if os.path.isfile(mapping_filename):
        vprint("Using group mappings '" + os.path.basename(mapping_filename) + "'")
        with open(mapping_filename, 'r') as yf:
            group_mappings = yaml_load(yf)

    return group_mappings

def main(args):
    """
    Main function

    Called when executed from the command line. Collects arguments and runs the RTL injector.

    Args:
        args (list): list of command line arguments
    """

    global verbose

    parser = argparse.ArgumentParser(description="Inject errors/logic into RTL module and simulate")
    # Options
    parser.add_argument('-v', '--verbose', action='store_true', help="Verbose")
    parser.add_argument('-l', '--lib', required=True, help="Cell library")
    parser.add_argument('-s', '--src', required=True, help="RTL file")
    parser.add_argument('-t', '--top', required=True, help="Top module")
    parser.add_argument('-f', '--fault', choices=["FLIP", "STUCKAT0", "STUCKAT1"], help="Fault model", default="FLIP")
    parser.add_argument('-n', '--num', type=int, help="Number of faults to inject", default=1)
    parser.add_argument('-c', '--cache', help="Cache directory", default="cache")
    parser.add_argument('-p', '--path', help="RTL path", default="./")
    parser.add_argument('-x', '--target', action='append', help="Module/keyword/groups to inject errors at (output)")
    parser.add_argument('-e', '--checker', nargs=2, metavar=("FILE", "OPTIONS"), help="Filter checker file (mako template) and JSON options")
    parser.add_argument('-i', '--input', nargs=2, metavar=("VALUE", "PORT"), action='append', help="Input value/port")
    parser.add_argument('-o', '--output', action='append', help="Return/Output ports")
    parser.add_argument('-g', '--group', action='append', help="Display groups (RTL module, keyword, group)")
    parser.add_argument('-k', '--clk', nargs=2, metavar=("PORT", "STAGES"), help="Clk port and amount of stages")
    parser.add_argument('-d', '--dir', help="Output directory")
    parser.add_argument('-q', '--info', action='store_true', help="Display RTL info/statistics")
    parser.add_argument('-m', '--mapping', help="YAML cell library mappings/keywords/groupings")
    parser.add_argument('-y', '--pretty', action='store_true', help="Pretty print!")
    parser.add_argument('-z', '--debug', action='store_true', help="Debugging")

    # Parse args
    options = parser.parse_args(args)
    verbose = options.verbose

    # Initial arg check
    if not options.lib and not options.src and not options.top:
        parser.print_help()
        sys.exit(1)

    # Parse input/output mapping
    input_ports = {}
    output_ports = {}
    if options.input:
        for (v, k) in options.input:
            input_ports[k] = v

    if options.output:
        for io in options.output:
            output_ports[io] = 0

    # Checker params
    (checker_file, checker_options) = parse_checker_args(options.checker)

    # Clk params
    (clk_port, clk_stages) = parse_clk_args(options.clk)

    # Find/parse mapping file
    group_mappings = parse_group_mapping(options.mapping, options.lib, options.dir)

    # Run
    if options.output:
        result = run(options.lib, options.src, options.top, options.fault, options.num, options.target, options.group, input_ports, output_ports, clk_port, clk_stages, checker_file, checker_options, group_mappings, options.path, options.dir, options.cache, info=options.info, debug=options.debug)

        # Print result
        print_results(result, options.pretty)

    else:
        startup(options.lib, options.src, options.top, options.fault, options.num, options.target, options.path, options.cache)

if __name__ == '__main__':
    main(sys.argv[1:])
