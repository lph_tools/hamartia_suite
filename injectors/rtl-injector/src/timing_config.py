import os

def _config_get_assert(config, field):
    result = config[field] if field in config else None
    if not result:
        raise Exception("{} field needed in config!".format(field))
    return result

def _get_cell_lib(config, modules):
    cell_libs = _config_get_assert(config, 'cell_libs')
    # Assume all modules synthesized with the same cell library
    lib = modules[0]['cell_lib']
    if lib in cell_libs:
        rtl_dir = cell_libs[lib]['dir']
        lib = os.path.join(rtl_dir, cell_libs[lib]['src'])
    else:
        raise Exception("{} is not one of the 'cell_libs' of config!".format(lib))
    return lib

def _get_rtl_src(config, modules):
    dirs = _config_get_assert(config, 'netlist_dirs')
    rtl_src_list = [os.path.join(dirs[module['dir']], module['rtl_src']) for module in modules]
    return rtl_src_list

def _get_corners(config, modules):
    libs = _config_get_assert(config, 'liberties')
    # Assume all modules synthesized with the same cell library
    lib = modules[0]['cell_lib']
    corners = {}
    if lib in libs:
        dir = libs[lib]['dir']
        for k, flist in libs[lib]['corners'].iteritems():
            corners[k] = [os.path.join(dir, f) for f in flist]
    else:
        raise Exception("{} is not one of the 'liberties' of config!".format(lib))
    return corners

def _get_verilog_defs(config):
    verilog_defs = []
    if 'verilog_defs' in config and config['verilog_defs']:
        verilog_defs = config['verilog_defs']
        if not isinstance(verilog_defs, list):
            verilog_defs = [verilog_defs]
    return verilog_defs

def _get_timer_path(config):
    path = _config_get_assert(config, 'open_timer_path')
    return path

def _get_fault(config):
    fault = _config_get_assert(config, 'fault')
    if any(k not in fault for k in ('type','index','duration','cycle_time')):
        raise Exception("Incomplete fault field in config!")
    return fault

def parse_config(config, modules):
    lib = _get_cell_lib(config, modules)
    rtl_src_list = _get_rtl_src(config, modules)
    corners = _get_corners(config, modules)
    verilog_defs = _get_verilog_defs(config)
    timer_path = _get_timer_path(config)
    fault = _get_fault(config)
    cache = config['cache'] if 'cache' in config else None
    return (lib, rtl_src_list, corners, verilog_defs, timer_path, fault, cache)

