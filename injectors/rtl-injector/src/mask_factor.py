#! /usr/bin/env python
"""
A script that simulates an RTL circuit based on particular inputs and fault model
chosen in a previous injection experiment to compute masking factor. It modifies 
the target unmasked fault to inject in the synthsized RTL, and performs simulation 
using iverilog's internal format. The output is dumped in a file named 
"rtl_inject.log" within the folder provided by user.

Usage
========
    $ ./mask_factor.py -p <RTL_DIR> -e <num_errors> -f <num_faults> 

    where RTL_DIR contains the synthsized RTL to simulate (tb.vo)
          num_errors is the total number of unmasked faults to inject
          num_faults is the number of faults to inject per experiment
"""
# Base imports
import os, sys, argparse, re, subprocess, json, generators
import fileinput

# Paths
IVERILOG = "iverilog"

# Filenames
TB_V = "tb.v"
TB_VO = "tb.vo"
OUT_V = "out.v"
IN_PIPE_NAME = "in_pipe.txt"
OUT_PIPE_NAME = "out_pipe.txt"
LOG_FILE = "rtl_inject.log"

# RTL
ERROR_SEQ = "err_seq"
ERR_SEQ_STR = ERROR_SEQ + " = (\d)+"
ERROR_NUM = "target_errnum"
ERR_NUM_STR = ERROR_NUM + " = (\d)+;"

def simulate(env, faults, wire_mapping_len):
    """
    Compile and Simulate verilog testbench (A simplified version of simulate() in inject.py)

    Args:
        env                 (str): path of environment (to copy files)
        faults              (str): number of faults to inject
        wire_mapping        (list): list of error signal mappings

    Returns:
        dict: parsed JSON output from the testbench
    """

    # Compile
    arg_comp = [IVERILOG, "-o", TB_V+"pp", "-s", "tb", TB_V+"o"]

    comp = subprocess.Popen(arg_comp, stdout=subprocess.PIPE, stderr=subprocess.PIPE, cwd=env)
    (out, err) = comp.communicate()

    # Simulate
    executable = os.path.join(env, TB_V+"pp")
    sim = subprocess.Popen([executable], stdout=subprocess.PIPE, stderr=subprocess.PIPE, cwd=env)

    # Feed in data
    in_pipe_name = os.path.join(env, IN_PIPE_NAME)
    injected_fault_ids = generators.feed_pipe(in_pipe_name, wire_mapping_len, faults)

    # Get results
    (out, err) = sim.communicate()
    out_pipe_name = os.path.join(env, OUT_PIPE_NAME)
    opipe = os.open(out_pipe_name, os.O_RDONLY)

    # Read from pipe
    result = ""
    rd = os.read(opipe, 1024)
    while rd:
        result += rd
        rd = os.read(opipe, 1024)
    os.close(opipe)
    os.remove(out_pipe_name)

    # Parse JSON results and return
    json_data = None
    try:
        json_data = json.loads(result)
    except ValueError as e:
        print result
        raise Exception(str(e))
    return (json_data, injected_fault_ids)

def reinject(path, errors, faults):
    """
    Redo injections to RTL synthesized in previous Monte Carlo injection experiments     

    Args:
        path                (str): path of directory containing synthesized RTL
        errors              (int): number of errors (unmasked faults) to inject
        faults              (str): number of faults to inject at a time

    """

    err_signal_num = 0

    # Obtain error signal lenths from testbench
    with open(os.path.join(path, TB_VO)) as f:
        fc = f.read()
        assert (len(fc)>0), "Synthesized RTL does not exist!"
        regex = re.compile(ERR_SEQ_STR)  
        token = regex.search(fc) 
        err_signal_num = int(token.group().split('=')[1].strip())

    assert (err_signal_num != 0), "Can't find number of error signals!"        

    # Modify number of errors to inject in testbench
    for line in fileinput.input(os.path.join(path, TB_VO), inplace=1):
        line = re.sub(ERR_NUM_STR, ERROR_NUM + " = " + str(errors) + ";", line.rstrip())
        print line # this prints to the file instead of stdout

    # Simulation
    (result, fault_ids) = simulate(path, faults, err_signal_num)

    return result
 
def main():
    parser = argparse.ArgumentParser(description="Inject errors to RTL to compute masking factor")
    parser.add_argument('-p', '--path', required=True, help="Path to directory with generated RTLs and testbench")
    parser.add_argument('-e', '--errors', default=10, help="Errors (unmasked faults) to inject in total")
    parser.add_argument('-n', '--faults', default=1, help="Faults to inject at a time")

    options = parser.parse_args()

    reinject(options.path, options.errors, options.faults)
 
if __name__ == '__main__':
    main()
