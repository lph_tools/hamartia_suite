% if clk_port:
create_clock -period ${cycle_time} -name virtual_clock [get_ports ${clk_port}]
% else:
create_clock -period ${cycle_time} -name virtual_clock
% endif
set_input_delay 0 -min -rise [all_inputs]
set_input_delay 0 -min -fall [all_inputs]
set_input_delay ${clk2q} -max -rise [all_inputs]
set_input_delay ${clk2q} -max -fall [all_inputs]
set_input_transition 0 -min -rise [all_inputs]
set_input_transition 0 -min -fall [all_inputs]
set_input_transition 0 -max -rise [all_inputs]
set_input_transition 0 -max -fall [all_inputs]
set_output_delay -0 -min -rise [all_outputs] -clock virtual_clock
set_output_delay -0 -min -fall [all_outputs] -clock virtual_clock
set_output_delay ${setup_time} -max -rise [all_outputs] -clock virtual_clock
set_output_delay ${setup_time} -max -fall [all_outputs] -clock virtual_clock
set_load -pin_load ${out_load} [all_outputs]
