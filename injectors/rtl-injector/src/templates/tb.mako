`timescale 1ps/1ps
`define IN_PIPE_NAME "${in_pipe_name}"
`define OUT_PIPE_NAME "${out_pipe_name}"
`define LOG_FILE "rtl_inject.log"
`define DEBUG_FILE "debug.log"

module tb;
	integer ifile;            // the file handle
	integer ofile;            // the file handle
	integer logfile;          // the file handle
	integer dfile;            // the file handle
	integer assignments;     // the number of correct signal assignments
	integer iterations;
	integer filtered;
	integer errnum;
	integer target_errnum;

% if clk_port:
	// Clock
	reg clk;
% endif

	// Inputs
% for io in inputs:
	reg [${io.width-1}:0] ${io.name};
% endfor

	// Outputs
% for io in outputs:
	wire [${io.width-1}:0] ${io.name};
	reg [${io.width-1}:0] ${io.name}_correct;
% endfor

	// Error sequence
	reg [${err_len-1}:0] ${err_name};

	// Module instance
	${top_module} dut(
% for io in inputs:
		.${io.name}(${io.name}),
% endfor
% for io in outputs:
		.${io.name}(${io.name}),
% endfor
% if clk_port:
		.${clk_port}(clk),
% endif
		.${err_name}(${err_name})
	);

	// Advance
	task advance;
	begin
		#1;
% if clk_port:
		repeat (${str(int(clk_stages)*2)}) begin
% if debug:
            $fwrite(dfile, "---\n");
            $fwrite(dfile, "clk: %b\n", clk);
            $fwrite(dfile, "${err_name}: %b\n", ${err_name});
% for io in inputs:
            $fwrite(dfile, "${io.name}: %b\n", ${io.name});
% endfor
% for io in outputs:
            $fwrite(dfile, "${io.name}: %b\n", ${io.name});
% endfor
% for sigf, gate, sigt in mapping:
            $fwrite(dfile, "${sigf}: %b, %b\n", dut.${sigf} , dut.${sigt} );
% endfor
% endif
			clk = ~clk;
			#1;
		end
% endif 
	end
	endtask

	// Checker function (returns true if filtered/caught)
	function checker;
	input zzz;
	begin
% if checker_file:
	<%include file="${checker_file}" args="options=checker_options, inputs=inputs, outputs=outputs" />
% else:
		checker = 1'b0;
% endif
	end
	endfunction

	initial begin
		// Initial values
		## TODO: add support for different data types
% for io in inputs:
		${io.name} = ${io.width}'${io.datatype}${io.value};
% endfor
% if clk_port:
		clk = 1'b0;
% endif
		${err_name} = ${err_len}'b0;

        // Debug
% if debug:
        dfile = $fopen(`DEBUG_FILE, "w");
% endif

		// Get Correct Result
		advance();
% for io in outputs:
		${io.name}_correct = ${io.name};
% endfor
	
		// Error Injection
		ifile = $fopen(`IN_PIPE_NAME, "r");
		ofile = $fopen(`OUT_PIPE_NAME, "w");
        logfile = $fopen(`LOG_FILE, "w");
		assignments = 1;
		iterations = 0;
		filtered = 0;
        errnum = 0;
	    target_errnum = ${err_num};

		if (ifile == 0)
		begin: error_opening_file
			## TODO: throw an error message? a return code?
			$fwrite(ofile, "{ \"filtered\": 0, \"iterations\": 0, \"err\": \"EOPEN\" }");

			$fclose(ofile);
			$fclose(ifile);

			$finish;  // quit
		end

		// Loop through file
		assignments = $fscanf(ifile, "%h", ${err_name});  // do-loop
		while (assignments > 0)
		begin : inner_read_loop
			advance();
			iterations = iterations + 1;
			if (
% for i, io in enumerate(outputs):
% 	if i != len(outputs)-1:
			${io.name}_correct != ${io.name} ||
%   else:
			${io.name}_correct != ${io.name}
%   endif
% endfor
			)
			begin : incorrect_output
				if (checker(0))
				begin: filtered_output
					// Filtered result
					filtered = filtered + 1;
					assignments = $fscanf(ifile, "%h", ${err_name});
				end else begin
					// Found incorrect and non-filtered result
					errnum = errnum + 1;
		            $fwrite(logfile, "error %d (%d iterations): ", errnum, iterations);
% for i, io in enumerate(outputs):
% 	if i != len(outputs)-1:
		$fwrite(logfile, "\"${io.name}\": %d,", ${io.name});
%   else:
		$fwrite(logfile, "\"${io.name}\": %d", ${io.name});
%   endif
% endfor
		            $fwrite(logfile, "\n");
                    if (errnum >= target_errnum)
                    begin
					    assignments = 0;
                    end else begin
				        assignments = $fscanf(ifile, "%h", ${err_name});
                        iterations = 0;
                    end
				end
			end else begin
				// Logically masked result
				assignments = $fscanf(ifile, "%h", ${err_name});
			end
		end

		// Wrap it up
		$fclose(ifile);

		// Results Return (JSON)
		$fwrite(ofile, "{ \"filtered\": %d, \"iterations\": %d, \"err\": %d, \"correct\": {", filtered, iterations, errnum);
% for i, io in enumerate(outputs):
% 	if i != len(outputs)-1:
		$fwrite(ofile, "\"${io.name}\": %d,", ${io.name}_correct);
%   else:
		$fwrite(ofile, "\"${io.name}\": %d", ${io.name}_correct);
%   endif
% endfor
		$fwrite(ofile, "}, \"incorrect\": {");
% for i, io in enumerate(outputs):
% 	if i != len(outputs)-1:
		$fwrite(ofile, "\"${io.name}\": %d,", ${io.name});
%   else:
		$fwrite(ofile, "\"${io.name}\": %d", ${io.name});
%   endif
% endfor
		$fwrite(ofile, "} }");
		$fclose(ofile);
		$fclose(logfile);
		$fclose(dfile);
		$finish;
	end

endmodule
