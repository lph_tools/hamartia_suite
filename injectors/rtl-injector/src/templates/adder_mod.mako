<%page args="options, inputs, outputs"/>
	checker = !(
% for i, m in enumerate(options.mods):
% 	if i == 0:
		((${options.a} % ${m} + ${options.b} % ${m}) % ${m} == ${options.z} % ${m})
% 	else:
		&& ((${options.a} % ${m} + ${options.b} % ${m}) % ${m} == ${options.z} % ${m})
% 	endif
% endfor
	);
