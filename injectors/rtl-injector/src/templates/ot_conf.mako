read_celllib ${liberty_path}
read_verilog ${rtl_src_path}
read_sdc ${sdc_path}
set_num_threads 1

% for net in non_tg_nets:
remove_net ${net}
% endfor

% for p in pin_info:
report_slack_at -pin ${p.name} -${p.trans}
% endfor
