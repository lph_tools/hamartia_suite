// testbench template without fault injection

`timescale 1ns/1ns
`define IN_PIPE_NAME "${in_pipe_name}"
`define OUT_PIPE_NAME "${out_pipe_name}"
`define LOG_FILE "rtl_inject.log"
`define DEBUG_FILE "debug.log"
`define VCD_FILE "dump.vcd"

module tb;
    integer ofile;            // the file handle
    integer dfile;            // the file handle

% if clk_port:
    // Clock
    reg clk;
% endif

    // Inputs
% for io in inputs[0]:
    reg [${io.width-1}:0] ${io.name};
% endfor

    // Outputs
% for io in outputs:
    wire [${io.width-1}:0] ${io.name};
% endfor

    // Module instance
    ${top_module} dut(
% if clk_port:
        .${clk_port}(clk),
% endif
% for io in inputs[0]:
        .${io.name}(${io.name}),
% endfor
% for i, io in enumerate(outputs):
%     if i != len(outputs)-1:
        .${io.name}(${io.name}),
%   else:
        .${io.name}(${io.name})
% endif
% endfor
    );

% if clk_port:
    task tick;
    begin
        clk = ~clk;
        #1;
        clk = ~clk;
        #1;
    end
    endtask
% endif

    initial begin
        // Initial values
% if clk_port:
        clk = 1'b0;
% endif

% if dump_vcd:
        $dumpfile(`VCD_FILE);
        $dumpvars(1, dut);
% endif

% if debug:
        dfile = $fopen(`DEBUG_FILE, "w");
% endif

% for in_pattern in inputs:

%   for io in in_pattern:
        ${io.name} = ${io.width}'${io.datatype}${io.value};
%   endfor

%   if clk_port:
        tick();
%   else:
        #1;
%   endif 

% endfor 

% if clk_port:
%   for i in range(int(clk_stages)):
        tick();
%   endfor
% endif 

        ofile = $fopen(`OUT_PIPE_NAME, "w");
        // start of JSON output
        $fwrite(ofile, "{");
        // Results Return (JSON)
% for i, io in enumerate(outputs):
%   if i != len(outputs)-1:
        $fwrite(ofile, "\"${io.name}\": %d,", ${io.name});
%   else:
        $fwrite(ofile, "\"${io.name}\": %d", ${io.name});
%   endif
% endfor
        // end of JSON output
        $fwrite(ofile, " }");
        $fclose(ofile);
        $fclose(dfile);
        $finish;
    end

endmodule
