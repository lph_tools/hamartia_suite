#! /usr/bin/env python
"""
A script that performs fault injection to a RTL gate-level circuit.
Random input will be used if input patterns are not provided.
For each input pattern, the iteration taken to generate an unmasked
fault and the error patterns for each output ports are recorded.
"""
# Base imports
import os, sys, argparse, re, random
import csv, json, yaml
import inject, mask_factor
import fileinput

# Filenames
LOG_FILE = "rtl_inject.log"
TB_VO_FILE = "tb.vo"
TB_VPP_FILE = "tb.vpp"

# Output 
CSV_HEADER = ['Iterations', 'Num Outputs', 'Correct', 'Error', 'Diff in Hex']
CSV = 'CSV'
JSON = 'JSON'

# For changing the CWD for the stand-alone tool to the tool root
SRC_FLDR = os.path.dirname(os.path.realpath(__file__))
TOOL_ROOT_FLDR = os.path.abspath(os.path.join(SRC_FLDR, os.pardir))

def parse_checker_args(args):
    """
    Parse checker args

    Args:
        args (list): [0] checker file, [1] checker options

    Returns:
        tuple: [0] parsed checker file, [1] parsed checker options dict
    """

    checker_file = None
    checker_options = None
    if args:
        checker_file = args[0]
        checker_options = args[1]
        if checker_options:
            checker_options = json.loads(checker_options)

    return (checker_file, checker_options)


def parse_config(config_yml):
    config = None
    with open(config_yml, 'r') as yf:
        config = yaml.load(yf)

    dirs = {}
    if 'dirs' in config:
        dirs = config['dirs']
    cell_libs = {}
    if 'cell_libs' in config:
        cell_libs = config['cell_libs']

    module = {}
    if 'module' in config:
        module = config['module']
    # Lib
    lib = module['cell_lib']
    mapping = None
    rtl_dir = '.'
    if lib in cell_libs:
        rtl_dir = cell_libs[lib]['dir']
        mapping = os.path.join(rtl_dir, cell_libs[lib]['mapping'])
        lib = os.path.join(rtl_dir, cell_libs[lib]['src'])
    if 'dir' in module:
        rtl_dir = module['dir']
    if rtl_dir in dirs:
        rtl_dir = dirs[rtl_dir]
    rtl_src = os.path.join(rtl_dir, module['rtl_src'])
    group_mappings = inject.parse_group_mapping(mapping, lib, rtl_dir)

    # Cache
    cache = 'cache'
    if 'cache' in config:
        cache = config['cache']

    # Experiment
    exp = {
        'trials' : 1,
        'errors' : 1
    }
    if 'experiment' in config:
        for k, v in config['experiment'].iteritems():
            exp[k] = v

    # Fault
    fault = {
        'type': "FLIP",
        'num' : 1,
        'targets': []
    }
    if 'fault' in config:
        for fk, fv in config['fault'].iteritems():
            fault[fk] = fv
    return lib, rtl_src, module, group_mappings, cache, exp, fault


def input_gen(module):
    input_ports = {}
    for k, v in module['inputs'].iteritems():
        if 'value' in v:
            input_ports[k] = int(v['value'])
        else:
            assert 'width' in v, "Input port (%r) width is not specified" % k
            bit_width = v['width']
            lower_bound = 0
            upper_bound = (1 << bit_width) - 1
            input_ports[k] = random.randint(lower_bound, upper_bound)

    return input_ports

def record_result(out_file, file_format, num_errors, correct_vals):

    def write_csv(csv_file, correct_vals, err_vals, diffs, iterations):
        num_outputs = len(correct_vals)
        num_errors = len(iterations)
        for i in range(num_errors):
            row = []
            row.append(iterations[i])
            row.append(num_outputs)
            for j in range(num_outputs):
                row.append(correct_vals[j])
            for j in range(num_outputs):
                row.append(err_vals[i][j])
            for j in range(num_outputs):
                row.append(diffs[i][j])
            csv_file.writerow(row)

    def write_json(json_file, correct_vals, err_vals, diffs, iterations):
        results = {}
        results['correct_outputs'] = correct_vals
        results['errors'] = []
        num_errors = len(iterations)
        for i in range(num_errors):
            err_dict = {}
            err_dict['iterations'] = int(iterations[i])
            err_dict['errorneous_outputs'] = err_vals[i]
            err_dict['diff'] = diffs[i]
            results['errors'].append(err_dict)    
        json.dump(results, json_file, indent=4)

    def get_write_method(format):
        my_dict = {
            CSV: write_csv,
            JSON: write_json,
        }
        return my_dict.get(format, None)


    # Regex for parsing iterations
    error_info_str = 'error\s+\d+\s+\(\s+(\d+)\s+iterations\)'
    regex = re.compile(error_info_str)

    # Process Error Patterns
    err_vals = []
    diffs = []
    iterations = []
    log_f_path = os.path.join('.', LOG_FILE)
    with open(log_f_path) as log_f:
        for line in log_f:
            error_info = line.split(':',1)[0] # error {error_id} ( {num_iterations} iterations ):)
            num_iteration = regex.search(error_info).group(1)
            iterations.append(int(num_iteration))
            
            output_part = line.split(':',1)[1]
            outputs = output_part.split(',')

            per_error_err_vals = []
            per_error_diffs = []
            for i, val in enumerate(outputs):
                err_val = int(outputs[i].split(':')[1].strip())
                per_error_err_vals.append(err_val)
                diff = format(err_val ^ correct_vals[i], 'x') # bitwise difference in hex
                per_error_diffs.append(diff)
            
            err_vals.append(per_error_err_vals)
            diffs.append(per_error_diffs)

    wm = get_write_method(file_format)
    assert wm, "Error getting output file write method"
    wm(out_file, correct_vals, err_vals, diffs, iterations)
    
def cleanup(debug=0):
    '''
        remove intermediate files
    '''
    if debug:
        return 
    log_f_path = os.path.join('.', LOG_FILE)
    tbvo_f_path = os.path.join('.', TB_VO_FILE)
    tbvpp_f_path = os.path.join('.', TB_VPP_FILE)
    try:
        os.remove(log_f_path)
        os.remove(tbvo_f_path)
        os.remove(tbvpp_f_path)
    except Exception as e:
        print str(e)
        sys.exit(1)

def main():
    parser = argparse.ArgumentParser(description="Inject errors to RTL to collect error patterns and compute masking factor")
    parser.add_argument('-c', '--config', required=True, help="Required config file for injection experiments")
    parser.add_argument('-o', '--out_file', default='stand_alone_out.csv', help="Name of output file. Must end with .csv or .json (case-sensitive)")
    parser.add_argument('-i', '--in_csv', default=None, help="Path of input pattern file")
    parser.add_argument('-e', '--checker', nargs=2, metavar=("FILE", "OPTIONS"), help="Filter checker file (mako template) and JSON options")
    parser.add_argument('-p', '--in_parallel', default=0, help="Running in parallel or not")
    parser.add_argument('-d', '--debug', default=None, help="Set to 1 to keep intermediate files")
    parser.add_argument('-v', '--verbose', action='store_true', help="Display debugging output")

    options = parser.parse_args()
    
    # if running in parallel, CWD should not be shared and output file path should be absolute
    # user is responsible for ensuring paths in all config files are absolute or independent
    if options.in_parallel:
        # check absolute paths
        assert os.path.isabs(options.out_file), "Output file should be absolute to run in parallel!" 
    else:
        # update all command-line paths to be absolute
        def make_abs(this_path):
            """ Update the path to be absolute. """
            return os.path.abspath(this_path) if this_path is not None else None

        assert options.out_file.endswith('.csv') or options.out_file.endswith('.json'), "Invalid output file format!"

        options.config = make_abs(options.config)
        options.out_file = make_abs(options.out_file)
        options.in_csv = make_abs(options.in_csv)
        options.checker = make_abs(options.checker)

        # change the CWD to the tool root folder, to allow relative paths
        # in all config files
        os.chdir(TOOL_ROOT_FLDR)
    
    lib, rtl_src, module, group_mappings, cache, exp, fault = parse_config(options.config)
    # Make all paths absolute
    # This allows the caches generated by the stand-alone mode to share with other modes
    lib = make_abs(lib)
    rtl_src = make_abs(rtl_src)
    cache = make_abs(cache)

    # Parse checker
    (checker_file, checker_options) = parse_checker_args(options.checker)
    # Invariant ports
    output_ports = {k : 0 for k, v in module['outputs'].iteritems()}
    clk_port = module['clk'] if 'clk' in module else None
    clk_stages = module['stages'] if 'stages' in module else 0

    with open(options.out_file, "wb") as raw_out_f:
        out_f_format = None
        out_f = None
        if options.out_file.endswith('.csv'):
            out_f_format = CSV
            out_f = csv.writer(raw_out_f, dialect='excel')
            out_f.writerow(CSV_HEADER)
        elif options.out_file.endswith('.json'):
            out_f_format = JSON
            out_f = raw_out_f
        assert out_f, "Error opening output file"

        no_input = not options.in_csv
        if no_input:
            trials = exp['trials']
            for ti in xrange(trials):
                # Generate random input patterns
                input_ports = input_gen(module)
                # First run to generate synthesized file
                results = inject.run(lib, rtl_src, module['top_module'], fault['type'], fault['num'], fault['targets'], [], input_ports, output_ports, clk_port, clk_stages, checker_file, checker_options, group_mappings, cache=cache)
                print results
                # Redo injection until specifed # of target errors are injected
                result = mask_factor.reinject('.', exp['errors'], fault['num'])
                # Obtain golden output value for comparison
                correct_vals = []
                for k, v in output_ports.iteritems():
                    correct_vals.append(int(result['correct'][k]))

                # Record outcome
                record_result(out_f, out_f_format, exp['errors'], correct_vals)
        else:
            # Assume header contains port names exactly same as config
            with open(options.in_csv, "rb") as in_f:
                doc = csv.reader(in_f)
                port_names = []
                for header_row in doc:
                    for n in header_row:
                        port_names.append(n)
                    break
                # Each data row corresponds to a trial
                for data_row in doc:
                    input_ports = {}
                    for k, v in module['inputs'].iteritems():
                        if 'value' in v:
                            input_ports[k] = int(v['value'])
                        else:
                            input_ports[k] = int(data_row[port_names.index(k)])

                    # First run to generate synthesized file
                    results = inject.run(lib, rtl_src, module['top_module'], fault['type'], fault['num'], fault['targets'], [], input_ports, output_ports, clk_port, clk_stages, None, None, group_mappings, cache=cache, debug=options.debug)
                    # Redo injection until specifed # of target errors are injected
                    result = mask_factor.reinject('.', exp['errors'], fault['num'])
                    # Obtain golden output value for comparison
                    correct_vals = []
                    for k, v in output_ports.iteritems():
                        correct_vals.append(int(result['correct'][k]))

                    # Record outcome
                    record_result(out_f, out_f_format, exp['errors'], correct_vals)
        # Remove intermediate files
        cleanup(options.debug)


if __name__ == '__main__':
    main()
