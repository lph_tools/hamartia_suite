#! /usr/bin/env python
"""
  Verify circuit functionality without fault injection
"""
# Base imports
from __future__ import print_function
import os, sys, subprocess, argparse, yaml, json, csv, tempfile
import verilog_parser
from inject import parse_modules

# Templating
from mako_helper import to_mako
from mako.template import Template
from mako.lookup import TemplateLookup

from cacheable import Cacheable
from vcd_parser import VCDParser

# Paths
SCRIPT_DIR = os.path.dirname(os.path.realpath(__file__))
TOOL_ROOT_DIR = os.path.abspath(os.path.join(SCRIPT_DIR, os.pardir))
IVERILOG = "iverilog"
TB_TEMPLATE = "templates/tb_no_inj.mako"

# Filenames
TB_V, TB_VO, TB_VPP = ("tb.v", "tb.vo", "tb.vpp")
IN_PIPE_NAME = "in_pipe.txt"
OUT_PIPE_NAME = "out_pipe.txt"
OUT_VCD = "dump.vcd"

def parse_config(config_yml):
    config = None
    with open(config_yml, 'r') as yf:
        config = yaml.load(yf)

    dirs = {}
    if 'dirs' in config:
        dirs = config['dirs']
    cell_libs = {}
    if 'cell_libs' in config:
        cell_libs = config['cell_libs']

    module = {}
    if 'module' in config:
        module = config['module']
    # Lib
    lib = module['cell_lib']
    rtl_dir = '.'
    if lib in cell_libs:
        rtl_dir = cell_libs[lib]['dir']
        lib = os.path.join(rtl_dir, cell_libs[lib]['src'])
    if 'dir' in module:
        rtl_dir = module['dir']
    if rtl_dir in dirs:
        rtl_dir = dirs[rtl_dir]
    rtl_src = os.path.join(rtl_dir, module['rtl_src'])

    dump_vcd = False
    if 'dump_vcd' in config and config['dump_vcd']:
        dump_vcd = True
    
    verilog_defs = []
    if 'verilog_defs' in config and config['verilog_defs']:
        verilog_defs = config['verilog_defs']
        if not isinstance(verilog_defs, list):
            verilog_defs = [verilog_defs]

    cache = config['cache'] if 'cache' in config else None
    return lib, rtl_src, module, dump_vcd, verilog_defs, cache

def parse_csv(csv_file, module):
    with open(csv_file, "rb") as in_f:
        # Assume header contains port names exactly same as config
        doc = csv.reader(in_f)
        assert doc, "Error opening input file"
        port_names = []
        for header_row in doc:
            for n in header_row:
                port_names.append(n)
            break
        # Each data row corresponds to input of each cycle
        inputs = []
        for data_row in doc:
            input_ports = {}
            for k, v in module['inputs'].iteritems():
                if 'value' in v:
                    input_ports[k] = int(v['value'])
                else:
                    input_ports[k] = int(data_row[port_names.index(k)])
            inputs.append(input_ports)        
        return inputs

def get_module_io(rtl_src, top, cache=None):
    if cache: 
        cache_src = os.path.splitext(os.path.basename(rtl_src))[0]
        Cacheable.setupPath(cache, cache_src)

    vobj = Cacheable("init_ast", value_deps=[top, rtl_src])
    module_io = Cacheable("module_io", value_deps=[top], cache_deps=[vobj])

    if not module_io.isValid():
        if not vobj.isValid():
            print("Parse module...", file=sys.stderr)
            vobj.set(verilog_parser.parse([rtl_src], table_output_dir=SCRIPT_DIR))
        else:
            print("Retrieved module from cache...", file=sys.stderr)

        parse_modules(vobj, module_io)
        vobj.end() # save init_ast to file system
    else:
        print("Retrieved module io mapping from cache...", file=sys.stderr)
    
    return module_io

def generate_testbench(env, top_module, inputs, outputs, module_io_mapping, clk_port, clk_stages, dump_vcd=False, debug=False):
    """
    Generate the testbench file
    """
    # Open template
    with open(os.path.join(SCRIPT_DIR, TB_TEMPLATE), 'r') as tbf:
        lookup_paths = TemplateLookup(directories=[".", SCRIPT_DIR])
        tb_template = Template(tbf.read(), lookup=lookup_paths)
        # Template data
        data = { 'top_module': top_module, 'inputs': [], 'outputs': []}

        # Format inputs (data from input file)
        for in_pat in inputs: # iterate through input patterns
            in_pat_data = []
            for port, value in in_pat.iteritems():
                io_data = {}
                io_data['name'] = port
                io_data['width'] = module_io_mapping[port][1]

                # Datatypes
                if str(value).startswith('0b'):
                    io_data['datatype'] = 'b';
                    io_data['value'] = value[2:] # strip '0b'
                elif str(value).startswith('0x'):
                    io_data['datatype'] = 'h';
                    io_data['value'] = value[2:] # strip '0x'
                else:
                    io_data['datatype'] = 'd';
                    io_data['value'] = value 

                in_pat_data.append(io_data)

            data['inputs'].append(in_pat_data)

        # Format outputs
        for port, value in outputs.iteritems():
            io_data = {}
            io_data['name'] = port
            io_data['width'] = module_io_mapping[port][1]

            data['outputs'].append(io_data)

        # Render template
        out = tb_template.render(in_pipe_name=IN_PIPE_NAME,
                                 out_pipe_name=OUT_PIPE_NAME,
                                 clk_port=clk_port,
                                 clk_stages=clk_stages,
                                 top_module=data['top_module'],
                                 inputs=to_mako(data['inputs']),
                                 outputs=to_mako(data['outputs']),
                                 dump_vcd=dump_vcd,
                                 debug=debug)

        # Write testbench
        with open(os.path.join(env, TB_V), 'w') as of:
            of.write(out)

def simulate(env, cell_library_path, rtl_src, verilog_defs=[]):
    """
    Compile and Simulate verilog testbench

    The generated testbench and RTL module are synthesized and compiled. Pipes are created for
    output to the testbench. The results are then returned to the script in a JSON format.

    Args:
        env                 (str): path of environment (to copy files)
        cell_library_path   (str): path to cell library
        rtl_src             (str): path to rtl source code
        verilog_defs       (list): verilog defines (-D<XXX>)

    Returns:
        dict: parsed JSON output from the testbench
    """

    # Combine
    arg_comb = [IVERILOG, "-E"]
    arg_comb.extend(["-D{}".format(d) for d in verilog_defs])
    arg_comb.append("-o")
    arg_comb.append(TB_VO)
    arg_comb.append(TB_V)
    arg_comb.append(os.path.abspath(rtl_src))
    arg_comb.append(os.path.abspath(cell_library_path))

    #print(" ".join(arg_comb))
    comb = subprocess.Popen(arg_comb, stdout=subprocess.PIPE, stderr=subprocess.PIPE, cwd=env)
    (out, err) = comb.communicate()

    # Compile
    arg_comp = [IVERILOG, "-o", TB_VPP, "-s", "tb", TB_VO]
    #print(" ".join(arg_comp))
    comp = subprocess.Popen(arg_comp, stdout=subprocess.PIPE, stderr=subprocess.PIPE, cwd=env)
    (out, err) = comp.communicate()

    # Simulate
    sim = subprocess.Popen(["./"+TB_VPP], stdout=subprocess.PIPE, stderr=subprocess.PIPE, cwd=env)

    # Get results
    (out, err) = sim.communicate()
    out_pipe_name = os.path.join(env, OUT_PIPE_NAME)
    opipe = os.open(out_pipe_name, os.O_RDONLY)

    # Read from pipe
    result = ""
    rd = os.read(opipe, 1024)
    while rd:
        result += rd
        rd = os.read(opipe, 1024)
    os.close(opipe)
    os.remove(out_pipe_name)

    # Parse JSON results and return
    json_data = None
    try:
        json_data = json.loads(result)
    except ValueError as e:
        print(result)
        raise Exception(str(e))
    return json_data

def get_vcd(env):
    vcd_file = os.path.join(env, OUT_VCD)
    if os.path.exists(vcd_file):
        return vcd_file
    else:
        return None

def parse_vcd(vcd_file, module):
    capture_time = (0, 1) # for combination logic (i.e., no FF)
                          # time 0/ time 1: ouptut ready for 1st/2nd pair, respectively
    if 'output_toggle_time' in module and module['output_toggle_time']:
        capture_time = tuple(module['output_toggle_time'])
    vcd_parser = VCDParser(capture_time[0], capture_time[1])
    vcd_parser.parse(vcd_file)
    tg_nets, transitions = vcd_parser.get_toggled_nets()
    tg_nets = [n[7:] for n in tg_nets] # strip tb.dut.
    #tg_nets = [n for n in tg_nets if ":" not in n] # disgard nets within standard cells
    all_wires = vcd_parser.get_wires_of('dut')
    non_tg_nets = list(set(all_wires) - set(tg_nets))

    return tg_nets, transitions, non_tg_nets
 
def create_env(debug=False):
    if debug:
        return './'
    tmp = tempfile.mkdtemp()
    return tmp

def cleanup(env, debug=False):
    to_remove = (TB_VO, TB_VPP) if debug else (TB_V, TB_VO, TB_VPP, OUT_VCD)
    for fn in to_remove:
        f = os.path.join(env, fn)
        if os.path.exists(f):
            os.remove(f)

def main():
    parser = argparse.ArgumentParser(description="Circuit verification script")
    parser.add_argument('-c', '--config', required=True, help="Required config file for injection experiments")
    parser.add_argument('-i', '--in_csv', default=None, help="Path of input pattern file")
    parser.add_argument('-o', '--out_file', default='verify.json', help="Name of output JSON file")
    parser.add_argument('-d', '--debug', default=False, help="Set to 1 to keep intermediate files")
    options = parser.parse_args()

    # change the CWD to the tool root folder, to allow relative paths
    # in all config files
    os.chdir(TOOL_ROOT_DIR)

    env = create_env(options.debug)
    lib, rtl_src, module, dump_vcd, verilog_defs, cache = parse_config(options.config)
    # Invariant ports
    output_ports = {k : 0 for k, v in module['outputs'].iteritems()}
    clk_port = module['clk'] if 'clk' in module else None
    clk_stages = module['stages'] if 'stages' in module else 0

    inputs = parse_csv(options.in_csv, module)
    with open(options.out_file, "wb") as out_f:
        assert out_f, "Error opening output file"

        top = module['top_module']
        module_io = get_module_io(rtl_src, top, cache=cache)
       
        generate_testbench(env, top, inputs, output_ports, module_io[top]['io'], \
                           clk_port, clk_stages, dump_vcd)

        module_io.end() # save module_io to file system

        results = {}
        output_vals = simulate(env, lib, rtl_src, verilog_defs)
        results['outputs'] = output_vals

        vcd_file = get_vcd(env)
        if vcd_file:
            tg_nets, transitions, non_tg_nets = parse_vcd(vcd_file, module)
            results['toggled_nets'] = tg_nets
            results['toggled_dirs'] = transitions
            results['nontoggled_nets'] = non_tg_nets

        json.dump(results, out_f, indent=4)

    # Remove intermediate files
    cleanup(env, options.debug)
                

if __name__ == '__main__':
    main()
