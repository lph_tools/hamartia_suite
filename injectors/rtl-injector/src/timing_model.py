# Libs
from __future__ import print_function
import os, sys, yaml, time, shutil
from collections import defaultdict
from random import randint

# Internal modules
from timing_config import parse_config
from verify import create_env, get_module_io, generate_testbench, simulate, get_vcd, parse_vcd
from timing_inject import timing_analysis, get_fault_masks, inject_output

# Hamartia API
if 'HAMARTIA_DIR' in os.environ:
    sys.path.append(os.path.join(os.environ['HAMARTIA_DIR'], 'src/python'))
import hamartia_api

class RTLErrorModel(hamartia_api.error_model.ErrorModel):
    """
    Error Model Front-end for RTL Injector (model)
    """
    def __init__(self, name):
        """
        Error-model constructor

        Args:
            name (str): error model name
        """
        super(RTLErrorModel, self).__init__(name)
        self.mask_gen = None

    def _find_match(self, cxt, config):
        if not 'mappings' in config:
            raise Exception("Instruction to module mapping needed!")
        if not 'modules' in config:
            raise Exception("RTL module settings needed!")

        inst_match = {
                'width' : cxt.instruction_data.width,
                'operation' : hamartia_api.interface.Operation_Name(cxt.instruction_data.instruction.operation),
                'datatype' : hamartia_api.interface.DataType_Name(cxt.instruction_data.instruction.data),
                'packing' : hamartia_api.interface.Packing_Name(cxt.instruction_data.instruction.packing)
        }

        # Find matching RTL module for instruction
        for name, module in config['mappings'].iteritems():
            match = False
            for inst in module['instructions']:
                match = True
                for k, v in inst_match.iteritems():
                    if k in inst and inst[k] != v:
                        match = False
                        break
                if match:
                    break

            if match:
                modules = [config['modules'][sc] for sc in module['sub_circuits']]
                return (name, modules)

        return (None, None)

    def _get_liberty(self, fault, corners):
        liberty = corners[fault['type']][fault['index']]
        print ("Use liberty file: " + liberty, file=sys.stderr)
        return liberty

    def _get_inject_mask(self, fault, num_stages, reset=False):
        def scw_gen(ns):
            m = [0] * ns
            x = ns - 1
            while True:
                m[x] = 1
                yield m
                x = (x - 1) % ns 
                m = [0] * ns

        def mc_gen(num_stages, duration):
            def int_to_mask(x, num_bits):
                # zfill to num_bits
                bin_format = '0{}b'.format(num_bits)
                s = format(x, bin_format)
                # convert binary str to list of 0's and 1's
                # e.g., '0110' to [0,1,1,0]
                ASCII_0 = 48
                return [ord(c) - ASCII_0 for c in list(s)]
            m_int = 1
            d = duration
            while True:
                mask = int_to_mask(m_int, num_stages)
                yield mask
                d -= 1
                m_int = (m_int << 1) | (d > 0)
                m_int &= (1 << num_stages) - 1 

        duration = fault['duration'].lower()
        if duration == 'max':
            # Best case for multi-cycle variations
            return [1]*num_stages
        elif duration == 'random':
            # Best case for single-cycle variations
            mask = [0]*num_stages
            fault_stage = randint(0, num_stages-1)
            mask[fault_stage] = 1
            return mask
        elif duration == 'single_cycle_worst':
            # Worst case for single-cycle variations
            if num_stages != 1:
                self.mask_gen = scw_gen(num_stages) if reset else self.mask_gen
                return next(self.mask_gen)
            else: # (corner case) for single-stage circuit, we inject only upon reset
                return [1] if reset else [0]
        elif duration.startswith('multi_cycle'):
            # multi-cycle variations
            dur_in_cycles = int(duration.split('#')[1])
            if dur_in_cycles < 1: 
                raise Exception("Duration should be a positive number: {}".format(dur_in_cycles))
            if num_stages != 1:
                self.mask_gen = mc_gen(num_stages, dur_in_cycles) if reset else self.mask_gen
                return next(self.mask_gen)
            else: # (corner case) for single-stage circuit, we inject only upon reset
                return [1] if reset else [0]
        else:
            raise Exception("Unimplemented fault duration type: {}".format(duration))

    def _get_input_pair(self, cxt, module):

        def _get_val(cxt, asm_idx, lane, prev=False):
            if prev:
                return cxt.prev_instruction_data.inputs[asm_idx].value[lane].uint()
            else:
                return cxt.instruction_data.inputs[asm_idx].value[lane].uint()

        num_lanes = min(cxt.prev_instruction_data.simd_width, cxt.instruction_data.simd_width)
        prev_inputs, cur_inputs = [], []

        for lane in range(num_lanes):
            prev_input_ports, input_ports = {}, {}

            for k, v in module['inputs'].iteritems():
                if 'asm_idx' in v:
                    if type(v['asm_idx']) is list:
                        shf = int(v['width'])/len(v['asm_idx'])
                        prev_input_ports[k] = input_ports[k] = 0
                        if cxt is not None:
                            for i, asm_idx in enumerate(v['asm_idx']):
                                prev_input_ports[k] |= (_get_val(cxt, asm_idx, lane, True) << (i * shf))
                                input_ports[k] |= (_get_val(cxt, asm_idx, lane, False) << (i * shf))
                    else:
                        if cxt is not None:
                            prev_input_ports[k] = _get_val(cxt, v['asm_idx'], lane, True)
                            input_ports[k] = _get_val(cxt, v['asm_idx'], lane, False)
                        else:
                            prev_input_ports[k], input_ports[k] = 0, 0
                elif 'value' in v:
                    prev_input_ports[k], input_ports[k] = int(v['value']), int(v['value'])

            prev_inputs.append(prev_input_ports)
            cur_inputs.append(input_ports)

        return (prev_inputs, cur_inputs)

    def _get_toggled_output_pins(self, tg_nets, transitions, output_ports):
        def is_output(net, output_ports):
            result = False
            for o in output_ports:
                result |= net.startswith(o)
            return result

        pin_names, pin_trans = [], []
        for i, n in enumerate(tg_nets):
            if is_output(n, output_ports):
                pin_names.append(n)
                pin_trans.append(transitions[i])

        return (pin_names, pin_trans)

    def _get_prev_out_vals(self, output_vals, toggled_pins):
        """
            Get output value of the previous instruction from current instruction

            Args:
                output_vals     (dict): output port names to values of current instruction
                toggled_pins    (list[str]): a list of output pin names

            Returns:
                prev_vals   (dict): output port names to values of previous instruction
        """
        toggled_masks = get_fault_masks(output_vals.keys(), toggled_pins)
        prev_vals = {}
        for name, val in output_vals.iteritems():
            prev_vals[name] = val ^ toggled_masks[name] 

        return prev_vals

    def _run_sim(self, cxt, config, modules, run=True):
        lib, rtl_src_list, corners, verilog_defs, timer_path, fault, cache = parse_config(config, modules)
        dump_vcd = True
        liberty = self._get_liberty(fault, corners)
        reset = True if cxt.trigger.cont else False
        inj_mask = self._get_inject_mask(fault, len(modules), reset)
        print ("Injection mask: {}".format(inj_mask), file=sys.stderr)
        cycle_time = fault['cycle_time']
        env = create_env()

        prev_inputs, cur_inputs = self._get_input_pair(cxt, modules[0])
        print ("Prev input: {}".format(prev_inputs), file=sys.stderr)
        print ("Cur input: {}".format(cur_inputs), file=sys.stderr)
        num_lanes = len(cur_inputs)
        results = []
        timer = defaultdict(int)
        info = {}
        info['liberty'], info['inj_mask'], info['cycle_time'] = liberty, inj_mask, cycle_time

        for lane_id in range(num_lanes):
            input_ports = [prev_inputs[lane_id], cur_inputs[lane_id]]
            for m_id, module in enumerate(modules): 
                # setup
                output_ports = {k : 0 for k, v in module['outputs'].iteritems()}
                rtl_src = rtl_src_list[m_id]
                top = module['top_module']

                start = time.time()
                module_io = get_module_io(rtl_src, top, cache=cache)
                end = time.time()
                timer['parse_module_io'] += (end - start)

                clk_port = module['clk'] if 'clk' in module else None
                clk_stages = module['stages'] if 'stages' in module else 0

                generate_testbench(env, top, input_ports, output_ports, module_io[top]['io'], \
                                   clk_port, clk_stages, dump_vcd)

                module_io.end() # save module_io to file system
                    
                # verilog simulation
                start = time.time()
                output_vals = simulate(env, lib, rtl_src, verilog_defs)
                end = time.time()
                timer['simulation'] += (end - start)

                # parse value change dump
                start = time.time()
                vcd_file = get_vcd(env)
                if not vcd_file:
                    raise Exception("vcd was not generated!")
                tg_nets, transitions, non_tg_nets = parse_vcd(vcd_file, module)
                
                # get toggled output pin names and transitions
                tg_o_names, tg_o_trans = self._get_toggled_output_pins(tg_nets, transitions, output_ports)
                end = time.time()
                timer['parse_vcd'] += (end - start)
            
                if inj_mask[m_id]:
                    start = time.time()
                    slacks = timing_analysis(env, timer_path, liberty, rtl_src, \
                                             cycle_time, clk_port, non_tg_nets, tg_o_names, tg_o_trans)

                    end = time.time()
                    timer['timing_analysis'] += (end - start)

                    if slacks:
                        err_pins = [p for p, s in slacks.iteritems() if s < 0]
                        fm = get_fault_masks(output_ports.keys(), err_pins)

                        new_output_vals = inject_output(output_vals, fm)
                    else:
                        new_output_vals = output_vals
                else:
                    new_output_vals = output_vals

                # prepare input pair for next stage (if any)
                prev_output_vals = self._get_prev_out_vals(output_vals, tg_o_names)
                input_ports = [prev_output_vals, new_output_vals]

            results.append(new_output_vals)
        
        # remove env
        shutil.rmtree(env)

        timer['total'] = sum(timer.values())
        info['timer'] = timer
        return results, info


    def Startup(self, config):
        for name, module in config['modules'].iteritems():
            print ("Caching" + name + "...")
            self._run_sim(None, config, name, module, False)


    # Injection of error
    def InjectError(self, cxt, config, log = None): 
        """
        Inject error (defaults to no error)
        
        Args:
            cxt    (object): The context before error injection
            config (object): YAML config file for configuring the model
            log    (object): Log for adding extra logging information
        
        Returns:
            bool: Successful
        """
        
        if not config:
            raise Exception("RTL config file needed!")

        # Find match
        (mod_name, modules) = self._find_match(cxt, config)
        
        if mod_name is not None:
            # Run simulation
            results, info = self._run_sim(cxt, config, modules)
            print ("Simulation done for {}".format(mod_name), file=sys.stderr)
            num_lanes = len(results)
            
            # Update outputs
            module = modules[-1]
            for lane in range(num_lanes):
                for k, v in results[lane].iteritems():
                    width = module['outputs'][k]['width'] if 'width' in module['outputs'][k] else 1
                    idxs = module['outputs'][k]['asm_idx']
                    if type(idxs) is not list:
                        idxs = [idxs]
                    width = width / len(idxs)
                    for i, idx in enumerate(idxs):
                        cxt.setOutput(hamartia_api.DataValue((v >> (width * i)) & ((1 << width)-1)), \
                                      idx, lane)
            
            # Populate log
            log.ModelAddLogString('timing_rtl', 'module', mod_name)
            for lane in range(num_lanes):
                for k, v in results[lane].iteritems():
                    log.ModelAddLogString('timing_rtl', 'SIMD_lane_{}_{}'.format(lane, k), str(bin(v)))

            timer = info.pop('timer', None)
            if timer:
                for k, v in timer.iteritems():
                    log.ModelAddLogString('model_overhead', str(k), str(v))
            for k, v in info.iteritems():
                log.ModelAddLogString('info', str(k), str(v))
             
            # Update RFLAGS
            return True

        # No matching RTL module found
        hamartia_api.debug.FatalError("No inst/module mapping!")
        return False

    # Output log/stats
    def SaveOutput(self, output_file): 
        """
        Save the logging output

        Returns:
            bool: Success
        """
        pass


class RTLErrorModelLibrary(hamartia_api.error_model.ErrorModelLibrary):
    """
    Basic error models
    """
    
    def __init__(self):
        """
        Initialize basic error model library, register all error models
        """

        # Init
        super(RTLErrorModelLibrary, self).__init__()

        # RTL Error Model
        rtlm = RTLErrorModel('TIMING_RTL')
        self.AddModel(rtlm)
        rtlm.__disown__()
        super(RTLErrorModel, rtlm).__disown__()


    def GetName(self):
        """
        Get name of library

        Returns:
            str: Name of library
        """

        return "rtl_error_models"


# Register library
register = RTLErrorModelLibrary

if __name__ == '__main__':
    if len(sys.argv) <= 1:
        raise Exception("Config files required!")

    rtl = RTLErrorModel('rtl')
    for config in sys.argv[1:]:
        config_yml = None
        with open(config, 'r') as yf:
            config_yml = yaml.load(yf)
        rtl.Startup(config_yml)
