import re, os
from collections import defaultdict
import pyverilog.vparser.ast as ast
from pyverilog.ast_code_generator.codegen import ASTCodeGenerator
import verilog_parser as parser

class LibManager(object):
    """ 
        Parse IO of cells 

        Format: {'module_name': {'input': [], 'output': []} }
    """
    def __init__(self, lib_f):
        self.cur_module = None
        self.cell_io = defaultdict(dict)
        self.parse_cell_io(lib_f)

    def parse_cell_io(self, lib_f):
        with open(lib_f) as f:
            for l in f:
                if l.startswith("module"):
                    self.parse_module_line(l)
                elif l.startswith("output"):
                    self.parse_io_line(l, "output")
                elif l.startswith("input"):
                    self.parse_io_line(l, "input")
                elif l.startswith("endmodule"):
                    self.cur_module = None

    def parse_module_line(self, l):
        module_name = l.split()[1]
        self.cur_module = self.cell_io[module_name]
        self.cur_module["input"] = []
        self.cur_module["output"] = []

    def parse_io_line(self, l, keyword):
        if self.cur_module:
            line = l.replace(keyword, "").replace(";", "")
            ports = line.split(",")
            self.cur_module[keyword] += [p.strip() for p in ports]
   
class InstanceManager(object):
    def __init__(self, lib_file):
        self.lib_manager = LibManager(lib_file)

    def has_output_to(self, inst, nets):
        my_out = set(self.get_output_of(inst))
        return not my_out.isdisjoint(nets) 
        
    def has_input_to(self, inst, nets):
        my_in = set(self.get_input_of(inst))
        return not my_in.isdisjoint(nets) 

    def get_output_of(self, inst):
        portlist = inst.instances[0].portlist
        module_out = self.lib_manager.cell_io[inst.module]["output"]
        return [InstanceManager.parg2net(p.argname) for p in portlist if p.portname in module_out]

    def get_input_of(self, inst):
        portlist = inst.instances[0].portlist
        module_in = self.lib_manager.cell_io[inst.module]["input"]
        return [InstanceManager.parg2net(p.argname) for p in portlist if p.portname in module_in and not isinstance(p.argname, ast.IntConst)]

    @staticmethod
    def rename_ports_of(inst, func):
        portlist = inst.instances[0].portlist
        for p in portlist:
            if isinstance(p.argname, ast.IntConst):
                continue
            new_argname = InstanceManager.set_parg_name(p.argname, func(InstanceManager.parg2net(p.argname)))
            if new_argname:
                p.argname = new_argname

    @staticmethod
    def adjust_ports_of(inst, func):
        portlist = inst.instances[0].portlist
        for p in portlist:
            if isinstance(p.argname, ast.IntConst):
                continue
            if isinstance(p.argname, ast.Pointer):
                offset = func(p.argname.var.name)
                if offset:
                    p.argname.ptr = ast.IntConst(str(int(p.argname.ptr.value) - offset))
            if isinstance(p.argname, ast.Partselect):
                offset = func(p.argname.var.name)
                if offset:
                    p.argname.msb.value = ast.IntConst(str(int(p.argname.msb.value) - offset))
                    p.argname.lsb.value = ast.IntConst(str(int(p.argname.msb.value) - offset))

    @staticmethod
    def get_inst_ports(inst):
        portlist = inst.instances[0].portlist
        return [InstanceManager.parg2net(p.argname) for p in portlist if not isinstance(p.argname, ast.IntConst)]

    @staticmethod
    def has_connection_to(inst, nets):
        my_nets = set(InstanceManager.get_inst_ports(inst))
        return not my_nets.isdisjoint(nets) 

    @staticmethod
    def parg2net(parg):
        """Convert PortArg to its name."""
        if isinstance(parg, ast.Pointer) or isinstance(parg, ast.Partselect):
            return parg.var.name
        elif isinstance(parg, ast.Identifier):
            return parg.name
        elif isinstance(parg, ast.IntConst):
            return None
        else:
            assert 0, "parg2net error: {}".format(parg.name)

    @staticmethod
    def set_parg_name(parg, name):
        if isinstance(name, tuple): # name is a Pointer 
            if isinstance(parg, ast.Pointer):
                parg.var.name = name[0]
                parg.ptr = ast.IntConst(name[1])
            elif isinstance(parg, ast.Identifier): # change identifier to pointer
                return ast.Pointer(ast.Identifier(name[0]), ast.IntConst(name[1])) 
            else:
                assert 0, "parg2net error: {}".format(parg.name)
        else:
            if isinstance(parg, ast.Pointer) or isinstance(parg, ast.Partselect):
                parg.var.name = name
            elif isinstance(parg, ast.Identifier):
                parg.name = name
            else:
                assert 0, "parg2net error: {}".format(parg.name)

class ModuleDefManager(object):   
    def __init__(self, design_file, lib_file, pipe_prefix, simplify=True):
        self.num_stages = 0
        self.inst_manager = InstanceManager(lib_file)
        self.pipe_prefix = pipe_prefix
        self.module_defs = parser.parse([design_file]).description.definitions
        self.md_dict = self._build_md_dict(simplify)
        self.top_instances = self._inst_lists_of(self.md_dict['top'], exclude=self.pipe_prefix)
        self.top_wires = self.decls_of(self.md_dict['top'], "wire")
        self.pipe_instances = self._pipe_inst_lists_of(self.md_dict['top'])

    def top_md(self):
        """Return the definition of the top module."""
        return self.md_dict['top']

    def nxt_md(self, cur_stage):
        """
            Return the module definition of next stage and a string describing the module.
            If next stage exists, the string is the stage id;
            otherwise, return the top module's definition and 'top'
        """
        nxt_stage = str(cur_stage + 1) if cur_stage + 1 <= self.num_stages else 'top'
        return self.md_dict[nxt_stage], nxt_stage

    def ports_with_name_in(self, md, names):
        """Return ports with their name in names."""
        return [p for p in md.portlist.ports if p.name in names]

    def decls_of(self, md, type):
        """Return decls of a particular type."""
        DIR = {"input": ast.Input, "output": ast.Output, "wire": ast.Wire}
        dir = DIR.get(type, None)
        return [e for e in self._decls_of(md) if isinstance(e, dir)]

    def pop_instances(self, nets):
        """
            Return instances with output in nets.

            Search in a BFS manner.
            Side-effect: top_instances are modified.
        """
        match_bucket, nonmatch_bucket = [], []
        nxt_nets = []
        for i in self.top_instances:
            if self.inst_manager.has_output_to(i, nets): 
                match_bucket.append(i)
                nxt_nets.extend(self.inst_manager.get_input_of(i))
            else:
                nonmatch_bucket.append(i)
        if match_bucket:
            self.top_instances = nonmatch_bucket
            match_bucket += self.pop_instances(nxt_nets)
            return match_bucket
        else: # end of recursion
            return []

    def pop_wires(self, instances, io_names):
        """Returns wires connecting to instances but not in io_names."""
        ports = set([p for i in instances for p in self.inst_manager.get_inst_ports(i)]) 
        non_io_ports = [p for p in ports if p not in io_names]
        match_bucket, nonmatch_bucket = [], []
        for w in self.top_wires:
            if w.name in non_io_ports:
                match_bucket.append(w)
            else:
                nonmatch_bucket.append(w)
        self.top_wires = nonmatch_bucket
        return match_bucket
        
    def get_pipe_regs(self, cur_stage):
        """
            Returns pipeline registers belonging to the cur_stage.
            Note that 1st stage has no pipeline registers (see README).
        """
        if str(cur_stage) in self.md_dict:
            md = self.md_dict[str(cur_stage)]
            return self._inst_lists_of(md)
        else:
            return []
   
    def create_md(self, md_name, port_list, items, param=None):
        port_list = ast.Portlist(tuple(port_list))
        return ast.ModuleDef(md_name, param, port_list, items)

    def get_pass_thrus(self, stage_idx, input_names, clock_tag):
        """
            Returns three variants of pass-through nets.
        """
        if stage_idx < len(self.pipe_instances):
            pass_thrus_v1, pass_thrus_v2, pass_thrus_v3 = [], [], []
            preg_ports = self.pipe_instances[stage_idx].portlist
            for p in preg_ports:
                net_name = InstanceManager.parg2net(p.argname)
                if net_name in input_names and net_name not in clock_tag:
                    if isinstance(p.argname, ast.Identifier): # variant 1
                        pass_thrus_v1.append(net_name)
                    elif isinstance(p.argname, ast.Pointer): # variant 2
                        before = net_name + "[" + str(p.argname.ptr) + "]"
                        dc_dump = "\\" + before
                        after = ModuleDefManager._simplify(dc_dump)
                        pass_thrus_v2.append((before, after))
                    elif isinstance(p.argname, ast.Partselect): # variant 3
                        pass_thrus_v3.append(net_name)
                    else:
                        continue
            return (pass_thrus_v1, pass_thrus_v2, pass_thrus_v3)
        else:
            return ([],[],[])

    def is_mid_stage(self, stage_idx):
        return stage_idx > 0 and stage_idx < self.num_stages

    def get_z_out_table(self, stage_idx, pregs):
        preg_ports = self.pipe_instances[stage_idx-1].portlist
        my_net_name = set([InstanceManager.parg2net(p.argname) for p in preg_ports])
        nxt_preg_ports = self.pipe_instances[stage_idx].portlist
        nxt_net_name = set([InstanceManager.parg2net(p.argname) for p in nxt_preg_ports])
        z_out_argnames = my_net_name.intersection(nxt_net_name)
        if z_out_argnames:
            z_out_port2arg = {p.portname : InstanceManager.parg2net(p.argname) for p in preg_ports
                                if InstanceManager.parg2net(p.argname) in z_out_argnames}
            return z_out_port2arg
        else:
            return None

    @staticmethod
    def _simplify(s):
        # Replace all invalid char with underscores.
        return re.sub("[^a-zA-Z0-9_$]", "_", s)

    def _build_md_dict(self, simplify):
        # Build a dict with keys being 'top' or stage_id in str and
        #                   values being module definitions  
        md_dict = {}
        for md in self.module_defs:
            if md.name.startswith(self.pipe_prefix):
                n = int(md.name.split(self.pipe_prefix)[1])
                md_dict[str(n)] = self._rename_escaped_id(md) if simplify else md
                self.num_stages = n if n > self.num_stages else self.num_stages
            else: # top module
                md_dict['top'] = self._rename_escaped_id(md) if simplify else md
        return md_dict

    def _rename_escaped_id(self, md):
        # Rename all identifiers in a module definition to a form compatible with Verilog
        def name_simplify(list):
            for x in list:
                x.name = ModuleDefManager._simplify(x.name)

        def gate_port_simplify(gates):
            for g in gates:
                InstanceManager.rename_ports_of(g, ModuleDefManager._simplify)

        name_simplify(md.portlist.ports)
        name_simplify(self._decls_of(md))
        gate_port_simplify(self._inst_lists_of(md))
        return md

    def _decls_of(self, md):
        d_list = [i for i in md.items if isinstance(i, ast.Decl)]
        return [e for d in d_list for e in d.list]

    def _inst_lists_of(self, md, exclude=()):
        return [i for i in md.items if isinstance(i, ast.InstanceList) and
                                       not i.module.startswith(exclude)]

    def _pipe_inst_lists_of(self, md):
        return [i.instances[0] for i in md.items if isinstance(i, ast.InstanceList) and
                                                    i.module.startswith(self.pipe_prefix)]
 
class ModuleTransformer(object):    

    def __init__(self, buf_factory):
        self.buf_factory = buf_factory

    def create_buffers(self, nets, rename_table):
        bufs = []
        for n in nets:
            k, v = n.name, rename_table[n.name]
            if n.width.msb.value == n.width.lsb.value: # 1-bit wire
                bufs.append(self.buf_factory.create(k, v))
            else:
                width = int(n.width.msb.value) - int(n.width.lsb.value) + 1
                bufs += self.buf_factory.create_batch(k, v, width)
        return bufs

    def create_buffers_from_tuple(self, nets):
        bufs = []
        for n in nets:
            k, v = n[0], n[1]
            bufs.append(self.buf_factory.create(k, v))
        return bufs

    def create_buffers_v3(self, nets, rename_table):
        bufs = []
        for n in nets:
            k, v = n.name, rename_table[n.name]
            width = int(n.width.msb.value) - int(n.width.lsb.value) + 1
            offset = int(n.width.lsb.value)
            bufs += self.buf_factory.create_batch_offset(k, v, width, offset)
        return bufs

    @staticmethod
    def rm_clk(nets, clock_tag=[]):
        CLK = ('clk', 'clock', 'CLK') if not clock_tag else clock_tag
        return [n for n in nets if n.name not in CLK]

    @staticmethod
    def flip_direction(nets): 
        def astIn2Out(i):
            return ast.Output(i.name, i.width)

        return [astIn2Out(n) for n in nets]

    @staticmethod
    def build_rename_table(nets, rename_table):
        for n in nets:
            rename_table[n] = n + "_o_renamed"

    @staticmethod
    def rename(l, rename_table):
        for e in l:
            e.name = rename_table.get(e.name, e.name)

    @staticmethod
    def rename_offset(l, rename_table, offset_table):
        for e in l:
            e.name = rename_table.get(e.name, e.name)
            offset = int(e.width.lsb.value)
            if offset != 0:
                e.width.lsb.value = ast.IntConst(str(0))
                e.width.msb.value = ast.IntConst(str(int(e.width.msb.value) - offset))
                offset_table[e.name] = offset

    @staticmethod
    def rename_ports(instances, rename_table):
        rename_func = lambda x: rename_table.get(x, x)
        for i in instances:
            InstanceManager.rename_ports_of(i, rename_func)

    @staticmethod
    def adjust_ports(instances, offset_table):
        adjust_func = lambda x: offset_table.get(x, None)
        for i in instances:
            InstanceManager.adjust_ports_of(i, adjust_func)


class BUF_FACTORY(object):
    def __init__(self, module, name_base, in_port, out_port):
        self.module = module
        self.name_base = name_base
        self.in_port = in_port
        self.out_port = out_port
        self.serial_num = 0

    def create(self, i, o):
        return self._create_instance_list(ast.Identifier(i), ast.Identifier(o))

    def create_batch(self, i, o, width):
        inst_list = []
        for w in range(width):
            il = self._create_instance_list(ast.Pointer(ast.Identifier(i), ast.IntConst(w)),
                                            ast.Pointer(ast.Identifier(o), ast.IntConst(w)))
            inst_list.append(il)
        return inst_list

    def create_batch_offset(self, i, o, width, offset):
        inst_list = []
        for w in range(width):
            il = self._create_instance_list(ast.Pointer(ast.Identifier(i), ast.IntConst(w + offset)),
                                            ast.Pointer(ast.Identifier(o), ast.IntConst(w)))
            inst_list.append(il)
        return inst_list

    def _nxt_name(self):
        name = self.name_base + str(self.serial_num)
        self.serial_num += 1 
        return name
    
    def _create_instance_list(self, in_argname, out_argname):
        NULL_PARAM = []
        in_parg = ast.PortArg(self.in_port, in_argname)
        out_parg = ast.PortArg(self.out_port, out_argname)
        port_list = (in_parg, out_parg)
        inst = ast.Instance(self.module, self._nxt_name(), port_list, NULL_PARAM)
        return ast.InstanceList(self.module, NULL_PARAM, [inst])

class SAED32NBUFF(BUF_FACTORY):
    def __init__(self):
        MODULE, NAME_BASE = "NBUFFX2_RVT", "nbuff_hm_renamed_"
        IN_PORT, OUT_PORT = "A", "Y"
        super(SAED32NBUFF, self).__init__(MODULE, NAME_BASE, IN_PORT, OUT_PORT)

class BuffCatalog(object):
    catalog = {
        "saed32nm.v" : SAED32NBUFF, 
    }        

    @staticmethod
    def get_buff_factory(cell_lib):
        cell_lib = os.path.basename(cell_lib)
        buff_class = BuffCatalog.catalog.get(cell_lib, None)
        assert buff_class, "Cannot find the desired buff for {}".format(cell_lib)
        return buff_class()


class CodeGenerator(object):
    codegen = ASTCodeGenerator()

    @staticmethod
    def dump(md, path):
        src = CodeGenerator.wrap_def(md)
        CodeGenerator.dump_verilog(src, path)

    @staticmethod
    def wrap_def(md):
        defs = tuple([md])
        dscrp = ast.Description(defs)
        return ast.Source('test', dscrp)
    
    @staticmethod
    def dump_verilog(src, path):
        def remove_bus(code):
            return code.replace(r"[0:0]", "")
    
        with open(path, "w") as f:
            vcode = CodeGenerator.codegen.visit(src)
            f.write(remove_bus(vcode))


class RenameTable(object):
    RENAMED_OUT = defaultdict(dict)

    @staticmethod
    def get_table_of(stage_idx):
        return RenameTable.RENAMED_OUT[stage_idx]

class OffsetTable(object):
    RENAMED_OUT = defaultdict(dict)

    @staticmethod
    def get_table_of(stage_idx):
        return OffsetTable.RENAMED_OUT[stage_idx]


