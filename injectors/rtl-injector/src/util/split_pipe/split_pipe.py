"""
    Split a pipelined Verilog netlist into modules   

    Assumption:
      Input has already been processed with synthesis tool.
      Specifically, pipeline registers of each stage are grouped into 
      individual modules with for instance the "group" command in DesignCompiler.
      User specifies a tag so that this tool can know which modules are pipeline 
      registers.

    Input: a config file with the following fields:
      - design file: original Verilog netlist
      - cell library file: Verilog file of the cell library
      - tag (or prefix) of the pipeline register modules
      - clock signal names

    Output: Verilog files for each stage
"""
import argparse, yaml, os
from split_util import ModuleDefManager, ModuleTransformer, CodeGenerator
from split_util import RenameTable, OffsetTable, BuffCatalog
from merge_ports import merge_ports

def split(design_file, cell_lib_file, pipe_reg, clock_tag):
    OFILE_PATHS = []
    MD_MNGR = ModuleDefManager(design_file, cell_lib_file, pipe_reg)
    XFMR = ModuleTransformer(BuffCatalog.get_buff_factory(cell_lib_file))
    cur_md = MD_MNGR.top_md()
    
    num_stages = MD_MNGR.num_stages
    for stage_idx in range(num_stages+1):
        print ("===== Stage {} =====".format(stage_idx))
        # setup io ports
        input_decls = MD_MNGR.decls_of(cur_md, "input")
        input_names = set([i.name for i in input_decls])
        
        nxt_md, md_key = MD_MNGR.nxt_md(stage_idx)
        if md_key != 'top':
            nxt_md_decls = MD_MNGR.decls_of(nxt_md, "input") 
            output_decls = XFMR.flip_direction(XFMR.rm_clk(nxt_md_decls, clock_tag))
        else:    
            nxt_md_decls = MD_MNGR.decls_of(nxt_md, "output") 
            output_decls = XFMR.rm_clk(nxt_md_decls, clock_tag)
        output_names = set([i.name for i in output_decls])
      
        in_port_list = [p for p in cur_md.portlist.ports if p.name in input_names]
        out_port_list = [p for p in nxt_md.portlist.ports if p.name in output_names]
    
        # get gates from the original module
        print ("#Instances before pop: {}".format(len(MD_MNGR.top_instances)))
        instances = MD_MNGR.pop_instances(output_names)
        print ("#Instances after pop: {}".format(len(MD_MNGR.top_instances)))
    
        # get wires
        print ("#Wires before pop: {}".format(len(MD_MNGR.top_wires)))
        wires = MD_MNGR.pop_wires(instances, input_names | output_names)
        print ("#Wires after pop: {}".format(len(MD_MNGR.top_wires)))
    
        # get pipeline registers
        pregs = MD_MNGR.get_pipe_regs(stage_idx)
        ## rename pass-through nets from last stage (if any)
        if pregs and RenameTable.get_table_of(stage_idx-1):
            rename_table = RenameTable.get_table_of(stage_idx-1)
            XFMR.rename_ports(pregs, rename_table)
        if pregs and OffsetTable.get_table_of(stage_idx-1):
            offset_table = OffsetTable.get_table_of(stage_idx-1)
            XFMR.adjust_ports(pregs, offset_table)
        ## rename pregs with output directly connecting to input of next stage
        if pregs and MD_MNGR.is_mid_stage(stage_idx):
            zout_table = MD_MNGR.get_z_out_table(stage_idx, pregs)
            if zout_table:
                print ("Fix pipeline registers with Z output...")
                XFMR.rename_ports(pregs, zout_table)

        instances += pregs
    
        # process pass-through nets
        ## since it is not valid to have input and output ports with same identifiers
        ## we have to add buffers for these cases
        ## this should happen only at the first stage
        pass_thrus, pass_thrus_v2, pass_thrus_v3 = MD_MNGR.get_pass_thrus(stage_idx, input_names, clock_tag)
        if pass_thrus:
            rename_table = RenameTable.get_table_of(stage_idx)
            XFMR.build_rename_table(pass_thrus, rename_table)
            renamed_decls = [v for v in output_decls if v.name in rename_table]
            instances += XFMR.create_buffers(renamed_decls, rename_table)
            XFMR.rename(out_port_list, rename_table)
            XFMR.rename(output_decls, rename_table)
            # rename input decls of next stage that are pass-through
            nxt_md_decls = MD_MNGR.decls_of(nxt_md, "input") 
            XFMR.rename(nxt_md_decls, rename_table)
            print("Rename table v1 at stage {}".format(stage_idx))
            print rename_table
        if pass_thrus_v2:
            instances += XFMR.create_buffers_from_tuple(pass_thrus_v2)
        if pass_thrus_v3:
            rename_table = RenameTable.get_table_of(stage_idx)
            XFMR.build_rename_table(pass_thrus_v3, rename_table)
            renamed_decls = [v for v in output_decls if v.name in rename_table]
            instances += XFMR.create_buffers_v3(renamed_decls, rename_table)
            XFMR.rename(out_port_list, rename_table)
            offset_table = OffsetTable.get_table_of(stage_idx)
            XFMR.build_rename_table(pass_thrus_v3, rename_table)
            XFMR.rename_offset(output_decls, rename_table, offset_table)
            # rename input decls of next stage that are pass-through
            nxt_md_decls = MD_MNGR.decls_of(nxt_md, "input") 
            XFMR.rename(nxt_md_decls, rename_table)
            print("Rename table v3 at stage {}".format(stage_idx))
            print rename_table
           
        cur_md_name = MD_MNGR.top_md().name + '_stage_' + str(stage_idx)
        items = input_decls + output_decls + wires + instances
        md = MD_MNGR.create_md(cur_md_name, in_port_list + out_port_list, items)
    
        out_file = "{}_{}.v".format(os.path.basename(design_file).replace(".v", ""), str(stage_idx))
        CodeGenerator.dump(md, out_file)
        OFILE_PATHS.append(out_file)

        cur_md = nxt_md
    
    assert len(MD_MNGR.top_instances) == 0, "Instances left after splitting"
    return OFILE_PATHS

def main():
    parser = argparse.ArgumentParser(description="Splits a pre-processed pipelined circuit into modules")
    parser.add_argument('-i', '--config', required=True, help="Config file in YAML")
    options = parser.parse_args()

    config = {}
    with open(options.config) as f:
        config = yaml.load(f)
        assert "design" in config and os.path.exists(config["design"]) and \
               "cell_lib" in config and os.path.exists(config["cell_lib"]) and \
               "pipe_reg_tag" in config and config["pipe_reg_tag"]

    # split pass
    ofiles = split(config["design"], config["cell_lib"], config["pipe_reg_tag"], 
                   config.get("clock_tag", []))
    # merge pass
    merge_ports(ofiles, config["cell_lib"], config.get("clock_tag", []))

if __name__ == "__main__":
    main()
