import pytest
import os
import sys
import yaml
import subprocess
import difflib

EXE_PATH = "../split_pipe.py" 
CONFIG_TMPLT = "config_saed32nm.yaml"
tmp_config = "tmp.yaml"
GOLDEN_DIR = "golden"

def test_fp_addsub_32b_2_stages():
    funcname = sys._getframe().f_code.co_name
    stage_ids = range(3)
    core_test(funcname, stage_ids)

def test_fp_addsub_64b_2_stages():
    funcname = sys._getframe().f_code.co_name
    stage_ids = range(3)
    core_test(funcname, stage_ids)

def test_mult_32b_2_stages():
    funcname = sys._getframe().f_code.co_name
    stage_ids = range(3)
    core_test(funcname, stage_ids)

def gen_config(module_name):
    with open(CONFIG_TMPLT) as cf:
        config = yaml.load(cf)
        config["design"] = module_name + ".v"
        with open(tmp_config, "w") as of:
            yaml.dump(config, of, default_flow_style=False)

def core_test(funcname, stage_ids):
    module_name = funcname.split("test_")[1]
    gen_config(module_name)

    cmd = ['python', EXE_PATH, '-i', tmp_config]
    child_process = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    out, err = child_process.communicate()

    for s in stage_ids:
        outf = "{}_{}.v".format(module_name, s) 
        golden_f = os.path.join(GOLDEN_DIR, outf)
        with open(outf) as f1, open(golden_f) as f2:
            assert f1.read() == f2.read()
        os.remove(outf)

    os.remove(tmp_config)
