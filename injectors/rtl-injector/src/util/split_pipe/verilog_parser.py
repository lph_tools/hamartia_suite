"""
Verilog Helper

Basically, Pyverilog generates some helper files (optimized lexer/parsers). Unfortunately,
if it doesn't know where these are it will keep generating it. This drastically slows down
parsing. This sets up the paths for the parser so they are inserted in the src folder and 
reused each time.

Methods
=======
"""

# Base
import os, sys, shutil, re
SCRIPT_DIR = os.path.dirname(os.path.realpath(__file__))

# Pyverilog
from pyverilog.vparser import ply
import pyverilog.vparser.parser as vparser
import pyverilog.vparser.ast as ast

def parse(filelist, table_output_dir="./", preprocess_output='preprocess.output', debug=0):
    """
     Verilog parser front-end (allows for generating/reusing the table outputs)

     Args:
        filelist            (list): verilog files to parse
        table_output_dir    (str): path to table output directory [optional]
        preprocess_output   (str): name of preprocess output [optional]
        debug               (int): debug mode [optional]
    """

    text = vparser.preprocess(filelist, preprocess_output)
    parser = vparser.VerilogParser()

    # Overwrite yacc to look in right directory
    parser.parser = ply.yacc.yacc(module=parser, method="LALR", outputdir=table_output_dir)

    # Move files if generated
    files = ["parser.out", "parsetab.py", "parsetab.pyc"]
    for f in files:
        if os.path.isfile(f):
            shutil.move(f, os.path.join(table_output_dir, f))

    ast = parser.parse(text, debug=debug)
    return ast

def parse_cell_library(path, module_io):
    """
    Parse the RTL cell-library provided. Basicially extract the different cells and their I/O.

    Args:
        path        (str): cell library path
        module_io   (dict): dict of all modules parsed (in a format similar to: parse_modules)
    """

    # Parsing regex (module and input/output respectively)
    module_re = re.compile(r"module (.*?)\(")
    io_re = re.compile(r"(input|output).*?([\w0-9_]+)[;, ]+")

    # Open cell library
    with open(path, 'r') as cells:
        mod = ""
        record = False
        for l in cells:
            # FIXME: could have issues with inner module defs (do they exist?)
            if 'module' in l:
                if 'endmodule' in l:
                    # End module
                    record = False
                else:
                    # Start module
                    m = re.match(module_re, l)
                    mod = m.group(1).strip()
                    module_io[mod] = {'ins': 0, 'lib': True, 'io': {}}
                    record = True
            elif record:
                # Record inputs/outputs
                for io in re.finditer(io_re, l):
                    # Assume width 1
                    # FIXME: include width!
                    module_io[mod]['io'][io.group(2)] = (io.group(1), 1) # group(2): io name; group(1): 'input' or 'output'

# Parse module info (io, wires, etc)
def parse_modules(node, module_io, parents=[]):
    """
    Parse the module information (recursively). Basically extract modules and their I/O, wires, connection info.

    Args:
        node        (object): current node object (AST object)
        module_io   (dict): dict of all modules parsed
        parents     (list): list of parent nodes/objects [optional]

    module_io format::

        {
            '$module_name': {
                'def': module_def,              # Module Def (AST node)
                'ins': instances,               # Number of instances
                'lib': cell_lib,                # Is Cell library (bool)
                'io': {
                    '$io_name': 'input|output'  # I/O name and input/output
                },
                'in_wires': {                   # Wires into the module/instance
                    '$wire_name': [
                        [
                            wire_node,          # AST node for wire/pointer/partselect
                            parent_node,        # Parent node (module instance)
                            port_node,          # Port node (ast.Port)
                            index_in_concat     # Index in wire concat (i.e. { line[1:0], wire, 1'b0 })
                        ]
                    ]
                },
                'out_wires': {                  # Wires out of the module
                    '$wire_name': [
                        wire_node,              # AST node for wire/pointer/partselect
                        parent_node,            # Parent node (module instance)
                        port_node,              # Port node (ast.Port)
                        index_in_concat         # Index in wire concat (i.e. { line[1:0], wire, 1'b0 })
                    ]
                }
            }
        }
    """

    parent = None
    if parents:
        parent = parents[0]

    # Found module, add to list
    if isinstance(node, ast.ModuleDef):
        module_io[node.name] = {'def': node, 'ins': 0, 'lib': False, 'io': {}, 'in_wires': {}, 'out_wires': {}}

    # Recursively parse children
    if node.children():
        for c in node.children():
            parse_modules(c, module_io, [node] + parents)

    # Found I/O, add to list for module
    if isinstance(node, ast.Variable):
        for p in parents:
            if isinstance(p, ast.ModuleDef):
                # input, output, wire, reg
                node_type = type(node).__name__.lower()
                module_io[p.name]['io'][node.name] = (node_type, int(node.width.msb.value)-int(node.width.lsb.value)+1) # (node_type, width)
                break;

    # Found net/wire usage, add to list
    if isinstance(node, ast.PortArg) and isinstance(parent, ast.Instance):
        #print "PortArg {}, argname: {}".format(node.portname, node.argname.var.name)
        ports = [node.argname]
        # If a concat of wires, add each one
        if isinstance(node.argname, ast.Concat):
            ports = node.argname.list

        # Record I/O
        for pi, port in enumerate(ports):
            wire_name = ''
            # FIXME: do I need to record width? If everything ends at a gate, shouldn't it only be one wire?
            if isinstance(port, ast.Identifier):
                # Regular wire
                wire_name = port.name
            elif isinstance(port, ast.Pointer):
                # Pointer (i.e. wire[x])
                wire_name = port.var.name + ' [' + str(port.ptr) + ']'
            elif isinstance(port, ast.Partselect):
                # Range (i.e. wire[x:y])
                wire_name = port.var.name + ' [' + str(port.msb) + ':' + str(port.lsb) + ']'
            elif not isinstance(port, ast.IntConst):
                # Unknown/unparsed port (skip)
                print "UNKNOWN TYPE:",type(port),port.__dict__
                continue

            # Wires
            # TODO: add primatives to lib?
            if parent.module in module_io and module_io[parent.module]['lib'] and module_io[parent.module]['io'][node.portname][0] in ['input', 'inout']:
                # Input into terminal module (cell library)
                for p in parents:
                    if isinstance(p, ast.ModuleDef):
                        if not wire_name in module_io[p.name]['in_wires']:
                            module_io[p.name]['in_wires'][wire_name] = []
                        module_io[p.name]['in_wires'][wire_name].append([node, parent, port, pi])
            elif parent.module in module_io and module_io[parent.module]['lib'] and module_io[parent.module]['io'][node.portname][0] in ['inout', 'output']:
                # Output from terminal module (for selecting wires)
                for p in parents:
                    if isinstance(p, ast.ModuleDef):
                        module_io[p.name]['out_wires'][wire_name] = [node, parent, port, pi]


