"""
    Given a list of Verilog pipeline stages, merge the ports between stages
    into width of 64.
"""

import pyverilog.vparser.ast as ast
from split_util import ModuleDefManager, ModuleTransformer, CodeGenerator, RenameTable

def _merge_decls(decls, stage_idx, clock_tag, bundle=64):
    """
      bundle 1-bit ports into 64-bit ports
      return (1) a list of new decls and 
             (2) a map from old decl to new decl
    """
    def is_one_bit(decl):
        return decl.width.msb.value == decl.width.lsb.value

    def create_width(w):
        return ast.Width(ast.IntConst(w-1), ast.IntConst(0))

    port_type = ast.Input if isinstance(decls[0], ast.Input) else ast.Output

    unchanged_decls = [d for d in decls if not is_one_bit(d)] + [d for d in decls if d.name in clock_tag]
    one_bit_decls = [d for d in decls if is_one_bit(d) and d.name not in clock_tag]
    one_bit_groups = [one_bit_decls[x : x+bundle] for x in range(0, len(one_bit_decls), bundle)]
    new_decls = []
    rename_table = {}
    for gid, group in enumerate(one_bit_groups):
        port_name = ""
        if port_type is ast.Input:
            port_name = "stage_{}_out_{}".format(stage_idx-1, gid)
        else:    
            port_name = "stage_{}_out_{}".format(stage_idx, gid)
        port_width = len(group)
        for did, d in enumerate(group):
            rename_table[d.name] = (port_name, did) # port name, index
        new_decls.append( port_type(port_name, create_width(port_width)) )
    
    return (unchanged_decls + new_decls, rename_table)

def _create_ports_from_decls(decls):
    return [ast.Port(d.name, None, None) for d in decls]

def is_one_bit(decl):
    return decl.width.msb.value == decl.width.lsb.value

def merge_ports(files, cell_lib_file, clock_tag):
    INVALID_TAG = "INVALID_PIPE_TAG"
    md_mngrs = [ModuleDefManager(df, cell_lib_file, INVALID_TAG, False) for df in files]
    
    for mid, md_mngr in enumerate(md_mngrs):
        md = md_mngr.top_md()
        inst_mngr = md_mngr.inst_manager
        in_decls, out_decls = md_mngr.decls_of(md, "input"), md_mngr.decls_of(md, "output") 
        in_ports = md_mngr.ports_with_name_in(md, set([d.name for d in in_decls]))
        out_ports = md_mngr.ports_with_name_in(md, set([d.name for d in out_decls]))
        wires = md_mngr.top_wires
        instances = md_mngr.top_instances
    
        if mid != len(md_mngrs) - 1:
            # merge output
            unchanged_decls = [d for d in out_decls if not is_one_bit(d)] + [d for d in out_decls if d.name in clock_tag]
            out_decls, out_rename_table = _merge_decls(out_decls, mid, clock_tag)
            out_ports = _create_ports_from_decls(out_decls)
            ## rename instance out port names to merged output
            renamed = out_rename_table.keys()
            insts_to_rename = [i for i in instances if inst_mngr.has_output_to(i, renamed) or \
                                                       inst_mngr.has_input_to(i, renamed)]
            ModuleTransformer.rename_ports(insts_to_rename, out_rename_table)
    
        if mid != 0:
            # merge input
            in_decls, in_rename_table = _merge_decls(in_decls, mid, clock_tag)
            in_ports = _create_ports_from_decls(in_decls) 
            ## rename instance in port names to merged input
            renamed = in_rename_table.keys()
            insts_to_rename = [i for i in instances if inst_mngr.has_input_to(i, renamed)]
            ModuleTransformer.rename_ports(insts_to_rename, in_rename_table)
    
        items = in_decls + out_decls + wires + instances
        new_md = md_mngr.create_md(md_mngr.top_md().name, in_ports + out_ports, items)
    
        out_file = files[mid]
        CodeGenerator.dump(new_md, out_file)
    
