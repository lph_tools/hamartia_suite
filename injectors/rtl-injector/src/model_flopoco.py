# Copyright 2015, The University of Texas at Austin 
# All rights reserved.
# 
# THIS FILE IS PART OF THE HAMARTIA FAULT/ERROR ANALYSIS SUITE
# 
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met: 
# 
# 1. Redistributions of source code must retain the above copyright
# notice, this list of conditions and the following disclaimer. 
# 
# 2. Redistributions in binary form must reproduce the above copyright
# notice, this list of conditions and the following disclaimer in the
# documentation and/or other materials provided with the distribution. 
# 
# 3. Neither the name of the copyright holder nor the names of its
# contributors may be used to endorse or promote products derived from
# this software without specific prior written permission. 
# 
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
# FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
# COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
# INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
# BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
# ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

# Libs
import os, sys, yaml

# RTL injector
import inject

# Hamartia API
if 'HAMARTIA_DIR' in os.environ:
    sys.path.append(os.path.join(os.environ['HAMARTIA_DIR'], 'src/python'))
import hamartia_api

class RTLErrorModel(hamartia_api.error_model.ErrorModel):
    """
    Error Model Front-end for RTL Injector (model)
    """
    def __init__(self, name):
        """
        Error-model constructor

        Args:
            name (str): error model name
        """
        super(RTLErrorModel, self).__init__(name)


    def _find_match(self, cxt, config):
        if not 'modules' in config:
            raise Exception("RTL module settings needed!")

        inst_match = {
                'width' : cxt.instruction_data.width,
                'operation' : hamartia_api.interface.Operation_Name(cxt.instruction_data.instruction.operation),
                'datatype' : hamartia_api.interface.DataType_Name(cxt.instruction_data.instruction.data),
                'packing' : hamartia_api.interface.Packing_Name(cxt.instruction_data.instruction.packing)
        }

        # Find matching RTL module for instruction
        for name, module in config['modules'].iteritems():
            match = False
            for inst in module['instructions']:
                match = True
                for k, v in inst_match.iteritems():
                    if k in inst and inst[k] != v:
                        match = False
                        break
                if match:
                    break

            if match:
                return (name, module)

        return (None, None)

    def _ieee2flopoco(self, uint_val, data_type):
        WIDTH = {
            'DT_FLOAT': 32,       
            'DT_DOUBLE': 64,       
        }        
        if uint_val == 0: # 0 is special case
            return 0
        else:    
            bin_str = "{0:b}".format(uint_val)
            width = WIDTH.get(data_type, None)
            assert width, "Invalid data type {}".format(data_type)
            bin_str = '01' + bin_str.zfill(width)
            return int(bin_str, 2)

    def _flopoco2ieee(self, uint_val, data_type):
        WIDTH = {
            'DT_FLOAT': 34,       
            'DT_DOUBLE': 66,       
        }        
        bin_str = "{0:b}".format(uint_val)
        width = WIDTH.get(data_type, None)
        assert width, "Invalid data type {}".format(data_type)
        bin_str = bin_str.zfill(width)[2:] # remove the two MSB's
        return int(bin_str, 2)

    def _run_sim(self, cxt, config, mod_name, module, run=True):
        # TODO: setup only once?
        dirs = {}
        if 'dirs' in config:
            dirs = config['dirs']

        cell_libs = {}
        if 'cell_libs' in config:
            cell_libs = config['cell_libs']

        # Lib
        lib = module['cell_lib']
        mapping = None
        rtl_dir = '.'
        if lib in cell_libs:
            rtl_dir = cell_libs[lib]['dir']
            mapping = os.path.join(rtl_dir, cell_libs[lib]['mapping'])
            lib = os.path.join(rtl_dir, cell_libs[lib]['src'])
            
        if 'dir' in module:
            rtl_dir = module['dir']
        if rtl_dir in dirs:
            rtl_dir = dirs[rtl_dir]
        rtl_src = os.path.join(rtl_dir, module['rtl_src'])
        group_mappings = inject.parse_group_mapping(mapping, lib, rtl_dir)

        # Cache
        cache = 'cache'
        if 'cache' in config:
            cache = config['cache']

        # Ports
        # FIXME: does this work? Is it 0 if not a SIMD instruction?
        if cxt is not None:
            lane = cxt.error_info.target_simd_lane
        lane = 0

        dp = hamartia_api.interface.DataType_Name(cxt.instruction_data.instruction.data)

        input_ports = {}
        for k, v in module['inputs'].iteritems():
            if 'asm_idx' in v:
                if type(v['asm_idx']) is list: # FIXME: FP shouldn't take this path?
                    shf = int(v['width'])/len(v['asm_idx'])
                    input_ports[k] = 0
                    if cxt is not None:
                        for i, asm_idx in enumerate(v['asm_idx']):
                            input_ports[k] |= cxt.instruction_data.inputs[asm_idx].value[lane].uint() << (i * shf)
                else:
                    if cxt is not None:
                        if dp in ['DT_FLOAT', 'DT_DOUBLE']:
                            ieee_val = cxt.instruction_data.inputs[v['asm_idx']].value[lane].uint()
                            input_ports[k] = self._ieee2flopoco(ieee_val, dp)
                        else:
                            input_ports[k] = cxt.instruction_data.inputs[v['asm_idx']].value[lane].uint()
                    else:
                        input_ports[k] = 0
            elif 'value' in v:
                input_ports[k] = int(v['value'])

        output_ports = {k : 0 for k, v in module['outputs'].iteritems()}
        clk_port = module['clk'] if 'clk' in module else None
        clk_stages = module['stages'] if 'stages' in module else 0

        # Fault
        fault = {
            'type': "FLIP",
            'num' : 1,
            'targets': []
        }

        if 'fault' in config:
            for fk, fv in config['fault'].iteritems():
                fault[fk] = fv

        # save RTL or not
        rtl_save = 'rtl_save'
        rtl_save_path = None
        if rtl_save in dirs:
            rtl_save_path = dirs[rtl_save] 
            
        # Inject
        inject.verbose = True# (not hamartia_api.debug.DebugEnabled())
        results = inject.run(lib, rtl_src, module['top_module'], fault['type'], fault['num'], fault['targets'], [], input_ports, output_ports, clk_port, clk_stages, None, None, group_mappings, dir=rtl_save_path ,run_sim=run, cache=cache)

        return results


    def Startup(self, config):
        for name, module in config['modules'].iteritems():
            print "Caching",name,"..."
            self._run_sim(None, config, name, module, False)


    # Injection of error
    def InjectError(self, cxt, config, log = None): 
        """
        Inject error (defaults to no error)
        
        Args:
            cxt    (object): The context before error injection
            config (object): YAML config file for configuring the model
            log    (object): Log for adding extra logging information
        
        Returns:
            bool: Successful
        """

        if not config:
            raise Exception("RTL config file needed!")

        # Find match
        (mod_name, module) = self._find_match(cxt, config)

        if mod_name is not None:
            # Run simulation
            results = self._run_sim(cxt, config, mod_name, module)

            # FIXME: does this work? Is it 0 if not a SIMD instruction?
            lane = cxt.error_info.target_simd_lane
            lane = 0

            dp = hamartia_api.interface.DataType_Name(cxt.instruction_data.instruction.data)
            # Update outputs
            for k, v in results['incorrect'].iteritems():
                width = module['outputs'][k]['width'] if 'width' in module['outputs'][k] else 1
                if dp in ['DT_FLOAT', 'DT_DOUBLE']:
                    v = self._flopoco2ieee(v, dp)
                    if width in [34, 66]:
                        width -= 2 # exclude the two extra bits in FloPoCo format
                idxs = module['outputs'][k]['asm_idx']
                if type(idxs) is not list:
                    idxs = [idxs]
                width = width / len(idxs)
                for i, idx in enumerate(idxs):
                    cxt.setOutput(hamartia_api.DataValue((v >> (width * i)) & ((1 << width)-1)), idx, lane)

            # Populate log
            log.ModelAddLogString('rtl', 'module', mod_name)
            for k, v in results.iteritems():
                log.ModelAddLogString('rtl', str(k), str(v))

            # Update RFLAGS
            return True

        # No matching RTL module found
        hamartia_api.debug.FatalError("No inst/module mapping!")
        return False

    # Output log/stats
    def SaveOutput(self, output_file): 
        """
        Save the logging output

        Returns:
            bool: Success
        """
        pass


class RTLErrorModelLibrary(hamartia_api.error_model.ErrorModelLibrary):
    """
    Basic error models
    """
    
    def __init__(self):
        """
        Initialize basic error model library, register all error models
        """

        # Init
        super(RTLErrorModelLibrary, self).__init__()

        # RTL Error Model
        rtlm = RTLErrorModel('RTL')
        self.AddModel(rtlm)
        rtlm.__disown__()
        super(RTLErrorModel, rtlm).__disown__()


    def GetName(self):
        """
        Get name of library

        Returns:
            str: Name of library
        """

        return "rtl_error_models"


# Register library
register = RTLErrorModelLibrary

if __name__ == '__main__':
    if len(sys.argv) <= 1:
        raise Exception("Config files required!")

    rtl = RTLErrorModel('rtl')
    for config in sys.argv[1:]:
        config_yml = None
        with open(config, 'r') as yf:
            config_yml = yaml.load(yf)
        rtl.Startup(config_yml)
