#! /bin/bash

# Directory of script
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

# TACC specific
if [ $(hostname | grep tacc) ]
then
    module load python
    module load gcc/4.9.1
fi

# Environment
export LD_LIBRARY_PATH=$HAMARTIA_DIR/build:$HAMARTIA_DIR/src/python/hamartia_api:$LD_LIBRARY_PATH
export DYLD_LIBRARY_PATH=$HAMARTIA_DIR/build:$HAMARTIA_DIR/src/python/hamartia_api:$DYLD_LIBRARY_PATH
export PYTHONPATH=$HAMARTIA_DIR/src/python:$PYTHONPATH

# Launch injector
python $DIR/../src/model.py "$@"
