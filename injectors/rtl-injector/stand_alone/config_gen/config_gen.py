#! /usr/bin/env python
"""
    A helper script that generates config from a module library.
    It takes a module name in the module library file and generates the corresponding config.
    Currently, we provide these module libraries:
        - 'dw.yaml' : (default) RTL circuits from Synopsys DesignWare Library
        - 'flopoco.yaml' : RTL circuits from the FloPoCo open-source arithmetic core library
        - 'dw_saed32.yaml' : similar to 'dw.yaml' but synthesized with SAED32 cell library 

    Example usage: `python config_gen.py -n add_32b`
"""
import os
import argparse
import yaml

# Filenames
DIR_PATH = os.path.abspath(os.path.dirname(__file__))
CONFIG_TEMPLT = os.path.join(DIR_PATH, "templates/tmplt.yaml")

# Cache path should change with library
# MAP of library to cache path 
CACHE_MAP = {
    "dw.yaml" : "./cache_dw",
    "flopoco.yaml" : "./cache_flopoco",
    "dw_saed32.yaml" : "./cache_dw_saed32",
}

def run(name, lib, out, extra={}):
    assert os.path.basename(lib) in CACHE_MAP, "Unknown module library!"

    module = get_module(name, lib)
    gen_config(module, lib, out, extra)
 
def get_module(module_name, module_lib):
    with open(module_lib) as mf:
        modules = yaml.load(mf)
        module = modules['modules'].get(module_name, None)
        assert module, "Cannot find the target module!"
        return module

def gen_config(module, module_lib, outfile, extra):
    with open(CONFIG_TEMPLT) as tf:
        template = yaml.load(tf)
        template['module'] = module
        template['cache'] = CACHE_MAP.get(os.path.basename(module_lib), "./cache")
        for k, v in extra.items():
            template[k] = v    
        with open(outfile, "w") as of:
            yaml.dump(template, of, default_flow_style=False)

def main():
    parser = argparse.ArgumentParser(description="Generate target config for RTL-level injection")
    parser.add_argument('-n', '--name', required=True, help="Target module name")
    parser.add_argument('-l', '--lib', default='dw.yaml', help="Module library name")
    parser.add_argument('-o', '--out', default='out.yaml', help="Output config name")
    options = parser.parse_args()

    run(options.name, options.lib, options.out)

    if options.name.startswith('fp'):
        print "Make sure transforming the inputs using fp_convert.py before providing them to the injector."

if __name__ == '__main__':
    main()
