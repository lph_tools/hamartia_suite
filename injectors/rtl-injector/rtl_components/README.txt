To synthesize the Verilog components (in Nangate 45nm technology):
1. Load synthetic tools, for example:
    > module load syn/syn/2016 

2. Change working directory to where the circuits locate
    > cd <design_dir>

3. Run synthetic tool
   3.1 Change the cell library and <design_dir> in the tcl script in <../synthesis>
       - libs: cell library
       - gates: <design_dir>

   3.2 Synthesize from Verilog design to Verilog netlist
    > dc_shell-t -f ../synthesis/dc_syn.tcl

   3.3 Synthesize from VHDL design to Verilog netlist
    > dc_shell-t -f ../synthesis/vhdl.tcl

   3.4 Synthesize from Verilog design to Verilog netlist with Register Retiming (auto pipelining)
       3.3.1 Insert pipeline registers to the designs (cf. Design Compiler user guide)
             - From my experience, inserting regs to outputs is more likely to get functionally correct netlists
       3.3.2 Name the designs "XXX_(\d)_stages.v" where (\d) is the regexp of the number of stages
       3.3.2 Synthesize by running
        > dc_shell-t -f ../synthesis/retiming.tcl
       
4. Test the gate-level netlists with the injector's stand-alone mode

