/////////////////////////////////////////////////////////////
// Created by: Synopsys DC Expert(TM) in wire load mode
// Version   : L-2016.03-SP5-3
// Date      : Wed Nov 21 10:31:55 2018
/////////////////////////////////////////////////////////////


module DW01_addsub_inst ( inst_A, inst_B, inst_CI, inst_ADD_SUB, SUM_inst, 
        CO_inst );
  input [31:0] inst_A;
  input [31:0] inst_B;
  output [31:0] SUM_inst;
  input inst_CI, inst_ADD_SUB;
  output CO_inst;
  wire   \U1/n237 , \U1/n236 , \U1/n235 , \U1/n234 , \U1/n233 , \U1/n232 ,
         \U1/n231 , \U1/n230 , \U1/n229 , \U1/n228 , \U1/n227 , \U1/n226 ,
         \U1/n225 , \U1/n224 , \U1/n223 , \U1/n222 , \U1/n221 , \U1/n220 ,
         \U1/n219 , \U1/n218 , \U1/n217 , \U1/n216 , \U1/n215 , \U1/n214 ,
         \U1/n213 , \U1/n212 , \U1/n211 , \U1/n210 , \U1/n209 , \U1/n208 ,
         \U1/n207 , \U1/n206 , \U1/n205 , \U1/n204 , \U1/n203 , \U1/n202 ,
         \U1/n201 , \U1/n200 , \U1/n199 , \U1/n198 , \U1/n197 , \U1/n196 ,
         \U1/n195 , \U1/n194 , \U1/n193 , \U1/n192 , \U1/n191 , \U1/n190 ,
         \U1/n189 , \U1/n188 , \U1/n187 , \U1/n186 , \U1/n185 , \U1/n184 ,
         \U1/n183 , \U1/n182 , \U1/n181 , \U1/n180 , \U1/n179 , \U1/n178 ,
         \U1/n177 , \U1/n176 , \U1/n175 , \U1/n174 , \U1/n173 , \U1/n172 ,
         \U1/n171 , \U1/n170 , \U1/n169 , \U1/n168 , \U1/n167 , \U1/n166 ,
         \U1/n165 , \U1/n164 , \U1/n163 , \U1/n162 , \U1/n161 , \U1/n160 ,
         \U1/n159 , \U1/n158 , \U1/n157 , \U1/n156 , \U1/n155 , \U1/n154 ,
         \U1/n153 , \U1/n152 , \U1/n151 , \U1/n150 , \U1/n149 , \U1/n148 ,
         \U1/n147 , \U1/n146 , \U1/n145 , \U1/n144 , \U1/n143 , \U1/n142 ,
         \U1/n141 , \U1/n140 , \U1/n139 , \U1/n138 , \U1/n137 , \U1/n136 ,
         \U1/n135 , \U1/n134 , \U1/n133 , \U1/n132 , \U1/n131 , \U1/n130 ,
         \U1/n129 , \U1/n128 , \U1/n127 , \U1/n126 , \U1/n125 , \U1/n124 ,
         \U1/n123 , \U1/n122 , \U1/n121 , \U1/n120 , \U1/n119 , \U1/n118 ,
         \U1/n117 , \U1/n116 , \U1/n115 , \U1/n114 , \U1/n113 , \U1/n112 ,
         \U1/n111 , \U1/n110 , \U1/n109 , \U1/n108 , \U1/n107 , \U1/n106 ,
         \U1/n105 , \U1/n104 , \U1/n103 , \U1/n102 , \U1/n101 , \U1/n100 ,
         \U1/n99 , \U1/n98 , \U1/n97 , \U1/n96 , \U1/n95 , \U1/n94 , \U1/n93 ,
         \U1/n92 , \U1/n91 , \U1/n90 , \U1/n89 , \U1/n88 , \U1/n87 , \U1/n86 ,
         \U1/n85 , \U1/n84 , \U1/n83 , \U1/n82 , \U1/n81 , \U1/n80 , \U1/n79 ,
         \U1/n78 , \U1/n77 , \U1/n76 , \U1/n75 , \U1/n74 , \U1/n73 , \U1/n72 ,
         \U1/n71 , \U1/n70 , \U1/n69 , \U1/n68 , \U1/n67 , \U1/n66 , \U1/n65 ,
         \U1/n64 , \U1/n63 , \U1/n62 , \U1/n61 , \U1/n60 , \U1/n59 , \U1/n58 ,
         \U1/n57 , \U1/n56 , \U1/n55 , \U1/n54 , \U1/n53 , \U1/n52 , \U1/n51 ,
         \U1/n50 , \U1/n49 , \U1/n48 , \U1/n47 , \U1/n46 , \U1/n45 , \U1/n44 ,
         \U1/n43 , \U1/n42 , \U1/n41 , \U1/n40 , \U1/n39 , \U1/n38 , \U1/n37 ,
         \U1/n36 , \U1/n35 , \U1/n34 , \U1/n33 , \U1/n32 , \U1/n31 , \U1/n30 ,
         \U1/n29 , \U1/n28 , \U1/n27 , \U1/n26 , \U1/n25 , \U1/n24 , \U1/n23 ,
         \U1/n22 , \U1/n21 , \U1/n20 , \U1/n19 , \U1/n18 , \U1/n17 , \U1/n16 ,
         \U1/n15 , \U1/n14 , \U1/n13 , \U1/n12 , \U1/n11 , \U1/n10 , \U1/n9 ,
         \U1/n8 , \U1/n7 , \U1/n6 , \U1/n5 , \U1/n4 , \U1/n3 , \U1/n2 ,
         \U1/n1 ;

  XOR2X1_RVT \U1/U271  ( .A1(inst_ADD_SUB), .A2(inst_B[27]), .Y(\U1/n192 ) );
  OR2X1_RVT \U1/U270  ( .A1(\U1/n192 ), .A2(inst_A[27]), .Y(\U1/n100 ) );
  XOR2X1_RVT \U1/U269  ( .A1(inst_ADD_SUB), .A2(inst_B[26]), .Y(\U1/n194 ) );
  OR2X1_RVT \U1/U268  ( .A1(\U1/n194 ), .A2(inst_A[26]), .Y(\U1/n98 ) );
  XOR2X1_RVT \U1/U267  ( .A1(inst_ADD_SUB), .A2(inst_B[25]), .Y(\U1/n195 ) );
  OR2X1_RVT \U1/U266  ( .A1(\U1/n195 ), .A2(inst_A[25]), .Y(\U1/n103 ) );
  XOR2X1_RVT \U1/U265  ( .A1(inst_ADD_SUB), .A2(inst_B[24]), .Y(\U1/n196 ) );
  OR2X1_RVT \U1/U264  ( .A1(\U1/n196 ), .A2(inst_A[24]), .Y(\U1/n105 ) );
  AND4X1_RVT \U1/U263  ( .A1(\U1/n100 ), .A2(\U1/n98 ), .A3(\U1/n103 ), .A4(
        \U1/n105 ), .Y(\U1/n90 ) );
  XOR2X1_RVT \U1/U262  ( .A1(inst_ADD_SUB), .A2(inst_B[23]), .Y(\U1/n198 ) );
  OR2X1_RVT \U1/U261  ( .A1(\U1/n198 ), .A2(inst_A[23]), .Y(\U1/n118 ) );
  XOR2X1_RVT \U1/U260  ( .A1(inst_ADD_SUB), .A2(inst_B[22]), .Y(\U1/n200 ) );
  OR2X1_RVT \U1/U259  ( .A1(\U1/n200 ), .A2(inst_A[22]), .Y(\U1/n116 ) );
  XOR2X1_RVT \U1/U258  ( .A1(inst_ADD_SUB), .A2(inst_B[21]), .Y(\U1/n201 ) );
  OR2X1_RVT \U1/U257  ( .A1(\U1/n201 ), .A2(inst_A[21]), .Y(\U1/n121 ) );
  XOR2X1_RVT \U1/U256  ( .A1(inst_ADD_SUB), .A2(inst_B[20]), .Y(\U1/n202 ) );
  OR2X1_RVT \U1/U255  ( .A1(\U1/n202 ), .A2(inst_A[20]), .Y(\U1/n123 ) );
  AND4X1_RVT \U1/U254  ( .A1(\U1/n118 ), .A2(\U1/n116 ), .A3(\U1/n121 ), .A4(
        \U1/n123 ), .Y(\U1/n108 ) );
  XOR2X1_RVT \U1/U253  ( .A1(inst_ADD_SUB), .A2(inst_B[16]), .Y(\U1/n232 ) );
  NAND2X0_RVT \U1/U252  ( .A1(inst_A[16]), .A2(\U1/n232 ), .Y(\U1/n148 ) );
  XOR2X1_RVT \U1/U251  ( .A1(inst_ADD_SUB), .A2(inst_B[17]), .Y(\U1/n237 ) );
  OR2X1_RVT \U1/U250  ( .A1(\U1/n237 ), .A2(inst_A[17]), .Y(\U1/n142 ) );
  NAND2X0_RVT \U1/U249  ( .A1(inst_A[17]), .A2(\U1/n237 ), .Y(\U1/n146 ) );
  AO21X1_RVT \U1/U248  ( .A1(\U1/n20 ), .A2(\U1/n142 ), .A3(\U1/n19 ), .Y(
        \U1/n235 ) );
  XOR2X1_RVT \U1/U247  ( .A1(inst_ADD_SUB), .A2(inst_B[18]), .Y(\U1/n236 ) );
  OR2X1_RVT \U1/U246  ( .A1(\U1/n236 ), .A2(inst_A[18]), .Y(\U1/n137 ) );
  NAND2X0_RVT \U1/U245  ( .A1(inst_A[18]), .A2(\U1/n236 ), .Y(\U1/n136 ) );
  AO21X1_RVT \U1/U244  ( .A1(\U1/n235 ), .A2(\U1/n137 ), .A3(\U1/n17 ), .Y(
        \U1/n233 ) );
  XOR2X1_RVT \U1/U243  ( .A1(inst_ADD_SUB), .A2(inst_B[19]), .Y(\U1/n234 ) );
  OR2X1_RVT \U1/U242  ( .A1(\U1/n234 ), .A2(inst_A[19]), .Y(\U1/n139 ) );
  NAND2X0_RVT \U1/U241  ( .A1(inst_A[19]), .A2(\U1/n234 ), .Y(\U1/n138 ) );
  AO21X1_RVT \U1/U240  ( .A1(\U1/n233 ), .A2(\U1/n139 ), .A3(\U1/n16 ), .Y(
        \U1/n126 ) );
  OR2X1_RVT \U1/U239  ( .A1(\U1/n232 ), .A2(inst_A[16]), .Y(\U1/n144 ) );
  XOR2X1_RVT \U1/U238  ( .A1(inst_ADD_SUB), .A2(inst_B[11]), .Y(\U1/n214 ) );
  OR2X1_RVT \U1/U237  ( .A1(\U1/n214 ), .A2(inst_A[11]), .Y(\U1/n173 ) );
  XOR2X1_RVT \U1/U236  ( .A1(inst_ADD_SUB), .A2(inst_B[10]), .Y(\U1/n216 ) );
  OR2X1_RVT \U1/U235  ( .A1(\U1/n216 ), .A2(inst_A[10]), .Y(\U1/n171 ) );
  XOR2X1_RVT \U1/U234  ( .A1(inst_ADD_SUB), .A2(inst_B[9]), .Y(\U1/n217 ) );
  OR2X1_RVT \U1/U233  ( .A1(\U1/n217 ), .A2(inst_A[9]), .Y(\U1/n43 ) );
  XOR2X1_RVT \U1/U232  ( .A1(inst_ADD_SUB), .A2(inst_B[8]), .Y(\U1/n218 ) );
  OR2X1_RVT \U1/U231  ( .A1(\U1/n218 ), .A2(inst_A[8]), .Y(\U1/n47 ) );
  AND4X1_RVT \U1/U230  ( .A1(\U1/n173 ), .A2(\U1/n171 ), .A3(\U1/n43 ), .A4(
        \U1/n47 ), .Y(\U1/n164 ) );
  XOR2X1_RVT \U1/U229  ( .A1(inst_ADD_SUB), .A2(inst_B[7]), .Y(\U1/n220 ) );
  OR2X1_RVT \U1/U228  ( .A1(\U1/n220 ), .A2(inst_A[7]), .Y(\U1/n55 ) );
  XOR2X1_RVT \U1/U227  ( .A1(inst_ADD_SUB), .A2(inst_B[6]), .Y(\U1/n222 ) );
  OR2X1_RVT \U1/U226  ( .A1(\U1/n222 ), .A2(inst_A[6]), .Y(\U1/n53 ) );
  XOR2X1_RVT \U1/U225  ( .A1(inst_ADD_SUB), .A2(inst_B[5]), .Y(\U1/n223 ) );
  OR2X1_RVT \U1/U224  ( .A1(\U1/n223 ), .A2(inst_A[5]), .Y(\U1/n58 ) );
  XOR2X1_RVT \U1/U223  ( .A1(inst_ADD_SUB), .A2(inst_B[4]), .Y(\U1/n224 ) );
  OR2X1_RVT \U1/U222  ( .A1(\U1/n224 ), .A2(inst_A[4]), .Y(\U1/n61 ) );
  AND4X1_RVT \U1/U221  ( .A1(\U1/n55 ), .A2(\U1/n53 ), .A3(\U1/n58 ), .A4(
        \U1/n61 ), .Y(\U1/n175 ) );
  XOR2X1_RVT \U1/U220  ( .A1(inst_ADD_SUB), .A2(inst_B[0]), .Y(\U1/n226 ) );
  NAND2X0_RVT \U1/U219  ( .A1(inst_A[0]), .A2(\U1/n226 ), .Y(\U1/n180 ) );
  XOR2X1_RVT \U1/U218  ( .A1(inst_ADD_SUB), .A2(inst_B[1]), .Y(\U1/n231 ) );
  OR2X1_RVT \U1/U217  ( .A1(\U1/n231 ), .A2(inst_A[1]), .Y(\U1/n84 ) );
  NAND2X0_RVT \U1/U216  ( .A1(inst_A[1]), .A2(\U1/n231 ), .Y(\U1/n132 ) );
  AO21X1_RVT \U1/U215  ( .A1(\U1/n40 ), .A2(\U1/n84 ), .A3(\U1/n39 ), .Y(
        \U1/n229 ) );
  XOR2X1_RVT \U1/U214  ( .A1(inst_ADD_SUB), .A2(inst_B[2]), .Y(\U1/n230 ) );
  OR2X1_RVT \U1/U213  ( .A1(\U1/n230 ), .A2(inst_A[2]), .Y(\U1/n69 ) );
  NAND2X0_RVT \U1/U212  ( .A1(inst_A[2]), .A2(\U1/n230 ), .Y(\U1/n68 ) );
  AO21X1_RVT \U1/U211  ( .A1(\U1/n229 ), .A2(\U1/n69 ), .A3(\U1/n37 ), .Y(
        \U1/n227 ) );
  XOR2X1_RVT \U1/U210  ( .A1(inst_ADD_SUB), .A2(inst_B[3]), .Y(\U1/n228 ) );
  OR2X1_RVT \U1/U209  ( .A1(\U1/n228 ), .A2(inst_A[3]), .Y(\U1/n71 ) );
  NAND2X0_RVT \U1/U208  ( .A1(inst_A[3]), .A2(\U1/n228 ), .Y(\U1/n70 ) );
  AO21X1_RVT \U1/U207  ( .A1(\U1/n227 ), .A2(\U1/n71 ), .A3(\U1/n36 ), .Y(
        \U1/n177 ) );
  XOR2X1_RVT \U1/U206  ( .A1(inst_ADD_SUB), .A2(inst_CI), .Y(\U1/n130 ) );
  OR2X1_RVT \U1/U205  ( .A1(\U1/n226 ), .A2(inst_A[0]), .Y(\U1/n131 ) );
  AND2X1_RVT \U1/U204  ( .A1(\U1/n130 ), .A2(\U1/n131 ), .Y(\U1/n225 ) );
  AND4X1_RVT \U1/U203  ( .A1(\U1/n69 ), .A2(\U1/n71 ), .A3(\U1/n84 ), .A4(
        \U1/n225 ), .Y(\U1/n178 ) );
  NAND2X0_RVT \U1/U202  ( .A1(inst_A[4]), .A2(\U1/n224 ), .Y(\U1/n64 ) );
  NAND2X0_RVT \U1/U201  ( .A1(inst_A[5]), .A2(\U1/n223 ), .Y(\U1/n62 ) );
  AO21X1_RVT \U1/U200  ( .A1(\U1/n35 ), .A2(\U1/n58 ), .A3(\U1/n34 ), .Y(
        \U1/n221 ) );
  NAND2X0_RVT \U1/U199  ( .A1(inst_A[6]), .A2(\U1/n222 ), .Y(\U1/n52 ) );
  AO21X1_RVT \U1/U198  ( .A1(\U1/n221 ), .A2(\U1/n53 ), .A3(\U1/n32 ), .Y(
        \U1/n219 ) );
  NAND2X0_RVT \U1/U197  ( .A1(inst_A[7]), .A2(\U1/n220 ), .Y(\U1/n54 ) );
  AO21X1_RVT \U1/U196  ( .A1(\U1/n219 ), .A2(\U1/n55 ), .A3(\U1/n31 ), .Y(
        \U1/n176 ) );
  AO221X1_RVT \U1/U195  ( .A1(\U1/n175 ), .A2(\U1/n177 ), .A3(\U1/n178 ), .A4(
        \U1/n175 ), .A5(\U1/n176 ), .Y(\U1/n212 ) );
  NAND2X0_RVT \U1/U194  ( .A1(inst_A[8]), .A2(\U1/n218 ), .Y(\U1/n48 ) );
  NAND2X0_RVT \U1/U193  ( .A1(inst_A[9]), .A2(\U1/n217 ), .Y(\U1/n44 ) );
  AO21X1_RVT \U1/U192  ( .A1(\U1/n30 ), .A2(\U1/n43 ), .A3(\U1/n29 ), .Y(
        \U1/n215 ) );
  NAND2X0_RVT \U1/U191  ( .A1(inst_A[10]), .A2(\U1/n216 ), .Y(\U1/n170 ) );
  AO21X1_RVT \U1/U190  ( .A1(\U1/n215 ), .A2(\U1/n171 ), .A3(\U1/n27 ), .Y(
        \U1/n213 ) );
  NAND2X0_RVT \U1/U189  ( .A1(inst_A[11]), .A2(\U1/n214 ), .Y(\U1/n172 ) );
  AO21X1_RVT \U1/U188  ( .A1(\U1/n213 ), .A2(\U1/n173 ), .A3(\U1/n26 ), .Y(
        \U1/n165 ) );
  AO21X1_RVT \U1/U187  ( .A1(\U1/n164 ), .A2(\U1/n212 ), .A3(\U1/n165 ), .Y(
        \U1/n210 ) );
  XOR2X1_RVT \U1/U186  ( .A1(inst_ADD_SUB), .A2(inst_B[12]), .Y(\U1/n211 ) );
  OR2X1_RVT \U1/U185  ( .A1(\U1/n211 ), .A2(inst_A[12]), .Y(\U1/n161 ) );
  NAND2X0_RVT \U1/U184  ( .A1(inst_A[12]), .A2(\U1/n211 ), .Y(\U1/n166 ) );
  AO21X1_RVT \U1/U183  ( .A1(\U1/n210 ), .A2(\U1/n161 ), .A3(\U1/n25 ), .Y(
        \U1/n208 ) );
  XOR2X1_RVT \U1/U182  ( .A1(inst_ADD_SUB), .A2(inst_B[13]), .Y(\U1/n209 ) );
  OR2X1_RVT \U1/U181  ( .A1(\U1/n209 ), .A2(inst_A[13]), .Y(\U1/n158 ) );
  NAND2X0_RVT \U1/U180  ( .A1(inst_A[13]), .A2(\U1/n209 ), .Y(\U1/n162 ) );
  AO21X1_RVT \U1/U179  ( .A1(\U1/n208 ), .A2(\U1/n158 ), .A3(\U1/n24 ), .Y(
        \U1/n206 ) );
  XOR2X1_RVT \U1/U178  ( .A1(inst_ADD_SUB), .A2(inst_B[14]), .Y(\U1/n207 ) );
  OR2X1_RVT \U1/U177  ( .A1(\U1/n207 ), .A2(inst_A[14]), .Y(\U1/n153 ) );
  NAND2X0_RVT \U1/U176  ( .A1(inst_A[14]), .A2(\U1/n207 ), .Y(\U1/n152 ) );
  AO21X1_RVT \U1/U175  ( .A1(\U1/n206 ), .A2(\U1/n153 ), .A3(\U1/n22 ), .Y(
        \U1/n204 ) );
  XOR2X1_RVT \U1/U174  ( .A1(inst_ADD_SUB), .A2(inst_B[15]), .Y(\U1/n205 ) );
  OR2X1_RVT \U1/U173  ( .A1(\U1/n205 ), .A2(inst_A[15]), .Y(\U1/n155 ) );
  NAND2X0_RVT \U1/U172  ( .A1(inst_A[15]), .A2(\U1/n205 ), .Y(\U1/n154 ) );
  AO21X1_RVT \U1/U171  ( .A1(\U1/n204 ), .A2(\U1/n155 ), .A3(\U1/n21 ), .Y(
        \U1/n145 ) );
  AND3X1_RVT \U1/U170  ( .A1(\U1/n137 ), .A2(\U1/n139 ), .A3(\U1/n142 ), .Y(
        \U1/n203 ) );
  AND3X1_RVT \U1/U169  ( .A1(\U1/n144 ), .A2(\U1/n145 ), .A3(\U1/n203 ), .Y(
        \U1/n127 ) );
  NAND2X0_RVT \U1/U168  ( .A1(inst_A[20]), .A2(\U1/n202 ), .Y(\U1/n128 ) );
  NAND2X0_RVT \U1/U167  ( .A1(inst_A[21]), .A2(\U1/n201 ), .Y(\U1/n124 ) );
  AO21X1_RVT \U1/U166  ( .A1(\U1/n15 ), .A2(\U1/n121 ), .A3(\U1/n14 ), .Y(
        \U1/n199 ) );
  NAND2X0_RVT \U1/U165  ( .A1(inst_A[22]), .A2(\U1/n200 ), .Y(\U1/n115 ) );
  AO21X1_RVT \U1/U164  ( .A1(\U1/n199 ), .A2(\U1/n116 ), .A3(\U1/n12 ), .Y(
        \U1/n197 ) );
  NAND2X0_RVT \U1/U163  ( .A1(inst_A[23]), .A2(\U1/n198 ), .Y(\U1/n117 ) );
  AO21X1_RVT \U1/U162  ( .A1(\U1/n197 ), .A2(\U1/n118 ), .A3(\U1/n11 ), .Y(
        \U1/n110 ) );
  AO221X1_RVT \U1/U161  ( .A1(\U1/n108 ), .A2(\U1/n126 ), .A3(\U1/n127 ), .A4(
        \U1/n108 ), .A5(\U1/n110 ), .Y(\U1/n190 ) );
  NAND2X0_RVT \U1/U160  ( .A1(inst_A[24]), .A2(\U1/n196 ), .Y(\U1/n111 ) );
  NAND2X0_RVT \U1/U159  ( .A1(inst_A[25]), .A2(\U1/n195 ), .Y(\U1/n106 ) );
  AO21X1_RVT \U1/U158  ( .A1(\U1/n10 ), .A2(\U1/n103 ), .A3(\U1/n9 ), .Y(
        \U1/n193 ) );
  NAND2X0_RVT \U1/U157  ( .A1(inst_A[26]), .A2(\U1/n194 ), .Y(\U1/n97 ) );
  AO21X1_RVT \U1/U156  ( .A1(\U1/n193 ), .A2(\U1/n98 ), .A3(\U1/n7 ), .Y(
        \U1/n191 ) );
  NAND2X0_RVT \U1/U155  ( .A1(inst_A[27]), .A2(\U1/n192 ), .Y(\U1/n99 ) );
  AO21X1_RVT \U1/U154  ( .A1(\U1/n191 ), .A2(\U1/n100 ), .A3(\U1/n6 ), .Y(
        \U1/n92 ) );
  AO21X1_RVT \U1/U153  ( .A1(\U1/n90 ), .A2(\U1/n190 ), .A3(\U1/n92 ), .Y(
        \U1/n188 ) );
  XOR2X1_RVT \U1/U152  ( .A1(inst_ADD_SUB), .A2(inst_B[28]), .Y(\U1/n189 ) );
  OR2X1_RVT \U1/U151  ( .A1(\U1/n189 ), .A2(inst_A[28]), .Y(\U1/n87 ) );
  NAND2X0_RVT \U1/U150  ( .A1(inst_A[28]), .A2(\U1/n189 ), .Y(\U1/n93 ) );
  AO21X1_RVT \U1/U149  ( .A1(\U1/n188 ), .A2(\U1/n87 ), .A3(\U1/n5 ), .Y(
        \U1/n186 ) );
  XOR2X1_RVT \U1/U148  ( .A1(inst_ADD_SUB), .A2(inst_B[29]), .Y(\U1/n187 ) );
  OR2X1_RVT \U1/U147  ( .A1(\U1/n187 ), .A2(inst_A[29]), .Y(\U1/n81 ) );
  NAND2X0_RVT \U1/U146  ( .A1(inst_A[29]), .A2(\U1/n187 ), .Y(\U1/n88 ) );
  AO21X1_RVT \U1/U145  ( .A1(\U1/n186 ), .A2(\U1/n81 ), .A3(\U1/n4 ), .Y(
        \U1/n184 ) );
  XOR2X1_RVT \U1/U144  ( .A1(inst_ADD_SUB), .A2(inst_B[30]), .Y(\U1/n185 ) );
  OR2X1_RVT \U1/U143  ( .A1(\U1/n185 ), .A2(inst_A[30]), .Y(\U1/n76 ) );
  NAND2X0_RVT \U1/U142  ( .A1(inst_A[30]), .A2(\U1/n185 ), .Y(\U1/n75 ) );
  AO21X1_RVT \U1/U141  ( .A1(\U1/n184 ), .A2(\U1/n76 ), .A3(\U1/n2 ), .Y(
        \U1/n182 ) );
  XOR2X1_RVT \U1/U140  ( .A1(inst_ADD_SUB), .A2(inst_B[31]), .Y(\U1/n183 ) );
  OR2X1_RVT \U1/U139  ( .A1(\U1/n183 ), .A2(inst_A[31]), .Y(\U1/n78 ) );
  NAND2X0_RVT \U1/U138  ( .A1(inst_A[31]), .A2(\U1/n183 ), .Y(\U1/n77 ) );
  AO21X1_RVT \U1/U137  ( .A1(\U1/n182 ), .A2(\U1/n78 ), .A3(\U1/n1 ), .Y(
        \U1/n181 ) );
  XOR2X1_RVT \U1/U136  ( .A1(\U1/n181 ), .A2(inst_ADD_SUB), .Y(CO_inst) );
  NAND2X0_RVT \U1/U135  ( .A1(\U1/n131 ), .A2(\U1/n180 ), .Y(\U1/n179 ) );
  XNOR2X1_RVT \U1/U134  ( .A1(\U1/n179 ), .A2(\U1/n130 ), .Y(SUM_inst[0]) );
  NAND2X0_RVT \U1/U133  ( .A1(\U1/n171 ), .A2(\U1/n170 ), .Y(\U1/n174 ) );
  OR2X1_RVT \U1/U132  ( .A1(\U1/n177 ), .A2(\U1/n178 ), .Y(\U1/n60 ) );
  AO21X1_RVT \U1/U131  ( .A1(\U1/n175 ), .A2(\U1/n60 ), .A3(\U1/n176 ), .Y(
        \U1/n46 ) );
  AO21X1_RVT \U1/U130  ( .A1(\U1/n46 ), .A2(\U1/n47 ), .A3(\U1/n30 ), .Y(
        \U1/n42 ) );
  AOI21X1_RVT \U1/U129  ( .A1(\U1/n43 ), .A2(\U1/n42 ), .A3(\U1/n29 ), .Y(
        \U1/n169 ) );
  XOR2X1_RVT \U1/U128  ( .A1(\U1/n174 ), .A2(\U1/n169 ), .Y(SUM_inst[10]) );
  NAND2X0_RVT \U1/U127  ( .A1(\U1/n172 ), .A2(\U1/n173 ), .Y(\U1/n167 ) );
  OA21X1_RVT \U1/U126  ( .A1(\U1/n28 ), .A2(\U1/n169 ), .A3(\U1/n170 ), .Y(
        \U1/n168 ) );
  XOR2X1_RVT \U1/U125  ( .A1(\U1/n167 ), .A2(\U1/n168 ), .Y(SUM_inst[11]) );
  NAND2X0_RVT \U1/U124  ( .A1(\U1/n161 ), .A2(\U1/n166 ), .Y(\U1/n163 ) );
  AO21X1_RVT \U1/U123  ( .A1(\U1/n164 ), .A2(\U1/n46 ), .A3(\U1/n165 ), .Y(
        \U1/n160 ) );
  XNOR2X1_RVT \U1/U122  ( .A1(\U1/n163 ), .A2(\U1/n160 ), .Y(SUM_inst[12]) );
  NAND2X0_RVT \U1/U121  ( .A1(\U1/n158 ), .A2(\U1/n162 ), .Y(\U1/n159 ) );
  AO21X1_RVT \U1/U120  ( .A1(\U1/n160 ), .A2(\U1/n161 ), .A3(\U1/n25 ), .Y(
        \U1/n157 ) );
  XNOR2X1_RVT \U1/U119  ( .A1(\U1/n159 ), .A2(\U1/n157 ), .Y(SUM_inst[13]) );
  NAND2X0_RVT \U1/U118  ( .A1(\U1/n153 ), .A2(\U1/n152 ), .Y(\U1/n156 ) );
  AOI21X1_RVT \U1/U117  ( .A1(\U1/n157 ), .A2(\U1/n158 ), .A3(\U1/n24 ), .Y(
        \U1/n151 ) );
  XOR2X1_RVT \U1/U116  ( .A1(\U1/n156 ), .A2(\U1/n151 ), .Y(SUM_inst[14]) );
  NAND2X0_RVT \U1/U115  ( .A1(\U1/n154 ), .A2(\U1/n155 ), .Y(\U1/n149 ) );
  OA21X1_RVT \U1/U114  ( .A1(\U1/n23 ), .A2(\U1/n151 ), .A3(\U1/n152 ), .Y(
        \U1/n150 ) );
  XOR2X1_RVT \U1/U113  ( .A1(\U1/n149 ), .A2(\U1/n150 ), .Y(SUM_inst[15]) );
  NAND2X0_RVT \U1/U112  ( .A1(\U1/n144 ), .A2(\U1/n148 ), .Y(\U1/n147 ) );
  XNOR2X1_RVT \U1/U111  ( .A1(\U1/n147 ), .A2(\U1/n145 ), .Y(SUM_inst[16]) );
  NAND2X0_RVT \U1/U110  ( .A1(\U1/n142 ), .A2(\U1/n146 ), .Y(\U1/n143 ) );
  AO21X1_RVT \U1/U109  ( .A1(\U1/n144 ), .A2(\U1/n145 ), .A3(\U1/n20 ), .Y(
        \U1/n141 ) );
  XNOR2X1_RVT \U1/U108  ( .A1(\U1/n143 ), .A2(\U1/n141 ), .Y(SUM_inst[17]) );
  NAND2X0_RVT \U1/U107  ( .A1(\U1/n137 ), .A2(\U1/n136 ), .Y(\U1/n140 ) );
  AOI21X1_RVT \U1/U106  ( .A1(\U1/n141 ), .A2(\U1/n142 ), .A3(\U1/n19 ), .Y(
        \U1/n135 ) );
  XOR2X1_RVT \U1/U105  ( .A1(\U1/n140 ), .A2(\U1/n135 ), .Y(SUM_inst[18]) );
  NAND2X0_RVT \U1/U104  ( .A1(\U1/n138 ), .A2(\U1/n139 ), .Y(\U1/n133 ) );
  OA21X1_RVT \U1/U103  ( .A1(\U1/n18 ), .A2(\U1/n135 ), .A3(\U1/n136 ), .Y(
        \U1/n134 ) );
  XOR2X1_RVT \U1/U102  ( .A1(\U1/n133 ), .A2(\U1/n134 ), .Y(SUM_inst[19]) );
  NAND2X0_RVT \U1/U101  ( .A1(\U1/n84 ), .A2(\U1/n132 ), .Y(\U1/n129 ) );
  AO21X1_RVT \U1/U100  ( .A1(\U1/n130 ), .A2(\U1/n131 ), .A3(\U1/n40 ), .Y(
        \U1/n83 ) );
  XNOR2X1_RVT \U1/U99  ( .A1(\U1/n129 ), .A2(\U1/n83 ), .Y(SUM_inst[1]) );
  NAND2X0_RVT \U1/U98  ( .A1(\U1/n123 ), .A2(\U1/n128 ), .Y(\U1/n125 ) );
  OR2X1_RVT \U1/U97  ( .A1(\U1/n126 ), .A2(\U1/n127 ), .Y(\U1/n109 ) );
  XNOR2X1_RVT \U1/U96  ( .A1(\U1/n125 ), .A2(\U1/n109 ), .Y(SUM_inst[20]) );
  NAND2X0_RVT \U1/U95  ( .A1(\U1/n121 ), .A2(\U1/n124 ), .Y(\U1/n122 ) );
  AO21X1_RVT \U1/U94  ( .A1(\U1/n109 ), .A2(\U1/n123 ), .A3(\U1/n15 ), .Y(
        \U1/n120 ) );
  XNOR2X1_RVT \U1/U93  ( .A1(\U1/n122 ), .A2(\U1/n120 ), .Y(SUM_inst[21]) );
  NAND2X0_RVT \U1/U92  ( .A1(\U1/n116 ), .A2(\U1/n115 ), .Y(\U1/n119 ) );
  AOI21X1_RVT \U1/U91  ( .A1(\U1/n120 ), .A2(\U1/n121 ), .A3(\U1/n14 ), .Y(
        \U1/n114 ) );
  XOR2X1_RVT \U1/U90  ( .A1(\U1/n119 ), .A2(\U1/n114 ), .Y(SUM_inst[22]) );
  NAND2X0_RVT \U1/U89  ( .A1(\U1/n117 ), .A2(\U1/n118 ), .Y(\U1/n112 ) );
  OA21X1_RVT \U1/U88  ( .A1(\U1/n13 ), .A2(\U1/n114 ), .A3(\U1/n115 ), .Y(
        \U1/n113 ) );
  XOR2X1_RVT \U1/U87  ( .A1(\U1/n112 ), .A2(\U1/n113 ), .Y(SUM_inst[23]) );
  NAND2X0_RVT \U1/U86  ( .A1(\U1/n105 ), .A2(\U1/n111 ), .Y(\U1/n107 ) );
  AO21X1_RVT \U1/U85  ( .A1(\U1/n108 ), .A2(\U1/n109 ), .A3(\U1/n110 ), .Y(
        \U1/n91 ) );
  XNOR2X1_RVT \U1/U84  ( .A1(\U1/n107 ), .A2(\U1/n91 ), .Y(SUM_inst[24]) );
  NAND2X0_RVT \U1/U83  ( .A1(\U1/n103 ), .A2(\U1/n106 ), .Y(\U1/n104 ) );
  AO21X1_RVT \U1/U82  ( .A1(\U1/n91 ), .A2(\U1/n105 ), .A3(\U1/n10 ), .Y(
        \U1/n102 ) );
  XNOR2X1_RVT \U1/U81  ( .A1(\U1/n104 ), .A2(\U1/n102 ), .Y(SUM_inst[25]) );
  NAND2X0_RVT \U1/U80  ( .A1(\U1/n98 ), .A2(\U1/n97 ), .Y(\U1/n101 ) );
  AOI21X1_RVT \U1/U79  ( .A1(\U1/n102 ), .A2(\U1/n103 ), .A3(\U1/n9 ), .Y(
        \U1/n96 ) );
  XOR2X1_RVT \U1/U78  ( .A1(\U1/n101 ), .A2(\U1/n96 ), .Y(SUM_inst[26]) );
  NAND2X0_RVT \U1/U77  ( .A1(\U1/n99 ), .A2(\U1/n100 ), .Y(\U1/n94 ) );
  OA21X1_RVT \U1/U76  ( .A1(\U1/n8 ), .A2(\U1/n96 ), .A3(\U1/n97 ), .Y(
        \U1/n95 ) );
  XOR2X1_RVT \U1/U75  ( .A1(\U1/n94 ), .A2(\U1/n95 ), .Y(SUM_inst[27]) );
  NAND2X0_RVT \U1/U74  ( .A1(\U1/n87 ), .A2(\U1/n93 ), .Y(\U1/n89 ) );
  AO21X1_RVT \U1/U73  ( .A1(\U1/n90 ), .A2(\U1/n91 ), .A3(\U1/n92 ), .Y(
        \U1/n86 ) );
  XNOR2X1_RVT \U1/U72  ( .A1(\U1/n89 ), .A2(\U1/n86 ), .Y(SUM_inst[28]) );
  NAND2X0_RVT \U1/U71  ( .A1(\U1/n81 ), .A2(\U1/n88 ), .Y(\U1/n85 ) );
  AO21X1_RVT \U1/U70  ( .A1(\U1/n86 ), .A2(\U1/n87 ), .A3(\U1/n5 ), .Y(
        \U1/n80 ) );
  XNOR2X1_RVT \U1/U69  ( .A1(\U1/n85 ), .A2(\U1/n80 ), .Y(SUM_inst[29]) );
  NAND2X0_RVT \U1/U68  ( .A1(\U1/n69 ), .A2(\U1/n68 ), .Y(\U1/n82 ) );
  AOI21X1_RVT \U1/U67  ( .A1(\U1/n83 ), .A2(\U1/n84 ), .A3(\U1/n39 ), .Y(
        \U1/n67 ) );
  XOR2X1_RVT \U1/U66  ( .A1(\U1/n82 ), .A2(\U1/n67 ), .Y(SUM_inst[2]) );
  NAND2X0_RVT \U1/U65  ( .A1(\U1/n76 ), .A2(\U1/n75 ), .Y(\U1/n79 ) );
  AOI21X1_RVT \U1/U64  ( .A1(\U1/n80 ), .A2(\U1/n81 ), .A3(\U1/n4 ), .Y(
        \U1/n74 ) );
  XOR2X1_RVT \U1/U63  ( .A1(\U1/n79 ), .A2(\U1/n74 ), .Y(SUM_inst[30]) );
  NAND2X0_RVT \U1/U62  ( .A1(\U1/n77 ), .A2(\U1/n78 ), .Y(\U1/n72 ) );
  OA21X1_RVT \U1/U61  ( .A1(\U1/n3 ), .A2(\U1/n74 ), .A3(\U1/n75 ), .Y(
        \U1/n73 ) );
  XOR2X1_RVT \U1/U60  ( .A1(\U1/n72 ), .A2(\U1/n73 ), .Y(SUM_inst[31]) );
  NAND2X0_RVT \U1/U59  ( .A1(\U1/n70 ), .A2(\U1/n71 ), .Y(\U1/n65 ) );
  OA21X1_RVT \U1/U58  ( .A1(\U1/n38 ), .A2(\U1/n67 ), .A3(\U1/n68 ), .Y(
        \U1/n66 ) );
  XOR2X1_RVT \U1/U57  ( .A1(\U1/n65 ), .A2(\U1/n66 ), .Y(SUM_inst[3]) );
  NAND2X0_RVT \U1/U56  ( .A1(\U1/n61 ), .A2(\U1/n64 ), .Y(\U1/n63 ) );
  XNOR2X1_RVT \U1/U55  ( .A1(\U1/n63 ), .A2(\U1/n60 ), .Y(SUM_inst[4]) );
  NAND2X0_RVT \U1/U54  ( .A1(\U1/n58 ), .A2(\U1/n62 ), .Y(\U1/n59 ) );
  AO21X1_RVT \U1/U53  ( .A1(\U1/n60 ), .A2(\U1/n61 ), .A3(\U1/n35 ), .Y(
        \U1/n57 ) );
  XNOR2X1_RVT \U1/U52  ( .A1(\U1/n59 ), .A2(\U1/n57 ), .Y(SUM_inst[5]) );
  NAND2X0_RVT \U1/U51  ( .A1(\U1/n53 ), .A2(\U1/n52 ), .Y(\U1/n56 ) );
  AOI21X1_RVT \U1/U50  ( .A1(\U1/n57 ), .A2(\U1/n58 ), .A3(\U1/n34 ), .Y(
        \U1/n51 ) );
  XOR2X1_RVT \U1/U49  ( .A1(\U1/n56 ), .A2(\U1/n51 ), .Y(SUM_inst[6]) );
  NAND2X0_RVT \U1/U48  ( .A1(\U1/n54 ), .A2(\U1/n55 ), .Y(\U1/n49 ) );
  OA21X1_RVT \U1/U47  ( .A1(\U1/n33 ), .A2(\U1/n51 ), .A3(\U1/n52 ), .Y(
        \U1/n50 ) );
  XOR2X1_RVT \U1/U46  ( .A1(\U1/n49 ), .A2(\U1/n50 ), .Y(SUM_inst[7]) );
  NAND2X0_RVT \U1/U45  ( .A1(\U1/n47 ), .A2(\U1/n48 ), .Y(\U1/n45 ) );
  XNOR2X1_RVT \U1/U44  ( .A1(\U1/n45 ), .A2(\U1/n46 ), .Y(SUM_inst[8]) );
  NAND2X0_RVT \U1/U43  ( .A1(\U1/n43 ), .A2(\U1/n44 ), .Y(\U1/n41 ) );
  XNOR2X1_RVT \U1/U42  ( .A1(\U1/n41 ), .A2(\U1/n42 ), .Y(SUM_inst[9]) );
  INVX1_RVT \U1/U41  ( .A(\U1/n180 ), .Y(\U1/n40 ) );
  INVX1_RVT \U1/U40  ( .A(\U1/n132 ), .Y(\U1/n39 ) );
  INVX1_RVT \U1/U39  ( .A(\U1/n69 ), .Y(\U1/n38 ) );
  INVX1_RVT \U1/U38  ( .A(\U1/n68 ), .Y(\U1/n37 ) );
  INVX1_RVT \U1/U37  ( .A(\U1/n70 ), .Y(\U1/n36 ) );
  INVX1_RVT \U1/U36  ( .A(\U1/n64 ), .Y(\U1/n35 ) );
  INVX1_RVT \U1/U35  ( .A(\U1/n62 ), .Y(\U1/n34 ) );
  INVX1_RVT \U1/U34  ( .A(\U1/n53 ), .Y(\U1/n33 ) );
  INVX1_RVT \U1/U33  ( .A(\U1/n52 ), .Y(\U1/n32 ) );
  INVX1_RVT \U1/U32  ( .A(\U1/n54 ), .Y(\U1/n31 ) );
  INVX1_RVT \U1/U31  ( .A(\U1/n48 ), .Y(\U1/n30 ) );
  INVX1_RVT \U1/U30  ( .A(\U1/n44 ), .Y(\U1/n29 ) );
  INVX1_RVT \U1/U29  ( .A(\U1/n171 ), .Y(\U1/n28 ) );
  INVX1_RVT \U1/U28  ( .A(\U1/n170 ), .Y(\U1/n27 ) );
  INVX1_RVT \U1/U27  ( .A(\U1/n172 ), .Y(\U1/n26 ) );
  INVX1_RVT \U1/U26  ( .A(\U1/n166 ), .Y(\U1/n25 ) );
  INVX1_RVT \U1/U25  ( .A(\U1/n162 ), .Y(\U1/n24 ) );
  INVX1_RVT \U1/U24  ( .A(\U1/n153 ), .Y(\U1/n23 ) );
  INVX1_RVT \U1/U23  ( .A(\U1/n152 ), .Y(\U1/n22 ) );
  INVX1_RVT \U1/U22  ( .A(\U1/n154 ), .Y(\U1/n21 ) );
  INVX1_RVT \U1/U21  ( .A(\U1/n148 ), .Y(\U1/n20 ) );
  INVX1_RVT \U1/U20  ( .A(\U1/n146 ), .Y(\U1/n19 ) );
  INVX1_RVT \U1/U19  ( .A(\U1/n137 ), .Y(\U1/n18 ) );
  INVX1_RVT \U1/U18  ( .A(\U1/n136 ), .Y(\U1/n17 ) );
  INVX1_RVT \U1/U17  ( .A(\U1/n138 ), .Y(\U1/n16 ) );
  INVX1_RVT \U1/U16  ( .A(\U1/n128 ), .Y(\U1/n15 ) );
  INVX1_RVT \U1/U15  ( .A(\U1/n124 ), .Y(\U1/n14 ) );
  INVX1_RVT \U1/U14  ( .A(\U1/n116 ), .Y(\U1/n13 ) );
  INVX1_RVT \U1/U13  ( .A(\U1/n115 ), .Y(\U1/n12 ) );
  INVX1_RVT \U1/U12  ( .A(\U1/n117 ), .Y(\U1/n11 ) );
  INVX1_RVT \U1/U11  ( .A(\U1/n111 ), .Y(\U1/n10 ) );
  INVX1_RVT \U1/U10  ( .A(\U1/n106 ), .Y(\U1/n9 ) );
  INVX1_RVT \U1/U9  ( .A(\U1/n98 ), .Y(\U1/n8 ) );
  INVX1_RVT \U1/U8  ( .A(\U1/n97 ), .Y(\U1/n7 ) );
  INVX1_RVT \U1/U7  ( .A(\U1/n99 ), .Y(\U1/n6 ) );
  INVX1_RVT \U1/U6  ( .A(\U1/n93 ), .Y(\U1/n5 ) );
  INVX1_RVT \U1/U5  ( .A(\U1/n88 ), .Y(\U1/n4 ) );
  INVX1_RVT \U1/U4  ( .A(\U1/n76 ), .Y(\U1/n3 ) );
  INVX1_RVT \U1/U3  ( .A(\U1/n75 ), .Y(\U1/n2 ) );
  INVX1_RVT \U1/U2  ( .A(\U1/n77 ), .Y(\U1/n1 ) );
endmodule

