module nand_tree (
nx1,
nx7,
nx3,
nx2,
nx6,
nx23,
nx22);

// Start PIs
input nx1;
input nx7;
input nx3;
input nx2;
input nx6;

// Start POs
output nx23;
output nx22;

// Start wires
wire net_1;
wire nx23;
wire nx1;
wire nx7;
wire nx3;
wire net_2;
wire nx22;
wire nx6;
wire net_0;
wire net_3;
wire nx2;

// Start cells
NAND2X2_RVT inst_0 ( .Y(net_1), .A2(nx6), .A1(nx3) );
NAND2X2_RVT inst_1 ( .Y(net_0), .A2(nx3), .A1(nx1) );
NAND2X2_RVT inst_2 ( .Y(net_2), .A2(net_1), .A1(nx7) );
NAND2X2_RVT inst_3 ( .Y(net_3), .A2(net_1), .A1(nx2) );
NAND2X2_RVT inst_4 ( .A1(net_3), .A2(net_2), .Y(nx23) );
NAND2X2_RVT inst_5 ( .A2(net_3), .A1(net_0), .Y(nx22) );

endmodule
