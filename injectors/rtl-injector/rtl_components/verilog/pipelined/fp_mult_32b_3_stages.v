module DW_fp_mult_inst( inst_a, inst_b, inst_rnd, clk, z_inst, status_inst );

parameter sig_width = 23;
parameter exp_width = 8;
parameter ieee_compliance = 1;


input [sig_width+exp_width : 0] inst_a;
input [sig_width+exp_width : 0] inst_b;
input [2 : 0] inst_rnd;
input clk;

output [sig_width+exp_width : 0] z_inst;
output [7 : 0] status_inst;

wire [sig_width+exp_width : 0] z_temp;
wire [7 : 0] status_temp;

reg [sig_width+exp_width : 0] z_pipe;
reg [7 : 0] status_pipe;

reg [sig_width+exp_width : 0] z_pipe2;
reg [7 : 0] status_pipe2;

reg [sig_width+exp_width : 0] z_reg;
reg [7 : 0] status_reg;


    // Instance of DW_fp_mult
    DW_fp_mult #(sig_width, exp_width, ieee_compliance)
	  U1 ( .a(inst_a), .b(inst_b), .rnd(inst_rnd), .z(z_temp), .status(status_temp) );

assign z_inst = z_reg;
assign statug_inst = status_reg;

always @ (posedge clk)
begin
	z_pipe <= z_temp;
	status_pipe <= status_temp;

	z_pipe2 <= z_pipe;
	status_pipe2 <= status_pipe;

	z_reg <= z_pipe2;
	status_reg <= status_pipe2;
end


endmodule
