module DW_fp_addsub_inst( inst_a, inst_b, inst_rnd, inst_op, clk, z_inst, 
		status_inst );

parameter sig_width = 23;
parameter exp_width = 8;
parameter ieee_compliance = 1;


input [sig_width+exp_width : 0] inst_a;
input [sig_width+exp_width : 0] inst_b;
input [2 : 0] inst_rnd;
input inst_op;
input clk;

output [sig_width+exp_width : 0] z_inst;
output [7 : 0] status_inst;

reg [sig_width+exp_width : 0] a_pipe;
reg [sig_width+exp_width : 0] b_pipe;
reg [2 : 0] rnd_pipe;
reg op_pipe;

reg [sig_width+exp_width : 0] a_pipe2;
reg [sig_width+exp_width : 0] b_pipe2;
reg [2 : 0] rnd_pipe2;
reg op_pipe2;

wire [sig_width+exp_width : 0] z_temp;
wire [7 : 0] status_temp;

reg [sig_width+exp_width : 0] z_reg;
reg [7 : 0] status_reg;

    // Instance of DW_fp_addsub
    DW_fp_addsub #(sig_width, exp_width, ieee_compliance)
	  U1 ( .a(a_pipe2), .b(b_pipe2), .rnd(rnd_pipe2), 
               .op(op_pipe2), .z(z_temp), .status(status_temp) );

assign z_inst = z_reg;
assign statug_inst = status_reg;

always @ (posedge clk)
begin
        a_pipe   <= inst_a;
        b_pipe   <= inst_b;
        rnd_pipe <= inst_rnd;
        op_pipe  <= inst_op;

        a_pipe2   <= a_pipe;
        b_pipe2   <= b_pipe;
        rnd_pipe2 <= rnd_pipe;
        op_pipe2  <= op_pipe;
	
	z_reg <= z_temp;
	status_reg <= status_temp;
end

endmodule
