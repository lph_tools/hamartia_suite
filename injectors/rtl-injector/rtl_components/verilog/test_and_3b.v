//-----------------------------------------------------------------------------
// Component : And
// Summary : A simple AND test circuit.
// Author : Michael Sullivan <mbsullivan@utexas.edu>
//-----------------------------------------------------------------------------
// Input Ports
// A : WIDTH    : The first input
// B : WIDTH    : The second input
//-----------------------------------------------------------------------------
// Output Ports
// Z : WIDTH    : The result of the AND gate.
//-----------------------------------------------------------------------------
// Design Parameters
// WIDTH: The width of the addition.
//-----------------------------------------------------------------------------
// Designs
// Behavioral: A behavioral adder.
//-----------------------------------------------------------------------------


module And(A, B, Z);


// target design
parameter DESIGN = 0;
// design parameters
parameter WIDTH = 3;


// input declarations
input [WIDTH-1:0] A;    // The first operand.
input [WIDTH-1:0] B;    // The second operand.
// output declarations
output [WIDTH-1:0] Z;    // The result.


generate // to select a design at compile time

//-----------------------------------------------------------------------------
// Design: Behavioral
// Alternative aliases: B, Beh
//-----------------------------------------------------------------------------
if (DESIGN == 0) begin : Behavioral

    assign Z = A & B;

end // end Behavioral

//-----------------------------------------------------------------------------
// Sanity check design and design parameters
//-----------------------------------------------------------------------------
if (DESIGN < 0 || DESIGN > 0) begin : design_check
initial begin
    $display("Error: DESIGN is undefined!!");
    $finish(1);
end end
if (WIDTH < 0) begin : WIDTH_check
initial begin
    $display("Error: WIDTH is undefined!!");
    $finish(1);
end end

endgenerate // end design selection


endmodule
