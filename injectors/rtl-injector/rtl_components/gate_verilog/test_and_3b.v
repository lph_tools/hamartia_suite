
module And ( A, B, Z );
  input [2:0] A;
  input [2:0] B;
  output [2:0] Z;


  AND2_X1 U4 ( .A1(B[0]), .A2(A[0]), .ZN(Z[0]) );
  AND2_X1 U5 ( .A1(B[1]), .A2(A[1]), .ZN(Z[1]) );
  AND2_X1 U6 ( .A1(B[2]), .A2(A[2]), .ZN(Z[2]) );
endmodule

