/////////////////////////////////////////////////////////////
// Created by: Synopsys DC Expert(TM) in wire load mode
// Version   : L-2016.03-SP5-3
// Date      : Wed Sep  5 10:49:54 2018
/////////////////////////////////////////////////////////////


module DW_ecc_decode_inst_DW04_par_gen_12 ( datain, parity );
  input [62:0] datain;
  output parity;
  wire   n1, n2, n3, n4, n5, n6, n7, n8, n9, n10, n11, n12, n13, n14, n15, n16,
         n17, n18, n19, n20, n21, n22, n23, n24, n25, n26, n27, n28, n29, n30,
         n31;

  XNOR2_X1 U1 ( .A(n4), .B(n5), .ZN(n3) );
  XOR2_X1 U2 ( .A(datain[23]), .B(datain[22]), .Z(n1) );
  XOR2_X1 U3 ( .A(datain[31]), .B(datain[30]), .Z(n5) );
  XNOR2_X1 U4 ( .A(n6), .B(n7), .ZN(n4) );
  INV_X1 U5 ( .A(datain[29]), .ZN(n7) );
  XNOR2_X1 U6 ( .A(datain[28]), .B(datain[27]), .ZN(n8) );
  XNOR2_X1 U7 ( .A(n30), .B(n31), .ZN(n26) );
  XNOR2_X1 U8 ( .A(n8), .B(n9), .ZN(n6) );
  XNOR2_X1 U9 ( .A(datain[26]), .B(datain[25]), .ZN(n9) );
  XNOR2_X1 U10 ( .A(n22), .B(n21), .ZN(n20) );
  XNOR2_X1 U11 ( .A(n14), .B(n15), .ZN(n13) );
  XNOR2_X1 U12 ( .A(n26), .B(n27), .ZN(n25) );
  XNOR2_X1 U13 ( .A(n16), .B(datain[10]), .ZN(n15) );
  XNOR2_X1 U14 ( .A(datain[24]), .B(datain[19]), .ZN(n31) );
  XNOR2_X1 U15 ( .A(datain[15]), .B(datain[0]), .ZN(n21) );
  XNOR2_X1 U16 ( .A(datain[16]), .B(datain[4]), .ZN(n22) );
  XNOR2_X1 U17 ( .A(n23), .B(n24), .ZN(n19) );
  XNOR2_X1 U18 ( .A(datain[8]), .B(datain[7]), .ZN(n24) );
  XNOR2_X1 U19 ( .A(datain[14]), .B(datain[17]), .ZN(n16) );
  XNOR2_X1 U20 ( .A(n28), .B(n29), .ZN(n27) );
  XNOR2_X1 U21 ( .A(datain[21]), .B(datain[20]), .ZN(n28) );
  XNOR2_X1 U22 ( .A(n17), .B(n18), .ZN(n14) );
  XNOR2_X1 U23 ( .A(datain[9]), .B(datain[3]), .ZN(n17) );
  XNOR2_X1 U24 ( .A(n19), .B(n20), .ZN(n12) );
  XNOR2_X1 U25 ( .A(datain[6]), .B(datain[5]), .ZN(n23) );
  XNOR2_X1 U26 ( .A(datain[18]), .B(datain[13]), .ZN(n30) );
  XNOR2_X1 U27 ( .A(n25), .B(n1), .ZN(n10) );
  XNOR2_X1 U28 ( .A(n12), .B(n13), .ZN(n11) );
  XNOR2_X1 U29 ( .A(datain[2]), .B(datain[1]), .ZN(n18) );
  XNOR2_X1 U30 ( .A(n11), .B(n10), .ZN(n2) );
  XNOR2_X1 U31 ( .A(datain[12]), .B(datain[11]), .ZN(n29) );
  XNOR2_X1 U32 ( .A(n2), .B(n3), .ZN(parity) );
endmodule


module DW_ecc_decode_inst_DW04_par_gen_8 ( datain, parity );
  input [62:0] datain;
  output parity;
  wire   n1, n2, n3, n4, n5, n6, n7, n8, n9, n10, n11, n12, n13, n14, n15, n16,
         n17, n18, n19, n20, n21, n22, n23, n24, n25, n26, n27, n28, n29, n30,
         n31;

  XOR2_X1 U1 ( .A(n1), .B(datain[29]), .Z(n6) );
  XNOR2_X1 U2 ( .A(n8), .B(n9), .ZN(n1) );
  XNOR2_X1 U3 ( .A(n6), .B(n2), .ZN(n5) );
  XNOR2_X1 U4 ( .A(n7), .B(datain[30]), .ZN(n2) );
  XOR2_X1 U5 ( .A(datain[23]), .B(datain[22]), .Z(n3) );
  INV_X1 U6 ( .A(datain[31]), .ZN(n7) );
  XNOR2_X1 U7 ( .A(datain[28]), .B(datain[27]), .ZN(n8) );
  XNOR2_X1 U8 ( .A(datain[6]), .B(datain[5]), .ZN(n23) );
  XNOR2_X1 U9 ( .A(datain[8]), .B(datain[7]), .ZN(n24) );
  XNOR2_X1 U10 ( .A(datain[26]), .B(datain[25]), .ZN(n9) );
  XNOR2_X1 U11 ( .A(n23), .B(n24), .ZN(n19) );
  XNOR2_X1 U12 ( .A(n29), .B(n28), .ZN(n27) );
  XNOR2_X1 U13 ( .A(datain[9]), .B(datain[3]), .ZN(n17) );
  XNOR2_X1 U14 ( .A(datain[17]), .B(datain[14]), .ZN(n16) );
  XNOR2_X1 U15 ( .A(datain[0]), .B(datain[15]), .ZN(n21) );
  XNOR2_X1 U16 ( .A(n27), .B(n26), .ZN(n25) );
  XNOR2_X1 U17 ( .A(datain[12]), .B(datain[11]), .ZN(n29) );
  XNOR2_X1 U18 ( .A(n21), .B(n22), .ZN(n20) );
  XNOR2_X1 U19 ( .A(datain[16]), .B(datain[4]), .ZN(n22) );
  XNOR2_X1 U20 ( .A(datain[18]), .B(datain[13]), .ZN(n30) );
  XNOR2_X1 U21 ( .A(n25), .B(n3), .ZN(n10) );
  XNOR2_X1 U22 ( .A(datain[21]), .B(datain[20]), .ZN(n28) );
  XNOR2_X1 U23 ( .A(n30), .B(n31), .ZN(n26) );
  XNOR2_X1 U24 ( .A(datain[24]), .B(datain[19]), .ZN(n31) );
  XNOR2_X1 U25 ( .A(n4), .B(n5), .ZN(parity) );
  XNOR2_X1 U26 ( .A(n10), .B(n11), .ZN(n4) );
  XNOR2_X1 U27 ( .A(n13), .B(n12), .ZN(n11) );
  XNOR2_X1 U28 ( .A(n20), .B(n19), .ZN(n12) );
  XNOR2_X1 U29 ( .A(n14), .B(n15), .ZN(n13) );
  XNOR2_X1 U30 ( .A(n17), .B(n18), .ZN(n14) );
  XNOR2_X1 U31 ( .A(datain[1]), .B(datain[2]), .ZN(n18) );
  XNOR2_X1 U32 ( .A(n16), .B(datain[10]), .ZN(n15) );
endmodule


module DW_ecc_decode_inst_DW04_par_gen_11 ( datain, parity );
  input [62:0] datain;
  output parity;
  wire   n1, n2, n3, n4, n5, n6, n7, n8, n9, n10, n11, n12, n13, n14, n15, n16,
         n17, n18, n19, n20, n21, n22, n23, n24, n25, n26, n27, n28, n29, n30,
         n31;

  XNOR2_X1 U1 ( .A(n1), .B(n5), .ZN(n2) );
  XOR2_X1 U2 ( .A(datain[13]), .B(datain[18]), .Z(n1) );
  XNOR2_X1 U3 ( .A(n2), .B(n31), .ZN(n11) );
  XNOR2_X1 U4 ( .A(n3), .B(n4), .ZN(n31) );
  XNOR2_X1 U5 ( .A(datain[21]), .B(datain[20]), .ZN(n3) );
  XNOR2_X1 U6 ( .A(datain[12]), .B(datain[11]), .ZN(n4) );
  XNOR2_X1 U7 ( .A(datain[24]), .B(datain[19]), .ZN(n5) );
  XOR2_X1 U8 ( .A(n14), .B(n6), .Z(n13) );
  XOR2_X1 U9 ( .A(n15), .B(datain[30]), .Z(n6) );
  XNOR2_X1 U10 ( .A(n7), .B(n8), .ZN(n23) );
  XOR2_X1 U11 ( .A(datain[9]), .B(datain[3]), .Z(n7) );
  XOR2_X1 U12 ( .A(datain[1]), .B(datain[2]), .Z(n8) );
  XNOR2_X1 U13 ( .A(n9), .B(n10), .ZN(n27) );
  XOR2_X1 U14 ( .A(datain[15]), .B(datain[0]), .Z(n9) );
  XOR2_X1 U15 ( .A(datain[4]), .B(datain[16]), .Z(n10) );
  XNOR2_X1 U16 ( .A(n11), .B(n30), .ZN(n19) );
  XOR2_X1 U17 ( .A(n16), .B(datain[29]), .Z(n14) );
  INV_X1 U18 ( .A(datain[31]), .ZN(n15) );
  XNOR2_X1 U19 ( .A(datain[6]), .B(datain[5]), .ZN(n28) );
  XNOR2_X1 U20 ( .A(datain[8]), .B(datain[7]), .ZN(n29) );
  XNOR2_X1 U21 ( .A(n17), .B(n18), .ZN(n16) );
  XNOR2_X1 U22 ( .A(datain[26]), .B(datain[25]), .ZN(n18) );
  XNOR2_X1 U23 ( .A(datain[28]), .B(datain[27]), .ZN(n17) );
  XNOR2_X1 U24 ( .A(datain[23]), .B(datain[22]), .ZN(n30) );
  XNOR2_X1 U25 ( .A(n28), .B(n29), .ZN(n26) );
  XNOR2_X1 U26 ( .A(n26), .B(n27), .ZN(n21) );
  XNOR2_X1 U27 ( .A(n23), .B(n24), .ZN(n22) );
  XNOR2_X1 U28 ( .A(n21), .B(n22), .ZN(n20) );
  XNOR2_X1 U29 ( .A(n20), .B(n19), .ZN(n12) );
  XNOR2_X1 U30 ( .A(n12), .B(n13), .ZN(parity) );
  XNOR2_X1 U31 ( .A(n25), .B(datain[10]), .ZN(n24) );
  XNOR2_X1 U32 ( .A(datain[17]), .B(datain[14]), .ZN(n25) );
endmodule


module DW_ecc_decode_inst_DW04_par_gen_9 ( datain, parity );
  input [62:0] datain;
  output parity;
  wire   n1, n2, n3, n4, n5, n6, n7, n8, n9, n10, n11, n12, n13, n14, n15, n16,
         n17, n18, n19, n20, n21, n22, n23, n24, n25, n26, n27, n28, n29, n30;

  XNOR2_X1 U1 ( .A(n1), .B(datain[31]), .ZN(n24) );
  XNOR2_X1 U2 ( .A(datain[30]), .B(datain[29]), .ZN(n1) );
  XNOR2_X1 U3 ( .A(n2), .B(n26), .ZN(n25) );
  XOR2_X1 U4 ( .A(datain[26]), .B(datain[25]), .Z(n2) );
  XNOR2_X1 U5 ( .A(n30), .B(n4), .ZN(n27) );
  XNOR2_X1 U6 ( .A(n29), .B(n5), .ZN(n28) );
  XNOR2_X1 U7 ( .A(n20), .B(n3), .ZN(n19) );
  XNOR2_X1 U8 ( .A(datain[23]), .B(datain[22]), .ZN(n29) );
  XNOR2_X1 U9 ( .A(datain[21]), .B(datain[20]), .ZN(n20) );
  XOR2_X1 U10 ( .A(datain[17]), .B(datain[14]), .Z(n3) );
  XOR2_X1 U11 ( .A(datain[24]), .B(datain[19]), .Z(n4) );
  XOR2_X1 U12 ( .A(datain[12]), .B(datain[11]), .Z(n5) );
  XOR2_X1 U13 ( .A(datain[9]), .B(datain[3]), .Z(n6) );
  XNOR2_X1 U14 ( .A(datain[18]), .B(datain[13]), .ZN(n30) );
  XNOR2_X1 U15 ( .A(datain[6]), .B(datain[5]), .ZN(n15) );
  XNOR2_X1 U16 ( .A(datain[8]), .B(datain[7]), .ZN(n14) );
  XNOR2_X1 U17 ( .A(n18), .B(n19), .ZN(n9) );
  XNOR2_X1 U18 ( .A(n21), .B(n6), .ZN(n18) );
  XNOR2_X1 U19 ( .A(datain[2]), .B(datain[1]), .ZN(n21) );
  XNOR2_X1 U20 ( .A(datain[28]), .B(datain[27]), .ZN(n26) );
  XNOR2_X1 U21 ( .A(n24), .B(n25), .ZN(n23) );
  XNOR2_X1 U22 ( .A(n27), .B(n28), .ZN(n22) );
  XNOR2_X1 U23 ( .A(datain[16]), .B(datain[4]), .ZN(n17) );
  XNOR2_X1 U24 ( .A(n11), .B(n12), .ZN(n10) );
  XNOR2_X1 U25 ( .A(n10), .B(n9), .ZN(n8) );
  XNOR2_X1 U26 ( .A(n8), .B(n7), .ZN(parity) );
  XNOR2_X1 U27 ( .A(n16), .B(datain[0]), .ZN(n11) );
  XNOR2_X1 U28 ( .A(n13), .B(datain[10]), .ZN(n12) );
  XNOR2_X1 U29 ( .A(n14), .B(n15), .ZN(n13) );
  XNOR2_X1 U30 ( .A(n17), .B(datain[15]), .ZN(n16) );
  XNOR2_X1 U31 ( .A(n22), .B(n23), .ZN(n7) );
endmodule


module DW_ecc_decode_inst_DW04_par_gen_10 ( datain, parity );
  input [62:0] datain;
  output parity;
  wire   n1, n2, n3, n4, n5, n6, n7, n8, n9, n10, n11, n12, n13, n14, n15, n16,
         n17, n18, n19, n20, n21, n22, n23, n24, n25, n26, n27, n28, n29, n30,
         n31, n32;

  XNOR2_X1 U1 ( .A(n7), .B(n6), .ZN(parity) );
  XNOR2_X1 U2 ( .A(n25), .B(n3), .ZN(n24) );
  INV_X1 U3 ( .A(n28), .ZN(n27) );
  XNOR2_X1 U4 ( .A(n17), .B(n18), .ZN(n8) );
  XNOR2_X1 U5 ( .A(n20), .B(n5), .ZN(n17) );
  XNOR2_X1 U6 ( .A(n19), .B(n4), .ZN(n18) );
  XNOR2_X1 U7 ( .A(datain[26]), .B(datain[25]), .ZN(n25) );
  XNOR2_X1 U8 ( .A(datain[30]), .B(datain[29]), .ZN(n26) );
  XNOR2_X1 U9 ( .A(n1), .B(n31), .ZN(n30) );
  XOR2_X1 U10 ( .A(datain[23]), .B(datain[22]), .Z(n1) );
  XNOR2_X1 U11 ( .A(datain[12]), .B(datain[11]), .ZN(n31) );
  XNOR2_X1 U12 ( .A(n2), .B(n32), .ZN(n29) );
  XOR2_X1 U13 ( .A(datain[18]), .B(datain[13]), .Z(n2) );
  XNOR2_X1 U14 ( .A(datain[8]), .B(datain[7]), .ZN(n13) );
  INV_X1 U15 ( .A(datain[31]), .ZN(n28) );
  XOR2_X1 U16 ( .A(datain[28]), .B(datain[27]), .Z(n3) );
  XOR2_X1 U17 ( .A(datain[17]), .B(datain[14]), .Z(n4) );
  XOR2_X1 U18 ( .A(datain[9]), .B(datain[3]), .Z(n5) );
  XNOR2_X1 U19 ( .A(n26), .B(n27), .ZN(n23) );
  XNOR2_X1 U20 ( .A(datain[24]), .B(datain[19]), .ZN(n32) );
  XNOR2_X1 U21 ( .A(datain[6]), .B(datain[5]), .ZN(n14) );
  XNOR2_X1 U22 ( .A(n29), .B(n30), .ZN(n21) );
  XNOR2_X1 U23 ( .A(datain[21]), .B(datain[20]), .ZN(n19) );
  XNOR2_X1 U24 ( .A(datain[2]), .B(datain[1]), .ZN(n20) );
  XNOR2_X1 U25 ( .A(n23), .B(n24), .ZN(n22) );
  XNOR2_X1 U26 ( .A(datain[16]), .B(datain[4]), .ZN(n16) );
  XNOR2_X1 U27 ( .A(n9), .B(n8), .ZN(n7) );
  XNOR2_X1 U28 ( .A(n10), .B(n11), .ZN(n9) );
  XNOR2_X1 U29 ( .A(n15), .B(datain[0]), .ZN(n10) );
  XNOR2_X1 U30 ( .A(n12), .B(datain[10]), .ZN(n11) );
  XNOR2_X1 U31 ( .A(n13), .B(n14), .ZN(n12) );
  XNOR2_X1 U32 ( .A(n16), .B(datain[15]), .ZN(n15) );
  XNOR2_X1 U33 ( .A(n21), .B(n22), .ZN(n6) );
endmodule


module DW_ecc_decode_inst_DW04_par_gen_15 ( datain, parity );
  input [62:0] datain;
  output parity;
  wire   n1, n2, n3, n4, n5, n6, n7, n8, n9, n10, n11, n12, n13, n14, n15, n16,
         n17, n18, n19, n20, n21, n22, n23, n24, n25, n26, n27, n28, n29, n30,
         n31, n32;

  XNOR2_X1 U1 ( .A(n1), .B(n2), .ZN(n31) );
  XNOR2_X1 U2 ( .A(datain[21]), .B(datain[20]), .ZN(n1) );
  XOR2_X1 U3 ( .A(datain[12]), .B(datain[11]), .Z(n2) );
  XNOR2_X1 U4 ( .A(n3), .B(n4), .ZN(n19) );
  XNOR2_X1 U5 ( .A(n30), .B(n31), .ZN(n3) );
  XOR2_X1 U6 ( .A(datain[23]), .B(datain[22]), .Z(n4) );
  XNOR2_X1 U7 ( .A(n14), .B(n15), .ZN(n13) );
  XNOR2_X1 U8 ( .A(n5), .B(n6), .ZN(n16) );
  XOR2_X1 U9 ( .A(datain[28]), .B(datain[27]), .Z(n5) );
  XOR2_X1 U10 ( .A(datain[26]), .B(datain[25]), .Z(n6) );
  XNOR2_X1 U11 ( .A(n7), .B(n8), .ZN(n23) );
  XOR2_X1 U12 ( .A(datain[9]), .B(datain[3]), .Z(n7) );
  XOR2_X1 U13 ( .A(datain[1]), .B(datain[2]), .Z(n8) );
  XNOR2_X1 U14 ( .A(n9), .B(n29), .ZN(n26) );
  XOR2_X1 U15 ( .A(datain[6]), .B(datain[5]), .Z(n9) );
  XNOR2_X1 U16 ( .A(datain[8]), .B(datain[7]), .ZN(n29) );
  XNOR2_X1 U17 ( .A(n10), .B(n28), .ZN(n27) );
  XOR2_X1 U18 ( .A(datain[15]), .B(datain[0]), .Z(n10) );
  XNOR2_X1 U19 ( .A(n11), .B(n32), .ZN(n30) );
  XOR2_X1 U20 ( .A(datain[18]), .B(datain[13]), .Z(n11) );
  XNOR2_X1 U21 ( .A(datain[24]), .B(datain[19]), .ZN(n32) );
  INV_X1 U22 ( .A(datain[31]), .ZN(n18) );
  INV_X1 U23 ( .A(datain[30]), .ZN(n17) );
  XNOR2_X1 U24 ( .A(datain[17]), .B(datain[14]), .ZN(n25) );
  XNOR2_X1 U25 ( .A(datain[4]), .B(datain[16]), .ZN(n28) );
  XNOR2_X1 U26 ( .A(n26), .B(n27), .ZN(n21) );
  XNOR2_X1 U27 ( .A(n20), .B(n19), .ZN(n12) );
  XNOR2_X1 U28 ( .A(n12), .B(n13), .ZN(parity) );
  XNOR2_X1 U29 ( .A(n16), .B(datain[29]), .ZN(n15) );
  XNOR2_X1 U30 ( .A(n17), .B(n18), .ZN(n14) );
  XNOR2_X1 U31 ( .A(n21), .B(n22), .ZN(n20) );
  XNOR2_X1 U32 ( .A(n23), .B(n24), .ZN(n22) );
  XNOR2_X1 U33 ( .A(n25), .B(datain[10]), .ZN(n24) );
endmodule


module DW_ecc_decode_inst_DW04_par_gen_19 ( datain, parity );
  input [62:0] datain;
  output parity;
  wire   n1, n2, n3, n4, n5, n6, n7, n8, n9, n10, n11, n12, n13, n14, n15, n16,
         n17, n18, n19, n20, n21, n22, n23, n24, n25, n26, n27, n28, n29, n30;

  XNOR2_X1 U1 ( .A(n1), .B(datain[10]), .ZN(n19) );
  XNOR2_X1 U2 ( .A(datain[17]), .B(datain[14]), .ZN(n1) );
  XOR2_X1 U3 ( .A(n11), .B(datain[29]), .Z(n9) );
  XNOR2_X1 U4 ( .A(n22), .B(n23), .ZN(n16) );
  XNOR2_X1 U5 ( .A(n2), .B(n3), .ZN(n30) );
  XOR2_X1 U6 ( .A(datain[21]), .B(datain[20]), .Z(n2) );
  XOR2_X1 U7 ( .A(datain[11]), .B(datain[12]), .Z(n3) );
  XNOR2_X1 U8 ( .A(n5), .B(n4), .ZN(n29) );
  XOR2_X1 U9 ( .A(datain[13]), .B(datain[18]), .Z(n4) );
  XOR2_X1 U10 ( .A(datain[24]), .B(datain[19]), .Z(n5) );
  XOR2_X1 U11 ( .A(datain[23]), .B(datain[22]), .Z(n6) );
  XNOR2_X1 U12 ( .A(n9), .B(n10), .ZN(n8) );
  XNOR2_X1 U13 ( .A(datain[28]), .B(datain[27]), .ZN(n12) );
  XNOR2_X1 U14 ( .A(n12), .B(n13), .ZN(n11) );
  XNOR2_X1 U15 ( .A(datain[26]), .B(datain[25]), .ZN(n13) );
  XOR2_X1 U16 ( .A(datain[31]), .B(datain[30]), .Z(n10) );
  XNOR2_X1 U17 ( .A(n29), .B(n30), .ZN(n28) );
  XNOR2_X1 U18 ( .A(datain[2]), .B(datain[1]), .ZN(n21) );
  XNOR2_X1 U19 ( .A(n16), .B(n17), .ZN(n15) );
  XNOR2_X1 U20 ( .A(datain[9]), .B(datain[3]), .ZN(n20) );
  XNOR2_X1 U21 ( .A(datain[15]), .B(datain[0]), .ZN(n24) );
  XNOR2_X1 U22 ( .A(n28), .B(n6), .ZN(n14) );
  XNOR2_X1 U23 ( .A(datain[6]), .B(datain[5]), .ZN(n26) );
  XNOR2_X1 U24 ( .A(n18), .B(n19), .ZN(n17) );
  XNOR2_X1 U25 ( .A(n20), .B(n21), .ZN(n18) );
  XNOR2_X1 U26 ( .A(n14), .B(n15), .ZN(n7) );
  XNOR2_X1 U27 ( .A(n26), .B(n27), .ZN(n22) );
  XNOR2_X1 U28 ( .A(datain[4]), .B(datain[16]), .ZN(n25) );
  XNOR2_X1 U29 ( .A(n24), .B(n25), .ZN(n23) );
  XNOR2_X1 U30 ( .A(datain[8]), .B(datain[7]), .ZN(n27) );
  XNOR2_X1 U31 ( .A(n7), .B(n8), .ZN(parity) );
endmodule


module DW_ecc_decode_inst_DW04_par_gen_26 ( datain, parity );
  input [62:0] datain;
  output parity;
  wire   n1, n2, n3, n4, n5, n6, n7, n8, n9, n10, n11, n12, n13, n14, n15, n16,
         n17, n18, n19, n20, n21, n22, n23, n24, n25, n26, n27, n28, n29, n30,
         n31;

  XNOR2_X1 U1 ( .A(n13), .B(n1), .ZN(n12) );
  XNOR2_X1 U2 ( .A(n14), .B(datain[30]), .ZN(n1) );
  XOR2_X1 U3 ( .A(n15), .B(datain[29]), .Z(n13) );
  XNOR2_X1 U4 ( .A(n2), .B(n3), .ZN(n31) );
  XOR2_X1 U5 ( .A(datain[21]), .B(datain[20]), .Z(n2) );
  XOR2_X1 U6 ( .A(datain[12]), .B(datain[11]), .Z(n3) );
  XNOR2_X1 U7 ( .A(datain[26]), .B(datain[25]), .ZN(n17) );
  XNOR2_X1 U8 ( .A(n4), .B(n5), .ZN(n30) );
  XOR2_X1 U9 ( .A(datain[18]), .B(datain[13]), .Z(n4) );
  XOR2_X1 U10 ( .A(datain[24]), .B(datain[19]), .Z(n5) );
  XOR2_X1 U11 ( .A(datain[23]), .B(datain[22]), .Z(n6) );
  XNOR2_X1 U12 ( .A(n7), .B(n8), .ZN(n26) );
  XOR2_X1 U13 ( .A(datain[15]), .B(datain[0]), .Z(n7) );
  XOR2_X1 U14 ( .A(datain[4]), .B(datain[16]), .Z(n8) );
  XNOR2_X1 U15 ( .A(n16), .B(n17), .ZN(n15) );
  XNOR2_X1 U16 ( .A(datain[28]), .B(datain[27]), .ZN(n16) );
  XNOR2_X1 U17 ( .A(n9), .B(n10), .ZN(n22) );
  XOR2_X1 U18 ( .A(datain[9]), .B(datain[3]), .Z(n9) );
  XOR2_X1 U19 ( .A(datain[2]), .B(datain[1]), .Z(n10) );
  INV_X1 U20 ( .A(datain[31]), .ZN(n14) );
  XNOR2_X1 U21 ( .A(datain[8]), .B(datain[7]), .ZN(n28) );
  XNOR2_X1 U22 ( .A(n27), .B(n28), .ZN(n25) );
  XNOR2_X1 U23 ( .A(n31), .B(n30), .ZN(n29) );
  XNOR2_X1 U24 ( .A(datain[6]), .B(datain[5]), .ZN(n27) );
  XNOR2_X1 U25 ( .A(n22), .B(n23), .ZN(n21) );
  XNOR2_X1 U26 ( .A(n25), .B(n26), .ZN(n20) );
  XNOR2_X1 U27 ( .A(n29), .B(n6), .ZN(n18) );
  XNOR2_X1 U28 ( .A(n20), .B(n21), .ZN(n19) );
  XNOR2_X1 U29 ( .A(n11), .B(n12), .ZN(parity) );
  XNOR2_X1 U30 ( .A(n19), .B(n18), .ZN(n11) );
  XNOR2_X1 U31 ( .A(n24), .B(datain[10]), .ZN(n23) );
  XNOR2_X1 U32 ( .A(datain[17]), .B(datain[14]), .ZN(n24) );
endmodule


module DW_ecc_decode_inst_DW_ecc_0 ( gen, correct_n, datain, chkin, err_detect, 
        err_multpl, dataout, chkout );
  input [63:0] datain;
  input [7:0] chkin;
  output [63:0] dataout;
  output [7:0] chkout;
  input gen, correct_n;
  output err_detect, err_multpl;
  wire   n1, n2, n3, n4, n5, n6, n7, n8, n9, n10, n11, n12, n13, n14, n15, n16,
         n17, n18, n19, n20, n23, n24, n25, n26, n27, n28, n29, n30, n31, n32,
         n33, n34, n35, n36, n37, n38, n39, n40, n41, n42, n43, n44, n45, n46,
         n47, n48, n49, n50, n51, n52, n53, n54, n55, n56, n57, n58, n59, n60,
         n61, n62, n63, n64, n65, n66, n67, n68, n69, n70, n71, n72, n73, n74,
         n75, n76, n77, n78, n79, n80, n81, n82, n83, n84, n85, n86, n87, n88,
         n89, n90, n91, n92, n93, n94, n95, n96, n97, n98, n99, n100, n101,
         n102, n103, n104, n105, n106, n107, n108, n110, n111, n112, n113,
         n114, n115, n116, n117, n118, n119, n120, n121, n122, n123, n124,
         n125, n127, n128, n129, n130, n131, n132, n133, n134, n135, n136,
         n137, n138, n139, n140, n141, n142, n143, n144, n145, n146, n147,
         n148, n149, n150, n151, n153, n154, n155, n156, n157, n158, n159,
         n160, n161, n162, n163, n164, n165, n166, n167, n168, n169, n170,
         n171, n172, n173, n174, n175, n176, n177, n178, n179, n180, n181,
         n182, n183, n184, n185, n186, n187, n188, n189, n190, n191, n192,
         n193, n194, n195, n196, n197, n198, n199, n200, n201, n202, n203,
         n204, n205, n206, n207, n208, n209, n210, n211, n212, n213, n214,
         n215, n216, n217, n218, n219, n220, n221, n222, n223, n224, n225,
         n226, n227, n228, n229, n230, n231, n232, n233, n234, n235, n236,
         n237, n238, n239, n240, n241, n242, n243, n244, n245, n246, n247,
         n248, n249, n250, n251, n252, n253, n254, n255, n256, n260;
  wire   [7:0] chkout_int;

  DW_ecc_decode_inst_DW04_par_gen_12 \GEN_CB_8.U_PAR_GEN_6  ( .datain({1'b0, 
        1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 
        1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 
        1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, datain[63:56], datain[39:24], 
        datain[7:0]}), .parity(chkout_int[6]) );
  DW_ecc_decode_inst_DW04_par_gen_8 \GEN_CB_8.U_PAR_GEN_0  ( .datain({1'b0, 
        1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 
        1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 
        1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, datain[63], datain[61:60], 
        datain[58], datain[55:54], datain[52], datain[48:47], n83, datain[44], 
        datain[42], datain[39:38], datain[36], datain[32], datain[30], 
        datain[27], datain[25:24], datain[21], datain[19:17], datain[14], 
        datain[11], datain[9:8], datain[5], datain[3:1]}), .parity(
        chkout_int[0]) );
  DW_ecc_decode_inst_DW04_par_gen_11 \GEN_CB_8.U_PAR_GEN_1  ( .datain({1'b0, 
        1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 
        1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 
        1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, datain[60], datain[58], datain[56], 
        datain[54], datain[52], datain[50:48], n117, n138, datain[40], 
        datain[38], datain[36], datain[34:32], datain[28], datain[26], 
        datain[24], datain[22], datain[20], datain[18:16], datain[12], 
        datain[10], datain[8], datain[6], datain[4], datain[2:0]}), .parity(
        chkout_int[1]) );
  DW_ecc_decode_inst_DW04_par_gen_9 \GEN_CB_8.U_PAR_GEN_2  ( .datain({1'b0, 
        1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 
        1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 
        1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, datain[63], datain[61], 
        datain[58:57], datain[55], datain[52:51], datain[48:47], datain[45], 
        datain[42:41], datain[39], datain[36:35], datain[32], n88, datain[29], 
        datain[26:25], datain[23], datain[20:19], datain[16:15], datain[13], 
        datain[10:9], n33, datain[4:3], n260}), .parity(chkout_int[2]) );
  DW_ecc_decode_inst_DW04_par_gen_10 \GEN_CB_8.U_PAR_GEN_3  ( .datain({1'b0, 
        1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 
        1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 
        1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, datain[61:59], datain[55:53], 
        datain[49:48], datain[45:43], datain[39], n106, datain[37], 
        datain[33:32], n111, n84, n98, datain[23:21], datain[17:16], 
        datain[13:11], datain[7:5], datain[1], n260}), .parity(chkout_int[3])
         );
  DW_ecc_decode_inst_DW04_par_gen_15 \GEN_CB_8.U_PAR_GEN_4  ( .datain({1'b0, 
        1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 
        1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 
        1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, datain[63:62], datain[55:50], 
        datain[47:46], datain[39:34], datain[31:30], datain[23:18], 
        datain[15:14], datain[7:2]}), .parity(chkout_int[4]) );
  DW_ecc_decode_inst_DW04_par_gen_19 \GEN_CB_8.U_PAR_GEN_7  ( .datain({1'b0, 
        1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 
        1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 
        1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, datain[55:40], datain[31:24], 
        datain[7:0]}), .parity(chkout_int[7]) );
  DW_ecc_decode_inst_DW04_par_gen_26 \GEN_CB_8.U_PAR_GEN_5  ( .datain({1'b0, 
        1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 
        1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 
        1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, datain[63:56], datain[47:40], 
        datain[31:24], datain[15:8]}), .parity(chkout_int[5]) );
  XNOR2_X1 U2 ( .A(n1), .B(datain[50]), .ZN(dataout[50]) );
  OR2_X1 U3 ( .A1(n231), .A2(n188), .ZN(n1) );
  OR2_X1 U4 ( .A1(n154), .A2(n161), .ZN(n212) );
  XNOR2_X1 U5 ( .A(n2), .B(datain[22]), .ZN(dataout[22]) );
  OR2_X1 U6 ( .A1(n56), .A2(n212), .ZN(n2) );
  XNOR2_X1 U7 ( .A(datain[55]), .B(n3), .ZN(dataout[55]) );
  OR2_X1 U8 ( .A1(n192), .A2(n57), .ZN(n3) );
  XNOR2_X1 U9 ( .A(n4), .B(datain[11]), .ZN(dataout[11]) );
  OR2_X1 U10 ( .A1(n210), .A2(n66), .ZN(n4) );
  XNOR2_X1 U11 ( .A(n5), .B(datain[12]), .ZN(dataout[12]) );
  OR2_X1 U12 ( .A1(n66), .A2(n212), .ZN(n5) );
  XNOR2_X1 U13 ( .A(datain[61]), .B(n6), .ZN(dataout[61]) );
  OR2_X1 U14 ( .A1(n93), .A2(n192), .ZN(n6) );
  XNOR2_X1 U15 ( .A(n7), .B(datain[57]), .ZN(dataout[57]) );
  OR2_X1 U16 ( .A1(n93), .A2(n189), .ZN(n7) );
  XNOR2_X1 U17 ( .A(n8), .B(n114), .ZN(dataout[28]) );
  OR2_X1 U18 ( .A1(n170), .A2(n212), .ZN(n8) );
  XNOR2_X1 U19 ( .A(n9), .B(datain[43]), .ZN(dataout[43]) );
  OR2_X1 U20 ( .A1(n183), .A2(n232), .ZN(n9) );
  XNOR2_X1 U21 ( .A(n10), .B(datain[34]), .ZN(dataout[34]) );
  OR2_X1 U22 ( .A1(n180), .A2(n231), .ZN(n10) );
  CLKBUF_X1 U23 ( .A(n86), .Z(n14) );
  INV_X1 U24 ( .A(n161), .ZN(n11) );
  AND3_X2 U25 ( .A1(chkout[4]), .A2(n157), .A3(n176), .ZN(n12) );
  AND3_X1 U26 ( .A1(chkout[4]), .A2(n157), .A3(n176), .ZN(n132) );
  INV_X1 U27 ( .A(n54), .ZN(n13) );
  INV_X1 U28 ( .A(n16), .ZN(n15) );
  CLKBUF_X1 U29 ( .A(n108), .Z(n146) );
  AND2_X1 U30 ( .A1(chkout[7]), .A2(chkout[6]), .ZN(n16) );
  XNOR2_X1 U31 ( .A(n17), .B(datain[35]), .ZN(dataout[35]) );
  OR2_X1 U32 ( .A1(n189), .A2(n95), .ZN(n17) );
  XNOR2_X1 U33 ( .A(n18), .B(n138), .ZN(dataout[42]) );
  OR2_X1 U34 ( .A1(n190), .A2(n183), .ZN(n18) );
  BUF_X1 U35 ( .A(n27), .Z(n19) );
  XNOR2_X1 U36 ( .A(n20), .B(datain[62]), .ZN(dataout[62]) );
  OR2_X1 U37 ( .A1(n253), .A2(correct_n), .ZN(n20) );
  XNOR2_X1 U38 ( .A(chkout_int[5]), .B(n145), .ZN(chkout[5]) );
  XNOR2_X1 U39 ( .A(n148), .B(chkin[4]), .ZN(chkout[4]) );
  BUF_X1 U40 ( .A(n248), .Z(n23) );
  XNOR2_X1 U41 ( .A(datain[60]), .B(n24), .ZN(dataout[60]) );
  OR2_X1 U42 ( .A1(n139), .A2(n93), .ZN(n24) );
  AND2_X2 U43 ( .A1(n159), .A2(n158), .ZN(n25) );
  INV_X2 U44 ( .A(n25), .ZN(n228) );
  XOR2_X1 U45 ( .A(chkout_int[7]), .B(chkin[7]), .Z(n26) );
  AND2_X1 U46 ( .A1(n115), .A2(n135), .ZN(n27) );
  CLKBUF_X1 U47 ( .A(n229), .Z(n28) );
  AND2_X1 U48 ( .A1(n199), .A2(n174), .ZN(n29) );
  AND3_X1 U49 ( .A1(n194), .A2(n200), .A3(n146), .ZN(n30) );
  AND2_X1 U50 ( .A1(n199), .A2(n174), .ZN(n129) );
  AND2_X1 U51 ( .A1(chkout[3]), .A2(n158), .ZN(n31) );
  INV_X1 U52 ( .A(n94), .ZN(n32) );
  CLKBUF_X1 U53 ( .A(datain[7]), .Z(n33) );
  INV_X1 U54 ( .A(n246), .ZN(n171) );
  XNOR2_X1 U55 ( .A(n148), .B(chkin[4]), .ZN(n140) );
  AND2_X1 U56 ( .A1(n135), .A2(n174), .ZN(n34) );
  OR2_X1 U57 ( .A1(n167), .A2(n249), .ZN(err_detect) );
  XNOR2_X1 U58 ( .A(n35), .B(datain[49]), .ZN(dataout[49]) );
  OR2_X1 U59 ( .A1(n187), .A2(n250), .ZN(n35) );
  XNOR2_X1 U60 ( .A(n36), .B(n121), .ZN(dataout[39]) );
  OR2_X1 U61 ( .A1(n180), .A2(n192), .ZN(n36) );
  XNOR2_X1 U62 ( .A(n37), .B(datain[21]), .ZN(dataout[21]) );
  OR2_X1 U63 ( .A1(n210), .A2(n56), .ZN(n37) );
  XNOR2_X1 U64 ( .A(n38), .B(n101), .ZN(dataout[36]) );
  OR2_X1 U65 ( .A1(n190), .A2(n180), .ZN(n38) );
  AND2_X1 U66 ( .A1(n162), .A2(n199), .ZN(n96) );
  XNOR2_X1 U67 ( .A(n39), .B(datain[53]), .ZN(dataout[53]) );
  OR2_X1 U68 ( .A1(n57), .A2(n232), .ZN(n39) );
  XNOR2_X1 U69 ( .A(datain[58]), .B(n40), .ZN(dataout[58]) );
  OR2_X1 U70 ( .A1(n190), .A2(n93), .ZN(n40) );
  XNOR2_X1 U71 ( .A(n41), .B(datain[18]), .ZN(dataout[18]) );
  OR2_X1 U72 ( .A1(n211), .A2(n169), .ZN(n41) );
  XNOR2_X1 U73 ( .A(n42), .B(datain[59]), .ZN(dataout[59]) );
  OR2_X1 U74 ( .A1(n93), .A2(n232), .ZN(n42) );
  XNOR2_X1 U75 ( .A(n43), .B(datain[56]), .ZN(dataout[56]) );
  OR2_X1 U76 ( .A1(n93), .A2(n231), .ZN(n43) );
  XNOR2_X1 U77 ( .A(n44), .B(datain[20]), .ZN(dataout[20]) );
  OR2_X1 U78 ( .A1(n213), .A2(n169), .ZN(n44) );
  XNOR2_X1 U79 ( .A(n45), .B(datain[51]), .ZN(dataout[51]) );
  OR2_X1 U80 ( .A1(n189), .A2(n57), .ZN(n45) );
  XNOR2_X1 U81 ( .A(datain[52]), .B(n46), .ZN(dataout[52]) );
  OR2_X1 U82 ( .A1(n190), .A2(n188), .ZN(n46) );
  XNOR2_X1 U83 ( .A(n47), .B(datain[17]), .ZN(dataout[17]) );
  OR3_X1 U84 ( .A1(correct_n), .A2(n233), .A3(n167), .ZN(n47) );
  OR2_X1 U85 ( .A1(n248), .A2(correct_n), .ZN(n48) );
  INV_X1 U86 ( .A(n96), .ZN(n230) );
  XNOR2_X1 U87 ( .A(n49), .B(datain[15]), .ZN(dataout[15]) );
  OR2_X1 U88 ( .A1(n189), .A2(n166), .ZN(n49) );
  XNOR2_X1 U89 ( .A(n50), .B(datain[10]), .ZN(dataout[10]) );
  OR2_X1 U90 ( .A1(n213), .A2(n165), .ZN(n50) );
  XNOR2_X1 U91 ( .A(n51), .B(datain[19]), .ZN(dataout[19]) );
  OR2_X1 U92 ( .A1(n185), .A2(n169), .ZN(n51) );
  XNOR2_X1 U93 ( .A(n52), .B(datain[8]), .ZN(dataout[8]) );
  OR2_X1 U94 ( .A1(n211), .A2(n66), .ZN(n52) );
  XNOR2_X1 U95 ( .A(n53), .B(datain[13]), .ZN(dataout[13]) );
  OR2_X1 U96 ( .A1(n168), .A2(n165), .ZN(n53) );
  INV_X1 U97 ( .A(n198), .ZN(n54) );
  XNOR2_X1 U98 ( .A(datain[37]), .B(n55), .ZN(dataout[37]) );
  OR2_X1 U99 ( .A1(n95), .A2(n232), .ZN(n55) );
  NAND2_X1 U100 ( .A1(n12), .A2(n115), .ZN(n56) );
  AND2_X2 U101 ( .A1(n173), .A2(n181), .ZN(n115) );
  NAND2_X1 U102 ( .A1(n12), .A2(n77), .ZN(n57) );
  BUF_X1 U103 ( .A(datain[3]), .Z(n119) );
  XNOR2_X1 U104 ( .A(n58), .B(n88), .ZN(dataout[31]) );
  OR2_X1 U105 ( .A1(n172), .A2(n189), .ZN(n58) );
  NOR2_X1 U106 ( .A1(n248), .A2(correct_n), .ZN(n59) );
  XNOR2_X1 U107 ( .A(n60), .B(n106), .ZN(dataout[38]) );
  OR2_X1 U108 ( .A1(n139), .A2(n95), .ZN(n60) );
  XNOR2_X1 U109 ( .A(n61), .B(n104), .ZN(dataout[5]) );
  OR2_X1 U110 ( .A1(n163), .A2(n210), .ZN(n61) );
  XNOR2_X1 U111 ( .A(n62), .B(datain[23]), .ZN(dataout[23]) );
  OR2_X1 U112 ( .A1(n168), .A2(n56), .ZN(n62) );
  XNOR2_X1 U113 ( .A(n63), .B(datain[40]), .ZN(dataout[40]) );
  OR2_X1 U114 ( .A1(n183), .A2(n231), .ZN(n63) );
  XNOR2_X1 U115 ( .A(n64), .B(n99), .ZN(dataout[24]) );
  OR2_X1 U116 ( .A1(n170), .A2(n211), .ZN(n64) );
  XNOR2_X1 U117 ( .A(n65), .B(n117), .ZN(dataout[44]) );
  OR2_X1 U118 ( .A1(n139), .A2(n183), .ZN(n65) );
  NAND2_X1 U119 ( .A1(n131), .A2(n115), .ZN(n66) );
  XNOR2_X1 U120 ( .A(n67), .B(n87), .ZN(dataout[45]) );
  OR2_X1 U121 ( .A1(n183), .A2(n192), .ZN(n67) );
  XNOR2_X1 U122 ( .A(datain[54]), .B(n68), .ZN(dataout[54]) );
  OR2_X1 U123 ( .A1(n139), .A2(n188), .ZN(n68) );
  XNOR2_X1 U124 ( .A(n69), .B(n122), .ZN(dataout[27]) );
  OR2_X1 U125 ( .A1(n170), .A2(n210), .ZN(n69) );
  XNOR2_X1 U126 ( .A(n70), .B(n111), .ZN(dataout[29]) );
  OR2_X1 U127 ( .A1(n170), .A2(n168), .ZN(n70) );
  XNOR2_X1 U128 ( .A(n71), .B(datain[16]), .ZN(dataout[16]) );
  OR2_X1 U129 ( .A1(n207), .A2(correct_n), .ZN(n71) );
  XNOR2_X1 U130 ( .A(n72), .B(n120), .ZN(dataout[2]) );
  OR2_X1 U131 ( .A1(n211), .A2(n163), .ZN(n72) );
  XNOR2_X1 U132 ( .A(datain[26]), .B(n73), .ZN(dataout[26]) );
  OR2_X1 U133 ( .A1(n170), .A2(n213), .ZN(n73) );
  XNOR2_X1 U134 ( .A(n74), .B(n116), .ZN(dataout[4]) );
  OR2_X1 U135 ( .A1(n213), .A2(n163), .ZN(n74) );
  XNOR2_X1 U136 ( .A(n75), .B(n112), .ZN(dataout[6]) );
  OR2_X1 U137 ( .A1(n163), .A2(n212), .ZN(n75) );
  XNOR2_X1 U138 ( .A(n76), .B(n89), .ZN(dataout[7]) );
  OR2_X1 U139 ( .A1(n163), .A2(n168), .ZN(n76) );
  AND2_X1 U140 ( .A1(n26), .A2(n181), .ZN(n77) );
  BUF_X1 U141 ( .A(datain[48]), .Z(n102) );
  XNOR2_X1 U142 ( .A(n78), .B(n119), .ZN(dataout[3]) );
  OR2_X1 U143 ( .A1(n163), .A2(n185), .ZN(n78) );
  NOR2_X1 U144 ( .A1(n248), .A2(correct_n), .ZN(n91) );
  XNOR2_X1 U145 ( .A(n79), .B(n105), .ZN(dataout[30]) );
  OR2_X1 U146 ( .A1(n172), .A2(n247), .ZN(n79) );
  XNOR2_X1 U147 ( .A(n80), .B(datain[14]), .ZN(dataout[14]) );
  OR2_X1 U148 ( .A1(n166), .A2(n247), .ZN(n80) );
  AND3_X2 U149 ( .A1(n150), .A2(chkout[5]), .A3(n176), .ZN(n131) );
  XNOR2_X1 U150 ( .A(n81), .B(n100), .ZN(dataout[25]) );
  OR2_X1 U151 ( .A1(n170), .A2(n185), .ZN(n81) );
  XNOR2_X1 U152 ( .A(n82), .B(n113), .ZN(dataout[1]) );
  OR2_X1 U153 ( .A1(correct_n), .A2(n97), .ZN(n82) );
  CLKBUF_X1 U154 ( .A(datain[45]), .Z(n83) );
  CLKBUF_X1 U155 ( .A(datain[28]), .Z(n84) );
  XNOR2_X1 U156 ( .A(n85), .B(n102), .ZN(dataout[48]) );
  OR2_X1 U157 ( .A1(n242), .A2(correct_n), .ZN(n85) );
  NOR2_X1 U158 ( .A1(n154), .A2(n161), .ZN(n86) );
  AND2_X2 U159 ( .A1(n153), .A2(n147), .ZN(n107) );
  CLKBUF_X1 U160 ( .A(n83), .Z(n87) );
  CLKBUF_X1 U161 ( .A(datain[31]), .Z(n88) );
  CLKBUF_X1 U162 ( .A(n33), .Z(n89) );
  NAND3_X1 U163 ( .A1(n128), .A2(n171), .A3(n34), .ZN(n217) );
  OR2_X1 U164 ( .A1(n156), .A2(n123), .ZN(n125) );
  AND2_X2 U165 ( .A1(n151), .A2(n150), .ZN(n130) );
  CLKBUF_X1 U166 ( .A(datain[2]), .Z(n120) );
  INV_X1 U167 ( .A(chkout[2]), .ZN(n90) );
  INV_X1 U168 ( .A(chkout[1]), .ZN(n92) );
  NAND2_X1 U169 ( .A1(n131), .A2(n54), .ZN(n93) );
  XNOR2_X1 U170 ( .A(chkout_int[2]), .B(n123), .ZN(n158) );
  INV_X1 U171 ( .A(n195), .ZN(n94) );
  NAND2_X1 U172 ( .A1(n12), .A2(n108), .ZN(n95) );
  NAND3_X1 U173 ( .A1(n155), .A2(n16), .A3(n130), .ZN(n97) );
  CLKBUF_X1 U174 ( .A(datain[27]), .Z(n98) );
  XNOR2_X1 U175 ( .A(n156), .B(n123), .ZN(n178) );
  CLKBUF_X1 U176 ( .A(datain[24]), .Z(n99) );
  CLKBUF_X1 U177 ( .A(datain[25]), .Z(n100) );
  CLKBUF_X1 U178 ( .A(datain[36]), .Z(n101) );
  NAND2_X1 U179 ( .A1(n173), .A2(n181), .ZN(n103) );
  CLKBUF_X1 U180 ( .A(datain[5]), .Z(n104) );
  CLKBUF_X1 U181 ( .A(datain[30]), .Z(n105) );
  CLKBUF_X1 U182 ( .A(datain[38]), .Z(n106) );
  AND2_X1 U183 ( .A1(n173), .A2(chkout[6]), .ZN(n108) );
  XOR2_X1 U184 ( .A(chkout_int[7]), .B(chkin[7]), .Z(chkout[7]) );
  OR2_X1 U185 ( .A1(n110), .A2(n160), .ZN(n227) );
  XNOR2_X1 U186 ( .A(chkout_int[1]), .B(chkin[1]), .ZN(n110) );
  CLKBUF_X1 U187 ( .A(datain[29]), .Z(n111) );
  CLKBUF_X1 U188 ( .A(datain[6]), .Z(n112) );
  CLKBUF_X1 U189 ( .A(datain[1]), .Z(n113) );
  CLKBUF_X1 U190 ( .A(n84), .Z(n114) );
  CLKBUF_X1 U191 ( .A(datain[4]), .Z(n116) );
  CLKBUF_X1 U192 ( .A(datain[44]), .Z(n117) );
  CLKBUF_X1 U193 ( .A(datain[32]), .Z(n118) );
  CLKBUF_X1 U194 ( .A(datain[39]), .Z(n121) );
  CLKBUF_X1 U195 ( .A(n98), .Z(n122) );
  NAND2_X1 U196 ( .A1(n156), .A2(n123), .ZN(n124) );
  NAND2_X1 U197 ( .A1(n125), .A2(n124), .ZN(n153) );
  INV_X1 U198 ( .A(chkin[2]), .ZN(n123) );
  XOR2_X1 U199 ( .A(chkout_int[6]), .B(chkin[6]), .Z(chkout[6]) );
  OR3_X1 U200 ( .A1(n147), .A2(chkout[2]), .A3(n227), .ZN(n211) );
  NOR3_X1 U201 ( .A1(n220), .A2(n219), .A3(n234), .ZN(n222) );
  INV_X1 U202 ( .A(n218), .ZN(n220) );
  AND2_X1 U203 ( .A1(n191), .A2(n127), .ZN(n197) );
  NAND2_X1 U204 ( .A1(n11), .A2(n142), .ZN(n127) );
  INV_X1 U205 ( .A(n219), .ZN(n200) );
  OR3_X1 U206 ( .A1(n219), .A2(n15), .A3(n247), .ZN(n252) );
  AND2_X1 U207 ( .A1(n31), .A2(chkout[1]), .ZN(n128) );
  OR3_X1 U208 ( .A1(n23), .A2(n249), .A3(n250), .ZN(n251) );
  INV_X1 U209 ( .A(n201), .ZN(n204) );
  AND3_X1 U210 ( .A1(n31), .A2(chkout[1]), .A3(n174), .ZN(n133) );
  XNOR2_X1 U211 ( .A(n150), .B(n144), .ZN(n134) );
  AND2_X1 U212 ( .A1(n150), .A2(n151), .ZN(n135) );
  XNOR2_X1 U213 ( .A(chkout_int[0]), .B(chkin[0]), .ZN(n174) );
  XNOR2_X1 U214 ( .A(chkout_int[1]), .B(chkin[1]), .ZN(n199) );
  XNOR2_X1 U215 ( .A(chkout_int[5]), .B(chkin[5]), .ZN(n151) );
  XNOR2_X1 U216 ( .A(datain[63]), .B(n136), .ZN(dataout[63]) );
  OR2_X1 U217 ( .A1(n193), .A2(n13), .ZN(n136) );
  XOR2_X1 U218 ( .A(chkout_int[1]), .B(chkin[1]), .Z(n137) );
  OR2_X1 U219 ( .A1(n229), .A2(n199), .ZN(n201) );
  CLKBUF_X1 U220 ( .A(datain[42]), .Z(n138) );
  AND2_X1 U221 ( .A1(n168), .A2(n212), .ZN(n214) );
  XNOR2_X1 U222 ( .A(chkout_int[3]), .B(chkin[3]), .ZN(n179) );
  BUF_X1 U223 ( .A(n233), .Z(n139) );
  XNOR2_X1 U224 ( .A(chkout_int[4]), .B(chkin[4]), .ZN(n150) );
  XNOR2_X1 U225 ( .A(chkout_int[3]), .B(chkin[3]), .ZN(n159) );
  OAI211_X1 U226 ( .C1(n241), .C2(n14), .A(n240), .B(n130), .ZN(n244) );
  INV_X1 U227 ( .A(n240), .ZN(n235) );
  AND2_X1 U228 ( .A1(n242), .A2(n97), .ZN(n243) );
  XNOR2_X1 U229 ( .A(chkout_int[5]), .B(n145), .ZN(n144) );
  INV_X1 U230 ( .A(chkin[5]), .ZN(n145) );
  NAND2_X1 U231 ( .A1(n27), .A2(n133), .ZN(n207) );
  AND3_X1 U232 ( .A1(chkout[0]), .A2(n92), .A3(n25), .ZN(n141) );
  NOR3_X1 U233 ( .A1(n223), .A2(n222), .A3(n221), .ZN(n224) );
  INV_X1 U234 ( .A(n202), .ZN(n203) );
  AND2_X1 U235 ( .A1(n179), .A2(n178), .ZN(n142) );
  AND2_X1 U236 ( .A1(n179), .A2(n178), .ZN(n143) );
  OAI211_X1 U237 ( .C1(n237), .C2(n238), .A(n236), .B(n134), .ZN(n245) );
  INV_X1 U238 ( .A(chkout[5]), .ZN(n157) );
  OAI221_X1 U239 ( .B1(n230), .B2(n28), .C1(n94), .C2(n228), .A(n103), .ZN(
        n238) );
  AOI21_X1 U240 ( .B1(n209), .B2(n19), .A(n208), .ZN(n226) );
  AND2_X1 U241 ( .A1(n211), .A2(n210), .ZN(n215) );
  NAND4_X1 U242 ( .A1(n234), .A2(n233), .A3(n232), .A4(n231), .ZN(n237) );
  NOR3_X1 U243 ( .A1(n247), .A2(n103), .A3(n23), .ZN(n221) );
  NAND4_X1 U244 ( .A1(n197), .A2(n247), .A3(n232), .A4(n234), .ZN(n209) );
  NAND4_X1 U245 ( .A1(n215), .A2(n214), .A3(n213), .A4(n185), .ZN(n216) );
  XNOR2_X1 U246 ( .A(chkout_int[0]), .B(chkin[0]), .ZN(n160) );
  NOR4_X1 U247 ( .A1(n256), .A2(n255), .A3(n254), .A4(n30), .ZN(err_multpl) );
  INV_X1 U248 ( .A(n174), .ZN(chkout[0]) );
  INV_X1 U249 ( .A(n110), .ZN(chkout[1]) );
  INV_X1 U250 ( .A(datain[0]), .ZN(n260) );
  INV_X1 U251 ( .A(chkout_int[2]), .ZN(n156) );
  INV_X1 U252 ( .A(n178), .ZN(chkout[2]) );
  INV_X1 U253 ( .A(chkout_int[4]), .ZN(n148) );
  XNOR2_X1 U254 ( .A(chkout_int[6]), .B(chkin[6]), .ZN(n181) );
  XNOR2_X1 U255 ( .A(chkout_int[7]), .B(chkin[7]), .ZN(n173) );
  XOR2_X1 U256 ( .A(chkout_int[3]), .B(chkin[3]), .Z(n147) );
  NAND2_X1 U257 ( .A1(n147), .A2(n158), .ZN(n229) );
  NAND2_X1 U258 ( .A1(chkout[7]), .A2(chkout[6]), .ZN(n246) );
  NOR2_X1 U259 ( .A1(n217), .A2(correct_n), .ZN(n149) );
  XOR2_X1 U260 ( .A(datain[0]), .B(n149), .Z(dataout[0]) );
  XOR2_X1 U261 ( .A(chkout_int[3]), .B(chkin[3]), .Z(chkout[3]) );
  NAND2_X1 U262 ( .A1(n153), .A2(chkout[3]), .ZN(n154) );
  INV_X1 U263 ( .A(n227), .ZN(n195) );
  NAND2_X1 U264 ( .A1(n195), .A2(n107), .ZN(n191) );
  INV_X1 U265 ( .A(n191), .ZN(n155) );
  INV_X1 U266 ( .A(correct_n), .ZN(n176) );
  NAND2_X1 U267 ( .A1(n132), .A2(n16), .ZN(n163) );
  NAND3_X1 U268 ( .A1(chkout[0]), .A2(n92), .A3(n25), .ZN(n185) );
  NAND2_X1 U269 ( .A1(n137), .A2(n160), .ZN(n161) );
  INV_X1 U270 ( .A(n161), .ZN(n196) );
  NAND2_X1 U271 ( .A1(n25), .A2(n11), .ZN(n213) );
  INV_X1 U272 ( .A(n160), .ZN(n162) );
  NAND2_X1 U273 ( .A1(n96), .A2(n107), .ZN(n210) );
  NAND2_X1 U274 ( .A1(n31), .A2(n29), .ZN(n168) );
  NAND2_X1 U275 ( .A1(n131), .A2(n115), .ZN(n165) );
  NOR2_X1 U276 ( .A1(n185), .A2(n165), .ZN(n164) );
  XOR2_X1 U277 ( .A(n164), .B(datain[9]), .Z(dataout[9]) );
  NAND3_X1 U278 ( .A1(n179), .A2(n90), .A3(n96), .ZN(n247) );
  NAND2_X1 U279 ( .A1(n140), .A2(n144), .ZN(n248) );
  NAND2_X1 U280 ( .A1(n115), .A2(n59), .ZN(n166) );
  NAND2_X1 U281 ( .A1(n25), .A2(n29), .ZN(n189) );
  NAND2_X1 U282 ( .A1(n115), .A2(n135), .ZN(n167) );
  NAND2_X1 U283 ( .A1(n12), .A2(n115), .ZN(n169) );
  NAND2_X1 U284 ( .A1(n131), .A2(n171), .ZN(n170) );
  NAND2_X1 U285 ( .A1(n91), .A2(n16), .ZN(n172) );
  NAND2_X1 U286 ( .A1(chkout[6]), .A2(n173), .ZN(n198) );
  NAND2_X1 U287 ( .A1(n130), .A2(n108), .ZN(n202) );
  NOR4_X1 U288 ( .A1(n201), .A2(n202), .A3(n174), .A4(correct_n), .ZN(n175) );
  XOR2_X1 U289 ( .A(n175), .B(n118), .Z(dataout[32]) );
  NAND3_X1 U290 ( .A1(n86), .A2(n130), .A3(n176), .ZN(n187) );
  NOR2_X1 U291 ( .A1(n187), .A2(n13), .ZN(n177) );
  XOR2_X1 U292 ( .A(n177), .B(datain[33]), .Z(dataout[33]) );
  NAND2_X1 U293 ( .A1(n196), .A2(n143), .ZN(n231) );
  NAND2_X1 U294 ( .A1(n12), .A2(n108), .ZN(n180) );
  NAND2_X1 U295 ( .A1(n32), .A2(n25), .ZN(n190) );
  NAND2_X1 U296 ( .A1(n107), .A2(n129), .ZN(n232) );
  NAND2_X1 U297 ( .A1(n31), .A2(n96), .ZN(n192) );
  NAND2_X1 U298 ( .A1(n26), .A2(n181), .ZN(n250) );
  NAND2_X1 U299 ( .A1(n131), .A2(n77), .ZN(n183) );
  NOR2_X1 U300 ( .A1(n183), .A2(n189), .ZN(n182) );
  XOR2_X1 U301 ( .A(n182), .B(datain[41]), .Z(dataout[41]) );
  NAND2_X1 U302 ( .A1(n129), .A2(n142), .ZN(n249) );
  NOR3_X1 U303 ( .A1(n48), .A2(n249), .A3(n250), .ZN(n184) );
  XOR2_X1 U304 ( .A(n184), .B(datain[46]), .Z(dataout[46]) );
  NAND2_X1 U305 ( .A1(n141), .A2(n91), .ZN(n193) );
  NOR2_X1 U306 ( .A1(n193), .A2(n250), .ZN(n186) );
  XOR2_X1 U307 ( .A(datain[47]), .B(n186), .Z(dataout[47]) );
  NAND4_X1 U308 ( .A1(n128), .A2(chkout[0]), .A3(n77), .A4(n130), .ZN(n242) );
  NAND2_X1 U309 ( .A1(n132), .A2(n77), .ZN(n188) );
  INV_X1 U310 ( .A(n249), .ZN(n194) );
  NAND3_X1 U311 ( .A1(n194), .A2(n200), .A3(n146), .ZN(n253) );
  NAND2_X1 U312 ( .A1(n107), .A2(n195), .ZN(n233) );
  NAND2_X1 U313 ( .A1(n29), .A2(n25), .ZN(n234) );
  NAND2_X1 U314 ( .A1(n250), .A2(n198), .ZN(n240) );
  NAND2_X1 U315 ( .A1(n140), .A2(n144), .ZN(n219) );
  NAND3_X1 U316 ( .A1(n141), .A2(n200), .A3(n240), .ZN(n206) );
  NAND3_X1 U317 ( .A1(chkout[0]), .A2(n204), .A3(n203), .ZN(n205) );
  NAND3_X1 U318 ( .A1(n205), .A2(n206), .A3(n207), .ZN(n208) );
  NAND2_X1 U319 ( .A1(n103), .A2(n246), .ZN(n218) );
  NAND3_X1 U320 ( .A1(n216), .A2(n134), .A3(n218), .ZN(n225) );
  NAND2_X1 U321 ( .A1(err_detect), .A2(n217), .ZN(n223) );
  NAND3_X1 U322 ( .A1(n226), .A2(n225), .A3(n224), .ZN(n256) );
  NAND2_X1 U323 ( .A1(n143), .A2(n29), .ZN(n239) );
  NAND2_X1 U324 ( .A1(n235), .A2(n239), .ZN(n236) );
  INV_X1 U325 ( .A(n239), .ZN(n241) );
  NAND3_X1 U326 ( .A1(n245), .A2(n244), .A3(n243), .ZN(n255) );
  NAND2_X1 U327 ( .A1(n252), .A2(n251), .ZN(n254) );
endmodule


module DW_ecc_decode_inst ( allow_correct_n, wr_data_sys, rd_data_mem, 
        rd_checkbits, rd_error, real_bad_rd_error, wr_checkbits, rd_data_sys, 
        error_syndrome );
  input [63:0] wr_data_sys;
  input [63:0] rd_data_mem;
  input [7:0] rd_checkbits;
  output [7:0] wr_checkbits;
  output [63:0] rd_data_sys;
  output [7:0] error_syndrome;
  input allow_correct_n;
  output rd_error, real_bad_rd_error;


  DW_ecc_decode_inst_DW_ecc_0 U1 ( .gen(1'b0), .correct_n(allow_correct_n), 
        .datain(rd_data_mem), .chkin(rd_checkbits), .err_detect(rd_error), 
        .err_multpl(real_bad_rd_error), .dataout(rd_data_sys), .chkout(
        error_syndrome) );
endmodule

