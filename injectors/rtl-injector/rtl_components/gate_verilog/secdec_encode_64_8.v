/////////////////////////////////////////////////////////////
// Created by: Synopsys DC Expert(TM) in wire load mode
// Version   : L-2016.03-SP5-3
// Date      : Wed Sep  5 10:50:00 2018
/////////////////////////////////////////////////////////////


module DW_ecc_encode_inst_DW04_par_gen_9 ( datain, parity );
  input [62:0] datain;
  output parity;
  wire   n1, n2, n3, n4, n5, n6, n7, n8, n9, n10, n11, n12, n13, n14, n15, n16,
         n17, n18, n19, n20, n21, n22, n23, n24, n25, n26, n27, n28, n29, n30,
         n31;

  XNOR2_X1 U1 ( .A(n1), .B(n2), .ZN(n29) );
  XNOR2_X1 U2 ( .A(datain[21]), .B(datain[20]), .ZN(n1) );
  XNOR2_X1 U3 ( .A(datain[11]), .B(datain[12]), .ZN(n2) );
  INV_X1 U4 ( .A(datain[31]), .ZN(n8) );
  XOR2_X1 U5 ( .A(datain[23]), .B(datain[22]), .Z(n3) );
  XOR2_X1 U6 ( .A(n9), .B(datain[29]), .Z(n6) );
  XNOR2_X1 U7 ( .A(n6), .B(n7), .ZN(n5) );
  XNOR2_X1 U8 ( .A(n8), .B(datain[30]), .ZN(n7) );
  XNOR2_X1 U9 ( .A(n4), .B(n5), .ZN(parity) );
  XNOR2_X1 U10 ( .A(n10), .B(n11), .ZN(n9) );
  XNOR2_X1 U11 ( .A(datain[28]), .B(datain[27]), .ZN(n10) );
  XNOR2_X1 U12 ( .A(n12), .B(n13), .ZN(n4) );
  XNOR2_X1 U13 ( .A(datain[26]), .B(datain[25]), .ZN(n11) );
  XNOR2_X1 U14 ( .A(n27), .B(n3), .ZN(n12) );
  XNOR2_X1 U15 ( .A(n14), .B(n15), .ZN(n13) );
  XNOR2_X1 U16 ( .A(datain[24]), .B(datain[19]), .ZN(n31) );
  XNOR2_X1 U17 ( .A(n16), .B(n17), .ZN(n15) );
  XNOR2_X1 U18 ( .A(n21), .B(n22), .ZN(n14) );
  XNOR2_X1 U19 ( .A(n18), .B(datain[10]), .ZN(n17) );
  XNOR2_X1 U20 ( .A(n29), .B(n28), .ZN(n27) );
  XNOR2_X1 U21 ( .A(n25), .B(n26), .ZN(n21) );
  XNOR2_X1 U22 ( .A(datain[15]), .B(datain[0]), .ZN(n23) );
  XNOR2_X1 U23 ( .A(n30), .B(n31), .ZN(n28) );
  XNOR2_X1 U24 ( .A(datain[18]), .B(datain[13]), .ZN(n30) );
  XNOR2_X1 U25 ( .A(datain[6]), .B(datain[5]), .ZN(n25) );
  XNOR2_X1 U26 ( .A(datain[8]), .B(datain[7]), .ZN(n26) );
  XNOR2_X1 U27 ( .A(datain[9]), .B(datain[3]), .ZN(n19) );
  XNOR2_X1 U28 ( .A(datain[14]), .B(datain[17]), .ZN(n18) );
  XNOR2_X1 U29 ( .A(n23), .B(n24), .ZN(n22) );
  XNOR2_X1 U30 ( .A(datain[4]), .B(datain[16]), .ZN(n24) );
  XNOR2_X1 U31 ( .A(n19), .B(n20), .ZN(n16) );
  XNOR2_X1 U32 ( .A(datain[1]), .B(datain[2]), .ZN(n20) );
endmodule


module DW_ecc_encode_inst_DW04_par_gen_11 ( datain, parity );
  input [62:0] datain;
  output parity;
  wire   n1, n2, n3, n4, n5, n6, n7, n8, n9, n10, n11, n12, n13, n14, n15, n16,
         n17, n18, n19, n20, n21, n22, n23, n24, n25, n26, n27, n28, n29, n30,
         n31, n32;

  XNOR2_X1 U1 ( .A(n1), .B(n2), .ZN(n19) );
  XNOR2_X1 U2 ( .A(datain[3]), .B(datain[9]), .ZN(n1) );
  XNOR2_X1 U3 ( .A(datain[1]), .B(datain[2]), .ZN(n2) );
  XNOR2_X1 U4 ( .A(n3), .B(n4), .ZN(n29) );
  XNOR2_X1 U5 ( .A(datain[18]), .B(datain[13]), .ZN(n3) );
  XNOR2_X1 U6 ( .A(datain[24]), .B(datain[19]), .ZN(n4) );
  XNOR2_X1 U7 ( .A(n9), .B(n5), .ZN(n8) );
  XNOR2_X1 U8 ( .A(n10), .B(datain[30]), .ZN(n5) );
  INV_X1 U9 ( .A(datain[31]), .ZN(n10) );
  XOR2_X1 U10 ( .A(datain[23]), .B(datain[22]), .Z(n6) );
  XNOR2_X1 U11 ( .A(n11), .B(n12), .ZN(n9) );
  INV_X1 U12 ( .A(datain[29]), .ZN(n12) );
  XNOR2_X1 U13 ( .A(n13), .B(n14), .ZN(n11) );
  XNOR2_X1 U14 ( .A(n7), .B(n8), .ZN(parity) );
  XNOR2_X1 U15 ( .A(n15), .B(n16), .ZN(n7) );
  XNOR2_X1 U16 ( .A(datain[28]), .B(datain[27]), .ZN(n13) );
  XNOR2_X1 U17 ( .A(n28), .B(n6), .ZN(n15) );
  XNOR2_X1 U18 ( .A(n18), .B(n17), .ZN(n16) );
  XNOR2_X1 U19 ( .A(n29), .B(n30), .ZN(n28) );
  XNOR2_X1 U20 ( .A(datain[26]), .B(datain[25]), .ZN(n14) );
  XNOR2_X1 U21 ( .A(n22), .B(n23), .ZN(n17) );
  XNOR2_X1 U22 ( .A(n19), .B(n20), .ZN(n18) );
  XNOR2_X1 U23 ( .A(n26), .B(n27), .ZN(n22) );
  XNOR2_X1 U24 ( .A(datain[8]), .B(datain[7]), .ZN(n27) );
  XNOR2_X1 U25 ( .A(datain[6]), .B(datain[5]), .ZN(n26) );
  XNOR2_X1 U26 ( .A(n21), .B(datain[10]), .ZN(n20) );
  XNOR2_X1 U27 ( .A(datain[12]), .B(datain[11]), .ZN(n32) );
  XNOR2_X1 U28 ( .A(n24), .B(n25), .ZN(n23) );
  XNOR2_X1 U29 ( .A(datain[16]), .B(datain[4]), .ZN(n25) );
  XNOR2_X1 U30 ( .A(n31), .B(n32), .ZN(n30) );
  XNOR2_X1 U31 ( .A(datain[21]), .B(datain[20]), .ZN(n31) );
  XNOR2_X1 U32 ( .A(datain[15]), .B(datain[0]), .ZN(n24) );
  XNOR2_X1 U33 ( .A(datain[14]), .B(datain[17]), .ZN(n21) );
endmodule


module DW_ecc_encode_inst_DW04_par_gen_13 ( datain, parity );
  input [62:0] datain;
  output parity;
  wire   n1, n2, n3, n4, n5, n6, n7, n8, n9, n10, n11, n12, n13, n14, n15, n16,
         n17, n18, n19, n20, n21, n22, n23, n24, n25, n26, n27, n28, n29, n30,
         n31;

  XNOR2_X1 U1 ( .A(n1), .B(n2), .ZN(n24) );
  XNOR2_X1 U2 ( .A(datain[15]), .B(datain[0]), .ZN(n1) );
  XNOR2_X1 U3 ( .A(datain[16]), .B(datain[4]), .ZN(n2) );
  XOR2_X1 U4 ( .A(n3), .B(datain[29]), .Z(n10) );
  XNOR2_X1 U5 ( .A(n12), .B(n13), .ZN(n3) );
  XNOR2_X1 U6 ( .A(n10), .B(n4), .ZN(n9) );
  XNOR2_X1 U7 ( .A(n11), .B(datain[30]), .ZN(n4) );
  XNOR2_X1 U8 ( .A(n25), .B(n26), .ZN(n23) );
  XNOR2_X1 U9 ( .A(n5), .B(n6), .ZN(n28) );
  XOR2_X1 U10 ( .A(datain[18]), .B(datain[13]), .Z(n5) );
  XOR2_X1 U11 ( .A(datain[19]), .B(datain[24]), .Z(n6) );
  XOR2_X1 U12 ( .A(datain[23]), .B(datain[22]), .Z(n7) );
  INV_X1 U13 ( .A(datain[31]), .ZN(n11) );
  XNOR2_X1 U14 ( .A(n8), .B(n9), .ZN(parity) );
  XNOR2_X1 U15 ( .A(n14), .B(n15), .ZN(n8) );
  XNOR2_X1 U16 ( .A(n27), .B(n7), .ZN(n14) );
  XNOR2_X1 U17 ( .A(n24), .B(n23), .ZN(n16) );
  XNOR2_X1 U18 ( .A(n21), .B(n22), .ZN(n18) );
  XNOR2_X1 U19 ( .A(n28), .B(n29), .ZN(n27) );
  XNOR2_X1 U20 ( .A(datain[14]), .B(datain[17]), .ZN(n20) );
  XNOR2_X1 U21 ( .A(datain[28]), .B(datain[27]), .ZN(n12) );
  XNOR2_X1 U22 ( .A(datain[26]), .B(datain[25]), .ZN(n13) );
  XNOR2_X1 U23 ( .A(datain[6]), .B(datain[5]), .ZN(n25) );
  XNOR2_X1 U24 ( .A(datain[8]), .B(datain[7]), .ZN(n26) );
  XNOR2_X1 U25 ( .A(n30), .B(n31), .ZN(n29) );
  XNOR2_X1 U26 ( .A(datain[9]), .B(datain[3]), .ZN(n21) );
  XNOR2_X1 U27 ( .A(datain[12]), .B(datain[11]), .ZN(n31) );
  XNOR2_X1 U28 ( .A(n16), .B(n17), .ZN(n15) );
  XNOR2_X1 U29 ( .A(datain[1]), .B(datain[2]), .ZN(n22) );
  XNOR2_X1 U30 ( .A(n18), .B(n19), .ZN(n17) );
  XNOR2_X1 U31 ( .A(n20), .B(datain[10]), .ZN(n19) );
  XNOR2_X1 U32 ( .A(datain[20]), .B(datain[21]), .ZN(n30) );
endmodule


module DW_ecc_encode_inst_DW04_par_gen_12 ( datain, parity );
  input [62:0] datain;
  output parity;
  wire   n1, n2, n3, n4, n5, n6, n7, n8, n9, n10, n11, n12, n13, n14, n15, n16,
         n17, n18, n19, n20, n21, n22, n23, n24, n25, n26, n27, n28, n29, n30,
         n31;

  XOR2_X1 U1 ( .A(n26), .B(n27), .Z(n23) );
  XNOR2_X1 U2 ( .A(n23), .B(n24), .ZN(n22) );
  XNOR2_X1 U3 ( .A(n25), .B(n3), .ZN(n24) );
  XNOR2_X1 U4 ( .A(n28), .B(n29), .ZN(n21) );
  XNOR2_X1 U5 ( .A(n1), .B(n30), .ZN(n29) );
  XOR2_X1 U6 ( .A(datain[23]), .B(datain[22]), .Z(n1) );
  XNOR2_X1 U7 ( .A(datain[26]), .B(datain[25]), .ZN(n25) );
  XNOR2_X1 U8 ( .A(n2), .B(n31), .ZN(n28) );
  XOR2_X1 U9 ( .A(datain[18]), .B(datain[13]), .Z(n2) );
  XNOR2_X1 U10 ( .A(n17), .B(n18), .ZN(n8) );
  XNOR2_X1 U11 ( .A(n20), .B(n5), .ZN(n17) );
  XNOR2_X1 U12 ( .A(n19), .B(n4), .ZN(n18) );
  XOR2_X1 U13 ( .A(datain[28]), .B(datain[27]), .Z(n3) );
  XOR2_X1 U14 ( .A(datain[17]), .B(datain[14]), .Z(n4) );
  XOR2_X1 U15 ( .A(datain[9]), .B(datain[3]), .Z(n5) );
  INV_X1 U16 ( .A(datain[31]), .ZN(n27) );
  XNOR2_X1 U17 ( .A(n9), .B(n8), .ZN(n7) );
  XNOR2_X1 U18 ( .A(n10), .B(n11), .ZN(n9) );
  XNOR2_X1 U19 ( .A(datain[30]), .B(datain[29]), .ZN(n26) );
  XNOR2_X1 U20 ( .A(n7), .B(n6), .ZN(parity) );
  XNOR2_X1 U21 ( .A(datain[12]), .B(datain[11]), .ZN(n30) );
  XNOR2_X1 U22 ( .A(n15), .B(datain[0]), .ZN(n10) );
  XNOR2_X1 U23 ( .A(datain[6]), .B(datain[5]), .ZN(n14) );
  XNOR2_X1 U24 ( .A(datain[8]), .B(datain[7]), .ZN(n13) );
  XNOR2_X1 U25 ( .A(datain[21]), .B(datain[20]), .ZN(n19) );
  XNOR2_X1 U26 ( .A(datain[16]), .B(datain[4]), .ZN(n16) );
  XNOR2_X1 U27 ( .A(datain[24]), .B(datain[19]), .ZN(n31) );
  XNOR2_X1 U28 ( .A(datain[2]), .B(datain[1]), .ZN(n20) );
  XNOR2_X1 U29 ( .A(n12), .B(datain[10]), .ZN(n11) );
  XNOR2_X1 U30 ( .A(n13), .B(n14), .ZN(n12) );
  XNOR2_X1 U31 ( .A(n16), .B(datain[15]), .ZN(n15) );
  XNOR2_X1 U32 ( .A(n21), .B(n22), .ZN(n6) );
endmodule


module DW_ecc_encode_inst_DW04_par_gen_14 ( datain, parity );
  input [62:0] datain;
  output parity;
  wire   n1, n2, n3, n4, n5, n6, n7, n8, n9, n10, n11, n12, n13, n14, n15, n16,
         n17, n18, n19, n20, n21, n22, n23, n24, n25, n26, n27, n28, n29, n30,
         n31;

  XOR2_X1 U1 ( .A(n26), .B(n27), .Z(n23) );
  XNOR2_X1 U2 ( .A(n23), .B(n24), .ZN(n22) );
  XNOR2_X1 U3 ( .A(n25), .B(n5), .ZN(n24) );
  XNOR2_X1 U4 ( .A(n28), .B(n29), .ZN(n21) );
  XNOR2_X1 U5 ( .A(n1), .B(n30), .ZN(n29) );
  XOR2_X1 U6 ( .A(datain[23]), .B(datain[22]), .Z(n1) );
  XNOR2_X1 U7 ( .A(datain[30]), .B(datain[29]), .ZN(n26) );
  XNOR2_X1 U8 ( .A(datain[26]), .B(datain[25]), .ZN(n25) );
  XNOR2_X1 U9 ( .A(n2), .B(n31), .ZN(n28) );
  XOR2_X1 U10 ( .A(datain[18]), .B(datain[13]), .Z(n2) );
  XNOR2_X1 U11 ( .A(n17), .B(n18), .ZN(n8) );
  XNOR2_X1 U12 ( .A(n20), .B(n4), .ZN(n17) );
  XNOR2_X1 U13 ( .A(n19), .B(n3), .ZN(n18) );
  INV_X1 U14 ( .A(datain[31]), .ZN(n27) );
  XOR2_X1 U15 ( .A(datain[17]), .B(datain[14]), .Z(n3) );
  XOR2_X1 U16 ( .A(datain[9]), .B(datain[3]), .Z(n4) );
  XOR2_X1 U17 ( .A(datain[28]), .B(datain[27]), .Z(n5) );
  XNOR2_X1 U18 ( .A(n7), .B(n6), .ZN(parity) );
  XNOR2_X1 U19 ( .A(n9), .B(n8), .ZN(n7) );
  XNOR2_X1 U20 ( .A(n10), .B(n11), .ZN(n9) );
  XNOR2_X1 U21 ( .A(n15), .B(datain[0]), .ZN(n10) );
  XNOR2_X1 U22 ( .A(datain[8]), .B(datain[7]), .ZN(n13) );
  XNOR2_X1 U23 ( .A(datain[6]), .B(datain[5]), .ZN(n14) );
  XNOR2_X1 U24 ( .A(datain[12]), .B(datain[11]), .ZN(n30) );
  XNOR2_X1 U25 ( .A(datain[21]), .B(datain[20]), .ZN(n19) );
  XNOR2_X1 U26 ( .A(datain[2]), .B(datain[1]), .ZN(n20) );
  XNOR2_X1 U27 ( .A(datain[24]), .B(datain[19]), .ZN(n31) );
  XNOR2_X1 U28 ( .A(datain[4]), .B(datain[16]), .ZN(n16) );
  XNOR2_X1 U29 ( .A(n12), .B(datain[10]), .ZN(n11) );
  XNOR2_X1 U30 ( .A(n13), .B(n14), .ZN(n12) );
  XNOR2_X1 U31 ( .A(n16), .B(datain[15]), .ZN(n15) );
  XNOR2_X1 U32 ( .A(n21), .B(n22), .ZN(n6) );
endmodule


module DW_ecc_encode_inst_DW04_par_gen_8 ( datain, parity );
  input [62:0] datain;
  output parity;
  wire   n1, n2, n3, n4, n5, n6, n7, n8, n9, n10, n11, n12, n13, n14, n15, n16,
         n17, n18, n19, n20, n21, n22, n23, n24, n25, n26, n27, n28, n29, n30,
         n31, n32;

  XNOR2_X1 U1 ( .A(n1), .B(n2), .ZN(n25) );
  XNOR2_X1 U2 ( .A(datain[6]), .B(datain[5]), .ZN(n1) );
  XOR2_X1 U3 ( .A(datain[8]), .B(datain[7]), .Z(n2) );
  XNOR2_X1 U4 ( .A(n11), .B(n12), .ZN(n10) );
  INV_X1 U5 ( .A(datain[31]), .ZN(n15) );
  XOR2_X1 U6 ( .A(datain[24]), .B(datain[19]), .Z(n3) );
  XOR2_X1 U7 ( .A(datain[23]), .B(datain[22]), .Z(n4) );
  XNOR2_X1 U8 ( .A(n5), .B(n6), .ZN(n13) );
  XOR2_X1 U9 ( .A(datain[28]), .B(datain[27]), .Z(n5) );
  XOR2_X1 U10 ( .A(datain[26]), .B(datain[25]), .Z(n6) );
  INV_X1 U11 ( .A(datain[30]), .ZN(n14) );
  XNOR2_X1 U12 ( .A(n27), .B(n7), .ZN(n26) );
  XOR2_X1 U13 ( .A(datain[4]), .B(datain[16]), .Z(n7) );
  XNOR2_X1 U14 ( .A(n30), .B(n29), .ZN(n28) );
  XNOR2_X1 U15 ( .A(n32), .B(n3), .ZN(n29) );
  XNOR2_X1 U16 ( .A(n8), .B(n31), .ZN(n30) );
  XNOR2_X1 U17 ( .A(n25), .B(n26), .ZN(n18) );
  XOR2_X1 U18 ( .A(datain[12]), .B(datain[11]), .Z(n8) );
  XNOR2_X1 U19 ( .A(datain[2]), .B(datain[1]), .ZN(n24) );
  XNOR2_X1 U20 ( .A(datain[9]), .B(datain[3]), .ZN(n23) );
  XNOR2_X1 U21 ( .A(datain[18]), .B(datain[13]), .ZN(n32) );
  XNOR2_X1 U22 ( .A(datain[21]), .B(datain[20]), .ZN(n31) );
  XNOR2_X1 U23 ( .A(datain[15]), .B(datain[0]), .ZN(n27) );
  XNOR2_X1 U24 ( .A(datain[17]), .B(datain[14]), .ZN(n22) );
  XNOR2_X1 U25 ( .A(n16), .B(n17), .ZN(n9) );
  XNOR2_X1 U26 ( .A(n9), .B(n10), .ZN(parity) );
  XNOR2_X1 U27 ( .A(n13), .B(datain[29]), .ZN(n12) );
  XNOR2_X1 U28 ( .A(n14), .B(n15), .ZN(n11) );
  XNOR2_X1 U29 ( .A(n18), .B(n19), .ZN(n17) );
  XNOR2_X1 U30 ( .A(n20), .B(n21), .ZN(n19) );
  XNOR2_X1 U31 ( .A(n22), .B(datain[10]), .ZN(n21) );
  XNOR2_X1 U32 ( .A(n23), .B(n24), .ZN(n20) );
  XNOR2_X1 U33 ( .A(n28), .B(n4), .ZN(n16) );
endmodule


module DW_ecc_encode_inst_DW04_par_gen_10 ( datain, parity );
  input [62:0] datain;
  output parity;
  wire   n1, n2, n3, n4, n5, n6, n7, n8, n9, n10, n11, n12, n13, n14, n15, n16,
         n17, n18, n19, n20, n21, n22, n23, n24, n25, n26, n27, n28, n29, n30,
         n31, n32;

  XNOR2_X1 U1 ( .A(n1), .B(n2), .ZN(n26) );
  XNOR2_X1 U2 ( .A(datain[15]), .B(datain[0]), .ZN(n1) );
  XOR2_X1 U3 ( .A(datain[4]), .B(datain[16]), .Z(n2) );
  XNOR2_X1 U4 ( .A(n11), .B(n12), .ZN(n10) );
  XOR2_X1 U5 ( .A(datain[23]), .B(datain[22]), .Z(n3) );
  XNOR2_X1 U6 ( .A(n4), .B(n5), .ZN(n13) );
  XOR2_X1 U7 ( .A(datain[28]), .B(datain[27]), .Z(n4) );
  XOR2_X1 U8 ( .A(datain[26]), .B(datain[25]), .Z(n5) );
  INV_X1 U9 ( .A(datain[31]), .ZN(n15) );
  XOR2_X1 U10 ( .A(datain[12]), .B(datain[11]), .Z(n6) );
  XNOR2_X1 U11 ( .A(n7), .B(n27), .ZN(n25) );
  XOR2_X1 U12 ( .A(datain[6]), .B(datain[5]), .Z(n7) );
  XNOR2_X1 U13 ( .A(n32), .B(n8), .ZN(n29) );
  XOR2_X1 U14 ( .A(datain[24]), .B(datain[19]), .Z(n8) );
  INV_X1 U15 ( .A(datain[30]), .ZN(n14) );
  XNOR2_X1 U16 ( .A(n30), .B(n29), .ZN(n28) );
  XNOR2_X1 U17 ( .A(n6), .B(n31), .ZN(n30) );
  XNOR2_X1 U18 ( .A(datain[18]), .B(datain[13]), .ZN(n32) );
  XNOR2_X1 U19 ( .A(n25), .B(n26), .ZN(n18) );
  XNOR2_X1 U20 ( .A(datain[21]), .B(datain[20]), .ZN(n31) );
  XNOR2_X1 U21 ( .A(datain[2]), .B(datain[1]), .ZN(n24) );
  XNOR2_X1 U22 ( .A(datain[8]), .B(datain[7]), .ZN(n27) );
  XNOR2_X1 U23 ( .A(datain[9]), .B(datain[3]), .ZN(n23) );
  XNOR2_X1 U24 ( .A(datain[17]), .B(datain[14]), .ZN(n22) );
  XNOR2_X1 U25 ( .A(n16), .B(n17), .ZN(n9) );
  XNOR2_X1 U26 ( .A(n9), .B(n10), .ZN(parity) );
  XNOR2_X1 U27 ( .A(n13), .B(datain[29]), .ZN(n12) );
  XNOR2_X1 U28 ( .A(n14), .B(n15), .ZN(n11) );
  XNOR2_X1 U29 ( .A(n18), .B(n19), .ZN(n17) );
  XNOR2_X1 U30 ( .A(n20), .B(n21), .ZN(n19) );
  XNOR2_X1 U31 ( .A(n22), .B(datain[10]), .ZN(n21) );
  XNOR2_X1 U32 ( .A(n23), .B(n24), .ZN(n20) );
  XNOR2_X1 U33 ( .A(n28), .B(n3), .ZN(n16) );
endmodule


module DW_ecc_encode_inst_DW04_par_gen_15 ( datain, parity );
  input [62:0] datain;
  output parity;
  wire   n1, n2, n3, n4, n5, n6, n7, n8, n9, n10, n11, n12, n13, n14, n15, n16,
         n17, n18, n19, n20, n21, n22, n23, n24, n25, n26, n27, n28, n29, n30,
         n31, n32;

  XNOR2_X1 U1 ( .A(n12), .B(n13), .ZN(n11) );
  INV_X1 U2 ( .A(datain[31]), .ZN(n16) );
  XNOR2_X1 U3 ( .A(n1), .B(n32), .ZN(n29) );
  XOR2_X1 U4 ( .A(datain[18]), .B(datain[13]), .Z(n1) );
  XOR2_X1 U5 ( .A(datain[12]), .B(datain[11]), .Z(n2) );
  XNOR2_X1 U6 ( .A(n4), .B(n3), .ZN(n21) );
  XOR2_X1 U7 ( .A(datain[3]), .B(datain[9]), .Z(n3) );
  XOR2_X1 U8 ( .A(datain[2]), .B(datain[1]), .Z(n4) );
  XOR2_X1 U9 ( .A(datain[8]), .B(datain[7]), .Z(n5) );
  XOR2_X1 U10 ( .A(datain[23]), .B(datain[22]), .Z(n6) );
  XNOR2_X1 U11 ( .A(n7), .B(n8), .ZN(n14) );
  XOR2_X1 U12 ( .A(datain[28]), .B(datain[27]), .Z(n7) );
  XOR2_X1 U13 ( .A(datain[26]), .B(datain[25]), .Z(n8) );
  INV_X1 U14 ( .A(datain[30]), .ZN(n15) );
  XNOR2_X1 U15 ( .A(n31), .B(n2), .ZN(n30) );
  XNOR2_X1 U16 ( .A(n29), .B(n30), .ZN(n28) );
  XNOR2_X1 U17 ( .A(n9), .B(n26), .ZN(n25) );
  XNOR2_X1 U18 ( .A(n24), .B(n25), .ZN(n19) );
  XNOR2_X1 U19 ( .A(n27), .B(n5), .ZN(n24) );
  XOR2_X1 U20 ( .A(datain[4]), .B(datain[16]), .Z(n9) );
  XNOR2_X1 U21 ( .A(datain[24]), .B(datain[19]), .ZN(n32) );
  XNOR2_X1 U22 ( .A(datain[15]), .B(datain[0]), .ZN(n26) );
  XNOR2_X1 U23 ( .A(datain[21]), .B(datain[20]), .ZN(n31) );
  XNOR2_X1 U24 ( .A(datain[6]), .B(datain[5]), .ZN(n27) );
  XNOR2_X1 U25 ( .A(n18), .B(n17), .ZN(n10) );
  XNOR2_X1 U26 ( .A(datain[17]), .B(datain[14]), .ZN(n23) );
  XNOR2_X1 U27 ( .A(n10), .B(n11), .ZN(parity) );
  XNOR2_X1 U28 ( .A(n14), .B(datain[29]), .ZN(n13) );
  XNOR2_X1 U29 ( .A(n15), .B(n16), .ZN(n12) );
  XNOR2_X1 U30 ( .A(n19), .B(n20), .ZN(n18) );
  XNOR2_X1 U31 ( .A(n21), .B(n22), .ZN(n20) );
  XNOR2_X1 U32 ( .A(n23), .B(datain[10]), .ZN(n22) );
  XNOR2_X1 U33 ( .A(n28), .B(n6), .ZN(n17) );
endmodule


module DW_ecc_encode_inst_DW_ecc_0 ( gen, correct_n, datain, chkin, err_detect, 
        err_multpl, dataout, chkout );
  input [63:0] datain;
  input [7:0] chkin;
  output [63:0] dataout;
  output [7:0] chkout;
  input gen, correct_n;
  output err_detect, err_multpl;
  wire   n1, n2, n3;

  DW_ecc_encode_inst_DW04_par_gen_9 \GEN_CB_8.U_PAR_GEN_6  ( .datain({1'b0, 
        1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 
        1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 
        1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, n2, datain[62:56], datain[39:24], 
        datain[7:0]}), .parity(chkout[6]) );
  DW_ecc_encode_inst_DW04_par_gen_11 \GEN_CB_8.U_PAR_GEN_0  ( .datain({1'b0, 
        1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 
        1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 
        1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, n2, datain[61:60], datain[58], 
        datain[55:54], datain[52], datain[48:47], datain[45:44], datain[42], 
        datain[39:38], datain[36], datain[32], datain[30], datain[27], 
        datain[25:24], datain[21], datain[19:17], datain[14], datain[11], 
        datain[9:8], datain[5], datain[3:1]}), .parity(chkout[0]) );
  DW_ecc_encode_inst_DW04_par_gen_13 \GEN_CB_8.U_PAR_GEN_1  ( .datain({1'b0, 
        1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 
        1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 
        1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, datain[60], datain[58], datain[56], 
        datain[54], datain[52], datain[50:48], datain[44], datain[42], 
        datain[40], datain[38], datain[36], datain[34:32], datain[28], 
        datain[26], datain[24], datain[22], datain[20], datain[18:16], 
        datain[12], datain[10], datain[8], datain[6], datain[4], datain[2:0]}), 
        .parity(chkout[1]) );
  DW_ecc_encode_inst_DW04_par_gen_12 \GEN_CB_8.U_PAR_GEN_2  ( .datain({1'b0, 
        1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 
        1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 
        1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, datain[63], datain[61], 
        datain[58:57], datain[55], datain[52:51], datain[48:47], datain[45], 
        datain[42:41], datain[39], datain[36:35], datain[32:31], datain[29], 
        datain[26:25], datain[23], datain[20:19], datain[16:15], datain[13], 
        datain[10:9], datain[7], datain[4], n1, n3}), .parity(chkout[2]) );
  DW_ecc_encode_inst_DW04_par_gen_14 \GEN_CB_8.U_PAR_GEN_3  ( .datain({1'b0, 
        1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 
        1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 
        1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, datain[61:59], datain[55:53], 
        datain[49:48], datain[45:43], datain[39:37], datain[33:32], 
        datain[29:27], datain[23:21], datain[17:16], datain[13:11], 
        datain[7:5], datain[1], n3}), .parity(chkout[3]) );
  DW_ecc_encode_inst_DW04_par_gen_8 \GEN_CB_8.U_PAR_GEN_5  ( .datain({1'b0, 
        1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 
        1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 
        1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, n2, datain[62:56], datain[47:40], 
        datain[31:24], datain[15:8]}), .parity(chkout[5]) );
  DW_ecc_encode_inst_DW04_par_gen_10 \GEN_CB_8.U_PAR_GEN_7  ( .datain({1'b0, 
        1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 
        1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 
        1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, datain[55:40], datain[31:24], 
        datain[7:0]}), .parity(chkout[7]) );
  DW_ecc_encode_inst_DW04_par_gen_15 \GEN_CB_8.U_PAR_GEN_4  ( .datain({1'b0, 
        1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 
        1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 
        1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, n2, datain[62], datain[55:50], 
        datain[47:46], datain[39:34], datain[31:30], datain[23:18], 
        datain[15:14], datain[7:2]}), .parity(chkout[4]) );
  CLKBUF_X1 U2 ( .A(datain[3]), .Z(n1) );
  BUF_X1 U3 ( .A(datain[63]), .Z(n2) );
  INV_X1 U4 ( .A(datain[0]), .ZN(n3) );
endmodule


module DW_ecc_encode_inst ( allow_correct_n, wr_data_sys, rd_data_mem, 
        rd_checkbits, rd_error, real_bad_rd_error, wr_checkbits, rd_data_sys, 
        error_syndrome );
  input [63:0] wr_data_sys;
  input [63:0] rd_data_mem;
  input [7:0] rd_checkbits;
  output [7:0] wr_checkbits;
  output [63:0] rd_data_sys;
  output [7:0] error_syndrome;
  input allow_correct_n;
  output rd_error, real_bad_rd_error;


  DW_ecc_encode_inst_DW_ecc_0 U2 ( .gen(1'b1), .correct_n(1'b1), .datain(
        wr_data_sys), .chkin({1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0}), 
        .chkout(wr_checkbits) );
endmodule

