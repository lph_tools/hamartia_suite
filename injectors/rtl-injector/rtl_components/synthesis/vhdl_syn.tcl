# Paths
set path [file dirname [info script]]
set libs "$path/../libs"
set gates "$path/../gate_pipelined"

# Setup libraries
puts "Setting up libraries..."
lappend search_path "$path/../libs"
set target_library {NangateOpenCellLibrary.db}
set link_library "$target_library"

# Synthesize each vhdl file
set sources {.}
set files [glob -directory $sources {*.vhdl}]
foreach file $files {
    # Read vhdl
    set filename [file tail $file]
    set top_design [file rootname [file tail $file]]
    set verilog_extension ".v"
    set new_filename $top_design$verilog_extension
    set gatefile [file join $gates $new_filename]

    # Check
    if {[file exists $gatefile]} {
        puts "Gate-vhdl for $filename exists, skipping..."
        continue
    }

    puts "Processing $filename..."
    read_file -f vhdl $file
    current_design $top_design     
    link

    # Configure
    set seq 0
    if {[lsearch [get_ports] clk] < 0} {
        # Combinational
        set_driving_cell -library NangateOpenCellLibrary -lib_cell DFF_X1 [all_inputs]
    } else {
        # Sequential
        set seq 1
        set_driving_cell -library NangateOpenCellLibrary -lib_cell DFF_X1 [remove_from_collection [all_inputs] [get_ports clk]]
    }

    set_load -pin_load 0.000846 [all_outputs]
    set_port_fanout_number 1.0 [all_outputs]
    set_fanout_load 1.0 [all_outputs]
    set_wire_load_mode top

    # Optimize to latency
    set_max_delay 0.0 -from [all_inputs] -to [all_outputs]

    # Compile and save
    if {$seq} {
        compile_ultra
    } else {
        compile
    }
    write -format verilog -hierarchy -output $gatefile

    # Reset
    reset_design
}
