# Paths
set path [file dirname [info script]]
set libs "$path/../libs"
set gates "$path/../gate_work_pipelined"

# Setup libraries
puts "Setting up libraries..."
lappend search_path "$path/../libs"
set synthetic_library "dw_foundation.sldb"
set target_library {NangateOpenCellLibrary.db}
set link_library "$target_library $synthetic_library"

# Synthesize each verilog file
set sources {.}
set files [glob -directory $sources {*.v}]
foreach file $files {
    # Read verilog
    set filename [file tail $file]
    set gatefile [file join $gates $filename]

    # Check
    if {[file exists $gatefile]} {
        puts "Gate-verilog for $filename exists, skipping..."
        continue
    }

    puts "Processing $filename..."
    read_verilog $file
    link

    # Configure
    set_driving_cell -library NangateOpenCellLibrary -lib_cell DFF_X1 [remove_from_collection [all_inputs] [get_ports clk]]

    set_load -pin_load 0.000846 [all_outputs]
    set_port_fanout_number 1.0 [all_outputs]
    set_fanout_load 1.0 [all_outputs]
    set_wire_load_mode top

    # Optimize to latency
    set_max_delay 0.0 -from [all_inputs] -to [all_outputs]

    # Compile and save
    set period 1.0
    regexp {\S+_(\d)_stages.v} $filename full_name num_stages  
    puts "Specify $num_stages stages..."

    create_clock clk -period [expr $period*$num_stages]
    compile_ultra
    create_clock clk -period [expr $period]
    optimize_registers
    write -format verilog -hierarchy -output [file join $gates $filename]

    # Reset
    reset_design
}
