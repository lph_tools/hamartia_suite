# /bin/bash

if [ -n "$1" ]; then
	echo "Make docs..."
	scons -c docs
	scons docs
	echo "Install docs at ${1}..."
	mkdir -p ${1}
	mkdir -p ${1}/pintool
	rm -r ${1}/pintool/*
	cp -r docs/pintool/images ${1}/pintool/images
	cp -r docs/pintool/_build/html/* ${1}/pintool
else
	echo "No destination!"
	echo "Usage: ./install_docs.sh <docs_destination>"
fi
