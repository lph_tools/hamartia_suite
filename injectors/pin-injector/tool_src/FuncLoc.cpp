// Copyright 2015, The University of Texas at Austin 
// All rights reserved.
// 
// THIS FILE IS PART OF THE HAMARTIA FAULT/ERROR ANALYSIS SUITE
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met: 
// 
// 1. Redistributions of source code must retain the above copyright
// notice, this list of conditions and the following disclaimer. 
// 
// 2. Redistributions in binary form must reproduce the above copyright
// notice, this list of conditions and the following disclaimer in the
// documentation and/or other materials provided with the distribution. 
// 
// 3. Neither the name of the copyright holder nor the names of its
// contributors may be used to endorse or promote products derived from
// this software without specific prior written permission. 
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
// FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
// COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
// BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
// ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

/**
 * \file
 * \brief Output call sites of the specified function in dynamic instruction number.
 * 	  This tool is meant to correlate injection results to application source code.
 *
 *  \version 1.0.0
 *  \author Chun-Kai Chang, University of Texas
 *
 *  \addtogroup error_profiler_injector
 *  @{
 */



#include "pin.H"
#include <iostream>
#include <iomanip>
#include <fstream>
#include <string.h>
#include <vector>
#include "utils.h"
#include "oclasses.h"
#include "helpers/instruction.h"
/// Command Line Options

/// Input function name
KNOB<string> KnobTargetFuncName(KNOB_MODE_WRITEONCE, "pintool", "f", "",
                    "function name to instrument" );
/// Instruction classes to filter
 KNOB<string> KnobOperationClasses(KNOB_MODE_WRITEONCE, "pintool",
		"c", "OPS_INT,OPS_FP", "specify the profiled operation Class[es, comma delimited list]");
/// MPI instrumentaion switch
KNOB<BOOL> KnobIsMpi(KNOB_MODE_WRITEONCE, "pintool", "m", "0",
                    "instrument MPI program or not" );
/// MPI rank to instrument
KNOB<UINT32> KnobRankId(KNOB_MODE_WRITEONCE, "pintool", "r", "0",
                    "MPI rank to instrument" );
/* =====================================================================
 * General Instrumentation
 * ===================================================================== */
string targetFuncName;
UINT64 insCount = 0;
UINT64 filteredInsCount = 0;
/// Callsites containers 
std::vector<UINT64> V;
std::vector<UINT64> fV;
/// Operation classes to filter
 std::vector<std::string> oclasses;
/* =====================================================================
 * MPI Instrumentation
 * ===================================================================== */
static UINT32 rank_id = 0;
static UINT64* rank_addr = 0;
BOOL disable_getrank = false;
BOOL is_mpi = false;

VOID Arg2Before(UINT64* in_id){
    if(disable_getrank)
	return;
    rank_addr = in_id;
    //std::cout<< "Before: Pid " << PIN_GetPid() << " has rank id address " << in_id <<"\n";
    //std::cout<< "Before: a = " << in_id <<"\n";
}
VOID RankAfter(){
    if(disable_getrank)
        return;
    int this_rank_id = (int)(*rank_addr);
    //std::cout<< "After: Pid " << PIN_GetPid() << " has rank id " << this_rank_id <<"\n";
    disable_getrank = true;
    PIN_RemoveInstrumentation();
    if(((int)rank_id) != this_rank_id){
        std::cout<< "Rank " << this_rank_id << " Detach!" << endl;
        PIN_Detach();
        return;
    }
} 
VOID GetRankId(IMG img, VOID *v){
    if(disable_getrank)
	return;
    //MPI-C (call by address)
    RTN rankRtn = RTN_FindByName(img, "MPI_Comm_rank");
    //MPI-Fortran (call by reference)
    RTN fortRankRtn = RTN_FindByName(img, "mpi_comm_rank");
    //INFO_PRINT("Looking for Rank Function...")
    if (RTN_Valid(rankRtn)){
        //INFO_PRINT("Found C Rank Function!")
	RTN_Open(rankRtn);
	RTN_InsertCall(rankRtn, IPOINT_BEFORE, (AFUNPTR)Arg2Before,
                       IARG_FUNCARG_ENTRYPOINT_VALUE, 1,//the 2nd arg is &rank
		       IARG_CALL_ORDER, CALL_ORDER_FIRST,
                       IARG_END);
	
        RTN_InsertCall(rankRtn, IPOINT_AFTER, (AFUNPTR)RankAfter,
		       IARG_CALL_ORDER, CALL_ORDER_FIRST+1,
		       IARG_END);
	RTN_Close(rankRtn);
    }
    else if (RTN_Valid(fortRankRtn)){
        //INFO_PRINT("Found Fortran Rank Function!")
	RTN_Open(fortRankRtn);
	RTN_InsertCall(fortRankRtn, IPOINT_BEFORE, (AFUNPTR)Arg2Before,
                       IARG_FUNCARG_ENTRYPOINT_REFERENCE, 1,//the 2nd arg is rank
		       IARG_CALL_ORDER, CALL_ORDER_FIRST,
                       IARG_END);
        RTN_InsertCall(fortRankRtn, IPOINT_AFTER, (AFUNPTR)RankAfter,
		       IARG_CALL_ORDER, CALL_ORDER_FIRST+1,
		       IARG_END);
	RTN_Close(fortRankRtn);

    }
}
/* =====================================================================
 * General Instrumentation
 * ===================================================================== */
/**
 * Record callsite
 *
 */
VOID FoundFunc()
{
    V.push_back(insCount);	
    fV.push_back(filteredInsCount);	
}

/**
 * Update instruction and filtered instruction counts
 *
 * \param ic Instruction count in this BB
 * \param fic Filtered instruction count in this BB
 */
VOID PIN_FAST_ANALYSIS_CALL CountIns(UINT32 ic, UINT32 fic)
{
    insCount += ic;
    filteredInsCount += fic;
}
/**
 * Looking for a certain function name using trace-level instrumentation
 *
 * \param trace Pin Trace
 */
VOID TraceFindFunc(TRACE trace, VOID *v)
{
/*
    if(is_mpi && !disable_getrank){
	return;
    }
*/
    INS insH = BBL_InsHead(TRACE_BblHead(trace));
    RTN rtn = INS_Rtn(insH);
   
    //The latter condition ensures runtime function call instead of loading-time instrumentation  
    if (RTN_Valid(rtn) && INS_Address(insH) == RTN_Address(rtn)) 
    {
	if(RTN_Name(rtn) == targetFuncName){
                INS_InsertCall(insH, IPOINT_BEFORE, (AFUNPTR)FoundFunc, 
                IARG_END);
	}
    }
    for (BBL bbl = TRACE_BblHead(trace); BBL_Valid(bbl); bbl = BBL_Next(bbl)){
	UINT32 inscnt = 0; UINT32 filteredinscnt = 0;
        for (INS ins = BBL_InsHead(bbl); INS_Valid(ins); ins = INS_Next(ins)) {
		if (InsInOperationClasses(ins, oclasses, 0)){
			filteredinscnt++;
		}
		inscnt++;
	}
        BBL_InsertCall(bbl, IPOINT_ANYWHERE, (AFUNPTR)CountIns, IARG_UINT32, inscnt, IARG_UINT32, filteredinscnt, IARG_END);
    }

}

/*
VOID InsFindFunc(INS ins, VOID *v){
	RTN rtn = INS_Rtn(ins);
    	if(RTN_Valid(rtn)){
    	    //SYM sym = RTN_Sym(rtn);
    	    //if(SYM_Valid(sym)){
    	        //string undFuncName = PIN_UndecorateSymbolName(SYM_Name(sym), UNDECORATION_NAME_ONLY);
  		//std::cout<< SYM_Name(sym)<<"\n"; 
  		std::cout<< RTN_Name(rtn)<<"\n"; 
    		//if(undFuncName == targetFuncName){
    		if(RTN_Name(rtn) == targetFuncName){
  			    //std::cout<< "Found target func in image "<< IMG_Name(SEC_Img(RTN_Sec(rtn)))<<"\n"; 
                	    INS_InsertCall(ins, IPOINT_BEFORE, (AFUNPTR)FoundFunc, 
                	        IARG_END);
	        }
	    //}	
	}
	insCount++;
}
*/

VOID Fini(INT32 code, VOID *v){
    std::cout<<targetFuncName << "is called at:" << std::endl;
    std::cout<< std::setw(15) << std::left << "Inst" << std::setw(15) << std::left << "Filtered Inst" << std::endl;
    for(unsigned i=0; i<V.size(); i++)
	std::cout<< std::setw(15) << std::left << V[i] << std::setw(15) << std::left << fV[i] << std::endl;
}

int main (INT32 argc, CHAR *argv[])
{
    // Initialize pin
    //
    if (PIN_Init(argc, argv)) return 0;

    // Initialize symbol processing
    //
    PIN_InitSymbols();

    //Initialize global variables
    targetFuncName = KnobTargetFuncName.Value();
    // Operation class
    oclasses = utils::StringSplit(KnobOperationClasses.Value());   
    
    // MPI program instrumentation
    is_mpi = KnobIsMpi.Value();
    // MPI process to inject error into
    rank_id = KnobRankId.Value();

    if (is_mpi){
	// Compare this MPI rank id to user specified rank id
	// Detach if they mismatch
	    IMG_AddInstrumentFunction(GetRankId, 0);
    } 
 
    TRACE_AddInstrumentFunction( TraceFindFunc, 0 );
    //INS_AddInstrumentFunction( InsFindFunc, 0 );
    
    PIN_AddFiniFunction(Fini, 0);
    // Start the program, never returns
    //
    PIN_StartProgram();
    
    return 0;
}
/** @} */
