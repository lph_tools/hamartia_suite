// Copyright 2015, The University of Texas at Austin 
// All rights reserved.
// 
// THIS FILE IS PART OF THE HAMARTIA FAULT/ERROR ANALYSIS SUITE
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met: 
// 
// 1. Redistributions of source code must retain the above copyright
// notice, this list of conditions and the following disclaimer. 
// 
// 2. Redistributions in binary form must reproduce the above copyright
// notice, this list of conditions and the following disclaimer in the
// documentation and/or other materials provided with the distribution. 
// 
// 3. Neither the name of the copyright holder nor the names of its
// contributors may be used to endorse or promote products derived from
// this software without specific prior written permission. 
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
// FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
// COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
// BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
// ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

/**
 *  \file
 *  \brief Instruction helpers
 *
 *  \version 1.0.0
 *  \author Michael Sullivan, University of Texas
 *  \author Nicholas Kelly, University of Texas
 *
 *  \addtogroup helpers
 *  @{
 */

#ifndef _PIN_INSTRUCTION_HELPER_H
#define _PIN_INSTRUCTION_HELPER_H

// Libs
#include <string>
#include <sstream>

// Pin
#include "pin.H"
#include "instlib.H"

#include <hamartia_api/interface.h>
#include <hamartia_api/utils/memory.h>
#include "operand.h"

namespace pin_instruction_helper
{
	using namespace hamartia_api;

	/// Instruction definition
	typedef struct {
		Operation operation; ///< Operation type (e.g. ADD)
		DataType data_type;  ///< Data type (e.g. INT)
		Packing packing;     ///< Packing (e.g. SCALAR)
		Condition condition; ///< Packing (e.g. SCALAR)
	} inst_t;

	/**
	 * Create instruction (XED to error api) mapping
	 *
	 * \return Mapping, indexed by XED opcode, containing error api instruction components
	 */
	std::map<OPCODE, inst_t> create_inst_map()
	{
		std::map<OPCODE, inst_t> inst_map;

		// Addition
		inst_map[XED_ICLASS_ADD]    = (inst_t) {OP_ADD, DT_UINT,   PK_SCALAR, CN_NONE};
		inst_map[XED_ICLASS_ADDPD]  = (inst_t) {OP_ADD, DT_DOUBLE, PK_PACKED, CN_NONE};
		inst_map[XED_ICLASS_ADDPS]  = (inst_t) {OP_ADD, DT_FLOAT,  PK_PACKED, CN_NONE};
		inst_map[XED_ICLASS_ADDSD]  = (inst_t) {OP_ADD, DT_DOUBLE, PK_SCALAR, CN_NONE};
		inst_map[XED_ICLASS_ADDSS]  = (inst_t) {OP_ADD, DT_FLOAT,  PK_SCALAR, CN_NONE};
		inst_map[XED_ICLASS_VADDPD] = (inst_t) {OP_ADD, DT_DOUBLE, PK_PACKED, CN_NONE};
		inst_map[XED_ICLASS_VADDPS] = (inst_t) {OP_ADD, DT_FLOAT,  PK_PACKED, CN_NONE};
		inst_map[XED_ICLASS_VADDSD] = (inst_t) {OP_ADD, DT_DOUBLE, PK_SCALAR, CN_NONE};
		inst_map[XED_ICLASS_VADDSS] = (inst_t) {OP_ADD, DT_FLOAT,  PK_SCALAR, CN_NONE};

		// Subtration
		inst_map[XED_ICLASS_SUB]    = (inst_t) {OP_SUB, DT_UINT,   PK_SCALAR, CN_NONE};
		inst_map[XED_ICLASS_SUBPD]  = (inst_t) {OP_SUB, DT_DOUBLE, PK_PACKED, CN_NONE};
		inst_map[XED_ICLASS_SUBPS]  = (inst_t) {OP_SUB, DT_FLOAT,  PK_PACKED, CN_NONE};
		inst_map[XED_ICLASS_SUBSD]  = (inst_t) {OP_SUB, DT_DOUBLE, PK_SCALAR, CN_NONE};
		inst_map[XED_ICLASS_SUBSS]  = (inst_t) {OP_SUB, DT_FLOAT,  PK_SCALAR, CN_NONE};
		inst_map[XED_ICLASS_VSUBPD] = (inst_t) {OP_SUB, DT_DOUBLE, PK_PACKED, CN_NONE};
		inst_map[XED_ICLASS_VSUBPS] = (inst_t) {OP_SUB, DT_FLOAT,  PK_PACKED, CN_NONE};
		inst_map[XED_ICLASS_VSUBSD] = (inst_t) {OP_SUB, DT_DOUBLE, PK_SCALAR, CN_NONE};
		inst_map[XED_ICLASS_VSUBSS] = (inst_t) {OP_SUB, DT_FLOAT,  PK_SCALAR, CN_NONE};

		// Multiplication
		inst_map[XED_ICLASS_MUL]    = (inst_t) {OP_MUL, DT_UINT,   PK_SCALAR, CN_NONE};
		inst_map[XED_ICLASS_IMUL]   = (inst_t) {OP_MUL, DT_INT,    PK_SCALAR, CN_NONE};
		inst_map[XED_ICLASS_MULPD]  = (inst_t) {OP_MUL, DT_DOUBLE, PK_PACKED, CN_NONE};
		inst_map[XED_ICLASS_MULPS]  = (inst_t) {OP_MUL, DT_FLOAT,  PK_PACKED, CN_NONE};
		inst_map[XED_ICLASS_MULSD]  = (inst_t) {OP_MUL, DT_DOUBLE, PK_SCALAR, CN_NONE};
		inst_map[XED_ICLASS_MULSS]  = (inst_t) {OP_MUL, DT_FLOAT,  PK_SCALAR, CN_NONE};
		inst_map[XED_ICLASS_VMULPD] = (inst_t) {OP_MUL, DT_DOUBLE, PK_PACKED, CN_NONE};
		inst_map[XED_ICLASS_VMULPS] = (inst_t) {OP_MUL, DT_FLOAT,  PK_PACKED, CN_NONE};
		inst_map[XED_ICLASS_VMULSD] = (inst_t) {OP_MUL, DT_DOUBLE, PK_SCALAR, CN_NONE};
		inst_map[XED_ICLASS_VMULSS] = (inst_t) {OP_MUL, DT_FLOAT,  PK_SCALAR, CN_NONE};

		// Division
		inst_map[XED_ICLASS_DIV]    = (inst_t) {OP_DIV, DT_UINT,   PK_SCALAR, CN_NONE};
		inst_map[XED_ICLASS_IDIV]   = (inst_t) {OP_DIV, DT_INT,    PK_SCALAR, CN_NONE};
		inst_map[XED_ICLASS_DIVPD]  = (inst_t) {OP_DIV, DT_DOUBLE, PK_PACKED, CN_NONE};
		inst_map[XED_ICLASS_DIVPS]  = (inst_t) {OP_DIV, DT_FLOAT,  PK_PACKED, CN_NONE};
		inst_map[XED_ICLASS_DIVSD]  = (inst_t) {OP_DIV, DT_DOUBLE, PK_SCALAR, CN_NONE};
		inst_map[XED_ICLASS_DIVSS]  = (inst_t) {OP_DIV, DT_FLOAT,  PK_SCALAR, CN_NONE};
		inst_map[XED_ICLASS_VDIVPD] = (inst_t) {OP_DIV, DT_DOUBLE, PK_PACKED, CN_NONE};
		inst_map[XED_ICLASS_VDIVPS] = (inst_t) {OP_DIV, DT_FLOAT,  PK_PACKED, CN_NONE};
		inst_map[XED_ICLASS_VDIVSD] = (inst_t) {OP_DIV, DT_DOUBLE, PK_SCALAR, CN_NONE};
		inst_map[XED_ICLASS_VDIVSS] = (inst_t) {OP_DIV, DT_FLOAT,  PK_SCALAR, CN_NONE};

		// Square-root
		inst_map[XED_ICLASS_SQRTPD]  = (inst_t) {OP_SQRT, DT_DOUBLE, PK_PACKED, CN_NONE};
		inst_map[XED_ICLASS_SQRTPS]  = (inst_t) {OP_SQRT, DT_FLOAT,  PK_PACKED, CN_NONE};
		inst_map[XED_ICLASS_SQRTSD]  = (inst_t) {OP_SQRT, DT_DOUBLE, PK_SCALAR, CN_NONE};
		inst_map[XED_ICLASS_SQRTSS]  = (inst_t) {OP_SQRT, DT_FLOAT,  PK_SCALAR, CN_NONE};
		inst_map[XED_ICLASS_VSQRTPD] = (inst_t) {OP_SQRT, DT_DOUBLE, PK_PACKED, CN_NONE};
		inst_map[XED_ICLASS_VSQRTPS] = (inst_t) {OP_SQRT, DT_FLOAT,  PK_PACKED, CN_NONE};
		inst_map[XED_ICLASS_VSQRTSD] = (inst_t) {OP_SQRT, DT_DOUBLE, PK_SCALAR, CN_NONE};
		inst_map[XED_ICLASS_VSQRTSS] = (inst_t) {OP_SQRT, DT_FLOAT,  PK_SCALAR, CN_NONE};

		// Arithemetic shift right
		inst_map[XED_ICLASS_SAR] = (inst_t) {OP_SAR, DT_INT, PK_SCALAR, CN_NONE};

		// Arithemetic shift left
		inst_map[XED_ICLASS_SALC] = (inst_t) {OP_SALC, DT_INT, PK_SCALAR, CN_NONE};

		// Logical shift right
		inst_map[XED_ICLASS_SHR] = (inst_t) {OP_SHR, DT_INT, PK_SCALAR, CN_NONE};

		// Logical shift left
		inst_map[XED_ICLASS_SHL] = (inst_t) {OP_SHL, DT_INT, PK_SCALAR, CN_NONE};

		// Compare
		inst_map[XED_ICLASS_CMP]    = (inst_t) {OP_CMP, DT_INT,    PK_SCALAR, CN_NONE};
		inst_map[XED_ICLASS_CMPPD]  = (inst_t) {OP_CMP, DT_DOUBLE, PK_PACKED, CN_NONE};
		inst_map[XED_ICLASS_CMPPS]  = (inst_t) {OP_CMP, DT_FLOAT,  PK_PACKED, CN_NONE};
		inst_map[XED_ICLASS_CMPSD]  = (inst_t) {OP_CMP, DT_DOUBLE, PK_SCALAR, CN_NONE};
		inst_map[XED_ICLASS_CMPSS]  = (inst_t) {OP_CMP, DT_FLOAT,  PK_SCALAR, CN_NONE};
		inst_map[XED_ICLASS_VCMPPD] = (inst_t) {OP_CMP, DT_DOUBLE, PK_PACKED, CN_NONE};
		inst_map[XED_ICLASS_VCMPPS] = (inst_t) {OP_CMP, DT_FLOAT,  PK_PACKED, CN_NONE};
		inst_map[XED_ICLASS_VCMPSD] = (inst_t) {OP_CMP, DT_DOUBLE, PK_SCALAR, CN_NONE};
		inst_map[XED_ICLASS_VCMPSS] = (inst_t) {OP_CMP, DT_FLOAT,  PK_SCALAR, CN_NONE};

		// Minimum
		inst_map[XED_ICLASS_MINPD]  = (inst_t) {OP_MIN, DT_DOUBLE, PK_PACKED, CN_NONE};
		inst_map[XED_ICLASS_MINPS]  = (inst_t) {OP_MIN, DT_FLOAT,  PK_PACKED, CN_NONE};
		inst_map[XED_ICLASS_MINSD]  = (inst_t) {OP_MIN, DT_DOUBLE, PK_SCALAR, CN_NONE};
		inst_map[XED_ICLASS_MINSS]  = (inst_t) {OP_MIN, DT_FLOAT,  PK_SCALAR, CN_NONE};
		inst_map[XED_ICLASS_VMINPD] = (inst_t) {OP_MIN, DT_DOUBLE, PK_PACKED, CN_NONE};
		inst_map[XED_ICLASS_VMINPS] = (inst_t) {OP_MIN, DT_FLOAT,  PK_PACKED, CN_NONE};
		inst_map[XED_ICLASS_VMINSD] = (inst_t) {OP_MIN, DT_DOUBLE, PK_SCALAR, CN_NONE};
		inst_map[XED_ICLASS_VMINSS] = (inst_t) {OP_MIN, DT_FLOAT,  PK_SCALAR, CN_NONE};

		// Maximum
		inst_map[XED_ICLASS_MAXPD]  = (inst_t) {OP_MAX, DT_DOUBLE, PK_PACKED, CN_NONE};
		inst_map[XED_ICLASS_MAXPS]  = (inst_t) {OP_MAX, DT_FLOAT,  PK_PACKED, CN_NONE};
		inst_map[XED_ICLASS_MAXSD]  = (inst_t) {OP_MAX, DT_DOUBLE, PK_SCALAR, CN_NONE};
		inst_map[XED_ICLASS_MAXSS]  = (inst_t) {OP_MAX, DT_FLOAT,  PK_SCALAR, CN_NONE};
		inst_map[XED_ICLASS_VMAXPD] = (inst_t) {OP_MAX, DT_DOUBLE, PK_PACKED, CN_NONE};
		inst_map[XED_ICLASS_VMAXPS] = (inst_t) {OP_MAX, DT_FLOAT,  PK_PACKED, CN_NONE};
		inst_map[XED_ICLASS_VMAXSD] = (inst_t) {OP_MAX, DT_DOUBLE, PK_SCALAR, CN_NONE};
		inst_map[XED_ICLASS_VMAXSS] = (inst_t) {OP_MAX, DT_FLOAT,  PK_SCALAR, CN_NONE};

		// Logical AND
		inst_map[XED_ICLASS_AND]     = (inst_t) {OP_AND, DT_INT,    PK_SCALAR, CN_NONE};
		inst_map[XED_ICLASS_ANDPD]   = (inst_t) {OP_AND, DT_DOUBLE, PK_PACKED, CN_NONE};
		inst_map[XED_ICLASS_ANDPS]   = (inst_t) {OP_AND, DT_FLOAT,  PK_PACKED, CN_NONE};
		inst_map[XED_ICLASS_VANDPD]  = (inst_t) {OP_AND, DT_DOUBLE, PK_PACKED, CN_NONE};
		inst_map[XED_ICLASS_VANDPS]  = (inst_t) {OP_AND, DT_FLOAT,  PK_PACKED, CN_NONE};
		inst_map[XED_ICLASS_ANDN]    = (inst_t) {OP_AND, DT_INT,    PK_SCALAR, CN_NONE};
		inst_map[XED_ICLASS_ANDNPD]  = (inst_t) {OP_AND, DT_DOUBLE, PK_PACKED, CN_NONE};
		inst_map[XED_ICLASS_ANDNPS]  = (inst_t) {OP_AND, DT_FLOAT,  PK_PACKED, CN_NONE};
		inst_map[XED_ICLASS_VANDNPD] = (inst_t) {OP_AND, DT_DOUBLE, PK_PACKED, CN_NONE};
		inst_map[XED_ICLASS_VANDNPS] = (inst_t) {OP_AND, DT_FLOAT,  PK_PACKED, CN_NONE};

		// Logical OR
		inst_map[XED_ICLASS_OR]    = (inst_t) {OP_OR, DT_INT,    PK_SCALAR, CN_NONE};
		inst_map[XED_ICLASS_ORPD]  = (inst_t) {OP_OR, DT_DOUBLE, PK_PACKED, CN_NONE};
		inst_map[XED_ICLASS_ORPS]  = (inst_t) {OP_OR, DT_FLOAT,  PK_PACKED, CN_NONE};
		inst_map[XED_ICLASS_VORPD] = (inst_t) {OP_OR, DT_DOUBLE, PK_PACKED, CN_NONE};
		inst_map[XED_ICLASS_VORPS] = (inst_t) {OP_OR, DT_FLOAT,  PK_PACKED, CN_NONE};

		// Logical XOR
		inst_map[XED_ICLASS_XOR]    = (inst_t) {OP_XOR, DT_INT,    PK_SCALAR, CN_NONE};
		inst_map[XED_ICLASS_XORPD]  = (inst_t) {OP_XOR, DT_DOUBLE, PK_PACKED, CN_NONE};
		inst_map[XED_ICLASS_XORPS]  = (inst_t) {OP_XOR, DT_FLOAT,  PK_PACKED, CN_NONE};
		inst_map[XED_ICLASS_VXORPD] = (inst_t) {OP_XOR, DT_DOUBLE, PK_PACKED, CN_NONE};
		inst_map[XED_ICLASS_VXORPS] = (inst_t) {OP_XOR, DT_FLOAT,  PK_PACKED, CN_NONE};

		// Logical NOT
		inst_map[XED_ICLASS_NOT] = (inst_t) {OP_NOT, DT_INT, PK_SCALAR, CN_NONE};

		// Move
		// TODO: inst_map[XED_ICLASS_CMOVB]     = (inst_t) {OP_MOV,  OP_INT, PK_SCALAR, CN_BELOW};
		// TODO: inst_map[XED_ICLASS_CMOVBE]    = (inst_t) {OP_MOV,  OP_INT, PK_SCALAR, CN_BELOW_OR_EQ};
		// TODO: inst_map[XED_ICLASS_CMOVNB]    = (inst_t) {OP_MOV,  OP_INT, PK_SCALAR, CN_BELOW};
		// TODO: inst_map[XED_ICLASS_CMOVNBE]   = (inst_t) {OP_MOV,  OP_INT, PK_SCALAR, CN_BELOW_OR_EQ};
		// TODO: inst_map[XED_ICLASS_CMOVNL]    = (inst_t) {OP_MOV,  OP_INT, PK_SCALAR, CN_LESS};
		// TODO: inst_map[XED_ICLASS_CMOVNLE]   = (inst_t) {OP_MOV,  OP_INT, PK_SCALAR, CN_LESS_OR_EQ};
		inst_map[XED_ICLASS_CMOVL]  = (inst_t) {OP_MOV, DT_INT, PK_SCALAR, CN_LESS};
		inst_map[XED_ICLASS_CMOVLE] = (inst_t) {OP_MOV, DT_INT, PK_SCALAR, CN_LESS_OR_EQ};
		inst_map[XED_ICLASS_CMOVO]  = (inst_t) {OP_MOV, DT_INT, PK_SCALAR, CN_OVERFLOW};
		inst_map[XED_ICLASS_CMOVNO] = (inst_t) {OP_MOV, DT_INT, PK_SCALAR, CN_N_OVERFLOW};
		inst_map[XED_ICLASS_CMOVS]  = (inst_t) {OP_MOV, DT_INT, PK_SCALAR, CN_SIGN};
		inst_map[XED_ICLASS_CMOVNS] = (inst_t) {OP_MOV, DT_INT, PK_SCALAR, CN_N_SIGN};
		inst_map[XED_ICLASS_CMOVP]  = (inst_t) {OP_MOV, DT_INT, PK_SCALAR, CN_PARITY};
		inst_map[XED_ICLASS_CMOVNP] = (inst_t) {OP_MOV, DT_INT, PK_SCALAR, CN_N_PARITY};
		inst_map[XED_ICLASS_CMOVZ]  = (inst_t) {OP_MOV, DT_INT, PK_SCALAR, CN_EQUAL};
		inst_map[XED_ICLASS_CMOVNZ] = (inst_t) {OP_MOV, DT_INT, PK_SCALAR, CN_N_EQUAL};

		// Load effective address
		inst_map[XED_ICLASS_LEA] = (inst_t) {OP_LEA, DT_UINT, PK_SCALAR, CN_NONE};

		// Stack pop
		inst_map[XED_ICLASS_POP] = (inst_t) {OP_POP, DT_UINT, PK_SCALAR, CN_NONE};

		// Stack push
		inst_map[XED_ICLASS_PUSH] = (inst_t) {OP_PUSH, DT_UINT, PK_SCALAR, CN_NONE};

		// Load
		inst_map[XED_ICLASS_LDDQU]  = (inst_t) {OP_LD, DT_UINT, PK_SCALAR, CN_NONE};
		inst_map[XED_ICLASS_VLDDQU] = (inst_t) {OP_LD, DT_UINT, PK_SCALAR, CN_NONE};
		inst_map[XED_ICLASS_LDS]    = (inst_t) {OP_LD, DT_UINT, PK_SCALAR, CN_NONE};
		inst_map[XED_ICLASS_LES]    = (inst_t) {OP_LD, DT_UINT, PK_SCALAR, CN_NONE};
		inst_map[XED_ICLASS_LFS]    = (inst_t) {OP_LD, DT_UINT, PK_SCALAR, CN_NONE};
		inst_map[XED_ICLASS_LGS]    = (inst_t) {OP_LD, DT_UINT, PK_SCALAR, CN_NONE};
		inst_map[XED_ICLASS_LSS]    = (inst_t) {OP_LD, DT_UINT, PK_SCALAR, CN_NONE};
		inst_map[XED_ICLASS_LODSB]  = (inst_t) {OP_LD, DT_UINT, PK_SCALAR, CN_NONE};
		inst_map[XED_ICLASS_LODSW]  = (inst_t) {OP_LD, DT_UINT, PK_SCALAR, CN_NONE};
		inst_map[XED_ICLASS_LODSD]  = (inst_t) {OP_LD, DT_UINT, PK_SCALAR, CN_NONE};

		// Store
		inst_map[XED_ICLASS_STOSB] = (inst_t) {OP_ST, DT_UINT, PK_SCALAR, CN_NONE};
		inst_map[XED_ICLASS_STOSW] = (inst_t) {OP_ST, DT_UINT, PK_SCALAR, CN_NONE};
		inst_map[XED_ICLASS_STOSD] = (inst_t) {OP_ST, DT_UINT, PK_SCALAR, CN_NONE};

		// Prefetch
		inst_map[XED_ICLASS_PREFETCHNTA]        = (inst_t) {OP_PREFETCH, DT_UINT, PK_SCALAR, CN_NONE};
		inst_map[XED_ICLASS_PREFETCHT0]         = (inst_t) {OP_PREFETCH, DT_UINT, PK_SCALAR, CN_NONE};
		inst_map[XED_ICLASS_PREFETCHT1]         = (inst_t) {OP_PREFETCH, DT_UINT, PK_SCALAR, CN_NONE};
		inst_map[XED_ICLASS_PREFETCHT2]         = (inst_t) {OP_PREFETCH, DT_UINT, PK_SCALAR, CN_NONE};
		inst_map[XED_ICLASS_PREFETCHW]          = (inst_t) {OP_PREFETCH, DT_UINT, PK_SCALAR, CN_NONE};
		inst_map[XED_ICLASS_PREFETCH_EXCLUSIVE] = (inst_t) {OP_PREFETCH, DT_UINT, PK_SCALAR, CN_NONE};
		inst_map[XED_ICLASS_PREFETCH_RESERVED]  = (inst_t) {OP_PREFETCH, DT_UINT, PK_SCALAR, CN_NONE};

		// Call
		inst_map[XED_ICLASS_CALL_NEAR] = (inst_t) {OP_CALL, DT_UINT, PK_SCALAR, CN_NONE};
		inst_map[XED_ICLASS_CALL_FAR]  = (inst_t) {OP_CALL, DT_UINT, PK_SCALAR, CN_NONE};

		// Return
		inst_map[XED_ICLASS_RET_NEAR] = (inst_t) {OP_RET, DT_UINT, PK_SCALAR, CN_NONE};
		inst_map[XED_ICLASS_RET_FAR]  = (inst_t) {OP_RET, DT_UINT, PK_SCALAR, CN_NONE};

		// Jump
		inst_map[XED_ICLASS_JMP]     = (inst_t) {OP_JMP, DT_UINT, PK_SCALAR, CN_NONE};
		inst_map[XED_ICLASS_JMP_FAR] = (inst_t) {OP_JMP, DT_UINT, PK_SCALAR, CN_NONE};

		// Branch (assuming conditional)
		inst_map[XED_ICLASS_JL]  = (inst_t) {OP_BR, DT_UINT, PK_SCALAR, CN_LESS};
		inst_map[XED_ICLASS_JLE] = (inst_t) {OP_BR, DT_UINT, PK_SCALAR, CN_LESS_OR_EQ};
		inst_map[XED_ICLASS_JO]  = (inst_t) {OP_BR, DT_INT,  PK_SCALAR, CN_OVERFLOW};
		inst_map[XED_ICLASS_JNO] = (inst_t) {OP_BR, DT_INT,  PK_SCALAR, CN_N_OVERFLOW};
		inst_map[XED_ICLASS_JS]  = (inst_t) {OP_BR, DT_INT,  PK_SCALAR, CN_SIGN};
		inst_map[XED_ICLASS_JNS] = (inst_t) {OP_BR, DT_INT,  PK_SCALAR, CN_N_SIGN};
		inst_map[XED_ICLASS_JP]  = (inst_t) {OP_BR, DT_INT,  PK_SCALAR, CN_PARITY};
		inst_map[XED_ICLASS_JNP] = (inst_t) {OP_BR, DT_INT,  PK_SCALAR, CN_N_PARITY};
		inst_map[XED_ICLASS_JZ]  = (inst_t) {OP_BR, DT_INT,  PK_SCALAR, CN_EQUAL};
		inst_map[XED_ICLASS_JNZ] = (inst_t) {OP_BR, DT_INT,  PK_SCALAR, CN_N_EQUAL};

		// Gather
		inst_map[XED_ICLASS_VGATHERDPD] = (inst_t) {OP_GATHER, DT_DOUBLE, PK_PACKED, CN_NONE};
		inst_map[XED_ICLASS_VGATHERDPS] = (inst_t) {OP_GATHER, DT_FLOAT,  PK_PACKED, CN_NONE};
		inst_map[XED_ICLASS_VGATHERQPD] = (inst_t) {OP_GATHER, DT_DOUBLE, PK_PACKED, CN_NONE};
		inst_map[XED_ICLASS_VGATHERQPS] = (inst_t) {OP_GATHER, DT_FLOAT,  PK_PACKED, CN_NONE};
		inst_map[XED_ICLASS_VPGATHERDD] = (inst_t) {OP_GATHER, DT_DOUBLE, PK_PACKED, CN_NONE};
		inst_map[XED_ICLASS_VPGATHERDQ] = (inst_t) {OP_GATHER, DT_FLOAT,  PK_PACKED, CN_NONE};
		inst_map[XED_ICLASS_VPGATHERQD] = (inst_t) {OP_GATHER, DT_DOUBLE, PK_PACKED, CN_NONE};
		inst_map[XED_ICLASS_VPGATHERQQ] = (inst_t) {OP_GATHER, DT_FLOAT,  PK_PACKED, CN_NONE};

		// TODO: Scatter

		// Blend
		inst_map[XED_ICLASS_BLENDPD]   = (inst_t) {OP_BLEND, DT_DOUBLE, PK_PACKED, CN_NONE};
		inst_map[XED_ICLASS_BLENDPS]   = (inst_t) {OP_BLEND, DT_FLOAT,  PK_PACKED, CN_NONE};
		inst_map[XED_ICLASS_BLENDVPD]  = (inst_t) {OP_BLEND, DT_DOUBLE, PK_PACKED, CN_NONE};
		inst_map[XED_ICLASS_BLENDVPS]  = (inst_t) {OP_BLEND, DT_FLOAT,  PK_PACKED, CN_NONE};
		inst_map[XED_ICLASS_VBLENDPD]  = (inst_t) {OP_BLEND, DT_DOUBLE, PK_PACKED, CN_NONE};
		inst_map[XED_ICLASS_VBLENDPS]  = (inst_t) {OP_BLEND, DT_FLOAT,  PK_PACKED, CN_NONE};
		inst_map[XED_ICLASS_VBLENDVPD] = (inst_t) {OP_BLEND, DT_DOUBLE, PK_PACKED, CN_NONE};
		inst_map[XED_ICLASS_VBLENDVPS] = (inst_t) {OP_BLEND, DT_FLOAT,  PK_PACKED, CN_NONE};
		
		// Permute
		inst_map[XED_ICLASS_VPERM2F128] = (inst_t) {OP_PERMUTE, DT_DOUBLE, PK_PACKED, CN_NONE};
		inst_map[XED_ICLASS_VPERM2I128] = (inst_t) {OP_PERMUTE, DT_DOUBLE, PK_PACKED, CN_NONE};
		inst_map[XED_ICLASS_VPERMD]     = (inst_t) {OP_PERMUTE, DT_DOUBLE, PK_PACKED, CN_NONE};
		inst_map[XED_ICLASS_VPERMIL2PD] = (inst_t) {OP_PERMUTE, DT_DOUBLE, PK_PACKED, CN_NONE};
		inst_map[XED_ICLASS_VPERMIL2PS] = (inst_t) {OP_PERMUTE, DT_FLOAT,  PK_PACKED, CN_NONE};
		inst_map[XED_ICLASS_VPERMILPD]  = (inst_t) {OP_PERMUTE, DT_DOUBLE, PK_PACKED, CN_NONE};
		inst_map[XED_ICLASS_VPERMILPS]  = (inst_t) {OP_PERMUTE, DT_FLOAT,  PK_PACKED, CN_NONE};
		inst_map[XED_ICLASS_VPERMPD]    = (inst_t) {OP_PERMUTE, DT_DOUBLE, PK_PACKED, CN_NONE};
		inst_map[XED_ICLASS_VPERMPS]    = (inst_t) {OP_PERMUTE, DT_DOUBLE, PK_PACKED, CN_NONE};
		inst_map[XED_ICLASS_VPERMQ]     = (inst_t) {OP_PERMUTE, DT_FLOAT,  PK_PACKED, CN_NONE};

		// System call
		inst_map[XED_ICLASS_SYSCALL]     = (inst_t) {OP_SYSCALL, DT_INT, PK_SCALAR, CN_NONE};
		inst_map[XED_ICLASS_SYSCALL_AMD] = (inst_t) {OP_SYSCALL, DT_INT, PK_SCALAR, CN_NONE};

		return inst_map;
	}
	/// Instruction mapping (XED to error api)
	std::map<OPCODE, inst_t> inst_map = create_inst_map();

	/**
	 * Initialize an error api instruction from Pin
	 *
	 * \param inst Error API instruction
	 * \param ins  Pin instruction
	 */
	inline void SetInstructionFromPin(Instruction *inst, INS ins)
	{
		// Opcode
		UINT32 opcode = INS_Opcode(ins);
		inst->opcode = opcode;
		// Find instruction mapping
		if (inst_map.find(opcode) != inst_map.end()) {
			inst_t inst_comps = inst_map[opcode];
			inst->operation = inst_comps.operation;
			inst->data = inst_comps.data_type;
			inst->packing = inst_comps.packing;
			inst->condition = inst_comps.condition;
		}
	}

	/**
	 * Set instruction data (error api) from Pin
	 *
	 * \param inst_data InstructionData (error api)
	 * \param ins       Pin instruction
	 */
	inline void SetInstructionDataFromPin(InstructionData &inst_data, INS ins)
	{
		// Set values
		inst_data.asm_line = INS_Disassemble(ins);
		inst_data.address.virt = INS_Address(ins);
		inst_data.address.phys = hamartia_api::memory::VirtualToPhysical(INS_Address(ins));
		inst_data.width = INS_OperandWidth(ins, 0);
		inst_data.simd_width = pin_operand_helper::SimdWidth(ins, 0, inst_data.width);

		// Set instruction
		SetInstructionFromPin(&inst_data.instruction, ins);
	}

	inline std::string DecodeRoutineName(std::string rtn_name)
	{
		if (rtn_name.find("_ZN") == 0) {
			// C++
			rtn_name = rtn_name.substr(3);
		} else if (rtn_name.find("_Z") == 0) {
			// C++
			rtn_name = rtn_name.substr(2);
		} else {
			// C
			return rtn_name;
		}
		// Parse C++ (name mangling)
		int length = 0;
		std::stringstream ss;
		std::string name;
		for (std::string::iterator c = rtn_name.begin(); c != rtn_name.end(); ++c) {
			if (length == 0) {
				if (*c >= '0' && *c <= '9') {
					ss << *c;
				} else {
					ss >> length;
					ss.clear();
					if (length == 0) break;
					if (!name.empty()) {
						name += "::";
					}
				}
			} 
			if (length != 0) {
				--length;
				name += *c;
			}
		}

		return name;
	}
}

#endif
/** @} */

