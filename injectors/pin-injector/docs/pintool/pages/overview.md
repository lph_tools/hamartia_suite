The Hamartia instruction-level Injector is a Pin-based injector which implements 
the Hamartia Error and Detection API. This allows any model to be run as long as 
it is implemented with the API. The features which are currently supported are:

  * Hamartia Error/Detector model execution (C++ or Python)
    * Simple error models (e.g., randomly flip bits)
    * Interface to RTL gate-level injection
  * Injection point logging
    * Values before and after injection
    * Context: functions and line number
    * Registers
    * Memory addresses
  * Flexible injection
    * Instruction filters (e.g., by type, width, etc.)
    * Inject intput/output operands of instruction
    * Inject in region(s) annotated in source code (e.g., specific functions, lines)
    * Inject a particular MPI rank
    * Inject into [Containment Domain][CD]

-----------------
## Error Injection Flow
The injection process consists of two phases: **profiling** and **injection**. 
The profiling phase obtains the bound of possible injection sites and other information 
required for customized injection experiments. 
The injection phase injects error(s) into the program based on command-line inputs 
specified by user. 
Reported are the injection site context, target instruction operands before and after 
injection, and other statistics useful for analysis.
We also provide a script that automates the flow and integrates these phases seamlessly.

![flow](img/flow.png)

[CD]: https://lph.ece.utexas.edu/public/CDs/ContainmentDomains
