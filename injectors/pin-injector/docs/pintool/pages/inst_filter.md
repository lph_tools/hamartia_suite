The Error injector pintool allows for different combinations of instructions to be selected/filtered 
using different categories/selectors. Currently, these selectors include:

* Opcode
* XED ICLASS (instruction type)
* Predefined category
    * `OPS_INT`: integer arithmetic operations
    * `OPS_INT_LOGIC`: integer logic operations
    * `OPS_FP`: floating-point arithmetic operations
    * `OPS_FP_LOGIC`: floating-point logic operations

These selectors can be combined at runtime for unique filtering combinations using `-c` flag. 
The default filter is "`-c OPS_INT,OPS_FP`" meaning that only arithmetic operations are counted.
Note that we chain the categories using a comma as delimiter.
However, additional categories or selectors can be added somewhat easily.

### Add a New Predefined Category

Users can define their own category in `$PIN_INJECTOR_DIR/src/oclasses.h`.
Suppose we only want to target at `add` and `sub` operations. Here is an example:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.cpp}
static const OPCODE ops_add_sub[] = {XED_ICLASS_ADD,
                             XED_ICLASS_SUB,
                             XED_ICLASS_ADDPD,
                             XED_ICLASS_ADDPS,
                             XED_ICLASS_ADDSD,
                             XED_ICLASS_ADDSS,
                             XED_ICLASS_SUBPD,
                             XED_ICLASS_SUBPS,
                             XED_ICLASS_SUBSD,
                             XED_ICLASS_SUBSS,
                             };
INITVEC(ops_add_sub, ops_add_sub_vec);   // creates ops_add_sub vector

std::map< std::string, std::vector<OPCODE> > create_operation_map()
{
    std::map< string, std::vector<OPCODE> > m;
    m[std::string("OPS_INT")] = ops_int_vec;
    m[std::string("OPS_INT_LOGIC")] = ops_int_logic_vec;
    m[std::string("OPS_FP")] = ops_fp_vec;
    m[std::string("OPS_FP_LOGIC")] = ops_fp_logic_vec;
    m[std::string("OPS_ADD_SUB")] = ops_add_sub_vec;   //add this line!
    return m;
}

// some other code here ...
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Now let's discuss what we just implemented. First, we create an array of OPCODE. This array includes
all the targeted operations (both scalar and SIMD operations). Next, we initialize a vector (through 
a macro) using the array we just created. Finally, we register this new category by adding it to 
a map. As you can see, it is easy to create your own category! We refer user to Intel's XED
[documentation](XED) for available operations you can specify. Also, make sure operations you include are 
supported by the Pin version you use.

By specifying the this particular category, only instructions in this category are counted. For example,

	$PIN_INJECTOR_DIR/bin/injector.sh -c OPS_ADD_SUB -i 3000 -- <application>

tells the injector to inject the 3000-th `add` or `sub` instruction in the application (including 
the library code called by the application). See **Injection Control** page for how 
to exclude library code.


### Add a New Selector 
TODO: how to add a new type of selector

[XED]: https://software.intel.com/sites/landingpage/xed/ref-manual/html/index.html"
