### Build

  * Build: `scons`
  * Clean: `scons -c`

----------------
### Running
Here we explain how to run the injector manually for each phase.
Running the error injector using the script we provide:

    $PIN_INJECTOR_DIR/bin/injector.sh <args> -- <application>

In general, running the error injector is broken into two steps: profiling and injection.

#### Profiling
First, a program is profiled to get information on the dynamic instruction stream.

    $PIN_INJECTOR_DIR/bin/injector.sh -c <op_class> -- <application>

This generates a logfile (`err_prof_inj.out`) which provides information useful for injection. 
Mainly you should look at the global/local (image) filtered Op Count to select (possibly randomly) 
a dynamic instruction for injection. `op_class` is the class filter (see below).

#### Injection
To run an injection:

    $PIN_INJECTOR_DIR/bin/injector.sh -c <op_class> -i <instruction_count> -e <error_model> -- <application>

This will possibly inject an error into the application specified. If there is no logfile 
at the end then the target instruction was not found/reached. Otherwise, if successful, 
there will be a logfile which describes information about the error.

#### Batch running
We provide a launcher script for launching error injectors in a templated manner. 
The launcher is located at `$HAMARTIA_DIR/src/python/launcher`. It allows user to launch 
experiments in an automatic and parallel manner. Injection result analysis can also be 
pipelined if needed.

    $LAUNCHER_DIR/launcher.py -c <config_file> -d <output_dir> -t <timeout_in_sec>

See examples of `config_file` within `$LAUNCHER_DIR/configs` directory.
 
----------------
### Command-Line Options

  * Injection Site Filter
    + Class filter (**-c**): filters the profiled/injected operation by:
        + Operation class (e.g. OPS_INT, OPS_FP), XED ICLASS, or OPCODE
    + Dynamic instruction (**-i**): dynamic instruction index to inject at (0 performs profiling)
    + Ignore images (**-n**): image names to ignore (while profiling and injecting)
    + Include images (**-l**): images to include in injection by index (0 = all, 1 = target program)
    + Regions (**-r**): only inject into designated code regions
    + Width (**-w**): width of operation to inject at
    + Injection point (**-p**): operand to inject (post = output operand (default); pre = input operand; any = both)
  * Error model (**-e**): specifies the error model to use for injection
  * Error config (**-q**): specifies the error configuration file
  * Detector model (**-d**): specifies the detector model to use for injection
  * Detector config (**-a**): specifies the detector configuration file
  * Output file (**-o**): name of the output file (default = err_prof_inj.out)
  * MPI Injection
    + MPI injection switch (**-m**): set to 1 to enable MPI injection
    + Target rank (**-t**): MPI process rank id to inject
  * CD Injection
    + CD injection switch (**-cd**): set to 1 to enable CD injection
    + CD injection mode (**-cdmode**): 0 = use dynamic instruction count, 1 = use CD name
    + CD to be injected (**-cdname**): valid only if cdmode is 1
  * PIN inherent options
    + Unique logfile (**-unique_logfile**): insert the PID into the log filename
    + Pin logfile (**-logfile**): filename of pin logfile
    + Help (**-help**): print out help
