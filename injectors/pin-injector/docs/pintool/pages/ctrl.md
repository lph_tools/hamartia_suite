This page demonstrates how one can control the injection sites. By default, 
the possible injection sites are arithmetic instructions executed by the application 
(including libraries called by the application). Since user may want to evaluate the 
application at different granularities (e.g. a particular piece of code, a function, 
an executable binary, etc.), we provide knobs to achieve this. See **Instruction Filters** 
page for how to filter instructions based on types.

In additional to injecting single-thread applications, we also demonstrate how the 
user can perform injection to MPI programs and Containment Domain programs.

### Injection Site Pinpointing
This section covers some knobs that can be used to pinpoint the injection sites 
of the application. As we covered in the **Usage** page, the profiling phase 
outputs instruction counts and categorizes the instructions executed by each image 
(executable binary). This information can be used to control the injection 
sites in the injeciton phase.

#### Including/Excluding Images
Suppose a user is only interested in excluding injecting errors to libraries 
called by the application. The include image (**-l**) knob can be used in this case:

    $PIN_INJECTOR_DIR/bin/injector.sh -i <instruction_count> -l 1 -- <application>

The -l 1 specifies the first image in the application, which is always the program itself. 
The user can also specify multiple images by connecting them with commas. 
The profiling result provides a mapping between image numbers to image names.

Note that the instruction count provided above should be no greater than the number 
of (filtered) instructions executed by the program (or a collection of images 
if multiple images are specified). Otherwise, the injector will not reach 
the target instruction.

Similarly, one can use ignore image (**-n**) to exclude injections in certain images.

#### Code Region Injection
> Note that this only works when the markers are not removed by the compiler. 

To pinpoint the target code regions, the user has to first decorate their source code 
with two special markers we provide: `BEGIN_ERROR_INJECTION()` and `END_ERROR_INJECTION()`. 
These two markers are defined in `$PIN_INJECTOR_DIR/src/code_regions.h`. 
An example is shown below to demonstrate how one can inject the inner loop within 
a two-level nested loop.

```
#include "code_regions.h"
void user_app(){
    //other code ...
    for(){
        BEGIN_ERROR_INJECTION();
        for(){
            //inner loop body
        }
        END_ERROR_INJECTION();
    }
    //other code ...
}
```

Note that one can wrap as many code regions as she wants. After decorating the application, 
user can then go through the same injection process except an additional knob is required. 
When launching the profiling phase, the user has to enable code region profiling:

    $PIN_INJECTOR_DIR/bin/injector.sh -r 1 -- <application>

The profiling result will then include only the instructions executed within the code regions. 

Similarly, at injection phase the same flag is also required.

    $PIN_INJECTOR_DIR/bin/injector.sh -i <instruction_count> -r 1 -- <application>

--------------
### Injecting MPI Programs
If the user just append the injector command after MPI command such as `mpirun`, 
the injector will attach and inject every MPI process, which is an extremely unlikely in reality. 
Therefore, we add additional knobs to control injecting MPI applications and to select 
the target MPI process:

  * MPI injection switch (**-m**): set to 1 to enable MPI injection
  * Target rank (**-t**): MPI process rank id to inject (default=0)

Example usage:

    mpirun -n <process_count> $PIN_INJECTOR_DIR/bin/injector.sh -i <instruction_count> -- <application>

> Injecting multi-thread programs is not yet supported. Therefore, injecting hybrid MPI-OpenMP program will result in undefined behavior and outcomes.


