#!/usr/bin/env python

"""

  A simple script to run a program with errors injected

  Michael Sullivan, University of Texas
  Last modified: 6/30/12

"""

import sys, os, itertools, subprocess, threading, string, random, time
import pprint
pp = pprint.PrettyPrinter(indent=4)

################################################################################
# Return Value Interface
################################################################################
NO_ERROR_OR_SDC = 0
TIMEOUT = 1
SEG_FAULT = 2
DRYRUN = 3

################################################################################
# Main Tool
################################################################################

PIN_STRING = "/opt/pin-2.11-49306-gcc.3.4.6-ia32_intel64-linux/pin -t /opt/pin-2.11-49306-gcc.3.4.6-ia32_intel64-linux/source/tools/error_injection/obj-intel64/$ptool -i $eloc -- "
EXEC_STRING = "/home/mbsullivan/bin/mg.S.x"

duration = None # will hold elapsed time
class Timer(object):
  """ From http://mrooney.blogspot.com/2009/07/simple-timing-of-python-code.html """
  def __enter__(self): self.start = time.time()
  def __exit__(self, *args): global duration; duration = time.time() - self.start

class TimeoutExpired(Exception):
  """Process Timed Out."""
  pass

class TimedCommand(object):
  '''
  From: https://gist.github.com/1306188

  Enables to run subprocess commands in a different thread
  with TIMEOUT option!

  Based on jcollado's solution:
  http://stackoverflow.com/questions/1191374/subprocess-with-timeout/4825933#4825933
  '''
  def __init__(self, cmd):
    self.cmd = cmd
    self.process = None

  def run(self, timeout=None, **kwargs):
    def target(**kwargs):
      self.process = subprocess.Popen(self.cmd, **kwargs)
      self.cobj = self.process.communicate()

    thread = threading.Thread(target=target, kwargs=kwargs)
    thread.start()

    thread.join(timeout)
    if thread.is_alive():
      self.process.kill()
      thread.join()
      raise TimeoutExpired

    return (self.cobj, self.process)

def find_replace(text, sub_dict):
  """ Search and replace all keys with their value. """
  for k,v in sub_dict.iteritems():
    # strings are immutable, does not change original
    text = text.replace(str(k), str(v))
  return text

def preappend_time_and_status_to_file(filename, status, errorloc, maxloc):
  """ Put a header on top of the output file. """
  with open(filename, "a") as f:
    f.seek(0) # go to beginning
    f.write("# ERRORLOC " + str(errorloc) + "\n")
    f.write("# MAXLOC " + str(maxloc) + "\n")
    f.write("# TIME (s) " + str(duration) + "\n")
    f.write("# STATUS " + str(status) + "\n")
  # append error_inject.out TODO: FIXME
  os.system("cat %s >> %s" % ("./error_inject.out", filename))
    

def main_exec(execstring, output_folder, numops, timeout, seed, options):
  """ The starting point for code. Inputs have been vetted. """
  # generate error location
  if seed is not None:
    random.seed(seed)
  else:
    random.seed(os.urandom(64))
  error_loc = random.randint(1, numops)
  #TODO: add override ability to error_loc
  #error_loc = 12898361481
  outfile = (options.outfilebase + ".dat") if options.nopin \
              else (options.outfilebase + str(error_loc) + ".dat")
  full_outfile = os.path.join(output_folder, outfile)
  # replace file, if it exists
  if os.path.isfile(full_outfile):
    os.remove(full_outfile)

  # run program
  if options.nopin:
    # run program without pin, and output to [outfilebase].dat
    full_execstring = string.join([execstring, full_outfile])
  else:
    # inject error through pin, and output to [outfilebase][error_loc].dat
    sub_dict = {"$eloc": error_loc, "$ptool": options.tool}
    # string.join([execstring, full_outfile])
    full_execstring = string.join([find_replace(PIN_STRING, sub_dict), \
                        "%s > %s" % (execstring, full_outfile)])
  if options.verbose:
    print full_execstring

  try:
    with Timer(): # time the execution
      tc = TimedCommand(full_execstring)
      (cobj,proc) = tc.run(timeout=timeout, shell=True, \
                           stdout=subprocess.PIPE, stderr=subprocess.PIPE)
  # check for timeout
  except TimeoutExpired:
      preappend_time_and_status_to_file(full_outfile, "TIMEOUT", error_loc, numops)
      return TIMEOUT

  # check for seg fault
  if (proc.returncode != 0):  # works for the selected app
    preappend_time_and_status_to_file(full_outfile, "SEG_FAULT", error_loc, numops)
    return SEG_FAULT

  # SDC or no error
  preappend_time_and_status_to_file(full_outfile, "NO_ERROR_OR_SDC", error_loc, numops)
  return NO_ERROR_OR_SDC


################################################################################
# Command-line interface
################################################################################

import optparse, inspect
from contextlib import contextmanager

@contextmanager
def handle_stackframe_without_leak(currentframe):
  """ For automatically deleting a stackframe, to prevent a long memory leak upon inpsection. 

      Called WITH frame already queried, to prevent calling stack from being broken. """
  try:
    # do something with the frame
    yield currentframe
  finally:
    del currentframe    

def calling_lineno():
  """Returns the line number of the CALLING routine."""
  
  # [3] to go back TWO caller frames (since this is a function itself)
  # [2] to extract the lineno
  # Note: [1:4] would extract (source file, lineno, function name)
  with handle_stackframe_without_leak(inspect.currentframe()) as currframe:
    return inspect.getouterframes(currframe)[3][2]

def test_file_existence(parser,
                    variable,
                    label=None,
                    isfolder=False,
                    createfolder=False,
                    none_msg="\"{0:s} the {1:s} must be defined!\".format(\
                               calling_lineno(), \
                               label if (label is not None) else \
                               \"filename\")",
                    missing_msg="\"{0:s} {1:s} does not exist!\".format(\
                                 \"folder\" if (isfolder is True) else \"file\",\
                                 abs_filepath)"):
  """ Test for a file/folder's existence, erroring out with an optparse parser otherwise. """   

  # if variable is None (in Python)
  if variable is None:
    parser.error(eval(none_msg))       

  # if variable is missing from filesystem 
  abs_filepath = os.path.abspath(variable)
  if isfolder and (not os.path.isdir(abs_filepath)):          # isfolder
    if not createfolder:  # error out
      parser.error(eval(missing_msg))
    else:                 # create the missing folder
      os.makedirs(abs_filepath)
  elif (not isfolder) and (not os.path.isfile(abs_filepath)): # isfile
    parser.error(eval(missing_msg))

  # if all is okay, return the full (absolute) filepath
  return abs_filepath

def test_existence(parser,
                   variable,
                   label=None,
                   none_msg="\"{0:s} the {1:s} must be defined!\".format(\
                               calling_lineno(), \
                               label if (label is not None) else \
                               \"variable\")"):
  """ Test for a variable's existence. """   

  # if variable is None (in Python)
  if variable is None:
    parser.error(eval(none_msg))       
  return variable

def test_integer_validity(parser,
                    variable,
                    label=None,
                    lowlimit=None,
                    highlimit=None,
                    isfloat=False,
                    none_msg="\"{0:d}: the {1:s} must be defined!\".format(\
                               calling_lineno(), \
                               label if (label is not None) else \
                               \"variable\")",
                    low_msg="\"{0:s} is below the allowable limit!\".format(\
                                 label if (label is not None) else \
                                 (str(calling_lineno()) + \"variable\"))",
                    high_msg="\"{0:s} is above the allowable limit!\".format(\
                                 label if (label is not None) else \
                                 (str(calling_lineno()) + \"variable\"))",
                    invalid_msg="\"{0:s} is invalid!\".format(\
                                 label if (label is not None) else \
                                 (str(calling_lineno()) + \"variable\"))"):
  """ Test for the validity of an integer/float, erroring out with an optparse parser otherwise. """   

  # if variable is None (in Python)
  if variable is None:
    parser.error(eval(none_msg)) 

  # get appropriate test function (builtin)
  if isfloat is True:
    testfunc = lambda x: float(x)
  else:
    testfunc = lambda x: int(x)   

  # if variable is invalid or outside of given bounds
  try:
    if (lowlimit is not None) and (testfunc(variable) < lowlimit):
      parser.error(eval(low_msg))
    elif (highlimit is not None) and (testfunc(variable) > highlimit):
      parser.error(eval(high_msg))
    else:
      testfunc(variable)
  except ValueError:
    parser.error(eval(invalid_msg))

  # if all is okay, return the converted str->numeric value
  return testfunc(variable)     

class optopt(optparse.OptionParser):
  """ Subclassed OptionParser, to prevent exiting from the interpreter when
  called interactively. """

  class optexit(Exception):
      def __init__(self, msg, status):
          self.msg = msg
          self.status = status

      def __str__(self):
          return self.msg

  def exit(self, status=0, msg=None):
      """Base class version with sys.exit() replaced by raise."""

      if msg:
          sys.stderr.write(msg)
      raise self.optexit(msg, status)     

@contextmanager
def defaultParser(name, description, version, usage):
  """ Default experimental parser. """

  try:
    # CLI description
    parser = optopt(prog=name, \
                    add_help_option=True, \
                    description=description, \
                    version=version, \
                    usage=usage, \
                    formatter=optparse.IndentedHelpFormatter(), \
                    option_class=optparse.Option)

    def conditionalJoin(a,b):
      """ Join two paths if they exist, else returns None. """
      return os.path.join(a,b) if ((a is not None) and (b is not None)) else None

    # command line options
    parser.add_option("-n", "--numops", action="store", dest="numops", \
                      help="Number of targetted operations in the host program.", \
                      default=20902655457)
    parser.add_option("-d", "--dryrun", action="store_true", dest="dryrun", \
                      help="Just check validity of CLI and existence of resources. Don't run.")
    parser.add_option("-t", "--timeout", action="store", dest="timeout", \
                      help="Timeout (for erroneous control), in seconds.", \
                      default=120)
    parser.add_option("-v", "--verbose", action="store_true", dest="verbose", \
                      help="Display everything in gruesome detail.", \
                      default=False)
    parser.add_option("-e", "--seed", action="store", dest="seed", \
                      help="Seed the RNG.",
                      default=None)
    parser.add_option("-f", "--folder", action="store", type="string", dest="output_fldr", \
                      help="The folder for output data.",
                      default="./errordat/")
    parser.add_option("-o", "--outfilebase", action="store", dest="outfilebase", \
                      help="Output filename base (error location and .dat will be appended).", \
                      default="error_output")
    parser.add_option("-p", "--nopin", action="store_true", dest="nopin", \
                      help="Don't instrument with Pin.", \
                      default=False)
    parser.add_option("-l", "--tool", action="store", dest="tool", \
                      help="Pintool name.", \
                      default="err_inject.so")
    # pass back this parser
    yield parser

  except optopt.optexit as e:
      sys.exit(e.status)

  finally:
    pass  # nothing for now
def extract_information(argv, parser):
  """ Extracts the (options, args), and error checks. """

  # get out info
  parser.parse_args(argv)
  (options, args) = parser.parse_args(argv)

  # test for number validity
  numops = test_integer_validity(parser, options.numops, "Num Ops", 0, 1000000000000, isfloat=False)
  timeout = test_integer_validity(parser, options.timeout, "Timeout", 0, 3600, isfloat=False)
  seed = test_integer_validity(parser, options.seed, "RNG Seed", None, None, isfloat=False) if options.seed is not None else None # seed has no limits

  # test for output folder existence
  output_folder = test_file_existence(parser, options.output_fldr, label="Output Folder", isfolder=True, createfolder=True)

  # remove the mandatory argument from the list
  execstring = ""
  if len(args) <= 1:
    execstring = EXEC_STRING # default
  elif len(args) > 1:
    parser.error("too many arguments.")
  else:
    execstring = args.pop(0)

  # return vetted arguments
  return (execstring, output_folder, numops, timeout, seed, options)

def main(argv=None):
  """ Parse and check options, and then call main_exec(). """

  if argv is None:
    argv = sys.argv[1:]
  
  with defaultParser(name="inject_error.py",
                              description="A script to run a program-under-test with an injected error.",
                              version="v0.01",
                              usage="See ./inject_error.py --help") as parser:

    # add any experiment-specific command-line options, if applicable

    (execstring, output_folder, numops, timeout, seed, options) = \
     extract_information(argv, parser)

  # if all CLI options pass and NOT dry run, process & return
  if options.dryrun:
    return DRYRUN                               # success (dryrun)
  else:                                         # evaluate (run)
    return main_exec(execstring, output_folder, numops, timeout, seed, options)   

if __name__ == "__main__":
  sys.exit(main())
