#!/usr/bin/env python

"""

  An example script for error analysis (based on NPB's Multi-Grid with input A)

  Michael Sullivan, University of Texas
  Last modified: 6/30/12

"""

import glob, re

# MG/A
THEORETICAL_CORRECT = 0.5307707005734E-04 # A: float(0.1800564401355E-05)
MACHINE_CORRECT = 0.5307707005735E-04 # A: 0.2433365309069E-05
EPSILON = float(1E-8)

# EXTRACTION HELPERS

class Unit():

    def extract(self, s):
        regex = re.compile(s)
        def tryfloat(x):
            ' Convert any numbers to floats. '
            try:
                return float(x)
            except ValueError:
                try:
                    x2 = x.replace("+", "E+")
                    return float(x2)
                except:
                    return x
            except:
                return x
        return tuple(tryfloat(x) for x in regex.search(self.ot).groups())

    # ACCESSORS
    def __repr__(self):
        return pprint.pformat(self.__dict__)

# TODO: add in more error reporting?
# TODO: make tool-agnostic?
class UnitError(Unit):
    """ Helper class for holding and converting area information.  """
    def __init__(self, out_text, **kwargs):
        " Filter out area from synthesis output. "
        # fill Unit
        self.__dict__.update(kwargs)
        self.ot = out_text

        # error loc
        (self.errorloc,) = self.extract("ERRORLOC (\S+)")

        # error type
        (self.type,) = self.extract("# STATUS (\S+)")

        if self.type == "NO_ERROR_OR_SDC":
            # verification (bool)
            (self.verification,) = self.extract("VERIFICATION (\S+)")
            self.verification = self.verification == "SUCCESSFUL"
            (self.l2n) = self.extract("L2 Norm is\s+(\S+)")
            self.l2err = abs(float(self.l2n[0]) - THEORETICAL_CORRECT) / THEORETICAL_CORRECT
            if self.l2err < EPSILON:
                self.dver = True
            else:
                self.dver = False
            assert (self.verification == self.dver)

            # fully masked
            self.masked = self.l2n == MACHINE_CORRECT

            # error magnitude and rel magnitude
            (self.errmag,) = self.extract("ERRVAL (\S+)")
            (self.oldval,) = self.extract("OLDVAL (\S+)")
            (self.newval,) = self.extract("NEWVAL (\S+)")
            if self.oldval != 0:
                self.relerr = (self.newval-self.oldval)/self.oldval



# DO STUFF
units = []
for fn in glob.glob("./*.dat"):
    with open(fn) as f:
        fc = f.read()
        if len(fc) > 0:
            x = UnitError(fc, fn=fn)
            units.append(x)
            #print (x.fn, x.__dict__.get("relerr",0), x.l2err) 

def ad(x,thresh=0.01):
    return x.__dict__.get("verification",False) or abs(x.__dict__.get("relerr",100)) > thresh

for z in xrange(1, 11, 1):
    print "%s, %s" % (float(z)/100, 100*float(len([x for x in units if ad(x,thresh=float(z)/10000)]))/len(units))

for z in xrange(11, 100, 5):
    print "%s, %s" % (float(z)/100, 100*float(len([x for x in units if ad(x,thresh=float(z)/10000)]))/len(units))

for z in xrange(101, 50000, 10):
    print "%s, %s" % (float(z)/100, 100*float(len([x for x in units if ad(x,thresh=float(z)/10000)]))/len(units))
