#! /bin/bash

# Directory of script
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
# Tool path
INJ_TOOL=$DIR/../build/liberror_prof_inj.so

# Environment
export LD_LIBRARY_PATH=$HAMARTIA_DIR/build:$HAMARTIA_DIR/src/python/hamartia_api:$LD_LIBRARY_PATH
export DYLD_LIBRARY_PATH=$HAMARTIA_DIR/build:$HAMARTIA_DIR/src/python/hamartia_api:$DYLD_LIBRARY_PATH
export PYTHONPATH=$HAMARTIA_DIR/src/python:$PYTHONPATH

# Launch injector
# Running Pin 2.14 on newer Linux kernel needs a special flag
kernel_version=`uname -r | head -c 1`
if (($kernel_version > 3 )); then
    $PIN_HOME/intel64/bin/pinbin -ifeellucky -t $INJ_TOOL "$@"
else
    $PIN_HOME/intel64/bin/pinbin -t $INJ_TOOL "$@"
fi
