/*
 *  Packed SIMD 64-bit Floating-Point Back-to-Back Test
 */

#include <cstdlib>
#include <cstdio>
#include <cstdint>
#include <iostream>
#include "immintrin.h"
#include "emmintrin.h"
#include "../../test_utils.h"
#include "../../../src/code_regions.h"

int main(int argc, char *argv[])
{
	// Argument check
    if (argc <= 4 || argc > 5) {
        test_utils::usage("_mm_add_pd", 4, 1);
		return EXIT_FAILURE;
    }

	// Operand setup
	__m128d result1, result2;

	__m128d i11 = _mm_set_pd(test_utils::get_arg<double>(0, argc, argv),  // [63:0]
                             test_utils::get_arg<double>(1, argc, argv)); // [127:64]
	__m128d i12 = _mm_set_pd(1.0, 1.0);

	__m128d i21 = _mm_set_pd(test_utils::get_arg<double>(2, argc, argv),
                             test_utils::get_arg<double>(3, argc, argv));
	__m128d i22 = _mm_set_pd(1.0, 1.0);

	// Run operation
	BEGIN_ERROR_INJECTION();
	result1 = _mm_add_pd(i11, i12);
	result2 = _mm_add_pd(i21, i22);
	END_ERROR_INJECTION();

	// Print result
	uint64_t flags = 0; //test_utils::get_flags();
	test_utils::print_results<__m128d>(result1, flags, 2);
	test_utils::print_results<__m128d>(result2, flags, 2);

    return EXIT_SUCCESS;
}

