/*
 *  Testing multiple floating-point injection points
 */

#include <cstdlib>
#include <cstdio>
#include <cstdint>
#include <iostream>
#include "immintrin.h"
#include "emmintrin.h"
#include "../../test_utils.h"
#include "../../../src/code_regions.h"

int main(int argc, char *argv[])
{
	// Argument check
    if (argc <= 4) {
        test_utils::usage("_mm_mul_ss", 4, 1);
		return EXIT_FAILURE;
    }

	// Operand setup

	__m128 a, b, c, d;
	a = _mm_set_ss(test_utils::get_arg<float>(0, argc, argv));
	b = _mm_set_ss(test_utils::get_arg<float>(1, argc, argv));
	c = _mm_set_ss(test_utils::get_arg<float>(2, argc, argv));
	d = _mm_set_ss(test_utils::get_arg<float>(3, argc, argv));
	__m128 x = _mm_set_ss(2.0);
	__m128 ra, rb, rc, rd;

	// Run operation
	BEGIN_ERROR_INJECTION();
	ra = _mm_mul_ss(a, x);
	rb = _mm_mul_ss(b, x);
	rc = _mm_mul_ss(c, x);
	rd = _mm_mul_ss(d, x);
	END_ERROR_INJECTION();

	// Print result
	uint64_t flags = 0;
	test_utils::print_results<__m128>(ra, flags, 1);
	test_utils::print_results<__m128>(rb, flags, 1);
	test_utils::print_results<__m128>(rc, flags, 1);
	test_utils::print_results<__m128>(rd, flags, 1);

    return EXIT_SUCCESS;
}
