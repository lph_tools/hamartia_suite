/*
 *  Testing multiple injection points
 */

#include <cstdlib>
#include <cstdio>
#include <cstdint>
#include <iostream>
#include "../../test_utils.h"
#include "../../../src/code_regions.h"

int main(int argc, char *argv[])
{
	// Argument check
    if (argc <= 4) {
        test_utils::usage("add", 4, 1);
		return EXIT_FAILURE;
    }

	// Operand setup

	uint32_t a, b, c, d;
	a = test_utils::get_arg<uint32_t>(0, argc, argv);
	b = test_utils::get_arg<uint32_t>(1, argc, argv);
	c = test_utils::get_arg<uint32_t>(2, argc, argv);
	d = test_utils::get_arg<uint32_t>(3, argc, argv);

	// Run operation
	BEGIN_ERROR_INJECTION();
	asm(
	"add %[a], %[x];" 
	"add %[b], %[x];" 
	"add %[c], %[x];" 
	"add %[d], %[x];" 
	: [a] "+r" (a), [b] "+r" (b), [c] "+r" (c), [d] "+r" (d)
	: [x] "i" (1)
	: 
	);
	END_ERROR_INJECTION();

	// Print result
	uint64_t flags = test_utils::get_flags();
	test_utils::print_result<uint>(a, flags);
	test_utils::print_result<uint>(b, flags);
	test_utils::print_result<uint>(c, flags);
	test_utils::print_result<uint>(d, flags);

    return EXIT_SUCCESS;
}
