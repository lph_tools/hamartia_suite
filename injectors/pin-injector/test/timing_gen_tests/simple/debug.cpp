/*
 *  64-bit Floating-Point Back-to-Back Test
 */

#include <cstdlib>
#include <cstdio>
#include <cstdint>
#include <iostream>
#include "immintrin.h"
#include "emmintrin.h"
#include "../../test_utils.h"
#include "../../../src/code_regions.h"

int main(int argc, char *argv[])
{
	// Argument check
    if (argc <= 1 || argc > 7) {
        test_utils::usage("_mm_mul_sd", 4, 1);
		return EXIT_FAILURE;
    }

	// Operand setup
	__m128d result1, result2, result3;

	__m128d i11 = _mm_set_sd(test_utils::get_arg<double>(0, argc, argv));
	__m128d i12 = _mm_set_sd(test_utils::get_arg<double>(1, argc, argv));

	__m128d i21 = _mm_set_sd(test_utils::get_arg<double>(2, argc, argv));
	__m128d i22 = _mm_set_sd(test_utils::get_arg<double>(3, argc, argv));

	__m128d i31 = _mm_set_sd(test_utils::get_arg<double>(4, argc, argv));
	__m128d i32 = _mm_set_sd(test_utils::get_arg<double>(5, argc, argv));

	// Run operation
	BEGIN_ERROR_INJECTION();
	result1 = _mm_mul_sd(i11, i12);
	result2 = _mm_mul_sd(i21, i22);
	result3 = _mm_mul_sd(i31, i32);
	END_ERROR_INJECTION();

	// Print result
	uint64_t flags = 0; //test_utils::get_flags();
	test_utils::print_results<__m128d>(result1, flags, 1);
	test_utils::print_results<__m128d>(result2, flags, 1);
	test_utils::print_results<__m128d>(result3, flags, 1);

    return EXIT_SUCCESS;
}

