/*
 *  Floating-Point Back-to-Back Test
 */

#include <cstdlib>
#include <cstdio>
#include <cstdint>
#include <iostream>
#include "immintrin.h"
#include "emmintrin.h"
#include "../../test_utils.h"
#include "../../../src/code_regions.h"

int main(int argc, char *argv[])
{
	// Argument check
    if (argc <= 1 || argc > 3) {
        test_utils::usage("_mm_mul_ss", 1, 1);
		return EXIT_FAILURE;
    }

	// Operand setup
	__m128 result1, result2;

	__m128 i11 = _mm_set_ss(test_utils::get_arg<float>(0, argc, argv));
	__m128 i12 = _mm_set_ss(2.0);

	__m128 i21 = _mm_set_ss(test_utils::get_arg<float>(1, argc, argv));
	__m128 i22 = _mm_set_ss(2.0);

	// Run operation
	BEGIN_ERROR_INJECTION();
	result1 = _mm_mul_ss(i11, i12);
	result2 = _mm_mul_ss(i21, i22);
	END_ERROR_INJECTION();

	// Print result
	uint64_t flags = 0; //test_utils::get_flags();
	test_utils::print_results<__m128>(result1, flags, 1);
	test_utils::print_results<__m128>(result2, flags, 1);

    return EXIT_SUCCESS;
}

