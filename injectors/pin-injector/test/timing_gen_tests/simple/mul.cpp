/*
 *  Testing two back-to-back instructions
 */

#include <cstdlib>
#include <cstdio>
#include <cstdint>
#include <iostream>
#include "../../test_utils.h"
#include "../../../src/code_regions.h"

int main(int argc, char *argv[])
{
	// Argument check
    if (argc <= 1) {
        test_utils::usage("mul", 1, 1);
		return EXIT_FAILURE;
    }

	// Operand setup

	int a;
	a = test_utils::get_arg<int>(0, argc, argv);

	int c = 0;

	// Run operation
	BEGIN_ERROR_INJECTION();
	asm(
	"mul %%ebx;" 
	"mul %%ebx;" 
	: "=a"(a), "=d"(c)
	: "a"(a), "b"(2)
	: 
	);
	END_ERROR_INJECTION();

	// Print result
	uint64_t flags = test_utils::get_flags();
	test_utils::print_result<int>(a, flags);
	test_utils::print_result<int>(c, flags);

    return EXIT_SUCCESS;
}
