/*
 *  64-bit Floating-Point Back-to-Back Test
 */

#include <cstdlib>
#include <cstdio>
#include <cstdint>
#include <iostream>
#include "immintrin.h"
#include "emmintrin.h"
#include "../../test_utils.h"
#include "../../../src/code_regions.h"

int main(int argc, char *argv[])
{
	// Argument check
    if (argc <= 1 || argc > 3) {
        test_utils::usage("_mm_add_sd", 1, 1);
		return EXIT_FAILURE;
    }

	// Operand setup
	__m128d result1, result2;

	__m128d i11 = _mm_set_sd(test_utils::get_arg<double>(0, argc, argv));
	__m128d i12 = _mm_set_sd(1.0);

	__m128d i21 = _mm_set_sd(test_utils::get_arg<double>(1, argc, argv));
	__m128d i22 = _mm_set_sd(1.0);

	// Run operation
	BEGIN_ERROR_INJECTION();
	result1 = _mm_add_sd(i11, i12);
	result2 = _mm_add_sd(i21, i22);
	END_ERROR_INJECTION();

	// Print result
	uint64_t flags = 0; //test_utils::get_flags();
	test_utils::print_results<__m128d>(result1, flags, 1);
	test_utils::print_results<__m128d>(result2, flags, 1);

    return EXIT_SUCCESS;
}

