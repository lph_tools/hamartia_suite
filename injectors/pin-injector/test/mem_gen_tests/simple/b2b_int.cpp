/*
 *  Testing two back-to-back load instructions
 */

#include <cstdlib>
#include <cstdio>
#include <cstdint>
#include <iostream>
#include "immintrin.h"
#include "emmintrin.h"
#include "../../test_utils.h"
#include "../../../src/code_regions.h"

int main(int argc, char *argv[])
{
	// Argument check
    if (argc != 5) {
        test_utils::usage("add", 1, 4);
		return EXIT_FAILURE;
    }

	// Operand setup

    int32_t a[4] __attribute__ ((aligned(16))) ;
    std::cout << "Inputs: " << std::endl;
    for (int i=0; i<4; i++) {
	    a[i] = test_utils::get_arg<int>(i, argc, argv);
        std::cout << a[i] << " ";
    }
    std::cout << std::endl;

    __m128i b, c, d;
	// Run operation
	BEGIN_ERROR_INJECTION();
    b = _mm_load_si128((__m128i*)a);
    c = _mm_load_si128((__m128i*)a);
    d = _mm_add_epi32(b, c);
	END_ERROR_INJECTION();

	// Print result
	uint64_t flags = test_utils::get_flags();
    int32_t* d_int = (int32_t*) &d;
	test_utils::print_results<int32_t*>(d_int, flags, 4);

    return EXIT_SUCCESS;
}
