/*
 *  Testing two back-to-back instructions
 */

#include <cstdlib>
#include <cstdio>
#include <cstdint>
#include <iostream>
#include "immintrin.h"
#include "emmintrin.h"
#include "../../test_utils.h"
#include "../../../src/code_regions.h"

int main(int argc, char *argv[])
{
	// Argument check
    if (argc <= 4) {
        test_utils::usage("add", 2, 2);
		return EXIT_FAILURE;
    }

	// Operand setup

    float a[4];
    for (int i=0; i<4; i++) {
	    a[i] = test_utils::get_arg<float>(i, argc, argv);
        std::cout << a[i] << " ";
    }
    std::cout << std::endl;

    __m128 b;
	// Run operation
	BEGIN_ERROR_INJECTION();
    b = _mm_load_ps(a);
	END_ERROR_INJECTION();

	// Print result
	uint64_t flags = test_utils::get_flags();
	test_utils::print_results<__m128>(b, flags, 4);

    return EXIT_SUCCESS;
}
