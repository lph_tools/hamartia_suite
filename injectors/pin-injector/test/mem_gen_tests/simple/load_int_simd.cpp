/*
 *  Testing injecting a single SIMD load instruction
 */

#include <cstdlib>
#include <cstdio>
#include <cstdint>
#include <iostream>
#include "immintrin.h"
#include "emmintrin.h"
#include "../../test_utils.h"
#include "../../../src/code_regions.h"

int main(int argc, char *argv[])
{
	// Argument check
    if (argc != 5) {
        test_utils::usage("load_int_simd", 1, 4);
		return EXIT_FAILURE;
    }

	// Operand setup

    int a[4] __attribute__ ((aligned(16))) ;
    std::cout << "Inputs: " << std::endl;
    for (int i=0; i<4; i++) {
	    a[i] = test_utils::get_arg<int>(i, argc, argv);
        std::cout << a[i] << " ";
    }
    std::cout << std::endl;

    __m128i b;
	// Run operation
	BEGIN_ERROR_INJECTION();
    b = _mm_load_si128((__m128i*)a);
	END_ERROR_INJECTION();

    _mm_store_si128((__m128i*)a, b); // override a's data

	// Print result
	uint64_t flags = test_utils::get_flags();
	test_utils::print_results<int*>(a, flags, 4);

    return EXIT_SUCCESS;
}
