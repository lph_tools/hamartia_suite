/*
 *  Testing two back-to-back instructions
 */

#include <cstdlib>
#include <cstdio>
#include <cstdint>
#include <iostream>
#include "../../test_utils.h"
#include "../../../src/code_regions.h"

int main(int argc, char *argv[])
{
	// Argument check
    if (argc <= 1) {
        test_utils::usage("add", 2, 1);
		return EXIT_FAILURE;
    }

	// Operand setup

	uint32_t a;
	a = test_utils::get_arg<uint32_t>(0, argc, argv);

	uint32_t c;
	c = test_utils::get_arg<uint32_t>(1, argc, argv);

	// Run operation
	BEGIN_ERROR_INJECTION();
	asm(
	"add %[a], %[b];" 
	"add %[c], %[b];" 
	: [a] "+m" (a), [c] "+m" (c)
	: [b] "i" (1)
	: 
	);
	END_ERROR_INJECTION();

	// Print result
	uint64_t flags = test_utils::get_flags();
	test_utils::print_result<uint>(a, flags);
	test_utils::print_result<uint>(c, flags);

    return EXIT_SUCCESS;
}
