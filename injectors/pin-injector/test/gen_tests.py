#! /bin/env python

import os, sys, yaml, itertools, random, re
from mako_helper import to_mako
from mako.template import Template

# Templates
SCONS_TEMPLATE = "test/SConscript.mako"
TEMPLATE_DIR = "test/templates"
BIN_DIR = "./build_test"
# Types
TYPE_MAPPING = {
    'uint': { 8: 'uint8_t', 16: 'uint16_t', 32: 'uint32_t', 64: 'uint64_t', 128: '__m128i', 256: '_m256i'},
    'int': { 8: 'int8_t', 16: 'int16_t', 32: 'int32_t', 64: 'int64_t', 128: '__m128i', 256: '__m256i'},
    'float': { 32: 'float', 128: '__m128', 256: '__m256'},
    'double': { 64: 'double', 128: '__m128d', 256: '__m256d'}
}
# Args
ARG_REGISTER = 'r'
ARG_MEMORY = 'm'
ARG_IMMEDIATE = 'imm'
ARG_MAX = 512

class ArgType:
    def __init__(self, arg_str):
        self.types = {}
        opts = arg_str.split('/')
        last = []
        for i,o in enumerate(opts):
            m = re.match(r'([a-z]+)(\d*)', o)
            # Type
            atype = m.group(1)
            self.types[atype] = 0
            last.append(atype)
            # Width
            if m.group(2):
                width = int(m.group(2))
                for o in reversed(last):
                    if not self.types[o]:
                        self.types[o] = width

    def tuples(self):
        return [(t, w) for t, w in self.types.iteritems()]

class Arg:
    def __init__(self, arg, datatype, width, base, vals, r, w, reg):
        if arg != 'imm':
            self.arg = arg
            self.var = base
            atype = datatype
            if width in TYPE_MAPPING[datatype]:
                atype = TYPE_MAPPING[datatype][width]
            self.datatype = atype
        else:
            self.arg = 'i'
            self.datatype = 'i'
            self.var = vals[0]

        self.base = base
        self.width = width
        self.vals = vals
        self.r = r
        self.w = w
        self.reg = reg

class TestGroup:
    def __init__(self, model, props):
        self.model = model
        self.tests = []
        self.datatypes = props['types']
        self.props = props

    def addTest(self, test):
        self.tests.append(test)

class Test:
    def __init__(self, test, dirname, bindir, args, props):
        self.test = test
        self.test_name = '_'.join([a.arg + str(a.width) for a in args if not a.reg])
        self.test_file = self.test_name + ".cpp"
        self.test_path = os.path.join(dirname, self.test_file)
        self.bin_name = self.test + '_' + self.test_name
        self.bin_path = os.path.join(bindir, self.bin_name)
        self.run = self.bin_path + " " + " ".join([str(v) for a in args for v in a.vals if a.arg != 'i'])
        self.datatype = props['type']
        self.iclass = props['iclass']
        self.avx = ('XED_ICLASS_V' in self.iclass)

def decode_args(args):
    if type(args) is not list:
        args = [args]

    return [[ArgType(arg.strip()) for arg in argl.split(',')] for argl in args]

def gen_vals(datatype, simd, args, iregs, oregs):
    a_args = []
    vals = []
    base = 'a'
    out_type = ''

    for rn in set(iregs + oregs):
        width = 16
        r = (rn in iregs)
        w = (rn in oregs)
        if rn.startswith('e'):
            width = 32
        elif rn.startswith('r'):
            width = 64
        a_args.append(('g', width, r, w, rn))

    a_args.extend([(a, w, True, (i == 0 and not oregs), None) for i, (a, w) in enumerate(args)])

    for arg, width, r, w, reg in a_args:
        ws = width / simd
        v = []
        if datatype == "uint" or arg == 'imm':
            a_max = min(ARG_MAX, 2**ws-1)
            for i in range(simd):
                v.append(random.randint(0, a_max))

        elif datatype == "int":
            a_max = min(ARG_MAX, 2**(ws-1)-1)
            a_min = max(-ARG_MAX, -2**(ws-1))
            for i in range(simd):
                v.append(random.randint(a_min, a_max))

        elif datatype == "float":
            a_max = min(ARG_MAX, 2**7)
            a_min = max(-ARG_MAX, -2**7)
            for i in range(simd):
                v.append(random.uniform(a_min, a_max))

        elif datatype == "double":
            a_max = min(ARG_MAX, 2**10)
            a_min = max(-ARG_MAX, -2**10)
            for i in range(simd):
                v.append(random.uniform(a_min, a_max))

        vals.append(Arg(arg, datatype, width, base, v, r, w, reg))
        base = chr(ord(base)+1)
        if arg != 'imm' and w:
            out_type = vals[-1].datatype

    return (vals, out_type)

def gen_args(datatype, simd, args, iregs=[], oregs=[]):
    d_args = decode_args(args)
    t_args = [[a.tuples() for a in d_arg] for d_arg in d_args]
    arg_comb = []
    for t_arg in t_args:
        arg_comb.extend(list(itertools.product(*t_arg)))

    return [gen_vals(datatype, simd, a, iregs, oregs) for a in arg_comb]

def gen_tests(in_yaml, out_dir):
    """ Generate test C++ source code and its SConscript to the out_dir
        by taking a config YAML file
    """
    templates = {}
    tests = []

    # Load templates
    for templ in os.listdir(TEMPLATE_DIR):
        (name, ext) = os.path.splitext(templ)
        if ext != '.mako':
            continue
        with open(os.path.join(TEMPLATE_DIR, templ), 'r') as tf:
            templates[name] = Template(tf.read())

    # Read YAML
    in_data = None
    with open(in_yaml, 'r') as yf:
        in_data = yaml.load(yf)

    # Create out directory
    if not os.path.isdir(out_dir):
        os.mkdir(out_dir)

    # Bin directory
    bin_dir = os.path.join(BIN_DIR, os.path.basename(out_dir))

    # Create tests
    for op, props in in_data['ops'].iteritems():
        testname = op
        print testname
        test_dir = os.path.join(out_dir, testname)
        if not os.path.isdir(test_dir):
            os.mkdir(test_dir)

        simd = 1
        iregs = []
        oregs = []
        if 'simd' in props:
            simd  = props['simd']
        if 'iregs' in props:
            iregs = props['iregs']
        if 'oregs' in props:
            oregs = props['oregs']

        test_vals = gen_args(props['type'], simd, props['args'], iregs, oregs)

        for (vals, otype) in test_vals:
            test = Test(testname, test_dir, bin_dir, vals, props)

            is_vector = max([a.width for a in vals]) > 64
            num_args = len([v for v in vals if v.r and v.arg != 'i'])
            with open(test.test_path, 'w') as tf: # dump cpp file
                #print props['template']
                #print [v.__dict__ for v in vals]
                tf.write(templates[props['template']].render(testname=testname, op=op, otype=otype, type=props['type'], simd=simd, is_vector=is_vector, num_args=num_args, args=vals))

            tests.append(test)

    # Create scons
    scons_temp = None
    with open(SCONS_TEMPLATE, 'r') as tf:
        scons_temp = Template(tf.read())

    scons_path = os.path.join(out_dir, "SConscript")
    with open(scons_path, 'w') as tf:
        tf.write(scons_temp.render(bin_dir=bin_dir, tests=tests))

def main(args):
    if len(args) < 2:
        print "usage: ./gen_tests.py <in_yaml> <out_dir>"
        sys.exit(1)

    in_yaml = args[0]
    out_dir = args[1]
    gen_tests(in_yaml, out_dir)

if __name__ == "__main__":
    main(sys.argv[1:])
