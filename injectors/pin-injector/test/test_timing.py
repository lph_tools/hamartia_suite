""" 
Test scripts for testing timing error models

The general flow is 
    (1) Prepare input value
    (2) Launcher Pin instruction-level injector
    (3) Check the error patterns in the output file (should have only X-bit difference)

Test hierarchy
    Fixtures: used to iterate through all combinations of injection modes
    Test functions: one per op type
"""
import pytest
import os
import yaml
from static import INJ_PATH, TIMING_APP_DIR, OUT_FILE
from static import run, teardown

@pytest.fixture(params=['TIMING_PREV1'])
def get_model(request):
    emodel = request.param
    yield emodel

@pytest.fixture(params=['post'])
def get_point(request):
    point = request.param
    yield point
    teardown()

def test_back_to_back(get_model, get_point):
    # op1: prev_in_val += 1
    # op2: cur_in_val += 1
    # [prev_in_val, curr_in_val]
    in_val =  [12, 0] # i.e., error-free run should get (13, 1)
    expect = 12 + 1 # with error, we get (13, 13)
    
    app_path = os.path.join(TIMING_APP_DIR, 'b2b')
    inject(in_val, app_path, emodel=get_model, point=get_point)
    check(get_model, get_point, expect)

def test_gap(get_model, get_point):
    # op1: prev_in_val += 1
    # op2: cur_in_val <<= 1
    # op3: cur_in_val += 1
    # [prev_in_val, curr_in_val]
    in_val =  [2, 3] # i.e., error-free run should get (3, 7)
    expect = 2 + 1 # with error, we get (3, 3)
    
    app_path = os.path.join(TIMING_APP_DIR, 'gap')
    inject(in_val, app_path, emodel=get_model, point=get_point)
    check(get_model, get_point, expect)

def test_fp_back_to_back(get_model, get_point):
    # op1: prev_in_val += 1
    # op2: cur_in_val += 1
    # [prev_in_val, curr_in_val]
    in_val =  [12, 0] # i.e., error-free run should get (13, 1)
    expect = 12 + 1 # with error, we get (13, 13)
    
    app_path = os.path.join(TIMING_APP_DIR, 'fp')
    inject(in_val, app_path, emodel=get_model, point=get_point)
    check(get_model, get_point, expect)

def test_simd(get_model, get_point):
    # op1: prev_in_val[1] += 1 and prev_in_val[0] += 1
    # op2: cur_in_val[1] += 1 and cur_in_val[0] += 1
    # [prev_in_val[1], prev_in_val[0], cur_in_val[1], cur_in_val[0]]
    in_val =  [1, 2, 3, 4] # i.e., error-free run should get (2, 3, 4, 5)
    expect = [3, 2] # with error, we get (2, 3, 2, 3)
    
    app_path = os.path.join(TIMING_APP_DIR, 'fp_add_pd')
    inject(in_val, app_path, emodel=get_model, point=get_point)
    check_simd(get_model, get_point, expect)

def test_overprovision(get_model, get_point):
    """
        Test an optimization where the input pair has no transition
        In such case, we can try inject next pair
    """
    # op1: a += 1
    # op2: b += 1
    # op3: c += 1
    # op4: d += 1
    # [a, b, c, d]
    in_val =  [1, 1, 3, 5] # i.e., error-free run should get (2, 2, 4, 6)
    pts = ['-i', '1', '-i', '2']
    expect = 3 + 1 # with error, we get (2, 2, 4, 4)
    
    app_path = os.path.join(TIMING_APP_DIR, 'multi_point')
    multi_inject(in_val, app_path, pts, emodel=get_model, point=get_point)
    check(get_model, get_point, expect)

"""
def test_bhive(get_model, get_point):
    # op1: prev_in_val += 1
    # op2: cur_in_val += 1
    # [prev_in_val, curr_in_val]
    in_val =  [12, 0] # i.e., error-free run should get (13, 1)
    err_free_val = 1
    
    app_path = os.path.join(TIMING_APP_DIR, 'b2b')
    inject(in_val, app_path, emodel='TIMING_BHIVE5', point=get_point, oclass='OPS_BHIVE')

    with open(OUT_FILE) as f:
        out = yaml.load(f)
        injected_out_val = int(out['injected_operands']['outputs'][0]['value'][0])
        assert injected_out_val != err_free_val, "b-HiVE fails to inject error!"
"""
def inject(in_val, app_path, emodel='TIMING_PREV1', point='post', oclass='OPS_INT,OPS_FP'):
    in_vals = [str(int(v)) for v in in_val]
    cmd = [INJ_PATH, '-e', emodel, '-p', point, '-c', oclass, '-r', '1', '-l', '1', '-i', '1', '--', app_path] + in_vals
    run(cmd)

def multi_inject(in_val, app_path, pts, emodel='TIMING_PREV1', point='post', oclass='OPS_INT,OPS_FP'):
    in_vals = [str(int(v)) for v in in_val]
    cmd = [INJ_PATH, '-e', emodel, '-p', point, '-c', oclass, '-r', '1', '-l', '1'] + pts + ['--', app_path] + in_vals
    run(cmd)

def check(emodel, point, expect):
    assert os.path.exists(OUT_FILE), "Injection output file does not exist!"
    with open(OUT_FILE) as f:
        out = yaml.load(f)
        if point == 'post':
            injected_out_val = int(out['injected_operands']['outputs'][0]['value'][0])
        assert injected_out_val == expect, "Expect {} but got {} instead".format(expect, injected_out_val) 

def check_simd(emodel, point, expect):
    assert os.path.exists(OUT_FILE), "Injection output file does not exist!"
    with open(OUT_FILE) as f:
        out = yaml.load(f)
        if point == 'post':
            for i, exp in enumerate(expect):
                injected_out_val = int(out['injected_operands']['outputs'][0]['value'][i])
                assert injected_out_val == exp, "Expect {} at lane {} but got {} instead".format(exp, i, injected_out_val) 
