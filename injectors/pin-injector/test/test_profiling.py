""" 
Test scripts for testing profiling mode
"""
import pytest
import os
import yaml
from static import INJ_PATH, APP_DIR, OUT_FILE
from static import run, teardown


@pytest.fixture(params=['RANDBIT1'])
def get_model(request):
    emodel = request.param
    yield emodel

@pytest.fixture(params=['post'])
def get_point(request):
    point = request.param
    yield point
    teardown()

def test_profile(get_model, get_point):
    app_path = '/bin/ls'
    cmd = [INJ_PATH, '--', app_path]
    run(cmd)

