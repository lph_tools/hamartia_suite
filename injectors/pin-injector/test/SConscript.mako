"""
Generated Error Injector Tests
"""

Import('tenv')
# Intel asm syntax
tenv.Append(CCFLAGS = ' -std=c++11 -masm=intel')

# Non-AVX tests
% for test in tests:
% if not test.avx:
tenv.Program('${test.bin_name}', '${test.test}/${test.test_name}.cpp')
% endif
% endfor

# AVX tests
atenv = tenv.Clone()
atenv.Append(CCFLAGS = ' -mavx')
% for test in tests:
% if test.avx:
atenv.Program('${test.bin_name}', '${test.test}/${test.test_name}.cpp')
% endif
% endfor
