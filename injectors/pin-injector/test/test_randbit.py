""" 
Test scripts for testing RANDBITX error model

The general flow is 
    (1) Prepare input value
    (2) Launcher Pin instruction-level injector
    (3) Check the error patterns in the output file (should have only X-bit difference)

Test hierarchy
    Fixtures: used to iterate through all combinations of injection modes
    Test functions: one per op type
"""
import pytest
import os
import yaml
from static import INJ_PATH, APP_DIR, OUT_FILE
from static import run, teardown


@pytest.fixture(params=['RANDBIT1','RANDBIT2'])
def get_model(request):
    emodel = request.param
    yield emodel

@pytest.fixture(params=['post','pre'])
def get_point(request):
    point = request.param
    yield point
    teardown()

def test_sar(get_model, get_point):
    in_val = 0x12345000 
    app_path = os.path.join(APP_DIR, 'sar_r32_i8')
    inject(in_val, app_path, emodel=get_model, point=get_point)
    check(get_model, get_point)

def test_add(get_model, get_point):
    in_val = 0x12345000 
    app_path = os.path.join(APP_DIR, 'add_r32_i32')
    inject(in_val, app_path, emodel=get_model, point=get_point)
    check(get_model, get_point)


def inject(in_val, app_path, emodel='RANDBIT1', point='post'):
    in_val = str(int(in_val))
    cmd = [INJ_PATH, '-e', emodel, '-p', point, '-r', '1', '-l', '1', '-i', '1', '--', app_path, in_val]
    run(cmd)

def check(emodel, point):
    def num_of_1s(val):
        return bin(int(str(val), 16))[2:].count("1")

    assert os.path.exists(OUT_FILE), "Injection output file does not exist!"
    with open(OUT_FILE) as f:
        out = yaml.load(f)
        if point == 'post':
            diff = out['difference']['outputs'][0]['diff'][0]['xor']
        elif point == 'pre':
            # FIXME: do we always inject the first input operand?
            diff = out['difference']['inputs'][0]['diff'][0]['xor']
        num_flipped = num_of_1s(diff)
        expect = int(emodel.split('RANDBIT')[1])
        assert num_flipped == expect, "Expect {} flipped but {} flipped instead".format(expect, num_flipped) 
