/*
 *  add - Simple asm
 */

#include <cstdlib>
#include <cstdio>
#include <cstdint>
#include <iostream>
#include "../../test_utils.h"
#include "../../../src/code_regions.h"

int main(int argc, char *argv[])
{
	// Argument check
    if (argc <= 1) {
        test_utils::usage("add", 1, 1);
		return EXIT_FAILURE;
    }

	// Operand setup

	uint32_t a;
	a = test_utils::get_arg<uint32_t>(0, argc, argv);


	// Run operation
	BEGIN_ERROR_INJECTION();
	asm(
	"add %[a], %[b];" 
	: [a] "+m" (a)
	: [b] "i" (313)
	: 
	);
	END_ERROR_INJECTION();

	// Print result
	uint64_t flags = test_utils::get_flags();
	test_utils::print_result<uint>(a, flags);

    return EXIT_SUCCESS;
}
