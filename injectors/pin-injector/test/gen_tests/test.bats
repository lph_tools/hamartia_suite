# Runs all testing scripts

INJ_SO=./build/liberror_prof_inj.so
PIN_OUT=pin.log
INJ_OUT=err_prof_inj.out

CORRECT_OUT=correct.out
INCORRECT_OUT=incorrect.out

function printout {
	echo " = CORRECT = "
	cat $CORRECT
	echo " = INCORRECT = "
	cat $INCORRECT
	echo " = INJ OUT = "
	if [ -e "$INJ_OUT" ]
	then
		cat $INJ_OUT
	else
		echo "No $INJ_OUT!"
	fi
	echo " = PIN OUT = "
	if [ -e "$PIN_OUT" ]
	then
		cat $PIN_OUT
	else
		echo "No $PIN_OUT!"
	fi
}

teardown() {
	if [ -e "$INJ_OUT" ] 
	then
		rm $INJ_OUT
	fi
	if [ -e "$CORRECT_OUT" ] 
	then
		rm $CORRECT_OUT
	fi
	if [ -e "$INCORRECT_OUT" ] 
	then
		rm $INCORRECT_OUT
	fi
	if [ -e "$PIN_OUT" ] 
	then
		rm $PIN_OUT
	fi
}

# MANTRAND4I
# MANTRAND16I
# MANTB2
# BURST4
@test "BURST4, add, r32_i32" {
	corr_out="$(./build_test/gen_tests/add_r32_i32 460 > $CORRECT_OUT)"
	incorr_out="$($PIN_HOME/pin -t $INJ_SO -e BURST4 -c XED_ICLASS_ADD -r -l 1 -i 1 -- ./build_test/gen_tests/add_r32_i32 460 > $INCORRECT_OUT)"
	[ -e "$INJ_OUT" ] && [ -s "$INJ_OUT" ] && [ -n "`diff $INCORRECT_OUT $CORRECT_OUT`" ]
}
@test "BURST4, add, m32_i32" {
	corr_out="$(./build_test/gen_tests/add_m32_i32 288 > $CORRECT_OUT)"
	incorr_out="$($PIN_HOME/pin -t $INJ_SO -e BURST4 -c XED_ICLASS_ADD -r -l 1 -i 1 -- ./build_test/gen_tests/add_m32_i32 288 > $INCORRECT_OUT)"
	[ -e "$INJ_OUT" ] && [ -s "$INJ_OUT" ] && [ -n "`diff $INCORRECT_OUT $CORRECT_OUT`" ]
}
@test "BURST4, add, r32_r32" {
	corr_out="$(./build_test/gen_tests/add_r32_r32 180 342 > $CORRECT_OUT)"
	incorr_out="$($PIN_HOME/pin -t $INJ_SO -e BURST4 -c XED_ICLASS_ADD -r -l 1 -i 1 -- ./build_test/gen_tests/add_r32_r32 180 342 > $INCORRECT_OUT)"
	[ -e "$INJ_OUT" ] && [ -s "$INJ_OUT" ] && [ -n "`diff $INCORRECT_OUT $CORRECT_OUT`" ]
}
@test "BURST4, add, r32_m32" {
	corr_out="$(./build_test/gen_tests/add_r32_m32 72 508 > $CORRECT_OUT)"
	incorr_out="$($PIN_HOME/pin -t $INJ_SO -e BURST4 -c XED_ICLASS_ADD -r -l 1 -i 1 -- ./build_test/gen_tests/add_r32_m32 72 508 > $INCORRECT_OUT)"
	[ -e "$INJ_OUT" ] && [ -s "$INJ_OUT" ] && [ -n "`diff $INCORRECT_OUT $CORRECT_OUT`" ]
}
# RANDOM
@test "RANDOM, add, r32_i32" {
	corr_out="$(./build_test/gen_tests/add_r32_i32 460 > $CORRECT_OUT)"
	incorr_out="$($PIN_HOME/pin -t $INJ_SO -e RANDOM -c XED_ICLASS_ADD -r -l 1 -i 1 -- ./build_test/gen_tests/add_r32_i32 460 > $INCORRECT_OUT)"
	[ -e "$INJ_OUT" ] && [ -s "$INJ_OUT" ] && [ -n "`diff $INCORRECT_OUT $CORRECT_OUT`" ]
}
@test "RANDOM, add, m32_i32" {
	corr_out="$(./build_test/gen_tests/add_m32_i32 288 > $CORRECT_OUT)"
	incorr_out="$($PIN_HOME/pin -t $INJ_SO -e RANDOM -c XED_ICLASS_ADD -r -l 1 -i 1 -- ./build_test/gen_tests/add_m32_i32 288 > $INCORRECT_OUT)"
	[ -e "$INJ_OUT" ] && [ -s "$INJ_OUT" ] && [ -n "`diff $INCORRECT_OUT $CORRECT_OUT`" ]
}
@test "RANDOM, add, r32_r32" {
	corr_out="$(./build_test/gen_tests/add_r32_r32 180 342 > $CORRECT_OUT)"
	incorr_out="$($PIN_HOME/pin -t $INJ_SO -e RANDOM -c XED_ICLASS_ADD -r -l 1 -i 1 -- ./build_test/gen_tests/add_r32_r32 180 342 > $INCORRECT_OUT)"
	[ -e "$INJ_OUT" ] && [ -s "$INJ_OUT" ] && [ -n "`diff $INCORRECT_OUT $CORRECT_OUT`" ]
}
@test "RANDOM, add, r32_m32" {
	corr_out="$(./build_test/gen_tests/add_r32_m32 72 508 > $CORRECT_OUT)"
	incorr_out="$($PIN_HOME/pin -t $INJ_SO -e RANDOM -c XED_ICLASS_ADD -r -l 1 -i 1 -- ./build_test/gen_tests/add_r32_m32 72 508 > $INCORRECT_OUT)"
	[ -e "$INJ_OUT" ] && [ -s "$INJ_OUT" ] && [ -n "`diff $INCORRECT_OUT $CORRECT_OUT`" ]
}
# RANDOM_LH
@test "RANDOM_LH, add, r32_i32" {
	corr_out="$(./build_test/gen_tests/add_r32_i32 460 > $CORRECT_OUT)"
	incorr_out="$($PIN_HOME/pin -t $INJ_SO -e RANDOM_LH -c XED_ICLASS_ADD -r -l 1 -i 1 -- ./build_test/gen_tests/add_r32_i32 460 > $INCORRECT_OUT)"
	[ -e "$INJ_OUT" ] && [ -s "$INJ_OUT" ] && [ -n "`diff $INCORRECT_OUT $CORRECT_OUT`" ]
}
@test "RANDOM_LH, add, m32_i32" {
	corr_out="$(./build_test/gen_tests/add_m32_i32 288 > $CORRECT_OUT)"
	incorr_out="$($PIN_HOME/pin -t $INJ_SO -e RANDOM_LH -c XED_ICLASS_ADD -r -l 1 -i 1 -- ./build_test/gen_tests/add_m32_i32 288 > $INCORRECT_OUT)"
	[ -e "$INJ_OUT" ] && [ -s "$INJ_OUT" ] && [ -n "`diff $INCORRECT_OUT $CORRECT_OUT`" ]
}
@test "RANDOM_LH, add, r32_r32" {
	corr_out="$(./build_test/gen_tests/add_r32_r32 180 342 > $CORRECT_OUT)"
	incorr_out="$($PIN_HOME/pin -t $INJ_SO -e RANDOM_LH -c XED_ICLASS_ADD -r -l 1 -i 1 -- ./build_test/gen_tests/add_r32_r32 180 342 > $INCORRECT_OUT)"
	[ -e "$INJ_OUT" ] && [ -s "$INJ_OUT" ] && [ -n "`diff $INCORRECT_OUT $CORRECT_OUT`" ]
}
@test "RANDOM_LH, add, r32_m32" {
	corr_out="$(./build_test/gen_tests/add_r32_m32 72 508 > $CORRECT_OUT)"
	incorr_out="$($PIN_HOME/pin -t $INJ_SO -e RANDOM_LH -c XED_ICLASS_ADD -r -l 1 -i 1 -- ./build_test/gen_tests/add_r32_m32 72 508 > $INCORRECT_OUT)"
	[ -e "$INJ_OUT" ] && [ -s "$INJ_OUT" ] && [ -n "`diff $INCORRECT_OUT $CORRECT_OUT`" ]
}
# BURST1
@test "BURST1, add, r32_i32" {
	corr_out="$(./build_test/gen_tests/add_r32_i32 460 > $CORRECT_OUT)"
	incorr_out="$($PIN_HOME/pin -t $INJ_SO -e BURST1 -c XED_ICLASS_ADD -r -l 1 -i 1 -- ./build_test/gen_tests/add_r32_i32 460 > $INCORRECT_OUT)"
	[ -e "$INJ_OUT" ] && [ -s "$INJ_OUT" ] && [ -n "`diff $INCORRECT_OUT $CORRECT_OUT`" ]
}
@test "BURST1, add, m32_i32" {
	corr_out="$(./build_test/gen_tests/add_m32_i32 288 > $CORRECT_OUT)"
	incorr_out="$($PIN_HOME/pin -t $INJ_SO -e BURST1 -c XED_ICLASS_ADD -r -l 1 -i 1 -- ./build_test/gen_tests/add_m32_i32 288 > $INCORRECT_OUT)"
	[ -e "$INJ_OUT" ] && [ -s "$INJ_OUT" ] && [ -n "`diff $INCORRECT_OUT $CORRECT_OUT`" ]
}
@test "BURST1, add, r32_r32" {
	corr_out="$(./build_test/gen_tests/add_r32_r32 180 342 > $CORRECT_OUT)"
	incorr_out="$($PIN_HOME/pin -t $INJ_SO -e BURST1 -c XED_ICLASS_ADD -r -l 1 -i 1 -- ./build_test/gen_tests/add_r32_r32 180 342 > $INCORRECT_OUT)"
	[ -e "$INJ_OUT" ] && [ -s "$INJ_OUT" ] && [ -n "`diff $INCORRECT_OUT $CORRECT_OUT`" ]
}
@test "BURST1, add, r32_m32" {
	corr_out="$(./build_test/gen_tests/add_r32_m32 72 508 > $CORRECT_OUT)"
	incorr_out="$($PIN_HOME/pin -t $INJ_SO -e BURST1 -c XED_ICLASS_ADD -r -l 1 -i 1 -- ./build_test/gen_tests/add_r32_m32 72 508 > $INCORRECT_OUT)"
	[ -e "$INJ_OUT" ] && [ -s "$INJ_OUT" ] && [ -n "`diff $INCORRECT_OUT $CORRECT_OUT`" ]
}
# BURST2
@test "BURST2, add, r32_i32" {
	corr_out="$(./build_test/gen_tests/add_r32_i32 460 > $CORRECT_OUT)"
	incorr_out="$($PIN_HOME/pin -t $INJ_SO -e BURST2 -c XED_ICLASS_ADD -r -l 1 -i 1 -- ./build_test/gen_tests/add_r32_i32 460 > $INCORRECT_OUT)"
	[ -e "$INJ_OUT" ] && [ -s "$INJ_OUT" ] && [ -n "`diff $INCORRECT_OUT $CORRECT_OUT`" ]
}
@test "BURST2, add, m32_i32" {
	corr_out="$(./build_test/gen_tests/add_m32_i32 288 > $CORRECT_OUT)"
	incorr_out="$($PIN_HOME/pin -t $INJ_SO -e BURST2 -c XED_ICLASS_ADD -r -l 1 -i 1 -- ./build_test/gen_tests/add_m32_i32 288 > $INCORRECT_OUT)"
	[ -e "$INJ_OUT" ] && [ -s "$INJ_OUT" ] && [ -n "`diff $INCORRECT_OUT $CORRECT_OUT`" ]
}
@test "BURST2, add, r32_r32" {
	corr_out="$(./build_test/gen_tests/add_r32_r32 180 342 > $CORRECT_OUT)"
	incorr_out="$($PIN_HOME/pin -t $INJ_SO -e BURST2 -c XED_ICLASS_ADD -r -l 1 -i 1 -- ./build_test/gen_tests/add_r32_r32 180 342 > $INCORRECT_OUT)"
	[ -e "$INJ_OUT" ] && [ -s "$INJ_OUT" ] && [ -n "`diff $INCORRECT_OUT $CORRECT_OUT`" ]
}
@test "BURST2, add, r32_m32" {
	corr_out="$(./build_test/gen_tests/add_r32_m32 72 508 > $CORRECT_OUT)"
	incorr_out="$($PIN_HOME/pin -t $INJ_SO -e BURST2 -c XED_ICLASS_ADD -r -l 1 -i 1 -- ./build_test/gen_tests/add_r32_m32 72 508 > $INCORRECT_OUT)"
	[ -e "$INJ_OUT" ] && [ -s "$INJ_OUT" ] && [ -n "`diff $INCORRECT_OUT $CORRECT_OUT`" ]
}
# MANTRAND2
# BURST8
@test "BURST8, add, r32_i32" {
	corr_out="$(./build_test/gen_tests/add_r32_i32 460 > $CORRECT_OUT)"
	incorr_out="$($PIN_HOME/pin -t $INJ_SO -e BURST8 -c XED_ICLASS_ADD -r -l 1 -i 1 -- ./build_test/gen_tests/add_r32_i32 460 > $INCORRECT_OUT)"
	[ -e "$INJ_OUT" ] && [ -s "$INJ_OUT" ] && [ -n "`diff $INCORRECT_OUT $CORRECT_OUT`" ]
}
@test "BURST8, add, m32_i32" {
	corr_out="$(./build_test/gen_tests/add_m32_i32 288 > $CORRECT_OUT)"
	incorr_out="$($PIN_HOME/pin -t $INJ_SO -e BURST8 -c XED_ICLASS_ADD -r -l 1 -i 1 -- ./build_test/gen_tests/add_m32_i32 288 > $INCORRECT_OUT)"
	[ -e "$INJ_OUT" ] && [ -s "$INJ_OUT" ] && [ -n "`diff $INCORRECT_OUT $CORRECT_OUT`" ]
}
@test "BURST8, add, r32_r32" {
	corr_out="$(./build_test/gen_tests/add_r32_r32 180 342 > $CORRECT_OUT)"
	incorr_out="$($PIN_HOME/pin -t $INJ_SO -e BURST8 -c XED_ICLASS_ADD -r -l 1 -i 1 -- ./build_test/gen_tests/add_r32_r32 180 342 > $INCORRECT_OUT)"
	[ -e "$INJ_OUT" ] && [ -s "$INJ_OUT" ] && [ -n "`diff $INCORRECT_OUT $CORRECT_OUT`" ]
}
@test "BURST8, add, r32_m32" {
	corr_out="$(./build_test/gen_tests/add_r32_m32 72 508 > $CORRECT_OUT)"
	incorr_out="$($PIN_HOME/pin -t $INJ_SO -e BURST8 -c XED_ICLASS_ADD -r -l 1 -i 1 -- ./build_test/gen_tests/add_r32_m32 72 508 > $INCORRECT_OUT)"
	[ -e "$INJ_OUT" ] && [ -s "$INJ_OUT" ] && [ -n "`diff $INCORRECT_OUT $CORRECT_OUT`" ]
}
# MANTRAND8
# RANDBIT2
@test "RANDBIT2, add, r32_i32" {
	corr_out="$(./build_test/gen_tests/add_r32_i32 460 > $CORRECT_OUT)"
	incorr_out="$($PIN_HOME/pin -t $INJ_SO -e RANDBIT2 -c XED_ICLASS_ADD -r -l 1 -i 1 -- ./build_test/gen_tests/add_r32_i32 460 > $INCORRECT_OUT)"
	[ -e "$INJ_OUT" ] && [ -s "$INJ_OUT" ] && [ -n "`diff $INCORRECT_OUT $CORRECT_OUT`" ]
}
@test "RANDBIT2, add, m32_i32" {
	corr_out="$(./build_test/gen_tests/add_m32_i32 288 > $CORRECT_OUT)"
	incorr_out="$($PIN_HOME/pin -t $INJ_SO -e RANDBIT2 -c XED_ICLASS_ADD -r -l 1 -i 1 -- ./build_test/gen_tests/add_m32_i32 288 > $INCORRECT_OUT)"
	[ -e "$INJ_OUT" ] && [ -s "$INJ_OUT" ] && [ -n "`diff $INCORRECT_OUT $CORRECT_OUT`" ]
}
@test "RANDBIT2, add, r32_r32" {
	corr_out="$(./build_test/gen_tests/add_r32_r32 180 342 > $CORRECT_OUT)"
	incorr_out="$($PIN_HOME/pin -t $INJ_SO -e RANDBIT2 -c XED_ICLASS_ADD -r -l 1 -i 1 -- ./build_test/gen_tests/add_r32_r32 180 342 > $INCORRECT_OUT)"
	[ -e "$INJ_OUT" ] && [ -s "$INJ_OUT" ] && [ -n "`diff $INCORRECT_OUT $CORRECT_OUT`" ]
}
@test "RANDBIT2, add, r32_m32" {
	corr_out="$(./build_test/gen_tests/add_r32_m32 72 508 > $CORRECT_OUT)"
	incorr_out="$($PIN_HOME/pin -t $INJ_SO -e RANDBIT2 -c XED_ICLASS_ADD -r -l 1 -i 1 -- ./build_test/gen_tests/add_r32_m32 72 508 > $INCORRECT_OUT)"
	[ -e "$INJ_OUT" ] && [ -s "$INJ_OUT" ] && [ -n "`diff $INCORRECT_OUT $CORRECT_OUT`" ]
}
# MANTR1
# MANTR2
# RANDBIT1
@test "RANDBIT1, add, r32_i32" {
	corr_out="$(./build_test/gen_tests/add_r32_i32 460 > $CORRECT_OUT)"
	incorr_out="$($PIN_HOME/pin -t $INJ_SO -e RANDBIT1 -c XED_ICLASS_ADD -r -l 1 -i 1 -- ./build_test/gen_tests/add_r32_i32 460 > $INCORRECT_OUT)"
	[ -e "$INJ_OUT" ] && [ -s "$INJ_OUT" ] && [ -n "`diff $INCORRECT_OUT $CORRECT_OUT`" ]
}
@test "RANDBIT1, add, m32_i32" {
	corr_out="$(./build_test/gen_tests/add_m32_i32 288 > $CORRECT_OUT)"
	incorr_out="$($PIN_HOME/pin -t $INJ_SO -e RANDBIT1 -c XED_ICLASS_ADD -r -l 1 -i 1 -- ./build_test/gen_tests/add_m32_i32 288 > $INCORRECT_OUT)"
	[ -e "$INJ_OUT" ] && [ -s "$INJ_OUT" ] && [ -n "`diff $INCORRECT_OUT $CORRECT_OUT`" ]
}
@test "RANDBIT1, add, r32_r32" {
	corr_out="$(./build_test/gen_tests/add_r32_r32 180 342 > $CORRECT_OUT)"
	incorr_out="$($PIN_HOME/pin -t $INJ_SO -e RANDBIT1 -c XED_ICLASS_ADD -r -l 1 -i 1 -- ./build_test/gen_tests/add_r32_r32 180 342 > $INCORRECT_OUT)"
	[ -e "$INJ_OUT" ] && [ -s "$INJ_OUT" ] && [ -n "`diff $INCORRECT_OUT $CORRECT_OUT`" ]
}
@test "RANDBIT1, add, r32_m32" {
	corr_out="$(./build_test/gen_tests/add_r32_m32 72 508 > $CORRECT_OUT)"
	incorr_out="$($PIN_HOME/pin -t $INJ_SO -e RANDBIT1 -c XED_ICLASS_ADD -r -l 1 -i 1 -- ./build_test/gen_tests/add_r32_m32 72 508 > $INCORRECT_OUT)"
	[ -e "$INJ_OUT" ] && [ -s "$INJ_OUT" ] && [ -n "`diff $INCORRECT_OUT $CORRECT_OUT`" ]
}
# MANTR4
# MANTRAND8I
# RANDBIT4
@test "RANDBIT4, add, r32_i32" {
	corr_out="$(./build_test/gen_tests/add_r32_i32 460 > $CORRECT_OUT)"
	incorr_out="$($PIN_HOME/pin -t $INJ_SO -e RANDBIT4 -c XED_ICLASS_ADD -r -l 1 -i 1 -- ./build_test/gen_tests/add_r32_i32 460 > $INCORRECT_OUT)"
	[ -e "$INJ_OUT" ] && [ -s "$INJ_OUT" ] && [ -n "`diff $INCORRECT_OUT $CORRECT_OUT`" ]
}
@test "RANDBIT4, add, m32_i32" {
	corr_out="$(./build_test/gen_tests/add_m32_i32 288 > $CORRECT_OUT)"
	incorr_out="$($PIN_HOME/pin -t $INJ_SO -e RANDBIT4 -c XED_ICLASS_ADD -r -l 1 -i 1 -- ./build_test/gen_tests/add_m32_i32 288 > $INCORRECT_OUT)"
	[ -e "$INJ_OUT" ] && [ -s "$INJ_OUT" ] && [ -n "`diff $INCORRECT_OUT $CORRECT_OUT`" ]
}
@test "RANDBIT4, add, r32_r32" {
	corr_out="$(./build_test/gen_tests/add_r32_r32 180 342 > $CORRECT_OUT)"
	incorr_out="$($PIN_HOME/pin -t $INJ_SO -e RANDBIT4 -c XED_ICLASS_ADD -r -l 1 -i 1 -- ./build_test/gen_tests/add_r32_r32 180 342 > $INCORRECT_OUT)"
	[ -e "$INJ_OUT" ] && [ -s "$INJ_OUT" ] && [ -n "`diff $INCORRECT_OUT $CORRECT_OUT`" ]
}
@test "RANDBIT4, add, r32_m32" {
	corr_out="$(./build_test/gen_tests/add_r32_m32 72 508 > $CORRECT_OUT)"
	incorr_out="$($PIN_HOME/pin -t $INJ_SO -e RANDBIT4 -c XED_ICLASS_ADD -r -l 1 -i 1 -- ./build_test/gen_tests/add_r32_m32 72 508 > $INCORRECT_OUT)"
	[ -e "$INJ_OUT" ] && [ -s "$INJ_OUT" ] && [ -n "`diff $INCORRECT_OUT $CORRECT_OUT`" ]
}
# MANTB1
# MANTR8
# RANDBIT8
@test "RANDBIT8, add, r32_i32" {
	corr_out="$(./build_test/gen_tests/add_r32_i32 460 > $CORRECT_OUT)"
	incorr_out="$($PIN_HOME/pin -t $INJ_SO -e RANDBIT8 -c XED_ICLASS_ADD -r -l 1 -i 1 -- ./build_test/gen_tests/add_r32_i32 460 > $INCORRECT_OUT)"
	[ -e "$INJ_OUT" ] && [ -s "$INJ_OUT" ] && [ -n "`diff $INCORRECT_OUT $CORRECT_OUT`" ]
}
@test "RANDBIT8, add, m32_i32" {
	corr_out="$(./build_test/gen_tests/add_m32_i32 288 > $CORRECT_OUT)"
	incorr_out="$($PIN_HOME/pin -t $INJ_SO -e RANDBIT8 -c XED_ICLASS_ADD -r -l 1 -i 1 -- ./build_test/gen_tests/add_m32_i32 288 > $INCORRECT_OUT)"
	[ -e "$INJ_OUT" ] && [ -s "$INJ_OUT" ] && [ -n "`diff $INCORRECT_OUT $CORRECT_OUT`" ]
}
@test "RANDBIT8, add, r32_r32" {
	corr_out="$(./build_test/gen_tests/add_r32_r32 180 342 > $CORRECT_OUT)"
	incorr_out="$($PIN_HOME/pin -t $INJ_SO -e RANDBIT8 -c XED_ICLASS_ADD -r -l 1 -i 1 -- ./build_test/gen_tests/add_r32_r32 180 342 > $INCORRECT_OUT)"
	[ -e "$INJ_OUT" ] && [ -s "$INJ_OUT" ] && [ -n "`diff $INCORRECT_OUT $CORRECT_OUT`" ]
}
@test "RANDBIT8, add, r32_m32" {
	corr_out="$(./build_test/gen_tests/add_r32_m32 72 508 > $CORRECT_OUT)"
	incorr_out="$($PIN_HOME/pin -t $INJ_SO -e RANDBIT8 -c XED_ICLASS_ADD -r -l 1 -i 1 -- ./build_test/gen_tests/add_r32_m32 72 508 > $INCORRECT_OUT)"
	[ -e "$INJ_OUT" ] && [ -s "$INJ_OUT" ] && [ -n "`diff $INCORRECT_OUT $CORRECT_OUT`" ]
}
# MANTRAND
# MANTRAND16
# MANTRAND2I
# MANTB4
# MANTB8
# MANTRAND4
