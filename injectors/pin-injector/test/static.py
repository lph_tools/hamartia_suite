"""
Static global constants/functions for testing
"""
import os
import subprocess

INJ_PATH = os.path.abspath('../bin/injector.sh')
APP_DIR = os.path.abspath('../build_test/gen_tests/')
TIMING_APP_DIR = os.path.abspath('../build_test/timing_gen_tests/')
MEM_APP_DIR = os.path.abspath('../build_test/mem_gen_tests/')
MEM_ERR_CONFIG_DIR = os.path.abspath('mem_gen_tests/error_configs/')
OUT_FILE = 'err_prof_inj.out'
MASKED_FILE = 'masked.yaml'

def run(cmd):
    child_process = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    out, err = child_process.communicate()

def teardown():    
    # teardown code
    if os.path.exists(OUT_FILE):
        os.remove(OUT_FILE)
    if os.path.exists(MASKED_FILE):
        os.remove(MASKED_FILE)
