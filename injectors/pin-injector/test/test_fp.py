""" 
Tests for injecting floating-point operations

"""
import pytest
import os
import yaml
from static import INJ_PATH, APP_DIR, OUT_FILE
from static import run, teardown

@pytest.fixture(params=['RANDBIT1'])
def get_model(request):
    emodel = request.param
    yield emodel

@pytest.fixture(params=['post'])
def get_point(request):
    point = request.param
    yield point
    teardown()

def test_addpd(get_model, get_point):
    # add two 64-bit fp numbers in parallel (c[i] = a[i] + b[i] i=0,1)
    a = [1, 2]
    b = [2, 1]
    in_val = a + b
    app_path = os.path.join(APP_DIR, '_mm_add_pd_r128_r128')
    inject(in_val, app_path, emodel=get_model, point=get_point)
    check(get_model, get_point)

def test_vaddpd(get_model, get_point):
    # add four 64-bit fp numbers in parallel (c[i] = a[i] + b[i] i=0,1,2,3)
    a = [1, 2, 3, 4]
    b = [4, 3, 2, 1]
    in_val = a + b
    app_path = os.path.join(APP_DIR, '_mm256_add_pd_r256_r256')
    inject(in_val, app_path, emodel=get_model, point=get_point)
    check(get_model, get_point)


def inject(in_val, app_path, emodel='RANDBIT1', point='post'):
    in_val = [str(x) for x in in_val]
    cmd = [INJ_PATH, '-e', emodel, '-p', point, '-r', '1', '-l', '1', '-i', '1', '--', app_path] + in_val
    run(cmd)

def check(emodel, point):
    def num_of_1s(val):
        return bin(int(str(val), 16))[2:].count("1")

    assert os.path.exists(OUT_FILE), "Injection output file does not exist!"
    with open(OUT_FILE) as f:
        out = yaml.load(f)
        if point == 'post':
            # FIXME: do we always inject the first lane?
            diff = out['difference']['outputs'][0]['diff'][0]['xor']
        elif point == 'pre':
            diff = out['difference']['inputs'][0]['diff'][0]['xor']
        num_flipped = num_of_1s(diff)
        expect = int(emodel.split('RANDBIT')[1])
        assert num_flipped == expect, "Expect {} flipped but {} flipped instead".format(expect, num_flipped) 
