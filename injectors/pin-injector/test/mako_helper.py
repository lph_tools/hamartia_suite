# Copyright 2015, The University of Texas at Austin 
# All rights reserved.
# 
# THIS FILE IS PART OF THE HAMARTIA FAULT/ERROR ANALYSIS SUITE
# 
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met: 
# 
# 1. Redistributions of source code must retain the above copyright
# notice, this list of conditions and the following disclaimer. 
# 
# 2. Redistributions in binary form must reproduce the above copyright
# notice, this list of conditions and the following disclaimer in the
# documentation and/or other materials provided with the distribution. 
# 
# 3. Neither the name of the copyright holder nor the names of its
# contributors may be used to endorse or promote products derived from
# this software without specific prior written permission. 
# 
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
# FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
# COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
# INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
# BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
# ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

"""
Mako Template Helpers

Allow for regular python dictionaries to be converted into a more easily accessible format
in Mako. For example, instead of "{$variable[key]}" you can use "{$variable.key}".
"""

# Mako Helpers
class MakoBunch(dict):
    def __init__(self, d):
        """
        Args:
            d (dict): dictionary to convert
        """

        dict.__init__(self, d)
        self.__dict__.update(d)

def to_mako(dl):
    """
    Make a dict into a object (easy for mako)
    
    Args:
        dl (object): object to convert (mainly handles dicts)

    Returns:
        object: Mako friendly object
    """
    if type(dl) is list:
        # If list, just convert elements
        r = []
        for i in dl:
            r.append(to_mako(i))
        return r
    elif type(dl) is dict:
        # Convert dicts
        r = {}
        for k, v in dl.items():
            if isinstance(v, dict):
                v = to_mako(v)
            r[k] = v
        return MakoBunch(r)
