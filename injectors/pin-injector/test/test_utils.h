#ifndef TEST_UTILS
#define TEST_UTILS

#include <cstdint>
#include <string>
#include <sstream>
#include <iostream>

namespace test_utils {
	void usage(std::string op, int num_args, int simd)
	{
		std::cout << "Args : ";
		for (int i = 0; i < num_args; ++i) {
			if (simd > 1) {
				for (int j = 0; j < simd; ++j) {
					std::cout << "<" << char('a' + i) << j << "> ";
				}
			} else {
				std::cout << "<" << char('a' + i) << "> ";
			}
		}
		std::cout << "\nOutput: " << op;
		for (int i = 0; i < num_args; ++i) {
			if (i > 0) std::cout << ",";
			std::cout << " <" << char('a' + i) << ">";
		}
		std::cout << std::endl;
	}

	template<typename T>
	T get_arg(int index, int argc, char* argv[])
	{
		++index;
		if (index >= argc) return 0;

		std::stringstream ss(argv[index]);
		T val;
		ss >> val;

		return val;
	}

	inline uint64_t get_flags()
	{
		uint64_t flags = 0;
		asm("pushfq;"
			"pop rax;"
			"mov %0, rax;" 
			: "=m" (flags) 
			: 
			: "rax"
		   );

		return flags;
	}

	template<typename T>
	void print_result(T val, uint64_t flags)
	{
		std::cout << val << std::endl;
		std::cout << flags << std::endl;
	}

	template<typename T>
	void print_results(T val, uint64_t flags, int simd)
	{
		for (int i = 0; i < simd; ++i) {
			std::cout << val[i] << std::endl;
		}
		std::cout << flags << std::endl;
	}
}

#endif
