/*
 *  ${testname} - Simple intrinsic
 */

#include <cstdlib>
#include <cstdio>
#include <cstdint>
#include <iostream>
#include "immintrin.h"
#include "emmintrin.h"
#include "../../test_utils.h"
#include "../../../src/code_regions.h"

int main(int argc, char *argv[])
{
	// Argument check
    if (argc <= ${num_args * simd}) {
        test_utils::usage("${op}", ${num_args}, ${simd});
		return EXIT_FAILURE;
    }

	// Operand setup
% if is_vector:
	${otype} result = { ${', '.join(["0"] * simd)} };
% else:
	${otype} result = 0;
% endif
<% arg_idx = 0 %>
% for a in args:
% if a.arg != 'i':
	${a.datatype} ${a.var};
% if a.width <= 64:
	${a.var} = test_utils::get_arg<${a.datatype}>(${arg_idx}, argc, argv);
% else:
% for iv, v in enumerate(a.vals):
	${a.var}[${iv}] = test_utils::get_arg<${type}>(${arg_idx + iv}, argc, argv);
% endfor
% endif
<%   arg_idx += len(a.vals) %>
% endif
% endfor

	// Run operation
	BEGIN_ERROR_INJECTION();
	result = ${op}(${', '.join([str(a.var) if a.arg != 'm' else "&"+str(a.var) for a in args])});
	END_ERROR_INJECTION();

	// Print result
	uint64_t flags = 0; //test_utils::get_flags();
% if is_vector:
	test_utils::print_results<${otype}>(result, flags, ${simd});
% else:
	test_utils::print_result<${type}>(result, flags);
% endif

    return EXIT_SUCCESS;
}

