/*
 *  ${testname} - Simple op
 */

#include <cstdlib>
#include <cstdio>
#include <cstdint>
#include <iostream>
#include "test_utils.h"

int main(int argc, char *argv[])
{
	// Argument check
    if (argc <= ${num_args * simd}) {
        test_utils::usage("${op}", ${num_args}, ${simd});
		return EXIT_FAILURE;
    }

	// Operand setup
	${args[0][2]} result = 0;
<% arg_idx = 0 %>
% for arg, width, atype, base, vals in args:
	${atype} ${base};
% if width <= 64:
	${base} = test_utils::get_arg<${atype}>(${arg_idx}, argc, argv);
% else:
% for iv, v in enumerate(vals):
	${base}[${iv}] = test_utils::get_arg<${type}>(${arg_idx + iv}, argc, argv);
% endfor
% endif
<%   arg_idx += len(vals) %>
% endfor

	// Run operation
    result = ${args[0][3]} ${op} ${args[1][3]};

	// Print result
	uint64_t flags = test_utils::get_flags();
	test_utils::print_result<${type}>(result, flags, ${simd});

    return EXIT_SUCCESS;
}
