/*
 *  ${testname} - Simple asm
 */

#include <cstdlib>
#include <cstdio>
#include <cstdint>
#include <iostream>
#include "../../test_utils.h"
#include "../../../src/code_regions.h"

int main(int argc, char *argv[])
{
	// Argument check
    if (argc <= ${num_args * simd}) {
        test_utils::usage("${op}", ${num_args}, ${simd});
		return EXIT_FAILURE;
    }

	// Operand setup
<% arg_idx = 0 %>
% for a in args:
% if a.arg != 'i':
	${a.datatype} ${a.var};
% if a.r:
% if a.width <= 64:
	${a.var} = test_utils::get_arg<${a.datatype}>(${arg_idx}, argc, argv);
% else:
% for iv, v in enumerate(a.vals):
	${base}[${iv}] = test_utils::get_arg<${type}>(${arg_idx + iv}, argc, argv);
% endfor
% endif
<%   arg_idx += len(a.vals) %>
% endif
% endif
% endfor

	// Run operation
	BEGIN_ERROR_INJECTION();
	asm(
% for arg in [a for a in args if a.reg and a.r]:
	"mov ${arg.reg}, %[${arg.base}];"
% endfor
	"${op} ${', '.join(['%[' + a.base + ']' for a in args if not a.reg])};" 
% for arg in [a for a in args if a.reg and a.w]:
	"mov %[${arg.base}], ${arg.reg};"
% endfor
	: ${', '.join(['[' + a.base + '] "+' + a.arg + '" (' + str(a.var) + ')' for a in args if a.w])}
	: ${', '.join(['[' + a.base + '] "' + a.arg + '" (' + str(a.var) + ')' for a in args if a.r and not a.w])}
	: ${', '.join(['"' + a.reg + '"' for a in args if a.reg])}
	);
	END_ERROR_INJECTION();

	// Print result
	uint64_t flags = test_utils::get_flags();
% for a in [x for x in args if x.w]:
% if simd > 1:
	test_utils::print_results<${a.datatype}>(${a.var}, flags, ${simd});
% else:
	test_utils::print_result<${type}>(${a.var}, flags);
% endif
% endfor

    return EXIT_SUCCESS;
}
