Tests for Instruction-Level Error Injector
==========================================

Dependencies
------------
- Python module
    `pip install pytest pyyaml mako`  
- Linux packages
    `sudo zypper/apt-get install scons`

Flow
----
The testing flow consists of two major steps: *test generation* and *test running*.

1. **Test generation**: run `scons test` in the parent folder
    - *Results*: Binaries to test are generated in `../build_test/`
    - *How it works*: the `gen_tests` function in `gen_tests.py` is called by `../SConstruct`

2. **Test running**: runs `pytest` in this folder
    - *Results*: pytest will show the test results

Directory Structure
-------------------
The most important files that would be modified if new features are added:
- `gen_test.py`: script that generates test source code in C++ and the corresponding SConscript
- `test.yaml`: test config files (error models and operation to test)
- `test_*.py`: pytest scripts for each test

The following files are just dependencies and helpers.
- `SConscript`: interface for `../SConstruct`
- `SConscript.mako`: mako template of SConscript for compiling C++ code
- `test_utils.h`: utility functions needed by C++ code
- `templates`(dir): templates of C++ code
