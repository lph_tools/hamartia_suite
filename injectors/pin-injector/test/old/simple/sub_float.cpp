/*
 *  Simple subtract float test
 */

#include <stdlib.h>
#include <stdio.h>
#include "unroll.h"

int main(int argc, char *argv[])
{
    if (argc <= 1) {
        printf("Args  : <a> <b>\n");
        printf("Output: <a> - <b>\n");
    }

    float a = atof(argv[1]);
    float b = atof(argv[2]);
    float result = 0;

    result = a - b;

    printf("%.32f\n", result);

    return EXIT_SUCCESS;
}
