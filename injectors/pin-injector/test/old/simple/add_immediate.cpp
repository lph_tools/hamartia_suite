/*
 *  Simple add with immediate test
 */

#include <stdlib.h>
#include <stdio.h>

int main(int argc, char *argv[])
{
    if (argc <= 1) {
        printf("Args  : <a>\n");
        printf("Output: <a> + 10\n");
    }

    int result = atoi(argv[1]);

    result += 10;

    printf("%d\n", result);

    return EXIT_SUCCESS;
}
