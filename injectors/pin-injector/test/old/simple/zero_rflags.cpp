/*
 *  Simple rflags test
 */

#include <stdlib.h>
#include <stdio.h>

int main(int argc, char *argv[])
{
    // Prevent optimization
    volatile int a = 5;
    volatile int b = 5;

    if (a - b) {
        printf("NONZERO\n");
    } else {
        printf("ZERO\n");
    }

    return EXIT_SUCCESS;
}
