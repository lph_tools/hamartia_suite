/*
 * Unroller
 */

template <unsigned N> struct unroll {
    template <typename F> static void call(F const& f) {
        f();
        unroll<N-1>::call(f);
    }
};

template <> struct unroll<0u> {
    template <typename F> static void call(F const&) {}
};
