/*
 *  Simple add with memory test
 */

#include <stdlib.h>
#include <stdio.h>

int main(int argc, char *argv[])
{
    if (argc <= 1) {
        printf("Args  : <a> <b>\n");
        printf("Output: <a> + <b>\n");
    }

    // FIXME: kind of works?
    int *b = (int*) malloc(sizeof(int));
    *b = atoi(argv[2]);
    int result = atoi(argv[1]);

    result += *b;
    free(b);

    printf("%d\n", result);

    return EXIT_SUCCESS;
}
