/*
 *  Simple multiply test
 */

#include <stdlib.h>
#include <stdio.h>

int main(int argc, char *argv[])
{
    if (argc <= 1) {
        printf("Args  : <a> <b>\n");
        printf("Output: <a> * <b>\n");
    }

    unsigned int a = atoi(argv[1]);
    unsigned int b = atoi(argv[2]);
    unsigned int result = 0;

    result = a * b;

    printf("%d\n", result);

    return EXIT_SUCCESS;
}
