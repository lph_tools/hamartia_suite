/*
 *  Simple add double test
 */

#include <stdlib.h>
#include <stdio.h>
#include "unroll.h"

int main(int argc, char *argv[])
{
    if (argc <= 1) {
        printf("Args  : <a> <b>\n");
        printf("Output: <a> + <b>\n");
    }

    double a = atof(argv[1]);
    double b = atof(argv[2]);
    double result = 0;

    result = a + b;

    printf("%.32f\n", result);

    return EXIT_SUCCESS;
}
