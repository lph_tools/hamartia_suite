""" 
Test scripts for testing memory injection

The general flow is 
    (1) Prepare input value
    (2) Launcher Pin instruction-level injector
    (3) Check the error patterns in the output file (should have only X-bit difference)

Test hierarchy
    Fixtures: used to iterate through all combinations of injection modes
    Test functions: one per op type
"""
import pytest
import os
import yaml
from static import INJ_PATH, APP_DIR, MEM_APP_DIR, MEM_ERR_CONFIG_DIR, OUT_FILE
from static import run, teardown


@pytest.fixture(params=['RANDBIT1'])
def get_model(request):
    emodel = request.param
    yield emodel

@pytest.fixture(params=['pre'])
def get_point(request):
    point = request.param
    yield point
    teardown()

def test_add(get_model, get_point): # transient memory pre-injection
    in_val = [0x12345000]
    app_path = os.path.join(APP_DIR, 'add_m32_i32')
    inject(in_val, app_path, emodel=get_model, emodel_cfg='', point=get_point)
    check(get_model, get_point)

def test_load_int_simd(get_model, get_point): # permanent memory pre-injection
    in_val = [0, 2, 3, 4]
    app_path = os.path.join(MEM_APP_DIR, 'load_int_simd')
    ecfg_path = os.path.join(MEM_ERR_CONFIG_DIR, 'simple.yaml') # inject LSB of lane 0
    inject(in_val, app_path, emodel='MEM_HIGH', emodel_cfg=ecfg_path, point=get_point, oclass='OPS_MOV')
    assert os.path.exists(OUT_FILE), "Injection output file does not exist!"
    with open(OUT_FILE) as f:
        out = yaml.load(f)
        out_vals = out['injected_operands']['outputs'][0]['value']
        assert out_vals == [1, 2, 3, 4]


def test_load_ps(get_model, get_point):
    in_val = [1, 2, 3, 4]
    app_path = os.path.join(MEM_APP_DIR, 'ldps')
    ecfg_path = os.path.join(MEM_ERR_CONFIG_DIR, 'simple.yaml') # inject LSB of lane 0
    inject(in_val, app_path, emodel='MEM_HIGH', emodel_cfg=ecfg_path, point=get_point, oclass='OPS_MOV')
    # FIXME: Guarantees no seg-fault for now. Need a more fine-grained check.


#def test_perm_back_to_back(get_model, get_point):
#    # in_val = 0x12345000
#    # op1: in_val += 1
#    # op2: in_val += 1
#    # error_free: in_val == 0x12345002
#    # expect: in_val == 0x12345005
#    in_val = 0x12345000 
#    app_path = 
#    inject(in_val, app_path, emodel='MEM_PERM_STUCK_1_LSB', point=get_point)
#    # check


def inject(in_val, app_path, emodel='RANDBIT1', emodel_cfg='', point='pre', oclass='OPS_INT,OPS_FP'):
    in_vals = [str(int(v)) for v in in_val]
    cmd = [INJ_PATH, '-e', emodel, '-q', emodel_cfg, '-p', point, '-c', oclass, '-r', '1', '-l', '1', '-i', '1', '--', app_path] + in_vals
    run(cmd)

def check(emodel, point):
    def num_of_1s(val):
        return bin(int(str(val), 16))[2:].count("1")

    assert os.path.exists(OUT_FILE), "Injection output file does not exist!"
    with open(OUT_FILE) as f:
        out = yaml.load(f)
        if point == 'post':
            diff = out['difference']['outputs'][0]['diff'][0]['xor']
        elif point == 'pre':
            # FIXME: do we always inject the first input operand?
            diff = out['difference']['inputs'][0]['diff'][0]['xor']
        num_flipped = num_of_1s(diff)
        expect = int(emodel.split('RANDBIT')[1])
        assert num_flipped == expect, "Expect {} flipped but {} flipped instead".format(expect, num_flipped) 
