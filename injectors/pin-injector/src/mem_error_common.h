// Copyright 2015, The University of Texas at Austin 
// All rights reserved.
// 
// THIS FILE IS PART OF THE HAMARTIA FAULT/ERROR ANALYSIS SUITE
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met: 
// 
// 1. Redistributions of source code must retain the above copyright
// notice, this list of conditions and the following disclaimer. 
// 
// 2. Redistributions in binary form must reproduce the above copyright
// notice, this list of conditions and the following disclaimer in the
// documentation and/or other materials provided with the distribution. 
// 
// 3. Neither the name of the copyright holder nor the names of its
// contributors may be used to endorse or promote products derived from
// this software without specific prior written permission. 
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
// FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
// COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
// BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
// ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

/**
 *  \file
 *  \brief Common memory error types
 *
 *  \version 1.1.0
 *  \author Chun-Kai Chang, University of Texas
 *
 *  \addtogroup error_profiler_injector
 *  @{
 */

#ifndef _MEM_ERROR_COMMON_H
#define _MEM_ERROR_COMMON_H

#include <stdint.h>

typedef enum {
    AS_VIRT,
    AS_DRAM,
    AS_END
} AddrSpace;

typedef uint64_t virt_addr_t;
typedef uint64_t phys_addr_t;

typedef struct {
    uint8_t ch;
    uint8_t dimm;
    uint8_t rank;
    uint8_t bank;
    uint16_t row;
    uint16_t col;
} dram_addr_t;

typedef dram_addr_t (*phys2dram_fn_t)(phys_addr_t); // physical address to DRAM address function pointer type

typedef struct {
    uint8_t num_ch;
    uint8_t num_dimm_per_ch;
    uint8_t num_rank_per_dimm;
    uint8_t bus_width; // in bits
    uint8_t chip_width; // in bits
    uint8_t burst;
    uint8_t num_chips;
} dram_info_t;

#endif // #ifndef __MEM_ERROR_COMMON_H__
/** @} */
