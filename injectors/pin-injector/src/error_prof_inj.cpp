// Copyright 2015, The University of Texas at Austin 
// All rights reserved.
// 
// THIS FILE IS PART OF THE HAMARTIA FAULT/ERROR ANALYSIS SUITE
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met: 
// 
// 1. Redistributions of source code must retain the above copyright
// notice, this list of conditions and the following disclaimer. 
// 
// 2. Redistributions in binary form must reproduce the above copyright
// notice, this list of conditions and the following disclaimer in the
// documentation and/or other materials provided with the distribution. 
// 
// 3. Neither the name of the copyright holder nor the names of its
// contributors may be used to endorse or promote products derived from
// this software without specific prior written permission. 
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
// FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
// COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
// BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
// ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

/**
 *  \file
 *  \brief A PIN tool for error profiling and injection
 *
 *  \version 1.0.0
 *  \author Michael Sullivan, University of Texas
 *  \author Nicholas Kelly, University of Texas
 *  \author Chun-Kai Chang, University of Texas
 *
 *  \addtogroup error_profiler_injector
 *  @{
 */

// Error API
#include <hamartia_api/debug.h>
#include <hamartia_api/utils/error.h>
#include <hamartia_api/error_manager.h>

// Includes
#include "error_prof_inj.h"
#include "utils.h"
#include "pin_cd.h"
#include "pin_mpi.h"
#include "profile.h"
#include "injector_factory.h"

/* =====================================================================
 * Interface
 * ===================================================================== */
/// Injection dynamic instruction number (instruction to inject at)
KNOB<UINT64> KnobInject(KNOB_MODE_APPEND, "pintool",
        "i", "0",
        "perform profiling (on 0) or inject at given Instruction (> 0); adding multiple flags will form a list of injection points");
/// Errors to record
//// Note that #inject can be different from #record
//// e.g., the user can overprovision injection points (with multiple -i flags) to increase the likelihood of successful error injection
KNOB<UINT64> KnobRecord(KNOB_MODE_WRITEONCE,  "pintool",
        "x", "1", "number of errors to record (i.e., successful error injection)");
/// Output filename
KNOB<string> KnobOutputFile(KNOB_MODE_WRITEONCE, "pintool",
        "o", "err_prof_inj.out",
        "specify the Output file name (for either injection or profiling)");
/// Instruction classes to inject at
KNOB<string> KnobOperationClasses(KNOB_MODE_WRITEONCE, "pintool",
        "c", "OPS_INT,OPS_FP", "specify the profiled/injected operation Class[es, comma delimited list]");
/// Image to inject into
KNOB<string> KnobImages(KNOB_MODE_WRITEONCE, "pintool",
        "l", "0", "specify the injected libraries/images (0 = all)");
/// Images to ignore
KNOB<string> KnobIgnoreImgs(KNOB_MODE_WRITEONCE,  "pintool",
        "n", "", "ignore images. Given as a comma delineated list.");
/// Use function regions (BEGIN_ERROR_INJECTION, END_ERROR_INJECTION)
KNOB<BOOL> KnobCodeRegion(KNOB_MODE_WRITEONCE, "pintool",
        "r", "0", "use code function regions");
/// Width of instruction to inject at
KNOB<UINT32> KnobWidth(KNOB_MODE_WRITEONCE, "pintool",
        "w", "0", "specify the width of instructions selected");
/// Error model
KNOB<string> KnobErrorModel(KNOB_MODE_WRITEONCE, "pintool",
        "e", "RANDBIT1", "specify the Error model");
/// Error config
KNOB<string> KnobErrorConfig(KNOB_MODE_WRITEONCE, "pintool",
        "q", "", "specify the Error config path");
/// Error point
KNOB<string > KnobErrorPoint(KNOB_MODE_WRITEONCE, "pintool",
        "p", "post", "specify the Error injection point ('pre', 'post', 'any')");
/// Detector model
KNOB<string> KnobDetectorModel(KNOB_MODE_WRITEONCE, "pintool",
        "d", "", "specify the Detector model");
/// Detector config
KNOB<string> KnobDetectorConfig(KNOB_MODE_WRITEONCE, "pintool",
        "a", "", "specify the Detector config path");

// TODO: Function
// KNOB<string> KnobFunction(KNOB_MODE_WRITEONCE, "pintool",
//         "f", "0", "function name");
// TODO: Line number
// KNOB<UINT64> KnobLine(KNOB_MODE_WRITEONCE, "pintool",
//         "l", "0", "line number (need to be compiled with -g)");


/**
 * Print tool usage
 */
INT32 Usage()
{
    cerr << "This tool injects errors into a running program." << endl;
    cerr << endl << KNOB_BASE::StringKnobSummary() << endl;
    return -1;
}

/* =====================================================================
 * ERROR INJECTION GLOBAL VARIABLES
 * ===================================================================== */

/* Instruction Couting */
/// Target instruction count
UINT64 ticount = 0;
/// Running instruction count
UINT64 ricount = 0;
/// List of target instructions (for multiple injection)
std::vector<UINT64> tilist;
UINT32 tilist_idx = 0;

/* Injection Switches */
/// Set to disable instrumentation
BOOL detach_switch = false;
/// Inject code regions
BOOL inject_regions;
/// Inject toggle
BOOL inject_toggle;

/* Injection Target Filters */
/// Target Images
std::vector<UINT32> images;
/// Ignored images
std::vector<string> ignore_imgs;
/// Operation classes
std::vector<std::string> oclasses;
/// Target SIMD lane
UINT32 op_tgt_index;
/// Target instruction with this operand width (in bits)
UINT32 inject_width;

/* Injector */
PinInjector * injector;

/* Files */
ofstream out_file;

/* Statistics */
/// Time stamps in the injection process
struct timeval pin_start, pin_inj, pin_end;

/* Conduits between instrumentation and analysis routines */
/// Operands for potential/filtered injection points
// We use this map to record operand because we cannot pass INS to analysis routines
std::unordered_map<ADDRINT, hamartia_api::InstructionData> g_inst_data_map;
/// Memory write address of instruction
ADDRINT write_ea;

/* Fast communication between analysis routines */
/// Pin scratch register (FIXME: unused currently)
static REG ScratchReg;


/* =====================================================================
 * Common Instrumentation and Analysis Routines
 * ===================================================================== */
// COUNTING FUNCTIONS
/**
 * This function is called before every (matched) instruction (for counting)
 */
ADDRINT insif_incr()
{
    return ++ricount >= ticount;
}

/**
 * This function is called after every (matched) instruction
 */
ADDRINT insif()
{
    return (ricount >= ticount);
}

/**
 * Insert an analysis routine with arguments matching the instruction's operands
 *
 * \param ins Instruction
 * \param mem_r Number of memory read operands
 * \param mem_w Number of memory write operands
 * \param r_operands read operands registers
 * \param w_operands write operands registers
 * \param func function pointer to analysis routine
 */
VOID InsertThenTemplate(INS& ins, const uint32_t& mem_r, const uint32_t& mem_w, REG* r_operands, REG* w_operands, AFUNPTR func)
{
            // Pass the appropriate parameters based on the number of read and write operands
            if (mem_r == 0 && mem_w == 0) {
                // No memory operands
                INS_InsertThenCall(ins, IPOINT_BEFORE, func, \
                                        IARG_UINT32, INS_Opcode(ins), \
                                        IARG_INST_PTR, \
                                        IARG_REG_REFERENCE, r_operands[0], \
                                        IARG_REG_REFERENCE, r_operands[1], \
                                        IARG_REG_REFERENCE, r_operands[2], \
                                        IARG_REG_CONST_REFERENCE, w_operands[0], \
                                        IARG_REG_CONST_REFERENCE, w_operands[1], \
                                        IARG_REG_CONST_REFERENCE, w_operands[2], \
                                        IARG_REG_VALUE, REG_RFLAGS, \
                                        IARG_ADDRINT, (ADDRINT) 0, \
                                        IARG_ADDRINT, (ADDRINT) 0, \
                                        IARG_ADDRINT, (ADDRINT) 0, \
                                        IARG_END);
            }
            else if (mem_r == 1 && mem_w == 0) {
                // 1 memory read operand
                INS_InsertThenCall(ins, IPOINT_BEFORE, func, \
                                        IARG_UINT32, INS_Opcode(ins), \
                                        IARG_INST_PTR, \
                                        IARG_REG_REFERENCE, r_operands[0], \
                                        IARG_REG_REFERENCE, r_operands[1], \
                                        IARG_REG_REFERENCE, r_operands[2], \
                                        IARG_REG_CONST_REFERENCE, w_operands[0], \
                                        IARG_REG_CONST_REFERENCE, w_operands[1], \
                                        IARG_REG_CONST_REFERENCE, w_operands[2], \
                                        IARG_REG_VALUE, REG_RFLAGS, \
                                        IARG_ADDRINT, (ADDRINT) 0, \
                                        IARG_MEMORYREAD_EA, \
                                        IARG_ADDRINT, (ADDRINT) 0, \
                                        IARG_END);
            }
            else if (mem_r == 2 && mem_w == 0) {
                // 2 memory read operands
                INS_InsertThenCall(ins, IPOINT_BEFORE, func, \
                                        IARG_UINT32, INS_Opcode(ins), \
                                        IARG_INST_PTR, \
                                        IARG_REG_REFERENCE, r_operands[0], \
                                        IARG_REG_REFERENCE, r_operands[1], \
                                        IARG_REG_REFERENCE, r_operands[2], \
                                        IARG_REG_CONST_REFERENCE, w_operands[0], \
                                        IARG_REG_CONST_REFERENCE, w_operands[1], \
                                        IARG_REG_CONST_REFERENCE, w_operands[2], \
                                        IARG_REG_VALUE, REG_RFLAGS, \
                                        IARG_ADDRINT, (ADDRINT) 0, \
                                        IARG_MEMORYREAD_EA, \
                                        IARG_MEMORYREAD2_EA, \
                                        IARG_END);      
            }
            if (mem_r == 0 && mem_w > 0) {
                // No memory read, 1 memory write operands
                INS_InsertThenCall(ins, IPOINT_BEFORE, func, \
                                        IARG_UINT32, INS_Opcode(ins), \
                                        IARG_INST_PTR, \
                                        IARG_REG_REFERENCE, r_operands[0], \
                                        IARG_REG_REFERENCE, r_operands[1], \
                                        IARG_REG_REFERENCE, r_operands[2], \
                                        IARG_REG_CONST_REFERENCE, w_operands[0], \
                                        IARG_REG_CONST_REFERENCE, w_operands[1], \
                                        IARG_REG_CONST_REFERENCE, w_operands[2], \
                                        IARG_REG_VALUE, REG_RFLAGS, \
                                        IARG_MEMORYWRITE_EA, \
                                        IARG_ADDRINT, (ADDRINT) 0, \
                                        IARG_ADDRINT, (ADDRINT) 0, \
                                        IARG_END);
            }
            else if (mem_r == 1 && mem_w > 0) {
                // 1 memory read, 1 memory write operands
                INS_InsertThenCall(ins, IPOINT_BEFORE, func, \
                                        IARG_UINT32, INS_Opcode(ins), \
                                        IARG_INST_PTR, \
                                        IARG_REG_REFERENCE, r_operands[0], \
                                        IARG_REG_REFERENCE, r_operands[1], \
                                        IARG_REG_REFERENCE, r_operands[2], \
                                        IARG_REG_CONST_REFERENCE, w_operands[0], \
                                        IARG_REG_CONST_REFERENCE, w_operands[1], \
                                        IARG_REG_CONST_REFERENCE, w_operands[2], \
                                        IARG_REG_VALUE, REG_RFLAGS, \
                                        IARG_MEMORYWRITE_EA, \
                                        IARG_MEMORYREAD_EA, \
                                        IARG_ADDRINT, (ADDRINT) 0, \
                                        IARG_END);
            }
            else if (mem_r == 2 && mem_w > 0) {
                // 2 memory read, 1 memory write operands
                INS_InsertThenCall(ins, IPOINT_BEFORE, func, \
                                        IARG_UINT32, INS_Opcode(ins), \
                                        IARG_INST_PTR, \
                                        IARG_REG_REFERENCE, r_operands[0], \
                                        IARG_REG_REFERENCE, r_operands[1], \
                                        IARG_REG_REFERENCE, r_operands[2], \
                                        IARG_REG_CONST_REFERENCE, w_operands[0], \
                                        IARG_REG_CONST_REFERENCE, w_operands[1], \
                                        IARG_REG_CONST_REFERENCE, w_operands[2], \
                                        IARG_REG_VALUE, REG_RFLAGS, \
                                        IARG_MEMORYWRITE_EA, \
                                        IARG_MEMORYREAD_EA, \
                                        IARG_MEMORYREAD2_EA, \
                                        IARG_END);
            }
}    

/* =====================================================================
 * Common Injector Setup and Teardown Code
 * ===================================================================== */
/** 
 * Initializing the injector
 */
VOID InjectorInit() 
{
    // Setup models
    INFO_PRINT("Initialize (C++) error manager...")
    hamartia_api::error_manager::Init(true);
    
    // Create injector
    injector = InjectorFactory::Create(KnobErrorModel.Value(), KnobErrorConfig.Value(), KnobDetectorModel.Value(), KnobDetectorConfig.Value());

    // Configure injector with CLI args
    uint64_t numToInject = 1;
    if (tilist.size() == 0) {
        INFO_PRINT("Profiling mode...")
        numToInject = 0;
    } else if (tilist.size() == 1 && KnobRecord.Value() == 1) {
        INFO_PRINT("Single transient error injection...")
        numToInject = 1;
    } else if (tilist.size() > 1 && KnobRecord.Value() == 1) {
        INFO_PRINT("Overprovision injection points for single transient error...")
        numToInject = 1;
        // remove points that are 0's (due to auto launcher's implementations)
        tilist.erase(std::remove(tilist.begin(), tilist.end(), 0), tilist.end());
        // sort injection points and reset first injection point
        std::sort(tilist.begin(), tilist.end());
        ticount = tilist[0];
    } else if (tilist.size() == KnobRecord.Value() && KnobRecord.Value() > 1) {
        INFO_PRINT("Multiple error injection per run...")
        numToInject = tilist.size();
    } else if (tilist.size() != KnobRecord.Value() && KnobRecord.Value() > 1) {
        INFO_PRINT("Overprovision injection points for multiple errors...")
        numToInject = KnobRecord.Value();
        tilist.erase(std::remove(tilist.begin(), tilist.end(), 0), tilist.end());
        std::sort(tilist.begin(), tilist.end());
        ticount = tilist[0];
    } else {
        FATAL_ERROR("Invalid injection and record count: " << tilist.size() << ", " << KnobRecord.Value());
    }    

    std::string errorPoint = KnobErrorPoint.Value();
    if (errorPoint != "pre" && errorPoint != "post" && errorPoint != "any") {
        FATAL_ERROR("Invalid error point: " << errorPoint);
    }
    injector->Configure(numToInject, errorPoint);
}

/**
 * Emit injection log (called directly before detaching)
 */
VOID InjectFini()
{
    // Write to a file since cout and cerr maybe closed by the application
    out_file.setf(ios::showbase);
    injector->SaveLog(out_file);
    CommonFini();
}

/**
 * Cleanup data
 */
VOID CommonFini()
{
    INFO_PRINT("CommonFini...")
    // Cleanup injector
    delete injector;
    gettimeofday(&pin_end, NULL);
    INFO_PRINT("PIN Time Log:")
    INFO_PRINT("Start: " << (UINT64)(pin_start.tv_sec*1000000 + pin_start.tv_usec))
    INFO_PRINT("Inject: " << (UINT64)(pin_inj.tv_sec*1000000 + pin_inj.tv_usec))
    INFO_PRINT("End: " << (UINT64)(pin_end.tv_sec*1000000 + pin_end.tv_usec))
}

/**
 * Exception Handler( guaranteed error logging even if errors result in crash)
 */
static VOID OnException(THREADID threadIndex,
                        CONTEXT_CHANGE_REASON reason,
                        const CONTEXT *ctxtFrom,
                        CONTEXT *ctxtTo,
                        INT32 info,
                        VOID *v)
{
    if(reason == CONTEXT_CHANGE_REASON_FATALSIGNAL && injector != NULL){
        INFO_PRINT("Caught Fatal Signal...")
        InjectFini();
    }
}

/* =====================================================================
 * Main
 * ===================================================================== */

/**
 * Program main entry point
 */
int main(int argc, CHAR *argv[])
{
    // Read command line arguments and initialize Pin 
    if(PIN_Init(argc,argv)) {
        return Usage(); // catch illegal command line arguments
    }
    PIN_InitSymbols();
    // Get scratch regs for fast communication between analysis routines
    ScratchReg = PIN_ClaimToolRegister();
    if (!REG_valid(ScratchReg))
    {
        std::cerr << "Cannot allocate a scratch register.\n";
        std::cerr << std::flush;
        return 1;
    }

    // Parse CLI arguments 
    // TODO: input checking and error messages? 

    // Output file
    out_file.open(KnobOutputFile.Value().c_str());

    // Injection point
    for(unsigned i=0; i<KnobInject.NumberOfValues(); i++)
        tilist.push_back(KnobInject.Value(i));
    
    ticount = (KnobInject.NumberOfValues() > 0) ? tilist[0] : 0;

    // Operation class
    oclasses = utils::StringSplit(KnobOperationClasses.Value());   

    // Injection width
    inject_width = KnobWidth.Value();

    // Injection code regions
    inject_regions = KnobCodeRegion.Value();
    inject_toggle = false;
    
    // Images
    std::vector<std::string> image_strs = utils::StringSplit(KnobImages.Value());
    for (std::vector<std::string>::iterator is = image_strs.begin(); is != image_strs.end(); ++is) {
        images.push_back(atoi(is->c_str()));
    }
    // Images to ignore
    ignore_imgs = utils::StringSplit(KnobIgnoreImgs.Value());
    
    is_mpi = KnobIsMpi.Value();
    if (is_mpi) {
        MPIInit();
        // Compare this MPI rank id to user specified rank id
        // Detach if they mismatch; otherwise, initialize injector
        IMG_AddInstrumentFunction(GetRankId, 0);
    } 
    else {//serial application
        InjectorInit();
    }

    is_cd = KnobIsCD.Value();
    // Start profiling or injection
    if (ticount == 0) {
        // Profile 
        INFO_PRINT("Profiling Mode...")
        TRACE_AddInstrumentFunction(ProfileTrace, 0);
        PIN_AddFiniFunction(ProfileFini, 0);

    } else { 
        // Inject 
        INFO_PRINT("Injection Mode...")

        if(is_cd){
            CDInit();
            //INFO_PRINT("CD Fastforwarding...")
            TRACE_AddInstrumentFunction(TraceCDFSM, 0);
            // Injection Pintool
            INS_AddInstrumentFunction(CDInjectInstruction, 0);
        }
        else{ 
            //if (!disable_ff) {
            //    VOID* vticount = &ticount;
            //    TRACE_AddInstrumentFunction(TraceFF, vticount);
            //}

            // Register the main Pin instrumentation routine
            InjectorFactory::RegisterInstrument(KnobErrorModel.Value());
            // Register routine that executes when application finishes
            InjectorFactory::RegisterFini(KnobErrorModel.Value());
        }

        // Fini function is called directly before detaching
    }

    // Exception handler in case of lost error logging due to crash 
    PIN_AddContextChangeFunction(OnException, 0);

    //Log start time for serial program
    //MPI tasks log start time after knowing its rank id
    if(!is_mpi)
        gettimeofday(&pin_start, NULL);

    PIN_StartProgram();

    return 0;
}
/** @} */
