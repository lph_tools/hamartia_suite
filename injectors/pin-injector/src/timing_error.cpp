// Copyright 2015, The University of Texas at Austin 
// All rights reserved.
// 
// THIS FILE IS PART OF THE HAMARTIA FAULT/ERROR ANALYSIS SUITE
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met: 
// 
// 1. Redistributions of source code must retain the above copyright
// notice, this list of conditions and the following disclaimer. 
// 
// 2. Redistributions in binary form must reproduce the above copyright
// notice, this list of conditions and the following disclaimer in the
// documentation and/or other materials provided with the distribution. 
// 
// 3. Neither the name of the copyright holder nor the names of its
// contributors may be used to endorse or promote products derived from
// this software without specific prior written permission. 
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
// FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
// COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
// BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
// ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

/**
 *  \file
 *  \brief Implementation of timing-error injection
 *
 *  \version 1.1.0
 *  \author Chun-Kai Chang, University of Texas
 *
 *  \addtogroup error_profiler_injector
 *  @{
 */

#include <hamartia_api/error_manager.h>
#include <hamartia_api/utils/error.h>
#include <hamartia_api/types.h>

#include "timing_error.h"
#include "timing_error_logging.h"
#include "error_prof_inj.h"
#include "code_filter.h"
#include "oclasses.h"
#include "helpers/instruction.h"
#include "helpers/operand.h"
#include "utils.h"

/* =====================================================================
 * Global Variables
 * ===================================================================== */
static bool is_same_unit = false;

/* =====================================================================
 * Pin Routines
 * ===================================================================== */
static VOID pre_then_timing(UINT32 opcode, ADDRINT addr, 
                     PIN_REGISTER *r1, PIN_REGISTER *r2, PIN_REGISTER *r3, 
                     const PIN_REGISTER *w1, const PIN_REGISTER *w2, const PIN_REGISTER *w3,
                     ADDRINT rflags, ADDRINT mwrite_ea, ADDRINT mread_ea1, ADDRINT mread_ea2)
{
    if(detach_switch) return;
    INFO_PRINT("Pre-Inst routine...")

    // Disable instrumentation if we are done
    if (injector == nullptr) {
        //INFO_PRINT("Detach!")
        //PIN_Detach(); /*Ideally we should detach, but this API results in SDC*/
        INFO_PRINT("Disable Instrumentation...")
        detach_switch = true;
        //Jit new instrumentation code
        PIN_RemoveInstrumentation();
        return;
    }

    hamartia_api::InstructionData &inst_data = g_inst_data_map[addr];

    PIN_REGISTER * _r_operands[MAX_R_OPERANDS] = {r1, r2, r3}; 
    const PIN_REGISTER * _w_operands[MAX_W_OPERANDS] = {w1, w2, w3}; 
    ADDRINT _addrs[2] = {mread_ea1, mread_ea2};

    if (injector->Phase() == hamartia_api::PHASE_GET_PREV) {
        // Update inst_data from Pin state
        injector->PreCurrentStateToContext(inst_data, const_cast<const PIN_REGISTER**>(_r_operands), \
                                           _addrs, _w_operands, mwrite_ea);
    } else if (injector->Phase() == hamartia_api::PHASE_EVAL) {
        //use static_cast because the injector type is known
        TimingErrorInjector* timing_injector = static_cast<TimingErrorInjector*>(injector); 
        // test if this instruction uses the same execution unit as previous context
        if (timing_injector->isSameUnitAsPrev(inst_data)) {
            is_same_unit = true; // cache for *then_timing* checking
            // Update inst_data from Pin state
            timing_injector->PreCurrentStateToContext(inst_data, const_cast<const PIN_REGISTER**>(_r_operands), \
                                                      _addrs, _w_operands, mwrite_ea);
        } else {
            is_same_unit = false;
        }    
    }    
    // Register the write EA
    write_ea = mwrite_ea;
}

static void try_next_point(bool log=false)
{
    TimingErrorInjector* timing_injector = static_cast<TimingErrorInjector*>(injector);
    // Proceed to next injection point (if exists)
    if(++tilist_idx >= tilist.size()) {
        INFO_PRINT("No more injection points for this run...")
        INFO_PRINT("Early Exit...")
        if(log) {
            timing_injector->SaveMaskedLog();
        }
        CommonFini();
            
        injector = nullptr;
        std::exit(0);
    } else {
        INFO_PRINT("Try next injection point...")
        ticount = tilist[tilist_idx];
        timing_injector->resetPhase();
        timing_injector->resetInjectionTrigger();
        PIN_RemoveInstrumentation();
    }    
}

static void test_more_injection(hamartia_api::InstructionData &inst_data)
{
    TimingErrorInjector* timing_injector = static_cast<TimingErrorInjector*>(injector);
    if (timing_injector->reachNumToInject()) {
        TimingErrorLog::IncNextPoint(); 
        if (timing_injector->any_success) {
            INFO_PRINT("Finish!")
            timing_injector->SaveMaskedLog();
            InjectFini();
            injector = nullptr;
        } else { // all masked
            timing_injector->clearNumInjected();
            timing_injector->any_success = false;
            timing_injector->clearErrorLog();
            try_next_point(true);
        }    
    } else {
        INFO_PRINT("More injection...")
        // Case 1: an error that affect multiple instructions temporally close
        // e.g., droops that persist multiple cycles or affect pipelined circuits with multiple instr. inside
        timing_injector->setPrevInstData(inst_data);
        timing_injector->setPhaseEval();
        // Case 2 (no support yet): multiple timing errors
    }  
}

static VOID then_timing(CONTEXT * state, UINT32 opcode, ADDRINT addr,
                 PIN_REGISTER *w1, PIN_REGISTER *w2, PIN_REGISTER *w3, ADDRINT &rflags)
{
    if(detach_switch) return;
    // All methods should be inlined!
    INFO_PRINT("Post-Inst routine...")

    if (injector == nullptr) return;

    // Find proper sub-word to target, for packed instructions
    hamartia_api::InstructionData &inst_data = g_inst_data_map[addr];

    // Format function values
    PIN_REGISTER * _w_operands[MAX_W_OPERANDS] = {w1, w2, w3};

    TimingErrorInjector* timing_injector = static_cast<TimingErrorInjector*>(injector);
         
    if (injector->Phase() == hamartia_api::PHASE_GET_PREV) {
        // Populate *previous* context from instruction
        timing_injector->PostCurrentStateToPrevContext(inst_data, const_cast<const PIN_REGISTER**>(_w_operands), \
                                                       write_ea, rflags);
        // Injector phase is PHASE_EVAL now
            
    } else if (injector->Phase() == hamartia_api::PHASE_EVAL) {
        if (is_same_unit) {
            // Optimization: 
            // if we know inputs are the same (i.e., no input transitions), then no timing errors
            // proceed to next pair if injection points are overprovisioned
            // TODO: figure out how common this is 
            if(timing_injector->isDiffInputFromPrev(inst_data)) {
                // Populate *current* context from instruction
                injector->PostCurrentStateToContext(inst_data, const_cast<const PIN_REGISTER**>(_w_operands), \
                                                    write_ea, rflags);
                // Injector phase is PHASE_INJECT now
                injector->RollbackState(state);
                PIN_ExecuteAt(state);
            } else {
                INFO_PRINT("Encounter a same-value pair");
                TimingErrorLog::IncNumSameValPair(); 
                uint64_t num_to_inject = timing_injector->getNumToInject();
                if (num_to_inject == 1) {
                    TimingErrorLog::IncNextPoint(); 
                    try_next_point(false);
                } else if (num_to_inject > 1) {
                    timing_injector->incNumInjected();
                    test_more_injection(inst_data);
                }    
            }    
        }
    } else if (injector->Phase() == hamartia_api::PHASE_INJECT) {
        //FIXME: Is this the correct injection start time?
        gettimeofday(&pin_inj, NULL);
        // Inject error
        bool success = timing_injector->InjectError();
        INFO_PRINT("INJ " << success << " at filtered instr # "<< ticount)
        uint64_t num_to_inject = timing_injector->getNumToInject();

        if (num_to_inject == 1) {
            TimingErrorLog::IncNextPoint(); 

            if (success) {
                // Rollback state
                injector->OldOutputContextToState(state);
                // Update to injection state
                injector->InjectionContextToState(state, rflags);
                // Commit context
                injector->CommitContext();
    
                if (injector->Phase() == hamartia_api::PHASE_INJECTED) {
                    // Write out logs
                    INFO_PRINT("Finish!")
                    timing_injector->SaveMaskedLog();
                    InjectFini();
                    injector = nullptr;
                } 
                INFO_PRINT("Return")
                PIN_ExecuteAt(state);
            } else { 
                // Masked at this injection point
                timing_injector->clearErrorLog();
                try_next_point(true);
            }
        } else if (num_to_inject > 1) {
            
            if (success) {
                timing_injector->any_success = true;
                // Commit context
                injector->OldOutputContextToState(state);
                injector->InjectionContextToState(state, rflags);
                injector->CommitContext(); // num_error_injected is incremented inside
            } else {
                timing_injector->incNumInjected();
            }    

            test_more_injection(inst_data);

            INFO_PRINT("Return")
            PIN_ExecuteAt(state);
        } else {
            FATAL_ERROR("Reach undefined state!")
        }    
    }
}

VOID TimingErrorInstruction(INS ins, VOID *v)
{
    //Disable instrumentation if true
    if (detach_switch) {
        return;
    }
    // Check if MPI rank id is known
    if (is_mpi && !disable_getrank) {
        return;
    }
    // Find resident image (and ignore if in ignore_imgs) 
    if (isInIgnoredImg(ins, ignore_imgs, images)) {
        return;
    }    
    // Modify inject_toggle when we see code region headers
    if (inject_regions && isCodeRegionHeader(ins, inject_toggle)) {
        return;
    }    

    if (inject_toggle || !inject_regions) {
        // Filter out the operations from the desired classes 
        if (InsInOperationClasses(ins, oclasses, inject_width) && INS_HasFallThrough(ins)) {
            // Index
            uint32_t mem_r = 0, mem_w = 0;
            // Registers
            REG r_operands[MAX_R_OPERANDS] = {REG_INST_G0, REG_INST_G0, REG_INST_G0};
            REG w_operands[MAX_W_OPERANDS] = {REG_INST_G0, REG_INST_G0, REG_INST_G0};
            // Extract information for error injection
            pin_instruction_helper::GetInstOperandInfo(ins, g_inst_data_map, mem_r, mem_w, r_operands, w_operands);

            // Insert a call to register the instruction
            INS_InsertIfCall(ins, IPOINT_BEFORE, (AFUNPTR)insif_incr, IARG_END);

            InsertThenTemplate(ins, mem_r, mem_w, r_operands, w_operands, (AFUNPTR) pre_then_timing);

            // Insert a call to check if we reach target instruction
            INS_InsertIfCall(ins, IPOINT_AFTER, (AFUNPTR)insif, IARG_END);

            // Inject an error
            INS_InsertThenCall(ins, IPOINT_AFTER, (AFUNPTR)then_timing, 
                                    IARG_CONTEXT, \
                                    IARG_UINT32, INS_Opcode(ins), \
                                    IARG_INST_PTR, \
                                    IARG_REG_REFERENCE, w_operands[0], \
                                    IARG_REG_REFERENCE, w_operands[1], \
                                    IARG_REG_REFERENCE, w_operands[2], \
                                    IARG_REG_REFERENCE, REG_RFLAGS, \
                                    IARG_END);
        }
    }
}    

/* =====================================================================
 * Timing Error Specific Functions
 * ===================================================================== */
void TimingErrorInjector::Configure(uint64_t num_to_inject, const std::string& error_point)
{
    SetNumToRecord(num_to_inject);
    SetNumToInject(num_to_inject);

    if (error_point == "pre") {
        WARN_PRINT("Input injection is not valid for timing error injector");
    }    
    INFO_PRINT("Register Output injection")
    AcceptErrorInject();
}

void TimingErrorInjector::PostCurrentStateToPrevContext(hamartia_api::InstructionData &inst_data, const PIN_REGISTER *w_registers[], ADDRINT w_addr, ADDRINT rflags) 
{
    INFO_PRINT("Save Post state to previous context...")
    // Get (correct) destination values
    ADDRINT _addrs[1] = {w_addr};
    
    // Save error-free output values to both values and old values
    /// Originally, old values are used to reset pre-injection, but pre-injection is INVALID for timing-error injector.
    /// Thus, we overload "old values" to represent the error-free values to support multi-cycle timing errors
    /// (i.e., previous context may also be corrupted)
    for (uint32_t i = 0, l = inst_data.outputs.size(), io_w = 0, mem_w = 0; i < l; ++i) { // update current output values
        pin_operand_helper::UpdateOperandFromPin(&inst_data.outputs[i], inst_data.simd_width, io_w, mem_w, w_registers, _addrs);
    }
    for (uint32_t i = 0, l = inst_data.outputs.size(), io_w = 0, mem_w = 0; i < l; ++i) { // update old output values
        pin_operand_helper::UpdateOperandFromPin(&inst_data.outputs[i], inst_data.simd_width, io_w, mem_w, w_registers, _addrs, true);
    }
    
    // Set Instruction data
    current_context.prev_instruction_data = inst_data;

    prev_dp = inst_data.data_type;
    prev_width = inst_data.width;
    hamartia_api::Operation op = inst_data.instruction.operation;
    if (op2exu_map.find(op) != op2exu_map.end()) {
       prev_exu_id = op2exu_map[op];
    } else {
        FATAL_ERROR("Operation type " << hamartia_api::Operation_Name(op) << " is not supported ")
    }
    // Next phase
    phase = hamartia_api::PHASE_EVAL;
    INFO_PRINT("=====Enter PHASE_EVAL=====")
}    

TimingErrorInjector::op2exu_type TimingErrorInjector::InitExuMap()
{
    using namespace hamartia_api;
    op2exu_type op2exu_map;
    // FIXME: only arithmetic and logic operations
    op2exu_map[OP_ADD] = EXU_ADD;
    op2exu_map[OP_SUB] = EXU_ADD;
    op2exu_map[OP_MUL] = EXU_MUL;
    op2exu_map[OP_DIV] = EXU_DIV;
    op2exu_map[OP_SQRT] = EXU_SQRT;
    op2exu_map[OP_SAR] = EXU_SHF;
    op2exu_map[OP_SALC] = EXU_SHF;
    op2exu_map[OP_SHR] = EXU_SHF;
    op2exu_map[OP_SHL] = EXU_SHF;
    op2exu_map[OP_AND] = EXU_LOGIC;
    op2exu_map[OP_OR] = EXU_LOGIC;
    op2exu_map[OP_XOR] = EXU_LOGIC;
    op2exu_map[OP_NOT] = EXU_LOGIC;
    return op2exu_map;
}

bool TimingErrorInjector::isSameUnitAsPrev(hamartia_api::InstructionData &inst_data)
{
    if (inst_data.data_type != prev_dp || inst_data.width != prev_width) {
        return false;
    }

    hamartia_api::Operation op = inst_data.instruction.operation;
    if (op2exu_map.find(op) != op2exu_map.end()) {
        return prev_exu_id == op2exu_map[op];
    } else {
        return false;
    }

    return true;
}    

bool TimingErrorInjector::isDiffInputFromPrev(hamartia_api::InstructionData &inst_data)
{
    hamartia_api::InstructionData &prev_inst_data = current_context.prev_instruction_data;
    
    // We only checked if the instruction pair uses the same execution unit
    // Here we further check if they also are of the same operation
    // e.g., SUB 1, 1 is different from ADD 1, 1 , though using the same unit
    if (inst_data.instruction.operation != prev_inst_data.instruction.operation) 
        return true;
    
    int min_num_inputs = (inst_data.inputs.size() < prev_inst_data.inputs.size()) ? 
                            inst_data.inputs.size() : prev_inst_data.inputs.size();
    for (int i = 0; i < min_num_inputs; ++i) {
        // FIXME: check other SIMD lanes as well?
        if (inst_data.inputs[i].value[0].uint() != prev_inst_data.inputs[i].value[0].uint())
            return true;
    }
    return false;    
}

void TimingErrorInjector::SaveMaskedLog()
{
    TimingErrorLog::Emit(log_file); 
} 

static bool isProbabilisticModel(const std::string& emodel_str)
{
    // these models don't loop at certain injection point
    if (utils::stringStartsWith(emodel_str, "TIMING_PREV") || 
        utils::stringStartsWith(emodel_str, "TIMING_LUT") || 
        utils::stringStartsWith(emodel_str, "TIMING_RTL")) {
        INFO_PRINT("Iterative injection loop disabled ...")
        return false;
    }

    return true;
}

static uint64_t getMaxIteration()
{
    // FIXME: each timing error models should have its own max iter?
    return 150;
}

bool TimingErrorInjector::InjectError(bool auto_mode)
{
    bool unmasked = false;
    bool undetected = false;
    bool success = true;
    bool loop = isProbabilisticModel(current_context.error_info.error_model);

    uint64_t max_iter = getMaxIteration();
    do {
        // Invoke error injection
        success = hamartia_api::error_manager::InjectError(&injection_context, &log);
        hamartia_api::Resp emodel_response = injection_context.error_info.response;

        if (success && emodel_response == hamartia_api::RESP_SUCCESS) {
            // Check whether error is masked
            success = hamartia_api::error_manager::CheckError(&current_context, &injection_context, unmasked, undetected, &log);
            if (!success) {
                INFO_PRINT("Injected error masked by instruction, continue...")
            }
        } else { // this terminates the program!
            HandleResponse(emodel_response, injection_context.error_info.response_message); 
        }

        // Update statistics
        hamartia_api::InjectionStats& stats = hamartia_api::ErrorContext::CurrentStats(&injection_context);
        stats.iterations++;
        stats.updateDetection(unmasked, undetected);
        if (stats.iterations > max_iter) {
            INFO_PRINT("Reach max iterations...")
            INFO_PRINT("Dump error log...")
            DumpAndExit(); // never return
        }

    } while (loop);

    current_context.trigger.cont = injection_context.trigger.cont = false;

    if (success) {
        log.LogInjectedValues(injection_context, num_errors_recorded);
        ++num_errors_recorded;

        phase = hamartia_api::PHASE_INJECTED;

        return true;  
    } else {
        TimingErrorLog::AddToLog(injection_context, log);

        return false;
    }    
}    

void TimingErrorInjector::DumpAndExit()
{
    // Early exit
    log.LogInjectedValues(injection_context, num_errors_recorded);
    out_file.setf(ios::showbase); //out_file is a global variable defined in error_prof_inj.cpp
    SaveLog(out_file);
    delete this;
    std::exit(0);
}

void TimingErrorInjector::HandleResponse(const hamartia_api::Resp& emodel_response, const std::string& response_message)
{
    switch (emodel_response) {
        case hamartia_api::RESP_INVALID_ERROR: 
            INFO_PRINT("Masked Error: " << response_message);
            DumpAndExit();
            break;
        case hamartia_api::RESP_NO_MODEL: 
        case hamartia_api::RESP_ERROR:
        default:    
            FATAL_ERROR("Error Model Error: " << response_message);
    }
}    

   
