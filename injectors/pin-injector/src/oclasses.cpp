// Copyright 2015, The University of Texas at Austin 
// All rights reserved.
// 
// THIS FILE IS PART OF THE HAMARTIA FAULT/ERROR ANALYSIS SUITE
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met: 
// 
// 1. Redistributions of source code must retain the above copyright
// notice, this list of conditions and the following disclaimer. 
// 
// 2. Redistributions in binary form must reproduce the above copyright
// notice, this list of conditions and the following disclaimer in the
// documentation and/or other materials provided with the distribution. 
// 
// 3. Neither the name of the copyright holder nor the names of its
// contributors may be used to endorse or promote products derived from
// this software without specific prior written permission. 
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
// FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
// COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
// BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
// ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

/**
 *  \file
 *  \brief Operation classes
 *
 *  \version 1.0.0
 *  \author Michael Sullivan, University of Texas
 *  \author Nicholas Kelly, University of Texas
 *
 *  \addtogroup operation_classes
 *  @{
 */

// LIBS
#include <string>
#include <vector>
#include <map>
#include <algorithm>

#include "oclasses.h"

// Define operation classes 
#define CLASS_IDENTIFIER "XED_ICLASS_"
#define CATEGORY_IDENTIFIER "XED_CATEGORY_"

// Initialize a std::vector of operation classes using a C-style array 
#define INITVEC(arr, vec) std::vector<OPCODE> vec (arr, \
                          arr + sizeof(arr) / sizeof(arr[0]));

/// Integer arithmetic transformations 
static const OPCODE ops_int[] = {XED_ICLASS_ADD,
                              XED_ICLASS_SUB,
                              XED_ICLASS_MUL,
                              XED_ICLASS_DIV,
                              XED_ICLASS_SAR,
                              XED_ICLASS_SALC,
                              XED_ICLASS_SHR,
                              XED_ICLASS_SHL
                             };
INITVEC(ops_int, ops_int_vec);   // creates ops_int_vec

/// Integer logical transformations 
static const OPCODE ops_int_logic[] = {XED_ICLASS_AND,
                                    XED_ICLASS_ANDN,
                                    XED_ICLASS_OR,
                                    XED_ICLASS_XOR,
                                    XED_ICLASS_NOT
                                   };
INITVEC(ops_int_logic, ops_int_logic_vec);   // creates ops_int_logic_vec

/// Floating-point arithmetic transformations 
static const OPCODE ops_fp[] = {
                             // SSE (128-bit)
                             XED_ICLASS_ADDPD,
                             XED_ICLASS_ADDPS,
                             XED_ICLASS_ADDSD,
                             XED_ICLASS_ADDSS,
                             XED_ICLASS_SUBPD,
                             XED_ICLASS_SUBPS,
                             XED_ICLASS_SUBSD,
                             XED_ICLASS_SUBSS,
                             XED_ICLASS_MULPD,
                             XED_ICLASS_MULPS,
                             XED_ICLASS_MULSD,
                             XED_ICLASS_MULSS,
                             XED_ICLASS_DIVPD,
                             XED_ICLASS_DIVPS,
                             XED_ICLASS_DIVSD,
                             XED_ICLASS_DIVSS,
                             XED_ICLASS_SQRTPD,
                             XED_ICLASS_SQRTPS,
                             XED_ICLASS_SQRTSD,
                             XED_ICLASS_SQRTSS,
                             // AVX (256-bit)
                             XED_ICLASS_VADDPD,
                             XED_ICLASS_VADDPS,
                             XED_ICLASS_VADDSD,
                             XED_ICLASS_VADDSS,
                             XED_ICLASS_VSUBPD,
                             XED_ICLASS_VSUBPS,
                             XED_ICLASS_VSUBSD,
                             XED_ICLASS_VSUBSS,
                             XED_ICLASS_VMULPD,
                             XED_ICLASS_VMULPS,
                             XED_ICLASS_VMULSD,
                             XED_ICLASS_VMULSS,
                             XED_ICLASS_VDIVPD,
                             XED_ICLASS_VDIVPS,
                             XED_ICLASS_VDIVSD,
                             XED_ICLASS_VDIVSS,
                             XED_ICLASS_VSQRTPD,
                             XED_ICLASS_VSQRTPS,
                             XED_ICLASS_VSQRTSD,
                             XED_ICLASS_VSQRTSS
                            };
INITVEC(ops_fp, ops_fp_vec);   // creates ops_fp_vec

/// Floating-point logical transformations (rarely used)
static const OPCODE ops_fp_logic[] = {
                                   XED_ICLASS_ANDPD,
                                   XED_ICLASS_ANDPS,
                                   XED_ICLASS_ANDNPD,
                                   XED_ICLASS_ANDNPS,
                                   XED_ICLASS_ORPD,
                                   XED_ICLASS_ORPS,
                                   XED_ICLASS_XORPD,
                                   XED_ICLASS_XORPS,
                                   XED_ICLASS_VANDPD,
                                   XED_ICLASS_VANDPS,
                                   XED_ICLASS_VANDNPD,
                                   XED_ICLASS_VANDNPS,
                                   XED_ICLASS_VORPD,
                                   XED_ICLASS_VORPS,
                                   XED_ICLASS_VXORPD,
                                   XED_ICLASS_VXORPS
                                  };
INITVEC(ops_fp_logic, ops_fp_logic_vec);   // creates ops_fp_logic_vec

/// move operations
static const OPCODE ops_mov[] = {
                              //FIXME: add more, MUST also add to helpers/instruction.cpp
                              // integer
                              //XED_ICLASS_MOV, 
                              //XED_ICLASS_MOVD, // this usually uses sp and thus ignored
                              XED_ICLASS_MOVDQA,
                              XED_ICLASS_MOVDQU,
                              XED_ICLASS_VMOVDQA,
                              XED_ICLASS_VMOVDQU,
                              // floating point
                              XED_ICLASS_MOVSS,
                              XED_ICLASS_MOVSD,
                              XED_ICLASS_MOVAPS,
                              XED_ICLASS_MOVAPD,
                              XED_ICLASS_MOVHPD,
                              XED_ICLASS_MOVLPD,
                              XED_ICLASS_VMOVAPS,
                              XED_ICLASS_VMOVAPD 
                             };
INITVEC(ops_mov, ops_mov_vec);   // creates ops_mov_vec

/// Operations supported by the b-HiVE timing error model 
static const OPCODE ops_bhive[] = {XED_ICLASS_ADD,
                              XED_ICLASS_SUB,
                              XED_ICLASS_MUL,
                              XED_ICLASS_OR,
                              XED_ICLASS_AND,
                              XED_ICLASS_XOR,
                              XED_ICLASS_ADDPD,
                              XED_ICLASS_ADDPS,
                              XED_ICLASS_ADDSD,
                              XED_ICLASS_ADDSS,
                              XED_ICLASS_SUBPD,
                              XED_ICLASS_SUBPS,
                              XED_ICLASS_SUBSD,
                              XED_ICLASS_SUBSS,
                              XED_ICLASS_MULPD,
                              XED_ICLASS_MULPS,
                              XED_ICLASS_MULSD,
                              XED_ICLASS_MULSS,
                              XED_ICLASS_DIVPD,
                              XED_ICLASS_DIVPS,
                              XED_ICLASS_DIVSD,
                              XED_ICLASS_DIVSS,
                              XED_ICLASS_VADDPD,
                              XED_ICLASS_VADDPS,
                              XED_ICLASS_VADDSD,
                              XED_ICLASS_VADDSS,
                              XED_ICLASS_VSUBPD,
                              XED_ICLASS_VSUBPS,
                              XED_ICLASS_VSUBSD,
                              XED_ICLASS_VSUBSS,
                              XED_ICLASS_VMULPD,
                              XED_ICLASS_VMULPS,
                              XED_ICLASS_VMULSD,
                              XED_ICLASS_VMULSS,
                              XED_ICLASS_VDIVPD,
                              XED_ICLASS_VDIVPS,
                              XED_ICLASS_VDIVSD,
                              XED_ICLASS_VDIVSS
                             };
INITVEC(ops_bhive, ops_bhive_vec);   // creates ops_bive_vec

/// Operations supported by Hamartia's timing error model 
static const OPCODE ops_timing[] = {XED_ICLASS_ADD,
                              XED_ICLASS_SUB,
                              XED_ICLASS_MUL,
                              XED_ICLASS_ADDPD,
                              XED_ICLASS_ADDPS,
                              XED_ICLASS_ADDSD,
                              XED_ICLASS_ADDSS,
                              XED_ICLASS_SUBPD,
                              XED_ICLASS_SUBPS,
                              XED_ICLASS_SUBSD,
                              XED_ICLASS_SUBSS,
                              XED_ICLASS_MULPD,
                              XED_ICLASS_MULPS,
                              XED_ICLASS_MULSD,
                              XED_ICLASS_MULSS,
                              XED_ICLASS_DIVPD,
                              XED_ICLASS_DIVPS,
                              XED_ICLASS_DIVSD,
                              XED_ICLASS_DIVSS,
                              XED_ICLASS_VADDPD,
                              XED_ICLASS_VADDPS,
                              XED_ICLASS_VADDSD,
                              XED_ICLASS_VADDSS,
                              XED_ICLASS_VSUBPD,
                              XED_ICLASS_VSUBPS,
                              XED_ICLASS_VSUBSD,
                              XED_ICLASS_VSUBSS,
                              XED_ICLASS_VMULPD,
                              XED_ICLASS_VMULPS,
                              XED_ICLASS_VMULSD,
                              XED_ICLASS_VMULSS,
                              XED_ICLASS_VDIVPD,
                              XED_ICLASS_VDIVPS,
                              XED_ICLASS_VDIVSD,
                              XED_ICLASS_VDIVSS,
                             };
INITVEC(ops_timing, ops_timing_vec);   // creates ops_bive_vec


/* string -> class mapping */

/**
 * Create operation class mapping
 *
 * \return Operation class mapping
 */
std::map< std::string, std::vector<OPCODE> > create_operation_map()
{
    std::map< string, std::vector<OPCODE> > m;
    m[std::string("OPS_INT")] = ops_int_vec;
    m[std::string("OPS_INT_LOGIC")] = ops_int_logic_vec;
    m[std::string("OPS_FP")] = ops_fp_vec;
    m[std::string("OPS_FP_LOGIC")] = ops_fp_logic_vec;
    m[std::string("OPS_MOV")] = ops_mov_vec;
    m[std::string("OPS_BHIVE")] = ops_bhive_vec;
    m[std::string("OPS_TIMING")] = ops_timing_vec;
    return m;
}
/// Operation class mapping
static std::map< std::string, std::vector<OPCODE> > operation_map = create_operation_map();

bool InsInOperationClasses(const INS &ins, std::vector<std::string> &sel_classes, UINT32 width)
{
    for (std::vector<std::string>::iterator it_class = sel_classes.begin();
            it_class != sel_classes.end(); ++it_class) {

        std::size_t idx;
        // Check width
        if (width != 0 && INS_OperandCount(ins) > 0) {
            UINT32 ins_width = INS_OperandWidth(ins, DEST_OPERAND);
            if (ins_width != width) return false;
        }

        // Check instruction classes/opcodes/categories
        if ((idx = it_class->find(CATEGORY_IDENTIFIER)) == 0) {
            // XED CATEGORY
            return (INS_Category(ins) == str2xed_category_enum_t(it_class->c_str()+(idx+sizeof(CATEGORY_IDENTIFIER)-1)));
        } else if ((idx = it_class->find(CLASS_IDENTIFIER)) == 0) {
            // OPCODE
            return (INS_Opcode(ins) == str2xed_iclass_enum_t(it_class->c_str()+(idx+sizeof(CLASS_IDENTIFIER)-1)));
        } else if (operation_map.find(*it_class) != operation_map.end()) {
            // OCLASS
            std::vector<OPCODE> tgt_op_class = operation_map[*it_class];
            for (std::vector<OPCODE>::iterator it_op = tgt_op_class.begin();
                        it_op != tgt_op_class.end(); ++it_op) {
                if (INS_Opcode(ins) == *it_op) {
                    return true;
                }
            }
        }
    }
    return false;
}

