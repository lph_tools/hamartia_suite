// Copyright 2015, The University of Texas at Austin 
// All rights reserved.
// 
// THIS FILE IS PART OF THE HAMARTIA FAULT/ERROR ANALYSIS SUITE
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met: 
// 
// 1. Redistributions of source code must retain the above copyright
// notice, this list of conditions and the following disclaimer. 
// 
// 2. Redistributions in binary form must reproduce the above copyright
// notice, this list of conditions and the following disclaimer in the
// documentation and/or other materials provided with the distribution. 
// 
// 3. Neither the name of the copyright holder nor the names of its
// contributors may be used to endorse or promote products derived from
// this software without specific prior written permission. 
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
// FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
// COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
// BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
// ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

/**
 *  \file
 *  \brief Code-filtering implementation
 *
 *  \version 1.0.0
 *  \author Chun-Kai Chang, University of Texas
 *
 *  \addtogroup error_profiler_injector
 *  @{
 */

#include <algorithm>
#include <vector>
#include <string>

#include "code_filter.h"
#include "utils.h"
#include "code_regions.h"

static const BOOL IGNORED = true; // this instruction should be ignored
static const BOOL CONTINUE = false; // this instruction need further analysis

BOOL isInIgnoredImg(const INS& ins, const std::vector<string>& ignore_imgs, const std::vector<UINT32>& images)
{
    RTN rtn = INS_Rtn(ins);
    string img_name = (RTN_Valid(rtn) && SEC_Valid(RTN_Sec(rtn)) && IMG_Valid(SEC_Img(RTN_Sec(rtn)))) ? 
                       IMG_Name(SEC_Img(RTN_Sec(rtn))) : "INVALID";
    // check ignored images first
    if (binary_search(ignore_imgs.begin(), ignore_imgs.end(), utils::toUpper(img_name))) {
        return IGNORED;
    }
    // then check target images
    ADDRINT addr = INS_Address(ins);
    IMG img = IMG_FindByAddress(addr);
    if (IMG_Valid(img)) {
        bool good = false;
        UINT32 img_id = IMG_Id(img);
        for (std::vector<UINT32>::const_iterator img = images.begin(); img != images.end(); ++img) {
            if (*img == 0 || *img == img_id) {
                good = true;
                break;
            }
        }
        if (!good) {
            return IGNORED;
        }
    } else {
        return IGNORED;
    }

    return CONTINUE;
}

static const BOOL HEADER = true; 
BOOL isCodeRegionHeader(const INS& ins, BOOL& inject_toggle)
{
    RTN rtn = INS_Rtn(ins);
    if (RTN_Valid(rtn)) {
        std::string rtn_name = utils::DecodeRoutineName(RTN_Name(rtn));
        if (rtn_name == INJECT_CODE_REGION_BEGIN) {
            inject_toggle = true;
            return HEADER;
        } else if (rtn_name == INJECT_CODE_REGION_END) {
            inject_toggle = false;
            return HEADER;
        }
    }
    return !HEADER;
}
