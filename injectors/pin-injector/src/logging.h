// Copyright 2015, The University of Texas at Austin 
// All rights reserved.
// 
// THIS FILE IS PART OF THE HAMARTIA FAULT/ERROR ANALYSIS SUITE
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met: 
// 
// 1. Redistributions of source code must retain the above copyright
// notice, this list of conditions and the following disclaimer. 
// 
// 2. Redistributions in binary form must reproduce the above copyright
// notice, this list of conditions and the following disclaimer in the
// documentation and/or other materials provided with the distribution. 
// 
// 3. Neither the name of the copyright holder nor the names of its
// contributors may be used to endorse or promote products derived from
// this software without specific prior written permission. 
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
// FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
// COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
// BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
// ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

/**
 * \file
 * \brief Logging (profile yaml file)
 *
 * \version 1.0.0
 * \author Nicholas Kelly, University of Texas
 *
 * \addtogroup logging
 * @{
 */

#ifndef _LOGGING_H
#define _LOGGING_H

// Libs
#include <cstdint>
#include <sstream>
#include <iostream>
#include <fstream>
#include <map>
#include <vector>
#include <yaml-cpp/yaml.h>

namespace logging {
    /* =====================================================================
     * PROFILE STRUCTURES
     * ===================================================================== */
    
    /// Operation profile
    typedef struct OperationProfileData {
        uint64_t count;                     ///< Operation count     
        std::vector<std::string> inputs;    ///< Operation intputs    
        std::vector<std::string> outputs;   ///< Operation outputs
    } OperationProfileData;
    
    /// Local (image) profile data
    typedef struct LocalProfileData {
        uint64_t op_count;                      ///< Operation count
        uint64_t filtered_op_count;             ///< Filtered operation count
        std::map<std::string, uint64_t> ops;    ///< Operations
        std::string img_name;                   ///< Image name
    } LocalProfileData;
    
    /// Global profile data
    typedef struct ProfileData {
        uint64_t g_op_count;                                ///< Global operation count
        uint64_t g_filtered_op_count;                       ///< Global filtered operation count
        std::map<std::string, OperationProfileData> g_ops;  ///< Global operations
        std::map<int, LocalProfileData> g_images;           ///< Image profile data
    } ProfileData;
    
    void AddProfileOp(ProfileData &profile_data, INS ins, uint32_t img_id)
    {
        std::string name = OPCODE_StringShort(INS_Opcode(ins));
        if (profile_data.g_ops.find(name) == profile_data.g_ops.end()) {
            OperationProfileData opd;
            opd.count = 1;
            
            for (uint32_t i = 0, l = INS_OperandCount(ins); i < l; ++i) {
                std::string op_str;
                if (INS_OperandIsReg(ins, i)) {
                    op_str = "reg (" + REG_StringShort(INS_OperandReg(ins, i)) + ")";
                } else if (INS_OperandIsImmediate(ins, i)) {
                    op_str = "imm";
                } else if (INS_OperandIsMemory(ins, i)) {
                    op_str = "mem";
                }
                if (INS_OperandRead(ins, i)) {
                    opd.inputs.push_back(op_str);
                }
                if (INS_OperandWritten(ins, i)) {
                    opd.outputs.push_back(op_str);
                }
            }
            
            profile_data.g_ops[name] = opd;
        } else {
            profile_data.g_ops[name].count++;
        }
        
        if (img_id && profile_data.g_images[img_id].ops.find(name) == profile_data.g_images[img_id].ops.end()) {
            profile_data.g_images[img_id].ops[name] = 1;
        } else if (img_id) {
            profile_data.g_images[img_id].ops[name]++;
        }
    }
    
    /**
     * Save the profiling output (YAML)
     *
     * \param profile_data Profile data to save
     * \param out_file     Output file (stream)
     * 
     * \return Success
     */
    bool EmitProfiling(ProfileData &profile_data, std::ofstream &out_file)
    {
        YAML::Emitter yaml_out;
        // Global operation counts
        yaml_out << YAML::BeginMap;
        yaml_out << YAML::Key << "global_op_count" << YAML::Value << profile_data.g_op_count;
        yaml_out << YAML::Key << "global_filtered_op_count" << YAML::Value << profile_data.g_filtered_op_count;
        yaml_out << YAML::Key << "ops" << YAML::BeginMap;
        // Ops
        for (std::map<std::string, OperationProfileData>::iterator op = profile_data.g_ops.begin();
             op != profile_data.g_ops.end(); ++op) {
            yaml_out << YAML::Key << op->first << YAML::Value;
            yaml_out << YAML::BeginMap;
            yaml_out << YAML::Key << "count" << YAML::Value << op->second.count;
            yaml_out << YAML::Key << "inputs" << YAML::Value;
            yaml_out << YAML::BeginSeq;
            for (std::vector<std::string>::iterator in = op->second.inputs.begin();
                 in != op->second.inputs.end(); ++in) {
                yaml_out << YAML::Value << *in;
            }
            yaml_out << YAML::EndSeq;
            yaml_out << YAML::Key << "outputs" << YAML::Value;
            yaml_out << YAML::BeginSeq;
            for (std::vector<std::string>::iterator out = op->second.outputs.begin();
                 out != op->second.outputs.end(); ++out) {
                yaml_out << YAML::Value << *out;
            }
            yaml_out << YAML::EndSeq;
            yaml_out << YAML::EndMap;
        }
        yaml_out << YAML::EndMap;
        yaml_out << YAML::Key << "images" << YAML::BeginMap;
        // Image specific operation counts
        for (std::map<int, LocalProfileData>::iterator img = profile_data.g_images.begin();
             img != profile_data.g_images.end(); ++img) {
            yaml_out << YAML::Key << img->first << YAML::Value;
            yaml_out << YAML::BeginMap;
            yaml_out << YAML::Key << "path" << YAML::Value << img->second.img_name;
            yaml_out << YAML::Key << "op_count" << YAML::Value << img->second.op_count;
            yaml_out << YAML::Key << "filtered_op_count" << YAML::Value << img->second.filtered_op_count;
            yaml_out << YAML::Key << "ops" << YAML::BeginMap;
            // Ops
            for (std::map<std::string, uint64_t>::iterator op = img->second.ops.begin();
                 op != img->second.ops.end(); ++op) {
                yaml_out << YAML::Key << op->first << YAML::Value << op->second;
            }
            yaml_out << YAML::EndMap;
            yaml_out << YAML::EndMap;
        }
        yaml_out << YAML::EndMap;
        yaml_out << YAML::EndMap;
        
        // Write out YAML log
        if (out_file.is_open()) {
            out_file << yaml_out.c_str() << std::endl;
            out_file.close();
        
            return true;
        }
        
        // Error
        return false;
    }
}
#endif
/** @} */
