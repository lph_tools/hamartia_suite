// Copyright 2015, The University of Texas at Austin 
// All rights reserved.
// 
// THIS FILE IS PART OF THE HAMARTIA FAULT/ERROR ANALYSIS SUITE
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met: 
// 
// 1. Redistributions of source code must retain the above copyright
// notice, this list of conditions and the following disclaimer. 
// 
// 2. Redistributions in binary form must reproduce the above copyright
// notice, this list of conditions and the following disclaimer in the
// documentation and/or other materials provided with the distribution. 
// 
// 3. Neither the name of the copyright holder nor the names of its
// contributors may be used to endorse or promote products derived from
// this software without specific prior written permission. 
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
// FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
// COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
// BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
// ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

/**
 *  \file
 *  \brief Pin injector
 *
 *  \version 1.0.0
 *  \author Michael Sullivan, University of Texas
 *  \author Nicholas Kelly, University of Texas
 *  \author Chun-Kai Chang, University of Texas
 *
 *  \addtogroup injector
 *  @{
 */

#ifndef _PIN_INJECTOR_H
#define _PIN_INJECTOR_H

// Error API
#include <hamartia_api/interface.h>
#include <hamartia_api/injector.h>

// Pin
#include "pin.H"
#include "instlib.H"

class PinInjector : public hamartia_api::Injector {
    public:
    /**
     * Constructor
     */
    PinInjector() : Injector() {}
    
    /**
     * Constructor with error model parameters
     *
     * \param error_model  Error model to execute
     * \param error_config Error model configuration file
     */
    PinInjector(std::string error_model, std::string error_config) : Injector(error_model, error_config) {}
    
    /**
     * Constructor with error model parameters
     *
     * \param error_model     Error model to execute
     * \param error_config    Error model configuration file
     * \param detector_model  Detector model to execute
     * \param detector_config Detector model configuration file
     */
    PinInjector(std::string error_model, std::string error_config, std::string detector_model, std::string detector_config) 
        : Injector(error_model, error_config, detector_model, detector_config) {}
    
    ~PinInjector() {}
  
    /**
     * Configure the injector
     *
     * \param num_to_inject   Number of errors to inject
     * \param error_point     The error point to inject (pre, post, or any)
     */
    virtual void Configure(uint64_t num_to_inject, const std::string& error_point) = 0;

    /**
     * Save the current state of Pin to current context, before output is calculated
     *
     * \param inst_data   Instruction data (error api)
     * \param r_registers Pin read registers (from instruction)
     * \param r_addrs     Pin read addresses (from instruction)
     * \param w_registers Pin write registers (from instruction)
     * \param w_addr      Pin write addresses (from instruction)
     */
    void PreCurrentStateToContext(hamartia_api::InstructionData &inst_data, const PIN_REGISTER * r_registers[], ADDRINT r_addrs[],
                                  const PIN_REGISTER * w_registers[], ADDRINT w_addr);
    
    /**
     * Save the current state of Pin to current context, after output is calculated
     *
     * \param inst_data   Instruction data (error api)
     * \param w_registers Pin write registers (from instruction)
     * \param w_addr      Pin write addresses (from instruction)
     * \param rflags      Pin RFLAGS
     */
    void PostCurrentStateToContext(hamartia_api::InstructionData &inst_data, const PIN_REGISTER *w_registers[], ADDRINT w_addr, ADDRINT rflags);
    
    /**
     * Save the current state of Pin to the injection context, after output is calculated (after pre-injection)
     *
     * \param inst_data   Instruction data (error api)
     * \param w_registers Pin write registers (from instruction)
     * \param w_addr      Pin write addresses (from instruction)
     * \param rflags      Pin RFLAGS
     */
    void InjectionStateToContext(const PIN_REGISTER *w_registers[], ADDRINT w_addr, ADDRINT rflags);
    
    /**
     * Rollback the Pin state to the target instruction
     *
     * \param state Pin context (state)
     */
    void RollbackState(CONTEXT *state);
    
    /**
     * Save the current context's old values to the Pin state, after pre-injection
     * 
     * \param state  Pin context (state)
     */
    void OldInputContextToState(CONTEXT *state);
    
    /**
     * Save the current context's old values to the Pin state, after injection
     * 
     * \param state  Pin context (state)
     * \param rflags RFLAGS handle
     */
    void OldOutputContextToState(CONTEXT *state);
    
    /**
     * Save the injection context to the Pin state, after pre-injection
     * 
     * \param r_registers Read (input) pin registers
     */
    void PreInjectionContextToState(PIN_REGISTER * r_registers[]);
    
    /**
     * Save the injection context to the Pin state, after injection
     * 
     * \param state  Pin context (state)
     * \param rflags RFLAGS handle
     */
    void InjectionContextToState(CONTEXT *state, ADDRINT &rflags);

    /**
     * Purge injection context with error-free context
     */
    void PurgeInjectionContext();

    private:
    
};

#endif
/** @} */

