// Copyright 2015, The University of Texas at Austin 
// All rights reserved.
// 
// THIS FILE IS PART OF THE HAMARTIA FAULT/ERROR ANALYSIS SUITE
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met: 
// 
// 1. Redistributions of source code must retain the above copyright
// notice, this list of conditions and the following disclaimer. 
// 
// 2. Redistributions in binary form must reproduce the above copyright
// notice, this list of conditions and the following disclaimer in the
// documentation and/or other materials provided with the distribution. 
// 
// 3. Neither the name of the copyright holder nor the names of its
// contributors may be used to endorse or promote products derived from
// this software without specific prior written permission. 
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
// FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
// COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
// BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
// ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

/**
 *  \file
 *  \brief Interface of timing-error injection
 *
 *  \version 1.1.0
 *  \author Chun-Kai Chang, University of Texas
 *
 *  \addtogroup error_profiler_injector
 *  @{
 */

#ifndef _TIMING_ERROR_H
#define _TIMING_ERROR_H

#include "pin_injector.h"

/**
 * Instruction-level instrumentation routine
 *
 * \param ins Instruction
 */
VOID TimingErrorInstruction(INS ins, VOID *v);


class TimingErrorInjector : public PinInjector {

    public:
    //FIXME: should also take a config of execution ports
    TimingErrorInjector(std::string error_model, std::string error_config, std::string detector_model, std::string detector_config) 
        : PinInjector(error_model, error_config, detector_model, detector_config) 
    {
        // Starts with PHASE_GET_PREV
        resetPhase();
        op2exu_map = InitExuMap();
        log_file.open("masked.yaml");
        resetInjectionTrigger();
    }

    ~TimingErrorInjector() {}

    void Configure(uint64_t num_to_inject, const std::string& error_point);

    /**
     * Save the current state of Pin to previous context, after output is calculated
     *
     * \param inst_data   Instruction data (error api)
     * \param w_registers Pin write registers (from instruction)
     * \param w_addr      Pin write addresses (from instruction)
     * \param rflags      Pin RFLAGS
     */
    void PostCurrentStateToPrevContext(hamartia_api::InstructionData &inst_data, const PIN_REGISTER *w_registers[], ADDRINT w_addr, ADDRINT rflags);

    /**
     * Test if input instruction data uses the same execution unit as previous instruction
     *
     * \param inst_data   Instruction data (error api)
     */
    bool isSameUnitAsPrev(hamartia_api::InstructionData &inst_data);

    /**
     * Test if this instruction has input values different from previous instruction
     *
     * \param inst_data   Instruction data (error api)
     */
    bool isDiffInputFromPrev(hamartia_api::InstructionData &inst_data);

    /**
     * Save all masked instructions to a log file
     */
    void SaveMaskedLog();

    /**
     * Reset the injector phase to PHASE_GET_PREV
     */
    inline void resetPhase()
    {
        phase = hamartia_api::PHASE_GET_PREV;
        INFO_PRINT("=====Enter PHASE_GET_PREV=====")
    }

    inline void setPhaseEval()
    {
        phase = hamartia_api::PHASE_EVAL;
        INFO_PRINT("=====Enter PHASE_EVAL=====")
    }

    /**
     * Set instruction data of previous context
     */ 
    inline void setPrevInstData(hamartia_api::InstructionData &inst_data)
    {
        current_context.prev_instruction_data = inst_data;
    }

    /**
     * Reset the injection trigger (used only in gate-level model)
     */ 
    inline void resetInjectionTrigger()
    {
        current_context.trigger.cont = true;
    }

    inline uint64_t getNumToInject()
    {
        return num_errors_to_inject;
    }

    inline void incNumInjected()
    {
        ++num_errors_injected;
    }

    inline void clearNumInjected()
    {
        num_errors_injected = 0;
    }

    inline bool reachNumToInject()
    {
        return num_errors_injected >= num_errors_to_inject;
    }    

    inline void clearErrorLog()
    {
        log.Clear();
    }    

    /**
     * Specialized InjectError for timing error injection
     *
     * \param auto_mode Unused, kept for compatability
     *
     * \return Successfully injected
     */
    virtual bool InjectError(bool auto_mode = false); 
    
    bool any_success = false; // for inject errors to consecutive instructions

    private:
    typedef enum // execution unit ID
    {
        EXU_ADD,
        EXU_MUL,
        EXU_DIV,
        EXU_SQRT,
        EXU_SHF,
        EXU_LOGIC,
        EXU_END
    } EXUID;
    typedef std::map<hamartia_api::Operation, EXUID> op2exu_type;
    op2exu_type op2exu_map;

    hamartia_api::DataType prev_dp;
    uint32_t prev_width;
    EXUID prev_exu_id;
    ofstream log_file;
   
    op2exu_type InitExuMap();
    void DumpAndExit();
    void HandleResponse(const hamartia_api::Resp&, const std::string&); 

};

#endif // #ifndef __TIMING_ERROR_H__
/** @} */

