// Copyright 2015, The University of Texas at Austin 
// All rights reserved.
// 
// THIS FILE IS PART OF THE HAMARTIA FAULT/ERROR ANALYSIS SUITE
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met: 
// 
// 1. Redistributions of source code must retain the above copyright
// notice, this list of conditions and the following disclaimer. 
// 
// 2. Redistributions in binary form must reproduce the above copyright
// notice, this list of conditions and the following disclaimer in the
// documentation and/or other materials provided with the distribution. 
// 
// 3. Neither the name of the copyright holder nor the names of its
// contributors may be used to endorse or promote products derived from
// this software without specific prior written permission. 
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
// FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
// COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
// BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
// ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

/**
 *  \file
 *  \brief Instruction helpers
 *
 *  \version 1.0.0
 *  \author Michael Sullivan, University of Texas
 *  \author Nicholas Kelly, University of Texas
 *
 *  \addtogroup helpers
 *  @{
 */

#ifndef _PIN_INSTRUCTION_HELPER_H
#define _PIN_INSTRUCTION_HELPER_H

// Libs
#include <string>
#include <unordered_map>
#include <map>

// Pin
#include "pin.H"
#include "instlib.H"

#include <hamartia_api/interface.h>
#include <hamartia_api/utils/memory.h>

/// Maximum number of operands possible
#define MAX_OPERANDS 3
/// Maximum number of write/destination operands possible
#define MAX_W_OPERANDS 3
/// Maximum number of read/source operands possible
#define MAX_R_OPERANDS 3

namespace pin_instruction_helper
{
    /**
     * Initialize an error api instruction from Pin
     *
     * \param inst Error API instruction
     * \param ins  Pin instruction
     */
    void SetInstructionFromPin(hamartia_api::Instruction *inst, const INS& ins);

    /**
     * Set instruction data (error api) from Pin
     *
     * \param inst_data InstructionData (error api)
     * \param ins       Pin instruction
     */
    void SetInstructionDataFromPin(hamartia_api::InstructionData &inst_data, const INS& ins);

    /**
     * Get operand info from the map
     *
     * \param ins   Pin instruction
     * \param inst_data_map   Map from PC to InstructionData
     * \param mem_r Number of memory read operands
     * \param mem_w Number of memory write operands
     * \param r_operands read operands registers
     * \param w_operands write operands registers
     */
    void GetInstOperandInfo(const INS& ins, std::unordered_map<ADDRINT, hamartia_api::InstructionData>& inst_data_map, 
            uint32_t& mem_r, uint32_t& mem_w, REG* r_operands, REG* w_operands);

    /**
     * Get metadata of an instruction (e.g., source code line, process id, etc.)
     * \param  inst_data InstructionData (error api)
     * \return a map from field name string to value string
     */
    std::map<std::string, std::string> GetMetaData(hamartia_api::InstructionData &inst_data);
}

#endif
/** @} */

