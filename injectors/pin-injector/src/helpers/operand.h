// Copyright 2015, The University of Texas at Austin 
// All rights reserved.
// 
// THIS FILE IS PART OF THE HAMARTIA FAULT/ERROR ANALYSIS SUITE
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met: 
// 
// 1. Redistributions of source code must retain the above copyright
// notice, this list of conditions and the following disclaimer. 
// 
// 2. Redistributions in binary form must reproduce the above copyright
// notice, this list of conditions and the following disclaimer in the
// documentation and/or other materials provided with the distribution. 
// 
// 3. Neither the name of the copyright holder nor the names of its
// contributors may be used to endorse or promote products derived from
// this software without specific prior written permission. 
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
// FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
// COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
// BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
// ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

/**
 *  \file
 *  \brief Operand helpers
 *
 *  \version 1.0.0
 *  \author Michael Sullivan, University of Texas
 *  \author Nicholas Kelly, University of Texas
 *
 *  \addtogroup helpers
 *  @{
 */

#ifndef _PIN_OPERAND_HELPER_H
#define _PIN_OPERAND_HELPER_H
//DEBUG
#include <hamartia_api/debug.h>

#include <hamartia_api/types.h>
#include <hamartia_api/interface.h>
#include <hamartia_api/utils/memory.h>

namespace pin_operand_helper
{
    /**
     * Is the instruction a packed (SIMD) instruction
     *
     * \param ins Pin instruction
     * \return If instruction is SIMD
     */
    inline bool IsPackedSimd(INS ins)
    {
        xed_decoded_inst_t* xedd = INS_XedDec(ins);
        return !xed_decoded_inst_get_attribute(xedd, XED_ATTRIBUTE_SIMD_SCALAR);
    }

    /**
     * Determine the element-size within a SIMD/packed instruction
     *
     * \param ins      Pin instruction
     * \param op_index Operand index
     *
     * \return Element size/width
     */
    inline uint32_t SimdElementSize(INS ins, uint32_t op_index)
    {
        xed_decoded_inst_t* xedd = INS_XedDec(ins);
        return  xed_decoded_inst_operand_element_size_bits(xedd, op_index);
    }

    /**
     * Calculate number of SIMD lanes (packed)
     *
     * \param ins      Pin instruction
     * \param op_index Operand index
     * \param op_width Operation width
     *
     * \return SIMD lanes
     */
    inline uint32_t SimdWidth(INS ins, uint32_t op_index, uint32_t op_width)
    {
        uint32_t element_width = SimdElementSize(ins, op_index);
        if (IsPackedSimd(ins) && element_width > 0) {
            return op_width / element_width;
        } else {
            return 1;   // is a scalar instruction
        }
    }

    /**
     * Determine the datatype of the instruction
     *
     * \param ins      Pin instruction
     * \param op_index Operand index
     * 
     * \return Datatype
     */
    inline hamartia_api::DataType OperandDataType(INS ins, uint32_t op_index)
    {
        xed_decoded_inst_t* xedd = INS_XedDec(ins);
        xed_operand_element_type_enum_t type = xed_decoded_inst_operand_element_type(xedd, op_index);
        if (type == XED_OPERAND_ELEMENT_TYPE_UINT) {
            return hamartia_api::DT_UINT;
        } else if (type == XED_OPERAND_ELEMENT_TYPE_INT) {
            return hamartia_api::DT_INT;
        } else if (type == XED_OPERAND_ELEMENT_TYPE_SINGLE) {
            return hamartia_api::DT_FLOAT;
        } else if (type == XED_OPERAND_ELEMENT_TYPE_DOUBLE) {
            return hamartia_api::DT_DOUBLE;
        }
        return hamartia_api::DT_UINT;
    }

    /**
     * Update the RFLAGS by inferring some flags based upon the result
     *
     * \param cxt    Error context
     * \param rflags Current RFLAGS value
     */
    // FIXME: use new functions
    inline void UpdateRFlags(hamartia_api::ErrorContext *cxt, ADDRINT &rflags)
    {
        hamartia_api::DataValue dv;
        hamartia_api::DataValue result = cxt->getOutputValue();
        dv.uint(rflags);
        // Parity flag 
        // FIXME: parity bit should be set when there are even 1's in least significant byte instead of just checking lsb
        dv.rflags_parity(~result.uint() & 0x1);
        // Zero flag
        // FIXME: what about FP?
        dv.rflags_zero(result.uint() == 0);
        // Sign flag
        // FIXME: correct, or is the conversion taken care of?
        if (cxt->instruction_data.width == 32) {
            dv.rflags_sign(result.uint() & ((uint64_t) 1 << 31));
        } else {
            dv.rflags_sign(result.uint() & ((uint64_t) 1 << 63));
        }
        // Save
        rflags = dv.uint();
    }

    /**
     * Set/initialize an (error api) Operand from Pin
     *
     * \param op      Operand to set
     * \param ins     Pin instruction
     * \param op_idx  Pin operand index
     * \param reg_idx Register index
     * \param regs    Pin registers
     * \param mem_idx Memory (read) operand index
     *
     * \return Operand type
     */
    inline hamartia_api::OperandType SetOperandFromPin(hamartia_api::Operand *op, INS ins, uint32_t op_idx, uint32_t &reg_idx, REG regs[], uint32_t &mem_idx)
    {
        // General operand data
        op->width = INS_OperandWidth(ins, op_idx);
        op->simd_width = pin_operand_helper::SimdWidth(ins, op_idx, op->width);
        op->data_type = OperandDataType(ins, op_idx);

        if (INS_OperandIsImmediate(ins, op_idx)) {
            // Immediate
            op->type = hamartia_api::OT_IMMEDIATE;
            // FIXME: macro?
            hamartia_api::DataValue val;
            val.uint(INS_OperandImmediate(ins, op_idx));
            op->setValue(val, 0);
        } else if (INS_OperandIsReg(ins, op_idx)) {
            // Register
            REG reg = INS_OperandReg(ins, op_idx);
            op->reg_id = reg;
            op->reg = REG_StringShort(reg);
             //INFO_PRINT("REG Operand= " << op->reg)    
            if (REG_is_flags(reg)) {
                // Flags
                op->type = hamartia_api::OT_FLAGS;
            } else if (REG_is_status_flags(reg)) {
                // Status
                op->type = hamartia_api::OT_STATUS;
            } else {
                // Register
                op->type = hamartia_api::OT_REGISTER;
                regs[reg_idx++] = reg;
            }
        } else if (INS_OperandIsMemory(ins, op_idx)) {
            // Memory
            op->width = INS_MemoryOperandSize(ins, mem_idx++) * 8;  // overwrite
            op->type = hamartia_api::OT_MEMORY;
        }

        // Return operand type
        return op->type;
    }

    /**
     * Update an (error api) Operand from Pin
     *
     * \param op         Operand to update
     * \param simd_lanes SIMD width
     * \param reg_idx    Register index
     * \param mem_idx    Memory operand index
     * \param regs       Pin registers
     * \param addrs      Memory addresses
     * \param old_value  Update old value instead
     */
    inline void UpdateOperandFromPin(hamartia_api::Operand *op, uint8_t simd_lanes, uint32_t &reg_idx, uint32_t &mem_idx, const PIN_REGISTER *regs[], ADDRINT addrs[], bool old_value = false)
    {
        hamartia_api::DataValue val;
        // For memory
        uint64_t mem[8];

        if (op->type == hamartia_api::OT_REGISTER || op->type == hamartia_api::OT_FLAGS) {
            // Register
            const PIN_REGISTER * rop = regs[reg_idx++];
            if (op->data_type == hamartia_api::DT_FLOAT) {
                // FP
                for (int i = 0; i < simd_lanes; ++i) {
                    val.fp32(rop->flt[i]);
                    if (!old_value) {
                        op->setValue(val, i);
                    } else {
                        op->setOldValue(val, i);
                    }
                }
            } else if (op->data_type == hamartia_api::DT_DOUBLE) {
                for (int i = 0; i < simd_lanes; ++i) {
                    val.fp64(rop->dbl[i]);
                    if (!old_value) {
                        op->setValue(val, i);
                    } else {
                        op->setOldValue(val, i);
                    }
                }

            } else {
                // Scalar
                for (int i = 0; i < simd_lanes; ++i) {
                    uint32_t element_width = op->elementSize();
                    if (element_width == 8) {
                        val.uint(rop->byte[i]);
                    } else if (element_width == 16) {
                        val.uint(rop->word[i]);
                    } else if (element_width == 32) {
                        val.uint(rop->dword[i]);
                    } else {
                        val.uint(rop->qword[i]);
                    }
                    if (!old_value) {
                        op->setValue(val, i);
                    } else {
                        op->setOldValue(val, i);
                    }
                }
            }
        } else if (op->type == hamartia_api::OT_MEMORY) {
            // Memory
            // Address
            ADDRINT addr = addrs[mem_idx++];
            op->address.virt = addr;
            op->address.phys = hamartia_api::memory::VirtualToPhysical(addr);

            // Values
            // FIXME: replace with  new function!
            uint64_t element_width = op->width / simd_lanes;
            PIN_SafeCopy(&mem, reinterpret_cast<VOID*>(addr), op->width / 8);
            for (int i = 0; i < simd_lanes; ++i) {
                int mi = i * element_width / 64;
                int ms = i * element_width % 64;
                uint64_t mask = (((uint64_t) 1) << element_width) - 1;
                if (element_width == 64) mask = 0xFFFFFFFFFFFFFFFF;

                val.uint((mem[mi] >> ms) & mask);
                // FIXME: change type!
                if (!old_value) {
                    op->setValue(val, i);
                } else {
                    op->setOldValue(val, i);
                }
            }
        }
    }

    /**
     * Update/set Pin register with Datavalue
     *
     * \param state Pin Context
     * \param reg   Register to update
     * \param width Width of data
     * \param val   Value to set
     */
    inline void UpdatePinReg(CONTEXT *state, REG reg, uint8_t width, hamartia_api::DataValue val)
    {
        UINT8 buf[8];
        for (int j = 0, m = width / 8; j < m; ++j) {
            buf[j] = val.uint() >> (j * 8);
        }
        PIN_SetContextRegval(state, reg, (UINT8 *) &buf);
    }

    /**
     * Update register in a Pin context with Operand
     *
     * \param state     Pin Context
     * \param op        Operand pointer
     * \param old_value Update with old value or current one
     */   
    inline void UpdateRegInContextFromOperand(CONTEXT *state, const hamartia_api::Operand *op, bool old_value)
    {
        UINT8 buf[32];
        REG reg = (REG) op->reg_id;
        uint32_t element_width = op->elementSize();
        int l = (old_value) ? op->old_value.size() : op->value.size(); // no. of SIMD lanes
        for (int i = 0; i < l; ++i) {
            const hamartia_api::DataValue & value = (old_value) ? op->old_value[i] : op->value[i];
            for (int j = 0, m = element_width / 8; j < m; ++j) {
                buf[i * m + j] = value.uint() >> (j * 8);
            }
        }
        PIN_SetContextRegval(state, reg, (UINT8 *) &buf);
    }

    /**
     * Update register directly with Operand
     *
     * \param reg       Pointer to register
     * \param op        Operand pointer
     * \param old_value Update with old value or current one
     */ 
    inline void UpdateRegFromOperand(PIN_REGISTER *reg, const hamartia_api::Operand *op, bool old_value)
    {
        uint32_t element_width = op->elementSize();
        int l = (old_value) ? op->old_value.size() : op->value.size();
        for (int i = 0; i < l; ++i) {
            const hamartia_api::DataValue & value = (old_value) ? op->old_value[i] : op->value[i];
            
            if (op->data_type == hamartia_api::DT_FLOAT) {
                reg->flt[i] = value.fp32();
            } else if (op->data_type == hamartia_api::DT_DOUBLE) {
                reg->dbl[i] = value.fp64();
            } else {
                if (element_width == 8) {
                    reg->byte[i] = value.uint();
                } else if (element_width == 16) {
                    reg->word[i] = value.uint();
                } else if (element_width == 32) {
                    reg->dword[i] = value.uint();
                } else {
                    reg->qword[i] = value.uint();
                }
            }
        }
    }

    /**
     * Update memory content with Operand
     *
     * \param op        Operand pointer
     * \param old_value Update with old value or current one
     */ 
    inline void UpdateMemFromOperand(const hamartia_api::Operand *op, bool old_value)
    {
        uint64_t mem[8] = {0,0,0,0,0,0,0,0};
        uint8_t simd_lanes = op->simd_width;
        uint64_t element_width = op->width / simd_lanes;
        uint64_t mask = (((uint64_t) 1) << element_width) - 1;
        if (element_width == 64) mask = 0xFFFFFFFFFFFFFFFF;
        for (auto i = 0; i < simd_lanes; ++i) {
            int mi = i * element_width / 64;
            int ms = i * element_width % 64;
            const hamartia_api::DataValue & value = (old_value) ? op->old_value[i] : op->value[i];
            mem[mi] |= ((mask & value.uint()) << ms);
        }    
        /*
        for (auto i=0; i<8; i++) {
            INFO_PRINT("mem " << i << " has value 0x" << std::hex << mem[i]<< std::dec)
        } 
        */   
        PIN_SafeCopy(reinterpret_cast<VOID*>((ADDRINT) op->address.virt), &mem, op->width/8);
    }    

    /**
     * Update the Pin state from an (error api) Operand
     *
     * \param op        Operand
     * \param state     Pin context
     * \param old_value Update from old value instead
     */
    inline void UpdatePinFromOperand(const hamartia_api::Operand *op, CONTEXT *state, bool old_value = false)
    {
        // FIXME: what about addresses?
        if (op->type == hamartia_api::OT_REGISTER) {
            UpdateRegInContextFromOperand(state, op, old_value);
        } else if (op->type == hamartia_api::OT_FLAGS) {
            // FIXME: look into use cases...
        } else if (op->type == hamartia_api::OT_STATUS) {
            // FIXME: look into use cases...
        } else if (op->type == hamartia_api::OT_MEMORY) {
            UpdateMemFromOperand(op, old_value);
        }
    }

    /**
     * Update the Pin state from an (error api) Operand
     *
     * \param op        Operand
     * \param reg_idx   (Current) Register index
     * \param regs      PIN_REGISTER pointers
     * \param old_value Update from old value instead
     */
    inline void UpdatePinFromOperand(const hamartia_api::Operand *op, uint32_t &reg_idx, PIN_REGISTER *regs[], bool old_value = false)
    {
        // FIXME: what about addresses?
        if (op->type == hamartia_api::OT_REGISTER) {
            UpdateRegFromOperand(regs[reg_idx], op, old_value);
            ++reg_idx;
        } else if (op->type == hamartia_api::OT_FLAGS) {
            // FIXME: look into use cases...
        } else if (op->type == hamartia_api::OT_STATUS) {
            // FIXME: look into use cases...
        } else if (op->type == hamartia_api::OT_MEMORY) {
            UpdateMemFromOperand(op, old_value);
        }
    }
}

#endif
/** @} */
