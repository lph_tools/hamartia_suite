// Copyright 2015, The University of Texas at Austin 
// All rights reserved.
// 
// THIS FILE IS PART OF THE HAMARTIA FAULT/ERROR ANALYSIS SUITE
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met: 
// 
// 1. Redistributions of source code must retain the above copyright
// notice, this list of conditions and the following disclaimer. 
// 
// 2. Redistributions in binary form must reproduce the above copyright
// notice, this list of conditions and the following disclaimer in the
// documentation and/or other materials provided with the distribution. 
// 
// 3. Neither the name of the copyright holder nor the names of its
// contributors may be used to endorse or promote products derived from
// this software without specific prior written permission. 
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
// FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
// COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
// BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
// ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

/**
 *  \file
 *  \brief Implementation of transient-error injection
 *
 *  \version 1.0.0
 *  \author Michael Sullivan, University of Texas
 *  \author Nicholas Kelly, University of Texas
 *  \author Chun-Kai Chang, University of Texas
 *
 *  \addtogroup error_profiler_injector
 *  @{
 */

#include <hamartia_api/utils/error.h>

#include "transient_error.h"
#include "error_prof_inj.h"
#include "fast_forward.h"
#include "code_filter.h"
#include "oclasses.h"
#include "helpers/instruction.h"
#include "helpers/operand.h"


/**
 * Target instruction found; setup operands and other data before injecting
 *
 * \param opcode    Opcode for instruction
 * \param ins_index Instruction index to lookup instruction data from global buffer
 * \param r1        First read operand
 * \param r2        Second read operand
 * \param w1        First write operand
 * \param w2        Second write operand
 * \param rflags    RFLAGS value
 * \param mwrite_ea Memory write address
 * \param mread_ea1 First memory read address
 * \param mread_ea2 Second memory read address
 */
static VOID pre_then(UINT32 opcode, ADDRINT addr, 
                     PIN_REGISTER *r1, PIN_REGISTER *r2, PIN_REGISTER *r3, 
                     const PIN_REGISTER *w1, const PIN_REGISTER *w2, const PIN_REGISTER *w3,
                     ADDRINT rflags, ADDRINT mwrite_ea, ADDRINT mread_ea1, ADDRINT mread_ea2)
{
    if(detach_switch) return;
    INFO_PRINT("Pre-Inst routine...")

    // Disable instrumentation if we are done
    if (injector == nullptr) {
        //INFO_PRINT("Detach!")
        //PIN_Detach(); /*Ideally we should detach, but this API results in SDC*/
        INFO_PRINT("Disable Instrumentation...")
        detach_switch = true;
        //Jit new instrumentation code
        PIN_RemoveInstrumentation();
        return;
    }

    hamartia_api::InstructionData &inst_data = g_inst_data_map[addr];
    // Find proper sub-word to target, for packed instructions
    // Pre-calculate target SIMD lane
    if (inst_data.simd_width > 1)
        op_tgt_index = (UINT32)(hamartia_api::utils::mt_random() % inst_data.simd_width);  // random SIMD lane
    else
        op_tgt_index = 0;  // non-SIMD operation

    PIN_REGISTER * _r_operands[MAX_R_OPERANDS] = {r1, r2, r3}; 
    const PIN_REGISTER * _w_operands[MAX_W_OPERANDS] = {w1, w2, w3}; 
    ADDRINT _addrs[2] = {mread_ea1, mread_ea2};

    if (injector->Phase() == hamartia_api::PHASE_EVAL) {
        // Evaulation phase
        injector->PreCurrentStateToContext(inst_data, const_cast<const PIN_REGISTER**>(_r_operands), _addrs, _w_operands, mwrite_ea);
    } else {
        // Injection phase
        // Pre-injection
        bool success = injector->PreInjectError();
        if (success) {
            injector->PreInjectionContextToState(_r_operands);
        }
        // TODO: could write to the context and then only run the preinjecter if not in
        // the pre inject phase (just continue)
    }

    // Register the write EA
    write_ea = mwrite_ea;
}

/**
 * Target instruction found; inject an error!
 *
 * \param context   Machine context (state)
 * \param opcode    Opcode for instruction
 * \param addr      Instruction address (PC)
 * \param ins_index Instruction index to lookup instruction data from global buffer
 * \param w1        First write operand register
 * \param w2        Second write operand register
 * \param rflags    Initial RFLAGS
 */

static VOID then(CONTEXT * state, UINT32 opcode, ADDRINT addr,
                 PIN_REGISTER *w1, PIN_REGISTER *w2, PIN_REGISTER *w3, ADDRINT &rflags)
{

    if(detach_switch) return;
    // All methods should be inlined!
    INFO_PRINT("Post-Inst routine...")

    if (injector == nullptr || injector->Done()) return;

    // Find proper sub-word to target, for packed instructions
    hamartia_api::InstructionData &inst_data = g_inst_data_map[addr];

    // Format function values
    PIN_REGISTER * _w_operands[MAX_W_OPERANDS] = {w1, w2, w3};

    if (injector->Phase() == hamartia_api::PHASE_EVAL) {
        // Evaluation phase
        // Populate context from instruction
        injector->PostCurrentStateToContext(inst_data, const_cast<const PIN_REGISTER**>(_w_operands), write_ea, rflags);
        injector->RollbackState(state);
    } else {
        //FIXME: Is this the correct injection start time?
        gettimeofday(&pin_inj, NULL);
        // Injection phase
        if (injector->Phase() == hamartia_api::PHASE_PRE_INJECT) {
            injector->InjectionStateToContext(const_cast<const PIN_REGISTER**>(_w_operands), write_ea, rflags);
        }
        // Inject error
        bool success = injector->InjectError();
        INFO_PRINT("INJ " << success << " at filtered instr # "<< ticount)

        if (success) {
            // Rollback state
            injector->OldOutputContextToState(state);
            // Update to injection state
            injector->InjectionContextToState(state, rflags);
            // Commit context
            injector->CommitContext();
    
            if (injector->Phase() == hamartia_api::PHASE_INJECTED) {
                // Write out log
                INFO_PRINT("Finish!")
                InjectFini();
                injector = nullptr;
            } else{//more injections
                //update target instruction
                tilist_idx++;
                ticount = tilist[tilist_idx];
                PIN_RemoveInstrumentation();
            }
            INFO_PRINT("Return")
        } else { 
            // Only pre-injection can follow this path because InjectError() with post-injection always returns success 
            INFO_PRINT("=============Maksed/Detected Pre-Injection Error=============")
            // purge the injection context and rollback
            injector->PurgeInjectionContext();
            injector->RollbackState(state);
        }
    }
    // Executed context
    PIN_ExecuteAt(state);
}

VOID InsertPreThen(INS& ins, const uint32_t& mem_r, const uint32_t& mem_w, REG* r_operands, REG* w_operands)
{
    InsertThenTemplate(ins, mem_r, mem_w, r_operands, w_operands, (AFUNPTR) pre_then);
}    

VOID TransientErrorInstruction(INS ins, VOID *v)
{
    //Disable instrumentation if true
    if (detach_switch) {
        return;
    }
    if (fast_forwarding) {
        return;
    }
    // Check if MPI rank id is known
    if (is_mpi && !disable_getrank) {
        return;
    }
    // Find resident image (and ignore if in ignore_imgs) 
    if (isInIgnoredImg(ins, ignore_imgs, images)) {
        return;
    }    
    // Ignore code region headers
    /// Side effect: Flip inject_toggle whenever we see code region headers
    if (inject_regions && isCodeRegionHeader(ins, inject_toggle)) {
        return;
    }    

    if (inject_toggle || !inject_regions) {
        // Filter out the operations from the desired classes 
        if (InsInOperationClasses(ins, oclasses, inject_width) && INS_HasFallThrough(ins)) {
            // Index
            uint32_t mem_r = 0, mem_w = 0;
            // Registers
            REG r_operands[MAX_R_OPERANDS] = {REG_INST_G0, REG_INST_G0, REG_INST_G0};
            REG w_operands[MAX_W_OPERANDS] = {REG_INST_G0, REG_INST_G0, REG_INST_G0};
            // Extract information for error injection
            pin_instruction_helper::GetInstOperandInfo(ins, g_inst_data_map, mem_r, mem_w, r_operands, w_operands);

            // Insert a call to register the instruction
            INS_InsertIfCall(ins, IPOINT_BEFORE, (AFUNPTR)insif_incr, IARG_END);

            InsertPreThen(ins, mem_r, mem_w, r_operands, w_operands);

            // Insert a call to check if we reach target instruction
            INS_InsertIfCall(ins, IPOINT_AFTER, (AFUNPTR)insif, IARG_END);

            // Inject an error
            INS_InsertThenCall(ins, IPOINT_AFTER, (AFUNPTR)then, 
                                    IARG_CONTEXT, \
                                    IARG_UINT32, INS_Opcode(ins), \
                                    IARG_INST_PTR, \
                                    IARG_REG_REFERENCE, w_operands[0], \
                                    IARG_REG_REFERENCE, w_operands[1], \
                                    IARG_REG_REFERENCE, w_operands[2], \
                                    IARG_REG_REFERENCE, REG_RFLAGS, \
                                    IARG_END);
        }
    }
}


/* =====================================================================
 * Transient Error Specific Functions
 * ===================================================================== */
void TransientErrorInjector::Configure(uint64_t num_to_inject, const std::string& error_point)
{
    SetNumToRecord(num_to_inject);
    SetNumToInject(num_to_inject);
    // Set injection method(s)
    if (error_point == "pre" || error_point == "any") {
        INFO_PRINT("Register Input injection")
        AcceptErrorPreInject();
    }
    if (error_point == "post" || error_point == "any") {
        INFO_PRINT("Register Output injection")
        AcceptErrorInject();
    }
}    
