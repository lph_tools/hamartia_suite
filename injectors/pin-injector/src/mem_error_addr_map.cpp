// Copyright 2015, The University of Texas at Austin 
// All rights reserved.
// 
// THIS FILE IS PART OF THE HAMARTIA FAULT/ERROR ANALYSIS SUITE
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met: 
// 
// 1. Redistributions of source code must retain the above copyright
// notice, this list of conditions and the following disclaimer. 
// 
// 2. Redistributions in binary form must reproduce the above copyright
// notice, this list of conditions and the following disclaimer in the
// documentation and/or other materials provided with the distribution. 
// 
// 3. Neither the name of the copyright holder nor the names of its
// contributors may be used to endorse or promote products derived from
// this software without specific prior written permission. 
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
// FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
// COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
// BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
// ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

/**
 *  \file
 *  \brief Physical to DRAM address mapping implementation
 *
 *  \version 1.1.0
 *  \author Chun-Kai Chang, University of Texas
 *
 *  \addtogroup error_profiler_injector
 *  @{
 */

// Mapping is based on study by
// Pessl, Peter, et al. "DRAMA: Exploiting DRAM Addressing for Cross-CPU Attacks." USENIX Security Symposium. 2016.

#include <unordered_map>

#include <hamartia_api/debug.h>
#include <hamartia_api/utils/memory.h>

#include "mem_error_addr_map.h"


dram_addr_t intel_sandy_1_ch(phys_addr_t);
dram_addr_t intel_sandy_2_ch(phys_addr_t);
AddrMapManager::AddrFieldWidth intel_sandy_addr_field_width = {3, 16, 10}; // bank_num_bits, row_num_bit, col_num_bits

dram_addr_t intel_haswell(phys_addr_t);
AddrMapManager::AddrFieldWidth intel_haswell_addr_field_width = {4, 16, 10};

phys2dram_fn_t AddrMapManager::addr_map = nullptr;
AddrMapManager::AddrFieldWidth AddrMapManager::addr_field_width = intel_sandy_addr_field_width;

void AddrMapManager::RegisterAddrMap(std::string map_str, dram_info_t dram_info)
{
   if (map_str == "haswell") {
       FATAL_ERROR("Unimplemented address mapping...")
       addr_map = &intel_haswell;
       addr_field_width = intel_haswell_addr_field_width;
   } else if (map_str == "sandy") {
       if (dram_info.num_ch == 1) {
           addr_map = &intel_sandy_1_ch;
       } else {
           addr_map = &intel_sandy_2_ch;
       }
       addr_field_width = intel_sandy_addr_field_width;
   } else {
       FATAL_ERROR("Invalid address map: " << map_str)
   }
}    
 
dram_addr_t AddrMapManager::Virt2DRAM(virt_addr_t vaddr)
{
    static std::unordered_map<virt_addr_t, dram_addr_t> tlb; 

    std::unordered_map<virt_addr_t, dram_addr_t>::iterator it = tlb.find(vaddr);
    if ( it == tlb.end()) { 
        phys_addr_t paddr = hamartia_api::memory::VirtualToPhysical(vaddr);
        if (!paddr) {
            FATAL_ERROR("Virtual to physical address mapping is not available...");
        }
        dram_addr_t daddr = (*addr_map)(paddr); 
        tlb[vaddr] = daddr;
        return daddr;
    } else {
        return it->second;
    }    
}

dram_addr_t AddrMapManager::Phys2DRAM(phys_addr_t paddr)
{
    return (*addr_map)(paddr); 
}    

AddrMapManager::AddrFieldWidth AddrMapManager::GetFieldWidth()
{
    return addr_field_width;
}

/* =====================================================================
 * Details
 * ===================================================================== */
const uint8_t BUS_BITS = 3; // assume 64-bit-wide bus

inline static phys_addr_t get_lower_n_bits(phys_addr_t addr, uint64_t n)
{
    return addr & ((1ULL << n) - 1);
}

inline static uint8_t get_bit_n(phys_addr_t addr, uint8_t n)
{
    return (addr >> n) & 1;
}

inline static phys_addr_t remove_bit_n(phys_addr_t addr, uint8_t n)
{
    return get_lower_n_bits(addr, n) + ((addr >> (n+1)) << n);
}

/** Mapping Legend:
 * top: bit position; bottom: bit meaning
 * bs: bus, ch: channel, co: column, ro: row, rk: rank
 * b{i}: bank addr bit i (hash if appear at multiple bit positions)
 * g{i}: bank group bit i (hash if appear at multiple bit positions)
 */

/** SandyBridge 
 *
 * Single-Channel
 * 35 .. 20 19 18 17 16 15 14 13 12 11 10 09 08 07 06 05 04 03 02 01 00
 * -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
 * ro ro ro b2 b1 b0 rk b2 b1 b0 co co co co co co co co co co bs bs bs
 * 
 * Dual-Channel
 * 36 .. 20 19 18 17 16 15 14 13 12 11 10 09 08 07 06 05 04 03 02 01 00
 * -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
 * ro ro b2 b1 b0 rk b2 b1 b0 co co co co co co co ch co co co bs bs bs
 *
 * Note that the only difference is if bit 06 is used as the channel bit
 */
dram_addr_t intel_sandy_1_ch(phys_addr_t phys_addr)
{
    dram_addr_t ret = {0,0,0,0,0,0};

    phys_addr >>= BUS_BITS;
    int col_bits = 10; 
    ret.col = static_cast<uint16_t>(get_lower_n_bits(phys_addr, col_bits));
    phys_addr >>= col_bits; // now bit 0 is original bit 13 (1 ch) or 14 (2 ch)

    ret.rank = static_cast<uint8_t>(get_bit_n(phys_addr, 3));

    int bank_bits = 3;
    for(int i = 0; i < bank_bits; i++) {
        ret.bank |= ((get_bit_n(phys_addr, i) ^ get_bit_n(phys_addr, i+4)) << i);
    }
    phys_addr >>= 7; // now bit 0 is original bit 20 or 21

    int row_bits = 16;
    ret.row = static_cast<uint16_t>(get_lower_n_bits(phys_addr, row_bits));

    return ret;
}

dram_addr_t intel_sandy_2_ch(phys_addr_t phys_addr)
{
    // save the channel bit
    uint8_t ch = get_bit_n(phys_addr, 6);
    // call the single channel version with bit 06 removed
    dram_addr_t ret = intel_sandy_1_ch( remove_bit_n(phys_addr, 6) );
    ret.ch = ch;

    return ret;
}

dram_addr_t intel_haswell(phys_addr_t phys_addr)
{
    dram_addr_t ret = {0,0,0,0,0,0};
    return ret;
}


/** @} */
