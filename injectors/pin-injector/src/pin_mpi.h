// Copyright 2015, The University of Texas at Austin 
// All rights reserved.
// 
// THIS FILE IS PART OF THE HAMARTIA FAULT/ERROR ANALYSIS SUITE
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met: 
// 
// 1. Redistributions of source code must retain the above copyright
// notice, this list of conditions and the following disclaimer. 
// 
// 2. Redistributions in binary form must reproduce the above copyright
// notice, this list of conditions and the following disclaimer in the
// documentation and/or other materials provided with the distribution. 
// 
// 3. Neither the name of the copyright holder nor the names of its
// contributors may be used to endorse or promote products derived from
// this software without specific prior written permission. 
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
// FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
// COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
// BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
// ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

/**
 *  \file
 *  \brief Routines for MPI instrumentation
 *
 *  \version 1.0.0
 *  \author Chun-Kai Chang, University of Texas
 *
 *  \addtogroup error_profiler_injector
 *  @{
 */

#ifndef __PIN_MPI_H__
#define __PIN_MPI_H__

#include "error_prof_inj.h"

/* Command-line switches */
/// MPI program switch (default: non-MPI single process single thread)
KNOB<BOOL> KnobIsMpi(KNOB_MODE_WRITEONCE, "pintool",
        "m", "0", "MPI application");
/// MPI task to inject into (valid only if IsMpi is true)
KNOB<UINT32> KnobRankId(KNOB_MODE_WRITEONCE, "pintool",
        "t", "0", "specify the MPI task id to inject");


BOOL is_mpi = false;
BOOL disable_getrank = false;
static UINT32 rank_id = 0;
static UINT64* rank_addr = 0;

VOID MPIInit()
{
    INFO_PRINT("Use MPI Mode...")
    // MPI process to inject error into
    rank_id = KnobRankId.Value();
}

VOID Arg2Before(UINT64* in_id) 
{
    if(disable_getrank)
        return;
    rank_addr = in_id;
    //std::cout<< "Before: Pid " << PIN_GetPid() << " has rank id address " << in_id <<"\n";
    //std::cout<< "Before: a = " << in_id <<"\n";
}

VOID RankAfter()
{
    if(disable_getrank)
        return;
    int this_rank_id = (int)(*rank_addr);
    disable_getrank = true;
    if(((int)rank_id) != this_rank_id){
        INFO_PRINT("Rank " << this_rank_id << " Detaches!")
        //PIN_Detach(); /*Ideally we should detach, but this API results in SDC*/
        detach_switch = true;
    }
    else{ // only the target rank setups injector
        InjectorInit();
        gettimeofday(&pin_start, NULL);
    }
    PIN_RemoveInstrumentation();
}

VOID GetRankId(IMG img, VOID *v)
{
    if(disable_getrank)
        return;
    //MPI-C (call by address)
    RTN rankRtn = RTN_FindByName(img, "MPI_Comm_rank");
    //MPI-Fortran (call by reference)
    RTN fortRankRtn = RTN_FindByName(img, "mpi_comm_rank");
    //INFO_PRINT("Looking for Rank Function...")
    if (RTN_Valid(rankRtn)){
        //INFO_PRINT("Found C Rank Function!")
        RTN_Open(rankRtn);
        RTN_InsertCall(rankRtn, IPOINT_BEFORE, (AFUNPTR)Arg2Before,
                       IARG_FUNCARG_ENTRYPOINT_VALUE, 1,//the 2nd arg is &rank
                       IARG_CALL_ORDER, CALL_ORDER_FIRST,
                       IARG_END);
    
        RTN_InsertCall(rankRtn, IPOINT_AFTER, (AFUNPTR)RankAfter,
                       IARG_CALL_ORDER, CALL_ORDER_FIRST+1,
                       IARG_END);
        RTN_Close(rankRtn);
    }
    else if (RTN_Valid(fortRankRtn)){
        //INFO_PRINT("Found Fortran Rank Function!")
        RTN_Open(fortRankRtn);
        RTN_InsertCall(fortRankRtn, IPOINT_BEFORE, (AFUNPTR)Arg2Before,
                       IARG_FUNCARG_ENTRYPOINT_REFERENCE, 1,//the 2nd arg is rank
                       IARG_CALL_ORDER, CALL_ORDER_FIRST,
                       IARG_END);
        RTN_InsertCall(fortRankRtn, IPOINT_AFTER, (AFUNPTR)RankAfter,
                       IARG_CALL_ORDER, CALL_ORDER_FIRST+1,
                       IARG_END);
        RTN_Close(fortRankRtn);
    }
}

#endif // #ifndef __PIN_MPI_H__
/** @} */
