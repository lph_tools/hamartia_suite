// Copyright 2015, The University of Texas at Austin 
// All rights reserved.
// 
// THIS FILE IS PART OF THE HAMARTIA FAULT/ERROR ANALYSIS SUITE
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met: 
// 
// 1. Redistributions of source code must retain the above copyright
// notice, this list of conditions and the following disclaimer. 
// 
// 2. Redistributions in binary form must reproduce the above copyright
// notice, this list of conditions and the following disclaimer in the
// documentation and/or other materials provided with the distribution. 
// 
// 3. Neither the name of the copyright holder nor the names of its
// contributors may be used to endorse or promote products derived from
// this software without specific prior written permission. 
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
// FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
// COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
// BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
// ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

/**
 *  \file
 *  \brief Interface of transient-error injection
 *
 *  \version 1.0.0
 *  \author Chun-Kai Chang, University of Texas
 *
 *  \addtogroup error_profiler_injector
 *  @{
 */

#ifndef _TRANSIENT_ERROR_H
#define _TRANSIENT_ERROR_H

#include "pin_injector.h"

/**
 * Instruction-level instrumentation routine
 *
 * \param ins Instruction
 */
VOID TransientErrorInstruction(INS ins, VOID *v);

/**
 * Wrapper for InsertThenTemplate in "error_prof_inj.h" 
 * (Needed by "pin_cd.h")
 */
VOID InsertPreThen(INS& ins, const uint32_t& mem_r, const uint32_t& mem_w, REG* r_operands, REG* w_operands);

class TransientErrorInjector : public PinInjector {
    public:
    TransientErrorInjector(std::string error_model, std::string error_config, std::string detector_model, std::string detector_config) 
        : PinInjector(error_model, error_config, detector_model, detector_config) {}

    ~TransientErrorInjector() {}

    void Configure(uint64_t num_to_inject, const std::string& error_point);
};

#endif // #ifndef __TRANSIENT_ERROR_H__
/** @} */

