// Copyright 2015, The University of Texas at Austin 
// All rights reserved.
// 
// THIS FILE IS PART OF THE HAMARTIA FAULT/ERROR ANALYSIS SUITE
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met: 
// 
// 1. Redistributions of source code must retain the above copyright
// notice, this list of conditions and the following disclaimer. 
// 
// 2. Redistributions in binary form must reproduce the above copyright
// notice, this list of conditions and the following disclaimer in the
// documentation and/or other materials provided with the distribution. 
// 
// 3. Neither the name of the copyright holder nor the names of its
// contributors may be used to endorse or promote products derived from
// this software without specific prior written permission. 
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
// FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
// COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
// BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
// ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

/**
 * \file
 * \brief Two-State fast forwarding
 *
 *  \version 1.0.0
 *  \author Michael Sullivan, University of Texas
 *
 *  \addtogroup error_profiler_injector
 *  @{
 */

// FIXME: Currently broken for complex filtering cases?

#ifndef __FAST_FORWARD_H__
#define __FAST_FORWARD_H__

#include <iostream>
#include <fstream>
#include "pin.H"

/*
 * Uses a two-phase model, with
 *
 * (1) fast-forwarding
 * (2) injection
 * (3) detatch
 *
 */

/* ================================================================== */
// Global variables 
/* ================================================================== */

/// Fastforward disabled
#ifdef NO_FAST_FORWARD
BOOL disable_ff = true;
#else
BOOL disable_ff = false;
#endif
/// Currently fastforwarding
BOOL fast_forwarding = !disable_ff;   // true = fast-forwarding, false = injecting
/// Global instruction count (FF-only)
UINT64 gcount = 0;

/* ===================================================================== */
// Fast-Forward Period
/* ===================================================================== */

/**
 * Fastfoward instruction count (and if)
 *
 * \param bbl_size        BBL size (number of instructions)
 * \param tgt_instruction Target dynamic instruction count
 *
 * \return Reached near-injection point
 */
ADDRINT PIN_FAST_ANALYSIS_CALL ffif(ADDRINT bbl_size, ADDRINT tgt_instruction)
{
    // Adjust counts
    gcount += bbl_size;
    return gcount >= tgt_instruction;
}

/**
 * Fastforward disable (if-then)
 */
VOID PIN_FAST_ANALYSIS_CALL ffthen ()
{
    // turn off fast-forwarding
    fast_forwarding = false;

    // do not re-attach fast forwarding
    disable_ff = true;

    // re-instrument the program)
    PIN_RemoveInstrumentation();
}

/**
 * Fastforward instrumentation
 *
 * \param trace Pin Trace
 * \param vtgt_instruction Target instruction
 */
VOID TraceFF(TRACE trace, VOID *vtgt_instruction)
{
    // TODO: Check image

    // Get the target instruction
    ADDRINT tgt_instruction = *(ADDRINT*)vtgt_instruction;
    
    // Check if fast_forwarding is disabled
    if (disable_ff || tgt_instruction == 0)
        return;

    // Count every BB until the target instruction is reached
    for (BBL bbl = TRACE_BblHead(trace); BBL_Valid(bbl); bbl = BBL_Next(bbl))
    {
        // Insert a call to count the BBL
        BBL_InsertIfCall(bbl, IPOINT_BEFORE, (AFUNPTR)ffif, 
                         IARG_FAST_ANALYSIS_CALL,
                         IARG_ADDRINT, BBL_NumIns(bbl),
                         IARG_ADDRINT, tgt_instruction,
                         IARG_END);
        // Stop fast-forwarding if we've reached the target
        BBL_InsertThenCall(bbl, IPOINT_BEFORE, AFUNPTR(ffthen), 
                IARG_FAST_ANALYSIS_CALL,
                IARG_END);
    }
}


#endif // #ifndef __FAST_FORWARD_H__
/** @} */
