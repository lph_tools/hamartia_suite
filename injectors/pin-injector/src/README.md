Source description
------------

## Main
-  error_prof_inj.h

      Contains declaration of global variables and common 
      functions.

-  error_prof_inj.cpp

      Contains main function and common functions for all
      Pin tools.

## Profiler
-  profile.h

      Instrumentation routines for profiling.

## Injectors
-  pin_injector

      Implementation of a derived injector class using Pin's API.
      Serves as a base Pin injector class.

-  transient_error

      Implementation of a transient error injector.
      Derived class of PinInjector.

-  timing_error

      Implementation of a timing error injector.
      Derived class of PinInjector.

-  injector_factory

      Factory method for creating Pin injectors and registering
      associated instrumentation code.

## Special Injection Mode
-  pin_mpi.h

      Support for injecting MPI programs (C/C++ and Fortran only). 

-  pin_cd.h

      Support for injecting programs wrapped by Containment Domains. 

## Optimization
-  fast_forward.h

      Fast-forwarding with trace-level instrumentation (currently not used).

## Others
-  logging.h

      Methods related to output logging. 

-  oclasses

      The XED2 OPCODE classes which are used to classify instructions. 

-  code_regions.h

      Support for injecting code regions wrapped by special markers. 

-  helpers/instruction.h

      Instruction helpers (e.g., set instruction data from Pin state)
      
-  helpers/operand.h
      
      Helpers that update or get operands from Pin state 

-  util.h

      Utility functions.


