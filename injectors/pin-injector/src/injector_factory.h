// Copyright 2015, The University of Texas at Austin 
// All rights reserved.
// 
// THIS FILE IS PART OF THE HAMARTIA FAULT/ERROR ANALYSIS SUITE
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met: 
// 
// 1. Redistributions of source code must retain the above copyright
// notice, this list of conditions and the following disclaimer. 
// 
// 2. Redistributions in binary form must reproduce the above copyright
// notice, this list of conditions and the following disclaimer in the
// documentation and/or other materials provided with the distribution. 
// 
// 3. Neither the name of the copyright holder nor the names of its
// contributors may be used to endorse or promote products derived from
// this software without specific prior written permission. 
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
// FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
// COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
// BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
// ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

/**
 *  \file
 *  \brief Pin-injector factory
 *
 *  \version 1.0.0
 *  \author Chun-Kai Chang, University of Texas
 *
 *  \note this avoids modification to the main function when new injectors are added
 *  \addtogroup error_profiler_injector
 *  @{
 */

#ifndef _INJECTOR_FACTORY_H
#define _INJECTOR_FACTORY_H

#include <string>
#include "pin_injector.h"
#include "transient_error.h"
#include "timing_error.h"
#include "mem_error.h"
#include "utils.h"


class InjectorFactory {
    public:
    typedef enum {
        IT_Transient,
        IT_Timing,
        IT_Memory,
        IT_END
    } InjectorType;    
 
    /* Create injector object based on error model */
    static PinInjector* Create(const std::string& error_model, const std::string& error_config,
                               const std::string& detector_model, const std::string& detector_config) 
    {

        InjectorType type = GetInjectorType(error_model);
        switch(type) {
            case IT_Transient:
                INFO_PRINT("Create transient error injector...")
                return new TransientErrorInjector(error_model, error_config, detector_model, detector_config);
            case IT_Timing:
                INFO_PRINT("Create timing error injector...")
                return new TimingErrorInjector(error_model, error_config, detector_model, detector_config);
            case IT_Memory:
                INFO_PRINT("Create memory error injector...")
                return new MemoryErrorInjector(error_model, error_config, detector_model, detector_config);
            default:
                FATAL_ERROR("Injector creation failed due to invalid injector type...")
        }    
        throw "Invalid injector type";
    }
    
    /* Register instrumentation routine based on error model */
    static void RegisterInstrument(const std::string& error_model) 
    {
        InjectorType type = GetInjectorType(error_model);
        switch(type) {
            case IT_Transient:
                INS_AddInstrumentFunction(TransientErrorInstruction, 0);
                break;
            case IT_Timing:
                INS_AddInstrumentFunction(TimingErrorInstruction, 0);
                break;
            case IT_Memory:
                INS_AddInstrumentFunction(MemoryErrorInstruction, 0);
                break;
            default:
                FATAL_ERROR("Instrumentation registration failed due to invalid injector type...")
        }    
    }    

    /* Register fini routine based on error model */
    static void RegisterFini(const std::string& error_model) 
    {
        InjectorType type = GetInjectorType(error_model);
        if (type == IT_Memory) {
            PIN_AddFiniFunction(MemoryErrorFini, 0);
        } else {
            // do nothing
            // other models usually call the fini routine when injection finishes
        }    
    }    

    private:
    /* Query injector type based on error_model string */
    static InjectorType GetInjectorType(const std::string& error_model) 
    {
        if (utils::stringStartsWith(error_model, "TIMING")) {
            return IT_Timing;
        } else if (utils::stringStartsWith(error_model, "MEM")) {
            return IT_Memory;
        } else {
            return IT_Transient;
        }
    }
   
};

#endif // #ifndef __INJECTOR_FACTORY_H__
/** @} */
