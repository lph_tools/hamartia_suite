// Copyright 2015, The University of Texas at Austin 
// All rights reserved.
// 
// THIS FILE IS PART OF THE HAMARTIA FAULT/ERROR ANALYSIS SUITE
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met: 
// 
// 1. Redistributions of source code must retain the above copyright
// notice, this list of conditions and the following disclaimer. 
// 
// 2. Redistributions in binary form must reproduce the above copyright
// notice, this list of conditions and the following disclaimer in the
// documentation and/or other materials provided with the distribution. 
// 
// 3. Neither the name of the copyright holder nor the names of its
// contributors may be used to endorse or promote products derived from
// this software without specific prior written permission. 
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
// FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
// COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
// BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
// ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

/**
 * \file
 * \brief CD-related profiler and error injector
 *
 *  \version 1.0.0
 *  \author Chun-Kai Chang, University of Texas
 *
 *  \addtogroup error_profiler_injector
 *  @{
 */


#ifndef __PIN_CD_H__
#define __PIN_CD_H__

#include <iostream>
#include <fstream>
#include <string.h>
#include "error_prof_inj.h"
#include "transient_error.h"
#include "code_filter.h"
#include "oclasses.h"
#include "helpers/instruction.h"
//debug
#include <hamartia_api/debug.h>

/* Command-line switches */
/// CD program switch (default: non-CD program)
KNOB<BOOL> KnobIsCD(KNOB_MODE_WRITEONCE, "pintool",
        "cd", "0", "CD-wrapped application");
/// CD mode (default: inject CD by global count)
KNOB<UINT32> KnobCDMode(KNOB_MODE_WRITEONCE, "pintool",
        "cdmode", "0", "specify the cd injection mode");
/// Target CD Name(valid only if IsCD is true)
KNOB<string> KnobCDName(KNOB_MODE_WRITEONCE, "pintool",
        "cdname", "root", "specify the target cd name to inject (valid only for mode 1)");


/*
 * Uses a four states FSM, with
 *
 * (1) fast-forwarding
 * (2) injection
 * (3) check rollback
 * (4) detach
 * Note that this version requires modification to CD library to work
 */
typedef enum{
  GLOBAL,
  BYNAME
} CDInjectorMode;

typedef enum{
  CD_FF,//Looking for target Marker
  CD_INJ,
  CD_CHK_ROLLBACK,
  CD_DETACH
} CDInjectorPhase;
/* ================================================================== */
// Global variables 
/* ================================================================== */
CDInjectorMode CDMode;
CDInjectorPhase CDPinState = CD_FF;

BOOL is_cd = false;
/* ================================================================== */
// ByName Mode
/* ================================================================== */
/// Target cd name
std::string tgtcd_name;

/* ================================================================== */
// Global Mode
/* ================================================================== */
/// Target cd id (used to identfiy a specific CD) 
typedef struct{
  UINT32 level;
  UINT32 rankInLevel;
  UINT32 seq;
} CDID_t;

CDID_t CDID;
/// CD id initialization flag
/// For error injected before root CD.
/// If this is the case, then Pin detach right after first injection.
BOOL cdid_initialized = false;

BOOL cdid_update_enable = true;
/// Local target instruction count
UINT32 delta = 0;

/**
 * CD mode Initialization
 */
VOID CDInit()
{
    if(KnobCDMode.Value()==0){
        INFO_PRINT("CD Global Mode")
        CDMode = GLOBAL;
    }
    else{
        INFO_PRINT("CD By name Mode")
        CDMode = BYNAME;
    }
    tgtcd_name = KnobCDName.Value();
    if(CDMode == GLOBAL)
        CDPinState = CD_INJ;

}    

/* =====================================================================
 * CD instruction-level instrumentation
 * ===================================================================== */
/**
 * CD Global Mode 
 */
ADDRINT insif_incr2()
{
    //update local instruction offset
    ++delta;
    return ++ricount >= ticount;
}
static VOID CDthen(CONTEXT * state, UINT32 opcode, ADDRINT addr,
                 PIN_REGISTER *w1, PIN_REGISTER *w2, PIN_REGISTER *w3, ADDRINT rflags)
{
    // All methods should be inlined!
    if(detach_switch) return;
    INFO_PRINT("CD Post-Inst routine...")
    if (injector == NULL) return;

    // Find proper sub-word to target, for packed instructions
    hamartia_api::InstructionData &inst_data = g_inst_data_map[addr];
    // FIXME: remove? better address?
    //inst_data.op_addr = addr;

    // Format function values
    PIN_REGISTER * _w_operands[MAX_W_OPERANDS] = {w1, w2, w3};

    if (injector->Phase() == hamartia_api::PHASE_EVAL) {
        // Evaluation phase
        // Populate context from instruction
        injector->PostCurrentStateToContext(inst_data, const_cast<const PIN_REGISTER**>(_w_operands), write_ea, rflags);
        injector->RollbackState(state);
    } else {

        //FIXME: Is this the correct injection start time?
        gettimeofday(&pin_inj, NULL);
        // Injection phase
        if (injector->Phase() == hamartia_api::PHASE_PRE_INJECT) {
            injector->InjectionStateToContext(const_cast<const PIN_REGISTER**>(_w_operands), write_ea, rflags);
        }

        // Inject error
        bool success = injector->InjectError();
        INFO_PRINT("INJ " << success << " at filtered instr # "<< ticount)

        if (success) {
            // Rollback state
            injector->OldOutputContextToState(state);
            // Update to injection state
            injector->InjectionContextToState(state, rflags);
            // Commit context
            injector->CommitContext();

            // disable cdid update because target instr is reached
            cdid_update_enable = false;
            INFO_PRINT("Disable CDID update: "<<cdid_update_enable)

            // Write out log
            //TODO: CD version should not detach directly (Have to check how many errors to record) 
            if (injector->Phase() == hamartia_api::PHASE_INJECTED || (CDMode==GLOBAL && !cdid_initialized)) {//finish injection or target inst is outside any CD
                INFO_PRINT("Finish!")
                CDPinState = CD_DETACH;
                InjectFini();//write out log and delete injector
                //CODECACHE_FlushCache();
                PIN_RemoveInstrumentation();
            } else{
                //once we inject the first error, we know the specific CD
                //we can then use function instrumentation to speed up following injections
                CDPinState = CD_CHK_ROLLBACK;
                //CDMode = BYNAME;
                INFO_PRINT("CD Injector Checking rollback...")
                INFO_PRINT("Target CDID..."<<CDID.level<<","<<CDID.rankInLevel<<","<<CDID.seq)
                // flush the code cache (re-instrument the program)
                //CODECACHE_FlushCache();
                PIN_RemoveInstrumentation();
            }
            INFO_PRINT("Return")
        } else {
            injector->RollbackState(state);
        }
    }

    // Executed context
    PIN_ExecuteAt(state);
}


VOID CDInjectInstruction(INS ins, VOID *v)
{
    //Disable instrumentation if true
    if(detach_switch)
        return;
  
    if(is_mpi && !disable_getrank)
        return;
    
    // Check injector state
    if(CDPinState != CD_INJ)
        return;
    
    // Find resident image (and ignore if in ignore_imgs) 
    if (isInIgnoredImg(ins, ignore_imgs, images)) {
        return;
    }    

    // FIXME: does CD injection need to integrate with region injection?
    // Modify inject_toggle when we see code region headers
    if (inject_regions && isCodeRegionHeader(ins, inject_toggle)) {
        return;
    }    

    if (inject_toggle || !inject_regions) {
        // Filter out the operations from the desired classes 
        if (InsInOperationClasses(ins, oclasses, inject_width)) {
            // Index
            uint32_t mem_r = 0, mem_w = 0;
            // Registers
            REG r_operands[MAX_R_OPERANDS] = {REG_INST_G0, REG_INST_G0, REG_INST_G0};
            REG w_operands[MAX_W_OPERANDS] = {REG_INST_G0, REG_INST_G0, REG_INST_G0};
            // Extract information for error injection
            pin_instruction_helper::GetInstOperandInfo(ins, g_inst_data_map, mem_r, mem_w, r_operands, w_operands);

            // Insert a call to register the instruction
            if(CDMode == BYNAME){ 
                INS_InsertIfCall(ins, IPOINT_BEFORE, (AFUNPTR)insif_incr, IARG_END);
            }
            else if(CDMode == GLOBAL){
                if(cdid_update_enable)//target CD is still unknown, keep incrementing local count 
                    INS_InsertIfCall(ins, IPOINT_BEFORE, (AFUNPTR)insif_incr2, IARG_END);
                else
                    INS_InsertIfCall(ins, IPOINT_BEFORE, (AFUNPTR)insif_incr, IARG_END);
            }
           
            InsertPreThen(ins, mem_r, mem_w, r_operands, w_operands);

            // Insert a call to register the instruction
            INS_InsertIfCall(ins, IPOINT_AFTER, (AFUNPTR)insif, IARG_END);
            // Inject an error
            INS_InsertThenCall(ins, IPOINT_AFTER, (AFUNPTR)CDthen, \
                               IARG_CONTEXT, \
                               IARG_UINT32, INS_Opcode(ins), \
                               IARG_INST_PTR, \
                               IARG_REG_REFERENCE, w_operands[0], \
                               IARG_REG_REFERENCE, w_operands[1], \
                               IARG_REG_REFERENCE, w_operands[2], \
                               IARG_REG_REFERENCE, REG_RFLAGS, \
                               IARG_END);
        }
    }
}

/* =====================================================================
 * CD trace-level instrumentation
 * ===================================================================== */

/**
 * Fastfoward to target cd (and if) - By Name Mode Only
 *
 * \param name Target cd name
 *
 * \return Reached near-injection point
 */

ADDRINT PIN_FAST_ANALYSIS_CALL cdffifname(CHAR* name)
{
    std::string name_str = name;
    //INFO_PRINT("CD Name= "<< name_str << " Target CD Name= " << tgtcd_name)
    return (name_str == tgtcd_name);
}

/**
 * Fastforward disable (if-then) - By Name Mode Only
 */
VOID PIN_FAST_ANALYSIS_CALL cdffthen ()
{
    INFO_PRINT("Reach the target CD marker routine...")
    // enable injection
    CDPinState = CD_INJ;
    // reset running instr count
    ricount = 0;
    PIN_RemoveInstrumentation();

}
/**
 * Test CD id
 * \param name Query cd name string
 * \param lv Query cd level
 * \param rank Query cd rank in level
 * \param seq Query cd sequential id
 * \return CD id matched 
 */
ADDRINT PIN_FAST_ANALYSIS_CALL ifcdid (CHAR* name, UINT32 lv, UINT32 rank, UINT32 seq)
{
    INFO_PRINT("Compare CDID..."<<CDID.level<<","<<CDID.rankInLevel<<","<<CDID.seq<<" vs " << lv <<","<<rank<<","<<seq)
    return (CDID.level == lv && CDID.rankInLevel == rank && CDID.seq == seq);
}
/**
 * Prepare for next injection
 */
VOID PIN_FAST_ANALYSIS_CALL cdprepthen ()
{
    INFO_PRINT("Prepare for next injection...")
    // enable injection
    CDPinState = CD_INJ;
    // reset running instr count
    ricount = 0;
    // update target instruction count to local instr offset
    ticount = delta; 
    // flush the code cache (re-instrument the program)
    //CODECACHE_FlushCache();
    PIN_RemoveInstrumentation();
}
/**
 * Rollback(if-then)
 */
VOID PIN_FAST_ANALYSIS_CALL cdrollbackthen ()
{
    INFO_PRINT("Found rollback. Start CDID matching process...")
    CDPinState = CD_FF;
    //CODECACHE_FlushCache();    
    PIN_RemoveInstrumentation();
}

/**
 * Check CD id update switch
 * This switch is turned off after 1st injection
 */
ADDRINT PIN_FAST_ANALYSIS_CALL cdifupdate()
{
    return cdid_update_enable;
}
/**
 * Update CDID as we go through CDs if cdid update is enabled (if-then)
 */
VOID PIN_FAST_ANALYSIS_CALL updateCDID (CHAR* name, UINT32 lv, UINT32 rank, UINT32 seq)
{
    cdid_initialized = true;
    CDID.level = lv;
    CDID.rankInLevel = rank;
    CDID.seq = seq;
    INFO_PRINT("Update target CD id to (" << lv <<", " << rank <<", " << seq <<")")
    PIN_RemoveInstrumentation();
}

/**
 * CD trace-level instrumentation
 *
 * This instrumentation function acts as a switch among multiple CD injection modes.
 * Each mode has its own FSM to control injection flow. Please refer to documentation for detail.
 * \param trace Pin Trace
 */
VOID TraceCDFSM(TRACE trace, VOID *v)
{
    if(detach_switch)
        return;
    if(!is_cd)
        return;
    if(!disable_getrank)
        return;
    //RTN rtn = TRACE_Rtn(trace);
    if(CDMode == GLOBAL){
        for (BBL bbl = TRACE_BblHead(trace); BBL_Valid(bbl); bbl = BBL_Next(bbl)) {
            INS ins = BBL_InsHead(bbl);
            RTN rtn = INS_Rtn(ins);
            if(RTN_Valid(rtn)){
                SYM sym = RTN_Sym(rtn);
                if(SYM_Valid(sym)){
                    string undFuncName = PIN_UndecorateSymbolName(SYM_Name(sym), UNDECORATION_NAME_ONLY);
   
                    if(undFuncName == "HamartiaRollback"){
                        //INFO_PRINT("A rollback marker...CD state: " << CDPinState)
                        if(CDPinState == CD_CHK_ROLLBACK){
                            //RTN_Open(rtn); 
                            //INS ins = RTN_InsHead(rtn);
                            /*
                            //Version 1: Check cd id before switching state
                            // Check CD id
                            INS_InsertIfCall(ins, IPOINT_BEFORE, (AFUNPTR)ifcdid, 
                                             IARG_FAST_ANALYSIS_CALL,
                                             IARG_FUNCARG_ENTRYPOINT_VALUE, 0,//the 1st arg is CD name string
                                             IARG_FUNCARG_ENTRYPOINT_VALUE, 1,//the 2nd arg is CD level
                                             IARG_FUNCARG_ENTRYPOINT_VALUE, 2,//the 3rd arg is CD rank in level
                                             IARG_FUNCARG_ENTRYPOINT_VALUE, 3,//the 4th arg is CD sequential ID
                                             IARG_END);
                            // Reset state to CD_FF
                            INS_InsertThenCall(ins, IPOINT_BEFORE, (AFUNPTR)cdrollbackthen, 
                                               IARG_FAST_ANALYSIS_CALL,
                                               IARG_END);
                            */
                            //Version 2: Switching state regardless of which CD rollbacks
                            INS_InsertCall(ins, IPOINT_BEFORE, (AFUNPTR)cdrollbackthen, 
                                           IARG_FAST_ANALYSIS_CALL,
                                           IARG_END);
                            //RTN_Close(rtn);
                            INFO_PRINT("A rollback marker...CD state: Check")
                        }
                        else{
                            //do nothing
                        }
                    }
                    if(undFuncName == "HamartiaMarker"){
                        //INFO_PRINT("A begin marker...CD state: " << CDPinState)
                        //RTN_Open(rtn);   
                        if(CDPinState == CD_FF){
                            //INS ins = RTN_InsHead(rtn);
                            // Check CD id
                            INS_InsertIfCall(ins, IPOINT_BEFORE, (AFUNPTR)ifcdid, 
                                             IARG_FAST_ANALYSIS_CALL,
                                             IARG_FUNCARG_ENTRYPOINT_VALUE, 0,//the 1st arg is CD name string
                                             IARG_FUNCARG_ENTRYPOINT_VALUE, 1,//the 2nd arg is CD level
                                             IARG_FUNCARG_ENTRYPOINT_VALUE, 2,//the 3rd arg is CD rank in level
                                             IARG_FUNCARG_ENTRYPOINT_VALUE, 3,//the 4th arg is CD sequential ID
                                             IARG_END);
                            // Prepare for next injection
                            INS_InsertThenCall(ins, IPOINT_BEFORE, (AFUNPTR)cdprepthen, 
                                               IARG_FAST_ANALYSIS_CALL,
                                               IARG_END);
                            INFO_PRINT("A begin marker...CD state: FF")
                        }
                        else if(CDPinState == CD_INJ){
                            //INS ins = RTN_InsHead(rtn);
                            // Check CD id update enable
                            INS_InsertIfCall(ins, IPOINT_BEFORE, (AFUNPTR)cdifupdate, 
                                             IARG_FAST_ANALYSIS_CALL,
                                             IARG_END);
                            INS_InsertThenCall(ins, IPOINT_BEFORE, (AFUNPTR)updateCDID, 
                                               IARG_FAST_ANALYSIS_CALL,
                                               IARG_FUNCARG_ENTRYPOINT_VALUE, 0,//the 1st arg is CD name string
                                               IARG_FUNCARG_ENTRYPOINT_VALUE, 1,//the 2nd arg is CD level
                                               IARG_FUNCARG_ENTRYPOINT_VALUE, 2,//the 3rd arg is CD rank in level
                                               IARG_FUNCARG_ENTRYPOINT_VALUE, 3,//the 4th arg is CD sequential ID
                                               IARG_END);
                            //Reset local target offset
                            delta = 0;
                            INFO_PRINT("A begin marker...CD state: INJ")
                        }
                        else if(CDPinState == CD_CHK_ROLLBACK){
                            //do nothing
                        }
                        else{//CD_DETACH
                            //PIN_Detach(); /*Ideally we should detach, but this API results in SDC*/
                            INFO_PRINT("Disable Instrumentation...")
                            detach_switch = true;
                            PIN_RemoveInstrumentation();
                            return;
                        }
                        //RTN_Close(rtn);
                    }
                }
            }
        }
    } 
    if(CDMode == BYNAME){
        for (BBL bbl = TRACE_BblHead(trace); BBL_Valid(bbl); bbl = BBL_Next(bbl)) {
            INS ins = BBL_InsHead(bbl);
            RTN rtn = INS_Rtn(ins);
            if(RTN_Valid(rtn)){
                SYM sym = RTN_Sym(rtn);
                if(SYM_Valid(sym)){
                    string undFuncName = PIN_UndecorateSymbolName(SYM_Name(sym), UNDECORATION_NAME_ONLY);
            
                    if(undFuncName == "HamartiaRollback"){
                        INFO_PRINT("A rollback marker...CD state: " << CDPinState)
                        if(CDPinState == CD_CHK_ROLLBACK){
                            //Switching state regardless of which CD rollbacks
                            INS_InsertCall(ins, IPOINT_BEFORE, (AFUNPTR)cdrollbackthen, 
                                           IARG_FAST_ANALYSIS_CALL,
                                           IARG_END);
            
                            INFO_PRINT("A rollback marker...CD state: Check")
                        }
                        else{
                            //do nothing
                        }
                    }
                    if(undFuncName == "HamartiaMarker"){
                        if(CDPinState == CD_FF){
                            //locate CD by CD name 
                            INS_InsertIfCall(ins, IPOINT_BEFORE, (AFUNPTR)cdffifname, 
                                             IARG_FAST_ANALYSIS_CALL,
                                             IARG_FUNCARG_ENTRYPOINT_VALUE, 0,//the 1st arg is CD name string
                                             IARG_END);
                            // Stop fast-forwarding if we've reached the target
                            INS_InsertThenCall(ins, IPOINT_BEFORE, (AFUNPTR)cdffthen, 
                                               IARG_FAST_ANALYSIS_CALL,
                                               IARG_END);
                            INFO_PRINT("A begin marker...CD state: FF")
                        }
                        else if(CDPinState == CD_INJ){
                            //do nothing
                        }
                        else if(CDPinState == CD_CHK_ROLLBACK){
                            //do nothing
                        }
                        else{//CD_DETACH
                            //PIN_Detach(); /*Ideally we should detach, but this API results in SDC*/
                            INFO_PRINT("Disable Instrumentation...")
                            detach_switch = true;
                            PIN_RemoveInstrumentation();
                            return;
                        }
                    }
                }
            }
        }
    } 

}
/**
 * Note: this routine level instrumentaion does not work. I just left it for reference.
 * Basically, image- and routine-level instrumentaion are so called "ahead-of-time instrumentation,"
 * which means instrumentation is done when the relevant image is first loaded. We have no way to
 * re-instrument these types of instrumentaion.
 */ 
/*
VOID RoutineCDFSM(RTN rtn, VOID *v){
    if(!is_cd)
	return;
    if(!disable_getrank)
    	return;
    SYM sym = RTN_Sym(rtn);
    if(SYM_Valid(sym)){
    	string undFuncName = PIN_UndecorateSymbolName(SYM_Name(sym), UNDECORATION_NAME_ONLY);
	if(CDMode == BYNAME){
	    if(undFuncName == "HamartiaMarker"){
   	        //std::cout << "Meet a CD Begin\n";
                if (RTN_Valid(rtn)){
            	RTN_Open(rtn);
	        
	    	if(CDPinState == CD_FF){
    	    	    INS ins = RTN_InsHead(rtn);
    	    	    // Check CD name string
            	    INS_InsertIfCall(ins, IPOINT_BEFORE, (AFUNPTR)cdffifname, 
            	                 IARG_FAST_ANALYSIS_CALL,
                            	 IARG_FUNCARG_ENTRYPOINT_VALUE, 0,//the 1st arg is CD name string
            	                 IARG_END);
                	    //INS_InsertIfCall(ins, IPOINT_BEFORE, (AFUNPTR)ifcdid, 
                	    //             IARG_FAST_ANALYSIS_CALL,
                            //    	 IARG_FUNCARG_ENTRYPOINT_VALUE, 1,//the 2nd arg is CD level
                            //    	 IARG_FUNCARG_ENTRYPOINT_VALUE, 2,//the 3rd arg is CD rank in level
                            //    	 IARG_FUNCARG_ENTRYPOINT_VALUE, 3,//the 4th arg is CD sequential ID
                	    //             IARG_END);
            	    // Stop fast-forwarding if we've reached the target
            	    INS_InsertThenCall(ins, IPOINT_BEFORE, (AFUNPTR)cdffthen, 
            	        IARG_FAST_ANALYSIS_CALL,
            	        IARG_END);
            	        //INS_InsertThenCall(ins, IPOINT_BEFORE, (AFUNPTR)cdprepthen, 
            	        //    IARG_FAST_ANALYSIS_CALL,
            	        //    IARG_END);
	    	}
	    	else if(CDPinState == CD_INJ){
	    	    // Meet next-level CD
	    	    // do nothing
	    	}
	    	else if(CDPinState == CD_CHK_ROLLBACK){
	    	    // do nothing
	    	}		
	    	else{//CD_DETACH
	    	    // do nothing
	    	}
	         
            	RTN_Close(rtn);
                }
	    }
	    
	    else if(undFuncName == "HamartiaRollback"){
	        if(CDPinState == CD_CHK_ROLLBACK){
                    if (RTN_Valid(rtn)){
            	    RTN_Open(rtn); 
    	    	    INS ins = RTN_InsHead(rtn);
	    	    // Check CD name string
            	    INS_InsertIfCall(ins, IPOINT_BEFORE, (AFUNPTR)cdffifname, 
            	                 IARG_FAST_ANALYSIS_CALL,
                            	 IARG_FUNCARG_ENTRYPOINT_VALUE, 0,//the 1st arg is CD name string
            	                 IARG_END);
                	    //INS_InsertIfCall(ins, IPOINT_BEFORE, (AFUNPTR)ifcdid, 
                	    //             IARG_FAST_ANALYSIS_CALL,
                            //    	 IARG_FUNCARG_ENTRYPOINT_VALUE, 1,//the 2nd arg is CD level
                            //    	 IARG_FUNCARG_ENTRYPOINT_VALUE, 2,//the 3rd arg is CD rank in level
                            //    	 IARG_FUNCARG_ENTRYPOINT_VALUE, 3,//the 4th arg is CD sequential ID
                	    //             IARG_END);
            	    // Reset state to CD_FF
            	    INS_InsertThenCall(ins, IPOINT_BEFORE, (AFUNPTR)cdrollbackthen, 
            	        IARG_FAST_ANALYSIS_CALL,
            	        IARG_END);
            	    RTN_Close(rtn);
	
	    	    }
	        }	    
	    }
	    //if(undFuncName == "cd::internal::CD::Begin"){
	    //}
	    //
	    //if(undFuncName == "cd::internal::CD::Complete"){
            //    //std::cout << "Meet a CD Complete\n";   
	    //}
	    //if(undFuncName == "cd::internal::CD::Destroy"){
            //    //std::cout << "Meet a CD Destroy\n";       
            //}
	}
	
	else if(CDMode == GLOBAL){
  	        //if(undFuncName == "HamartiaRollback"){
  	        if(RTN_Name(rtn) == "HamartiaRollback"){
    	            INFO_PRINT("A rollback marker...")
            	    //if (RTN_Valid(rtn)){
	            if(CDPinState == CD_CHK_ROLLBACK){
                	    RTN_Open(rtn); 
    	        	    INS ins = RTN_InsHead(rtn);
	        	    // Check CD id
                	    INS_InsertIfCall(ins, IPOINT_BEFORE, (AFUNPTR)ifcdid, 
                	                 IARG_FAST_ANALYSIS_CALL,
                            	 	 IARG_FUNCARG_ENTRYPOINT_VALUE, 0,//the 1st arg is CD name string
                                	 IARG_FUNCARG_ENTRYPOINT_VALUE, 1,//the 2nd arg is CD level
                                	 IARG_FUNCARG_ENTRYPOINT_VALUE, 2,//the 3rd arg is CD rank in level
                                	 IARG_FUNCARG_ENTRYPOINT_VALUE, 3,//the 4th arg is CD sequential ID
                	                 IARG_END);
                	    // Reset state to CD_FF
                	    INS_InsertThenCall(ins, IPOINT_BEFORE, (AFUNPTR)cdrollbackthen, 
                	        IARG_FAST_ANALYSIS_CALL,
                	        IARG_END);
                	    RTN_Close(rtn);
	            }
		    else{
    	            	//do nothing
		    }
		    //}	    
	        }
	        //if(undFuncName == "HamartiaMarker"){
	        if(RTN_Name(rtn) == "HamartiaMarker"){
            	    //if (RTN_Valid(rtn)){
            	    RTN_Open(rtn);   
	    	    if(CDPinState == CD_FF){
    	    	        INS ins = RTN_InsHead(rtn);
    	    	        // Check CD id
            	        INS_InsertIfCall(ins, IPOINT_BEFORE, (AFUNPTR)ifcdid, 
            	                     IARG_FAST_ANALYSIS_CALL,
                            	 	 IARG_FUNCARG_ENTRYPOINT_VALUE, 0,//the 1st arg is CD name string
                                	 IARG_FUNCARG_ENTRYPOINT_VALUE, 1,//the 2nd arg is CD level
                                	 IARG_FUNCARG_ENTRYPOINT_VALUE, 2,//the 3rd arg is CD rank in level
                                	 IARG_FUNCARG_ENTRYPOINT_VALUE, 3,//the 4th arg is CD sequential ID
            	                     IARG_END);
            	        // Prepare for next injection
            	        INS_InsertThenCall(ins, IPOINT_BEFORE, (AFUNPTR)cdprepthen, 
            	            IARG_FAST_ANALYSIS_CALL,
            	            IARG_END);
	    	    }
	    	    else if(CDPinState == CD_INJ){
	   	        INS ins = RTN_InsHead(rtn);
    	    	        // Check CD id update enable
            	        INS_InsertIfCall(ins, IPOINT_BEFORE, (AFUNPTR)cdifupdate, 
            	                     IARG_FAST_ANALYSIS_CALL,
            	                     IARG_END);
            	        INS_InsertThenCall(ins, IPOINT_BEFORE, (AFUNPTR)updateCDID, 
            	                     IARG_FAST_ANALYSIS_CALL,
                            	 	 IARG_FUNCARG_ENTRYPOINT_VALUE, 0,//the 1st arg is CD name string
                                	 IARG_FUNCARG_ENTRYPOINT_VALUE, 1,//the 2nd arg is CD level
                                	 IARG_FUNCARG_ENTRYPOINT_VALUE, 2,//the 3rd arg is CD rank in level
                                	 IARG_FUNCARG_ENTRYPOINT_VALUE, 3,//the 4th arg is CD sequential ID
            	                     IARG_END);
			//Reset local target offset
			delta = 0;			
		    }
		    else{
			//do nothing
		    }		
            	    RTN_Close(rtn);
		   // }
 	        }
	    
	}
    }	
}
*/
#endif // #ifndef __PIN_CD_H__
/** @} */
