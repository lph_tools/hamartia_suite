// Copyright 2015, The University of Texas at Austin 
// All rights reserved.
// 
// THIS FILE IS PART OF THE HAMARTIA FAULT/ERROR ANALYSIS SUITE
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met: 
// 
// 1. Redistributions of source code must retain the above copyright
// notice, this list of conditions and the following disclaimer. 
// 
// 2. Redistributions in binary form must reproduce the above copyright
// notice, this list of conditions and the following disclaimer in the
// documentation and/or other materials provided with the distribution. 
// 
// 3. Neither the name of the copyright holder nor the names of its
// contributors may be used to endorse or promote products derived from
// this software without specific prior written permission. 
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
// FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
// COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
// BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
// ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

/**
 *  \file
 *  \brief Interface of memory error injection
 *
 *  \version 1.1.0
 *  \author Sangkug Lym, University of Texas
 *  \author Chun-Kai Chang, University of Texas
 *
 *  \addtogroup error_profiler_injector
 *  @{
 */

#include <hamartia_api/error_manager.h>
#include <hamartia_api/utils/error.h>

#include "mem_error.h"
#include "error_prof_inj.h"
#include "code_filter.h"
#include "oclasses.h"
#include "helpers/instruction.h"
#include "helpers/operand.h"

/* =====================================================================
 * Global Variables
 * ===================================================================== */
static bool is_faulty_read = false;


/* =====================================================================
 * Pin Routines
 * ===================================================================== */
static VOID pre_then(UINT32 opcode, ADDRINT addr, 
                     PIN_REGISTER *r1, PIN_REGISTER *r2, PIN_REGISTER *r3, 
                     const PIN_REGISTER *w1, const PIN_REGISTER *w2, const PIN_REGISTER *w3,
                     ADDRINT rflags, ADDRINT mwrite_ea, ADDRINT mread_ea1, ADDRINT mread_ea2)
{
    if(detach_switch) return;
    INFO_PRINT("Pre-Inst routine...")

    // Disable instrumentation if the injector no longer exists
    if (injector == nullptr) {
        //INFO_PRINT("Detach!")
        //PIN_Detach(); /*Ideally we should detach, but this API results in SDC*/
        INFO_PRINT("Disable Instrumentation...")
        detach_switch = true;
        //Jit new instrumentation code
        PIN_RemoveInstrumentation();
        return;
    }

    hamartia_api::InstructionData &inst_data = g_inst_data_map[addr];

    PIN_REGISTER * _r_operands[MAX_R_OPERANDS] = {r1, r2, r3}; 
    const PIN_REGISTER * _w_operands[MAX_W_OPERANDS] = {w1, w2, w3}; 
    ADDRINT _addrs[2] = {mread_ea1, mread_ea2};

    MemoryErrorInjector* mem_injector = static_cast<MemoryErrorInjector*>(injector);
    if (!mem_injector) {
        FATAL_ERROR("Injector type casting failed...")
    }    

    if (injector->Phase() == hamartia_api::PHASE_EVAL) {
        uint8_t rd_size = inst_data.width / 8; // convert to bytes

        // Initialize fault range if it does not exist yet
        if (!mem_injector->HasFaultRange()) {
            mem_injector->SetFaultRange(mread_ea1, rd_size);       
        }    

        if (mem_injector->IsFaultyRead(mread_ea1, mread_ea2, rd_size)) { 
            is_faulty_read = true; // this also controls the *then* routine
            // Populate inst_data with error-free input
            mem_injector->PreCurrentStateToContext(inst_data, const_cast<const PIN_REGISTER**>(_r_operands), 
                                                        _addrs, _w_operands, mwrite_ea);
        }    
    } else if (injector->Phase() == hamartia_api::PHASE_INJECT) {
        if (is_faulty_read) { 

            gettimeofday(&pin_inj, NULL);

            bool success = mem_injector->PreInjectError();
            if (success) {
                // write corrupted data to memory
                injector->PreInjectionContextToState(_r_operands);
                INFO_PRINT("INJ " << success << " at filtered instr # "<< ticount)
            }
        } 
    }

    // Register the write EA
    write_ea = mwrite_ea;
}

static VOID then(CONTEXT * state, UINT32 opcode, ADDRINT addr,
                 PIN_REGISTER *w1, PIN_REGISTER *w2, PIN_REGISTER *w3, ADDRINT &rflags)
{

    if(detach_switch) return;
    // All methods should be inlined!
    INFO_PRINT("Post-Inst routine...")

    if (injector == nullptr) return;

    // Find proper sub-word to target, for packed instructions
    hamartia_api::InstructionData &inst_data = g_inst_data_map[addr];

    // Format function values
    PIN_REGISTER * _w_operands[MAX_W_OPERANDS] = {w1, w2, w3};

    if (is_faulty_read) { // set by *pre* routine
        if (injector->Phase() == hamartia_api::PHASE_EVAL) {
            // Populate *current* context from instruction
            injector->PostCurrentStateToContext(inst_data, const_cast<const PIN_REGISTER**>(_w_operands), write_ea, rflags);
            // Injector phase is PHASE_INJECT now
            injector->RollbackState(state);
            PIN_ExecuteAt(state);
        } else if (injector->Phase() == hamartia_api::PHASE_INJECT) {
            // record output
            injector->InjectionStateToContext(const_cast<const PIN_REGISTER**>(_w_operands), write_ea, rflags);
            injector->InjectError(); // this logs the output value

            if (injector->Phase() == hamartia_api::PHASE_INJECTED) {
                INFO_PRINT("Finish!")
                InjectFini();
                injector = nullptr;
            } else { // more errors to inject
                is_faulty_read = false;
            }    
        }
    }
}


VOID MemoryErrorInstruction(INS ins, VOID *v)
{
    //Disable instrumentation if true
    if (detach_switch) {
        return;
    }
    // Check if MPI rank id is known
    if (is_mpi && !disable_getrank) {
        return;
    }
    // Find resident image (and ignore if in ignore_imgs) 
    if (isInIgnoredImg(ins, ignore_imgs, images)) {
        return;
    }    
    // Ignore code region headers
    /// Side effect: Flip inject_toggle whenever we see code region headers
    if (inject_regions && isCodeRegionHeader(ins, inject_toggle)) {
        return;
    }    

    if (inject_toggle || !inject_regions) {
        // Filter out the operations from the desired classes
        if (InsInOperationClasses(ins, oclasses, inject_width) && INS_HasFallThrough(ins) && INS_IsMemoryRead(ins)) {
            // Index
            uint32_t mem_r = 0, mem_w = 0;
            // Registers
            REG r_operands[MAX_R_OPERANDS] = {REG_INST_G0, REG_INST_G0, REG_INST_G0};
            REG w_operands[MAX_W_OPERANDS] = {REG_INST_G0, REG_INST_G0, REG_INST_G0};
            // Extract information for error injection
            pin_instruction_helper::GetInstOperandInfo(ins, g_inst_data_map, mem_r, mem_w, r_operands, w_operands);

            // FIXME: use Predicate version for CMOVs ?
            // Insert a call to register the instruction
            INS_InsertIfCall(ins, IPOINT_BEFORE, (AFUNPTR)insif_incr, IARG_END);

            InsertThenTemplate(ins, mem_r, mem_w, r_operands, w_operands, (AFUNPTR) pre_then);

            // Insert a call to check if we reach target instruction
            INS_InsertIfCall(ins, IPOINT_AFTER, (AFUNPTR)insif, IARG_END);

            // Inject an error
            INS_InsertThenCall(ins, IPOINT_AFTER, (AFUNPTR)then, 
                                    IARG_CONTEXT, \
                                    IARG_UINT32, INS_Opcode(ins), \
                                    IARG_INST_PTR, \
                                    IARG_REG_REFERENCE, w_operands[0], \
                                    IARG_REG_REFERENCE, w_operands[1], \
                                    IARG_REG_REFERENCE, w_operands[2], \
                                    IARG_REG_REFERENCE, REG_RFLAGS, \
                                    IARG_END);
        }
    }
} 

VOID MemoryErrorFini(INT32 code, VOID *v)
{
    if (injector) {
        INFO_PRINT("Memory Error Fini...")
        // Write to a file since cout and cerr maybe closed by the application
        out_file.setf(ios::showbase);
        injector->SaveLog(out_file);
        CommonFini();
    }
}


/* =====================================================================
 * Memory Error Specific Functions
 * ===================================================================== */
MemoryErrorInjector::MemoryErrorInjector(std::string error_model, std::string error_config,
                                         std::string detector_model, std::string detector_config)
                                        : PinInjector(error_model, error_config, detector_model, detector_config)
{
    // default values
    addr_space = AS_VIRT;
    test_mode = false;

    SetNumToInject(UINTMAX_MAX); // hard error
    SetNumToRecord(UINTMAX_MAX);

    // override defaults with error config
    if(!error_config.empty()) {
        initFromErrorConfig(error_config);
    }
}

void MemoryErrorInjector::Configure(uint64_t num_to_inject, const std::string& error_point)
{
    // num_to_inject is ignored because it can be override by error config and thus already set in constructor
    
    if (error_point == "post") {
        WARN_PRINT("Output injection is not valid for memory error injector");
    }    
    INFO_PRINT("Register Input injection")
    AcceptErrorPreInject();
}

bool MemoryErrorInjector::PreInjectError(bool auto_mode)
{
    
    // setup fault pattern for each operand
    setupFaultPattern();

    bool success = true;       
    success = hamartia_api::error_manager::PreInjectError(&current_context, &injection_context, &log);
    if(!success) {
        FATAL_ERROR("Pre-Injection error: " << injection_context.error_info.response_message);
    }    

    ++num_errors_injected;

    return true;
}    

bool MemoryErrorInjector::InjectError(bool auto_mode)
{
    // FIXME: update statistics?

    // log output here
    log.LogInjectedValues(injection_context, num_errors_recorded);
    ++num_errors_recorded;

    if (num_errors_injected >= num_errors_to_inject) {
        phase = hamartia_api::PHASE_INJECTED;
        INFO_PRINT("=====Enter PHASE_INJECTED=====")
    } else {    
        phase = hamartia_api::PHASE_EVAL;
        INFO_PRINT("=====Enter PHASE_EVAL=====")
    }        

    return true;
}    

bool MemoryErrorInjector::IsFaultyRead(ADDRINT rd_addr1, ADDRINT rd_addr2, uint32_t rd_size)
{
    if(p_fault_range->Intersects(rd_addr1, rd_size) ||
       p_fault_range->Intersects(rd_addr2, rd_size)) {
        return true;
    }
    return false;
}

void MemoryErrorInjector::SetFaultRange(ADDRINT vaddr, uint8_t size)
{
    p_fault_range = FaultRangeFactory::Create(vaddr, size, *this);
}    


/* =====================================================================
 * Private Methods
 * ===================================================================== */

/*
 *  Return true if the specified memory operand accesses address in the fault range
 */
bool MemoryErrorInjector::isFaultyMemOp(const hamartia_api::Operand &op)
{
    if(op.type == hamartia_api::OT_MEMORY && 
       p_fault_range->Intersects(op.address.virt, op.width/8) ) {
        return true;
    }
    return false;
}    

void MemoryErrorInjector::setupFaultPattern()
{
    const hamartia_api::InstructionData &inst_data = injection_context.instruction_data;

    if(test_mode) {
        int target_op = err_cfg_yaml["test"]["target_operand"].as<int>();
        std::string fault_location = err_cfg_yaml["test"]["fault_location"].as<std::string>();

        hamartia_api::InjectionFilter filter;
        filter.int_match = target_op;
        filter.str_match = fault_location;
        injection_context.trigger.addFilter(filter);

    } else {
        for (unsigned i=0; i < inst_data.inputs.size(); i++) {
            const hamartia_api::Operand &op = inst_data.inputs[i];
            if(isFaultyMemOp(op)) {
                hamartia_api::InjectionFilter filter;
                filter.int_match = i;
                filter.str_match = p_fault_range->GetFaultLocation(op);
                injection_context.trigger.addFilter(filter);
            }    
        }
    }    
}    
