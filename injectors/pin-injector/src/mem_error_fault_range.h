// Copyright 2015, The University of Texas at Austin 
// All rights reserved.
// 
// THIS FILE IS PART OF THE HAMARTIA FAULT/ERROR ANALYSIS SUITE
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met: 
// 
// 1. Redistributions of source code must retain the above copyright
// notice, this list of conditions and the following disclaimer. 
// 
// 2. Redistributions in binary form must reproduce the above copyright
// notice, this list of conditions and the following disclaimer in the
// documentation and/or other materials provided with the distribution. 
// 
// 3. Neither the name of the copyright holder nor the names of its
// contributors may be used to endorse or promote products derived from
// this software without specific prior written permission. 
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
// FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
// COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
// BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
// ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

/**
 *  \file
 *  \brief Interface of memory fault ranges
 *
 *  \version 1.1.0
 *  \author Chun-Kai Chang, University of Texas
 *
 *  \addtogroup error_profiler_injector
 *  @{
 */

#ifndef _MEM_ERROR_FAULT_RANGE_H
#define _MEM_ERROR_FAULT_RANGE_H

#include <string>
#include <memory> // for std::unique_ptr

#include <hamartia_api/debug.h>
#include <hamartia_api/interface.h>

#include "mem_error_common.h"

// forward declaration
class MemoryErrorInjector;

class FaultRange {
    public:
    FaultRange(uint64_t vaddr) : m_vaddr(vaddr) {};
    ~FaultRange() {};    

    /**
     * Test if the memory access intersects with this fault range
     *
     * \param test_vaddr    virtual address of the memory access
     * \param test_size     size in bytes of the memory access
     */
    virtual bool Intersects(uint64_t test_vaddr, uint8_t test_size) = 0;

    /**
     * Get the fault location for the memory access
     *
     * \param op a memory operand that intersects with this fault range
     *
     * \return fault location as a string with ":" delimiting each SIMD lane
     *         e.g., "0x11:0x00:0x00:0x00" means bit 0 and bit 4 of the 0-th SIMD lane are faulty
     *               "0x01:0x02:0x00:0x00" means bit 0 of lane 0 and bit 1 of lane 1 are faulty
     */
    virtual std::string GetFaultLocation(const hamartia_api::Operand& op) = 0;

    protected:
    uint64_t m_vaddr;
};    

class VirtFaultRange : public FaultRange {
    public:
    VirtFaultRange(uint64_t vaddr, uint8_t size);
    ~VirtFaultRange(){};

    bool Intersects(uint64_t test_vaddr, uint8_t test_size);
    std::string GetFaultLocation(const hamartia_api::Operand& op);

    private:
    uint8_t m_size; // in bytes
};    

class DRAMFaultRange : public FaultRange {
    public:
    typedef enum {
        FT_1BIT,
        FT_1WORD,
        FT_1ROW,
        FT_1COL,
        FT_1BANK,
        FT_1RANK,
        NUM_FT
    } FaultType;

    DRAMFaultRange(uint64_t vaddr, const MemoryErrorInjector& injector);
    ~DRAMFaultRange(){};
    
    bool Intersects(uint64_t test_vaddr, uint8_t test_size);
    std::string GetFaultLocation(const hamartia_api::Operand& op);

#if DEBUG
    void print();
#endif
    private:
    FaultType      m_fault_type;
    dram_addr_t    m_daddr;
    uint8_t        m_fault_chip_id;
    const dram_info_t* m_p_dram_info;

    uint8_t getRandomChipId();
    void setFaultType(int fault_type);
}; 

class FaultRangeFactory {
    public:
    /**
     *  Create a fault range based on configuration of the injector
     *
     *  \param vaddr    virtual address of the memory access
     *  \param size     size in bytes 
     *  \param injector memory injector (for determing fault range types and address translation)
     */ 
    static std::unique_ptr<FaultRange> Create(uint64_t vaddr, uint8_t size, const MemoryErrorInjector& injector);
};    

#endif // #ifndef __MEM_ERROR_FAULT_RANGE_H__
/** @} */
