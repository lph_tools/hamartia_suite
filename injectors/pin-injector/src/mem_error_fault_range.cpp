// Copyright 2015, The University of Texas at Austin 
// All rights reserved.
// 
// THIS FILE IS PART OF THE HAMARTIA FAULT/ERROR ANALYSIS SUITE
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met: 
// 
// 1. Redistributions of source code must retain the above copyright
// notice, this list of conditions and the following disclaimer. 
// 
// 2. Redistributions in binary form must reproduce the above copyright
// notice, this list of conditions and the following disclaimer in the
// documentation and/or other materials provided with the distribution. 
// 
// 3. Neither the name of the copyright holder nor the names of its
// contributors may be used to endorse or promote products derived from
// this software without specific prior written permission. 
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
// FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
// COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
// BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
// ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

/**
 *  \file
 *  \brief Implementation of memory fault ranges
 *
 *  \version 1.1.0
 *  \author Chun-Kai Chang, University of Texas
 *
 *  \addtogroup error_profiler_injector
 *  @{
 */

#include <hamartia_api/debug.h>
#include <hamartia_api/utils/error.h>

#include "mem_error_fault_range.h"
#include "mem_error_addr_map.h"
#include "mem_error.h"

/* =====================================================================
 * Virtual Fault Range Methods
 * ===================================================================== */
VirtFaultRange::VirtFaultRange(uint64_t vaddr, uint8_t size) : FaultRange(vaddr), m_size(size)
{
}

bool VirtFaultRange::Intersects(uint64_t test_vaddr, uint8_t test_size)
{
    if(!test_vaddr) return false;
    
    uint64_t start = test_vaddr;    uint64_t end = start + test_size; 
    uint64_t fstart = m_vaddr;      uint64_t fend = fstart + m_size;
    if((start >= fstart && start < fend) || (end >= fstart && end < fend)) {
        return true;
    }    

    return false;
}    

std::string VirtFaultRange::GetFaultLocation(const hamartia_api::Operand& op)
{
    return "";
}    

/* =====================================================================
 * DRAM Fault Range Methods
 * ===================================================================== */

DRAMFaultRange::DRAMFaultRange(uint64_t vaddr, const MemoryErrorInjector& injector) : FaultRange(vaddr)
{                                                                 
    // this class is a friend of MemoryErrorInjector
    const YAML::Node& config = injector.err_cfg_yaml;
    setFaultType( (config["dram_fault_type"]) ? config["dram_fault_type"].as<int>() : 0 );

    m_daddr = AddrMapManager::Virt2DRAM(vaddr);
    m_p_dram_info = &(injector.dram_info);
    m_fault_chip_id = getRandomChipId();

#if DEBUG
    print();
#endif
    FATAL_ERROR("Finish test up to DRAMFaultRange's constructor!");
}

bool DRAMFaultRange::Intersects(uint64_t test_vaddr, uint8_t test_size)
{
    return false;
}    

std::string DRAMFaultRange::GetFaultLocation(const hamartia_api::Operand& op)
{
    return "";
}    

uint8_t DRAMFaultRange::getRandomChipId()
{
    // FIXME: how about ECC chips?
    uint8_t num_chips = m_p_dram_info->bus_width / m_p_dram_info->chip_width;
    return hamartia_api::utils::mt_random() % num_chips;
}

void DRAMFaultRange::setFaultType(int ft)
{
    if (ft >= 0 && ft < NUM_FT) {
        m_fault_type = static_cast<FaultType>(ft);
    } else if (ft == -1) { // placeholder for random fault type based on FIT rate data
        FATAL_ERROR("Unimplemented DRAM random faults...");
    } else {  
        FATAL_ERROR("Invalid DRAM fault type: " << ft);
    }        
}

#if DEBUG
void DRAMFaultRange::print()
{
    // Note: need to cast uint8_t to unsigned in order to print

    INFO_PRINT("DRAM Fault Range Addres:")
    INFO_PRINT("ch-" << (unsigned)m_daddr.ch << " , dimm-" << (unsigned)m_daddr.dimm << " , rank-" << (unsigned)m_daddr.rank << " , bank-" << (unsigned)m_daddr.bank << " , row-" << std::hex << m_daddr.row << " , col-" << m_daddr.col << std::dec)

    INFO_PRINT("DRAM Info:")
    INFO_PRINT("num_ch-" << (unsigned)m_p_dram_info->num_ch << " , num_dimm_per_ch-" << (unsigned)m_p_dram_info->num_dimm_per_ch << " , num_rank_per_dimm-" << (unsigned)m_p_dram_info->num_rank_per_dimm << " , bus_width-" << (unsigned)m_p_dram_info->bus_width << " , chip_width-" << (unsigned)m_p_dram_info->chip_width << " , burst-" << (unsigned)m_p_dram_info->burst)

    INFO_PRINT("Faulty Chip ID: " << (unsigned) m_fault_chip_id)
}    

#endif

/* =====================================================================
 * Fault Range Factory Methods
 * ===================================================================== */

std::unique_ptr<FaultRange> FaultRangeFactory::Create(uint64_t vaddr, uint8_t size, const MemoryErrorInjector& injector)
{
    AddrSpace addr_space = injector.GetAddrSpace();
    switch(addr_space) {
        case AS_VIRT: {
            INFO_PRINT("Create a virtual fault range...");
            std::unique_ptr<FaultRange> temp(new VirtFaultRange(vaddr, size));
            return temp;
        }    
        case AS_DRAM: {
            INFO_PRINT("Create a DRAM fault range...");
            std::unique_ptr<FaultRange> temp(new DRAMFaultRange(vaddr, injector));
            return temp;
        }    
        default:    
            FATAL_ERROR("Failed to create fault range due to invalid address space type...")
    }
}    
