// Copyright 2015, The University of Texas at Austin 
// All rights reserved.
// 
// THIS FILE IS PART OF THE HAMARTIA FAULT/ERROR ANALYSIS SUITE
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met: 
// 
// 1. Redistributions of source code must retain the above copyright
// notice, this list of conditions and the following disclaimer. 
// 
// 2. Redistributions in binary form must reproduce the above copyright
// notice, this list of conditions and the following disclaimer in the
// documentation and/or other materials provided with the distribution. 
// 
// 3. Neither the name of the copyright holder nor the names of its
// contributors may be used to endorse or promote products derived from
// this software without specific prior written permission. 
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
// FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
// COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
// BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
// ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

/**
 *  \file
 *  \brief Implementation of timing error log
 *
 *  \version 1.1.0
 *  \author Chun-Kai Chang, University of Texas
 *
 *  \addtogroup error_profiler_injector
 *  @{
 */

#include <hamartia_api/logging.h>
#include "timing_error_logging.h"
#include "helpers/instruction.h"

typedef std::pair<hamartia_api::InstructionData, hamartia_api::InstructionData> InstPairType;
typedef std::vector<hamartia_api::Operand>::const_iterator OpIt;

static std::vector<InstPairType> inst_pairs;
static std::vector<std::string> locations;
static std::vector<std::string> miscs;

static unsigned next_point = 0; // used by the over-provisioning optimization
static unsigned num_same_val_pair = 0;

static void EmitInstData(YAML::Emitter& e, const hamartia_api::InstructionData& inst_data)
{
    e << YAML::BeginMap;

    e << YAML::Key << "asm_line" << YAML::Value << inst_data.asm_line;

    e << YAML::Key << "instruction" << YAML::Value;
    hamartia_api::Log::YAMLInstruction(e, inst_data.instruction);
    
    e << YAML::Key << "inputs" << YAML::Value << YAML::BeginSeq;
    for (OpIt opi = inst_data.inputs.begin(); opi != inst_data.inputs.end(); ++opi) {
        hamartia_api::Log::YAMLOperand(e, *opi);
    }    
    e << YAML::EndSeq;

    e << YAML::EndMap;
} 

static void EmitLocation(YAML::Emitter& e, uint32_t loc_idx)
{
    if(loc_idx >= locations.size())
        return;
    // load string as a YAML Node
    e << YAML::Load(locations[loc_idx]);
}

static void EmitMisc(YAML::Emitter& e, uint32_t loc_idx)
{
    if(loc_idx >= miscs.size())
        return;
    e << YAML::Load(miscs[loc_idx]);
}

void TimingErrorLog::AddToLog(const hamartia_api::ErrorContext& context, hamartia_api::Log& log)
{
    inst_pairs.emplace_back(context.prev_instruction_data, context.instruction_data);

    YAML::Emitter src_location;
    log.DumpLocation(src_location);
    locations.push_back(src_location.c_str());

    YAML::Emitter model_specific;
    log.DumpMisc(model_specific);
    miscs.push_back(model_specific.c_str());
}

void TimingErrorLog::Emit(std::ofstream& log_file)
{
    YAML::Emitter yaml_out;

    yaml_out << YAML::BeginMap;

    if (inst_pairs.size() != 0) {
        yaml_out << YAML::Key << "masked_pairs" << YAML::Value << YAML::BeginSeq;
        unsigned pi = 0; // pair index
        for(std::vector<InstPairType>::iterator it = inst_pairs.begin(); 
            it != inst_pairs.end(); ++it, ++pi) {
            yaml_out << YAML::BeginMap;
            yaml_out << YAML::Key << "previous" << YAML::Value; 
            EmitInstData(yaml_out, it->first);
            yaml_out << YAML::Key << "current" << YAML::Value;
            EmitInstData(yaml_out, it->second);
            yaml_out << YAML::Key << "location" << YAML::Value;
            EmitLocation(yaml_out, pi);
            yaml_out << YAML::Key << "misc" << YAML::Value;
            EmitMisc(yaml_out, pi);
            yaml_out << YAML::EndMap;
        }    
        yaml_out << YAML::EndSeq;
    }

    yaml_out << YAML::Key << "next_point" << YAML::Value << next_point;

    yaml_out << YAML::Key << "num_same_val_pairs" << YAML::Value << num_same_val_pair;

    yaml_out << YAML::EndMap;

    if (log_file.is_open()) {
        log_file.setf(std::ios::showbase);
        log_file << yaml_out.c_str() << std::endl;
        log_file.close();
    } else { 
        FATAL_ERROR("Additional timing error log file is not open...");
    }
}    
   
void TimingErrorLog::IncNextPoint()
{
    next_point += 1;
}

void TimingErrorLog::IncNumSameValPair()
{
    num_same_val_pair += 1;
}    
