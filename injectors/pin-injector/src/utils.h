// Copyright 2015, The University of Texas at Austin 
// All rights reserved.
// 
// THIS FILE IS PART OF THE HAMARTIA FAULT/ERROR ANALYSIS SUITE
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met: 
// 
// 1. Redistributions of source code must retain the above copyright
// notice, this list of conditions and the following disclaimer. 
// 
// 2. Redistributions in binary form must reproduce the above copyright
// notice, this list of conditions and the following disclaimer in the
// documentation and/or other materials provided with the distribution. 
// 
// 3. Neither the name of the copyright holder nor the names of its
// contributors may be used to endorse or promote products derived from
// this software without specific prior written permission. 
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
// FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
// COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
// BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
// ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

/**
 *  \file
 *  \brief Utility functions
 *
 *  \version 1.0.0
 *  \author Michael Sullivan, University of Texas
 *  \author Nicholas Kelly, University of Texas
 *
 *  \addtogroup utils
 *  @{
 */

#ifndef _UTILS_H
#define _UTILS_H

#include <stdint.h>

namespace utils
{
    /* =====================================================================
     * PARSING HELPER FUNCTIONS
     * ===================================================================== */
    
    /**
     * Covert to uppercase
     *
     * \param s String to uppercase
     */
    std::string inline toUpper(std::string s)
    {
        std::transform(s.begin(), s.end(),
                       s.begin(), std::ptr_fun<int, int>(std::toupper));
        return s;
    }
    
    /**
     * Split string by delimiter
     *
     * \param source Source string
     * \param delimiter Character to split the string by
     * \param keep_empty Keep empty strings
     * \param to_upper Covert strings to uppercase
     *
     * \return String vector
     */
    std::vector<std::string> inline StringSplit(const std::string &source, const char *delimiter = ",", 
                                                bool keep_empty = false, bool to_upper = true)
    {
        std::vector<std::string> results;
        
        size_t prev = 0;
        size_t next = 0;
        
        while ((next = source.find_first_of(delimiter, prev)) != std::string::npos) {
            if (keep_empty || (next - prev != 0)) {
                if (to_upper)
                    results.push_back(toUpper(source.substr(prev, next - prev)));
                else
                    results.push_back(source.substr(prev, next - prev));
            }
            prev = next + 1;
        }
        
        if (prev < source.size()) {
            if (to_upper)
                results.push_back(toUpper(source.substr(prev)));
            else
                results.push_back(source.substr(prev));
        }
        
        return results;
    }

    /**
     * Decode routine name mangling
     *
     * \param rtn_name Routine name string
     * \return Routine name that is close to source code
     */
    std::string inline DecodeRoutineName(std::string rtn_name)
    {
        if (rtn_name.find("_ZN") == 0) {
            // C++
            rtn_name = rtn_name.substr(3);
        } else if (rtn_name.find("_Z") == 0) {
            // C++
            rtn_name = rtn_name.substr(2);
        } else {
            // C
            return rtn_name;
        }
        // Parse C++ (name mangling)
        int length = 0;
        std::stringstream ss;
        std::string name;
        for (std::string::iterator c = rtn_name.begin(); c != rtn_name.end(); ++c) {
            if (length == 0) {
                if (*c >= '0' && *c <= '9') {
                    ss << *c;
                } else {
                    ss >> length;
                    ss.clear();
                    if (length == 0) break;
                    if (!name.empty()) {
                        name += "::";
                    }
                }
            } 
            if (length != 0) {
                --length;
                name += *c;
            }
        }

        return name;
    }

    /**
     * Check if a string starts with a pattern
     *
     * \param s Query string
     * \param pattern Query pattern
     */
    bool inline stringStartsWith(const std::string& s, const std::string& pattern)
    {
        if(s.find(pattern) == 0) 
            return true;
        else 
            return false;
    }    

    /* =====================================================================
     * BIT MANIPULATION HELPERS
     * ===================================================================== */

    /**
     * Integer Log2 (the log2 in <cmath> promotes integer to float)
     *
     * \param val unsigned integer value
     */
    template<typename T> uint32_t inline ilog2(T val);
    // template specializations 
    template<> uint32_t inline ilog2<uint32_t>(uint32_t val)
    {
        return val? 31 - __builtin_clz(val) : 0;
    }    
    template<> uint32_t inline ilog2<uint64_t>(uint64_t val)
    {
        return val? 63 - __builtin_clz(val) : 0;
    }  

    /**
     * Circular left shift
     *
     * \param val   unsigned integer value
     * \param n     no. of bit positions to shift
     * \param w     the window length in bits beyond which bits are wrapped around
     *
     * \warning     w must be greater than or equal to n
     */
    uint32_t inline circularLSHF(uint32_t val, uint32_t n, uint32_t w)
    {
        return ((val << n) | (val >> (w - n))) & ((1U << w) - 1);
    }    
}
#endif 
/** @} */
