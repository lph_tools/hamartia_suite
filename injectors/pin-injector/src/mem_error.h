// Copyright 2015, The University of Texas at Austin 
// All rights reserved.
// 
// THIS FILE IS PART OF THE HAMARTIA FAULT/ERROR ANALYSIS SUITE
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met: 
// 
// 1. Redistributions of source code must retain the above copyright
// notice, this list of conditions and the following disclaimer. 
// 
// 2. Redistributions in binary form must reproduce the above copyright
// notice, this list of conditions and the following disclaimer in the
// documentation and/or other materials provided with the distribution. 
// 
// 3. Neither the name of the copyright holder nor the names of its
// contributors may be used to endorse or promote products derived from
// this software without specific prior written permission. 
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
// FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
// COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
// BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
// ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

/**
 *  \file
 *  \brief Interface of memory error injection
 *
 *  \version 1.1.0
 *  \author Sangkug Lym, University of Texas
 *  \author Chun-Kai Chang, University of Texas
 *
 *  \addtogroup error_profiler_injector
 *  @{
 */

#ifndef _MEM_ERROR_H
#define _MEM_ERROR_H

#include <yaml-cpp/yaml.h>

#include "pin_injector.h"
#include "mem_error_fault_range.h"

/**
 * Instruction-level instrumentation routine
 *
 * \param ins Instruction
 */
VOID MemoryErrorInstruction(INS ins, VOID *v);

/**
 * Fini routine for clean-up and logging
 */
VOID MemoryErrorFini(INT32 code, VOID *v);

class MemoryErrorInjector : public PinInjector {
    public:
    MemoryErrorInjector(std::string error_model, std::string error_config, std::string detector_model, std::string detector_config);

    ~MemoryErrorInjector() {}

    void Configure(uint64_t num_to_inject, const std::string& error_point);

    /**
     * Specialized PreInjectError for memory error
     *
     * \param auto_mode Unused, kept for compatability
     *
     * \return Successfully injected
     */
    virtual bool PreInjectError(bool auto_mode = false); 

    /**
     * Specialized InjectError for memory error
     *
     * \param auto_mode Unused, kept for compatability
     *
     * \return Successfully injected
     */
    virtual bool InjectError(bool auto_mode = false); 

    /**
     * Test if instruction accesses the fault range
     *  
     * \param rd_addr1  read address of first memory operand
     * \param rd_addr2  read address of second  memory operand
     * \param rd_size   read size in bytes
     */
    bool IsFaultyRead(ADDRINT rd_addr1, ADDRINT rd_addr2, uint32_t rd_size);

    /**
     * Set a fault range
     *  
     * \param vaddr  virtual address
     * \param size   access size in bytes
     */
    void SetFaultRange(ADDRINT vaddr, uint8_t size);

    AddrSpace GetAddrSpace() const { return addr_space; }

    bool HasFaultRange() { return p_fault_range != nullptr; }

    friend class DRAMFaultRange; // facilitate fault range creation and address check

    private:
    AddrSpace addr_space; // which address space to inject

    std::unique_ptr<FaultRange> p_fault_range; // FIXME: do we need multiple fault ranges?

    dram_info_t dram_info;

    bool test_mode;
    YAML::Node err_cfg_yaml; // error config in YAML 

    void initFromErrorConfig(std::string error_config); // defined in mem_error_init.cpp
    bool isFaultyMemOp(const hamartia_api::Operand &op);
    void setupFaultPattern();

};

#endif // #ifndef __MEM_ERROR_H__
/** @} */

