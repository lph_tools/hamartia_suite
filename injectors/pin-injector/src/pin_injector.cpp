// Copyright 2015, The University of Texas at Austin 
// All rights reserved.
// 
// THIS FILE IS PART OF THE HAMARTIA FAULT/ERROR ANALYSIS SUITE
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met: 
// 
// 1. Redistributions of source code must retain the above copyright
// notice, this list of conditions and the following disclaimer. 
// 
// 2. Redistributions in binary form must reproduce the above copyright
// notice, this list of conditions and the following disclaimer in the
// documentation and/or other materials provided with the distribution. 
// 
// 3. Neither the name of the copyright holder nor the names of its
// contributors may be used to endorse or promote products derived from
// this software without specific prior written permission. 
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
// FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
// COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
// BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
// ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

/**
 *  \file
 *  \brief Pin injector
 *
 *  \version 1.0.0
 *  \author Michael Sullivan, University of Texas
 *  \author Nicholas Kelly, University of Texas
 *  \author Chun-Kai Chang, University of Texas
 *
 *  \addtogroup injector
 *  @{
 */
#include <hamartia_api/error_manager.h>

#include <hamartia_api/debug.h>
#include <hamartia_api/interface.h>
#include "pin_injector.h"
#include "helpers/operand.h"
#include "helpers/instruction.h"

#if DEBUG
void PrintContextInput(hamartia_api::ErrorContext& context, std::string str_header);
void PrintContextOutput(hamartia_api::ErrorContext& context, std::string str_header);
#endif

void PinInjector::PreCurrentStateToContext(hamartia_api::InstructionData &inst_data, const PIN_REGISTER * r_registers[], ADDRINT r_addrs[], const PIN_REGISTER * w_registers[], ADDRINT w_addr)
{
    INFO_PRINT("Pre state save...")
    // Indexes
    uint32_t io_r = 0;
    uint32_t io_w = 0;
    uint32_t mem_r = 0;
    uint32_t mem_w = 0;
    
    // Parse operands
    ADDRINT w_addrs[1] = {w_addr};
    
    // Input values
    for (int i = 0, l = inst_data.inputs.size(); i < l; ++i) {
        pin_operand_helper::UpdateOperandFromPin(&inst_data.inputs[i], inst_data.simd_width, io_r, mem_r, r_registers, r_addrs);
    }
    
    // Old output values
    for (int i = 0, l = inst_data.outputs.size(); i < l; ++i) {
        pin_operand_helper::UpdateOperandFromPin(&inst_data.outputs[i], inst_data.simd_width, io_w, mem_w, w_registers, w_addrs, true);
    }
}

void PinInjector::PostCurrentStateToContext(hamartia_api::InstructionData &inst_data, const PIN_REGISTER *w_registers[], ADDRINT w_addr, ADDRINT rflags)
{
    INFO_PRINT("Post state save...")
    // Indexes
    uint32_t io_w = 0;
    uint32_t mem_w = 0;
    // Get (correct) destination values
    ADDRINT _addrs[1] = {w_addr};
    
    // Output values
    for (int i = 0, l = inst_data.outputs.size(); i < l; ++i) {
        pin_operand_helper::UpdateOperandFromPin(&inst_data.outputs[i], inst_data.simd_width, io_w, mem_w, w_registers, _addrs);
    }
    
    // Set Instruction data
    current_context.instruction_data = inst_data;
    
    // Set logging info
    PIN_LockClient();
    std::map<std::string, std::string> localMap = pin_instruction_helper::GetMetaData(inst_data);
    log.LocationAddLogEntry(localMap);
    PIN_UnlockClient();

    // Next phase
    log.LogInitialValues(current_context);
    injection_context = current_context;
    record_context = current_context;
    phase = hamartia_api::PHASE_INJECT;
    INFO_PRINT("=====Enter PHASE_INJECT=====")
}

void PinInjector::InjectionStateToContext(const PIN_REGISTER *w_registers[], ADDRINT w_addr, ADDRINT rflags)
{
    INFO_PRINT("Injection state save...")
    // Indexes
    uint32_t io_w = 0;
    uint32_t mem_w = 0;
    // Get (correct) destination values
    ADDRINT _addrs[1] = {w_addr};
    
    // Output values
    for (int i = 0, l = injection_context.instruction_data.outputs.size(); i < l; ++i) {
        pin_operand_helper::UpdateOperandFromPin(&injection_context.instruction_data.outputs[i], 
                                                 injection_context.instruction_data.simd_width, 
                                                 io_w, mem_w, w_registers, _addrs);
    }

#if DEBUG
    PrintContextOutput(injection_context, "Debug Pre-inject Output");
#endif        
}

void PinInjector::RollbackState(CONTEXT *state)
{
    // Rollback to target instruction
    INFO_PRINT("Rollback to target...")
    pin_operand_helper::UpdatePinReg(state, REG_INST_PTR, 64, hamartia_api::DataValue(current_context.instruction_data.address.virt));
    // Rollback inputs/outputs
    OldInputContextToState(state);
    OldOutputContextToState(state);
}

void PinInjector::OldInputContextToState(CONTEXT *state)
{
    // Update Pin state from inputs
    INFO_PRINT("Rollback inputs...")
    for (int i = 0, l = current_context.instruction_data.inputs.size(); i < l; ++i) {
        hamartia_api::Operand op = current_context.instruction_data.inputs[i];
        pin_operand_helper::UpdatePinFromOperand(&op, state);
    }
#if DEBUG
    PrintContextInput(current_context, "Debug Rollback Input");
#endif        
}

void PinInjector::OldOutputContextToState(CONTEXT *state)
{
    // Update Pin state from outputs
    INFO_PRINT("Rollback outputs...")
    for (int i = 0, l = current_context.instruction_data.outputs.size(); i < l; ++i) {
        hamartia_api::Operand op = current_context.instruction_data.outputs[i];
        pin_operand_helper::UpdatePinFromOperand(&op, state, true);
    }
}

void PinInjector::PreInjectionContextToState(PIN_REGISTER * r_registers[])
{
    // Update Pin state from inputs
    INFO_PRINT("Save pre-injection context...")
    uint32_t io_r = 0;

    // Corner case: input operands are the same (e.g., add edx, edx)
    // At micro-arch level, they are buffered at different input FFs of the execution unit 
    // However, at arch level, we are not able to model the event that only one FF is affected
    // because they use the same arch reg. Thus, we can only model the RF is striked directly
    // (i.e., both input operands are faulty)
    std::string last_reg = "";
    uint64_t last_addr = 0;
    int last_input_idx = -1;

    for (int i = 0, l = injection_context.instruction_data.inputs.size(); i < l; ++i) {
        hamartia_api::Operand& op = injection_context.instruction_data.inputs[i];

        // check if inputs operands have the same name
        // if so, we have to set those operands with the same value
        // otherwise, Pin updates the true register value using the value of the last operand
        if ((op.type == hamartia_api::OT_REGISTER && op.reg == last_reg) ||
            (op.type == hamartia_api::OT_MEMORY && op.address.virt == last_addr)) {
            
            for(unsigned i=0; i<op.value.size(); ++i)
                op.value[i] = injection_context.instruction_data.inputs[last_input_idx].value[i]; 
        }

        pin_operand_helper::UpdatePinFromOperand(&op, io_r, r_registers);

        // register last update
        if (op.type == hamartia_api::OT_REGISTER) {
            last_reg = op.reg;
        } else if (op.type == hamartia_api::OT_MEMORY) {
            last_addr = op.address.virt;
        }
        last_input_idx = i;
    }

#if DEBUG
    PrintContextInput(injection_context, "Debug Pre-inject Input");
#endif        
}

void PinInjector::InjectionContextToState(CONTEXT *state, ADDRINT &rflags)
{
    // Update Pin state from outputs
    INFO_PRINT("Save injection context...")
    for (int i = 0, l = injection_context.instruction_data.outputs.size(); i < l; ++i) {
        hamartia_api::Operand op = injection_context.instruction_data.outputs[i];
        pin_operand_helper::UpdatePinFromOperand(&op, state);
    }
    
    // Update rflags
    if (injection_context.error_info.response == hamartia_api::RESP_UPDATE) {
        pin_operand_helper::UpdateRFlags(&injection_context, rflags);
    }
}

void PinInjector::PurgeInjectionContext()
{
    injection_context.instruction_data = current_context.instruction_data;
}    

/////////////////////////////////
// Debugging Methods
/////////////////////////////////
#if DEBUG
std::string GetOpTypeName(hamartia_api::Operand& op)
{
    std::string name = "";
    switch(op.type) {
        case hamartia_api::OT_REGISTER: 
            name = "REG"; break;  
        case hamartia_api::OT_MEMORY: 
            name = "MEM"; break;  
        case hamartia_api::OT_IMMEDIATE: 
            name = "IMM"; break;  
        case hamartia_api::OT_FLAGS: 
            name = "FLAG"; break;  
        case hamartia_api::OT_STATUS: 
            name = "STAT"; break;
        default:
            name = "";    
    }
    return name;   
}    

void PrintContextInput(hamartia_api::ErrorContext& context, std::string str_header)
{
    for (int i = 0, l = context.instruction_data.inputs.size(); i < l; ++i) {
        hamartia_api::Operand& op = context.instruction_data.inputs[i];
        std::string name = GetOpTypeName(op);
        for (auto j=0; j<op.simd_width; j++) {
            INFO_PRINT(str_header << " "<< name << ",lane " << j << " has value 0x" << std::hex << op.value[j].uint() << std::dec)
        }        
    }  
}    

void PrintContextOutput(hamartia_api::ErrorContext& context, std::string str_header)
{
    for (int i = 0, l = context.instruction_data.outputs.size(); i < l; ++i) {
        hamartia_api::Operand& op = context.instruction_data.outputs[i];
        std::string name = GetOpTypeName(op);
        for (auto j=0; j<op.simd_width; j++) {
            INFO_PRINT(str_header << " "<< name << ",lane " << j << " has value 0x" << std::hex << op.value[j].uint() << std::dec)
        }    
    }  
}
#endif
/** @} */
