// Copyright 2015, The University of Texas at Austin 
// All rights reserved.
// 
// THIS FILE IS PART OF THE HAMARTIA FAULT/ERROR ANALYSIS SUITE
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met: 
// 
// 1. Redistributions of source code must retain the above copyright
// notice, this list of conditions and the following disclaimer. 
// 
// 2. Redistributions in binary form must reproduce the above copyright
// notice, this list of conditions and the following disclaimer in the
// documentation and/or other materials provided with the distribution. 
// 
// 3. Neither the name of the copyright holder nor the names of its
// contributors may be used to endorse or promote products derived from
// this software without specific prior written permission. 
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
// FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
// COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
// BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
// ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

/**
 *  \file
 *  \brief A PIN tool for error profiling and injection
 *
 *  \version 1.0.0
 *  \author Michael Sullivan, University of Texas
 *  \author Nicholas Kelly, University of Texas
 *  \author Chun-Kai Chang, University of Texas
 *
 *  \addtogroup error_profiler_injector
 *  @{
 */

#ifndef _ERROR_PROF_INJ_H
#define _ERROR_PROF_INJ_H

// Std libs
#include <iostream>
#include <fstream>
#include <cmath>
#include <stdlib.h>
#include <time.h>
#include <sys/time.h>
#include <string>
#include <vector>
#include <unordered_map>
#include <algorithm>

#include "pin_injector.h"

// Pin
#include "pin.H"
#include "instlib.H"

/* =====================================================================
 * Common Global Variables
 * ===================================================================== */
// defined in error_pro_inj.cpp
/* Command-line switches */
extern KNOB<string> KnobErrorModel;
extern KNOB<string> KnobErrorConfig;
extern KNOB<string> KnobDetectorModel;
extern KNOB<string> KnobDetectorConfig;

/* Instruction Couting */
extern UINT64 ticount;
extern UINT64 ricount; 
extern std::vector<UINT64> tilist;
extern UINT32 tilist_idx;

/* Injection Switches */
extern BOOL detach_switch; 
extern BOOL inject_regions;
extern BOOL inject_toggle;

/* Injection Target Filters */
extern std::vector<UINT32> images;
extern std::vector<string> ignore_imgs;
extern std::vector<std::string> oclasses;
extern UINT32 op_tgt_index;
extern UINT32 inject_width;

/* Injector */
extern PinInjector * injector;

/* Files */
extern ofstream out_file;

/* Statistics */
extern struct timeval pin_start, pin_inj, pin_end;

/* Conduits between instrumentation and analysis routines */
extern std::unordered_map<ADDRINT, hamartia_api::InstructionData> g_inst_data_map;
extern ADDRINT write_ea;

/* =====================================================================
 * Common Functions
 * ===================================================================== */
VOID InjectorInit();

ADDRINT insif_incr();
ADDRINT insif();

VOID InsertThenTemplate(INS& ins, const uint32_t& mem_r, const uint32_t& mem_w, REG* r_operands, REG* w_operands, AFUNPTR func);

VOID InjectFini();
VOID CommonFini();

/* =====================================================================
 * MPI Mode
 * ===================================================================== */
// defined in pin_mpi.h
/* Command-line switches */
extern KNOB<BOOL> KnobIsMpi;
extern KNOB<UINT32> KnobRankId;

/* Global variables */
/// MPI mode flag
extern BOOL is_mpi;
/// MPI get rank switch
extern BOOL disable_getrank; 

/* =====================================================================
 * CD Mode
 * ===================================================================== */
// defined in pin_cd.h
/* Command-line switches */
extern KNOB<BOOL> KnobIsCD;
extern KNOB<UINT32> KnobCDMode;
extern KNOB<string> KnobCDName;



#endif // #ifndef __ERROR_PROF_INJ_H__
/** @} */
