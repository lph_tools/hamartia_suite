// Copyright 2015, The University of Texas at Austin 
// All rights reserved.
// 
// THIS FILE IS PART OF THE HAMARTIA FAULT/ERROR ANALYSIS SUITE
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met: 
// 
// 1. Redistributions of source code must retain the above copyright
// notice, this list of conditions and the following disclaimer. 
// 
// 2. Redistributions in binary form must reproduce the above copyright
// notice, this list of conditions and the following disclaimer in the
// documentation and/or other materials provided with the distribution. 
// 
// 3. Neither the name of the copyright holder nor the names of its
// contributors may be used to endorse or promote products derived from
// this software without specific prior written permission. 
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
// FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
// COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
// BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
// ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

/**
 *  \file
 *  \brief Memory error initialization methods
 *
 *  \version 1.1.0
 *  \author Chun-Kai Chang, University of Texas
 *
 *  \addtogroup error_profiler_injector
 *  @{
 */

#include <stdlib.h> //strtoul

#include "mem_error.h"
#include "mem_error_addr_map.h"

static uint64_t getNumToInject(std::string mode)
{
    if (mode == "soft") {
        return 1;
    } else if (mode == "hard") {
        return UINTMAX_MAX;
    } else {
        uint64_t num_to_inject = strtoul(mode.c_str(), nullptr, 10); // decimal string
        if(num_to_inject) {
            return num_to_inject;
        } else {
            FATAL_ERROR("Invalid memory injection mode " << mode)
        }        
    }    
}    

static AddrSpace getAddrSpace(std::string addr_space)
{
    if (addr_space == "virt") {
        return AS_VIRT;
    } else if (addr_space == "dram") {
        return AS_DRAM;
    } else {
        FATAL_ERROR("Invalid address space: " << addr_space)
    }
}

static dram_info_t getDRAMInfo(YAML::Node dram_info)
{
    dram_info_t ret = {}; // init all members to 0
    ret.num_ch = (dram_info["num_ch"]) ? (dram_info["num_ch"].as<uint32_t>()) : 1;
    ret.num_dimm_per_ch = (dram_info["num_dimm_per_ch"]) ? (dram_info["num_dimm_per_ch"].as<uint32_t>()) : 1;
    ret.num_rank_per_dimm = (dram_info["num_rank_per_dimm"]) ? (dram_info["num_rank_per_dimm"].as<uint32_t>()) : 1;
    ret.bus_width = (dram_info["bus_width"]) ? (dram_info["bus_width"].as<uint32_t>()) : 64;
    ret.chip_width = (dram_info["chip_width"]) ? (dram_info["chip_width"].as<uint32_t>()) : 8;
    ret.burst = (dram_info["burst"]) ? (dram_info["burst"].as<uint32_t>()) : 8;
    ret.num_chips = ret.bus_width / ret.chip_width;
    return ret;
}    

void MemoryErrorInjector::initFromErrorConfig(std::string error_config)
{
    err_cfg_yaml = YAML::LoadFile(error_config);

    uint64_t num_to_inject = getNumToInject(err_cfg_yaml["mode"].as<std::string>());
    SetNumToInject(num_to_inject);
    SetNumToRecord(num_to_inject);

    INFO_PRINT("No. of errors to inject: " << num_errors_to_inject)

    if (err_cfg_yaml["addr_space"]) addr_space = getAddrSpace(err_cfg_yaml["addr_space"].as<std::string>());
    if (addr_space == AS_DRAM) {
        INFO_PRINT("Inject in DRAM address space")
        dram_info = getDRAMInfo(err_cfg_yaml["dram_info"]);
        AddrMapManager::RegisterAddrMap(err_cfg_yaml["addr_map"].as<std::string>(), dram_info);
    } else {
        INFO_PRINT("Inject in virtual address space")
    }

    if(err_cfg_yaml["test"]) test_mode = true;
}    

