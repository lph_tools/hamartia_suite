// Copyright 2015, The University of Texas at Austin 
// All rights reserved.
// 
// THIS FILE IS PART OF THE HAMARTIA FAULT/ERROR ANALYSIS SUITE
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met: 
// 
// 1. Redistributions of source code must retain the above copyright
// notice, this list of conditions and the following disclaimer. 
// 
// 2. Redistributions in binary form must reproduce the above copyright
// notice, this list of conditions and the following disclaimer in the
// documentation and/or other materials provided with the distribution. 
// 
// 3. Neither the name of the copyright holder nor the names of its
// contributors may be used to endorse or promote products derived from
// this software without specific prior written permission. 
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
// FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
// COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
// BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
// ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

/**
 *  \file
 *  \brief A PIN tool for error profiling and injection
 *
 *  \version 1.0.0
 *  \author Michael Sullivan, University of Texas
 *  \author Nicholas Kelly, University of Texas
 *
 *  \addtogroup error_profiler_injector
 *  @{
 */

#include "error_prof_inj.h"
#include "logging.h"
#include "code_filter.h"
#include "oclasses.h"

/// The running count of profiled instructions is kept here
static logging::ProfileData profile_data = {0, 0};

/** 
 * Keep global counts
 *
 * \param img_id ID of the Image associated with the BBL
 * \param tc     Total instructions in BBL
 * \param fc     Filtered instructions in BBL
 */
VOID count_bbl(UINT32 img_id, UINT32 tc, UINT32 fc)
{
    if (img_id > 0) {
        profile_data.g_images[img_id].op_count += tc;
        profile_data.g_images[img_id].filtered_op_count += fc;
    }
    profile_data.g_op_count += tc;
    profile_data.g_filtered_op_count += fc;
}

// TODO: add in DEBUG defines
// TODO: read in config for selecting which grouping to capture
/**
 * Block-specific counts are added at instrumentation time
 * 
 * \param trace Trace
 */
VOID ProfileTrace(TRACE trace, VOID *v)
{
    // Visit every basic block  in the trace
    for (BBL bbl = TRACE_BblHead(trace); BBL_Valid(bbl); bbl = BBL_Next(bbl)) {
        // Keep local counts
        UINT32 totcount = 0, filteredcount = 0;
        // Get image
        ADDRINT addr = BBL_Address(bbl);
        IMG img = IMG_FindByAddress(addr);
        UINT32 img_id = 0;
        if (IMG_Valid(img)) {
            img_id = IMG_Id(img);
            // New image
            if (profile_data.g_images.find(img_id) == profile_data.g_images.end()) {
                profile_data.g_images[img_id].op_count = 0;
                profile_data.g_images[img_id].filtered_op_count = 0;
                if (IMG_Valid(img)) {
                    profile_data.g_images[img_id].img_name = IMG_Name(img);
                }
            }
        }
        // Unfiltered instruction trace
        for (INS ins = BBL_InsHead(bbl); INS_Valid(ins); ins = INS_Next(ins)) {
            // Function
            if (inject_regions && isCodeRegionHeader(ins, inject_toggle)) {
                continue;
            }    
            if (inject_toggle || !inject_regions) {
                // Filter out the operations from the desired classes
                if (InsInOperationClasses(ins, oclasses, inject_width)) {
                    AddProfileOp(profile_data, ins, img_id);
                    filteredcount++;
                }
                // Total instructions
                totcount++;
            }
        }
        // Insert a call to register BB with global counters
        BBL_InsertCall(bbl, IPOINT_ANYWHERE, (AFUNPTR)count_bbl, IARG_UINT32,
                       img_id, IARG_UINT32, totcount, IARG_UINT32, filteredcount, 
                       IARG_END);
    }
}

/**
 * Write out profile data/file
 */
VOID ProfileFini(INT32 code, VOID *v)
{
    if(detach_switch) return; // prevent MPI processes from dumping the same output
    // Write to a file since cout and cerr maybe closed by the application
    if (!logging::EmitProfiling(profile_data, out_file)) {
        // FIXME: error!
    }
    out_file.close();
    // Common
    CommonFini();
}


