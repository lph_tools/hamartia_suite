Instruction-Level Error Injector with Pin
=========================================

Usage
-----
See *docs/pintool/pages/usage.md*


Development
-----------
All source files are stored in the src/ folder. See src/README.md for more details.

For testing, see test/README.md.


Limitations
------------

- This software was developed for and tested on Linux/x86_64 binaries.
  Use on other platforms and experimental setups will require some modification.

- Program type NOT supported
    - Multi-threaded programs binaries can be instrumented by Pin, but error injection 
      in such processes would require significant modification to the pintool and also 
      the error characterization and injection model would have to be extended to be 
      thread-id aware.

    - Programs that fork subprocesses (Try "-follow_execv 1" parameter of Pin)


