// Copyright 2015, The University of Texas at Austin 
// All rights reserved.
// 
// THIS FILE IS PART OF THE HAMARTIA FAULT/ERROR ANALYSIS SUITE
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met: 
// 
// 1. Redistributions of source code must retain the above copyright
// notice, this list of conditions and the following disclaimer. 
// 
// 2. Redistributions in binary form must reproduce the above copyright
// notice, this list of conditions and the following disclaimer in the
// documentation and/or other materials provided with the distribution. 
// 
// 3. Neither the name of the copyright holder nor the names of its
// contributors may be used to endorse or promote products derived from
// this software without specific prior written permission. 
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
// FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
// COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
// BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
// ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

/**
 * \file
 * \brief Model handler, generic class for implementing model handlers in other languages
 * \author Nicholas Kelly, University of Texas
 *
 * \addtogroup handlers
 * @{
 */

#ifndef _HAMARTIA_API_HANDLER_H
#define _HAMARTIA_API_HANDLER_H

#include <hamartia_api/interface.h>
#include <hamartia_api/logging.h>

namespace hamartia_api
{
    /// Model types
    typedef enum {
        MODEL_ERROR,
        MODEL_DETECTOR,
        MODEL_END
    } ModelType;

    /// Language types (handlers)
    typedef enum {
        LANG_CPP,
        LANG_PY,
        LAND_END
    } ModelLanguage;

    /**
     * Generic model handler
     */
    class ModelHandler
    {
        protected:
        /// The language the handler supports
        ModelLanguage lang;
        /// The model type for the handler
        ModelType type;
        /// Methods supported by the model (explicit/implicit)
        uint64_t methods;

        public:
        /// Constructor
        ModelHandler();

        /**
         * Constructor
         *
         * \param _lang Language of handler
         * \param _type Model type of handler
         */
        ModelHandler(ModelLanguage _lang, ModelType _type);

        /// Destructor
        ~ModelHandler();

        /**
         * Model name
         *
         * \return Name of model
         */
        virtual std::string modelName() = 0;

        /**
         * Invoke a method for the model
         *
         * \param method Method to invoke
         * \param cxt    Error context
         * \param log    Log
         *
         * \return Successful
         */
        virtual bool invoke(ModelMethod method, ErrorContext *cxt, Log *log = NULL) = 0;

        /**
         * Check if method is valid (defined)
         *
         * \param method Method to check
         *
         * \return Valid
         */
        bool methodValid(ModelMethod method);

        /**
         * Set method validity
         *
         * \param method Method to set
         * \param cxt    Error context
         */
        void setMethodValidity(ModelMethod method, ErrorContext *cxt);

        /**
         * Default response for a method
         *
         * \param success Success
         * \param cxt     Error context
         */
        static void DefaultResponse(bool success, ErrorContext *cxt);
    };
}

#endif
/** @} */
