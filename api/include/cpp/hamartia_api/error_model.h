// Copyright 2015, The University of Texas at Austin 
// All rights reserved.
// 
// THIS FILE IS PART OF THE HAMARTIA FAULT/ERROR ANALYSIS SUITE
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met: 
// 
// 1. Redistributions of source code must retain the above copyright
// notice, this list of conditions and the following disclaimer. 
// 
// 2. Redistributions in binary form must reproduce the above copyright
// notice, this list of conditions and the following disclaimer in the
// documentation and/or other materials provided with the distribution. 
// 
// 3. Neither the name of the copyright holder nor the names of its
// contributors may be used to endorse or promote products derived from
// this software without specific prior written permission. 
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
// FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
// COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
// BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
// ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

/**
 *  \file
 *  \brief Error model definition
 *  \author Nicholas Kelly, University of Texas
 *
 *  \addtogroup error_model
 *  @{
 */

#ifndef _HAMARTIA_API_ERROR_MODEL_H
#define _HAMARTIA_API_ERROR_MODEL_H

// Include first for no warnings
#include <Python.h>

// Interface
#include <hamartia_api/interface.h>
#include <hamartia_api/logging.h>

// Libs
#include <string>
#include <map>
#include <yaml-cpp/yaml.h>

// Registration macros
/// Register error model library helper
#define REGISTER_ERROR_LIBRARY(elclass) \
    extern "C" hamartia_api::ErrorModelLibrary * register_library() \
    { \
        return new elclass; \
    } \
    extern "C" void unregister_library(hamartia_api::ErrorModelLibrary * elib) \
    { \
        delete dynamic_cast<elclass *>(elib); \
    }

/// Register an error model
#define REGISTER_ERROR_MODEL(eclass, ename) \
    eclass * eclass ## _ ## ename ## _i = new eclass(# ename); \
    AddModel(eclass ## _ ## ename ## _i);

/// Register an error model with arguments
#define REGISTER_ERROR_MODEL_ARGS(eclass, ename, ...) \
    eclass * eclass ## _ ## ename ## _i = new eclass(# ename, __VA_ARGS__); \
    AddModel(eclass ## _ ## ename ## _i);


namespace hamartia_api
{
    // Forward delcarations
    class ErrorModelLibrary; 

    /**
     * Abstract class for creating Error models
     */
    class ErrorModel
    {
        protected:
        /// Error model name
        std::string name;

        public:
        /**
         * Constructor
         *
         * \param _name Error model name
         */
        ErrorModel(std::string _name);

        /**
         * Deconstructor
         */
        virtual ~ErrorModel() {}

        /**
         * Set the response (for extra options, e.g. RESP_UPDATE)
         *
         * \param cxt     Error context
         * \param resp    Response
         * \param message Response message
         */
        static void SetResponse(ErrorContext *cxt, Resp resp, std::string message = std::string());

        /**
         * Get the error model name
         *
         * \return Error model name
         */
        std::string GetName();

        /* =====================================================================
         * API
         * ===================================================================== */

        /**
         * Return which methods (e.g. InjectError) are defined (valid) for the model
         *
         * \return Bit-vector of valid methods
         */
        virtual uint64_t Methods();

        /**
         * Setup Error model (called once on load)
         *
         * \param cxt    Current context
         * \param config YAML config file for configuring the model
         * \param log    Log for adding extra logging information
         * 
         * \return Successful
         */
        virtual bool Setup(ErrorContext *cxt, YAML::Node *config, Log *log = NULL);
        // Python method (needed due to different YAML object, not converted with SWIG)
        virtual bool Setup(ErrorContext *cxt, PyObject *config, Log *log = NULL);

        /**
         * Cleanup Error model (called once on unload)
         *
         * \param cxt    Current context
         * \param config YAML config file for configuring the model
         * \param log    Log for adding extra logging information
         * 
         * \return Successful
         */
        virtual bool Cleanup(ErrorContext *cxt, YAML::Node *config, Log *log = NULL);
        // Python method (needed due to different YAML object, not converted with SWIG)
        virtual bool Cleanup(ErrorContext *cxt, PyObject *config, Log *log = NULL);

        /**
         * Pre-Inject error (inject into inputs before operation)
         *
         * \param cxt    Injection context
         * \param config YAML config file for configuring the model
         * \param log    Log for adding extra logging information
         *
         * \return Successful (no runtime errors)
         */
        virtual bool PreInjectError(ErrorContext *cxt, YAML::Node *config, Log *log = NULL);
        // Python method (needed due to different YAML object, not converted with SWIG)
        virtual bool PreInjectError(ErrorContext *cxt, PyObject *config, Log *log = NULL);

        /**
         * Inject error (defaults to no error)
         *
         * \param cxt    Injection context
         * \param config YAML config file for configuring the model
         * \param log    Log for adding extra logging information
         *
         * \return Successful (no runtime errors)
         */
        virtual bool InjectError(ErrorContext *cxt, YAML::Node *config, Log *log = NULL);
        // Python method (needed due to different YAML object, not converted with SWIG)
        virtual bool InjectError(ErrorContext *cxt, PyObject *config, Log *log = NULL);

    };

    /**
     * Library of Error models, used for registering models with the injection system
     */
    class ErrorModelLibrary
    {
        protected:
        /// Error models in library
        std::vector<ErrorModel *> models;

        public:
        /**
         * Constructor, where models are added
         */
        ErrorModelLibrary() {}
        virtual ~ErrorModelLibrary() {}

        /**
         * Get the name of the library
         *
         * \return Library name
         */
        virtual std::string GetName() = 0;

        /**
         * Add model to the library
         *
         * \param model Error model to add to the libary
         */
        void AddModel(ErrorModel *model);

        /**
         * Get models currently in library
         *
         * \return All models currently added to library
         */
        std::vector<ErrorModel *> GetModels();
    };
}

#endif // #ifndef __ERROR_MODEL_H__
/** @} */
