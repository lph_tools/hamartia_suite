// Copyright 2015, The University of Texas at Austin 
// All rights reserved.
// 
// THIS FILE IS PART OF THE HAMARTIA FAULT/ERROR ANALYSIS SUITE
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met: 
// 
// 1. Redistributions of source code must retain the above copyright
// notice, this list of conditions and the following disclaimer. 
// 
// 2. Redistributions in binary form must reproduce the above copyright
// notice, this list of conditions and the following disclaimer in the
// documentation and/or other materials provided with the distribution. 
// 
// 3. Neither the name of the copyright holder nor the names of its
// contributors may be used to endorse or promote products derived from
// this software without specific prior written permission. 
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
// FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
// COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
// BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
// ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

/**
 *  \file
 *  \brief Simple timing error model
 *
 *  \version 1.0.0
 *  \author Chun-Kai Chang, University of Texas
 *
 *  \addtogroup error_models
 *  @{
 */

#ifndef _TIMING_PREV_ERROR_H
#define _TIMING_PREV_ERROR_H

#include <hamartia_api/types.h>
#include <hamartia_api/utils/error.h>
#include <hamartia_api/error_model.h>

class TimingPrevError : public hamartia_api::ErrorModel
{

    private:
    // Dynamic instruction count between previous and injection context
    // e.g., a distance of 1 implies there is only one function unit of the same op type
    uint64_t distance;

    public:
    /**
     * Create TimingPrevError instance
     *
     * \param _name Error model name
     * \param _distance Number of dynamic instructions between previous and injection context
     */
    TimingPrevError(std::string _name, uint64_t _distance) : ErrorModel(_name)
    {
        distance = _distance;
    }

    /**
     * Inject randbit error
     *
     * \param cxt    Injection context
     * \param config YAML config file for configuring the model
     * \param log    Log for adding extra logging information
     *
     * \return Successful (no runtime errors)
     */
    bool InjectError(hamartia_api::ErrorContext *cxt, YAML::Node *config, hamartia_api::Log *log)
    {
        uint32_t cur_num_lanes = cxt->instruction_data.simd_width; 
        uint32_t prev_num_lanes = cxt->prev_instruction_data.simd_width;
        uint32_t num_lanes = cur_num_lanes < prev_num_lanes ? cur_num_lanes : prev_num_lanes;

        for (unsigned i = 0; i < num_lanes; i++) {
            hamartia_api::DataValue mod_result = cxt->getPrevOutputValue(DEST_OPERAND, i);
            cxt->setOutput(mod_result, DEST_OPERAND, i);
        }
        cxt->error_info.successResponse();
        return true;
    }

    bool PreInjectError(hamartia_api::ErrorContext *cxt, YAML::Node *config, hamartia_api::Log *log)
    {
        // never called
        return false;
    }
};

#endif
/** @} */
