// Copyright 2015, The University of Texas at Austin 
// All rights reserved.
// 
// THIS FILE IS PART OF THE HAMARTIA FAULT/ERROR ANALYSIS SUITE
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met: 
// 
// 1. Redistributions of source code must retain the above copyright
// notice, this list of conditions and the following disclaimer. 
// 
// 2. Redistributions in binary form must reproduce the above copyright
// notice, this list of conditions and the following disclaimer in the
// documentation and/or other materials provided with the distribution. 
// 
// 3. Neither the name of the copyright holder nor the names of its
// contributors may be used to endorse or promote products derived from
// this software without specific prior written permission. 
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
// FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
// COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
// BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
// ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

/**
 *  \file
 *  \brief b-HiVE error model (Tziantzioulis et al. DAC 2015)
 *
 *  \version 1.0.0
 *  \author Chun-Kai Chang, University of Texas
 *
 *  \addtogroup error_models
 *  @{
 */

#ifndef _BHIVE_ERROR_H
#define _BHIVE_ERROR_H

#include <hamartia_api/types.h>
#include <hamartia_api/utils/error.h>
#include <hamartia_api/error_model.h>

// b-HiVE error rate tables for each circuit
#include <hamartia_api/error_models/bhive/basicFaultModel.h>
#include <hamartia_api/error_models/bhive/addFaultModel.h>
#include <hamartia_api/error_models/bhive/mul64FaultModel.h>
#include <hamartia_api/error_models/bhive/moveFaultModel.h>
#include <hamartia_api/error_models/bhive/orrFaultModel.h>
#include <hamartia_api/error_models/bhive/andFaultModel.h>
#include <hamartia_api/error_models/bhive/xorFaultModel.h>
#include <hamartia_api/error_models/bhive/fpAddFaultModel.h>
#include <hamartia_api/error_models/bhive/fpMulFaultModel.h>
#include <hamartia_api/error_models/bhive/fpDivFaultModel.h>

class BhiveError : public hamartia_api::ErrorModel
{

    private:
    // Voltage level (unit: volt * 10)
    uint32_t voltage;
    uint32_t trial_cnt;

    static const uint32_t TRIAL_LIMIT = 30; 
    static const uint32_t MIN_VOLTAGE = 5;
    static const uint32_t MAX_OP_WIDTH = 64;
    static const uint64_t BHIVE_MAX_RANGE = 1000000;
    enum {
        BHIVE_CORRECT_RANGE = 0,
        BHIVE_GLITCH_RANGE = 1,
        BHIVE_PREV_CORRECT_RANGE = 2,
        BHIVE_PREV_OBSERVED_RANGE = 3
    };
    typedef std::map<hamartia_api::Operation, basicFaultModel*> op2model_type;
    op2model_type* _initIntMap();
    op2model_type* _initFpMap();

    void _ResetTrialCnt() { trial_cnt = 1; }

    bool _DecVoltage()
    {
        if (voltage > MIN_VOLTAGE) {
            --voltage;
            return true;
        } else {
            return false;
        }
    }

    /**
     * Generic b-HiVE injection
     *
     * \param model    Pointer to fault model 
     * \param op_width OP width in bits
     * \param pc_val   Previous instruction correct value (see paper)
     * \param po_val   Previous instruction observed value (see paper)
     * \param cc_val   Current instruction correct value (see paper)
     * \param co_val   Current instruction observed value (see paper) (OUTPUT)
     * \return Successful (no runtime errors)
     */
    bool _Inject(basicFaultModel* model, const uint32_t & op_width, 
                 const hamartia_api::DataValue & pc_val, const hamartia_api::DataValue & po_val,
                 const hamartia_api::DataValue & cc_val,       hamartia_api::DataValue & co_val)
    {
        uint64_t mask = 0; // bit-wise diff between cc_val and co_val

        uint64_t notXorPC_CC = ~ ( pc_val.uint() ^ cc_val.uint());
        uint64_t notXorPO_CC = ~ ( po_val.uint() ^ cc_val.uint());

        for (uint32_t i = 0; i < op_width; ++i) {

            uint64_t bitMask = (1UL<<i);
            // determine the error class and set mask
            int correctRange = model->getIt(voltage, i, BHIVE_CORRECT_RANGE);
            int glitchRange = correctRange + model->getIt(voltage, i, BHIVE_GLITCH_RANGE);
            int prevCorrectRange = glitchRange + model->getIt(voltage, i, BHIVE_PREV_CORRECT_RANGE);

            int rVal = hamartia_api::utils::mt_random() % BHIVE_MAX_RANGE;

            if (rVal <= correctRange) { // Correct
                //Do nothing
            } else if (rVal <= glitchRange) { // Glitch
                mask |= bitMask;
            } else if (rVal <= prevCorrectRange) { // Previous Correct
                bool bitNotEqual = ((notXorPC_CC & bitMask) == 0);
                if (bitNotEqual) {
                    mask |= bitMask;
                }
            } else { // Previous Observed
                bool bitNotEqual = ((notXorPO_CC & bitMask) == 0);
                if (bitNotEqual) {
                    mask |= bitMask;
                }
            }           
        }    
        co_val.uint(cc_val.uint() ^ mask);

        return true;
    }

    /**
     * Log the voltage level at which an error is successfully injected
     */
    void _LogSuccessVoltage(hamartia_api::Log *log)
    {
        std::stringstream ss;
        ss << voltage/10.0;
        log->ModelAddLogEntry("success_voltage", "volt", ss.str());
    }

    /**
     * Log the injected context
     */
    void _Log(hamartia_api::Log *log, uint64_t pc_val, uint64_t po_val, uint64_t cc_val, uint64_t co_val)
    {
        log->ModelAddLogEntry("context", "previous_correct", std::to_string(pc_val));
        log->ModelAddLogEntry("context", "previous_observed", std::to_string(po_val));
        log->ModelAddLogEntry("context", "current_correct", std::to_string(cc_val));
        std::stringstream vt_ss;
        vt_ss << voltage/10.0 << "-" << trial_cnt;
        log->ModelAddLogEntry("volt_trial->current_observed", vt_ss.str(), std::to_string(co_val));

        _LogSuccessVoltage(log);
    }


    public:

    BhiveError(std::string _name, uint32_t _voltage) : ErrorModel(_name)
    {
        voltage = _voltage; 
        trial_cnt = 0;
    }

    bool InjectError(hamartia_api::ErrorContext *cxt, YAML::Node *config, hamartia_api::Log *log)
    {
        // Initialize maps from operation type to fault model (only run once)
        static std::unique_ptr<op2model_type> p_IntMap(_initIntMap());
        static std::unique_ptr<op2model_type> p_FpMap(_initFpMap());
        
        // Lower voltage to increase error rate if we've been unable to generate error for a while
        if(trial_cnt++ > TRIAL_LIMIT) {
            if(_DecVoltage()) { // successfully lowers voltage 
                INFO_PRINT("Decrement voltage by 0.1V...")
                _ResetTrialCnt();
            } else {
                cxt->error_info.setResponse(hamartia_api::RESP_INVALID_ERROR, "Masked even at the lowest voltage level");
                _LogSuccessVoltage(log);
                return true;
            }    
        }    

        // Lookup fault model
        basicFaultModel* model = nullptr;
        hamartia_api::Operation op = cxt->instruction_data.instruction.operation;
        hamartia_api::DataType dp = cxt->instruction_data.data_type;

        if(dp == hamartia_api::DT_FLOAT || dp == hamartia_api::DT_DOUBLE) { // FP operations
            if (p_FpMap->find(op) != p_FpMap->end()) { // Operation has error rate info
                model = (*p_FpMap)[op];
            } else {
                cxt->error_info.setResponse(hamartia_api::RESP_NO_MODEL, "No error rate info for this instruction type");
                return false;
            }    
        } else {
            if (p_IntMap->find(op) != p_IntMap->end()) { // Operation has error rate info
                model = (*p_IntMap)[op];
            } else {
                cxt->error_info.setResponse(hamartia_api::RESP_NO_MODEL, "No error rate info for this instruction type");
                return false;
            }    
        }    

        uint32_t op_width = cxt->instruction_data.width;
        hamartia_api::DataValue prev_correct_result = cxt->getPrevOutputOldValue();
        hamartia_api::DataValue prev_observed_result = cxt->getPrevOutputValue();
        hamartia_api::DataValue correct_result = cxt->getOutputValue();
        hamartia_api::DataValue observed_result;

        _Inject(model, op_width, prev_correct_result, prev_observed_result, correct_result, observed_result);

        cxt->setOutput(observed_result);
        cxt->error_info.successResponse();

        _Log(log, prev_correct_result.uint(), prev_observed_result.uint(), correct_result.uint(), observed_result.uint());

        return true;
    }

    bool PreInjectError(hamartia_api::ErrorContext *cxt, YAML::Node *config, hamartia_api::Log *log)
    {
        // never called 
        return false;
    }

};

BhiveError::op2model_type* BhiveError::_initIntMap()
{
    BhiveError::op2model_type* p = new op2model_type;
    using namespace hamartia_api;
    (*p)[OP_ADD] = new addFaultModel();
    (*p)[OP_SUB] = new addFaultModel();
    (*p)[OP_MUL] = new mul64FaultModel();
    (*p)[OP_OR]  = new orrFaultModel();
    (*p)[OP_AND] = new andFaultModel();
    (*p)[OP_XOR] = new xorFaultModel();

    return p;
}

BhiveError::op2model_type* BhiveError::_initFpMap()
{
    BhiveError::op2model_type* p = new op2model_type;
    using namespace hamartia_api;
    (*p)[OP_ADD] = new fpAddFaultModel();
    (*p)[OP_SUB] = new fpAddFaultModel();
    (*p)[OP_MUL] = new fpMulFaultModel();
    (*p)[OP_DIV] = new fpDivFaultModel();

    return p;
}


#endif
/** @} */
