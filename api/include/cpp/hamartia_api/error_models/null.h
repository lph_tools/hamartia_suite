// Copyright 2015, The University of Texas at Austin 
// All rights reserved.
// 
// THIS FILE IS PART OF THE HAMARTIA FAULT/ERROR ANALYSIS SUITE
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met: 
// 
// 1. Redistributions of source code must retain the above copyright
// notice, this list of conditions and the following disclaimer. 
// 
// 2. Redistributions in binary form must reproduce the above copyright
// notice, this list of conditions and the following disclaimer in the
// documentation and/or other materials provided with the distribution. 
// 
// 3. Neither the name of the copyright holder nor the names of its
// contributors may be used to endorse or promote products derived from
// this software without specific prior written permission. 
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
// FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
// COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
// BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
// ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

/**
 *  \file
 *  \brief Null error model (for overhead profiling)
 *
 *  \version 1.0.0
 *  \author Chun-Kai Chang, University of Texas
 *
 *  \addtogroup error_models
 *  @{
 */

#ifndef _NULL_ERROR_H
#define _NULL_ERROR_H

#include <hamartia_api/types.h>
#include <hamartia_api/utils/error.h>
#include <hamartia_api/error_model.h>

class NullError : public hamartia_api::ErrorModel
{

    public:
    /**
     * Create NullError instance
     *
     * \param _name Error model name
     */
    NullError(std::string _name) : ErrorModel(_name) {}

    /**
     * Inject randbit error
     *
     * \param cxt    Injection context
     * \param config YAML config file for configuring the model
     * \param log    Log for adding extra logging information
     *
     * \return Successful (no runtime errors)
     */
    bool InjectError(hamartia_api::ErrorContext *cxt, YAML::Node *config, hamartia_api::Log *log)
    {
        return true;
    }

    bool PreInjectError(hamartia_api::ErrorContext *cxt, YAML::Node *config, hamartia_api::Log *log)
    {
        // never called
        return false;
    }
};

#endif
/** @} */
