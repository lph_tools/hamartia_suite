// Copyright 2015, The University of Texas at Austin 
// All rights reserved.
// 
// THIS FILE IS PART OF THE HAMARTIA FAULT/ERROR ANALYSIS SUITE
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met: 
// 
// 1. Redistributions of source code must retain the above copyright
// notice, this list of conditions and the following disclaimer. 
// 
// 2. Redistributions in binary form must reproduce the above copyright
// notice, this list of conditions and the following disclaimer in the
// documentation and/or other materials provided with the distribution. 
// 
// 3. Neither the name of the copyright holder nor the names of its
// contributors may be used to endorse or promote products derived from
// this software without specific prior written permission. 
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
// FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
// COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
// BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
// ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

/**
 *  \file
 *  \brief Timing error model
 *
 *  \version 1.1.0
 *  \author Chun-Kai Chang, University of Texas
 *
 *  \addtogroup error_models
 *  @{
 */

#ifndef _TIMING_LUT_ERROR_H
#define _TIMING_LUT_ERROR_H

#include <hamartia_api/types.h>
#include <hamartia_api/utils/error.h>
#include <hamartia_api/error_model.h>

// error rate tables for each circuit
#include "base_LUT.h"
#include "sub_LUT.h"
#include "fp_add_LUT.h"
#include "fp_sub_LUT.h"
#include "fp_mult_LUT.h"
#include "fp_div_LUT.h"

class TimingLUTError : public hamartia_api::ErrorModel
{
    private:
    uint32_t voltage;

    typedef std::map<hamartia_api::Operation, BaseLUT*> op2lut_t;
    op2lut_t* _initIntMap();
    op2lut_t* _initFpMap();

    bool _Inject(BaseLUT* lut, const hamartia_api::DataValue& prev_val, const hamartia_api::DataValue& cur_val,
                 hamartia_api::DataValue& ret_val)
    {
        static std::random_device rd;
        static std::mt19937 gen(rd()); 

        // Determine how many bits to flip
        std::vector<float> nw = lut->getNumWeights(voltage);
        if(nw.empty()) { // no injection if nw is empty
            ret_val.uint(cur_val.uint());
            return true;
        }    
        std::discrete_distribution<> num_dist(nw.begin(), nw.end()); 
        int num_bits = num_dist(gen);

        // Determine which bits to flip

        // Sampling without replacement: https://stackoverflow.com/questions/53632441/c-sampling-from-discrete-distribution-without-replacement
        std::vector<float> pw = lut->getPosWeights(voltage, num_bits);

        std::uniform_real_distribution<double> ud(0.0, 1.0);
        std::vector<float> vals;
        for (auto i : pw)
            vals.push_back(std::pow(ud(gen), 1. / i));

        using p_t = std::pair<int, float>; // (index, value) 
        std::vector<p_t> iv; 
        for(size_t i = 0; i < vals.size(); i++)
            iv.emplace_back(i, vals[i]);

        std::sort(iv.begin(), iv.end(), [](p_t x, p_t y) {return x.second > y.second;});

        // Set the difference mask
        uint64_t mask = 0; 
        uint64_t toggled_bit_mask = prev_val.uint() ^ cur_val.uint();
        for (int i = 0; i < num_bits; ++i) {
            // Generate a random position to inject
            int pos = iv[i].first;
            // Version 1:
            // we don't need to consider difference between prev and cur
            // because the data already encode that
            //mask |= (1UL << pos);   

            // Version 2:
            // flip only when prev and cur bit are different
            uint64_t test_bit = 1UL << pos;
            if (test_bit & toggled_bit_mask) {
                mask |= test_bit;   
            }    
        }

        ret_val.uint(cur_val.uint() ^ mask);

        return true;
    } 

    public:
    TimingLUTError(std::string _name, uint32_t _voltage) : ErrorModel(_name)
    {
        voltage = _voltage; 
    }

    bool InjectError(hamartia_api::ErrorContext *cxt, YAML::Node *config, hamartia_api::Log *log)
    {
        // Initialize maps from operation type to look-up table (only run once)
        static std::unique_ptr<op2lut_t> p_IntMap(_initIntMap());
        static std::unique_ptr<op2lut_t> p_FpMap(_initFpMap());
        
        uint32_t op_width = cxt->instruction_data.width;
        uint32_t num_packed = cxt->instruction_data.simd_width;
        // short circuit - we know 32-bit operations do not suffer from timing errors
        if (op_width == 32 || op_width / num_packed == 32) {
            //cxt->error_info.setResponse(hamartia_api::RESP_NO_MODEL, "No timing errors for 32-bt operations");
            return true;
        }    

        // Get look-up tables
        BaseLUT* lut = nullptr;
        hamartia_api::Operation op = cxt->instruction_data.instruction.operation;
        hamartia_api::DataType dp = cxt->instruction_data.data_type;
        if(dp == hamartia_api::DT_FLOAT || dp == hamartia_api::DT_DOUBLE) { // FP operations
            if (p_FpMap->find(op) != p_FpMap->end()) { // Operation has error rate info
                lut = (*p_FpMap)[op];
            } else {
                //cxt->error_info.setResponse(hamartia_api::RESP_NO_MODEL, "No error rate info for this instruction type");
                return true;
            }    
        } else { // INT operations
            if (p_IntMap->find(op) != p_IntMap->end()) { // Operation has error rate info
                lut = (*p_IntMap)[op];
            } else {
                //cxt->error_info.setResponse(hamartia_api::RESP_NO_MODEL, "No error rate info for this instruction type");
                return true;
            }    
        }    

        for (uint32_t lane = 0; lane < num_packed; lane++) {
            hamartia_api::DataValue prev_result = cxt->getPrevOutputValue(0, lane);
            hamartia_api::DataValue correct_result = cxt->getOutputValue(0, lane);
            hamartia_api::DataValue observed_result;

            _Inject(lut, prev_result, correct_result, observed_result);

            cxt->setOutput(observed_result, 0, lane);
        }
        cxt->error_info.successResponse();

        return true;
    }

    bool PreInjectError(hamartia_api::ErrorContext *cxt, YAML::Node *config, hamartia_api::Log *log)
    {
        // never called 
        return false;
    } 
};


TimingLUTError::op2lut_t* TimingLUTError::_initIntMap()
{
    TimingLUTError::op2lut_t* p = new op2lut_t;
    using namespace hamartia_api;
    //(*p)[OP_ADD] = new AddLUT();
    //(*p)[OP_SUB] = new SubLUT();

    return p;
}    

TimingLUTError::op2lut_t* TimingLUTError::_initFpMap()
{
    TimingLUTError::op2lut_t* p = new op2lut_t;
    using namespace hamartia_api;
    (*p)[OP_ADD] = new FpAddLUT();
    (*p)[OP_SUB] = new FpSubLUT();
    (*p)[OP_MUL] = new FpMultLUT();
    (*p)[OP_DIV] = new FpDivLUT();

    return p;
}    
    
#endif
/** @} */
