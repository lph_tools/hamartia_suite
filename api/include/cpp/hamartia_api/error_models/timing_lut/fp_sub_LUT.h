// Copyright 2015, The University of Texas at Austin 
// All rights reserved.
// 
// THIS FILE IS PART OF THE HAMARTIA FAULT/ERROR ANALYSIS SUITE
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met: 
// 
// 1. Redistributions of source code must retain the above copyright
// notice, this list of conditions and the following disclaimer. 
// 
// 2. Redistributions in binary form must reproduce the above copyright
// notice, this list of conditions and the following disclaimer in the
// documentation and/or other materials provided with the distribution. 
// 
// 3. Neither the name of the copyright holder nor the names of its
// contributors may be used to endorse or promote products derived from
// this software without specific prior written permission. 
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
// FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
// COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
// BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
// ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

/**
 *  \file
 *  \brief Look-up tables for floating-point subtraction circuit
 *
 *  \version 1.1.0
 *  \author Chun-Kai Chang, University of Texas
 *
 *  \addtogroup error_models
 *  @{
 */


#ifndef _FP_SUB_LUT_H
#define _FP_SUB_LUT_H

#include "base_LUT.h"

class FpSubLUT : public BaseLUT
{
    public:
    FpSubLUT() {};
    ~FpSubLUT() {};

    const static float v85_n[64];
    const static float v78_n[64];
    const static float v85_w[64][64];
    const static float v78_w[64][64];

    std::vector<float> getNumWeights(int volt)
    {
        std::vector<float> v;
        switch(volt) {
            case 85:
                v.assign(std::begin(v85_n), std::end(v85_n));
                break;
            case 78:
                v.assign(std::begin(v78_n), std::end(v78_n));
                break;
            default:
                break;
        }    
        return v;
    }

    std::vector<float> getPosWeights(int volt, int num_bits)
    {
        std::vector<float> v;
        switch(volt) {
            case 85:
                v.assign(std::begin(v85_w[num_bits]), std::end(v85_w[num_bits]));
                break;
            case 78:
                v.assign(std::begin(v78_w[num_bits]), std::end(v78_w[num_bits]));
                break;
            default:
                break;
        }   
        return v;
    }

};

const float FpSubLUT::v85_n[64] = {
#include "raw_data/85/fp_sub_64b_2_stages.num_bits"
};

const float FpSubLUT::v78_n[64] = {
#include "raw_data/78/fp_sub_64b_2_stages.num_bits"
};

const float FpSubLUT::v85_w[64][64] = {
#include "raw_data/85/fp_sub_64b_2_stages.positions"
};

const float FpSubLUT::v78_w[64][64] = {
#include "raw_data/78/fp_sub_64b_2_stages.positions"
};

#endif
