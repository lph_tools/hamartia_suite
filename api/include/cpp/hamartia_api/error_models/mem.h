// Copyright 2015, The University of Texas at Austin 
// All rights reserved.
// 
// THIS FILE IS PART OF THE HAMARTIA FAULT/ERROR ANALYSIS SUITE
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met: 
// 
// 1. Redistributions of source code must retain the above copyright
// notice, this list of conditions and the following disclaimer. 
// 
// 2. Redistributions in binary form must reproduce the above copyright
// notice, this list of conditions and the following disclaimer in the
// documentation and/or other materials provided with the distribution. 
// 
// 3. Neither the name of the copyright holder nor the names of its
// contributors may be used to endorse or promote products derived from
// this software without specific prior written permission. 
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
// FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
// COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
// BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
// ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

/**
 *  \file
 *  \brief Memory error model
 *
 *  \version 1.1.0
 *  \author Chun-Kai Chang, University of Texas
 *
 *  \addtogroup error_models
 *  @{
 */

#ifndef _MEM_ERROR_H
#define _MEM_ERROR_H

#include <hamartia_api/types.h>
#include <hamartia_api/utils/error.h>
#include <hamartia_api/error_model.h>

#include <stdlib.h> //strtoul

class MemError : public hamartia_api::ErrorModel
{

    private:
    typedef enum {
        FT_HIGH,    ///< stuck-at high
        FT_LOW,     ///< stuck-at low
        FT_FLIP,    ///< flip
        FT_END
    } FaultType;    

    uint8_t fault_type;

    /**
     * Split the fault location mask string into vectors
     */
    std::vector<uint64_t> _getFaultMask(std::string fstr)
    {
        std::string delim = ":";
        std::vector<uint64_t> v;
        size_t start = 0;
        size_t end = 0;
        while ((end = fstr.find(delim, start)) != std::string::npos)
        {
            uint64_t e = strtoul(fstr.substr(start, end - start).c_str(), nullptr, 16);
            v.push_back(e);
            start = end + delim.length();
        }

        uint64_t e = strtoul(fstr.substr(start, end).c_str(), nullptr, 16);
        v.push_back(e); 

        return v;
    }

    /**
     * Modify value based on fault_type and fault_mask 
     */
    hamartia_api::DataValue _Inject(uint8_t fault_type, uint64_t fault_mask, hamartia_api::DataValue old_val)
    {
        hamartia_api::DataValue mod_val;

        switch (fault_type) {
            case FT_HIGH:
                mod_val.uint(old_val.uint() | fault_mask);
                return mod_val;
            case FT_LOW:
                mod_val.uint(old_val.uint() & (~fault_mask));
                return mod_val;
            case FT_FLIP:
                mod_val.uint(old_val.uint() ^ fault_mask);
                return mod_val;
            default:
                return old_val; // unchanged
        }
    }

    public:
    /**
     * Create MemError instance
     *
     * \param _name Error model name
     * \param _fault_type fault type
     */
    MemError(std::string _name, uint8_t _fault_type) : ErrorModel(_name)
    {
        fault_type = _fault_type;
    }

    /**
     * Pre-Inject randbit error
     *
     * \param cxt    Injection context
     * \param config YAML config file for configuring the model
     * \param log    Log for adding extra logging information
     *
     * \return Successful (no runtime errors)
     */
    bool PreInjectError(hamartia_api::ErrorContext *cxt, YAML::Node *config, hamartia_api::Log *log)
    {
        // Use the InjectionTrigger field in the ErrorContext to find the target input operand(s) and target bit locations
        // Each InjectionFilter specify the fault pattern of an input operand
        //
        // The following fields of InjectionFilter are overloaded with the following meanings:
        // - int_match: specifies the index of the target operand
        //
        // - str_match: specifies the bit locations to inject with ":" delimiting each SIMD lane
        //      e.g., "0x11:0x00:0x00:0x00" means injecting bit 0 and bit 4 of the 0-th SIMD lane
        //            "0x01:0x02:0x00:0x00" means injecting bit 0 of lane 0 and bit 1 of lane 1  

        
        hamartia_api::InstructionData& inst_data = cxt->instruction_data;
        unsigned num_op_to_inject = cxt->trigger.filters.size();

        for (unsigned i = 0; i < num_op_to_inject; i++) {
            const hamartia_api::InjectionFilter& filter = cxt->trigger.filters[i];
            int op_idx = filter.int_match;
            hamartia_api::Operand &op = inst_data.inputs[op_idx];
             
            std::vector<uint64_t> fault_masks = _getFaultMask(filter.str_match);

            if (fault_masks.size() != op.simd_width) {
                cxt->error_info.setResponse(hamartia_api::RESP_ERROR, "No. of fault masks != No. of SIMD lanes");
                return false;
            }

            for (unsigned lane = 0; lane < fault_masks.size(); lane++) { // simd lanes 
                hamartia_api::DataValue old_val = op.value[lane];
                uint64_t fault_mask = fault_masks[lane];
                
                op.setValue(_Inject(fault_type, fault_mask, old_val), lane);
            }
        } 

        return true;
    }

    bool InjectError(hamartia_api::ErrorContext *cxt, YAML::Node *config, hamartia_api::Log *log)
    {
        // never called
        return false;
    }
};

#endif
/** @} */
