// Copyright 2015, The University of Texas at Austin 
// All rights reserved.
// 
// THIS FILE IS PART OF THE HAMARTIA FAULT/ERROR ANALYSIS SUITE
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met: 
// 
// 1. Redistributions of source code must retain the above copyright
// notice, this list of conditions and the following disclaimer. 
// 
// 2. Redistributions in binary form must reproduce the above copyright
// notice, this list of conditions and the following disclaimer in the
// documentation and/or other materials provided with the distribution. 
// 
// 3. Neither the name of the copyright holder nor the names of its
// contributors may be used to endorse or promote products derived from
// this software without specific prior written permission. 
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
// FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
// COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
// BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
// ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

/**
 *  \file
 *  \brief Burst error model
 *
 *  \version 1.0.0
 *  \author Michael Sullivan, University of Texas
 *  \author Nicholas Kelly, University of Texas
 *
 *  \addtogroup error_models
 *  @{
 */

#ifndef _BURST_ERROR_H
#define _BURST_ERROR_H

#include <hamartia_api/types.h>
#include <hamartia_api/utils/error.h>
#include <hamartia_api/error_model.h>

class BurstError : public hamartia_api::ErrorModel
{

    private:
    // Burst bits
    uint64_t bits;

    public:
    /**
     * Create BurstError instance
     *
     * \param _name Error model name
     * \param _bits Number of error bits to inject
     */
    BurstError(std::string _name, uint64_t _bits) : ErrorModel(_name)
    {
        bits = _bits;
    }

    /**
     * Generic Burst Injection
     *
     * \param op_width   Operand width   
     * \param num_packed SIMD width
     * \param old_val    Original value
     * \param mod_val    Modified value
     *
     * \return Successful (no runtime errors)
     */
    bool _Inject(uint32_t op_width, uint32_t num_packed, hamartia_api::DataValue* old_val, hamartia_api::DataValue* mod_val)
    {
        uint32_t lane_width = op_width / num_packed;
        if (lane_width == 8) {
            // Inject into byte
            mod_val->uint(hamartia_api::utils::BB_inject <uint8_t> (old_val->uint(), (0x1 << bits) - 1, 0, 8 - bits));

        } else if (lane_width == 16) {
            // Inject into word (16bit)
            mod_val->uint(hamartia_api::utils::BB_inject <uint16_t> (old_val->uint(), (0x1 << bits) - 1, 0, 16 - bits));

        } else if (lane_width == 32) {
            // Inject into double word (32bit)
            mod_val->uint(hamartia_api::utils::BB_inject <uint32_t> (old_val->uint(), (0x1 << bits) - 1, 0, 32 - bits));

        } else if (lane_width == 64) {
            // Inject into quad word (64bit)
            mod_val->uint(hamartia_api::utils::BB_inject <uint64_t> (old_val->uint(), (0x1 << bits) - 1, 0, 64 - bits));

        } else {
            mod_val->uint(hamartia_api::utils::BB_inject <uint64_t> (old_val->uint(), (0x1 << bits) - 1, 0, 64 - bits));
        }

        return true;
    }

    /**
     * Pre-Inject burst error
     *
     * \param cxt    Injection context
     * \param config YAML config file for configuring the model
     * \param log    Log for adding extra logging information
     *
     * \return Successful (no runtime errors)
     */
    bool PreInjectError(hamartia_api::ErrorContext *cxt, YAML::Node *config, hamartia_api::Log *log)
    {
        uint32_t op_width   = cxt->instruction_data.width;
        uint32_t num_packed = cxt->instruction_data.simd_width;
        //FIXME: Currently inject to 1st input operand. More generic in the future?
        hamartia_api::DataValue old_input = cxt->getInputValue();
        hamartia_api::DataValue mod_input;

        _Inject(op_width, num_packed, &old_input, &mod_input);

        cxt->setInput(mod_input);

        return true;
    }

    /**
     * Inject burst error
     *
     * \param cxt    Injection context
     * \param config YAML config file for configuring the model
     * \param log    Log for adding extra logging information
     *
     * \return Successful (no runtime errors)
     */
    bool InjectError(hamartia_api::ErrorContext *cxt, YAML::Node *config, hamartia_api::Log *log)
    {
        uint32_t op_width   = cxt->instruction_data.width;
        uint32_t num_packed = cxt->instruction_data.simd_width;
        hamartia_api::DataValue old_result = cxt->getOutputValue();
        hamartia_api::DataValue mod_result;

        _Inject(op_width, num_packed, &old_result, &mod_result);

        cxt->setOutput(mod_result);

        return true;
    }

};

#endif
/** @} */
