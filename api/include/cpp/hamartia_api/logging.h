// Copyright 2015, The University of Texas at Austin 
// All rights reserved.
// 
// THIS FILE IS PART OF THE HAMARTIA FAULT/ERROR ANALYSIS SUITE
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met: 
// 
// 1. Redistributions of source code must retain the above copyright
// notice, this list of conditions and the following disclaimer. 
// 
// 2. Redistributions in binary form must reproduce the above copyright
// notice, this list of conditions and the following disclaimer in the
// documentation and/or other materials provided with the distribution. 
// 
// 3. Neither the name of the copyright holder nor the names of its
// contributors may be used to endorse or promote products derived from
// this software without specific prior written permission. 
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
// FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
// COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
// BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
// ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

/**
 * \file
 * \brief Logging (main log output and misc model log)
 * \author Nicholas Kelly, University of Texas
 * \author Chun-Kai Chang, University of Texas
 *
 * \addtogroup logging
 * @{
 */

#ifndef _HAMARTIA_API_LOGGING_H
#define _HAMARTIA_API_LOGGING_H

#include <hamartia_api/debug.h>
#include <hamartia_api/interface.h>
#include <hamartia_api/types.h>

// Libs
#include <sstream>
#include <iostream>
#include <fstream>
#include <vector>
#include <map>
#include <yaml-cpp/yaml.h>

namespace hamartia_api 
{
    /**
     * Error injection data (used for logging)
     */
    class Log {
        private:
        /// Error model name
        std::string run_emodel;
        /// Detector model name
        std::string run_dmodel;

        /// Injection statistics
        InjectionStats statistics;
        
        /// Initial (pre-injection) instruction data (including operands)
        std::vector<InstructionData> instruction_data;

        // Location/context
        std::vector< std::map<std::string, std::string> > location;

        /// Injected input operands (normally the same)
        std::vector<Operand> injected_inputs;
        /// Injected output operands
        std::vector<Operand> injected_outputs;
        /// Recorded erroneous input operands
        std::vector< std::vector<Operand> > recorded_inputs;
        /// Recorded erroneous output operands
        std::vector< std::vector<Operand> > recorded_outputs;

        /// Misc (for error model use)
        std::map<std::string, std::map<std::string, std::string> > misc;

        public:
        /// Constructor
        Log() {}
        /// Destructor
        ~Log() {}

        /**
         * Convert int to HEX string
         *
         * \param uint Int to convert
         * \return HEX string
         */
        static std::string ToHex(uint64_t uint);

        /**
         * Construct a YAML node for an Operand
         *
         * \param e  YAML emitter (destination)
         * \param op Operand to display
         */
        static void YAMLOperand(YAML::Emitter &e, const Operand &op);

        /**
         * Construct a YAML node for Operand differences
         *
         * \param e   YAML emitter (destination)
         * \param op1 First operand
         * \param op2 Second operand
         */
        static void YAMLOperandDiff(YAML::Emitter &e, const Operand &op1, const Operand &op2);

        /**
         * Construct a YAML node for InstructionData
         *
         * \param e         YAML emitter (destination)
         * \param inst_data InstructionData
         */
        static void YAMLInstructionData(YAML::Emitter &e, const InstructionData &inst_data);

        /**
         * Construct a YAML node for an Instruction
         *
         * \param e    YAML emitter (destination)
         * \param inst Instruction
         */
        static void YAMLInstruction(YAML::Emitter &e, const Instruction &inst);

        /**
         * Construct a YAML node for an Address
         *
         * \param e    YAML emitter (destination)
         * \param addr Address
         */
        static void YAMLAddress(YAML::Emitter &e, const Address &addr);

        /**
         * Construct a YAML node for InjectionStats
         *
         * \param e     YAML emitter (destination)
         * \param stats InjectionStats
         */
        static void YAMLInjectionStats(YAML::Emitter &e, const InjectionStats &stats);

        /**
         * Construct a YAML node for a string/string map
         *
         * \param e    YAML emitter (destination)
         * \param map  Map 
         */
        static void YAMLMap(YAML::Emitter &e, const std::map<std::string, std::string> &map);

        /**
         * Dump an injection location to an YAML node
         *
         * \param e    YAML emitter (destination)
         * \param loc_idx  Location index 
         */
        void DumpLocation(YAML::Emitter &e, uint32_t loc_idx=0);

        /**
         * Dump model specific log to an YAML node
         *
         * \param e    YAML emitter (destination)
         */
        void DumpMisc(YAML::Emitter &e);

        /**
         * Add an entry to the model-specific log
         *
         * \param model_name Name of the error/detector model
         * \param key        Key/label of the entry
         * \param value      Value of the entry
         */
        template<typename T>
        void ModelAddLogEntry(std::string model_name, std::string key, T value)
        {
            std::stringstream ss;
            ss << value;
            misc[model_name][key] = ss.str();
        }

        /**
         * Add an entry to the location/context log
         *
         * \param locMap  map containing source code location of current instruction
         */
        void LocationAddLogEntry(const std::map<std::string, std::string> &locMap)
        {
            location.push_back(locMap);
        }

        /**
         * Emit the YAML log to an output stream (file)
         *
         * \param log_file     Output stream
         * \return Success
         */
        bool Emit(std::ofstream &log_file);

        /**
         * Clear the log data
         */
        void Clear();

        /**
         * Record initial (clean) context information
         *
         * \param context Error context to capture
         */
        inline void LogInitialValues(ErrorContext &context)
        {
            // Instruction data
            instruction_data.push_back(context.instruction_data);
        }

        /**
         * Record injected context information
         *
         * \param context Error context to capture
         * \param record  Record ID (0 = injected, >1 = recorded, but not injected)
         */
        inline void LogInjectedValues(ErrorContext &context, uint64_t record = 0)
        {
            if (record == 0) {
                // Injected operands
                for (std::vector<Operand>::iterator oit = context.instruction_data.inputs.begin(); 
                     oit != context.instruction_data.inputs.end(); ++oit) {
                    injected_inputs.push_back(*oit);
                }
                for (std::vector<Operand>::iterator oit = context.instruction_data.outputs.begin();
                     oit != context.instruction_data.outputs.end(); ++oit) {
                    injected_outputs.push_back(*oit);
                }

                // Run data
                run_emodel = context.error_info.error_model;
                run_dmodel = context.error_info.detector_model;

                // InjectionStats
                statistics = ErrorContext::CurrentStats(&context);
            } else {
                // Record operands
                std::vector<Operand> rin;
                std::vector<Operand> rout;
                for (std::vector<Operand>::iterator oit = context.instruction_data.inputs.begin(); 
                     oit != context.instruction_data.inputs.end(); ++oit) {
                    rin.push_back(*oit);
                }
                for (std::vector<Operand>::iterator oit = context.instruction_data.outputs.begin();
                     oit != context.instruction_data.outputs.end(); ++oit) {
                    rout.push_back(*oit);
                }
                recorded_inputs.push_back(rin);
                recorded_outputs.push_back(rout);
            }
        }
    };
}

#endif
/** @} */
