// Copyright 2015, The University of Texas at Austin 
// All rights reserved.
// 
// THIS FILE IS PART OF THE HAMARTIA FAULT/ERROR ANALYSIS SUITE
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met: 
// 
// 1. Redistributions of source code must retain the above copyright
// notice, this list of conditions and the following disclaimer. 
// 
// 2. Redistributions in binary form must reproduce the above copyright
// notice, this list of conditions and the following disclaimer in the
// documentation and/or other materials provided with the distribution. 
// 
// 3. Neither the name of the copyright holder nor the names of its
// contributors may be used to endorse or promote products derived from
// this software without specific prior written permission. 
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
// FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
// COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
// BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
// ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

/*
 * \file
 * \brief Instruction data
 * \author Nicholas Kelly, University of Texas
 *
 * \addtogroup interface
 * @{
 */

#ifndef _HAMARTIA_API_INSTRUCTION_DATA_H
#define _HAMARTIA_API_INSTRUCTION_DATA_H

#include <cstdint>
#include <string>
#include <vector>

#include <hamartia_api/types.h>
#include <hamartia_api/interface/instruction_types.h>

/// Default destination operand
#define DEST_OPERAND 0
/// Default source operand
#define SRC_OPERAND 0

namespace hamartia_api
{
    /// Operand Type
    typedef enum 
    {
        OT_REGISTER,  ///< Register
        OT_IMMEDIATE, ///< Immediate value
        OT_MEMORY,    ///< Memory value
        OT_FLAGS,     ///< Flags register
        OT_STATUS     ///< Status register
    } OperandType;

    /// OperandType names
    static const char *OperandType_Names[] = {
        "OT_REGISTER",  // Register
        "OT_IMMEDIATE", // Immediate value
        "OT_MEMORY",    // Memory value
        "OT_FLAGS",     // Flags register
        "OT_STATUS"     // Status register
    };

    /**
     * Get OperandType name
     *
     * \param v OperandType
     *
     * \return Name
     */
    static std::string OperandType_Name(OperandType v) { return OperandType_Names[(uint32_t) v]; }

    /// Address
    typedef struct
    {
        uint64_t virt; ///< Virtual address
        uint64_t phys; ///< Physical address (not usually populated)
    } Address;

    /**
     * Instruction operand (I/O)
     */
    class Operand
    {
        public:
        OperandType            type;       ///< Operand type
        DataType               data_type;  ///< Data/element type
        uint16_t               width;      ///< Entire data width
        uint8_t                simd_width; ///< SIMD lanes (packed)
        std::vector<DataValue> value;      ///< Value
        std::vector<DataValue> old_value;  ///< Old value (before instruction was executed, valid for outputs only)
        uint32_t               reg_id;     ///< Register identifier
        std::string            reg;        ///< Register name
        Address                address;    ///< Memory address

        Operand();
        Operand(OperandType _type, DataType _data_type = DT_UINT, uint16_t _width = 32, uint8_t _simd_width = 1);
        ~Operand();

        /// Assignment
        Operand& operator=(const Operand &op);

        /**
         * Element size (SIMD)
         *
         * \return Element size (bits)
         */
        inline uint8_t elementSize() const
        {
            return width / simd_width;
        }

        /**
         * Set a value of an Operand (by DataValue)
         *
         * \param val   Value to set
         * \param lane  Value or SIMD lane to set
         */
        inline void setValue(DataValue val, uint8_t lane=0)
        {
            value[lane] = val;
        }

        /**
         * Set an Int value of an Operand
         *
         * \param val Value to set
         * \param lane  Value or SIMD lane to set
         */
        inline void setValueInt(uint64_t val, uint8_t lane=0)
        {
            value[lane].uint(val);
        }

        /**
         * Set an Float value of an Operand
         *
         * \param val Value to set
         * \param lane  Value or SIMD lane to set
         */
        inline void setValueFloat(fp32_t val, uint8_t lane=0)
        {
            value[lane].fp32(val);
        }

        /**
         * Set an Double value of an Operand
         *
         * \param val Value to set
         * \param lane  Value or SIMD lane to set
         */
        inline void setValueDouble(fp64_t val, uint8_t lane=0)
        {
            value[lane].fp64(val);
        }

        /**
         * Set an old_value of an Operand (by DataValue)
         *
         * \param val Value to set
         * \param lane  Value or SIMD lane to set
         */
        inline void setOldValue(DataValue val, uint8_t lane=0)
        {
            old_value[lane] = val;
        }

        /**
         * Set an Int old_value of an Operand (by DataValue)
         *
         * \param val Value to set
         * \param lane  Value or SIMD lane to set
         */
        inline void setOldValueInt(uint64_t val, uint8_t lane=0)
        {
            old_value[lane].uint(val);
        }

        /**
         * Set an Float old_value of an Operand (by DataValue)
         *
         * \param val Value to set
         * \param lane  Value or SIMD lane to set
         */
        inline void setOldValueFloat(fp32_t val, uint8_t lane=0)
        {
            old_value[lane].fp32(val);
        }

        /**
         * Set an Double old_value of an Operand (by DataValue)
         *
         * \param val Value to set
         * \param lane  Value or SIMD lane to set
         */
        inline void setOldValueDouble(fp64_t val, uint8_t lane=0)
        {
            old_value[lane].fp64(val);
        }

        inline bool hasReg() const
        {
            return (type == OT_REGISTER || type == OT_FLAGS || type == OT_STATUS);
        }    

        inline bool hasAddress() const
        {
            return (type == OT_MEMORY);
        }    
    };

    /**
     * Instruction information
     */
    class Instruction
    {
        public:
        uint32_t    opcode;    ///< Instruction OPCODE
        Operation   operation; ///< Generic operation type
        DataType    data;      ///< Data type
        Packing     packing;   ///< Packing type
        Condition   condition; ///< Condition (predication)
        std::string grouping;  ///< Grouping/category

        /// Constructor
        Instruction();
        /// Destructor
        ~Instruction();

        /// Assignment
        Instruction& operator=(const Instruction &inst);
    };

    /**
     * Instruction data
     */
    class InstructionData
    {
        public:
        Instruction          instruction; ///< Instruction
        std::string          asm_line;    ///< Assembly source-line
        std::string          asm_data;    ///< Assembly data
        Address              address;     ///< Instruction address
        DataType             data_type;   ///< Main data type
        uint32_t             simd_width;  ///< SIMD lanes (packed)
        uint32_t             width;       ///< Data width
        std::vector<Operand> inputs;      ///< Input operands (could be RW)
        std::vector<Operand> outputs;     ///< Output operands (could be RW)

        /// Constructor
        InstructionData();
        /// Destructor
        ~InstructionData();

        /// Assignment
        InstructionData& operator=(const InstructionData &inst_data);

        /**
         * Add an input
         *
         * \param _simd_width SIMD width
         *
         * \return New input
         */
        Operand * addInput(uint32_t _simd_width = 0);

        /**
         * Add an output
         *
         * \param _simd_width SIMD width
         *
         * \return New output
         */
        Operand * addOutput(uint32_t _simd_width = 0);
    };
}

#endif
/** @} */
