// Copyright 2015, The University of Texas at Austin 
// All rights reserved.
// 
// THIS FILE IS PART OF THE HAMARTIA FAULT/ERROR ANALYSIS SUITE
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met: 
// 
// 1. Redistributions of source code must retain the above copyright
// notice, this list of conditions and the following disclaimer. 
// 
// 2. Redistributions in binary form must reproduce the above copyright
// notice, this list of conditions and the following disclaimer in the
// documentation and/or other materials provided with the distribution. 
// 
// 3. Neither the name of the copyright holder nor the names of its
// contributors may be used to endorse or promote products derived from
// this software without specific prior written permission. 
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
// FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
// COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
// BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
// ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

/*
 * \file
 * \brief Instruction types (abstract mapping)
 * \author Nicholas Kelly, University of Texas
 *
 * \addtogroup interface
 * @{
 */

#ifndef _HAMARTIA_API_INSTRUCTION_TYPES_H
#define _HAMARTIA_API_INSTRUCTION_TYPES_H

#include <cstdint>
#include <string>
#include <vector>

namespace hamartia_api
{
    /// Operation (generic instructions)
    typedef enum 
    {
        // Arithmetic
        OP_ADD,      ///< Addition
        OP_SUB,      ///< Subtraction
        OP_MUL,      ///< Multiplication
        OP_DIV,      ///< Division
        OP_SQRT,     ///< Square root
        OP_SAR,      ///< Arithmetic shift right
        OP_SALC,     ///< Arithmetic shift left
        OP_SHR,      ///< Logical shift right
        OP_SHL,      ///< Logical shfit left
        // Compare
        OP_CMP,      ///< Compare
        OP_MIN,      ///< Minimum
        OP_MAX,      ///< Maximum
        // Logical
        OP_AND,      ///< Bitwise AND
        OP_OR,       ///< Bitwise OR
        OP_XOR,      ///< Bitwise XOR
        OP_NOT,      ///< Bitwise NOT
        // Register
        OP_MOV,      ///< Move
        OP_LEA,      ///< Load effective address
        OP_POP,      ///< Stack pop
        OP_PUSH,     ///< Stack push
        // Memory
        OP_LD,       ///< Load
        OP_ST,       ///< Store
        OP_PREFETCH, ///< Prefetch data
        // Control flow
        OP_CALL,     ///< Subroutine call
        OP_RET,      ///< Subroutine return
        OP_JMP,      ///< Jump
        OP_BR,       ///< Branch
        // SIMD
        OP_GATHER,   ///< Gather (vector)
        OP_SCATTER,  ///< Scatter (vector)
        OP_BLEND,    ///< Blend
        OP_PERMUTE,  ///< Permute
        // OS
        OP_SYSCALL,  ///< System-call (trap)
        // END
        OP_END
    } Operation;

    /// Operation names
    static const char *Operation_Names[] = {
        // Arithmetic
        "OP_ADD",      ///< Addition
        "OP_SUB",      ///< Subtraction
        "OP_MUL",      ///< Multiplication
        "OP_DIV",      ///< Division
        "OP_SQRT",     ///< Square root
        "OP_SAR",      ///< Arithmetic shift right
        "OP_SALC",     ///< Arithmetic shift left
        "OP_SHR",      ///< Logical shift right
        "OP_SHL",      ///< Logical shfit left
        // Compare
        "OP_CMP",      ///< Compare
        "OP_MIN",      ///< Minimum
        "OP_MAX",      ///< Maximum
        // Logical
        "OP_AND",      ///< Bitwise AND
        "OP_OR",       ///< Bitwise OR
        "OP_XOR",      ///< Bitwise XOR
        "OP_NOT",      ///< Bitwise NOT
        // Register
        "OP_MOV",      ///< Move
        "OP_LEA",      ///< Load effective address
        "OP_POP",      ///< Stack pop
        "OP_PUSH",     ///< Stack push
        // Memory
        "OP_LD",       ///< Load
        "OP_ST",       ///< Store
        "OP_PREFETCH", ///< Prefetch data
        // Control flow
        "OP_CALL",     ///< Subroutine call
        "OP_RET",      ///< Subroutine return
        "OP_JMP",      ///< Jump
        "OP_BR",       ///< Branch
        // SIMD
        "OP_GATHER",   ///< Gather (vector)
        "OP_SCATTER",  ///< Scatter (vector)
        "OP_BLEND",    ///< Blend
        "OP_PERMUTE",  ///< Permute
        // OS
        "OP_SYSCALL"   ///< System-call (trap)
    };

    /**
     * Get Operation name
     *
     * \param v Operation
     *
     * \return Operation name
     */
    static std::string Operation_Name(Operation v) { return Operation_Names[(uint32_t) v]; }

    /// Execution condition (predication)
    typedef enum
    {
        CN_NONE,          ///< None, always execute
        CN_OVERFLOW,      ///< If overflow (flag)
        CN_N_OVERFLOW,    ///< If not overflow (flag)
        CN_SIGN,          ///< If sign (flag)
        CN_N_SIGN,        ///< If not sign (flag)
        CN_EQUAL,         ///< If equal (flag)
        CN_N_EQUAL,       ///< If not equal (flag)
        CN_CARRY,         ///< If carry (flag)
        CN_N_CARRY,       ///< If not carry (flag)
        CN_LESS,          ///< If less than (flag)
        CN_GREATER_OR_EQ, ///< If greater than or equal to (flag)
        CN_LESS_OR_EQ,    ///< If less than or equal to (flag)
        CN_GREATER,       ///< If greater than (flag)
        CN_PARITY,        ///< If parity (flag)
        CN_N_PARITY,      ///< If not parity (flag)
        // END
        CN_EN
    } Condition;

    /// Condition names
    static const char *Condition_Names[] = {
        "CN_NONE",          ///< None, always execute
        "CN_OVERFLOW",      ///< If overflow (flag)
        "CN_N_OVERFLOW",    ///< If not overflow (flag)
        "CN_SIGN",          ///< If sign (flag)
        "CN_N_SIGN",        ///< If not sign (flag)
        "CN_EQUAL",         ///< If equal (flag)
        "CN_N_EQUAL",       ///< If not equal (flag)
        "CN_CARRY",         ///< If carry (flag)
        "CN_N_CARRY",       ///< If not carry (flag)
        "CN_LESS",          ///< If less than (flag)
        "CN_GREATER_OR_EQ", ///< If greater than or equal to (flag)
        "CN_LESS_OR_EQ",    ///< If less than or equal to (flag)
        "CN_GREATER",       ///< If greater than (flag)
        "CN_PARITY",        ///< If parity (flag)
        "CN_N_PARITY"       ///< If not parity (flag)
    };

    /**
     * Get Condition name
     *
     * \param v Condition
     *
     * \return Condition name
     */
    static std::string Condition_Name(Condition v) { return Condition_Names[(uint32_t) v]; }

    /// Data element type
    typedef enum
    {
        DT_UINT,   ///< Unsigned integer
        DT_INT,    ///< Signed integer
        DT_FLOAT,  ///< Single-precision floating point
        DT_DOUBLE, ///< Double-precision floating point
        DT_BYTES,  ///< Raw bytes
        // END
        DT_END
    } DataType;

    /// Datatype names
    static const char *DataType_Names[] = {
        "DT_UINT",   ///< Unsigned integer
        "DT_INT",    ///< Signed integer
        "DT_FLOAT",  ///< Single-precision floating point
        "DT_DOUBLE", ///< Double-precision floating point
        "DT_BYTES"   ///< Raw bytes
    };

    /**
     * Get Datatype name
     *
     * \param v Datatype
     *
     * \return Datatype name
     */
    static std::string DataType_Name(DataType v) { return DataType_Names[(uint32_t) v]; }

    /// Packing (SIMD)
    typedef enum
    {
        PK_SCALAR, ///< Scalar instruction
        PK_PACKED, ///< Packed (SIMD) instruction
        // END
        PK_END
    } Packing;

    /// Packing names
    static const char *Packing_Names[] = {
        "PK_SCALAR", ///< Scalar instruction
        "PK_PACKED"  ///< Packed (SIMD) instruction
    };

    /**
     * Get Packing name
     *
     * \param v Packing
     *
     * \return Packing name
     */
    static std::string Packing_Name(Packing v) { return Packing_Names[(uint32_t) v]; }
}

#endif
/** @} */
