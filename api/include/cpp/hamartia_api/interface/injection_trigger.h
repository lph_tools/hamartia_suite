// Copyright 2015, The University of Texas at Austin 
// All rights reserved.
// 
// THIS FILE IS PART OF THE HAMARTIA FAULT/ERROR ANALYSIS SUITE
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met: 
// 
// 1. Redistributions of source code must retain the above copyright
// notice, this list of conditions and the following disclaimer. 
// 
// 2. Redistributions in binary form must reproduce the above copyright
// notice, this list of conditions and the following disclaimer in the
// documentation and/or other materials provided with the distribution. 
// 
// 3. Neither the name of the copyright holder nor the names of its
// contributors may be used to endorse or promote products derived from
// this software without specific prior written permission. 
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
// FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
// COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
// BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
// ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

/*
 * \file
 * \brief Injection triggers
 * \author Nicholas Kelly, University of Texas
 *
 * \warning Experimental!
 *
 * \addtogroup interface
 * @{
 */

#ifndef _HAMARTIA_API_INJECTION_TRIGGER_H
#define _HAMARTIA_API_INJECTION_TRIGGER_H

#include <cstdint>
#include <string>
#include <vector>

namespace hamartia_api
{
    /// Filter types (watch points)
    typedef enum
    {
        FT_REG,  ///< Register filter
        FT_ADDR, ///< Adderss filter
        FT_FLAG, ///< Flag filter
        // END
        FT_END
    } FilterType;

    /**
     * Injection filter; indicates what instructions/events to invoke error model on
     */
    class InjectionFilter
    {
        public:
        FilterType  type;      ///< Filter type (what is being matched)
        bool        read;      ///< Match only when target is read
        bool        write;     ///< Match only when target is written to
        std::string str_match; ///< String value to match (e.g. register name)
        uint64_t    int_match; ///< Int value to match (e.g. address)

        /// Constructor
        InjectionFilter();
        /// Destructor
        ~InjectionFilter();
    };

    /**
     * Injection trigger (after initial injection)
     */
    class InjectionTrigger
    {
        public:
        bool                         cont;    ///< Continue calling error model
        std::vector<InjectionFilter> filters; ///< Filters

        /// Constructor
        InjectionTrigger();
        /// Destructor
        ~InjectionTrigger();

        inline void addFilter(const InjectionFilter& filter) 
        {
            filters.push_back(filter);
        }
    };
}

#endif
/** @} */

