// Copyright 2015, The University of Texas at Austin 
// All rights reserved.
// 
// THIS FILE IS PART OF THE HAMARTIA FAULT/ERROR ANALYSIS SUITE
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met: 
// 
// 1. Redistributions of source code must retain the above copyright
// notice, this list of conditions and the following disclaimer. 
// 
// 2. Redistributions in binary form must reproduce the above copyright
// notice, this list of conditions and the following disclaimer in the
// documentation and/or other materials provided with the distribution. 
// 
// 3. Neither the name of the copyright holder nor the names of its
// contributors may be used to endorse or promote products derived from
// this software without specific prior written permission. 
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
// FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
// COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
// BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
// ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

/*
 * \file
 * \brief Error Context, main class in API
 * \author Nicholas Kelly, University of Texas
 *
 * \addtogroup interface
 * @{
 */

#ifndef _HAMARTIA_API_ERROR_CONTEXT_H
#define _HAMARTIA_API_ERROR_CONTEXT_H

#include <vector>
#include <string>
#include <cstdint>

#include <hamartia_api/interface/instruction_data.h>
#include <hamartia_api/interface/injection_stats.h>
#include <hamartia_api/interface/injection_trigger.h>

namespace hamartia_api
{
    /// Model methods
    typedef enum {
        METHOD_NONE,         ///< No method
        // Pre/post process
        METHOD_SETUP,        ///< Setup method (alloc memory/data, preprocess)
        METHOD_CLEANUP,      ///< Cleanup method (dealloc memory/data)
        // Error model
        METHOD_PRE_INJECT,   ///< Pre-inject method
        METHOD_INJECT,       ///< Inject method
        // Detector model
        METHOD_CHECK_ERROR,  ///< Check error (detector)
        // End
        METHOD_END
    } ModelMethod;

    /// Response (error manager), in order of severity, categorized
    typedef enum
    {
        RESP_NONE,
        // Errors
        RESP_ERROR,         ///< Error (fatal)
        // Warnings
        RESP_WARNING,       ///< Warning
        RESP_NO_MODEL,      ///< No model exists (for name)
        RESP_NO_METHOD,     ///< Mechanism not defined
        // Successes
        RESP_SUCCESS,       ///< Model/command successfully completed (no specifics)
        RESP_INVALID_ERROR, ///< Error is masked or detected
        RESP_UPDATE,        ///< Update status/flag registers
        // End
        RESP_END
    } Resp;

    /**
     * Error information (models, method response)
     */
    class ErrorInfo
    {
        public:
        // MODELS
        /// Error model name
        std::string error_model;
        /// Error model config path
        std::string error_config;
        /// Detector model name
        std::string detector_model;
        /// Detector model config path
        std::string detector_config;

        // SETTINGS
        /// Target SIMD lane
        uint8_t target_simd_lane;

        // RESPONSE
        /// Method response
        Resp response;
        /// Method response message
        std::string response_message;

        /// Constructor
        ErrorInfo();
        /// Destructor
        ~ErrorInfo();

        /// Assignment
        ErrorInfo& operator=(const ErrorInfo &info);

        /*
         * Has an error model
         *
         * \return Error model present
         */
        bool hasErrorModel();

        /**
         * Has an error model config
         *
         * \return Error model config present
         */
        bool hasErrorConfig();

        /*
         * Has an detector model
         *
         * \return Detector model present
         */
        bool hasDetectorModel();

        /**
         * Has an detector model config
         *
         * \return Detector model config present
         */
        bool hasDetectorConfig();

        /**
         * Set success response
         */
        void successResponse();

        /**
         * Set error response
         *
         * \param message Optional error message
         */
        void errorResponse(std::string message);

        /**
         * Set arbitrary response
         *
         * \param resp    Response type
         * \param message Optional response message
         */
        void setResponse(Resp resp, std::string message = std::string());

        /**
         * Reset the response
         */
        void clearResponse();

        /**
         * Response exists
         *
         * \return Response exists
         */
        bool hasResponse();
    };

    /**
     * Error context, main API container
     */
    class ErrorContext
    {
        public:
        /// Error information
        ErrorInfo                   error_info;
        /// Instruction (to inject) data
        InstructionData             instruction_data;
        /// Instruction data of previous context (for errors that depend on history)
        /// CK: I could create another derived class but that complicated SWIG interface
        InstructionData             prev_instruction_data;
        /// Statistics stack (entry for each injection)
        std::vector<InjectionStats> statistics;
        /// Instruction triggers
        InjectionTrigger            trigger;

        /// Constructor
        ErrorContext();
        /// Destructor
        ~ErrorContext();

        /// Assignment
        ErrorContext& operator=(const ErrorContext &cxt);

        /**
         * Get an input value from a specified context
         *
         * \param index Input operand index
         * \param lane  Value or SIMD index
         *
         * \return Value of input operand
         */
        inline DataValue getInputValue(uint8_t index=SRC_OPERAND, uint8_t lane=0)
        {
            if (instruction_data.inputs[index].value.size() > lane) {
                return instruction_data.inputs[index].value[lane];
            }
            return DataValue();
        }
        
        /**
         * Get an input value from the previous context
         *
         * \param index Input operand index
         * \param lane  Value or SIMD index
         *
         * \return Value of input operand
         */
        inline DataValue getPrevInputValue(uint8_t index=SRC_OPERAND, uint8_t lane=0)
        {
            if (prev_instruction_data.inputs[index].value.size() > lane) {
                return prev_instruction_data.inputs[index].value[lane];
            }
            return DataValue();
        }
 
        /**
         * Get an output value from a specified context
         *
         * \param index Output operand index
         * \param lane  Value or SIMD index
         *
         * \return Value of output operand
         */
        inline DataValue getOutputValue(uint8_t index=DEST_OPERAND, uint8_t lane=0)
        {
            if (instruction_data.outputs[index].value.size() > lane) {
                return instruction_data.outputs[index].value[lane];
            }
            return DataValue();
        }

        /**
         * Get an output value from the previous context
         *
         * \param index Output operand index
         * \param lane  Value or SIMD index
         *
         * \return Value of output operand
         */
        inline DataValue getPrevOutputValue(uint8_t index=DEST_OPERAND, uint8_t lane=0)
        {
            if (prev_instruction_data.outputs[index].value.size() > lane) {
                return prev_instruction_data.outputs[index].value[lane];
            }
            return DataValue();
        }

        /**
         * Get an output old_value from a specified context
         *
         * \param index Output operand index
         * \param lane  Value or SIMD index
         *
         * \return OldValue of output operand
         */
        inline DataValue getOutputOldValue(uint8_t index=DEST_OPERAND, uint8_t lane=0)
        {
            if (instruction_data.outputs[index].old_value.size() > lane) {
                return instruction_data.outputs[index].old_value[lane];
            }
            return DataValue();
        }

        /**
         * Get an output old_value from the previous context
         *
         * \param index Output operand index
         * \param lane  Value or SIMD index
         *
         * \return OldValue of output operand
         */
        inline DataValue getPrevOutputOldValue(uint8_t index=DEST_OPERAND, uint8_t lane=0)
        {
            if (prev_instruction_data.outputs[index].old_value.size() > lane) {
                return prev_instruction_data.outputs[index].old_value[lane];
            }
            return DataValue();
        }

        /**
         * Get an input values from a specified context (all SIMD lanes)
         *
         * \param index Input operand index
         *
         * \return Values of input operand
         */
        inline std::vector<DataValue> & getInputValues(uint8_t index=SRC_OPERAND)
        {
            return instruction_data.inputs[index].value;
        }

        /**
         * Get an input values from the previous context (all SIMD lanes)
         *
         * \param index Input operand index
         *
         * \return Values of input operand
         */
        inline std::vector<DataValue> & getPrevInputValues(uint8_t index=SRC_OPERAND)
        {
            return prev_instruction_data.inputs[index].value;
        }

        /**
         * Get an output values from a specified context (all SIMD lanes)
         *
         * \param index Output operand index
         *
         * \return Values of output operand
         */
        inline std::vector<DataValue> & getOutputValues(uint8_t index=DEST_OPERAND)
        {
            return instruction_data.outputs[index].value;
        }

        /**
         * Get an output values from the previous context (all SIMD lanes)
         *
         * \param index Output operand index
         *
         * \return Values of output operand
         */
        inline std::vector<DataValue> & getPrevOutputValues(uint8_t index=DEST_OPERAND)
        {
            return prev_instruction_data.outputs[index].value;
        }

        /**
         * Get an output old_values from a specified context (all SIMD lanes)
         *
         * \param index Output operand index
         *
         * \return OldValues of output operand
         */
        inline std::vector<DataValue> getOutputOldValues(uint8_t index=DEST_OPERAND)
        {
            return instruction_data.outputs[index].old_value;
        }

        /**
         * Get an output old_values from the previous context (all SIMD lanes)
         *
         * \param index Output operand index
         *
         * \return OldValues of output operand
         */
        inline std::vector<DataValue> getPrevOutputOldValues(uint8_t index=DEST_OPERAND)
        {
            return prev_instruction_data.outputs[index].old_value;
        }

        /**
         * Set a value of an input operand
         *
         * \param value Value to set
         * \param index Input operand index
         * \param lane  Value or SIMD lane to set
         */
        inline void setInput(DataValue value, uint8_t index=SRC_OPERAND, uint8_t lane=0)
        {
            instruction_data.inputs[index].setValue(value, lane);
        }

        /**
         * Set an Int value of an input operand
         *
         * \param value Value to set
         * \param index Input operand index
         * \param lane  Value or SIMD lane to set
         */
        inline void setInputInt(uint64_t value, uint8_t index=SRC_OPERAND, uint8_t lane=0)
        {
            instruction_data.inputs[index].setValue(value, lane);
        }

        /**
         * Set a Float value of an input operand
         *
         * \param value Value to set
         * \param index Input operand index
         * \param lane  Value or SIMD lane to set
         */
        inline void setInputFloat(fp32_t value, uint8_t index=SRC_OPERAND, uint8_t lane=0)
        {
            instruction_data.inputs[index].setValue(value, lane);
        }

        /**
         * Set a Double value of an input operand
         *
         * \param value Value to set
         * \param index Input operand index
         * \param lane  Value or SIMD lane to set
         */
        inline void setInputDouble(fp64_t value, uint8_t index=SRC_OPERAND, uint8_t lane=0)
        {
            instruction_data.inputs[index].setValue(value, lane);
        }

        /**
         * Set a value of an output operand
         *
         * \param value Value to set
         * \param index Output operand index
         * \param lane  Value or SIMD lane to set
         */
        inline void setOutput(DataValue value, uint8_t index=DEST_OPERAND, uint8_t lane=0)
        {
            instruction_data.outputs[index].setValue(value, lane);
        }

        /**
         * Set an Int value of an output operand
         *
         * \param value Value to set
         * \param index Output operand index
         * \param lane  Value or SIMD lane to set
         */
        inline void setOutputInt(uint64_t value, uint8_t index=DEST_OPERAND, uint8_t lane=0)
        {
            instruction_data.outputs[index].setValue(value, lane);
        }

        /**
         * Set a Float value of an output operand
         *
         * \param value Value to set
         * \param index Output operand index
         * \param lane  Value or SIMD lane to set
         */
        inline void setOutputFloat(fp32_t value, uint8_t index=DEST_OPERAND, uint8_t lane=0)
        {
            instruction_data.outputs[index].setValue(value, lane);
        }

        /**
         * Set a Double value of an output operand
         *
         * \param value Value to set
         * \param index Output operand index
         * \param lane  Value or SIMD lane to set
         */
        inline void setOutputDouble(fp64_t value, uint8_t index=DEST_OPERAND, uint8_t lane=0)
        {
            instruction_data.outputs[index].setValue(value, lane);
        }

        /**
         * Set values of an input operand (all SIMD lanes)
         *
         * \param values Values to set
         * \param index  Input operand index
         */
        inline void setInputs(std::vector<DataValue> & values, uint8_t index=SRC_OPERAND)
        {
            instruction_data.inputs[index].value = values;
        }

        /**
         * Set Int values of an input operand (all SIMD lanes)
         *
         * \param values Values to set
         * \param index  Input operand index
         */
        inline void setInputInts(std::vector<uint64_t> & values, uint8_t index=SRC_OPERAND)
        {
            for (int i = 0, l = values.size(); i < l; ++i) {
                setInput(values[i], i);
            }
        }

        /**
         * Set Float values of an input operand (all SIMD lanes)
         *
         * \param values Values to set
         * \param index  Input operand index
         */
        inline void setInputFloats(std::vector<fp32_t> & values, uint8_t index=SRC_OPERAND)
        {
            for (int i = 0, l = values.size(); i < l; ++i) {
                setInput(values[i], i);
            }
        }

        /**
         * Set Double values of an input operand (all SIMD lanes)
         *
         * \param values Values to set
         * \param index  Input operand index
         */
        inline void setInputDoubles(std::vector<fp64_t> & values, uint8_t index=SRC_OPERAND)
        {
            for (int i = 0, l = values.size(); i < l; ++i) {
                setInput(values[i], i);
            }
        }

        /**
         * Set values of an output operand (all SIMD lanes)
         *
         * \param values Values to set
         * \param index  Output operand index
         */
        inline void setOutputs(std::vector<DataValue> & values, uint8_t index=DEST_OPERAND)
        {
            instruction_data.outputs[index].value = values;
        }

        /**
         * Set Int values of an output operand (all SIMD lanes)
         *
         * \param values Values to set
         * \param index  Output operand index
         */
        inline void setOutputInts(std::vector<uint64_t> & values, uint8_t index=DEST_OPERAND)
        {
            for (int i = 0, l = values.size(); i < l; ++i) {
                setOutput(values[i], i);
            }
        }

        /**
         * Set Float values of an output operand (all SIMD lanes)
         *
         * \param values Values to set
         * \param index  Output operand index
         */
        inline void setOutputFloats(std::vector<fp32_t> & values, uint8_t index=DEST_OPERAND)
        {
            for (int i = 0, l = values.size(); i < l; ++i) {
                setOutput(values[i], i);
            }
        }

        /**
         * Set Double values of an output operand (all SIMD lanes)
         *
         * \param values Values to set
         * \param index  Output operand index
         */
        inline void setOutputDoubles(std::vector<fp64_t> & values, uint8_t index=DEST_OPERAND)
        {
            for (int i = 0, l = values.size(); i < l; ++i) {
                setOutput(values[i], i);
            }
        }

        /**
         * Get the current (top of stack) statistics (for current injection)
         *
         * \param cxt Error context
         *
         * \return The current statistics
         */
        static inline InjectionStats & CurrentStats(ErrorContext *cxt)
        {
            return cxt->statistics.back();
        }

        /**
         * Create new statistics for a new injection
         *
         * \return The new statistics
         */
        inline InjectionStats & newStats()
        {
            InjectionStats stats;
            statistics.push_back(stats);

            return CurrentStats(this);
        }

        /**
         * Push the statistics from one context (injection) to another
         *
         * \param dest Destination context
         */
        inline void pushStats(ErrorContext *dest)
        {
            CurrentStats(dest) = CurrentStats(this);
        }
    };
}

#endif
/** @} */
