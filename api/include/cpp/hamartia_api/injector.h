// Copyright 2015, The University of Texas at Austin 
// All rights reserved.
// 
// THIS FILE IS PART OF THE HAMARTIA FAULT/ERROR ANALYSIS SUITE
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met: 
// 
// 1. Redistributions of source code must retain the above copyright
// notice, this list of conditions and the following disclaimer. 
// 
// 2. Redistributions in binary form must reproduce the above copyright
// notice, this list of conditions and the following disclaimer in the
// documentation and/or other materials provided with the distribution. 
// 
// 3. Neither the name of the copyright holder nor the names of its
// contributors may be used to endorse or promote products derived from
// this software without specific prior written permission. 
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
// FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
// COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
// BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
// ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

/**
 *  \file
 *  \brief Generic error injector
 *  \author Nicholas Kelly, University of Texas
 *  \author Chun-Kai Chang, University of Texas
 *
 *  \addtogroup injector
 *  @{
 */

#ifndef _HAMARTIA_API_INJECTOR_H
#define _HAMARTIA_API_INJECTOR_H

#include <hamartia_api/interface.h>
#include <hamartia_api/logging.h>

namespace hamartia_api 
{
    /// Injection Phases
    typedef enum {
        /* Default */
        PHASE_EVAL,         ///< Evaluation phase (collect correct result)
        PHASE_INJECT,       ///< Injection phase
        PHASE_PRE_INJECT,   ///< Pre-injection phase
        PHASE_INJECTED,     ///< Post-injection phase
        /* Timing Error */
        PHASE_GET_PREV,       ///< Phase that gets the previous context
    } InjectorPhase;

    /**
     * Generic error injector (all injectors using the API will extend this)
     */
    class Injector {
        protected:
        /// Main log
        Log log;
        /// Current error context
        ErrorContext current_context;
        /// Injection error context
        ErrorContext injection_context;
        /// Record error context
        ErrorContext record_context;
        /// Status
        InjectorPhase phase;
        /// Error Methods
        uint64_t error_methods;
        /// Errors injected
        uint64_t num_errors_injected;
        /// Errors recorded
        uint64_t num_errors_recorded;
        /// Errors to inject
        uint64_t num_errors_to_inject;
        /// Errors to record
        uint64_t num_errors_to_record;

        public:
        /**
         * Constructor
         */
        Injector();

        /**
         * Constructor with error model parameters
         *
         * \param error_model  Error model to execute
         * \param error_config Error model configuration file
         */
        Injector(std::string error_model, std::string error_config);

        /**
         * Constructor with error model parameters
         *
         * \param error_model     Error model to execute
         * \param error_config    Error model configuration file
         * \param detector_model  Detector model to execute
         * \param detector_config Detector model configuration file
         */
        Injector(std::string error_model, std::string error_config, std::string detector_model, std::string detector_config);

        /// Destructor
        virtual ~Injector();

        /**
         * Add an accepted Error model method (inject/pre-inject)
         *
         * \param method Method to add
         */
        void AddAcceptedErrorMethod(ModelMethod method)
        {
            error_methods |= (1 << method);
        }

        /**
         * Add Pre-inject as an accepted method
         */
        inline void AcceptErrorPreInject()
        {
            AddAcceptedErrorMethod(METHOD_PRE_INJECT);
        }

        /**
         * Add Inject as an accepted method
         */
        inline void AcceptErrorInject()
        {
            AddAcceptedErrorMethod(METHOD_INJECT);
        }

        /**
         * Test whether method method is accepted for this error injection
         *
         * \param method Error model method to test
         *
         * \return Whether method is valid
         */
        inline bool AcceptedErrorMethod(ModelMethod method)
        {
            return (error_methods & (1 << method));
        }

        /**
         * Pre-Inject (before operation) an error to the current context
         *
         * \param auto_mode Automatically run State load and unload functions
         *
         * \return Successfully injected
         */
        virtual bool PreInjectError(bool auto_mode = false);

        /**
         * Inject (after operation) an error to the current context
         *
         * \attention This function also checks whether the injected error is masked or detected
         *
         * \param auto_mode Automatically run State load and unload functions
         *
         * \return Successfully injected
         */
        virtual bool InjectError(bool auto_mode = false);

        /**
         * Save the state (injector specific) to current context, before output is calculated
         */
        virtual void PreStateToContext();

        /**
         * Save the state (injector specific) to current context, after output is calculated
         */
        virtual void PostStateToContext();

        /**
         * Save the correct state (injector specific) to current context, before output is calculated
         */
        virtual void PreCurrentStateToContext() {}

        /**
         * Save the correct state (injector specific) to current context, after output is calculated
         */
        virtual void PostCurrentStateToContext();

        /**
         * Save the injected state (injector specific) to injection context, after output is calculated
         */
        virtual void InjectionStateToContext() {}

        /**
         * Rollback the operation (to state before)
         */
        virtual void RollbackState() {}

        /**
         * Save the current context's old values (before pre-injection) to the state (injector specific), after pre-injection
         */
        virtual void OldInputContextToState() {}

        /**
         * Save the current context's old values (before output) to the state (injector specific), after injection
         */
        virtual void OldOutputContextToState() {}

        /**
         * Save the injection context inputs to the state (injector specific), after pre-injection
         */
        virtual void PreInjectionContextToState() {}

        /**
         * Save the injection context outputs to the state (injector specific), after injection
         */
        virtual void InjectionContextToState() {}

        /**
         * Save the injection context to the current context
         */
        void CommitContext();

        /**
         * Set number of errors to record
         * 
         * \param record Number of errors to record
         */
        void SetNumToRecord(uint64_t record)
        {
            num_errors_to_record = record;
        }

        /**
         * Set number of errors to inject
         */
        void SetNumToInject(uint64_t inject)
        {
            num_errors_to_inject = inject;
        }

        /**
         * Get number of errors injected
         * 
         * \return Number of errors injected
         */
        inline uint64_t NumErrorsInjected()
        {
            return num_errors_injected;
        }

        /**
         * Get status
         * 
         * \return Injector status
         */
        inline InjectorPhase Phase()
        {
            return phase;
        }

        /**
         * Whether all injections have completed
         * 
         * \return Injector status
         */
        inline bool Done()
        {
            return num_errors_injected >= num_errors_to_inject;
        }

        /**
         * Export the log file
         *
         * \param log_file Output stread of log file
         */
        void SaveLog(std::ofstream &log_file);
    };
}

#endif
/** @} */
