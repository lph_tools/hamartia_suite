// Copyright 2015, The University of Texas at Austin 
// All rights reserved.
// 
// THIS FILE IS PART OF THE HAMARTIA FAULT/ERROR ANALYSIS SUITE
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met: 
// 
// 1. Redistributions of source code must retain the above copyright
// notice, this list of conditions and the following disclaimer. 
// 
// 2. Redistributions in binary form must reproduce the above copyright
// notice, this list of conditions and the following disclaimer in the
// documentation and/or other materials provided with the distribution. 
// 
// 3. Neither the name of the copyright holder nor the names of its
// contributors may be used to endorse or promote products derived from
// this software without specific prior written permission. 
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
// FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
// COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
// BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
// ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

/**
 *  \file
 *  \brief Debug printing functions
 *  \author Nicholas Kelly, University of Texas
 *
 *  \addtogroup debug
 *  @{
 */

#ifndef _HAMARTIA_API_DEBUG_H
#define _HAMARTIA_API_DEBUG_H

#include <iostream>

#ifndef LIB_LANG
#ifdef SWIGPYTHON
#define LIB_LANG "PY"
#else
#define LIB_LANG "CPP"
#endif
#endif

/// Print with debug tag
#define DEBUG_TAG(tag, msg) std::cerr << "[" << LIB_LANG << "][" << tag << "] " << msg << std::endl;

/// Print error and exit
#define FATAL_ERROR(msg) \
    DEBUG_TAG("FATAL", msg) \
    exit(EXIT_FAILURE);

#ifdef DEBUG

/// General debug print
#define DEBUG_PRINT(msg) std::cerr << msg << std::endl;
/// Information print
#define INFO_PRINT(msg) DEBUG_TAG("INFO", msg)
/// Warning print
#define WARN_PRINT(msg) DEBUG_TAG("WARN", msg)
/// Error print
#define ERROR_PRINT(msg) DEBUG_TAG("ERR", msg)
/// Emergency print
#define EMERG_PRINT(msg) DEBUG_TAG("EMERG", msg)

#else
#define DEBUG_PRINT(msg)

#ifdef DEBUG_INFO

/// Information print
#define INFO_PRINT(msg) DEBUG_TAG("INFO", msg)
/// Warning print
#define WARN_PRINT(msg) DEBUG_TAG("WARN", msg)
/// Error print
#define ERROR_PRINT(msg) DEBUG_TAG("ERR", msg)
/// Emergency print
#define EMERG_PRINT(msg) DEBUG_TAG("EMERG", msg)

#else
#define INFO_PRINT(msg)

#ifdef DEBUG_WARN

/// Warning print
#define WARN_PRINT(msg) DEBUG_TAG("WARN", msg)
/// Error print
#define ERROR_PRINT(msg) DEBUG_TAG("ERR", msg)
/// Emergency print
#define EMERG_PRINT(msg) DEBUG_TAG("EMERG", msg)

#else
#define WARN_PRINT(msg)
#ifdef DEBUG_ERROR

/// Error print
#define ERROR_PRINT(msg) DEBUG_TAG("ERR", msg)
/// Emergency print
#define EMERG_PRINT(msg) DEBUG_TAG("EMERG", msg)

#else
#define ERROR_PRINT(msg)
#ifdef DEBUG_EMERG

/// Emergency print
#define EMERG_PRINT(msg) DEBUG_TAG("EMERG", msg)

#else
#define EMERG_PRINT(msg)

#endif
#endif
#endif
#endif
#endif

#endif
/// @}
