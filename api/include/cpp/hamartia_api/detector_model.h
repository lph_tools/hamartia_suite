// Copyright 2015, The University of Texas at Austin 
// All rights reserved.
// 
// THIS FILE IS PART OF THE HAMARTIA FAULT/ERROR ANALYSIS SUITE
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met: 
// 
// 1. Redistributions of source code must retain the above copyright
// notice, this list of conditions and the following disclaimer. 
// 
// 2. Redistributions in binary form must reproduce the above copyright
// notice, this list of conditions and the following disclaimer in the
// documentation and/or other materials provided with the distribution. 
// 
// 3. Neither the name of the copyright holder nor the names of its
// contributors may be used to endorse or promote products derived from
// this software without specific prior written permission. 
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
// FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
// COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
// BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
// ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

/**
 *  \file
 *  \brief Detector model definition
 *  \author Nicholas Kelly, University of Texas
 *
 *  \warning Detectors have not been completely tested!
 *
 *  \addtogroup detector_model
 *  @{
 */

#ifndef _HAMARTIA_API_DETECTOR_MODEL_H
#define _HAMARTIA_API_DETECTOR_MODEL_H

// Include first for no warnings
#include <Python.h>

#include <hamartia_api/interface.h>
#include <hamartia_api/logging.h>

// Libs
#include <string>
#include <map>
#include <vector>
#include <yaml-cpp/yaml.h>

// Registration macros
/// Register error model library helper
#define REGISTER_DETECTOR_LIBRARY(elclass) \
    extern "C" hamartia_api::DetectorModelLibrary * register_detector_library() \
    { \
        return new elclass; \
    } \
    extern "C" void unregister_detector_library(hamartia_api::DetectorModelLibrary * elib) \
    { \
        delete dynamic_cast<elclass *>(elib); \
    }

/// Register a detector model
#define REGISTER_DETECTOR_MODEL(dclass, dname) \
    dclass * dclass ## _ ## dname ## _i = new dclass(# dname); \
    AddModel(dclass ## _ ## dname ## _i);

/// Register a detector model with arguments
#define REGISTER_DETECTOR_MODEL_ARGS(dclass, dname, ...) \
    dclass * dclass ## _ ## dname ## _i = new dclass(# dname, __VA_ARGS__); \
    AddModel(dclass ## _ ## dname ## _i);


namespace hamartia_api
{
    // Forward delcarations
    class DetectorModelLibrary;

    /**
     * Abstract class for creating Detector models
     */
    class DetectorModel
    {
        protected:
        /// Detector model name
        std::string name;

        public:
        /**
         * Constructor
         *
         * \param _name Detector model name
         */
        DetectorModel(std::string _name);

        /**
         * Deconstructor
         */
        virtual ~DetectorModel() {}

        /**
         * Check both for unmasked output and not detected
         *
         * \param cxt     The context without the injected error (correct)
         * \param inj_cxt The context with the injected error (incorrect)
         * \param log     Log for adding extra logging information
         *
         * \return Error clean (undetected and unmasked)
         */
        static bool VerifyError(ErrorContext *cxt, ErrorContext *inj_cxt, Log *log = NULL);

        /**
         * Check if operands are equal (masked)
         *
         * \param op1 Correct (non-injected) operand
         * \param op2 Incorrect (injected) operand
         *
         * \return Operands equal (masked)
         */
        static bool CheckEquality(Operand &op1, Operand &op2);

        /**
         * Set the response (for CheckError, RESP_SUCCESS = unmasked, RESP_INVALID_ERROR = masked)
         *
         * \param cxt     Error context
         * \param resp    Response
         * \param message Response message
         */
        static void SetResponse(ErrorContext *cxt, Resp resp, std::string message = std::string());

        /**
         * Get the detector model name
         *
         * \return Detector model name
         */
        std::string GetName();

        /* =====================================================================
         * API
         * ===================================================================== */

        /**
         * Return which methods (e.g. CheckError) are defined (valid) for the model
         *
         * \return Bit-vector of valid methods
         */
        virtual uint64_t Methods();

        /**
         * Setup Detector model (called once on load)
         *
         * \param cxt    Current context
         * \param config YAML config file for configuring the model
         * \param log    Log for adding extra logging information
         * 
         * \return Successful
         */
        virtual bool Setup(ErrorContext *cxt, YAML::Node *config, Log *log = NULL);
        // Python method
        virtual bool Setup(ErrorContext *cxt, PyObject *config, Log *log = NULL);

        /**
         * Cleanup Detector model (called once on unload)
         *
         * \param cxt    Current context
         * \param config YAML config file for configuring the model
         * \param log    Log for adding extra logging information
         * 
         * \return Successful
         */
        virtual bool Cleanup(ErrorContext *cxt, YAML::Node *config, Log *log = NULL);
        // Python method (needed due to different YAML object, not converted with SWIG)
        virtual bool Cleanup(ErrorContext *cxt, PyObject *config, Log *log = NULL);

        /**
         * Check the error (determine if masked/detected)
         *
         * \param cxt    The context with the injected error
         * \param config YAML config file for configuring the detector
         * \param log    Log for adding extra logging information
         *
         * \return Successful (no runtime errors; if detected error, RESP_INVALID ERROR should be set)
         */
        virtual bool CheckError(ErrorContext *cxt, YAML::Node *config, Log *log = NULL);
        // Python method (needed due to different YAML object, not converted with SWIG)
        virtual bool CheckError(ErrorContext *cxt, PyObject *config, Log *log = NULL);
    };

    /**
     * Library of Detector models, used for registering models with the injection system
     */
    class DetectorModelLibrary
    {
        protected:
        /// Detector models in library
        std::vector<DetectorModel *> models;

        public:
        /**
         * Constructor, where models are added
         */
        DetectorModelLibrary();
        virtual ~DetectorModelLibrary();

        /**
         * Get the name of the library
         *
         * \return Library name
         */
        virtual std::string GetName() = 0;

        /**
         * Add model to the library
         *
         * \param model Detector model to add to the libary
         */
        void AddModel(DetectorModel *model);

        /**
         * Get models currently in library
         *
         * \return All models currently added to library
         */
        std::vector<DetectorModel *> GetModels();
    };
}

#endif
/** @} */
