// Copyright 2015, The University of Texas at Austin 
// All rights reserved.
// 
// THIS FILE IS PART OF THE HAMARTIA FAULT/ERROR ANALYSIS SUITE
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met: 
// 
// 1. Redistributions of source code must retain the above copyright
// notice, this list of conditions and the following disclaimer. 
// 
// 2. Redistributions in binary form must reproduce the above copyright
// notice, this list of conditions and the following disclaimer in the
// documentation and/or other materials provided with the distribution. 
// 
// 3. Neither the name of the copyright holder nor the names of its
// contributors may be used to endorse or promote products derived from
// this software without specific prior written permission. 
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
// FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
// COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
// BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
// ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

/**
 *  \file
 *  \brief File helpers
 *  \author Michael Sullivan, University of Texas
 *  \author Nicholas Kelly, University of Texas
 *
 *  \addtogroup utils
 *  @{
 */

#ifndef _HAMARTIA_API_FILE_UTILS_H
#define _HAMARTIA_API_FILE_UTILS_H

#include <string>
#include <vector>

namespace hamartia_api
{
    namespace utils
    {
        /**
         * Basename (name) of file
         *
         * \param path Full file path
         *
         * \return File basename
         */
        std::string FileBasename(std::string path);

        /**
         * File directory
         *
         * \param path Full file path
         *
         * \return File directory path
         */
        std::string FileDirname(std::string path);

        /**
         * File extension
         *
         * \param path Full file path
         *
         * \return File extension
         */
        std::string FileExt(std::string path);

        /**
         * File filename (without extension)
         * 
         * \param path Full file path
         *
         * \return Filename
         */
        std::string FileFilename(std::string path);

        /**
         * Find files matching glob pattern
         *
         * \param glob_path Path including globs
         *
         * \return Vector of matching files
         */
        std::vector<std::string> FileGlob(std::string glob_path);
    }
}

#endif
/** @} */
