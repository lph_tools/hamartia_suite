// Copyright 2015, The University of Texas at Austin 
// All rights reserved.
// 
// THIS FILE IS PART OF THE HAMARTIA FAULT/ERROR ANALYSIS SUITE
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met: 
// 
// 1. Redistributions of source code must retain the above copyright
// notice, this list of conditions and the following disclaimer. 
// 
// 2. Redistributions in binary form must reproduce the above copyright
// notice, this list of conditions and the following disclaimer in the
// documentation and/or other materials provided with the distribution. 
// 
// 3. Neither the name of the copyright holder nor the names of its
// contributors may be used to endorse or promote products derived from
// this software without specific prior written permission. 
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
// FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
// COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
// BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
// ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

/**
 *  \file
 *  \brief Error model helpers (flipping bits, random, etc)
 *  \author Michael Sullivan, University of Texas
 *  \author Nicholas Kelly, University of Texas
 *
 *  \addtogroup utils
 *  @{
 */

#ifndef _HAMARTIA_API_ERROR_UTILS_H
#define _HAMARTIA_API_ERROR_UTILS_H

#include <cstdlib>
#include <cstdint>
#include <ctime>
#include <random>

namespace hamartia_api
{
    namespace utils
    {
        /* =====================================================================
         * RANDOM HELPERS
         * ===================================================================== */

        /**
         * Random generator 
         *
         * \return Random number
         */
        inline uint64_t mt_random()
        {
            /*
            uint64_t a = uint64_t(rand());
            uint64_t b = uint64_t(rand());
            uint64_t c = uint64_t(rand());
            return (a << 34) + (b << 4) + c;
            */
            std::random_device rd;
            std::mt19937_64 mt_rand(rd());
            return mt_rand();
        }

        /**
         * Small random number in between two bounds
         *
         * \param min Minimum value
         * \param max Maximum value
         *
         * \return Random number (within bounds)
         */
        inline uint32_t mt_randr(uint32_t min, uint32_t max)
        {
            std::random_device rd;
            std::mt19937_64 mt_rand(rd());
            //double scaled = (double)rand()/(RAND_MAX+1.0);
            double scaled = (double)mt_rand()/(mt_rand.max()+1.0);
            return (uint32_t)((max - min + 1)*scaled + min);
        }

        /* =====================================================================
         * BURSTBITS (Helper, not used directly)
         * ===================================================================== */

        // Flip a burst of bits in a sub-word

        /**
         * Create burst bits (similar to a mask)
         *
         * \param bmask     Burst mask
         * \param min_shift Minimum shift amount
         * \param max_shift Maximum shift amount
         *
         * \return Randomly shifted mask
         */
        template <class intType>
        intType formBurst(intType bmask, intType min_shift, intType max_shift)
        {
            intType ret = bmask << (intType)mt_randr((uint32_t)min_shift, (uint32_t)max_shift);
            return ret;
        }

        /**
         * Burst bit-flips depending on mask
         *
         * \param tgt       Target value
         * \param bmask     Burst mask
         * \param min_shift Minimum shift amount
         * \param max_shift Maximum shift amount
         *
         * \return Error value
         */
        template <class intType>
        static intType BB_inject(intType tgt, intType bmask, intType min_shift, intType max_shift)
        {
            return tgt ^ formBurst <intType> (bmask, min_shift, max_shift);
        }

        /* =====================================================================
         * MASKEDRANDOM (Helper, not used directly)
         * ===================================================================== */

        // Insert a random value, subject to a mask
        // NOTE: assumes unchecked casts

        /**
         * Masked random injection
         *
         * \param tgt   Target value
         * \param bmask Mask
         *
         * \return Error value
         */
        template <class intType>
        intType MR_inject(intType tgt, intType bmask)
        {
            return (tgt & ~bmask) | ((intType)mt_random() & bmask);
        }

        /* =====================================================================
         * MASKEDRANDOMBITS (Helper, not used directly)
         * ===================================================================== */

        // Flip N random bits, subject to a mask

        /**
         * Number of bits set (Brian Kernighan's method, requires one iteration per bit set)
         *
         * \param v Value
         * \return Total bits set
         */
        template <class intType>
        intType bitsSet (intType v)
        {
            intType c;      // c accumulates the total bits set in v
            for (c = 0; v; c++) {
                v &= v - 1; // clear the least significant bit set
            }
            return c;
        }

        // NOTE: can hang on invalid inputs! (No input checking.)
        /**
         * Generate a random bitmask
         *
         * \param num_bits Number of bit-flips
         * \param bmask    Mask
         *
         * \return Bit-flip mask
         */
        template <class intType>
        intType genMaskedRandBits (intType num_bits, intType bmask)
        {
            intType tgt = 0;
            /* set one bit, using BB_Inject, until num_bits have been set */
            while (bitsSet(tgt) < num_bits) {
                tgt ^= formBurst <intType> (0x1, 0, (intType)((sizeof(intType)<<3)-1)) & bmask;
            }
            return tgt;
        }

        /**
         * Masked random bit-flips
         *
         * \param tgt      Target value
         * \param bmask    Mask
         * \param num_bits Number of bits to flip
         *
         * \return Error value
         */
        template <class intType>
        intType MRB_inject (intType tgt, intType bmask, intType num_bits)
        {
            return tgt ^ genMaskedRandBits <intType> (num_bits, bmask);
        }

        /**
         * A Random bit-flip in a window
         *
         * \param tgt       Target value
         * \param wbits     Window width in bits
         * \param min_shift Minimum shift amount
         * \param max_shift Maximum shift amount
         *
         * \return Error value
         */
        template <class intType>
        static intType WR_inject(intType tgt, intType wbits, intType min_shift, intType max_shift)
        {
            intType cmask = formBurst <intType> (0x1, 0, wbits-1);
            intType mask = ((0x1 << wbits) - 1) & cmask; 
            return tgt ^ formBurst <intType> (mask, min_shift, max_shift);
        }

    }
}

#endif
/** @} */
