// Copyright 2015, The University of Texas at Austin 
// All rights reserved.
// 
// THIS FILE IS PART OF THE HAMARTIA FAULT/ERROR ANALYSIS SUITE
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met: 
// 
// 1. Redistributions of source code must retain the above copyright
// notice, this list of conditions and the following disclaimer. 
// 
// 2. Redistributions in binary form must reproduce the above copyright
// notice, this list of conditions and the following disclaimer in the
// documentation and/or other materials provided with the distribution. 
// 
// 3. Neither the name of the copyright holder nor the names of its
// contributors may be used to endorse or promote products derived from
// this software without specific prior written permission. 
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
// FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
// COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
// BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
// ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

/**
 *  \file
 *  \brief Integer residue-checking detector model with one modulus
 *
 *  \version 1.0.0
 *  \author Chun-Kai Chang, University of Texas
 *
 *  \addtogroup detector_models
 *  @{
 */

#ifndef _RESIDUE_DETECT_ONE_MOD_H
#define _RESIDUE_DETECT_ONE_MOD_H
#include <hamartia_api/types.h>
#include <hamartia_api/detector_model.h>
#include <hamartia_api/debug.h>

class ResidueCheck1 : public hamartia_api::DetectorModel
{

    private:
    // The modulus
    uint64_t mod;
    
    public:
    /**
     * Create ResidueCheck1 instance
     *
     * \param _name Detecotr model name
     * \param _mod The modulus to check against (usually a prime number)
     */
    ResidueCheck1(std::string _name, uint64_t _mod) : DetectorModel(_name)
    {
        mod = _mod;
    }

    //FIXME: should move this to utils directory for code resue
    template <class intType>
    uint64_t _Modulo(intType val, uint64_t mod)
    {
        if (val >= 0) {

            return val % mod;

        } else {

            return (mod - ((-val) % mod)) % mod;

        }
    }

    /**
     * Residue checking for integer addition, subtraction, and multiplication 
     *
     * \param op_width Data width in bits      
     * \param op Operation type      
     * \param in1 Fisrt input operand      
     * \param in2 Second input operand      
     * \param modified_out Modified output value      
     *
     * \return Detected (true) or not
     */
    bool _Detect(uint32_t op_width, hamartia_api::Operation op, \
                 hamartia_api::DataValue* in1, hamartia_api::DataValue* in2, hamartia_api::DataValue* modified_out)
    {
        uint64_t golden_out_mod = 0;
        uint64_t modified_out_mod = 0;

        if (op_width == 8) {

            int8_t signed_in1 = (int8_t)in1->uint();
            int8_t signed_in2 = (int8_t)in2->uint();
            int8_t signed_out = (int8_t)modified_out->uint();

            if (op == hamartia_api::OP_ADD) {

                golden_out_mod = _Modulo<uint64_t>( _Modulo<int8_t>(signed_in1, mod) + _Modulo<int8_t>(signed_in2, mod), mod);

            } else if (op == hamartia_api::OP_SUB) {

                golden_out_mod = _Modulo<uint64_t>( _Modulo<int8_t>(signed_in1, mod) + _Modulo<int8_t>(-signed_in2, mod), mod);

            } else if (op == hamartia_api::OP_MUL) { 

                golden_out_mod = _Modulo<uint64_t>( _Modulo<int8_t>(signed_in1, mod) * _Modulo<int8_t>(signed_in2, mod), mod);
            }

            modified_out_mod = _Modulo<int8_t>(signed_out, mod);

            INFO_PRINT("[Detector]Signed Inputs: " << signed_in1 << " " << signed_in2)
            INFO_PRINT("[Detector]Signed Output: " << signed_out)

        } else if (op_width == 16) {

            int16_t signed_in1 = (int16_t)in1->uint();
            int16_t signed_in2 = (int16_t)in2->uint();
            int16_t signed_out = (int16_t)modified_out->uint();

            if (op == hamartia_api::OP_ADD) {

                golden_out_mod = _Modulo<uint64_t>( _Modulo<int16_t>(signed_in1, mod) + _Modulo<int16_t>(signed_in2, mod), mod);

            } else if (op == hamartia_api::OP_SUB) {

                golden_out_mod = _Modulo<uint64_t>( _Modulo<int16_t>(signed_in1, mod) + _Modulo<int16_t>(-signed_in2, mod), mod);

            } else if (op == hamartia_api::OP_MUL) { 

                golden_out_mod = _Modulo<uint64_t>( _Modulo<int16_t>(signed_in1, mod) * _Modulo<int16_t>(signed_in2, mod), mod);
            }

            modified_out_mod = _Modulo<int16_t>(signed_out, mod);

            INFO_PRINT("[Detector]Signed Inputs: " << signed_in1 << " " << signed_in2)
            INFO_PRINT("[Detector]Signed Output: " << signed_out)

        } else if (op_width == 32) {

            int32_t signed_in1 = (int32_t)in1->uint();
            int32_t signed_in2 = (int32_t)in2->uint();
            int32_t signed_out = (int32_t)modified_out->uint();

            if (op == hamartia_api::OP_ADD) {

                golden_out_mod = _Modulo<uint64_t>( _Modulo<int32_t>(signed_in1, mod) + _Modulo<int32_t>(signed_in2, mod), mod);

            } else if (op == hamartia_api::OP_SUB) {

                golden_out_mod = _Modulo<uint64_t>( _Modulo<int32_t>(signed_in1, mod) + _Modulo<int32_t>(-signed_in2, mod), mod);

            } else if (op == hamartia_api::OP_MUL) { 

                golden_out_mod = _Modulo<uint64_t>( _Modulo<int32_t>(signed_in1, mod) * _Modulo<int32_t>(signed_in2, mod), mod);
            }

            modified_out_mod = _Modulo<int32_t>(signed_out, mod);

            INFO_PRINT("[Detector]Signed Inputs: " << signed_in1 << " " << signed_in2)
            INFO_PRINT("[Detector]Signed Output: " << signed_out)

        } else {

            int64_t signed_in1 = (int64_t)in1->uint();
            int64_t signed_in2 = (int64_t)in2->uint();
            int64_t signed_out = (int64_t)modified_out->uint();

            if (op == hamartia_api::OP_ADD) {

                golden_out_mod = _Modulo<uint64_t>( _Modulo<int64_t>(signed_in1, mod) + _Modulo<int64_t>(signed_in2, mod), mod);

            } else if (op == hamartia_api::OP_SUB) {

                golden_out_mod = _Modulo<uint64_t>( _Modulo<int64_t>(signed_in1, mod) + _Modulo<int64_t>(-signed_in2, mod), mod);

            } else if (op == hamartia_api::OP_MUL) {

                golden_out_mod = _Modulo<uint64_t>( _Modulo<int64_t>(signed_in1, mod) * _Modulo<int64_t>(signed_in2, mod), mod);
            }

            modified_out_mod = _Modulo<int64_t>(signed_out, mod);

            INFO_PRINT("[Detector]Signed Inputs: " << signed_in1 << " " << signed_in2)
            INFO_PRINT("[Detector]Signed Output: " << signed_out)

        }

        INFO_PRINT("[Detector]Residues: " << golden_out_mod << " (correct), " << modified_out_mod << " (wrong)")
        return golden_out_mod != modified_out_mod;
    }

    /**
     * Check error by computing residue
     *
     * \param cxt    Injection context
     * \param config YAML config file for configuring the model
     * \param log    Log for adding extra logging information
     *
     * \return Successful (no runtime errors)
     */
    bool CheckError(hamartia_api::ErrorContext *cxt, YAML::Node *config, hamartia_api::Log *log)
    {
        // check if this detector should be applied 
        // if not supported, return immediately (set response to success, meaning unmasked)
        hamartia_api::Operation op = cxt->instruction_data.instruction.operation;
        hamartia_api::DataType dp = cxt->instruction_data.instruction.data;
        uint32_t op_width   = cxt->instruction_data.width;
        uint32_t num_packed = cxt->instruction_data.simd_width;
        uint32_t lane_width = op_width / num_packed;

        if (op != hamartia_api::OP_ADD && op != hamartia_api::OP_SUB && op != hamartia_api::OP_MUL) {
            cxt->error_info.setResponse(hamartia_api::RESP_SUCCESS, "Undetected due to unsupported operation");
            return true;
        }
        if (dp != hamartia_api::DT_UINT && dp != hamartia_api::DT_INT) {
            cxt->error_info.setResponse(hamartia_api::RESP_SUCCESS, "Undetected due to unsupported data type");
            return true;
        }
        if (lane_width <= mod) {
            cxt->error_info.setResponse(hamartia_api::RESP_SUCCESS, "Undetected due to inconsistent data width");
            return true;
        }

        //FIXME: assume detect first SIMD lane only
        // also assume inputs are correct (maybe need to disable this for pre-injection?)
        hamartia_api::DataValue in1 = cxt->getInputValue(0); 
        hamartia_api::DataValue in2 = cxt->getInputValue(1); 
        hamartia_api::DataValue modified_out = cxt->getOutputValue();

        bool detected = _Detect(lane_width, op, &in1, &in2, &modified_out);

        if (detected) {
            cxt->error_info.setResponse(hamartia_api::RESP_INVALID_ERROR);
        }
        return true;
    }
};

#endif
/** @} */
