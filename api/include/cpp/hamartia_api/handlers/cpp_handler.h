// Copyright 2015, The University of Texas at Austin 
// All rights reserved.
// 
// THIS FILE IS PART OF THE HAMARTIA FAULT/ERROR ANALYSIS SUITE
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met: 
// 
// 1. Redistributions of source code must retain the above copyright
// notice, this list of conditions and the following disclaimer. 
// 
// 2. Redistributions in binary form must reproduce the above copyright
// notice, this list of conditions and the following disclaimer in the
// documentation and/or other materials provided with the distribution. 
// 
// 3. Neither the name of the copyright holder nor the names of its
// contributors may be used to endorse or promote products derived from
// this software without specific prior written permission. 
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
// FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
// COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
// BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
// ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

/**
 * \file
 * \brief C++ model handler, native API
 * \author Nicholas Kelly, University of Texas
 *
 * \warning Detector calling not tested!
 *
 * \addtogroup handlers
 * @{
 */

#ifndef _HAMARTIA_API_CPP_HANDLER_H
#define _HAMARTIA_API_CPP_HANDLER_H

#include <hamartia_api/error_model.h>
#include <hamartia_api/interface.h>
#include <hamartia_api/handler.h>
#include <hamartia_api/detector_model.h>
#include <string>
#include <vector>

namespace hamartia_api
{
    /**
     * C++ Error Model handler
     */
    class CppErrorModelHandler : public ModelHandler
    {
        private:
        /// Underlying error model (C++)
        ErrorModel *error_model;

        public:
        /**
         * Load an Error model from a shared library
         *
         * \param path Path to load library from
         *
         * \return Vector of model handlers (associated with models)
         */
        static std::vector<ModelHandler *> LoadFromSharedLibrary(std::string path);

        /**
         * Load an Error model from a model library
         *
         * \param lib Error Model Library object
         *
         * \return Vector of model handlers (associated with models)
         */
        static std::vector<ModelHandler *> LoadFromLibrary(ErrorModelLibrary * lib);

        /// Constructor
        CppErrorModelHandler();

        /**
         * Constructor
         *
         * \param _error_model Error model object
         */
        CppErrorModelHandler(ErrorModel * _error_model);

        /// Destructor
        ~CppErrorModelHandler();

        /**
         * Model name
         *
         * \return Name of model
         */
        std::string modelName();

        /**
         * Invoke a method for the C++ model
         *
         * \param method Method to invoke
         * \param cxt    Error context
         * \param log    Log
         *
         * \return Successful
         */
        bool invoke(ModelMethod method, ErrorContext *cxt, Log *log = NULL);
    };

    /**
     * C++ Detector Model handler
     */
    class CppDetectorModelHandler : public ModelHandler
    {
        private:
        /// Underlying detector model (C++)
        DetectorModel *detector_model;

        public:
        /**
         * Load a Detector model from a shared library
         *
         * \param path Path to load library from
         *
         * \return Vector of model handlers (associated with models)
         */
        static std::vector<ModelHandler *> LoadFromSharedLibrary(std::string path);

        /**
         * Load a Detector model from a model library
         *
         * \param lib Detector Model Library object
         *
         * \return Vector of model handlers (associated with models)
         */
        static std::vector<ModelHandler *> LoadFromLibrary(DetectorModelLibrary * lib);

        /// Constructor
        CppDetectorModelHandler();

        /**
         * Constructor
         *
         * \param _detector_model Detector model object
         */
        CppDetectorModelHandler(DetectorModel * _detector_model);

        /// Destructor
        ~CppDetectorModelHandler();

        /**
         * Model name
         *
         * \return Name of model
         */
        std::string modelName();

        /**
         * Invoke a method for the C++ model
         *
         * \param method Method to invoke
         * \param cxt    Error context
         * \param log    Log
         *
         * \return Successful
         */
        bool invoke(ModelMethod method, ErrorContext *cxt, Log *log = NULL);
    };
}

#endif
/** @} */
