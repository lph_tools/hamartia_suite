// Copyright 2015, The University of Texas at Austin 
// All rights reserved.
// 
// THIS FILE IS PART OF THE HAMARTIA FAULT/ERROR ANALYSIS SUITE
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met: 
// 
// 1. Redistributions of source code must retain the above copyright
// notice, this list of conditions and the following disclaimer. 
// 
// 2. Redistributions in binary form must reproduce the above copyright
// notice, this list of conditions and the following disclaimer in the
// documentation and/or other materials provided with the distribution. 
// 
// 3. Neither the name of the copyright holder nor the names of its
// contributors may be used to endorse or promote products derived from
// this software without specific prior written permission. 
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
// FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
// COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
// BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
// ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

/**
 * \file
 * \brief Python model handler, allows Python models to be called through SWIG/API
 * \author Nicholas Kelly, University of Texas
 *
 * \warning Detector calling not tested!
 *
 * \addtogroup handlers
 * @{
 */

#ifndef _HAMARTIA_API_PY_HANDLER_H
#define _HAMARTIA_API_PY_HANDLER_H

// Include first for no warnings
#include <Python.h>

#include <hamartia_api/interface.h>
#include <hamartia_api/handler.h>
#include <hamartia_api/error_model.h>
#include <hamartia_api/detector_model.h>

/// Check returned python pointer for error
#define PY_PTR_CHECK(ptr, msg) \
    if (ptr == NULL) { \
        if (PyErr_Occurred()) PyErr_Print(); \
        FATAL_ERROR(msg) \
    } 

namespace hamartia_api
{
    /**
     * Python Error Model handler
     */
    class PyErrorModelHandler : public ModelHandler
    {
        private:
        /// Underlying error model (python)
        PyObject *error_model;

        public:
        /**
         * Load an Error model from a Python module (from "register" variable)
         *
         * \param path Path to load module from
         *
         * \return Vector of model handlers (associated with models)
         */
        static std::vector<ModelHandler *> LoadFromModule(std::string path);

        /**
         * Load an Error model from a model library
         *
         * \param lib Error Model Library object
         *
         * \return Vector of model handlers (associated with models)
         */
        static std::vector<ModelHandler *> LoadFromLibrary(PyObject * lib);

        /// Constructor
        PyErrorModelHandler();

        /**
         * Constructor
         *
         * \param _error_model Error model object
         */
        PyErrorModelHandler(PyObject * _error_model);

        /// Destructor
        ~PyErrorModelHandler();

        /**
         * Model name
         *
         * \return Name of model
         */
        std::string modelName();

        /**
         * Invoke a method for the Python model
         *
         * \param method Method to invoke
         * \param cxt    Error context
         * \param log    Log
         *
         * \return Successful
         */
        bool invoke(ModelMethod method, ErrorContext *cxt, Log *log = NULL);
    };

    /**
     * Python Detector Model handler
     */
    class PyDetectorModelHandler : public ModelHandler
    {
        private:
        /// Underlying detector model (python)
        PyObject *detector_model;

        public:
        /**
         * Load a Detector model from a Python module (from "register" variable)
         *
         * \param path Path to load module from
         *
         * \return Vector of model handlers (associated with models)
         */
        static std::vector<ModelHandler *> LoadFromModule(std::string path);

        /**
         * Load a Detector model from a model library
         *
         * \param lib Error Model Library object
         *
         * \return Vector of model handlers (associated with models)
         */
        static std::vector<ModelHandler *> LoadFromLibrary(PyObject * lib);

        /// Constructor
        PyDetectorModelHandler();

        /**
         * Constructor
         *
         * \param _detector_model Detector model object
         */
        PyDetectorModelHandler(PyObject * _detector_model);

        /// Destructor
        ~PyDetectorModelHandler();

        /**
         * Model name
         *
         * \return Name of model
         */
        std::string modelName();

        /**
         * Invoke a method for the Python model
         *
         * \param method Method to invoke
         * \param cxt    Error context
         * \param log    Log
         *
         * \return Successful
         */
        bool invoke(ModelMethod method, ErrorContext *cxt, Log *log = NULL);
    };
}

#endif
/** @} */
