// Copyright 2015, The University of Texas at Austin 
// All rights reserved.
// 
// THIS FILE IS PART OF THE HAMARTIA FAULT/ERROR ANALYSIS SUITE
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met: 
// 
// 1. Redistributions of source code must retain the above copyright
// notice, this list of conditions and the following disclaimer. 
// 
// 2. Redistributions in binary form must reproduce the above copyright
// notice, this list of conditions and the following disclaimer in the
// documentation and/or other materials provided with the distribution. 
// 
// 3. Neither the name of the copyright holder nor the names of its
// contributors may be used to endorse or promote products derived from
// this software without specific prior written permission. 
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
// FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
// COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
// BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
// ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

/**
 * \file
 * \brief Error Manager, responsible calling error/detector models and/or other injectors
 * \author Nicholas Kelly, University of Texas
 *
 * \warning Dynamic loading and executable not tested!
 *
 * \addtogroup error_manager
 * @{
 */

#ifndef _HAMARTIA_API_ERROR_MANAGER_H
#define _HAMARTIA_API_ERROR_MANAGER_H

#include <hamartia_api/interface.h>
#include <hamartia_api/handler.h>
#include <hamartia_api/logging.h>

#include <string>
#include <map>
#include <vector>

namespace hamartia_api
{
    // Forward declare
    class ErrorModelLibrary;
    class DetectorModelLibrary;

    namespace error_manager {
        /**
         * Initialize the Error Manager
         *
         * \warning args depredicated for now...
         *
         * \param argc          Number of command line arguments
         * \param argv          Command line arguments
         */
        void Init(int argc = 0, char ** argv = NULL);

        /**
         * Print the help/usage
         *
         * \deprecated
         * \param msg Additional message to print with the usage
         */
        void _Help(std::string msg = "");

        /**
         * Dynamically load error model libraries from path (HAMARTIA_ERROR_PATH)
         */
        void LoadAllErrorModelsFromEnv();

        /**
         * Dynamically load detector model libraries from path (HAMARTIA_DETECTOR_PATH)
         */
        void LoadAllDetectorModelsFromEnv();

        /**
         * Register Handlers (i.e. Py, C++)
         *
         * \param handlers Vector of model handlers
         */
        void RegisterHandlers(std::vector<ModelHandler *> handlers);

        /**
         * Call Setup function in appropriate handler
         *
         * \note Somewhat new/experimental
         *
         * \param cxt     Error context
         * \param log     Log
         *
         * \return Successful
         */
        bool Setup(ErrorContext *cxt, Log *log);

        /**
         * Call Cleanup function in appropriate handler
         *
         * \note Somewhat new/experimental
         *
         * \param cxt     Error context
         * \param log     Log
         *
         * \return Successful
         */
        bool Cleanup(ErrorContext *cxt, Log *log);

        /**
         * Pre-Inject (before operation) error using the specified context
         *
         * \param cxt     Original error context
         * \param inj_cxt Injected error context
         * \param log     Log
         *
         * \return Successful
         */
        bool PreInjectError(ErrorContext *cxt, ErrorContext *inj_cxt, Log *log = NULL);

        /**
         * Inject error (after operation) using the specified context
         *
         * \param inj_cxt Injected error context
         * \param log     Log
         *
         * \return Successful
         */
        bool InjectError(ErrorContext *inj_cxt, Log *log = NULL);

        /**
         * Verify the error is unmasked and undetected
         *
         * \param cxt        Original error context
         * \param inj_cxt    Injected error context
         * \param unmasked   Is unmasked (actually results in error)
         * \param undetected Is undetected (not flagged by detector)
         * \param log        Log
         *
         * \return Successful
         */
        bool CheckError(ErrorContext *cxt, ErrorContext *inj_cxt, bool &unmasked, bool &undetected, Log *log = NULL);

        /**
         * Run an external (i.e. Python) or local (C++) error model
         *
         * \param model  Model type (error/detector)
         * \param method Method to run
         * \param cxt    Error context
         * \param log    Log
         *
         * \return Successful (no runtime errors)
         */
        bool RunModel(ModelType model, ModelMethod method, ErrorContext *cxt, Log *log = NULL);

        /**
         * Get current (in use) model name
         *
         * \param model Model type (error/detector)
         * \param cxt   Current context
         *
         * \return Model name
         */
        std::string GetModelName(ModelType model, ErrorContext *cxt);

        /**
         * Get the handler (e.g. Python/C++) for the model
         *
         * \param name Model name
         *
         * \return Model handler
         */
        ModelHandler * GetHandler(std::string name);

        /**
         * Check if a model's method is valid (defined)
         *
         * \param model  Model type (error/detector)
         * \param method Method to run
         * \param cxt    Error context
         *
         * \return Valid
         */
        bool ModelMethodValid(ModelType model, ModelMethod method, ErrorContext *cxt);
    }
}

#endif
/** @} */
