// Copyright 2015, The University of Texas at Austin 
// All rights reserved.
// 
// THIS FILE IS PART OF THE HAMARTIA FAULT/ERROR ANALYSIS SUITE
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met: 
// 
// 1. Redistributions of source code must retain the above copyright
// notice, this list of conditions and the following disclaimer. 
// 
// 2. Redistributions in binary form must reproduce the above copyright
// notice, this list of conditions and the following disclaimer in the
// documentation and/or other materials provided with the distribution. 
// 
// 3. Neither the name of the copyright holder nor the names of its
// contributors may be used to endorse or promote products derived from
// this software without specific prior written permission. 
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
// FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
// COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
// BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
// ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

/*
 * \file
 * \brief Error API types
 * \author Nicholas Kelly, University of Texas
 *
 * \addtogroup types
 * @{
 */

#ifndef _HAMARTIA_API_TYPES_H
#define _HAMARTIA_API_TYPES_H

#include <stdint.h>

// Bit helpers
/// Generate bitmask
#define BITMASK(len) ((((uint64_t) 1) << (len))-1)
/// Get bits from int
#define GET_BITS(gval, idx, len) ((gval >> (idx)) & BITMASK(len))
/// Set bits in int
#define SET_BITS(gval, idx, len, val) gval = (gval & ~(BITMASK(len) << idx)) | ((val & BITMASK(len)) << idx)

namespace hamartia_api
{
    // Typedefs
    /// 32-bit floating point
    typedef float fp32_t;
    /// 64-bit floating point
    typedef double fp64_t;
    /// Address type
    typedef uint64_t addr_t;

    /// Generic value
    typedef union {
        /// Scalar
        uint64_t uint;
        /// Floating point (32-bit)
        fp64_t fp64;
        /// Floating point (64-bit)
        fp32_t fp32;
    } Value;

    /**
     * A container for most data types with additional features
     */
    class DataValue
    {
        public:
        /// Base type
        Value v;

        /**
         * Default constructor
         */
        DataValue() { v.uint = 0; }

        /**
         * UINT constructor
         *
         * \param _uint Unsigned integer
         */
        DataValue(uint64_t _uint) { v.uint = _uint; }

        /**
         * DOUBLE constructor
         *
         * \param _fp64 Double (64-bit)
         */
        DataValue(fp64_t _fp64) { v.fp64 = _fp64; }

        /**
         * FLOAT constructor
         *
         * \param _fp32 Float (32-bit)
         */
        DataValue(fp32_t _fp32) { v.fp32 = _fp32; }

        /**
         * Uint getter
         *
         * \return Uint value
         */
        inline uint64_t uint() const
        {
            return v.uint;
        }

        /**
         * Uint setter
         *
         * \param u Uint value to set
         */
        inline void uint(uint64_t u)
        {
            v.uint = u;
        }

        /**
         * FP32 (float) getter
         *
         * \return FP32 value
         */
        inline fp32_t fp32() const
        {
            return v.fp32;
        }

        /**
         * FP32 (float) setter
         *
         * \param f FP32 value to set
         */
        inline void fp32(fp32_t f)
        {
            v.fp32 = f;
        }

        /**
         * FP64 (double) getter
         *
         * \return FP64 value
         */
        inline fp64_t fp64() const
        {
            return v.fp64;
        }

        /**
         * FP64 (double) setter
         *
         * \param d FP64 value to set
         */
        inline void fp64(fp64_t d)
        {
            v.fp64 = d;
        }

        // OPERATORS
        // TODO: add more operators?

        /**
         * Assignment operator
         *
         * \param dv Assign DataValue
         *
         * \return Final value
         */
        DataValue& operator=(const DataValue &dv)
        {
            v.uint = dv.v.uint;
            return *this;
        }

        /**
         * Equals operator
         *
         * \param dv DataValue to check equality
         *
         * \return Is equal
         */
        bool operator==(const DataValue &dv)
        {
            return (v.uint == dv.v.uint);
        }

        /**
         * Not equal operator
         *
         * \param dv DataValue to check non-equality
         *
         * \return Is not equal
         */
        bool operator!=(const DataValue &dv)
        {
            return (v.uint != dv.v.uint);
        }

        /**
         * Bitselect operator (select a single bit)
         *
         * \param idx Bit to select
         *
         * \return Selected bit
         */
        uint8_t operator[](std::size_t idx) const
        {
            return GET_BITS(v.uint, idx, 1);
        }

        // BITS

        /**
         * Get sequence of bits (as number)
         *
         * \param idx Starting idx
         * \param len Length of bits to fetch
         *
         * \return Bit sequence
         */
        uint64_t get_bits(uint8_t idx, uint8_t len) const
        {
            return GET_BITS(v.uint, idx, len);
        }

        /**
         * Set bit sequence from UINT
         *
         * \param val Value to set
         * \param idx Starting idx
         * \param len Length of bits to overwrite
         */
        void set_bits(uint64_t val, uint8_t idx, uint8_t len)
        {
            SET_BITS(v.uint, idx, len, val);
        }

        /**
         * Set bit sequence from DataValue
         *
         * \param dv DataValue to set
         * \param idx Starting idx
         * \param len Length of bits to overwrite
         */
        void set_bits(const DataValue &dv, uint8_t idx, uint8_t len)
        {
            SET_BITS(v.uint, idx, len, dv.v.uint);
        }

        // BITMASKS (SEGMENTS)

        // FP32 segments

        /// Get Float Fraction
        uint32_t fp32_frac() { return GET_BITS(v.uint, 0,  23); }
        /// Get Float Exponent
        uint8_t  fp32_exp()  { return GET_BITS(v.uint, 23, 8);  }
        /// Get Float Sign
        uint8_t  fp32_sign() { return GET_BITS(v.uint, 31, 1);  }

        /// Set Float Fraction
        void fp32_frac(uint32_t val) { SET_BITS(v.uint, 0,  23, val); }
        /// Set Float Exponent
        void fp32_exp(uint8_t   val) { SET_BITS(v.uint, 23, 8,  val); }
        /// Set Float Sign
        void fp32_sign(uint8_t  val) { SET_BITS(v.uint, 31, 1,  val); }

        // FP64 segments

        /// Get Double Fraction
        uint64_t fp64_frac() { return GET_BITS(v.uint, 0,  52); }
        /// Get Double Exponent
        uint16_t fp64_exp()  { return GET_BITS(v.uint, 52, 11); }
        /// Get Double Sign
        uint8_t  fp64_sign() { return GET_BITS(v.uint, 63, 1);  }

        /// Set Double Fraction
        void fp64_frac(uint64_t val) { SET_BITS(v.uint, 0,  52, val); }
        /// Set Double Exponent
        void fp64_exp(uint16_t  val) { SET_BITS(v.uint, 52, 11, val); }
        /// Set Double Sign
        void fp64_sign(uint8_t  val) { SET_BITS(v.uint, 63, 1,  val); }

        // RFLAGS

        // Get RFLAGS Carry
        uint8_t rflags_carry()     { return GET_BITS(v.uint, 0,  1); }
        // Get RFLAGS Parity
        uint8_t rflags_parity()    { return GET_BITS(v.uint, 2,  1); }
        // Get RFLAGS Adjust
        uint8_t rflags_adjust()    { return GET_BITS(v.uint, 4,  1); }
        // Get RFLAGS Zero
        uint8_t rflags_zero()      { return GET_BITS(v.uint, 6,  1); }
        // Get RFLAGS Sign
        uint8_t rflags_sign()      { return GET_BITS(v.uint, 7,  1); }
        // Get RFLAGS Trap
        uint8_t rflags_trap()      { return GET_BITS(v.uint, 8,  1); }
        // Get RFLAGS Interrupt
        uint8_t rflags_interrupt() { return GET_BITS(v.uint, 9,  1); }
        // Get RFLAGS Direction
        uint8_t rflags_direction() { return GET_BITS(v.uint, 10, 1); }
        // Get RFLAGS Overflow
        uint8_t rflags_overflow()  { return GET_BITS(v.uint, 11, 1); }

        // Set RFLAGS Carry
        void rflags_carry(uint8_t val)     { SET_BITS(v.uint, 0,  1, val); }
        // Set RFLAGS Parity
        void rflags_parity(uint8_t val)    { SET_BITS(v.uint, 2,  1, val); }
        // Set RFLAGS Adjust
        void rflags_adjust(uint8_t val)    { SET_BITS(v.uint, 4,  1, val); }
        // Set RFLAGS Zero
        void rflags_zero(uint8_t val)      { SET_BITS(v.uint, 6,  1, val); }
        // Set RFLAGS Sign
        void rflags_sign(uint8_t val)      { SET_BITS(v.uint, 7,  1, val); }
        // Set RFLAGS Trap
        void rflags_trap(uint8_t val)      { SET_BITS(v.uint, 8,  1, val); }
        // Set RFLAGS Interrupt
        void rflags_interrupt(uint8_t val) { SET_BITS(v.uint, 9,  1, val); }
        // Set RFLAGS Direction
        void rflags_direction(uint8_t val) { SET_BITS(v.uint, 10, 1, val); }
        // Set RFLAGS Overflow
        void rflags_overflow(uint8_t val)  { SET_BITS(v.uint, 11, 1, val); }
    };
}

#endif
/** @} */
