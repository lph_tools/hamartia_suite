Hamartia Error API
==================

Overview
--------
The Hamartia Error and Detector API was created in order to gain a better understanding of how
errors manifest using a Monte-carlo simulation/injection approach. However, the API can be used for
other error/detector-related things as well since it is a general API.

Motivation
----------
- Simple fault models are currently used
    - Such as flipping a bit in the result
    - More precise models should be used/evaluated
- Fault models have to be reimplemented each time
    - There should be a common, shared interface
    - Can be used across projects and injectors
- Injection should be fast and have low-overhead
    - Separate levels of abstraction
    - Faults should be guaranteed (at some level), focus on the error manifestation
- Allow for large-scale experiments and simulations

Features
--------
- Defines an API which can be used by error/detector models and injectors
- C++/Python Error/Detector model execution (more lanaguages could be added in the future)
- Instruction (or Instruction-like) state forwarding (to Error/Detector models)
- Statistics tracking
- Logging constructs

Extras
------
- Launcher scripts (`src/python/launcher`)
    - Allows for easy batch/parallel runs locally or remotely (i.e. sbatch)
    - Reacts to timeouts/errors that may occur
    - Resumable
- Post-Processing scripts (`src/python/post_processing`)
    - Detector coverage estimator

Installation
------------

The following steps will compile a shared library that can be used in different injectors.

1. Install the following packages (names may be different depending on OS):
    - `swig`
    - `scons`
    - `libyaml-cpp`
2. Install the following python packages (pip):
    - `pyyaml`
    - `mako`
    - `sphinx`
    - `sphinxcontrib-napoleon`
3. Checkout desired branch
4. Setup environment variables
    - `export HAMARTIA_DIR=< error_injector_api>`
5. Compile
    - `cd <error_injector_api>`
    - `scons -Q debug=1`
    - Note: if you want to not have debug messages, omit `-Q debug=1`
