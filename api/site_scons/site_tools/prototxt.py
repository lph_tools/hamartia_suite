"""
prototxt.py: Builder for proto text files
"""

import SCons.Action
import SCons.Builder
import SCons.Defaults
import SCons.Node.FS
import SCons.Util

import os.path

protocs = 'protoc'

def PrototxtEmitter(target, source, env):
    if not target and os.path.isdir(env['PROTOTXT_OUTDIR']):
        base = env['PROTOTXT_OUTDIR']
        target = []
        for src in source:
            name = os.path.splitext(os.path.basename(str(src)))[0]
            tgt = os.path.join(base, name+env['PROTOTXT_TRGTSUFFIX'])
            target.append(tgt)
    
    return target, source

PrototxtBuilder = SCons.Builder.Builder(
        action = "cat $SOURCE | $PROTOC --encode=$PROTOTXT_MESSAGE -I $PROTOTXT_PATH $PROTOTXT_PATH/$PROTOTXT_MESSAGESRC > $TARGET",
        emitter = PrototxtEmitter,
        suffix = '$PROTOTXT_TRGTSUFFIX',
        src_suffix = '$PROTOTXT_SRCSUFFIX')

def generate(env):
    try:
        bld = env['BUILDERS']['Prototxt']
    except KeyError:
        bld = PrototxtBuilder
        env['BUILDERS']['Prototxt'] = bld

    env['PROTOC'] = env.Detect(protocs) or 'protoc'
    env['PROTOTXT_SRCSUFFIX'] = ".proto"
    env['PROTOTXT_TRGTSUFFIX'] = ".protoc"

def exists(env):
    return env.Detect(protocs)
