Hamartia Installation Guide on TACC
===================================

Lonestar5
---------

0. Download Repositories
    - <error_injector_api>: path to Hamartia API repo
    - <error_prof_inj>: path to pin injector repo
    - <rtl_error_injector>: path to RTL injector repo
1. Install dependences
    - move `tacc_ls5.sh` to where you want to store the sources (e.g. $HOME/hamartia_env)
    - `./tacc_ls5.sh` (this will install some binaries and libraries in `$HOME/.local`)
2. Setup environment variables (modify your `~/.bashrc`)
    - `module load scons boost python2/2.7.15 swig`
    - add `$HOME/.local/bin` to $PATH
    - add `$HOME/.local/lib` to $LD_LIBRARY_PATH
    - export BOOST_ROOT=$TACC_BOOST_DIR
    - export PIN_HOME=`<the directory where tacc_ls5.sh was run>/pin` 
    - export HAMARTIA_DIR=<error_injector_api>
    - (RTL model only) export HAMARTIA_ERROR_PATH=`<rtl_error_injector>/src/model.py`
3. Modify RTL config file (RTL model only)
    - `cd <rtl_error_injector>/rtl_components/configs` 
    - `cp nangate.yaml mynangate.yaml`
    - open `mynangate.yaml`
    - modify `cache`,`dirs["rtl_comp"]`, and `cell_libs["dir"]` to the corresponding paths in your account 
4. Compile
    - `cd <error_injector_api>`
    - `scons -Q debug=1`
    - `cd <error_prof_inj>`
    - `scons -Q debug=1`
5. Simple Run
    - `cd <error_prof_inj>/bin`
    - Simple model: `./injector.sh -i 100 -- /bin/ls`
    - RTL model: `./injector.sh -i 100 -e RTL -q <path to your mynangate.yaml> -- /bin/ls`
    - check if `err_prof_inj.out` logs the correct error model and results
6. Regression Tests
    - `cd <error_prof_inj>/test`
    - `pytest`
    

Note: Make sure the environment variables are set correctly before proceeding step 4. Sometimes `source .bashrc` does not work. In that case, you have to reconnect TACC to reload those environment variables. 
