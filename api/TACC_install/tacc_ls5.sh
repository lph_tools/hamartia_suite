#! /bin/bash

# ELF
if [ ! -e ~/.local/lib/libelf.so ]
then
	ln -s /usr/lib64/libelf.so.1 ~/.local/lib/libelf.so
fi

# YAML-CPP
if [ ! -d yaml-cpp ]
then
	echo "Downloading YAML-cpp..."
	curl -OL https://github.com/jbeder/yaml-cpp/archive/release-0.5.3.tar.gz
	tar -xf release-0.5.3.tar.gz
	rm release-0.5.3.tar.gz
	mv yaml-cpp-release-0.5.3 yaml-cpp
fi

if [ ! -d ~/.local/include/yaml-cpp ]
then
	echo "Installing YAML-cpp..."
	mkdir yaml-cpp/build
	pushd `pwd`
	cd yaml-cpp/build; module load boost; export BOOST_ROOT=$TACC_BOOST_DIR; CXX=g++ CC=gcc cmake .. -DBUILD_SHARED_LIBS=ON -DCMAKE_INSTALL_PREFIX:PATH=~/.local; make; make install
	popd
fi

# GPerf
if [ ! -d gperf ]
then
	echo "Downloading gperf..."
	curl -OL http://ftp.gnu.org/pub/gnu/gperf/gperf-3.0.4.tar.gz
	tar gperf-3.0.4.tar.gz
	mv gperf-3.0.4 gperf
	rm gperf-3.0.4.tar.gz
fi

if [ ! -e ~/.local/bin/gperf ]
then
	echo "Installing gperf..."
	pushd `pwd`
	cd gperf; ./configure --prefix ~/.local; make clean; make; make install
	popd
fi

# IVerilog
if [ ! -d iverilog ]
then
	echo "Downloading iverilog..."
	curl -OL ftp://icarus.com/pub/eda/verilog/v10/verilog-10.1.1.tar.gz
	tar -xf verilog-10.1.1.tar.gz
	mv verilog-10.1.1 iverilog
	rm verilog-10.1.1.tar.gz
fi

if [ ! -e ~/.local/bin/iverilog ]
then
	echo "Installing iverilog..."
	pushd `pwd`
	cd iverilog; ./configure --prefix ~/.local; make clean; make; make install
	popd
fi

# Pin
if [ ! -d pin ]
then 
	echo "Downloading pin..."
	curl -OL http://software.intel.com/sites/landingpage/pintool/downloads/pin-2.14-71313-gcc.4.4.7-linux.tar.gz
	tar -xf pin-2.14-71313-gcc.4.4.7-linux.tar.gz
	mv pin-2.14-71313-gcc.4.4.7-linux pin
	rm pin-2.14-71313-gcc.4.4.7-linux.tar.gz
fi

#Python (note: the following module load command should be updated as the system updates)
module load python2/2.7.15
dummy_pygraph=0
if [ ! -d ~/.local/lib/python2.7/site-packages/pygraphviz ]
then
	echo "Making dummy pygraphviz..."
	dummy_pygraph=1
	mkdir -p ~/.local/lib/python2.7/site-packages/pygraphviz
	touch ~/.local/lib/python2.7/site-packages/pygraphviz/__init__.py
fi
pip install --trusted-host pypi.python.org --user -I pyverilog==0.9.3
if [ $dummy_pygraph ]
then
	echo "Removing dummy pygraphviz..."
	rm -rf ~/.local/lib/python2.7/site-packages/pygraphviz
fi
pip install --trusted-host pypi.python.org --user mako
pip install --trusted-host pypi.python.org --user jinja2
pip install --trusted-host pypi.python.org --user pyyaml
pip install --trusted-host pypi.python.org --user sphinx
pip install --trusted-host pypi.python.org --user sphinxcontrib-napoleon
pip install --trusted-host pypi.python.org --user paramiko
pip install --trusted-host pypi.python.org --user futures
pip install --trusted-host pypi.python.org --user pytest
