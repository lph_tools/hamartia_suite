// Copyright 2015, The University of Texas at Austin 
// All rights reserved.
// 
// THIS FILE IS PART OF THE HAMARTIA FAULT/ERROR ANALYSIS SUITE
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met: 
// 
// 1. Redistributions of source code must retain the above copyright
// notice, this list of conditions and the following disclaimer. 
// 
// 2. Redistributions in binary form must reproduce the above copyright
// notice, this list of conditions and the following disclaimer in the
// documentation and/or other materials provided with the distribution. 
// 
// 3. Neither the name of the copyright holder nor the names of its
// contributors may be used to endorse or promote products derived from
// this software without specific prior written permission. 
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
// FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
// COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
// BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
// ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

/*
 * \file
 * \brief Error API types
 * \author Nicholas Kelly, University of Texas
 *
 * \addtogroup types
 * @{
 */

#include <hamartia_api/interface/instruction_data.h>

namespace hamartia_api
{
    // Operand
    Operand::Operand() {}

    Operand::Operand(OperandType _type, DataType _data_type, uint16_t _width, uint8_t _simd_width) : type(_type), data_type(_data_type), width(_width), simd_width(_simd_width)
    {
        value.resize(_simd_width);
        old_value.resize(_simd_width);
    }

    Operand::~Operand() {}

    Operand& Operand::operator=(const Operand &op)
    {
        type       = op.type;
        data_type  = op.data_type;
        width      = op.width;
        simd_width = op.simd_width;
        value      = op.value;
        old_value  = op.old_value;
        reg_id     = op.reg_id;
        reg        = op.reg;
        address    = op.address;

        return *this;
    }

    // Instruction
    Instruction::Instruction() {}

    Instruction::~Instruction() {}

    Instruction& Instruction::operator=(const Instruction &inst)
    {
        opcode    = inst.opcode;
        operation = inst.operation;
        data      = inst.data;
        packing   = inst.packing;
        condition = inst.condition;
        grouping  = inst.grouping;

        return *this;
    }

    // Instruction data
    InstructionData::InstructionData() {}

    InstructionData::~InstructionData() {}

    InstructionData& InstructionData::operator=(const InstructionData &inst_data)
    {
        instruction = inst_data.instruction;
        asm_line    = inst_data.asm_line;
        asm_data    = inst_data.asm_data;
        address     = inst_data.address;
        data_type   = inst_data.data_type;
        simd_width  = inst_data.simd_width;
        width       = inst_data.width;
        inputs      = inst_data.inputs;
        outputs     = inst_data.outputs;

        return *this;
    }

    Operand * InstructionData::addInput(uint32_t _simd_width)
    {
        _simd_width = (_simd_width == 0) ? simd_width : _simd_width;
        Operand op(OT_REGISTER, data_type, width, _simd_width);
        inputs.push_back(op);

        return &inputs.back();
    }

    Operand * InstructionData::addOutput(uint32_t _simd_width)
    {
        _simd_width = (_simd_width == 0) ? simd_width : _simd_width;
        Operand op(OT_REGISTER, data_type, width, _simd_width);
        outputs.push_back(op);

        return &outputs.back();
    }
}
