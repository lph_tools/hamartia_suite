// Copyright 2015, The University of Texas at Austin 
// All rights reserved.
// 
// THIS FILE IS PART OF THE HAMARTIA FAULT/ERROR ANALYSIS SUITE
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met: 
// 
// 1. Redistributions of source code must retain the above copyright
// notice, this list of conditions and the following disclaimer. 
// 
// 2. Redistributions in binary form must reproduce the above copyright
// notice, this list of conditions and the following disclaimer in the
// documentation and/or other materials provided with the distribution. 
// 
// 3. Neither the name of the copyright holder nor the names of its
// contributors may be used to endorse or promote products derived from
// this software without specific prior written permission. 
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
// FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
// COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
// BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
// ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

/*
 * \file
 * \brief Error API types
 * \author Nicholas Kelly, University of Texas
 *
 * \addtogroup types
 * @{
 */

#include <hamartia_api/interface/error_context.h>

namespace hamartia_api
{
    // Error Info
    ErrorInfo::ErrorInfo() {}
    ErrorInfo::~ErrorInfo() {}

    ErrorInfo& ErrorInfo::operator=(const ErrorInfo &info)
    {
        error_model = info.error_model;
        error_config = info.error_config;
        detector_model = info.detector_model;
        detector_config = info.detector_config;

        return *this;
    }

    bool ErrorInfo::hasErrorModel()
    {
        return !error_model.empty();
    }

    bool ErrorInfo::hasErrorConfig()
    {
        return !error_config.empty();
    }

    bool ErrorInfo::hasDetectorModel()
    {
        return !detector_model.empty();
    }

    bool ErrorInfo::hasDetectorConfig()
    {
        return !detector_config.empty();
    }

    void ErrorInfo::successResponse()
    {
        setResponse(RESP_SUCCESS);
    }

    void ErrorInfo::errorResponse(std::string message)
    {
        setResponse(RESP_SUCCESS, message);
    }

    void ErrorInfo::setResponse(Resp resp, std::string message)
    {
        response = resp;
        response_message = message;
    }

    void ErrorInfo::clearResponse()
    {
        setResponse(RESP_NONE);
    }

    bool ErrorInfo::hasResponse()
    {
        return response != RESP_NONE;
    }

    // Error Context
    ErrorContext::ErrorContext() {}

    ErrorContext::~ErrorContext() {}

    ErrorContext& ErrorContext::operator=(const ErrorContext &cxt)
    {
        error_info       = cxt.error_info;
        instruction_data = cxt.instruction_data;
        prev_instruction_data = cxt.prev_instruction_data;
        statistics       = cxt.statistics;
        trigger          = cxt.trigger;

        return *this;
    }
}
