// Copyright 2015, The University of Texas at Austin 
// All rights reserved.
// 
// THIS FILE IS PART OF THE HAMARTIA FAULT/ERROR ANALYSIS SUITE
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met: 
// 
// 1. Redistributions of source code must retain the above copyright
// notice, this list of conditions and the following disclaimer. 
// 
// 2. Redistributions in binary form must reproduce the above copyright
// notice, this list of conditions and the following disclaimer in the
// documentation and/or other materials provided with the distribution. 
// 
// 3. Neither the name of the copyright holder nor the names of its
// contributors may be used to endorse or promote products derived from
// this software without specific prior written permission. 
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
// FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
// COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
// BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
// ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

/**
 *  \file
 *  \brief Error model definition
 *
 *  \version 1.0.0
 *  \author Nicholas Kelly, University of Texas
 *
 *  \addtogroup error_model_api
 *  @{
 */

// Libs
#include <hamartia_api/error_models/basic_models.h>
#include <string>

// Error models
#include <hamartia_api/error_models/null.h>
#include <hamartia_api/error_models/burst.h>
#include <hamartia_api/error_models/random.h>
#include <hamartia_api/error_models/randbit.h>
#include <hamartia_api/error_models/window_randbit.h>
#include <hamartia_api/error_models/mantissa_burst.h>
#include <hamartia_api/error_models/mantissa_random.h>
#include <hamartia_api/error_models/mantissa_randbit.h>
#include <hamartia_api/error_models/timing_prev.h>
//#include <hamartia_api/error_models/bhive/bhive.h>
#include <hamartia_api/error_models/timing_lut/timing_lut.h>
#include <hamartia_api/error_models/mem.h>

REGISTER_ERROR_LIBRARY(BasicErrorModels)

std::string BasicErrorModels::GetName()
{
    return "basic_error_models";
}

BasicErrorModels::BasicErrorModels()
{
    // No injection (for overhead porfiling)
    REGISTER_ERROR_MODEL(NullError, NULL)

    // Burst Models
    REGISTER_ERROR_MODEL_ARGS(BurstError, BURST1, 1)
    REGISTER_ERROR_MODEL_ARGS(BurstError, BURST2, 2)
    REGISTER_ERROR_MODEL_ARGS(BurstError, BURST4, 4)
    REGISTER_ERROR_MODEL_ARGS(BurstError, BURST8, 8)

    // Random models
    REGISTER_ERROR_MODEL_ARGS(RandomError, RANDOM, 1)
    REGISTER_ERROR_MODEL_ARGS(RandomError, RANDOM_LH, 2)

    // Random Bit models
    REGISTER_ERROR_MODEL_ARGS(RandBitError, RANDBIT1, 1)
    REGISTER_ERROR_MODEL_ARGS(RandBitError, RANDBIT2, 2)
    REGISTER_ERROR_MODEL_ARGS(RandBitError, RANDBIT4, 4)
    REGISTER_ERROR_MODEL_ARGS(RandBitError, RANDBIT8, 8)

    // Window Random Bit models
    REGISTER_ERROR_MODEL_ARGS(WRandBitError, WRANDBIT1, 1)
    REGISTER_ERROR_MODEL_ARGS(WRandBitError, WRANDBIT2, 2)
    REGISTER_ERROR_MODEL_ARGS(WRandBitError, WRANDBIT4, 4)
    REGISTER_ERROR_MODEL_ARGS(WRandBitError, WRANDBIT8, 8)

    // Mantissa Burst models
    REGISTER_ERROR_MODEL_ARGS(MantissaBurstError, MANTB1, 1)
    REGISTER_ERROR_MODEL_ARGS(MantissaBurstError, MANTB2, 2)
    REGISTER_ERROR_MODEL_ARGS(MantissaBurstError, MANTB4, 4)
    REGISTER_ERROR_MODEL_ARGS(MantissaBurstError, MANTB8, 8)

    // Mantissa Random models
    // Everything but top X-bits
    REGISTER_ERROR_MODEL_ARGS(MantissaRandomError, MANTRAND,   0,  0)
    REGISTER_ERROR_MODEL_ARGS(MantissaRandomError, MANTRAND2,  2,  0)
    REGISTER_ERROR_MODEL_ARGS(MantissaRandomError, MANTRAND4,  4,  0)
    REGISTER_ERROR_MODEL_ARGS(MantissaRandomError, MANTRAND8,  8,  0)
    REGISTER_ERROR_MODEL_ARGS(MantissaRandomError, MANTRAND16, 16, 0)
    // Top X-bits
    REGISTER_ERROR_MODEL_ARGS(MantissaRandomError, MANTRAND2I,  2,  1)
    REGISTER_ERROR_MODEL_ARGS(MantissaRandomError, MANTRAND4I,  4,  1)
    REGISTER_ERROR_MODEL_ARGS(MantissaRandomError, MANTRAND8I,  8,  1)
    REGISTER_ERROR_MODEL_ARGS(MantissaRandomError, MANTRAND16I, 16, 1)

    // Mantissa Random Bit models
    REGISTER_ERROR_MODEL_ARGS(MantissaRandBitError, MANTR1, 1)
    REGISTER_ERROR_MODEL_ARGS(MantissaRandBitError, MANTR2, 2)
    REGISTER_ERROR_MODEL_ARGS(MantissaRandBitError, MANTR4, 4)
    REGISTER_ERROR_MODEL_ARGS(MantissaRandBitError, MANTR8, 8)

    // Previous-value timing error models
    REGISTER_ERROR_MODEL_ARGS(TimingPrevError, TIMING_PREV1, 1)

    // b-HiVE error models at different Vdd levels
    // Vdd > 1.0 nearly has no errors, so we start from 0.9 V instead (BHIVE9 means 0.9 V)
    //REGISTER_ERROR_MODEL_ARGS(BhiveError, TIMING_BHIVE9, 9)
    //REGISTER_ERROR_MODEL_ARGS(BhiveError, TIMING_BHIVE8, 8)
    //REGISTER_ERROR_MODEL_ARGS(BhiveError, TIMING_BHIVE7, 7)
    //REGISTER_ERROR_MODEL_ARGS(BhiveError, TIMING_BHIVE6, 6)
    //REGISTER_ERROR_MODEL_ARGS(BhiveError, TIMING_BHIVE5, 5)

    // timing error model with look-up tables
    REGISTER_ERROR_MODEL_ARGS(TimingLUTError, TIMING_LUT85, 85)
    REGISTER_ERROR_MODEL_ARGS(TimingLUTError, TIMING_LUT78, 78)

    // memory error models
    REGISTER_ERROR_MODEL_ARGS(MemError, MEM_HIGH, 0)
    REGISTER_ERROR_MODEL_ARGS(MemError, MEM_LOW,  1)
    REGISTER_ERROR_MODEL_ARGS(MemError, MEM_FLIP, 2)

}

/** @} */
