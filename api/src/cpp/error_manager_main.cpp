// Copyright 2015, The University of Texas at Austin 
// All rights reserved.
// 
// THIS FILE IS PART OF THE HAMARTIA FAULT/ERROR ANALYSIS SUITE
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met: 
// 
// 1. Redistributions of source code must retain the above copyright
// notice, this list of conditions and the following disclaimer. 
// 
// 2. Redistributions in binary form must reproduce the above copyright
// notice, this list of conditions and the following disclaimer in the
// documentation and/or other materials provided with the distribution. 
// 
// 3. Neither the name of the copyright holder nor the names of its
// contributors may be used to endorse or promote products derived from
// this software without specific prior written permission. 
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
// FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
// COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
// BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
// ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#include <hamartia_api/error_manager.h>
#include <hamartia_api/debug.h>
#include <hamartia_api/interface.h>
#include <hamartia_api/error_model.h>
#include <hamartia_api/detector_model.h>
#include <hamartia_api/helpers/response.h>

// LIB INCLUDES
#include <dlfcn.h>
#include <glob.h>
#include <cstdio>
#include <cstdlib>
#include <unistd.h>
#include <getopt.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <errno.h>
#include <iostream>
#include <fstream>
#include <sstream>

// Main function (callable error manager)
int main(int argc, char *argv[])
{
    // Parse args and initialize
    hamartia_api::error_manager::Init(false, argc, argv);

    // Input loop
    char buf[BUFFER_LEN];
    while (1) {
        // Get length and context data
        std::cin.read(buf, sizeof(uint32_t));
        uint32_t cxt_len = *((uint32_t *) buf);
        INFO_PRINT("Msg in " << cxt_len)
        std::string in_data;
        while (std::cin && in_data.size() < cxt_len) {
            std::cin.getline(buf, BUFFER_LEN);
            in_data.append((char *) buf);
        }

        // Get error context
        hamartia_api::ErrorContext error_context;
        if (!error_context.ParseFromString(in_data)) {
            hamartia_api::response_helper::ErrorResponse(&error_context, "Error parsing context!");
        }

        // Commands
        bool success = false; 
        switch (error_context.error_info().cmd()) {
            case hamartia_api::CMD_RUN_ERROR_MODEL:
                if (error_context.error_info().mechanisms_size() == 1) {
                    FATAL_ERROR("Incorrect amount of mechanisms")
                }
                success = hamartia_api::ErrorModel::CallErrorModel(error_context.error_info().error_model(), error_context.error_info().mechanisms(0), &error_context);
                break;
            case hamartia_api::CMD_RUN_DETECTOR_MODEL:
                if (error_context.error_info().mechanisms_size() == 1) {
                    FATAL_ERROR("Incorrect amount of mechanisms")
                }
                success = hamartia_api::DetectorModel::CallDetectorModel(error_context.error_info().detector_model(), &error_context);
                break;
            default:
                break;
        }

        // Generic error or no CMD mapping
        if (!error_context.error_info().has_response()) {
            if (success) {
                hamartia_api::response_helper::SuccessResponse(&error_context);
            } else {
                hamartia_api::response_helper::ErrorResponse(&error_context, "Undefined error!");
            }
        }

        // Write out result
        std::string cxt_str;
        if (!error_context.SerializeToString(&cxt_str)) {
            FATAL_ERROR("Error serializing context!");
        }
        cxt_len = cxt_str.size();
        INFO_PRINT("Msg out " << cxt_len)
        std::cout.write((char *) &cxt_len, sizeof(uint32_t));
        std::cout << cxt_str;
        std::cout.flush();
    }
}
