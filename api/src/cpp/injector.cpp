// Copyright 2015, The University of Texas at Austin 
// All rights reserved.
// 
// THIS FILE IS PART OF THE HAMARTIA FAULT/ERROR ANALYSIS SUITE
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met: 
// 
// 1. Redistributions of source code must retain the above copyright
// notice, this list of conditions and the following disclaimer. 
// 
// 2. Redistributions in binary form must reproduce the above copyright
// notice, this list of conditions and the following disclaimer in the
// documentation and/or other materials provided with the distribution. 
// 
// 3. Neither the name of the copyright holder nor the names of its
// contributors may be used to endorse or promote products derived from
// this software without specific prior written permission. 
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
// FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
// COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
// BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
// ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

/**
 *  \file
 *  \brief Error models, data structures
 *
 *  \version 1.0.0
 *  \author Michael Sullivan, University of Texas
 *  \author Nicholas Kelly, University of Texas
 *  \author Chun-Kai Chang, University of Texas
 *
 *  \addtogroup error_profiler_injector
 *  @{
 */

// Include first for no warnings
#include <Python.h>

#include <hamartia_api/debug.h>
#include <hamartia_api/injector.h>
#include <hamartia_api/interface.h>
#include <hamartia_api/error_manager.h>
#include <hamartia_api/error_model.h>
#include <hamartia_api/detector_model.h>
#include <cassert>
namespace hamartia_api 
{
    Injector::Injector() : phase(PHASE_EVAL), error_methods(0), num_errors_injected(0), num_errors_recorded(0), num_errors_to_inject(1), num_errors_to_record(1)
    {}

    Injector::Injector(std::string error_model, std::string error_config) : phase(PHASE_EVAL), error_methods(0), num_errors_injected(0), num_errors_recorded(0), num_errors_to_inject(1), num_errors_to_record(1)
    {
        current_context.error_info.error_model = error_model;
        current_context.error_info.error_config = error_config;
        current_context.newStats();

        // Setup
        error_manager::Setup(&current_context, &log);
    }

    Injector::Injector(std::string error_model, std::string error_config, std::string detector_model, std::string detector_config) : phase(PHASE_EVAL), error_methods(0), num_errors_injected(0), num_errors_recorded(0), num_errors_to_inject(1), num_errors_to_record(1)
    {
        current_context.error_info.error_model = error_model;
        current_context.error_info.error_config = error_config;
        current_context.error_info.detector_model = detector_model;
        current_context.error_info.detector_config = detector_config;
        current_context.newStats();

        // Setup
        error_manager::Setup(&current_context, &log);
    }

    Injector::~Injector()
    {
        // Cleanup
        //error_manager::Cleanup(&current_context, &log);
    }

    bool Injector::PreInjectError(bool auto_mode)
    {
        bool success = true;
        ErrorContext * icontext = &injection_context;

        if (!auto_mode || phase != PHASE_EVAL) {
            // Inject into inputs
            if (error_manager::ModelMethodValid(MODEL_ERROR, METHOD_PRE_INJECT, &current_context) &&
                AcceptedErrorMethod(METHOD_PRE_INJECT)) {
                success = error_manager::PreInjectError(&current_context, icontext, &log);
                if (!success) {
                    FATAL_ERROR("Pre-Injection error: " << icontext->error_info.response_message);
                }

                // Successful injection
                if (icontext->error_info.response != RESP_NO_METHOD) {
                    phase = PHASE_PRE_INJECT;

                    // FIXME: only if num_errors_recorded == 0?
                    if (auto_mode) {
                        INFO_PRINT("Save input error state")
                        PreInjectionContextToState();
                    }
                }
            } else {
                INFO_PRINT("Preinject method invalid...")
                success = false;
            }
        } else if (auto_mode && phase == PHASE_EVAL) {
            // Gather correct inputs
            INFO_PRINT("Pre load...")
            PreStateToContext();
        }

        // If successful error (even if record > 0), inject into state, generated outputs needed either way
        return success;
    }

    uint64_t GetMaxIteration(ErrorContext* cxt)
    {
        // To avoid infinite loop, we need a upper bound of iterations
        // currently we inject the first SIMD lane only, so the injector loop iterates 4 * data width (in bits)
        // in the future, if we randomly select a SIMD lane to inject, the multiplier should also scales
        uint64_t op_width = cxt->instruction_data.width;
        uint64_t MULTIPLIER = 4; //use 4 so that shift can be used 
        return  op_width * MULTIPLIER;
    } 

    bool Injector::InjectError(bool auto_mode)
    {

        bool success = true;
        bool unmasked = false;
        bool undetected = false;
        bool loop = ((error_manager::ModelMethodValid(MODEL_ERROR, METHOD_INJECT, &current_context) &&
                      AcceptedErrorMethod(METHOD_INJECT)) &&
                    (error_manager::ModelMethodValid(MODEL_ERROR, METHOD_PRE_INJECT, &current_context) &&
                      !AcceptedErrorMethod(METHOD_PRE_INJECT)));//Disable loop when pre injection is registered
        ErrorContext * icontext = &injection_context;
        bool null_error_model = injection_context.error_info.error_model == "NULL";

        if (!auto_mode || phase != PHASE_EVAL) {
            // Check Pre-injection
            if (auto_mode && phase == PHASE_PRE_INJECT) {
                InjectionStateToContext();
            }

            // Save context
            ErrorContext inj_cxt_cpy;

            uint64_t max_iter = GetMaxIteration(&current_context);

            // Inject loop
            do {
                // Copy context
                inj_cxt_cpy = *icontext;

                // Inject into outputs
                if (error_manager::ModelMethodValid(MODEL_ERROR, METHOD_INJECT, &current_context) &&
                    AcceptedErrorMethod(METHOD_INJECT)) {
                    success = error_manager::InjectError(&inj_cxt_cpy, &log);
                    if (!success) {
                        FATAL_ERROR("Injection error: " << inj_cxt_cpy.error_info.response_message);
                    }
                } else {
                    INFO_PRINT("Inject method invalid...")
                }
                Resp error_model_response = inj_cxt_cpy.error_info.response;

                // Check error
                // Bypass checking for the null model (for overhead profiling)
                if ((success || phase == PHASE_PRE_INJECT) && !null_error_model) {
                    success = error_manager::CheckError(&current_context, &inj_cxt_cpy, unmasked, undetected, &log);
                    if (!success) {
                        INFO_PRINT("Injected error masked by instruction, continue...")
                    }
                }

                // Restore error model response
                inj_cxt_cpy.error_info.response = error_model_response;

                // Update stats
                InjectionStats & stats = ErrorContext::CurrentStats(&inj_cxt_cpy);
                stats.iterations++;
                stats.updateDetection(unmasked, undetected);
                inj_cxt_cpy.pushStats(&injection_context);

                if (stats.iterations > max_iter) { 
                    INFO_PRINT("Reach max iterations...")
                    INFO_PRINT("[All Detected] Terminate experiement!")
                    INFO_PRINT("Dump error log to err_prof_inj.out ...")
                    log.LogInjectedValues(injection_context, num_errors_recorded);
                    std::ofstream logfs("err_prof_inj.out", std::ofstream::out);
                    SaveLog(logfs);
                    logfs.close();
                    std::exit(0);
                }

            } while (loop && !success);

            // Successful injection
            if (success) {
                *icontext = inj_cxt_cpy;
                // Log record
                log.LogInjectedValues(injection_context, num_errors_recorded);
                ++num_errors_recorded;
        
                // Final record
                if (num_errors_recorded == num_errors_to_record) {
                    phase = PHASE_INJECTED;
                    if (auto_mode) {
                        INFO_PRINT("Old output to state...")
                        OldOutputContextToState();
                        INFO_PRINT("Save error state...")
                        InjectionContextToState();
                    }
                }
            }
            else if (!success && phase == PHASE_PRE_INJECT) {
                // Pre-Inject is logically masked
                // Should we log masked faults ?
                phase = PHASE_INJECT;
            }    
        } else if (auto_mode && phase == PHASE_EVAL) {
            // Gather correct outputs
            INFO_PRINT("Post load...")
            PostStateToContext();

            // Log initial values
            log.LogInitialValues(current_context);
        }

        return success;
    }

    void Injector::PreStateToContext()
    {
        if (phase == PHASE_EVAL) {
            // Collect correct inputs
            PreCurrentStateToContext();
        }
    }

    void Injector::PostStateToContext()
    {
        if (phase == PHASE_EVAL) {
            // Collect correct inputs
            PostCurrentStateToContext();
        } else {
            // Collect incorrect (injected) outputs
            InjectionStateToContext();
        }
    }

    void Injector::PostCurrentStateToContext()
    {
        phase = PHASE_INJECT;
        injection_context = current_context;
        record_context = current_context;
    }

    void Injector::CommitContext()
    {
        INFO_PRINT("Commit context...")

        // Update statistics (top of stack)
        injection_context.pushStats(&current_context);

        // Update instruction data
        current_context.instruction_data = injection_context.instruction_data;

        // Phase and increment error injected
        if (num_errors_recorded < num_errors_to_record) {//more errors to record
            phase = PHASE_EVAL;
        }
        current_context.newStats();
        ++num_errors_injected;
    }

    void Injector::SaveLog(std::ofstream &log_file)
    {
        log.Emit(log_file);
    }
}

/** @} */
