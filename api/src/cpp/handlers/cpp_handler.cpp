// Copyright 2015, The University of Texas at Austin 
// All rights reserved.
// 
// THIS FILE IS PART OF THE HAMARTIA FAULT/ERROR ANALYSIS SUITE
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met: 
// 
// 1. Redistributions of source code must retain the above copyright
// notice, this list of conditions and the following disclaimer. 
// 
// 2. Redistributions in binary form must reproduce the above copyright
// notice, this list of conditions and the following disclaimer in the
// documentation and/or other materials provided with the distribution. 
// 
// 3. Neither the name of the copyright holder nor the names of its
// contributors may be used to endorse or promote products derived from
// this software without specific prior written permission. 
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
// FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
// COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
// BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
// ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

/**
 * \file
 * \brief Error Manager, responsible calling error/detector models and/or other injectors
 * \author Nicholas Kelly, University of Texas
 *
 * \warning Dynamic loading and executable not tested!
 *
 * \addtogroup error_manager
 * @{
 */

#include <hamartia_api/handlers/cpp_handler.h>
#include <hamartia_api/debug.h>
#include <hamartia_api/interface.h>

#include <dlfcn.h>

namespace hamartia_api
{
    // Error model
    std::vector<ModelHandler *> CppErrorModelHandler::LoadFromSharedLibrary(std::string path)
    {
        INFO_PRINT("Loading " << path << "...")
        std::vector<ModelHandler *> handlers;
        void * so_handle = dlopen(path.c_str(), RTLD_LAZY);
        if (so_handle == NULL) {
            ERROR_PRINT(dlerror())
            return handlers;
        }

        ErrorModelLibrary * (*register_library)();
        register_library = (ErrorModelLibrary * (*)()) dlsym(so_handle, "register_library");
        if (register_library == NULL) {
            ERROR_PRINT(dlerror())
            return handlers;
        }

        void (*unregister_library)(ErrorModelLibrary *);
        unregister_library = (void (*)(ErrorModelLibrary *)) dlsym(so_handle, "unregister_library");
        if (unregister_library == NULL) {
            ERROR_PRINT(dlerror())
            return handlers;
        }

        ErrorModelLibrary * lib = (ErrorModelLibrary *) register_library();
        handlers = LoadFromLibrary(lib);
        unregister_library(lib);

        return handlers;
    }

    std::vector<ModelHandler *> CppErrorModelHandler::LoadFromLibrary(ErrorModelLibrary * lib)
    {
        std::vector<ModelHandler *> handlers;
        std::vector<ErrorModel *> models = lib->GetModels();
        for (std::vector<ErrorModel *>::iterator it = models.begin(); it != models.end(); ++it) {
            CppErrorModelHandler * handler = new CppErrorModelHandler(*it);
            handlers.push_back(handler);
        }

        return handlers;
    }

    CppErrorModelHandler::CppErrorModelHandler() : ModelHandler(LANG_CPP, MODEL_ERROR), error_model(NULL) {}

    CppErrorModelHandler::CppErrorModelHandler(ErrorModel * _error_model) : ModelHandler(LANG_CPP, MODEL_ERROR), error_model(_error_model)
    {
        methods = error_model->Methods();
    }

    CppErrorModelHandler::~CppErrorModelHandler()
    {
        delete error_model;
    }

    std::string CppErrorModelHandler::modelName()
    {
        return error_model->GetName();
    }

    bool CppErrorModelHandler::invoke(ModelMethod method, ErrorContext *cxt, Log *log)
    {
        // Get config
        YAML::Node * config = NULL;
        YAML::Node yml_config;
        if (cxt->error_info.hasErrorConfig()) {
            yml_config = YAML::LoadFile(cxt->error_info.error_config);
            config = &yml_config;
        }
        // Call
        bool success = false;
        switch(method)
        {
            case METHOD_SETUP:
                success = error_model->Setup(cxt, config, log);
                break;
            case METHOD_CLEANUP:
                success = error_model->Cleanup(cxt, config, log);
                break;
            case METHOD_INJECT:
                success = error_model->InjectError(cxt, config, log);
                break;
            case METHOD_PRE_INJECT:
                success = error_model->PreInjectError(cxt, config, log);
                break;
            default:
                FATAL_ERROR("No Error Model Method!")
                break;
        }

        ModelHandler::DefaultResponse(success, cxt);
        setMethodValidity(method, cxt);
        return success;
    }

    // Detector model
    std::vector<ModelHandler *> CppDetectorModelHandler::LoadFromSharedLibrary(std::string path)
    {
        INFO_PRINT("Loading " << path << "...")
        std::vector<ModelHandler *> handlers;
        void * so_handle = dlopen(path.c_str(), RTLD_LAZY);
        if (so_handle == NULL) {
            ERROR_PRINT(dlerror())
            return handlers;
        }

        DetectorModelLibrary * (*register_library)();
        register_library = (DetectorModelLibrary * (*)()) dlsym(so_handle, "register_detector_library");
        if (register_library == NULL) {
            ERROR_PRINT(dlerror())
            return handlers;
        }

        void (*unregister_library)(DetectorModelLibrary *);
        unregister_library = (void (*)(DetectorModelLibrary *)) dlsym(so_handle, "unregister_detector_library");
        if (unregister_library == NULL) {
            ERROR_PRINT(dlerror())
            return handlers;
        }

        DetectorModelLibrary * lib = (DetectorModelLibrary *) register_library();
        handlers = LoadFromLibrary(lib);
        unregister_library(lib);

        return handlers;
    }

    std::vector<ModelHandler *> CppDetectorModelHandler::LoadFromLibrary(DetectorModelLibrary * lib)
    {
        std::vector<ModelHandler *> handlers;
        std::vector<DetectorModel *> models = lib->GetModels();
        for (std::vector<DetectorModel *>::iterator it = models.begin(); it != models.end(); ++it) {
            CppDetectorModelHandler * handler = new CppDetectorModelHandler(*it);
            handlers.push_back(handler);
        }

        return handlers;
    }

    CppDetectorModelHandler::CppDetectorModelHandler() : ModelHandler(LANG_CPP, MODEL_DETECTOR), detector_model(NULL) {}

    CppDetectorModelHandler::CppDetectorModelHandler(DetectorModel * _detector_model) : ModelHandler(LANG_CPP, MODEL_DETECTOR), detector_model(_detector_model)
    {
        methods = detector_model->Methods();
    }

    CppDetectorModelHandler::~CppDetectorModelHandler()
    {
        delete detector_model;
    }

    std::string CppDetectorModelHandler::modelName()
    {
        return detector_model->GetName();
    }

    bool CppDetectorModelHandler::invoke(ModelMethod method, ErrorContext *cxt, Log *log)
    {
        // Get config
        YAML::Node * config = NULL;
        YAML::Node yml_config;
        if (cxt->error_info.hasDetectorConfig()) {
            yml_config = YAML::LoadFile(cxt->error_info.detector_config);
            config = &yml_config;
        }
        // Call
        bool success = false;
        switch(method)
        {
            case METHOD_SETUP:
                success = detector_model->Setup(cxt, config, log);
                break;
            case METHOD_CLEANUP:
                success = detector_model->Cleanup(cxt, config, log);
                break;
            case METHOD_CHECK_ERROR:
                success = detector_model->CheckError(cxt, config, log);
                break;
            default:
                FATAL_ERROR("No Detector Model Method!")
                break;
        }

        ModelHandler::DefaultResponse(success, cxt);
        setMethodValidity(method, cxt);
        return success;
    }
}

/** @} */
