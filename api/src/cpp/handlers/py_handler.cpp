// Copyright 2015, The University of Texas at Austin 
// All rights reserved.
// 
// THIS FILE IS PART OF THE HAMARTIA FAULT/ERROR ANALYSIS SUITE
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met: 
// 
// 1. Redistributions of source code must retain the above copyright
// notice, this list of conditions and the following disclaimer. 
// 
// 2. Redistributions in binary form must reproduce the above copyright
// notice, this list of conditions and the following disclaimer in the
// documentation and/or other materials provided with the distribution. 
// 
// 3. Neither the name of the copyright holder nor the names of its
// contributors may be used to endorse or promote products derived from
// this software without specific prior written permission. 
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
// FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
// COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
// BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
// ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

/**
 * \file
 * \brief Error Manager, responsible calling error/detector models and/or other injectors
 * \author Nicholas Kelly, University of Texas
 *
 * \warning Dynamic loading and executable not tested!
 *
 * \addtogroup error_manager
 * @{
 */

#include <hamartia_api/handlers/py_handler.h>
#include <hamartia_api/debug.h>
#include <hamartia_api/interface.h>
#include <hamartia_api/utils/file.h>
#include <hamartia_api/swig.h>

#include <string>

namespace hamartia_api
{
    static bool py_initalized = false;
    static PyObject *yaml;

    static void InitializePython()
    {
        if (py_initalized) {
            return;
        }
        INFO_PRINT("Init python...")
        Py_Initialize();

        // Modules
        yaml = PyImport_Import(PyString_FromString("yaml"));
        PY_PTR_CHECK(yaml, "Unable to load python yaml!")
        py_initalized = true;
    }

    static PyObject * PyLoadYaml(std::string path)
    {
        INFO_PRINT("Load PYYAML file...")
        PyObject *file = PyFile_FromString((char*) path.c_str(), "r");
        PY_PTR_CHECK(file, "Unable to load yaml file!")
        PyObject *yaml_out = PyObject_CallMethod(yaml, "load", "O", file);
        PyObject_CallMethod(file, "close", NULL);
        Py_DECREF(file);

        return yaml_out;
    }

    // TODO: call at one point!
    static void UninitializePython()
    {
        if (!py_initalized) {
            return;
        }
        INFO_PRINT("Uninit python...")
        Py_Finalize();
        py_initalized = false;
    }

    // Error model handler
    std::vector<ModelHandler *> PyErrorModelHandler::LoadFromModule(std::string path)
    {
        std::vector<ModelHandler *> handlers;

        InitializePython();

        std::string dir = utils::FileDirname(path);
        std::string module_str = utils::FileFilename(utils::FileBasename(path));
        PyObject * module_name, * module_path, * module, * reg_lib, * lib;

        module_path = PyString_FromString((char*) dir.c_str());
        PyList_Append(PySys_GetObject("path"), module_path);
        Py_DECREF(module_path);
       
        module_name = PyString_FromString((char*) module_str.c_str());
        module = PyImport_Import(module_name);
        Py_DECREF(module_name);

        if (module != NULL) {
            reg_lib = PyObject_GetAttrString(module, "register");
            lib = PyObject_CallObject(reg_lib, NULL);
            if (lib != NULL) {
                handlers = LoadFromLibrary(lib);
            } else {
                if (PyErr_Occurred()) PyErr_Print();
                FATAL_ERROR("'register' not defined in model library!")
            }

            Py_DECREF(lib);
            Py_DECREF(reg_lib);
            Py_DECREF(module);
        } else {
            if (PyErr_Occurred()) PyErr_Print();
            FATAL_ERROR("No module!")
        }

        return handlers;
    }

    std::vector<ModelHandler *> PyErrorModelHandler::LoadFromLibrary(PyObject * lib)
    {
        std::vector<ModelHandler *> handlers;
        PyObject * models, * model;

        models = PyObject_CallMethod(lib, "GetModels", NULL);
        for (int i = 0, l = PyObject_Length(models); i < l; ++i) {
            model = PyObject_GetItem(models, PyInt_FromLong(i));
            if (model != NULL) {
                // FIXME: no need to incref model, already inc by getitem
                PyErrorModelHandler * handler = new PyErrorModelHandler(model);
                handlers.push_back(handler);
            } else {
                if (PyErr_Occurred()) PyErr_Print();
                FATAL_ERROR("No models!")
            }
        }
        Py_XDECREF(models);

        return handlers;
    }

    PyErrorModelHandler::PyErrorModelHandler() : ModelHandler(LANG_PY, MODEL_ERROR) {}

    PyErrorModelHandler::PyErrorModelHandler(PyObject * _error_model) : ModelHandler(LANG_PY, MODEL_ERROR), error_model(_error_model)
    {
        PyObject * ret = PyObject_CallMethod(error_model, "Methods", NULL);
        PY_PTR_CHECK(ret, "Methods null!")
        methods = PyInt_AsUnsignedLongLongMask(ret);
        Py_DECREF(ret);
    }

    PyErrorModelHandler::~PyErrorModelHandler() 
    {
        Py_DECREF(error_model);
    }

    std::string PyErrorModelHandler::modelName()
    {
        PyObject * name;
        name = PyObject_CallMethod(error_model, "GetName", NULL);
        PY_PTR_CHECK(name, "Name null!")
        std::string cname(PyString_AsString(name));
        Py_DECREF(name);

        return cname;
    }

    bool PyErrorModelHandler::invoke(ModelMethod method, ErrorContext *cxt, Log *log)
    {
        InitializePython();

        std::string func_name;
        switch (method)
        {
            case METHOD_SETUP:
                func_name = "Setup";
                break;
            case METHOD_CLEANUP:
                func_name = "Cleanup";
                break;
            case METHOD_PRE_INJECT:
                func_name = "PreInjectError";
                break;
            case METHOD_INJECT:
                func_name = "InjectError";
                break;
            default:
                FATAL_ERROR("No Error Model Method!")
                return false;
        }

        PyObject *config = Py_None;
        if (cxt->error_info.hasErrorConfig()) {
            config = PyLoadYaml(cxt->error_info.error_config);
            PY_PTR_CHECK(config, "Error loading config!")
        } else {
            // Have to increment Py_None
            Py_INCREF(Py_None);
        }

        swig_type_info * cxt_info = SWIG_TypeQuery("hamartia_api::ErrorContext*");
        PY_PTR_CHECK(cxt_info, "Bad cxt info")
        PyObject *py_cxt = SWIG_NewPointerObj(cxt, cxt_info, 0);
        PY_PTR_CHECK(py_cxt, "Error loading cxt!")

        PyObject *py_log = Py_None;
        if (log) {
            swig_type_info * log_info = SWIG_TypeQuery("hamartia_api::Log*");
            PY_PTR_CHECK(log_info, "Bad log info")
            py_log = SWIG_NewPointerObj(log, log_info, 0);
            PY_PTR_CHECK(py_log, "Error loading log!")
        } else {
            Py_INCREF(Py_None);
        }

        INFO_PRINT("Call Python, " << func_name)
        // config's reference count was 0, so we need to increment it with "O"
        PyObject *result = PyObject_CallMethod(error_model, (char*) func_name.c_str(), "NON", py_cxt, config, py_log);
        PY_PTR_CHECK(result, "Invoke error!")
        bool success = PyObject_IsTrue(result);
        INFO_PRINT("Python Done (" << success << ")")

        Py_DECREF(result);
        if (py_log == Py_None) Py_DECREF(py_log);

        ModelHandler::DefaultResponse(success, cxt);
        setMethodValidity(method, cxt);
        return success;
    }

    // Detector model handler
    std::vector<ModelHandler *> PyDetectorModelHandler::LoadFromModule(std::string path)
    {
        std::vector<ModelHandler *> handlers;

        InitializePython();

        std::string dir = utils::FileDirname(path);
        std::string module_str = utils::FileFilename(utils::FileBasename(path));
        PyObject * module_name, * module_path, * module, * reg_lib, * lib;

        module_path = PyString_FromString((char*) dir.c_str());
        PyList_Append(PySys_GetObject("path"), module_path);
        Py_DECREF(module_path);
       
        module_name = PyString_FromString((char*) module_str.c_str());
        module = PyImport_Import(module_name);
        Py_DECREF(module_name);

        if (module != NULL) {
            reg_lib = PyObject_GetAttrString(module, "register");
            lib = PyObject_CallObject(reg_lib, NULL);
            if (lib != NULL) {
                handlers = LoadFromLibrary(lib);
            } else {
                if (PyErr_Occurred()) PyErr_Print();
                FATAL_ERROR("'register' not defined in model library!")
            }

            Py_DECREF(lib);
            Py_DECREF(reg_lib);
            Py_DECREF(module);
        } else {
            if (PyErr_Occurred()) PyErr_Print();
            FATAL_ERROR("No module!")
        }

        return handlers;
    }

    std::vector<ModelHandler *> PyDetectorModelHandler::LoadFromLibrary(PyObject * lib)
    {
        std::vector<ModelHandler *> handlers;
        PyObject * models, * model;

        models = PyObject_CallMethod(lib, "GetModels", NULL);
        for (int i = 0, l = PyObject_Length(models); i < l; ++i) {
            model = PyObject_GetItem(models, PyInt_FromLong(i));
            if (model != NULL) {
                // FIXME: no need to incref model, already inc by getitem
                PyDetectorModelHandler * handler = new PyDetectorModelHandler(model);
                handlers.push_back(handler);
            } else {
                if (PyErr_Occurred()) PyErr_Print();
                FATAL_ERROR("No models!")
            }
        }
        Py_XDECREF(models);

        return handlers;
    }

    PyDetectorModelHandler::PyDetectorModelHandler() : ModelHandler(LANG_PY, MODEL_DETECTOR) {}

    PyDetectorModelHandler::PyDetectorModelHandler(PyObject * _detector_model) : ModelHandler(LANG_PY, MODEL_DETECTOR), detector_model(_detector_model)
    {
        PyObject * ret = PyObject_CallMethod(detector_model, "Methods", NULL);
        PY_PTR_CHECK(ret, "Methods null!")
        methods = PyInt_AsUnsignedLongLongMask(ret);
        Py_DECREF(ret);
    }

    PyDetectorModelHandler::~PyDetectorModelHandler()
    {
        delete detector_model;
    }

    std::string PyDetectorModelHandler::modelName()
    {
        PyObject * name;
        name = PyObject_CallMethod(detector_model, "GetName", NULL);
        PY_PTR_CHECK(name, "Name null!")
        std::string cname(PyString_AsString(name));
        Py_DECREF(name);

        return cname;
    }

    bool PyDetectorModelHandler::invoke(ModelMethod method, ErrorContext *cxt, Log *log)
    {
        InitializePython();

        std::string func_name;
        switch (method)
        {
            case METHOD_SETUP:
                func_name = "Setup";
                break;
            case METHOD_CLEANUP:
                func_name = "Cleanup";
                break;
            case METHOD_CHECK_ERROR:
                func_name = "CheckError";
                break;
            default:
                FATAL_ERROR("No Detector Model Method!")
                return false;
        }

        PyObject *config = Py_None;
        if (cxt->error_info.hasDetectorConfig()) {
            config = PyLoadYaml(cxt->error_info.detector_config);
            PY_PTR_CHECK(config, "Error loading config!")
        } else {
            // Have to increment Py_None
            Py_INCREF(Py_None);
        }

        swig_type_info * cxt_info = SWIG_TypeQuery("hamartia_api::ErrorContext*");
        PY_PTR_CHECK(cxt_info, "Bad cxt info")
        PyObject *py_cxt = SWIG_NewPointerObj(cxt, cxt_info, 0);
        PY_PTR_CHECK(py_cxt, "Error loading cxt!")

        PyObject *py_log = Py_None;
        if (log) {
            swig_type_info * log_info = SWIG_TypeQuery("hamartia_api::Log*");
            PY_PTR_CHECK(log_info, "Bad log info")
            py_log = SWIG_NewPointerObj(log, log_info, 0);
            PY_PTR_CHECK(py_log, "Error loading log!")
        } else {
            Py_INCREF(Py_None);
        }

        INFO_PRINT("Call Python, " << func_name)
        PyObject *result = PyObject_CallMethod(detector_model, (char*) func_name.c_str(), "NON", py_cxt, config, py_log);
        PY_PTR_CHECK(result, "Invoke error!")
        INFO_PRINT("Python Done")
        bool success = PyObject_IsTrue(result);

        Py_DECREF(result);
        if (py_log == Py_None) Py_DECREF(py_log);

        ModelHandler::DefaultResponse(success, cxt);
        setMethodValidity(method, cxt);
        return success;
    }
}

/** @} */
