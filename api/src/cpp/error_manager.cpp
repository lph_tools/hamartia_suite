// Copyright 2015, The University of Texas at Austin 
// All rights reserved.
// 
// THIS FILE IS PART OF THE HAMARTIA FAULT/ERROR ANALYSIS SUITE
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met: 
// 
// 1. Redistributions of source code must retain the above copyright
// notice, this list of conditions and the following disclaimer. 
// 
// 2. Redistributions in binary form must reproduce the above copyright
// notice, this list of conditions and the following disclaimer in the
// documentation and/or other materials provided with the distribution. 
// 
// 3. Neither the name of the copyright holder nor the names of its
// contributors may be used to endorse or promote products derived from
// this software without specific prior written permission. 
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
// FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
// COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
// BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
// ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

/**
 * \file
 * \brief Error Manager
 *
 * - Helper functions for forwarding error modeling to python front-end
 * - Protobuf parsing/usage
 *
 *  \version 1.0.0
 *  \author Nicholas Kelly, University of Texas
 *
 *  \addtogroup error_manager
 *  @{
 */

// Include first for no warnings
#include <Python.h>

#include <hamartia_api/error_manager.h>
#include <hamartia_api/debug.h>
#include <hamartia_api/interface.h>
#include <hamartia_api/error_model.h>
#include <hamartia_api/detector_model.h>
#include <hamartia_api/error_models/basic_models.h>
#include <hamartia_api/detector_models/basic_models.h>
#include <hamartia_api/utils/file.h>
#include <hamartia_api/handler.h>
#include <hamartia_api/handlers/py_handler.h>
#include <hamartia_api/handlers/cpp_handler.h>

// LIB INCLUDES
#include <cstdio>
#include <cstdlib>
#include <getopt.h>
#include <iostream>
#include <fstream>
#include <sstream>

namespace hamartia_api 
{
    namespace error_manager 
    {

        static bool startup = false;
        static std::string output_dir(".");
        static std::map<std::string, ModelHandler *> model_handlers;

        // FIXME: arguments?
        void Init(int argc, char ** argv)
        {
            // Command line arguments
            if (argc > 0) {
                // Options
                struct option options[] = {
                    {"help",      no_argument,       0, 'h'},
                    {"startup",   no_argument,       0, 's'},
                    {"errorlib",  required_argument, 0, 'e'},
                    {"detectlib", required_argument, 0, 'd'},
                    {"include",   required_argument, 0, 'i'},
                    {"output",    required_argument, 0, 'o'},
                    {0,           0,                 0, 0}
                };
                // Parse args
                int c; 
                while (1) {
                    int oidx = 0;
                    c = getopt_long(argc, argv, "hse:d:i:o:", options, &oidx);
                    // End of options
                    if (c == -1) break;
                    // Parse
                    switch (c)
                    {
                        case 'h':
                            _Help();
                            break;
                        case 's':
                            startup = true;
                            break;
                        case 'e':
                            // FIXME: or remove
                            //LoadErrorModelLibrary(std::string(optarg));
                            break;
                        case 'i':
                            // FIXME: how to implement?
                            break;
                        case 'o':
                            output_dir = optarg;
                            break;
                        default:
                            _Help("Invalid argument!");
                    }
                }
            }

            // Initialize error manager paths
            if (std::getenv("HAMARTIA_DIR") == NULL) {
                FATAL_ERROR("HAMARTIA_DIR not defined in env!")
            }

            // Default models
            ErrorModelLibrary * basic_models = new BasicErrorModels();
            RegisterHandlers(CppErrorModelHandler::LoadFromLibrary(basic_models));
            delete basic_models;

            DetectorModelLibrary * basic_detector_models = new BasicDetectorModels();
            RegisterHandlers(CppDetectorModelHandler::LoadFromLibrary(basic_detector_models));
            delete basic_detector_models;

            // Load all models
            LoadAllErrorModelsFromEnv();
            LoadAllDetectorModelsFromEnv();
        }

        void _Help(std::string msg)
        {
            // Message
            if (!msg.empty()) std::cout << msg << std::endl;

            // Help message
            std::cout << "Usage: error_manager [OPTIONS]" << std::endl;
            std::cout << "C++ Error manager for Hamartia API, executes C++ models from stdin" << std::endl;
            std::cout << "\nEnvironment variables:" << std::endl;
            std::cout << "\tHAMARTIA_DIR\t\tDirectory of API (required)" << std::endl;
            std::cout << "\tHAMARTIA_ERROR_PATH\tPaths of error models to load (optional)" << std::endl;
            std::cout << "\tHAMARTIA_DETECTOR_PATH\tPaths of detector models to load (optional)" << std::endl;
            std::cout << "\nOptional arguments:" << std::endl;
            std::cout << "\t-h, --help\tPrint help" << std::endl;
            std::cout << "\t-s, --startup\tPerform startup tasks for models" << std::endl;
            std::cout << "\t-e, --errorlib\tPath of error model library to load" << std::endl;
            std::cout << "\t-d, --detectlib\tPath of detector model library to load" << std::endl;
            std::cout << "\t-o, --output\tOutput directory" << std::endl;

            exit(EXIT_FAILURE);
        }

        // Model loading
        void LoadAllErrorModelsFromEnv()
        {
            if (std::getenv("HAMARTIA_ERROR_PATH") != NULL) {
                std::stringstream paths(std::getenv("HAMARTIA_ERROR_PATH"));
                while (!paths.eof()) {
                    std::string path;
                    std::getline(paths, path, ':');
                    std::string ext = utils::FileExt(path);
                    std::vector<std::string> libs;
                    if (ext.empty()) {
                        std::vector<std::string> so_libs = utils::FileGlob(path + "/*.so");
                        libs.insert(libs.end(), so_libs.begin(), so_libs.end());
                        std::vector<std::string> py_libs = utils::FileGlob(path + "/*.py");
                        libs.insert(libs.end(), py_libs.begin(), py_libs.end());
                    } else {
                        libs.push_back(path);
                    }
                    for (std::vector<std::string>::iterator lib = libs.begin(); lib != libs.end(); ++lib) {
                        std::string lib_ext = utils::FileExt(*lib);
                        if (ext == "so") {
                            RegisterHandlers(CppErrorModelHandler::LoadFromSharedLibrary(*lib));
                        } else if (ext == "py") {
                            RegisterHandlers(PyErrorModelHandler::LoadFromModule(*lib));
                        }
                    }
                }
            }
        }

        void LoadAllDetectorModelsFromEnv()
        {
            if (std::getenv("HAMARTIA_DETECTOR_PATH") != NULL) {
                std::stringstream paths(std::getenv("HAMARTIA_DETECTOR_PATH"));
                while (!paths.eof()) {
                    std::string path;
                    std::getline(paths, path, ':');
                    std::string ext = utils::FileExt(path);
                    std::vector<std::string> libs;
                    if (ext.empty()) {
                        std::vector<std::string> so_libs = utils::FileGlob(path + "/*.so");
                        libs.insert(libs.end(), so_libs.begin(), so_libs.end());
                        std::vector<std::string> py_libs = utils::FileGlob(path + "/*.py");
                        libs.insert(libs.end(), py_libs.begin(), py_libs.end());
                    } else {
                        libs.push_back(path);
                    }
                    for (std::vector<std::string>::iterator lib = libs.begin(); lib != libs.end(); ++lib) {
                        std::string lib_ext = utils::FileExt(*lib);
                        if (ext == "so") {
                            RegisterHandlers(CppDetectorModelHandler::LoadFromSharedLibrary(*lib));
                        } else if (ext == "py") {
                            RegisterHandlers(PyDetectorModelHandler::LoadFromModule(*lib));
                        }
                    }
                }
            }
        }

        void RegisterHandlers(std::vector<ModelHandler *> handlers)
        {
            for (std::vector<ModelHandler *>::iterator hit = handlers.begin(); hit != handlers.end(); ++hit) {
                INFO_PRINT("Registering " << (*hit)->modelName() << "...")
                model_handlers[(*hit)->modelName()] = *hit;
            }
        }

        bool Setup(ErrorContext *cxt, Log *log)
        {
            bool success = true;
            // Setup error model
            if (cxt->error_info.hasErrorModel()) {
                success = RunModel(MODEL_ERROR, METHOD_SETUP, cxt, log);
            }
            // Setup detector model
            if (cxt->error_info.hasDetectorModel()) {
                success = RunModel(MODEL_DETECTOR, METHOD_SETUP, cxt, log);
            }

            return success;
        }

        bool Cleanup(ErrorContext *cxt, Log *log)
        {
            bool success = true;
            // Cleanup error model
            if (cxt->error_info.hasErrorModel()) {
                success = RunModel(MODEL_ERROR, METHOD_CLEANUP, cxt, log);
            }
            // Cleanup detector model
            if (cxt->error_info.hasDetectorModel()) {
                success = RunModel(MODEL_DETECTOR, METHOD_CLEANUP, cxt, log);
            }

            return success;
        }

        bool PreInjectError(ErrorContext *cxt, ErrorContext *inj_cxt, Log *log)
        {
            bool success = true;
            success = RunModel(MODEL_ERROR, METHOD_PRE_INJECT, inj_cxt, log);

            return success;
        }

        bool InjectError(ErrorContext *inj_cxt, Log *log)
        {
            bool success = true;
            success = RunModel(MODEL_ERROR, METHOD_INJECT, inj_cxt, log);

            return success;
        }

        bool CheckError(ErrorContext *cxt, ErrorContext *inj_cxt, bool &unmasked, bool &undetected, Log *log) {
            // Check if unmasked
            unmasked = DetectorModel::VerifyError(cxt, inj_cxt, log);

            // Check if not detected
            undetected = true;
            if (cxt->error_info.hasDetectorModel()) {
                if (!RunModel(MODEL_DETECTOR, METHOD_CHECK_ERROR, inj_cxt, log)) {
                    FATAL_ERROR("Detector Model Error")
                }
                undetected = (inj_cxt->error_info.response == RESP_SUCCESS);
            }

            return (unmasked && undetected);
        }

        bool RunModel(ModelType model, ModelMethod method, ErrorContext *cxt, Log *log)
        {
            // Clear
            cxt->error_info.clearResponse();
            // Run model
            ModelHandler * handler;
            std::string model_name = GetModelName(model, cxt);
            handler = GetHandler(model_name);
            if (handler == NULL) {
                FATAL_ERROR("Model '" << model_name << "' not found!")
            }

            return handler->invoke(method, cxt, log);
        }

        std::string GetModelName(ModelType model, ErrorContext *cxt)
        {
            std::string model_name;
            switch (model) {
                case MODEL_ERROR:
                    model_name = cxt->error_info.error_model;
                    break;
                case MODEL_DETECTOR:
                    model_name = cxt->error_info.detector_model;
                    break;
                default:
                    FATAL_ERROR("Invalid model type!")
            }

            return model_name;
        }

        ModelHandler * GetHandler(std::string name)
        {
            if (model_handlers.find(name) != model_handlers.end()) {
                return model_handlers[name];
            }
            return NULL;
        }

        bool ModelMethodValid(ModelType model, ModelMethod method, ErrorContext *cxt)
        {
            std::string model_name = GetModelName(model, cxt);
            ModelHandler * handler = GetHandler(model_name);
            if (handler) {
                return handler->methodValid(method);
            } else {
                FATAL_ERROR("Model '" << model_name << "' not found!")
            }
            return false;
        }
    }
}

/** @} */
