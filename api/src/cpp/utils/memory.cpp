// Copyright 2015, The University of Texas at Austin 
// All rights reserved.
// 
// THIS FILE IS PART OF THE HAMARTIA FAULT/ERROR ANALYSIS SUITE
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met: 
// 
// 1. Redistributions of source code must retain the above copyright
// notice, this list of conditions and the following disclaimer. 
// 
// 2. Redistributions in binary form must reproduce the above copyright
// notice, this list of conditions and the following disclaimer in the
// documentation and/or other materials provided with the distribution. 
// 
// 3. Neither the name of the copyright holder nor the names of its
// contributors may be used to endorse or promote products derived from
// this software without specific prior written permission. 
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
// FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
// COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
// BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
// ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

/*
 *  \file
 *  \brief Memory utilities
 *  \version 1.1.0
 *  \author Nicholas Kelly, University of Texas
 *  \author Chun-Kai Chang, University of Texas
 *
 *  \addtogroup utils
 *  @{
 */

#include <hamartia_api/debug.h>
#include <hamartia_api/interface.h>
#include <hamartia_api/utils/memory.h>

#include <unistd.h>
#include <stdio.h>
#include <fcntl.h>

namespace hamartia_api {
    namespace memory {
        // entry of pagemap (https://www.kernel.org/doc/Documentation/vm/pagemap.txt)
        typedef struct pfn_info {
            uint64_t pfn : 55;
            bool soft_dirty : 1;
            bool exclusive : 1;
            uint8_t padding : 4;
            bool file_or_shared_anon : 1;
            bool swapped : 1;
            bool present : 1;
        } __attribute__((packed)) pfn_info_t;

        uint64_t VirtualToPhysical(uint64_t addr)
        {
            char filename[256];
            unsigned pid = getpid();
            snprintf(filename, 256,"/proc/%d/pagemap", pid);

            int pagemap_fd = open(filename, O_RDONLY); 
            if (pagemap_fd == -1 ) {
                return 0; // mapping unavailable
            }    

            size_t page_size = getpagesize();
            size_t page_index = addr / page_size;
            size_t page_offset = addr % page_size;
            // adjust file pointer to info of current page
            lseek(pagemap_fd, page_index * sizeof(pfn_info_t), SEEK_SET);

            // read the entry
            pfn_info_t entry;
            read(pagemap_fd, &entry, sizeof(pfn_info_t)); 

            if(entry.present && entry.pfn > 0) {
                return entry.pfn * page_size + page_offset;
            } else {
                return 0; // mapping unavailable 
            }
        }
    }
}

/** @} */
