// Copyright 2015, The University of Texas at Austin 
// All rights reserved.
// 
// THIS FILE IS PART OF THE HAMARTIA FAULT/ERROR ANALYSIS SUITE
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met: 
// 
// 1. Redistributions of source code must retain the above copyright
// notice, this list of conditions and the following disclaimer. 
// 
// 2. Redistributions in binary form must reproduce the above copyright
// notice, this list of conditions and the following disclaimer in the
// documentation and/or other materials provided with the distribution. 
// 
// 3. Neither the name of the copyright holder nor the names of its
// contributors may be used to endorse or promote products derived from
// this software without specific prior written permission. 
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
// FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
// COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
// BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
// ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

/**
 *  \file
 *  \brief File helpers
 *  \author Michael Sullivan, University of Texas
 *  \author Nicholas Kelly, University of Texas
 *
 *  \addtogroup utils
 *  @{
 */

#include <hamartia_api/utils/file.h>
#include <glob.h>

namespace hamartia_api
{
    namespace utils
    {
        std::string FileBasename(std::string path)
        {
            std::size_t last_slash = path.find_last_of("/\\");
            if (last_slash == std::string::npos) {
                return path;
            }
            return path.substr(last_slash+1);
        }

        std::string FileDirname(std::string path)
        {
            std::size_t last_slash = path.find_last_of("/\\");
            if (last_slash == std::string::npos) {
                return path;
            }
            return path.substr(0, last_slash);
        }

        std::string FileExt(std::string path)
        {
            std::size_t dot = path.find_last_of(".");
            if (dot == std::string::npos) {
                return "";
            }
            return path.substr(dot+1);
        }

        std::string FileFilename(std::string path)
        {
            std::size_t dot = path.find_last_of(".");
            if (dot == std::string::npos) {
                return "";
            }
            return path.substr(0, dot);
        }

        std::vector<std::string> FileGlob(std::string glob_path)
        {
            std::vector<std::string> files;
            glob_t glob_libs;
            glob(glob_path.c_str(), GLOB_TILDE, NULL, &glob_libs);
            for (unsigned int i = 0; i < glob_libs.gl_pathc; ++i) {
                files.push_back(std::string(glob_libs.gl_pathv[i]));
            }

            return files;
        }
    }
}

/** @} */
