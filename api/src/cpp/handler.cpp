// Copyright 2015, The University of Texas at Austin 
// All rights reserved.
// 
// THIS FILE IS PART OF THE HAMARTIA FAULT/ERROR ANALYSIS SUITE
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met: 
// 
// 1. Redistributions of source code must retain the above copyright
// notice, this list of conditions and the following disclaimer. 
// 
// 2. Redistributions in binary form must reproduce the above copyright
// notice, this list of conditions and the following disclaimer in the
// documentation and/or other materials provided with the distribution. 
// 
// 3. Neither the name of the copyright holder nor the names of its
// contributors may be used to endorse or promote products derived from
// this software without specific prior written permission. 
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
// FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
// COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
// BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
// ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

/**
 * \file
 * \brief Error Manager, responsible calling error/detector models and/or other injectors
 * \author Nicholas Kelly, University of Texas
 *
 * \warning Dynamic loading and executable not tested!
 *
 * \addtogroup error_manager
 * @{
 */

#include <hamartia_api/handler.h>

namespace hamartia_api
{
    ModelHandler::ModelHandler() {}
    ModelHandler::ModelHandler(ModelLanguage _lang, ModelType _type) : lang(_lang), type(_type), methods(0xFFFFFFFFFFFFFFFF) {}

    ModelHandler::~ModelHandler() {}

    bool ModelHandler::methodValid(ModelMethod method)
    {
        return ((1 << method) & methods);
    }

    void ModelHandler::setMethodValidity(ModelMethod method, ErrorContext *cxt)
    {
        if (cxt->error_info.hasResponse() && cxt->error_info.response == RESP_NO_METHOD) {
            methods &= ~(1 << method);
        }
    }

    void ModelHandler::DefaultResponse(bool success, ErrorContext *cxt)
    {
        if (!cxt->error_info.hasResponse()) {
            if (success) {
                cxt->error_info.successResponse();
            } else {
                cxt->error_info.errorResponse("Undefined Model Error");
            }
        }
    }
}

/** @} */
