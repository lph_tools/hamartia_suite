// Copyright 2015, The University of Texas at Austin 
// All rights reserved.
// 
// THIS FILE IS PART OF THE HAMARTIA FAULT/ERROR ANALYSIS SUITE
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met: 
// 
// 1. Redistributions of source code must retain the above copyright
// notice, this list of conditions and the following disclaimer. 
// 
// 2. Redistributions in binary form must reproduce the above copyright
// notice, this list of conditions and the following disclaimer in the
// documentation and/or other materials provided with the distribution. 
// 
// 3. Neither the name of the copyright holder nor the names of its
// contributors may be used to endorse or promote products derived from
// this software without specific prior written permission. 
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
// FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
// COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
// BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
// ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

/**
 *  \file
 *  \brief Error model definition
 *
 *  \version 1.0.0
 *  \author Nicholas Kelly, University of Texas
 *
 *  \addtogroup error_model_api
 *  @{
 */

#include <hamartia_api/error_model.h>
#include <hamartia_api/logging.h>
#include <hamartia_api/interface.h>
#include <hamartia_api/debug.h>

// Libs
#include <string>
#include <yaml-cpp/yaml.h>

namespace hamartia_api
{
    /* =====================================================================
     * Error Model
     * ===================================================================== */

    // Constructor
    ErrorModel::ErrorModel(std::string _name)
    {
        name = _name;
    }

    // Get name
    std::string ErrorModel::GetName()
    {
        return name;
    }

    // Set response message
    void ErrorModel::SetResponse(ErrorContext *cxt, Resp resp, std::string message)
    {
        cxt->error_info.setResponse(resp, message);
    }

    uint64_t ErrorModel::Methods()
    {
        return 0xFFFFFFFFFFFFFFFF;
    }

    bool ErrorModel::Setup(ErrorContext *cxt, YAML::Node *config, Log *log) 
    {
        // Unused
        return true;
    }

    bool ErrorModel::Setup(ErrorContext *cxt, PyObject *config, Log *log)
    {
        // Unused
        return true;
    }

    bool ErrorModel::Cleanup(ErrorContext *cxt, YAML::Node *config, Log *log)
    {
        // Unused
        return true;
    }

    bool ErrorModel::Cleanup(ErrorContext *cxt, PyObject *config, Log *log)
    {
        // Unused
        return true;
    }

    // Pre-Inject error (defaults to no error)
    bool ErrorModel::PreInjectError(ErrorContext *cxt, YAML::Node *config, Log *log)
    {
        // Unused
        INFO_PRINT("PreInject Unused!")
        //SetResponse(cxt, RESP_NO_METHOD);
        return true;
    }

    // Pre-Inject error (defaults to no error)
    bool ErrorModel::PreInjectError(ErrorContext *cxt, PyObject *config, Log *log)
    {
        // Unused
        INFO_PRINT("PreInject Unused!")
        SetResponse(cxt, RESP_NO_METHOD);
        return true;
    }

    // Inject error (defaults to no error)
    bool ErrorModel::InjectError(ErrorContext *cxt, YAML::Node *config, Log *log)
    {
        // Unused
        INFO_PRINT("Inject Unused!")
        SetResponse(cxt, RESP_NO_METHOD);
        return true;
    }

    // Inject error (defaults to no error)
    bool ErrorModel::InjectError(ErrorContext *cxt, PyObject *config, Log *log)
    {
        // Unused
        INFO_PRINT("Inject Unused!")
        SetResponse(cxt, RESP_NO_METHOD);
        return true;
    }

    /* =====================================================================
     * Error Model Library
     * ===================================================================== */

    void ErrorModelLibrary::AddModel(ErrorModel *model)
    {
        models.push_back(model);
    }

    std::vector<ErrorModel *> ErrorModelLibrary::GetModels()
    {
        return models;
    }
}

/** @} */
