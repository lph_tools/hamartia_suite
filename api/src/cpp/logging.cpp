// Copyright 2015, The University of Texas at Austin 
// All rights reserved.
// 
// THIS FILE IS PART OF THE HAMARTIA FAULT/ERROR ANALYSIS SUITE
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met: 
// 
// 1. Redistributions of source code must retain the above copyright
// notice, this list of conditions and the following disclaimer. 
// 
// 2. Redistributions in binary form must reproduce the above copyright
// notice, this list of conditions and the following disclaimer in the
// documentation and/or other materials provided with the distribution. 
// 
// 3. Neither the name of the copyright holder nor the names of its
// contributors may be used to endorse or promote products derived from
// this software without specific prior written permission. 
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
// FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
// COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
// BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
// ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

/*
 * \file
 * \brief Operand Helper
 *
 * - Creation of operands for protobufs
 *
 *  \version 1.0.0
 *  \author Nicholas Kelly, University of Texas
 *  \author Chun-Kai Chang, University of Texas
 *
 *  \addtogroup error_manager
 *  @{
 */

#include <hamartia_api/interface.h>
#include <hamartia_api/logging.h>

// Libs
#include <sstream>
#include <iostream>
#include <fstream>
#include <vector>
#include <yaml-cpp/yaml.h>

namespace hamartia_api 
{
    // Convert int to HEX string
    std::string Log::ToHex(uint64_t uint) {
        std::stringstream ss;
        ss << std::hex << uint;
        return ss.str();
    }

    // Construct a YAML node for an operand
    void Log::YAMLOperand(YAML::Emitter &e, const Operand &op)
    {
        e << YAML::BeginMap;
        e << YAML::Key << "type" << YAML::Value << OperandType_Name(op.type);
        e << YAML::Key << "data_type" << YAML::Value << DataType_Name(op.data_type);
        e << YAML::Key << "width" << YAML::Value << op.width;
        e << YAML::Key << "simd_width" << YAML::Value << (int) op.simd_width;
        e << YAML::Key << "element_size" << YAML::Value << (int) op.elementSize();

        // Value
        e << YAML::Key << "value" << YAML::Value << YAML::BeginSeq;
        for (std::vector<DataValue>::const_iterator dvi = op.value.begin(); dvi != op.value.end(); ++dvi) {
            if (op.data_type == DT_FLOAT) {
                e << YAML::Value << dvi->fp32();
            } else if (op.data_type == DT_DOUBLE) {
                e << YAML::Value << dvi->fp64();
            } else {
                e << YAML::Value << dvi->uint();
            }
        }
        e << YAML::EndSeq;

        if (op.hasReg()) {
            e << YAML::Key << "reg" << YAML::Value << op.reg;
        }

        if (op.hasAddress()) {
            e << YAML::Key << "address" << YAML::Value;
            YAMLAddress(e, op.address);
        }

        e << YAML::EndMap;
    }

    void Log::YAMLOperandDiff(YAML::Emitter &e, const Operand &op1, const Operand &op2)
    {
        e << YAML::BeginMap;
        if(op1.reg == "")
            e << YAML::Key << "name" << YAML::Value << OperandType_Name(op1.type);
        else
            e << YAML::Key << "name" << YAML::Value << op1.reg;
        e << YAML::Key << "diff" << YAML::Value << YAML::BeginSeq;
        for (uint32_t i = 0, l = op1.value.size(); i < l; ++i) {
            const DataValue & op1_dv = op1.value[i];
            const DataValue & op2_dv = op2.value[i];
            e << YAML::BeginMap;
            e << YAML::Key << "xor" << YAML::Value << ToHex(op1_dv.uint() ^ op2_dv.uint());
            // TODO: add float components?
            if (op1.data_type == DT_FLOAT) {
                e << YAML::Key << "sub" << YAML::Value << (op1_dv.fp32() - op2_dv.fp32());
            } else if (op1.data_type == DT_DOUBLE) {
                e << YAML::Key << "sub" << YAML::Value << (op1_dv.fp64() - op2_dv.fp64());
            } else {
                e << YAML::Key << "sub" << YAML::Value << (op1_dv.uint() - op2_dv.uint());
            }
            e << YAML::EndMap;
        }
        e << YAML::EndSeq;
        e << YAML::EndMap;
    }

    void Log::YAMLInstructionData(YAML::Emitter &e, const InstructionData &inst_data)
    {
        e << YAML::BeginMap;
        e << YAML::Key << "instruction" << YAML::Value;
        YAMLInstruction(e, inst_data.instruction);
        e << YAML::Key << "asm_line" << YAML::Value << inst_data.asm_line;
        e << YAML::Key << "asm_data" << YAML::Value << inst_data.asm_data;
        e << YAML::Key << "address" << YAML::Value;
        YAMLAddress(e, inst_data.address);
        e << YAML::Key << "data_type" << YAML::Value << DataType_Name(inst_data.data_type);
        e << YAML::Key << "simd_width" << YAML::Value << inst_data.simd_width;
        e << YAML::Key << "width" << YAML::Value << inst_data.width;
        e << YAML::EndMap;
    }

    void Log::YAMLInstruction(YAML::Emitter &e, const Instruction &inst)
    {
        e << YAML::BeginMap;
        e << YAML::Key << "opcode" << YAML::Value << ToHex(inst.opcode);
        e << YAML::Key << "operation" << YAML::Value << Operation_Name(inst.operation);
        e << YAML::Key << "packing" << YAML::Value << Packing_Name(inst.packing);
        e << YAML::Key << "condition" << YAML::Value << Condition_Name(inst.condition);
        e << YAML::EndMap;
    }

    void Log::YAMLAddress(YAML::Emitter &e, const Address &addr)
    {
        e << YAML::BeginMap;
        e << YAML::Key << "physical" << YAML::Value << ToHex(addr.phys);
        e << YAML::Key << "virtual" << YAML::Value << ToHex(addr.virt);
        e << YAML::EndMap;
    }

    void Log::YAMLInjectionStats(YAML::Emitter &e, const InjectionStats &stats)
    {
        e << YAML::BeginMap;
        e << YAML::Key << "iterations" << YAML::Value << stats.iterations;
        e << YAML::Key << "tp_detected" << YAML::Value << stats.tp_detected;
        e << YAML::Key << "fp_detected" << YAML::Value << stats.fp_detected;
        e << YAML::Key << "tn_detected" << YAML::Value << stats.tn_detected;
        e << YAML::Key << "fn_detected" << YAML::Value << stats.fn_detected;
        e << YAML::Key << "total_combinations" << YAML::Value << stats.total_combinations;
        e << YAML::EndMap;
    }

    void Log::YAMLMap(YAML::Emitter &e, const std::map<std::string, std::string> &map)
    {
        e << YAML::BeginMap;
        for (std::map<std::string, std::string>::const_iterator it = map.begin();
             it != map.end(); ++it) {
            e << YAML::Key << it->first << YAML::Value << it->second;
        }
        e << YAML::EndMap;
    }

    void Log::DumpLocation(YAML::Emitter &e, uint32_t loc_idx)
    {
        YAMLMap(e, location[loc_idx]);
    }    

    void Log::DumpMisc(YAML::Emitter &e)
    {
        e << YAML::BeginMap;
        for (std::map<std::string, std::map<std::string, std::string> >::iterator mit = misc.begin();
                mit != misc.end(); ++mit) {

            e << YAML::Key << mit->first << YAML::Value;
            YAMLMap(e, mit->second);
        }
        e << YAML::EndMap;
    }

    // Emit the YAML log to an output stream (file)
    bool Log::Emit(std::ofstream &log_file)
    {
        // Construct YAML log
        YAML::Emitter yaml_out;
        yaml_out << YAML::BeginMap;

        // Models
        yaml_out << YAML::Key << "models" << YAML::Value << YAML::BeginMap;
        yaml_out << YAML::Key << "error" << YAML::Value << run_emodel;
        yaml_out << YAML::Key << "detector" << YAML::Value << run_dmodel;
        yaml_out << YAML::EndMap;

        // InjectionStats
        yaml_out << YAML::Key << "statistics" << YAML::Value;
        YAMLInjectionStats(yaml_out, statistics);

        // Injected operation
        yaml_out << YAML::Key << "instruction" << YAML::Value;
        YAMLInstructionData(yaml_out, instruction_data[0]);

        // Location
        yaml_out << YAML::Key << "location" << YAML::Value;
        DumpLocation(yaml_out);

        // Misc
        yaml_out << YAML::Key << "misc" << YAML::Value;
        DumpMisc(yaml_out);

        // Old (clean) operands
        yaml_out << YAML::Key << "initial_operands" << YAML::Value << YAML::BeginMap;
        // Inputs
        yaml_out << YAML::Key << "inputs" << YAML::Value << YAML::BeginSeq;
            for (std::vector<Operand>::iterator opi = instruction_data[0].inputs.begin(); 
                     opi != instruction_data[0].inputs.end(); ++opi) {
            YAMLOperand(yaml_out, *opi);
        }
        yaml_out << YAML::EndSeq;

        // Outputs
        yaml_out << YAML::Key << "outputs" << YAML::Value << YAML::BeginSeq;
            for (std::vector<Operand>::iterator opi = instruction_data[0].outputs.begin(); 
                     opi != instruction_data[0].outputs.end(); ++opi) {
            YAMLOperand(yaml_out, *opi);
        }
        yaml_out << YAML::EndSeq;
        yaml_out << YAML::EndMap;

        // New (injected) operands
        yaml_out << YAML::Key << "injected_operands" << YAML::Value << YAML::BeginMap;
        // Inputs
        yaml_out << YAML::Key << "inputs" << YAML::Value << YAML::BeginSeq;
            for (std::vector<Operand>::iterator opi = injected_inputs.begin();
                     opi != injected_inputs.end(); ++opi) {
            YAMLOperand(yaml_out, *opi);
        }
        yaml_out << YAML::EndSeq;

        // Outputs
        yaml_out << YAML::Key << "outputs" << YAML::Value << YAML::BeginSeq;
            for (std::vector<Operand>::iterator opi = injected_outputs.begin();
                     opi != injected_outputs.end(); ++opi) {
            YAMLOperand(yaml_out, *opi);
        }
        yaml_out << YAML::EndSeq;
        yaml_out << YAML::EndMap;

        // Differences between clean and injected operands (outputs)
        yaml_out << YAML::Key << "difference" << YAML::Value << YAML::BeginMap;
        // Inputs
        yaml_out << YAML::Key << "inputs" << YAML::Value << YAML::BeginSeq;
        for (uint8_t  i = 0; i < injected_inputs.size(); ++i) {
            const Operand &i_op = instruction_data[0].inputs[i];
            const Operand &e_op = injected_inputs[i];
            YAMLOperandDiff(yaml_out, i_op, e_op);
        }
        yaml_out << YAML::EndSeq;

        // Outputs
        yaml_out << YAML::Key << "outputs" << YAML::Value << YAML::BeginSeq;
        for (uint8_t i = 0; i < injected_outputs.size(); ++i) {
            const Operand &i_op = instruction_data[0].outputs[i];
            const Operand &e_op = injected_outputs[i];
            YAMLOperandDiff(yaml_out, i_op, e_op);
        }
        yaml_out << YAML::EndSeq;
        yaml_out << YAML::EndMap;

        // New (recorded) operands
        yaml_out << YAML::Key << "recorded_injections" << YAML::Value << YAML::BeginSeq;

        uint32_t injIdx = 1;
        for (std::vector< std::vector<Operand> >::iterator ri = recorded_inputs.begin(), ro = recorded_outputs.begin();
             ri != recorded_inputs.end() && ro != recorded_outputs.end(); ++ri, ++ro, ++injIdx) {

            yaml_out << YAML::BeginMap;
            // Injected operation
            yaml_out << YAML::Key << "instruction" << YAML::Value;
            YAMLInstructionData(yaml_out, instruction_data[injIdx]);
            
            // Location
            yaml_out << YAML::Key << "location" << YAML::Value;
            DumpLocation(yaml_out, injIdx);
            
            // Old (clean) operands
            yaml_out << YAML::Key << "initial_operands" << YAML::Value << YAML::BeginMap;
            // Inputs
            yaml_out << YAML::Key << "inputs" << YAML::Value << YAML::BeginSeq;
            for (std::vector<Operand>::iterator opi = instruction_data[injIdx].inputs.begin(); 
                      opi != instruction_data[injIdx].inputs.end(); ++opi) {
                YAMLOperand(yaml_out, *opi);
            }
            yaml_out << YAML::EndSeq;
            
            // Outputs
            yaml_out << YAML::Key << "outputs" << YAML::Value << YAML::BeginSeq;
            for (std::vector<Operand>::iterator opi = instruction_data[injIdx].outputs.begin(); 
                      opi != instruction_data[injIdx].outputs.end(); ++opi) {
                YAMLOperand(yaml_out, *opi);
            }
            yaml_out << YAML::EndSeq;
            yaml_out << YAML::EndMap;


            // Inputs
            yaml_out << YAML::Key << "inputs" << YAML::Value << YAML::BeginSeq;
            for (uint8_t i = 0; i < ri->size(); ++i) {
                const Operand &op = (*ri)[i];
                YAMLOperand(yaml_out, op);
            }
            yaml_out << YAML::EndSeq;

            // Outputs
            yaml_out << YAML::Key << "outputs" << YAML::Value << YAML::BeginSeq;
            for (uint8_t i = 0; i < ro->size(); ++i) {
                const Operand &op = (*ro)[i];
                YAMLOperand(yaml_out, op);
            }
            yaml_out << YAML::EndSeq;

            // Differences between clean and recorded operands (outputs)
            yaml_out << YAML::Key << "difference" << YAML::Value << YAML::BeginMap;
            // Inputs
            yaml_out << YAML::Key << "inputs" << YAML::Value << YAML::BeginSeq;
            for (uint8_t  i = 0; i < ri->size(); ++i) {
                const Operand &i_op = instruction_data[injIdx].inputs[i];
                const Operand &e_op = (*ri)[i];
                YAMLOperandDiff(yaml_out, i_op, e_op);
            }
            yaml_out << YAML::EndSeq;

            // Outputs
            yaml_out << YAML::Key << "outputs" << YAML::Value << YAML::BeginSeq;
            for (uint8_t i = 0; i < ro->size(); ++i) {
                const Operand &i_op = instruction_data[injIdx].outputs[i];
                const Operand &e_op = (*ro)[i];
                YAMLOperandDiff(yaml_out, i_op, e_op);
            }
            yaml_out << YAML::EndSeq;
            yaml_out << YAML::EndMap;
            yaml_out << YAML::EndMap;
        }
        yaml_out << YAML::EndSeq;

        // Write out YAML log
        if (log_file.is_open()) {
            log_file << yaml_out.c_str() << std::endl;
            log_file.close();

            return true;
        }

        // Error
        return false;
    }

    void Log::Clear()
    {
        // Clear/reset all members
        run_emodel = run_dmodel = "";
        statistics = InjectionStats();
        instruction_data.clear();
        location.clear();
        injected_inputs.clear();
        injected_outputs.clear();
        misc.clear();
    }

}

/** @} */
