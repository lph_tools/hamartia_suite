// Copyright 2015, The University of Texas at Austin 
// All rights reserved.
// 
// THIS FILE IS PART OF THE HAMARTIA FAULT/ERROR ANALYSIS SUITE
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met: 
// 
// 1. Redistributions of source code must retain the above copyright
// notice, this list of conditions and the following disclaimer. 
// 
// 2. Redistributions in binary form must reproduce the above copyright
// notice, this list of conditions and the following disclaimer in the
// documentation and/or other materials provided with the distribution. 
// 
// 3. Neither the name of the copyright holder nor the names of its
// contributors may be used to endorse or promote products derived from
// this software without specific prior written permission. 
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
// FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
// COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
// BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
// ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

/**
 *  \file
 *  \brief Detector Model
 *
 *  \version 1.0.0
 *  \author Nicholas Kelly, University of Texas
 *
 *  \addtogroup detector_model
 *  @{
 */

#include <hamartia_api/detector_model.h>
#include <hamartia_api/logging.h>
#include <hamartia_api/interface.h>
#include <hamartia_api/debug.h>

// Libs
#include <string>
#include <yaml-cpp/yaml.h>

namespace hamartia_api
{
    /* =====================================================================
     * Detector Model
     * ===================================================================== */

    // Constructor
    DetectorModel::DetectorModel(std::string _name)
    {
        name = _name;
    }

    // Get name
    std::string DetectorModel::GetName()
    {
        return name;
    }

    bool DetectorModel::VerifyError(ErrorContext *cxt, ErrorContext *inj_cxt, Log *log)
    {
        // FIXME: check width?
        for (uint32_t i = 0, l = cxt->instruction_data.outputs.size(); i < l; ++i) {
            Operand op = cxt->instruction_data.outputs[i];
            Operand inj_op = inj_cxt->instruction_data.outputs[i];
            // FIXME: should never be the case... (issue with RFLAGS earlier?)
            if (op.value.size() == 0) continue;

            // Check equality
            if (!CheckEquality(op, inj_op)) {
                return true;
            }
        }

        return false;
    }

    bool DetectorModel::CheckEquality(Operand &op1, Operand &op2)
    {
        if (op1.type != op2.type) return false;
        if (op1.type == OT_REGISTER && op1.reg != op2.reg) return false;
        // FIXME: can both reg and address be populated?
        if (op1.type == OT_MEMORY && op1.address.virt != op2.address.virt) return false;
        for (uint32_t i = 0, l = op1.value.size(); i < l; ++i) {
            if (DataValue(op1.value[i]) != DataValue(op2.value[i])) return false;
        }

        return true;
    }

    // Set response message
    void DetectorModel::SetResponse(ErrorContext *cxt, Resp resp, std::string message)
    {
        cxt->error_info.setResponse(resp, message);
    }

    uint64_t DetectorModel::Methods()
    {
        return 0xFFFFFFFFFFFFFFFF;
    }

    bool DetectorModel::Setup(ErrorContext *cxt, YAML::Node *config, Log *log) 
    {
        // Unused
        return true;
    }

    bool DetectorModel::Setup(ErrorContext *cxt, PyObject *config, Log *log)
    {
        // Unused
        return true;
    }

    bool DetectorModel::Cleanup(ErrorContext *cxt, YAML::Node *config, Log *log)
    {
        // Unused
        return true;
    }

    bool DetectorModel::Cleanup(ErrorContext *cxt, PyObject *config, Log *log)
    {
        // Unused
        return true;
    }

    bool DetectorModel::CheckError(ErrorContext *cxt, YAML::Node *config, Log *log)
    {
        // Unused
        SetResponse(cxt, RESP_NO_METHOD);
        return true;
    }

    bool DetectorModel::CheckError(ErrorContext *cxt, PyObject *config, Log *log)
    {
        // Unused
        SetResponse(cxt, RESP_NO_METHOD);
        return true;
    }

    /* =====================================================================
     * Error Model Library
     * ===================================================================== */

    DetectorModelLibrary::DetectorModelLibrary()
    { 
        //RegisterModels();
    }

    void DetectorModelLibrary::AddModel(DetectorModel *model)
    {
        models.push_back(model);
    }

    std::vector<DetectorModel *> DetectorModelLibrary::GetModels()
    {
        return models;
    }

    DetectorModelLibrary::~DetectorModelLibrary()
    {
    }
}
/** @} */
