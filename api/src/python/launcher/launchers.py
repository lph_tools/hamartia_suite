# Copyright 2015, The University of Texas at Austin 
# All rights reserved.
# 
# THIS FILE IS PART OF THE HAMARTIA FAULT/ERROR ANALYSIS SUITE
# 
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met: 
# 
# 1. Redistributions of source code must retain the above copyright
# notice, this list of conditions and the following disclaimer. 
# 
# 2. Redistributions in binary form must reproduce the above copyright
# notice, this list of conditions and the following disclaimer in the
# documentation and/or other materials provided with the distribution. 
# 
# 3. Neither the name of the copyright holder nor the names of its
# contributors may be used to endorse or promote products derived from
# this software without specific prior written permission. 
# 
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
# FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
# COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
# INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
# BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
# ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

import os, sys, time, shlex, random
import multiprocessing
from concurrent.futures import ThreadPoolExecutor
from concurrent.futures import wait
from concurrent.futures import FIRST_COMPLETED

from abc import ABCMeta, abstractmethod
from instrument import Instrument
from target import Target
from mako.template import Template
from config import Config
import template_helper
from job import Job
from job import Task
import vfs

# TODO: put in task start/end/maintence methods

class Argument():
    def __init__(self, granularity, func, fargs, args, arg_type=None):
        self.granularity = granularity
        self.func = func
        self.last = None
        self.cache = {}

        # Infer type
        if not arg_type:
            types = [type(a) if type(a) not in [list, tuple] else type(a[0]) for a in args if type(a) is not str]
            if types:
                arg_type = types[0]
        self.args = args
        self.fargs = fargs
        self.arg_type = arg_type

    def gen(self, iter, mako_args, in_order=True):
        # Template subsitute
        args = list(self.args)
        if iter in self.cache:
            return self.cache[iter]
        if type(args) is not list and type(args) is not tuple:
            args = [args]
        for i, arg in enumerate(args):
            if type(arg) is str:
                try:
                    args[i] = Template(arg).render(**mako_args)
                except:
                    return None
            elif type(arg) is list or type(arg) is tuple:
                try:
                    args[i] = [Template(ia).render(**mako_args) if type(ia) is str else ia for ia in arg]
                except:
                    return None
            if self.arg_type:
                if type(arg) is list or type(arg) is tuple:
                    args[i] = map(self.arg_type, args[i])
                else:
                    args[i] = self.arg_type(args[i])

        # Function
        if self.func:
            args = self.func(*[iter] + list(self.fargs) + list(args))
        if type(args) is tuple or type(args) is list:
            args = args[0]
        if not in_order:
            self.cache[iter] = args

        return args

    def next(self, iter, mako_args):
        if iter % self.granularity == 0:
            self.last = self.gen(iter, mako_args)

        return self.last
                        

class Launcher():
    __metaclass__ = ABCMeta

    def __init__(self, lconfig, options):
        # Filesystem setup
        self.fs = None
        if 'fs' in lconfig:
            if lconfig['fs']['type'] == 'local':
                self.fs = vfs.LocalFS()
            elif lconfig['fs']['type'] == 'ssh':
                self.fs = vfs.SSHFS(**lconfig['fs']['args'])
        else:
            self.fs = vfs.LocalFS()

        # Setup
        self.config = lconfig
        self.options = options
        self.restored_runs = {'pre': {}, 'iter': {}, 'post': {}}
        self.failed_runs = {'pre': {}, 'iter': {}, 'post': {}}

        # Workspace setup
        dirs = ['log', 'pre', 'post', 'iter']
        self.dir = self.config.value('dir', options.dir, default='.')

        self.dirs = {}
        for d in dirs:
            self.dirs[d] = os.path.join(self.dir, d)
            try:
                self.fs.mkdir(self.dirs[d])
            except:
                print d,"already exists... restoring..."
                # FIXME: faster recovery
                if d in ('pre', 'post', 'iter'):
                    (dout, derr) = self.fs.execute('find ' + self.dirs[d] + ' -type f -name COMPLETE -o -name DONE -o -name TIMEOUT | sort -V')
                    for dl in dout.split('\n'):
                        if dl:
                            it = int(os.path.basename(os.path.dirname(dl)))
                            self.restore_task(d, it, os.path.dirname(dl), os.path.basename(dl).lower())

        # Job setup
        job = self.config.value('job', required=True)
        self.type = self.config.value('job:type', required=True)
        self.procs = self.config.value('job:procs', options.procs, default=multiprocessing.cpu_count())
        self.job_args = self.config.value('job:args')
        self.job_config = None
        if 'config' in job:
            with open(job['config'], 'r') as jf:
                self.job_config = Template(jf.read())

        # Over-provisioning mode
        self.overpvsn = self.config.value('overprovisioning', default=None)
        self.batch_size = None
        if self.overpvsn:
            self.batch_size = self.overpvsn.get('batch_size', 10)

        # Instrument
        instrument = self.config.value('instrument', required=True)
        self.pre_instrument = None
        if 'pre_trial' in instrument:
            self.pre_instrument = Instrument(instrument['pre_trial'], fs=self.fs)
        self.post_instrument = None
        if 'post_trial' in instrument:
            self.post_instrument = Instrument(instrument['post_trial'], fs=self.fs)
        trial_instrument = self.config.value('instrument:trial', required=True)
        self.instrument = Instrument(trial_instrument, fs=self.fs)

        # Target
        target = self.config.value('target', required=True)
        self.target = Target(target, fs=self.fs)

        # Construct experiment
        self.iter = self.config.value('iter', required=True)
        experiment = self.config.value('experiment', default=[])
        (self.total_iter, self.processed_args) = Launcher.process_arguments(self.iter, experiment)
        self.trials = self.total_iter / self.iter
        self.env_args = template_helper.to_mako(os.environ)

        # Progress
        self.cur_iter = 0
        self.cur_trial = 0


    def get_trial(self, iter):
        return min(iter, self.total_iter-1) / self.iter


    def trial_bounds(self, trial):
        return (trial * self.iter, (trial+1) * self.iter)


    def update_args(self, args, c_iter=None):
        for k, v in args.iteritems():
            if k in self.processed_args and v is not None:
                if c_iter is None: 
                    self.processed_args[k].last = v
                else:
                    self.processed_args[k].cache[c_iter] = v


    @classmethod
    def process_arguments(cls, iter, arg_groups):
        """
        Process arguments, create generation functions for varying/dynamic arguments

        Args:
            iter  (int): Initial iterations
            args (list): Experiment arguments

        Returns:
            int: New iterations (incorporates all combinations of varying arguments)
            list: List of generation functions (processed args)
        """

        vargs = {}
        dims = 1
        stages = ['gen', 'trial', 'iter']
        # Parse args and replace
        for stage in stages:
            for arg_group in arg_groups:
                gdim = 1
                for arg, val in arg_group.iteritems():
                    # Correct stage
                    if type(val) is list:
                        if stage != val[0]:
                            continue
                    elif stage != 'iter':
                        continue

                    granularity = 1 if stage == 'iter' else iter
                    varg = None
                    if type(val) is list:
                        (cmd, cargs) = val[1:]

                        if cmd == 'rand_range' and len(cargs) == 2 and stage != 'gen':
                            # Random range
                            varg = Argument(granularity, lambda i, s, e: random.randint(s, e), (), tuple(cargs), int)
                        elif cmd == 'rand_list' and stage != 'gen':
                            # Random list
                            varg = Argument(granularity, lambda i, s, e: random.randint(s, e), (), tuple(cargs))
                        elif cmd == 'list':
                            # List
                            dim = dims if stage == 'gen' else 1
                            varg = Argument(granularity, lambda i, d, m, l: l[(i / d) % m], (dim, len(cargs)), [tuple(cargs)])
                            if stage == 'gen':
                                gdim = max(len(cargs), gdim)
                        elif cmd == 'range' and len(cargs) == 2:
                            # Range
                            dim = dims if stage == 'gen' else 1
                            varg = Argument(granularity, lambda i, d, s, e: range(s, e)[(i / d) % (e-s)], (dim, cargs[0], cargs[1]), int)
                            if stage == 'gen':
                                gdim = max(cargs[1]-cargs[0], gdim)
                        else:
                            raise Exception("Invalid substitution")
                    else:
                        varg = Argument(granularity, None, (), val)

                    vargs[arg] = varg

                if stage == 'gen':
                    dims = dims * gdim

        return (iter * dims, vargs)


    @classmethod
    def argument_generator(cls, start, end, vargs, mako_args):
        """
        Argument expansion generator, take processed arguments and generate final arguments
        used for launching.

        Args:
            iter   (int): Maximum iterations (incorporating varying arg combinations)
            vargs (list): Processed args

        Returns:
            list: Final arguments
        """

        # Generate arguments
        for i in xrange(start, end):
            iargs = {}
            for karg, varg in vargs.iteritems():
                iargs[karg] = varg.next(i, mako_args)

            yield template_helper.to_mako(iargs)


    @classmethod
    def argument_expansion(cls, i, vargs, mako_args):
        """
        Argument expansion generator, take processed arguments and generate final arguments
        used for launching.

        Args:
            iter   (int): Maximum iterations (incorporating varying arg combinations)
            vargs (list): Processed args

        Returns:
            list: Final arguments
        """

        # Expand arguments
        iargs = {}
        for karg, varg in vargs.iteritems():
            iargs[karg] = varg.gen(i, mako_args, False)

        return template_helper.to_mako(iargs)


    @abstractmethod
    def restore_task(self, group, it, path, status):
        pass


    @abstractmethod
    def start(self):
        pass


    @abstractmethod
    def end(self):
        pass


    @abstractmethod
    def progress(self):
        pass


    @abstractmethod
    def quit(self, signum, frame):
        pass


    @abstractmethod
    def kill(self, signum, frame):
        pass


def LocalLaunchIter((launcher, iterl, iter_args, pre_args, timeout, pre_dur)):
    task = Task(launcher, iterl, iter_args, pre_args, dir=launcher.dirs['iter'], timeout=timeout, pre_dur=pre_dur)
    if not task.status:
        task.save_info('started')
    if task.status == 'started':
        launcher.update_args(task.info['args'])
    elif task.status in ['done', 'complete', 'timeout']:
        if task.status == 'timeout':
            task.save_info('done', duration=task.timeout)
        elif task.status == 'complete':
            dur = task.read_duration()
            task.save_info('done', duration=dur)
        launcher.update_args(task.info['args'])
        return

    (out, err, duration) = task.execute()
    task.save_output(out, err)
    task.save_info('done', duration=duration)


class LocalLauncher(Launcher):
    def __init__(self, lconfig, options):
        # Initialize
        super(LocalLauncher, self).__init__(lconfig, options)
        self.pool = None
        self.running_tasks = {}
        self.future_to_iter = {}
        self.future_to_core = {}
        self.pre_dur = 0

    def restore_task(self, group, it, path, status):
        pass


    def progress(self):
        done = len([t for t in self.running_tasks.values() if t.done()])
        sys.stdout.write("\033[2J\033[1;1H")
        iter_percent = (self.cur_iter + done) * 100 / self.total_iter
        trial_percent = self.cur_trial * 100 / self.trials
        sys.stdout.write("Iterations: %d / %d (%d%%), Trials: %d / %d (%d%%)\n" % (self.cur_iter + done, self.total_iter, iter_percent, self.cur_trial, self.trials, trial_percent))
        sys.stdout.write("Running:\n")
        for ti, tt in self.running_tasks.iteritems():
            if tt.running():
                sys.stdout.write(" - Iter " + str(ti) + "\n")
        sys.stdout.flush()


    def pre_trial(self, trial, mako_args):
        args = {}
        if self.pre_instrument:
            t_iter = trial * self.iter
            trial_args = Launcher.argument_expansion(t_iter, self.processed_args, mako_args)
            task = Task(self, t_iter, trial_args, instrument=self.pre_instrument, dir=self.dirs['pre'])
            if not task.status:
                task.save_info('started')
            if task.status == 'started':
                self.update_args(task.info['args'], t_iter)
            elif task.status in ['done', 'complete', 'timeout']:
                self.update_args(task.info['args'], t_iter)
                return template_helper.to_mako(task.info['pre'])

            (out, err, duration) = task.execute()
            self.pre_dur = duration
            args = task.parse_output(out)
            task.save_info('done', duration=duration, pre=dict(args))

        return template_helper.to_mako(args)


    def post_trial(self):
        args = {}
        if self.post_instrument:
            t_iter = trial * self.iter
            trial_args = Launcher.argument_expansion(t_iter, self.processed_args, mako_args)
            task = Task(self, t_iter, trial_args, instrument=self.post_instrument, dir=self.dirs['post'])
            if not task.status:
                task.save_info('started')
            if task.status == 'started':
                task.iter_args = template_helper.to_mako(task.info['args'])
            elif task.status in ['done', 'complete', 'timeout']:
                return 

            (out, err, duration) = task.execute()
            task.save_info('done', duration=duration)


    def start(self):
        # Init pool
        self.pool = ThreadPoolExecutor(max_workers=self.procs)

        # Launch
        for trial in xrange(self.trials):
            self.cur_trial = trial
            # Mako args
            mako_args = {
                'env': self.env_args
            }

            # Pre-trial
            pre_args = self.pre_trial(trial, mako_args)
            mako_args['pre'] =  pre_args

            # Trials
            if not self.overpvsn:
                (tstart, tend) = self.trial_bounds(trial)
                arg_gen = Launcher.argument_generator(tstart, tend, self.processed_args, mako_args)
                for (t_iter, args) in zip(xrange(tstart, tend), arg_gen):
                    self.running_tasks[t_iter] = self.pool.submit(LocalLaunchIter, (self, t_iter, args, pre_args, self.options.timeout, self.pre_dur))

                while [t for t in self.running_tasks.values() if not t.done()]:
                    self.progress()
                    time.sleep(1)
                self.cur_iter += len(self.running_tasks)
                self.running_tasks = {}
            else:
                (tstart, tend) = self.trial_bounds(trial)
                arg_gen = Launcher.argument_generator(tstart, tend, self.processed_args, mako_args)
                s, e = 1, pre_args['global_filtered_op_count']
                batches = {p : self.generate_batch(s, e) for p in range(self.procs)}
                # initial runs
                for (t_iter, args) in zip(xrange(tstart, tstart + self.procs), arg_gen):
                    # override injection points
                    t_core = t_iter % self.procs
                    my_batch = batches[t_core]
                    batch_dict = self.generate_batch_dict(my_batch)
                    args.__dict__.update(batch_dict)
                    # submit initial runs
                    future = self.pool.submit(LocalLaunchIter, (self, t_iter, args, pre_args, self.options.timeout, self.pre_dur))
                    self.register_task_mappings(future, t_iter, t_core)

                t_iter = self.procs
                not_done = [t for t in self.running_tasks.values() if not t.done()]
                while not_done:
                    self.progress()
                    done, not_done = wait(not_done, return_when=FIRST_COMPLETED)
                    for future in done:
                        i, c = self.future_to_iter[future], self.future_to_core[future]
                        p = self.get_next_point(i)
                        #print("Iter {} on Core {} is done! Next point is {}".format(i, c, p))

                        if t_iter < tend:
                            args = next(arg_gen)
                            # override injection points
                            my_batch = batches[c][p:]
                            if my_batch:
                                batches[c] = my_batch
                            else: # run out of points; generate a new batch of points
                                batches[c] = self.generate_batch(s, e)
                                my_batch = batches[c]
                            batch_dict = self.generate_batch_dict(my_batch)
                            args.__dict__.update(batch_dict)

                            fut = self.pool.submit(LocalLaunchIter, (self, t_iter, args, pre_args, self.options.timeout, self.pre_dur))
                            self.register_task_mappings(fut, t_iter, c)
                            not_done.add(fut)
                            t_iter += 1
                    
                self.cur_iter += len(self.running_tasks)
                self.running_tasks = {}
                self.future_to_iter = {}
                self.future_to_core = {}

            # Post-trial
            self.post_trial()


    def end(self):
        self.progress()
        print "\nDone!"
        pass


    def quit(self, signum, frame):
        print "Quitting..."
        if self.pool:
            self.pool.shutdown()
        sys.exit(1)


    def kill(self, signum, frame):
        print "Killing..."
        if self.pool:
            self.pool.shutdown(wait=False)
        sys.exit(1)

    def generate_batch(self, start, end):
        return sorted([random.randint(start, end) for x in range(self.batch_size)])

    def generate_batch_dict(self, batch):
        if len(batch) < self.batch_size:
            batch = batch + [0] * (self.batch_size - len(batch))
        return {'inst{}'.format(i+1) : x for i,x in enumerate(batch)}
    
    def register_task_mappings(self, future, iter, core):
        self.running_tasks[iter] = future
        self.future_to_iter[future] = iter
        self.future_to_core[future] = core

    def get_next_point(self, iter):
        masked_file = os.path.join(self.dirs['iter'], str(iter), 'masked.yaml')
        masked_data = self.fs.read_file(masked_file)
        if masked_data:
            masked = template_helper.decode_str(masked_data, 'yaml')
            return masked['next_point']
        else: # empty masked.yaml means error was injected to the 0-th point
            return 1

class ClusterLauncher(Launcher):
    QUEUE_CHECK = 30

    def __init__(self, lconfig, options):
        super(ClusterLauncher, self).__init__(lconfig, options)

        # Get config
        cluster_config = self.config.value('job:config', required=True)
        self.cluster_config = Config(cluster_config)
        # FIXME: no template?
        self.cluster_template = Template(self.cluster_config.templates[0])

        # Config
        self.cluster_name = self.cluster_config.value('name', required=True)
        self.launch_cmd = self.cluster_config.value('cmd:launch', required=True)
        self.queue_cmd = self.cluster_config.value('cmd:queue', default=None)
        if self.queue_cmd:
            self.queue_cmd = Template(self.queue_cmd).render(env=self.env_args)
        self.cancel_cmd = self.cluster_config.value('cmd:cancel', default=None)
        if self.cancel_cmd:
            self.cancel_cmd = Template(self.cancel_cmd).render(env=self.env_args)

        self.alloc = self.config.value('job:args:alloc', default=self.cluster_config.value('alloc', default=''))
        self.queue = self.config.value('job:args:queue', default=self.cluster_config.value('queue', default=''))
        self.startup = self.cluster_config.value('startup', default=None)
        self.args = self.cluster_config.value('args', default={})
        self.timeout = self.cluster_config.value('timeout', default=60)
        self.iter_per_job = self.procs
        self.in_serial = self.config.value('job:in_serial', default=None)
        self.max_runtime = self.cluster_config.value('max_runtime', default=None)
        self.max_jobs = self.cluster_config.value('max_jobs', default=1)
        if self.in_serial:
            self.iter_per_job *= self.in_serial
        elif self.timeout and self.max_runtime:
            self.iter_per_job *= self.timeout / self.max_runtime

        self.running_jobs = {}
        self.pre_runs = []
        self.pre_durs = []
        self.post_runs = []
        self.stage = 'pre'


    def restore_task(self, group, it, path, status):
        self.restored_runs[group][it] = status


    def progress(self):
        sys.stdout.write("\033[2J\033[1;1H")
        iter_percent = self.cur_iter * 100 / self.total_iter
        trial_percent = self.cur_trial * 100 / self.trials
        sys.stdout.write("Iterations: %d / %d (%d%%), Trials: %d / %d (%d%%), Stage: %s\n" % (self.cur_iter, self.total_iter, iter_percent, self.cur_trial, self.trials, trial_percent, self.stage))
        sys.stdout.write("Running/Waiting:\n")
        for ji, jj in self.running_jobs.iteritems():
            jpercent = sum([1 for t in jj.tasks if t.status in ('done', 'complete', 'timeout')]) * 100 / len(jj.tasks);
            sys.stdout.write(" - Job " + jj.name + ", " + jj.type + " " + str(jj.id) + " (" + str(jpercent) + "%)\n")
        sys.stdout.flush()

        
    def pre_trial(self, mako_args):
        tasks = []
        job = None
        tr = 0
        while len(tasks) < self.iter_per_job and tr < self.trials:
            t_iter = tr * self.iter
            tr += 1
            if tr < len(self.pre_runs) and t_iter not in self.failed_runs['pre']:
                continue

            trial_args = Launcher.argument_expansion(t_iter, self.processed_args, mako_args)
            task = None
            if t_iter in self.failed_runs['pre']:
                task = self.failed_runs['pre'][t_iter]
                del self.failed_runs['pre'][t_iter]
            else:
                task = Task(self, t_iter, trial_args, instrument=self.pre_instrument, dir=self.dirs['pre'])
                self.pre_runs.append(None)
                self.pre_durs.append(0)

            if not task.status:
                task.save_info('started')
            if task.status == 'started':
                self.update_args(task.info['args'], t_iter)
            elif task.status in ['done', 'complete', 'timeout']:
                if task.status == 'timeout':
                    task.save_info('timeout', duration=task.timeout, pre={})
                    timeout_file = os.path.join(task.dir, "TIMEOUT")
                    done_file = os.path.join(task.dir, "DONE")
                    self.fs.rename(timeout_file, done_file)
                    self.info['pre'] = {}
                elif task.status == 'complete':
                    args = task.parse_output()
                    dur = task.read_duration()
                    task.save_info('done', duration=dur, pre=dict(args))
                    complete_file = os.path.join(task.dir, "COMPLETE")
                    done_file = os.path.join(task.dir, "DONE")
                    self.fs.rename(complete_file, done_file)
                    self.info['pre'] = dict(args)
                self.update_args(task.info['args'], t_iter)
                self.pre_runs[-1] = template_helper.to_mako(task.info['pre'])
                self.pre_durs[-1] = task.info['duration']
                continue

            tasks.append(task)
        if tasks:
            job = Job(self, tr * self.iter, 'pre', tasks)
        return job


    def post_trial(self, mako_args):
        tasks = []
        job = None
        tr = 0
        while len(tasks) < self.iter_per_job and tr < self.trials:
            t_iter = tr * self.iter
            tr += 1
            if tr < len(self.post_runs) and t_iter not in self.failed_runs['post']:
                continue

            trial_args = Launcher.argument_expansion(t_iter, self.processed_args, mako_args)
            task = None
            if t_iter in self.failed_runs['post']:
                task = self.failed_runs['post'][t_iter]
                del self.failed_runs['post'][t_iter]
            else:
                task = Task(self, t_iter, trial_args, instrument=self.post_instrument, dir=self.dirs['post'])
                self.post_runs.append(None)

            if not task.status:
                task.save_info('started')
            if task.status == 'started':
                task.iter_args = template_helper.to_mako(task.info['args'])
            elif task.status in ['done', 'complete', 'timeout']:
                if task.status == 'timeout':
                    task.save_info('timeout', duration=task.timeout)
                    timeout_file = os.path.join(task.dir, "TIMEOUT")
                    done_file = os.path.join(task.dir, "DONE")
                    self.fs.rename(timeout_file, done_file)
                elif task.status == 'complete':
                    dur = task.read_duration()
                    task.save_info('done', duration=dur)
                    complete_file = os.path.join(task.dir, "COMPLETE")
                    done_file = os.path.join(task.dir, "DONE")
                    self.fs.rename(complete_file, done_file)
                continue

            tasks.append(task)
        if tasks:
            job = Job(self, trial * self.iter, 'post', tasks)
        return job


    def manage_jobs(self):
        # Check running
        try:
            (out, err) = self.fs.execute(shlex.split(self.queue_cmd))
        except Exception as e:
            raise Exception("Job queue issue (" + str(e) + ")")
        out = out + err

        # Remove completed
        for jname in self.running_jobs.keys():
            job = self.running_jobs[jname]
            if jname not in out:
                finished_iter = 0
                for t in job.tasks:
                    complete_file = os.path.join(t.dir, "COMPLETE")
                    timeout_file = os.path.join(t.dir, "TIMEOUT")
                    if self.fs.exists(timeout_file):
                        args = { 'duration': t.timeout }
                        if job.type == 'pre':
                            args['pre'] = {}
                            self.pre_runs[t.trial] = template_helper.to_mako({})
                            self.pre_durs[t.trial] = t.timeout
                        elif job.type == 'post':
                            # TODO
                            post_runs[t.trial] = True
                        elif job.type == 'iter':
                            finished_iter += 1
                        t.save_info('timeout', **args)
                        done_file = os.path.join(t.dir, "DONE")
                        self.fs.rename(timeout_file, done_file)
                    elif self.fs.exists(complete_file):
                        dur = t.read_duration()
                        args = { 'duration': dur }
                        if job.type == 'pre':
                            pre = t.parse_output()
                            pre['duration'] = dur
                            args['pre'] = dict(pre)
                            self.pre_runs[t.trial] = template_helper.to_mako(pre)
                            self.pre_durs[t.trial] = dur
                        elif job.type == 'post':
                            # TODO
                            post_runs[t.trial] = True
                        elif job.type == 'iter':
                            finished_iter += 1
                        t.save_info('done', **args)
                        done_file = os.path.join(t.dir, "DONE")
                        self.fs.rename(complete_file, done_file)
                    else:
                        self.failed_runs[job.type][t.iter] = t

                if job.type == 'iter':
                    self.cur_iter += finished_iter
                    self.cur_trial = self.cur_iter / self.iter

                del self.running_jobs[jname]
            else:
                for t in job.tasks:
                    complete_file = os.path.join(t.dir, "COMPLETE")
                    timeout_file = os.path.join(t.dir, "TIMEOUT")
                    if self.fs.exists(timeout_file):
                        t.status = 'timeout'
                    elif self.fs.exists(complete_file):
                        t.status = 'complete'


    def start(self):
        cur_iter = 0
        post_trial = 0

        # Mako args
        mako_args = {
            'env': self.env_args,
            'pre': template_helper.to_mako({})
        }

        # Pre
        if self.pre_instrument:
            while len(self.pre_runs) < self.trials or len(self.failed_runs['pre']) > 0:
                # Start new jobs
                for i in range(self.max_jobs-len(self.running_jobs)):
                    pre_job = self.pre_trial(mako_args)
                    if pre_job:
                        pre_job.gen_config()
                        pre_job.submit()
                        self.running_jobs[pre_job.name] = pre_job
                        self.progress()

                    if len(self.pre_runs) == self.trials and len(self.failed_runs['pre']) == 0:
                        break

                # Job management
                self.progress()
                self.manage_jobs()

                # Sleep
                if len(self.running_jobs):
                    time.sleep(ClusterLauncher.QUEUE_CHECK)

        self.stage = 'iter'
        self.progress()

        # Iter
        arg_gen = None
        while cur_iter < self.total_iter or len(self.running_jobs) or len(self.failed_runs['iter']) > 0:
            # Start new jobs
            for i in range(self.max_jobs-len(self.running_jobs)):
                min_trial = self.get_trial(cur_iter)
                max_trial = self.get_trial(cur_iter+self.iter_per_job-1)

                if not self.pre_instrument or not [1 for t in range(min_trial, max_trial+1) if self.pre_runs[t] is None]:
                    # Iterations
                    tasks = []

                    it = cur_iter
                    while len(tasks) < self.iter_per_job and (it < self.total_iter or len(self.failed_runs['iter']) > 0):
                        if cur_iter >= self.total_iter:
                            if self.failed_runs['iter']:
                                it = self.failed_runs['iter'].keys()[0]
                            else:
                                break

                        trial = self.get_trial(it)
                        if it % self.iter == 0:
                            (tstart, tend) = self.trial_bounds(trial)
                            if self.pre_instrument:
                                mako_args['pre'] = self.pre_runs[trial]
                            arg_gen = Launcher.argument_generator(tstart, tend, self.processed_args, mako_args)

                        pre_args = self.pre_runs[trial] if self.pre_instrument else template_helper.to_mako({})
                        pre_dur = self.pre_durs[trial] if self.pre_instrument else 0
                        task = None
                        if cur_iter >= self.total_iter:
                            task = self.failed_runs['iter'][it]
                            del self.failed_runs['iter'][it]
                        elif it not in self.restored_runs['iter'] or self.restored_runs['iter'][it] in ('complete', 'timeout') or it % self.iter == 0:
                            task = Task(self, it, arg_gen.next(), pre_args, dir=self.dirs['iter'], timeout=self.options.timeout, pre_dur=pre_dur)
                        else:
                            arg_gen.next()
                            it += 1
                            self.cur_iter += 1
                            cur_iter += 1
                            if it % 64 == 0:
                                self.progress()
                            continue

                        if not task.status:
                            task.save_info('started')
                        if task.status == 'started':
                            self.update_args(task.info['args'])
                        elif task.status in ['done', 'complete', 'timeout']:
                            if task.status == 'timeout':
                                task.save_info('timeout', duration=task.timeout)
                                timeout_file = os.path.join(task.dir, "TIMEOUT")
                                done_file = os.path.join(task.dir, "DONE")
                                self.fs.rename(timeout_file, done_file)
                            elif task.status == 'complete':
                                dur = task.read_duration()
                                task.save_info('done', duration=dur)
                                complete_file = os.path.join(task.dir, "COMPLETE")
                                done_file = os.path.join(task.dir, "DONE")
                                self.fs.rename(complete_file, done_file)
                            self.update_args(task.info['args'])
                            it += 1
                            self.cur_iter += 1
                            cur_iter += 1
                            if it % 64 == 0:
                                self.progress()
                            continue

                        tasks.append(task)
                        it += 1

                    if tasks:
                        iter_job = Job(self, cur_iter, 'iter', tasks)
                        iter_job.gen_config()
                        iter_job.submit()
                        self.running_jobs[iter_job.name] = iter_job

                        cur_iter += len(tasks)
                        self.progress()

            # Job management
            self.manage_jobs()

            # Sleep
            if len(self.running_jobs):
                self.progress()
                time.sleep(ClusterLauncher.QUEUE_CHECK)

        self.stage = 'post'
        self.progress()

        # Post
        if self.post_instrument:
            while len(self.post_runs) < self.trials or len(self.failed_runs['post']) > 0:
                # Start new jobs
                for i in range(self.max_jobs-len(self.running_jobs)):
                    post_job = self.post_trial(post_trial, mako_args)
                    post_job.gen_config()
                    post_job.submit()
                    self.running_jobs[post_job.name] = post_job
                    self.progress()
                    post_trial += 1

                # Job management
                self.progress()
                self.manage_jobs()

                # Sleep
                if len(self.running_jobs):
                    time.sleep(ClusterLauncher.QUEUE_CHECK)


    def end(self):
        self.progress()
        print "\nDone!"
        pass


    def quit(self, signum, frame):
        print "Quitting..."
        sys.exit(1)


    def kill(self, signum, frame):
        print "Killing..."
        self.fs.execute(shlex.split(self.cancel_cmd))
        sys.exit(1)
