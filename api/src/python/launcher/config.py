# Copyright 2015, The University of Texas at Austin 
# All rights reserved.
# 
# THIS FILE IS PART OF THE HAMARTIA FAULT/ERROR ANALYSIS SUITE
# 
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met: 
# 
# 1. Redistributions of source code must retain the above copyright
# notice, this list of conditions and the following disclaimer. 
# 
# 2. Redistributions in binary form must reproduce the above copyright
# notice, this list of conditions and the following disclaimer in the
# documentation and/or other materials provided with the distribution. 
# 
# 3. Neither the name of the copyright holder nor the names of its
# contributors may be used to endorse or promote products derived from
# this software without specific prior written permission. 
# 
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
# FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
# COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
# INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
# BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
# ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

import os, sys, template_helper
from mako.template import Template

class Config():
    def __init__(self, path, has_config=True, has_templates=True, file_type='yaml'):
        self.path = path
        self.has_config = has_config
        self.has_templates = has_templates

        data = Config.Parse(path, file_type)
        if has_config:
            self.config = data[0]
        if has_templates:
            self.templates = data[1:]
        else:
            self.templates = []


    def renderTemplates(self, data):
        data = []
        for t in self.templates:
            tmp = Template(t)
            mako_data = template_helper.to_mako(data)
            data.append(tmp.render(**mako_data))
        
        return data


    def __getitem__(self, key):
        if self.has_config:
            return self.config[key]
        return None


    def __iter__(self):
        config = self.config if self.has_config else {}
        for k in self.config.keys():
            yield k


    def iteritems(self):
        config = self.config if self.has_config else {}
        for k, v in self.config.iteritems():
            yield (k, v)


    def value(self, key, option=None, default=None, required=False):
        return Config.GetValue(self, key, option, default, required)


    @staticmethod
    def GetValue(data, key, option=None, default=None, required=False):
        keys = key.split(':')
        if option and option != default:
            return option
        d = data
        for k in keys:
            if k in d:
                d = d[k]
            elif required:
                raise Exception("'" + key + "' is required in " + self.path)
            else:
                return default
        return d


    @classmethod
    def Parse(cls, path, file_type):
        files = []
        data = ""
        with open(path, 'r') as pf:
            for i, l in enumerate(pf):
                if l[:3] == "---" and i > 0:
                    files.append(data)
                    data = ""
                else:
                    data += l
            files.append(data)

        config = template_helper.decode_str(files[0], file_type)
        if len(files) > 1:
            return [config] + files[1:]
        return [config]
