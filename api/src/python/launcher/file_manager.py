# Copyright 2015, The University of Texas at Austin 
# All rights reserved.
# 
# THIS FILE IS PART OF THE HAMARTIA FAULT/ERROR ANALYSIS SUITE
# 
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met: 
# 
# 1. Redistributions of source code must retain the above copyright
# notice, this list of conditions and the following disclaimer. 
# 
# 2. Redistributions in binary form must reproduce the above copyright
# notice, this list of conditions and the following disclaimer in the
# documentation and/or other materials provided with the distribution. 
# 
# 3. Neither the name of the copyright holder nor the names of its
# contributors may be used to endorse or promote products derived from
# this software without specific prior written permission. 
# 
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
# FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
# COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
# INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
# BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
# ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

import os, sys, hashlib
import vfs

class FileManager():
    def __init__(self, fs=vfs.LocalFS(), auto=True):
        self.fs = fs
        self.hashes = {}
        self.files = {}
        self.auto = auto


    def add_file(self, filename, text):
        txthash = hashlib.sha256(text).hexdigest()
        self.hashes[txthash] = filename
        self.inc(filename)


    def file_exists(self, text):
        txthash = hashlib.sha256(text).hexdigest()
        if txthash in self.hashes:
            return self.hashes[txthash]
        return None


    def dec(self, filename):
        self.files[filename] -= 1
        if self.auto and self.files[filename] == 0:
            self.fs.remove(filename)
            del self.files[filename]


    def inc(self, filename):
        if filename in self.files:
            self.files[filename] += 1
        else:
            self.files[filename] = 1


    def clean(self):
        for f, c in self.files.iteritems():
            if c <= 0:
                self.fs.remove(f)
                del self.files[f]
