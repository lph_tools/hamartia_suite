# Copyright 2015, The University of Texas at Austin 
# All rights reserved.
# 
# THIS FILE IS PART OF THE HAMARTIA FAULT/ERROR ANALYSIS SUITE
# 
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met: 
# 
# 1. Redistributions of source code must retain the above copyright
# notice, this list of conditions and the following disclaimer. 
# 
# 2. Redistributions in binary form must reproduce the above copyright
# notice, this list of conditions and the following disclaimer in the
# documentation and/or other materials provided with the distribution. 
# 
# 3. Neither the name of the copyright holder nor the names of its
# contributors may be used to endorse or promote products derived from
# this software without specific prior written permission. 
# 
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
# FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
# COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
# INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
# BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
# ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

import os, sys, template_helper
from config import Config
from mako.template import Template
import vfs

class Target():
    def __init__(self, tconfig, fs=vfs.LocalFS(), file_tracker=None):
        # FS
        self.fs = fs

        # Defaults
        self.config = {}
        self.input_file = None
        self.output_file = "tgt.out"
        self.error_file = "tgt.err"
        self.args = {}
        self.config_templates = []
        self.config_names = []

        # Load config
        if 'config' in tconfig:
            self.config = Config(tconfig['config'])
            self.cmd = self.config.value('cmd', required=True)

            self.name = self.config.value('name', required=True)
            self.input_file = self.config.value('input', default=None)
            self.output_file = self.config.value('output', default=self.output_file)
            self.error_file = self.config.value('error', default=self.error_file)
            self.args = self.config.value('args', default={})

            # Templates
            self.config_templates = []
            if 'config_paths' in self.config:
                for cp in self.config['config_paths']:
                    with open(cp, 'r') as cf:
                        self.config_templates.append(Template(cf.read()))
            elif self.config:
                self.config_templates = [Template(tt) for tt in self.config.templates]

            self.config_names = self.config.value('config_names', default=[])
            if not self.config_names:
                if 'config_paths' in self.config:
                    self.config_names = [os.path.basename(cp) for cp in self.config['config_paths']]
                elif self.config:
                    self.config_names = ['tgt_config'+str(i)+'.txt' for i in range(len(self.config_templates))]

        # Override cmd
        if 'cmd' in tconfig:
            self.cmd = tconfig['cmd']
        elif 'config' not in tconfig:
            raise Exception("Target cmd required!")
        self.cmd_template = Template(self.cmd)

        # Setup
        if 'input' in tconfig:
            self.input_file = tconfig['input']
        if 'output' in tconfig:
            self.output_file = tconfig['output']
        if 'error' in tconfig:
            self.error_file = tconfig['error']

        # File-tracking
        self.file_tracker = file_tracker


    def mako_args(self, trial, iter, total):
        target = {
            'input': self.input_file,
            'output': self.output_file,
            'error': self.error_file,
            'trial': trial,
            'iter': iter,
            'total_iter': total,
            'config': self.config_names
        }
        return template_helper.to_mako(target)


    def gen_configs(self, trial, iter, total, pre_args, iter_args, env_args, dir='.'):
        config_paths = []
        target = self.mako_args(trial, iter, total)

        m_iter_args = template_helper.to_mako(self.args)
        template_helper.merge_mako(m_iter_args, iter_args)

        for name, template in zip(self.config_names, self.config_templates):
            cp = os.path.join(dir, name)
            ctxt = template.render(target=target, pre=pre_args, iter=m_iter_args, env=env_args)

            # File tracking
            if self.file_tracker:
                cpt = self.file_tracker.file_exists(ctxt)
                if cpt:
                    config_paths.append(cpt)
                    self.file_tracker.inc(cpt)
                    continue

            # Save file
            self.fs.write_file(cp, ctxt)
            if self.file_tracker:
                self.file_tracker.add_file(cp)
            config_paths.append(cp)

        return config_paths


    def gen_cmd(self, trial, iter, total, pre_args, iter_args, env_args):
        target = self.mako_args(trial, iter, total)

        m_iter_args = template_helper.to_mako(self.args)
        template_helper.merge_mako(m_iter_args, iter_args)

        return self.cmd_template.render(target=target, pre=pre_args, iter=m_iter_args, env=env_args)
