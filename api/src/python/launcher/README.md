Launcher Script
===============

Overview
--------
The launcher script provides a way of launching error injectors (or any other instrumentation
application) in a templated manner. YAML templates are used to set launch properties, which apps to
instrument and how to sweep parameters. Most template values can use Mako syntax as well for more
flexiblity and access to runtime variables. Features include:

- Local (thread pool) launching
- Batch (i.e. `sbatch`) launching
- Control via SSH (useful for servers which restrict processes running on login nodes)
- Timeout, error, restart handling
- Logging of stdin/out
- Completely configurable using templates (complete with front-matter)
- Configuration files can be created for passing arguments to applications


Packages Required
-----------------
- `mako`
- `pyyaml`
- `futures`


Usage
-----

## Template basics
JSON, XML, or YAML templates can be used; however, only YAML templates have been tested extensively.
A template can contain "front-matter" (similar to jenkins web templates) which defines variables
which can be used in the body of the template using Mako syntax.

## Launch template
For an example of all possible properties, look at `configs/launch_example.yaml`. Follow the steps
for creating a template:

1. Copy or create a new template file (`my_template.yaml`)
2. Specify the job information
    - `type`: Local or cluster (batch system)
    - `config`: Cluster template (for submission)
    - `procs`: Number of processors per node (or in thread pool)
    - `args`: Additional args for the cluster template
3. File-system abstraction
    - `type`: local or SSH (note: ssh may need a SSH key or permissions in sshconfig)
    - `args`: args (username, password for SSH)
4. Iterations (`iter`) the number of iterations *per trial*
5. Instrumentation
    - `pre_trail`: this command is run *per trail* and scans the output for variables to use during
      regular instrumentation
    - `trail`: command to use for *each iteration*
    - `post_trial`: command to run after *each trail*
6. Target program, the application to instrument
    - The final command run is: `<trial_cmd> <target_cmd>`
7. Experiment generation
    - Each list item is a grouping (per trial)
    - Within a list, a dictionary of arguments can be specified
    - Per iteration arguments:
        - `"static value"`: static argument (constant over all iterations)
        - `[iter, list, [1, 2, 3]]`: Static list assigned to each iteration (wraps)
        - `[iter, range, [1, 3]]`: Static range assigned to each iteration (wraps)
        - `[iter, rand_list, [1, 2, 3]]`: Random choice out of list each iteration
        - `[iter, rand_range, [1, 3]]`: Random choice out of range each iteration
    - Per trial arguments:
        - `[trial, list, [1, 2, 3]]`: Static list assigned to each trial (wraps)
        - `[trial, range, [1, 3]]`: Static range assigned to each trial (wraps)
        - `[trial, rand_list, [1, 2, 3]]`: Random choice out of list each trial
        - `[trial, rand_range, [1, 3]]`: Random choice out of range each trial
    - Trial generation (matrix formed):
        - `[gen, list, [1, 2, 3]]`: Static list sweep
        - `[gen, range, [1, 3]]`: Static range sweep

## Launching
To launch run (requires python2.7):
`./launcher.py -c <config_file> -d <output_dir>`

For timeout use:
`./launcher.py -c <config_file> -d <output_dir> -t <timeout>`
