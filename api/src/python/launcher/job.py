# Copyright 2015, The University of Texas at Austin 
# All rights reserved.
# 
# THIS FILE IS PART OF THE HAMARTIA FAULT/ERROR ANALYSIS SUITE
# 
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met: 
# 
# 1. Redistributions of source code must retain the above copyright
# notice, this list of conditions and the following disclaimer. 
# 
# 2. Redistributions in binary form must reproduce the above copyright
# notice, this list of conditions and the following disclaimer in the
# documentation and/or other materials provided with the distribution. 
# 
# 3. Neither the name of the copyright holder nor the names of its
# contributors may be used to endorse or promote products derived from
# this software without specific prior written permission. 
# 
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
# FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
# COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
# INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
# BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
# ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

import yaml, os, hashlib, re, shlex, time, template_helper
from mako.template import Template
from config import Config
import vfs

SCRIPT_DIR = os.path.dirname(os.path.realpath(__file__))

class Task:
    def __init__(self, launcher, iter, iter_args, pre_args=None, dir='.', instrument=None, target=None, workspace=True, timeout=0, pre_dur=0):
        # Setup
        self.iter = iter
        # Args
        self.iter_args = iter_args
        self.pre_args = template_helper.to_mako({})
        if pre_args:
            self.pre_args = pre_args
        self.launcher = launcher
        # Instrument
        self.instrument = launcher.instrument
        if instrument:
            self.instrument = instrument
        # Target
        self.target = launcher.target
        if target:
            self.target = target
        # Timeout
        self.timeout = 0
        if timeout:
            if timeout[-1].lower() == 'x' and pre_args:
                self.timeout = int(float(timeout[:-1])*float(pre_dur))
            else:
                self.timeout = int(timeout)

        # Setup working directory
        self.status = None
        if workspace:
            self.dir = self.setup_rundir(dir)
        else:
            self.dir = dir

        if self.status == 'started':
            self.iter_args = template_helper.to_mako(self.info['args'])

        # Setup instrument
        self.trial = self.launcher.get_trial(self.iter)
        self.instrument.gen_configs(self.trial, iter, self.launcher.total_iter, pre_args, iter_args, self.launcher.env_args, self.dir)

        # Setup target
        self.target.gen_configs(self.trial, iter, self.launcher.total_iter, pre_args, iter_args, self.launcher.env_args, self.dir)


    @property
    def cmd(self):
        inst_cmd = self.instrument.gen_cmd(self.trial, self.iter, self.launcher.total_iter, self.pre_args, self.iter_args, self.launcher.env_args)
        tgt_cmd = self.target.gen_cmd(self.trial, self.iter, self.launcher.total_iter, self.pre_args, self.iter_args, self.launcher.env_args)
        if self.instrument.target_args:
            tgt_cmd += self.instrument.gen_target_args(self.target, self.trial, self.iter, self.launcher.total_iter, self.pre_args, self.iter_args, self.launcher.env_args)
        return inst_cmd + tgt_cmd

    @property
    def cmd_str(self):
        return ' '.join(self.cmd)

    @property
    def sh_cmd(self):
        shcmd = ' '.join(self.cmd) + ' 1> ' + self.instrument.output_file + ' 2>' + self.instrument.error_file
        return shcmd

    @property
    def ext_sh_cmd(self):
        shcmd = 'pushd `pwd` ; cd "' + self.dir + '" ; (time ' + ' '.join(self.cmd) + ' 1> ' + self.instrument.output_file + ' 2>' + self.instrument.error_file + ') 2> time.out ; touch COMPLETE ; popd'
        return shcmd


    def execute(self):
        # Execute
        start = time.time()

        if self.timeout == 0: 
            (out, err) = self.launcher.fs.execute(self.cmd + " ; touch COMPLETE", cwd=self.dir)
        else:
            (out, err) = self.launcher.fs.execute(self.cmd + " && touch COMPLETE", cwd=self.dir, timeout=self.timeout)
        
        duration = time.time() - start

        return (out, err, duration)


    def parse_output(self, out=None):
        args = {}
        for parse in self.instrument.parse:
            if 'file' in parse:
                fout = self.launcher.fs.read_file(os.path.join(self.dir, parse['file']))
                args.update(dict(template_helper.decode_str(fout, parse['type'])))
            else:
                if out is None:
                    out = self.launcher.fs.read_file(os.path.join(self.dir, self.instrument.output_file))
                args.update(dict(template_helper.decode_str(out, parse['type'])))

        return args


    def save_output(self, out, err):
        # Save output
        if out:
            self.launcher.fs.write_file(os.path.join(self.dir, self.instrument.output_file), out)
        # Save errput
        if err:
            self.launcher.fs.write_file(os.path.join(self.dir, self.instrument.error_file), err)


    def read_duration(self, remove=True):
        duration = 0
        # Parse time
        tf = self.launcher.fs.read_file(os.path.join(self.dir, "time.out")).split('\n')
        multiplier = {'s': 1, 'm': 60, 'h': 3600, None: 1}
        for l in tf:
            if 'real' in l:
                for m in re.finditer(r'([\d\.]+)([a-z])?', l):
                    duration += float(m.group(1)) * multiplier[m.group(2)]
        # Remove
        self.launcher.fs.remove(os.path.join(self.dir, "time.out"))

        return duration


    def save_info(self, status, **kwargs):
        # Create debug file
        df = {
            'iter': self.iter, 
            'trial': self.trial, 
            'trial_iter': self.iter % self.launcher.iter,
            'cmd': ' '.join(self.cmd), 
            'args': self.iter_args.__dict__,
            'status': status
        }
        df.update(kwargs)
        self.launcher.fs.write_file(os.path.join(self.dir, "info.yaml"), template_helper.encode_str(df, 'yaml'))


    def setup_rundir(self, base_dir):
        """
        Create the run directory for test(s)

        Args:
            id    (int): Identifier for the test/run
            args (list): Args used during the run

        Returns:
            str: Directory path
        """

        # Create working directory
        wdir = os.path.join(base_dir, str(self.iter))
        try:
            self.launcher.fs.mkdir(wdir)
        except Exception as e:
            # Already exists
            info_file = os.path.join(wdir, 'info.yaml')
            complete_file = os.path.join(wdir, 'COMPLETE')
            timeout_file = os.path.join(wdir, 'TIMEOUT')
            info_data = None
            try:
                info_data = self.launcher.fs.read_file(info_file)
            except Exception as ie:
                print ie
                return wdir

            info = template_helper.decode_str(info_data, 'yaml')
            self.status = info['status']
            self.info = info
            if self.launcher.fs.exists(timeout_file) and self.status != 'done':
                self.status = 'timeout'
            elif self.launcher.fs.exists(complete_file) and self.status != 'done':
                self.status = 'complete'

        return wdir


class Job:
    def __init__(self, launcher, id, job_type, tasks):
        # ID (iteration)
        self.id = id
        self.type = job_type
        self.name = hashlib.sha256(str(id)+self.type).hexdigest()[:8]
        self.launcher = launcher
        self.tasks = tasks
        # Output file
        self.output = os.path.join(self.launcher.dirs['log'], self.name+"_stdout.log")
        # Error file
        self.error = os.path.join(self.launcher.dirs['log'], self.name+"_stderr.log")
        self.config = None


    def mako_args(self):
        task_alloc = [[] for x in range(min(len(self.tasks), self.launcher.procs))]
        for i, t in enumerate(self.tasks):
            task_alloc[i%self.launcher.procs].append(t)
        job = {
            'name': self.name,
            'output': self.output,
            'error': self.error,
            'timeout': self.launcher.timeout,
            'queue': self.launcher.queue,
            'alloc': self.launcher.alloc,
            'cores': self.launcher.procs,
            'startup': self.launcher.startup,
            'tasks': task_alloc,
            'args': self.launcher.args
        }
        return template_helper.to_mako(job)


    def gen_config(self):
        ct = self.launcher.cluster_template.render(job=self.mako_args(), env=self.launcher.env_args)
        filename = os.path.join(self.launcher.dirs['log'], str(self.id)+'_'+self.name+'.' + self.launcher.cluster_name)
        self.launcher.fs.write_file(filename, ct)

        self.config = filename
        return filename


    def submit(self):
        lt = Template(self.launcher.launch_cmd).render(job=self.mako_args(), env=self.launcher.env_args)
        launch_cmd = shlex.split(lt) + [self.config]
        try:
            (out, err) = self.launcher.fs.execute(launch_cmd)
            if 'error' in err:
                raise Exception("Job submit issue (" + err + ")")
            return out
        except Exception as e:
            raise Exception("Job submit issue (" + str(e) + ")")
