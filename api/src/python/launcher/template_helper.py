# Copyright 2015, The University of Texas at Austin 
# All rights reserved.
# 
# THIS FILE IS PART OF THE HAMARTIA FAULT/ERROR ANALYSIS SUITE
# 
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met: 
# 
# 1. Redistributions of source code must retain the above copyright
# notice, this list of conditions and the following disclaimer. 
# 
# 2. Redistributions in binary form must reproduce the above copyright
# notice, this list of conditions and the following disclaimer in the
# documentation and/or other materials provided with the distribution. 
# 
# 3. Neither the name of the copyright holder nor the names of its
# contributors may be used to endorse or promote products derived from
# this software without specific prior written permission. 
# 
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
# FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
# COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
# INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
# BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
# ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

import yaml, json, csv, vfs
import xml.etree.ElementTree as ET

def convert_xml(et):
    parent = {}
    tags = {}
    for e in et:
        if e.tag not in tags:
            parent[e.tag] = {}
            tags[e.tag] = 1
        else:
            parent[e.tag] = []

    for tag, elem in et.attrib.items() + [(e.tag, e) for e in et]:
        if isinstance(elem, ET.Element):
            if type(parent[tag]) is list:
                parent[tag].append(convert_xml(elem))
            else:
                parent[tag] = convert_xml(elem)
        else:
            parent[tag] = elem

    return parent


def decode_str(str_data, file_type):
    if file_type == 'yaml':
        return yaml.load(str_data)
    elif file_type == 'json':
        return json.loads(str_data)
    elif file_type == 'xml':
        return convert_xml(ET.fromstring(str_data))

    raise Exception("Invalid file format!")


def encode_str(data, file_type):
    if file_type == 'yaml':
        return yaml.dump(data)
    elif file_type == 'json':
        return json.dumps(data)
    
    raise Exception("Invalid file format!")


def decode_file(file_name, file_type, fs=vfs.LocalFS()):
    with fs.open(file_name, 'r') as pf:
        if file_type == 'yaml':
            return yaml.load(pf)
        elif file_type == 'json':
            return json.load(pf)
        elif file_type == 'csv':
            csv_read = csv.reader(pf)
            return [[c for c in r] for r in csv_read]
        elif file_type == 'xml':
            return convert_xml(ET.parse(file_name))
    
    raise Exception("Invalid file format!")


def encode_file(file_name, file_type, data, fs=vfs.LocalFS):
    with fs.open(file_name, 'w') as pf:
        if file_type == 'yaml':
            pf.write(yaml.dump(data))
        elif file_type == 'json':
             json.dump(data, pf)
        elif file_type == 'csv':
            csv_write = csv.writer(pf)
            for r in data:
                csv_write.writerow(list(r))
    
    raise Exception("Invalid file format!")


# Mako Helpers
class MakoBunch(dict):
    def __init__(self, d):
        """
        Args:
            d (dict): dictionary to convert
        """

        dict.__init__(self, d)
        self.__dict__.update(d)


def merge_mako(mp, ms):
    """
    Make a dict into a object (easy for mako)
    
    Args:
        dl (object): object to convert (mainly handles dicts)

    Returns:
        object: Mako friendly object
    """
    for k,v in ms.__dict__.iteritems():
        if k in mp.__dict__:
            if type(v) is list:
                for i, vv in enumerate(v):
                    if i < len(mp[k]):
                        if isinstance(vv, MakoBunch) and isinstance(mp.__dict__[k][i], MakoBunch):
                            mp.__dict__[k][i] = merge_mako(mp.__dict__[k][i], vv)
                        else:
                            mp.__dict__[k][i] = vv
                    else:
                        mp.__dict__[k].append(vv)
            elif isinstance(v, MakoBunch):
                mp.__dict__[k] = merge_mako(mp.__dict__[k], v)
            else:
                mp.__dict__[k] = v
        else:
            mp.__dict__[k] = v


def to_mako(dl):
    """
    Make a dict into a object (easy for mako)
    
    Args:
        dl (object): object to convert (mainly handles dicts)

    Returns:
        object: Mako friendly object
    """
    if type(dl) is list:
        # If list, just convert elements
        r = []
        for i in dl:
            r.append(to_mako(i))
        return r
    elif type(dl) is dict:
        # Convert dicts
        r = {}
        for k, v in dl.items():
            if isinstance(v, dict):
                v = to_mako(v)
            r[k] = v
        return MakoBunch(r)
