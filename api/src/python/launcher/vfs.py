# Copyright 2015, The University of Texas at Austin 
# All rights reserved.
# 
# THIS FILE IS PART OF THE HAMARTIA FAULT/ERROR ANALYSIS SUITE
# 
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met: 
# 
# 1. Redistributions of source code must retain the above copyright
# notice, this list of conditions and the following disclaimer. 
# 
# 2. Redistributions in binary form must reproduce the above copyright
# notice, this list of conditions and the following disclaimer in the
# documentation and/or other materials provided with the distribution. 
# 
# 3. Neither the name of the copyright holder nor the names of its
# contributors may be used to endorse or promote products derived from
# this software without specific prior written permission. 
# 
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
# FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
# COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
# INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
# BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
# ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

import os, sys, signal, paramiko, socket
import subprocess32 as subprocess
from paramiko import SSHException
from abc import ABCMeta, abstractmethod

# Virtual file system
class VFS():
    __metaclass__ = ABCMeta

    def __init__(self, basepath=None):
        self.basepath = basepath

    @abstractmethod
    def execute(self, cmd, cwd='.', *args, **kwargs):
        pass

    @abstractmethod
    def open(self, filename, mode, *args, **kwargs):
        pass

    @abstractmethod
    def mkdir(self, dirname, *args, **kwargs):
        pass

    @abstractmethod
    def rename(self, src, dest, *args, **kwargs):
        pass

    @abstractmethod
    def remove(self, filename, *args, **kwargs):
        pass

    @abstractmethod
    def exists(self, filename):
        pass

    @abstractmethod
    def read_file(self, filename, *args, **kwargs):
        pass

    @abstractmethod
    def write_file(self, filename, data, *args, **kwargs):
        pass


# Local file system
class LocalFS(VFS):
    def __init__(self, basepath=None):
        super(LocalFS, self).__init__(basepath)


    def execute(self, cmd, cwd='.', *args, **kwargs):
        out = None
        err = None
        timeout = kwargs.get('timeout', None)
        if timeout:
            timeout = int(timeout)
        try:
            p = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE, cwd=cwd, shell=True, preexec_fn=os.setsid)
            (out, err) = p.communicate(timeout=timeout)
        except subprocess.TimeoutExpired:
            os.killpg(os.getpgid(p.pid), signal.SIGKILL)
            (out, err) = p.communicate()
            with open(os.path.join(cwd,'TIMEOUT'), "w"):
                pass
            return (out, err)
        except Exception as e:
            raise Exception("Exec error on '" + ' '.join(cmd) + "' (" + str(e) + ")")

        return (out, err)


    def open(self, filename, mode, *args, **kwargs):
        fd = None
        try:
            fd = open(filename, mode, *args, **kwargs)
        except Exception as e:
            raise Exception("Open error on '" + filename + "' (" + str(e) + ")")
        return fd


    def mkdir(self, dirname, *args, **kwargs):
        fd = None
        try:
            fd = os.mkdir(dirname, *args, **kwargs)
        except Exception as e:
            raise Exception("Mkdir error on '" + dirname + "' (" + str(e) + ")")
        return fd


    def rename(self, src, dest, *args, **kwargs):
        fd = None
        try:
            fd = os.rename(src, dest, *args, **kwargs)
        except Exception as e:
            raise Exception("Rename error on '" + src + '/' + dest + "' (" + str(e) + ")")
        return fd


    def remove(self, filename, *args, **kwargs):
        fd = None
        try:
            fd = os.remove(filename, *args, **kwargs)
        except Exception as e:
            raise Exception("Remove error on '" + filename + "' (" + str(e) + ")")
        return fd


    def exists(self, filename):
        return (os.path.isfile(filename) or os.path.isdir(filename))


    def read_file(self, filename, *args, **kwargs):
        data = None
        try:
            with open(filename, 'r') as rf:
                data = rf.read()
        except Exception as e:
            raise Exception("Read error on '" + filename + "' (" + str(e) + ")")
        return data


    def write_file(self, filename, data, *args, **kwargs):
        try:
            with open(filename, 'w') as wf:
                wf.write(data)
        except Exception as e:
            raise Exception("Write error on '" + filename + "' (" + str(e) + ")")
        return True


# Local file system
class SSHFS(VFS):
    def __init__(self, host, username, password=None, port=22, basepath=None):
        super(SSHFS, self).__init__(basepath)

        self.host = host
        self.username = username
        self.password = password
        self.port = port
        self.basepath = basepath

        # SSH client
        self.client = paramiko.SSHClient()
        self.client.set_missing_host_key_policy(paramiko.AutoAddPolicy())

        # Get SSH config
        ssh_config = paramiko.SSHConfig()
        user_config_file = os.path.expanduser("~/.ssh/config")
        if os.path.exists(user_config_file):
            with open(user_config_file) as f:
                ssh_config.parse(f)

        cfg = {'hostname': self.host, 'username': self.username, 'look_for_keys': True}
        if self.password:
            cfg['password'] = self.password

        user_config = ssh_config.lookup(cfg['hostname'])
        for k in ('hostname', 'username', 'port'):
            if k in user_config:
                cfg[k] = user_config[k]
        if 'identityfile' in user_config:
            cfg['pkey'] = paramiko.RSAKey.from_private_key_file(user_config['identityfile'][0])

        if 'proxycommand' in user_config:
            cfg['sock'] = paramiko.ProxyCommand(user_config['proxycommand'])
        
        self.ssh_config = cfg
        self.connect()


    @property
    def connected(self):
        transport = self.client.get_transport() if self.client else None
        return transport and transport.is_active() and transport.is_authenticated()


    def connect(self):
        if not self.connected:
            self.client.connect(**self.ssh_config)
            self.sftp = self.client.open_sftp()


    def retry(self, func, *args, **kwargs):
        retires = 0
        result = None
        while 1:
            try:
                result = func(*args, **kwargs)
                break
            except (SSHException, socket.error) as se:
                if retries >= 10:
                    raise Exception("SSH connection error!")
                self.connect()
                retries += 1

        return result


    def execute(self, cmd, cwd='.', *args, **kwargs):
        out = None
        err = None
        try:
            cmdstr = "pushd `pwd` >/dev/null  && cd '" + cwd + "' && " + " ".join(cmd) + " && popd >/dev/null"
            (in_f, out_f, err_f) = self.retry(self.client.exec_command, cmdstr)
            out = out_f.read()
            err = err_f.read()
        except Exception as e:
            raise Exception("Exec error on '" + cmdstr + "' (" + str(e) + ")")
        return (out, err)


    def open(self, filename, mode, *args, **kwargs):
        fd = None
        try:
            fd = self.retry(self.sftp.open, filename, mode, *args, **kwargs)
        except Exception as e:
            raise Exception("Open error on '" + filename + "' (" + str(e) + ")")
        return fd


    def mkdir(self, dirname, *args, **kwargs):
        fd = None
        try:
            fd = self.retry(self.sftp.mkdir, dirname, *args, **kwargs)
        except Exception as e:
            raise Exception("Mkdir error on '" + dirname + "' (" + str(e) + ")")
        return fd


    def rename(self, src, dest, *args, **kwargs):
        fd = None
        try:
            fd = self.retry(self.sftp.rename, src, dest, *args, **kwargs)
        except Exception as e:
            raise Exception("Rename error on '" + src + '/' + dest + "' (" + str(e) + ")")
        return fd


    def remove(self, filename, *args, **kwargs):
        fd = None
        try:
            fd = self.retry(self.sftp.remove, filename, *args, **kwargs)
        except Exception as e:
            raise Exception("Remove error on '" + filename + "' (" + str(e) + ")")
        return fd


    def exists(self, filename):
        try:
            return isinstance(self.retry(self.sftp.lstat, filename), paramiko.sftp_attr.SFTPAttributes)
        except IOError as io:
            return False
        except Exception as e:
            raise Exception("Exists error on '" + filename + "' (" + str(e) + ")")
        return False


    def _read_file(self, filename, *args, **kwargs):
        data = None
        with self.sftp.open(filename, 'r') as rf:
            data = rf.read()
        return data


    def read_file(self, filename, *args, **kwargs):
        try:
            return self.retry(self._read_file, filename, *args, **kwargs)
        except Exception as e:
            raise Exception("Read error on '" + filename + "' (" + str(e) + ")")
        return None


    def _write_file(self, filename, data, *args, **kwargs):
        with self.sftp.open(filename, 'w') as wf:
            wf.write(data)
        return True


    def write_file(self, filename, data, *args, **kwargs):
        try:
            self.retry(self._write_file, filename, data, *args, **kwargs)
        except Exception as e:
            raise Exception("Write error on '" + filename + "' (" + str(e) + ")")
        return True
