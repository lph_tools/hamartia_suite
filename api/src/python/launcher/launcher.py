#! /usr/bin/env python2

# Copyright 2015, The University of Texas at Austin 
# All rights reserved.
# 
# THIS FILE IS PART OF THE HAMARTIA FAULT/ERROR ANALYSIS SUITE
# 
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met: 
# 
# 1. Redistributions of source code must retain the above copyright
# notice, this list of conditions and the following disclaimer. 
# 
# 2. Redistributions in binary form must reproduce the above copyright
# notice, this list of conditions and the following disclaimer in the
# documentation and/or other materials provided with the distribution. 
# 
# 3. Neither the name of the copyright holder nor the names of its
# contributors may be used to endorse or promote products derived from
# this software without specific prior written permission. 
# 
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
# FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
# COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
# INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
# BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
# ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

"""
Injector Launcher (capable of launching other programs as well), allows injector instances
to be launched on a single system (threadpool) or on cluster (through batch submission). Also,
allows any argument to be varied in some manner. This functionality makes it easy to conduct
large scale tests (constant, varied, or random/monte-carlo) while varying nearly any parameter.

Attributes
==========
Attributes:
    SCRIPT_DIR          (str): directory of the script (absolute path)
    TEMP_LOOKUP      (object): Lookup path for MAKO temlpates
    QUEUE_CHECK         (int): timeout/delay in between checking the batch queue and launching (seconds)
    launcher_options (object): command line options

Methods
=======
"""

# Libs
import os, sys, time, multiprocessing, subprocess, argparse, itertools, re, random, yaml, signal, operator
from mako.template import Template
from mako.lookup import TemplateLookup
from job import Job
import launchers
from config import Config

# Constants
SCRIPT_DIR = os.path.dirname(os.path.realpath(__file__))
TEMP_LOOKUP = TemplateLookup(directories=['.', SCRIPT_DIR])
QUEUE_CHECK = 30

# Settings
launcher_options = None

def vprint(msg):
    """
    Verbose print

    Args:
        msg (str): Message to print
    """

    global launcher_options

    if launcher_options.verbose:
        print msg


def setup_rundir(id, args):
    """
    Create the run directory for test(s)

    Args:
        id    (int): Identifier for the test/run
        args (list): Args used during the run

    Returns:
        str: Directory path
    """

    global launcher_options

    # Create working directory
    wdir = os.path.join(launcher_options.output, str(id))
    os.mkdir(wdir)
    # Create debug file
    df = {'id': id, 'args': args}
    with open(os.path.join(wdir, "info"), 'w') as infof:
        yaml.dump(df, infof)

    return wdir


def launch_injector((id, args)):
    """
    Launch an injector instance (locally), args as tuple

    Args:
        id    (int): Identifier for the test/run
        args (list): Args used during the run
    """

    # Setup working dir
    wdir = setup_rundir(id, args)
    # Launch injector
    proc = subprocess.Popen(args, stdout=subprocess.PIPE, stderr=subprocess.PIPE, cwd=wdir)
    vprint("Launched instance " + str(id))
    (out, err) = proc.communicate()
    # Save output/error
    with open(os.path.join(wdir, "stdout.log"), 'w') as fout:
        fout.write(out)
    with open(os.path.join(wdir, "stderr.log"), 'w') as ferr:
        ferr.write(err)

    # FIXME: how to parse results? Not sure what the file type is...


def launch_batch(max_iter, args, batch_type='sbatch', batch_size=1, max_tasks=2):
    """
    Launch batch job (cluster)

    Args:
        max_iter    (int): Maximum iterations (for the entire test)
        args       (list): Processed args (not expanded yet)
        batch_type  (str): Batch type (used for template lookup)
        batch_size  (int): Amount of instances to run in one batch job
        max_task    (int): Max amount of concurrently running/submitted jobs
    """

    global launcher_options

    iter = 0
    running = {}
    queue_cmd = None

    while iter < max_iter:
        # Start new jobs
        for i in range(max_tasks-len(running)):
            # Job creation
            jid = iter
            jargs = [argument_expansion(max_iter, args).next() for ji in range(batch_size)]
            wdir = setup_rundir(jid, jargs)
            job = Job(batch_type, jid, jargs, wdir)
            job.cores = launcher_options.proc
            if launcher_options.alloc is not None:
                job.alloc = launcher_options.alloc
            if launcher_options.queue is not None:
                job.queue = launcher_options.queue
            if queue_cmd is None:
                queue_cmd = job.queue_cmd

            # Generate batch file
            bfile = None
            with open(os.path.join(SCRIPT_DIR, 'batch_templates', batch_type + '.mako'), 'r') as tf:
                jtemp = Template(tf.read(), lookup=TEMP_LOOKUP)
                jfile = jtemp.render(job=job)

            # Write batch file
            jfilename = os.path.join(wdir, batch_type)
            with open(jfilename, 'w') as jf:
                jf.write(jfile)

            # Submit batch
            if job.batch_cmd is None:
                raise Exception("No batch cmd specified!")
            jl = subprocess.Popen(job.batch_cmd + [jfilename], stdout=subprocess.PIPE, stderr=subprocess.PIPE, cwd=wdir)
            (jout, jerr) = jl.communicate()

            # Verbose
            vprint("Submitted job " + job.name + " (" + str(iter/batch_size+1) + "/" + str(max_iter/batch_size+1) + ")")

            # Add
            running[job.name] = job
            iter += batch_size

        # Sleep
        time.sleep(QUEUE_CHECK)

        # Check running
        if queue_cmd is None:
            raise Exception("No queue cmd specified!")
        queue = subprocess.Popen(queue_cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        (out, err) = queue.communicate()

        # Remove completed
        for jname, job in running.iteritems():
            if jname not in out:
                vprint("Job " + jname + " finished")
                del running[jname]


def main(args):
    """
    Launcher entry point (CLI)

    Args:
        args (list): Command line arguments
    """

    global launcher_options

    # Description
    desc = "Hamartia Injector Launcher"

    # Argument parser
    parser = argparse.ArgumentParser(description=desc, formatter_class=argparse.RawDescriptionHelpFormatter)

    # Options
    parser.add_argument('-c',  '--config',        type=str,            help="Launch configuration file")
    parser.add_argument('-b',  '--batch',         type=str,            help="Batch file generation type/name/template")
    parser.add_argument('-a',  '--alloc',         type=str,            help="Allocation (for batch submission)")
    parser.add_argument('-q',  '--queue',         type=str,            help="Queue (for batch submission)")
    parser.add_argument('-p',  '--procs',         type=int,            help="Number of processes", default=multiprocessing.cpu_count())
    parser.add_argument('-i',  '--iter',          type=int,            help="Number of iterations")
    parser.add_argument('-d',  '--dir',           type=str,            help="Output directory", default='.')
    parser.add_argument('-z',  '--analysis',      action='store_true', help="Analyze results")
    parser.add_argument('-l',  '--lib',           action='append',     help="Error/detector library or path")
    parser.add_argument('-s',  '--startup',       type=str,            help="Startup script")
    parser.add_argument('-t',  '--timeout',       type=str,            help="Timeout ('x' indicates slowdown over pre)")
    parser.add_argument('-v',  '--verbose',       action='store_true', help="Verbose printing")

    # Parse
    options = parser.parse_args(args)

    # Launcher
    launcher_config = {}
    if options.config:
        launcher_config = Config(options.config, has_templates=False)
    job_type = launcher_config.value('job:type', default='local')
    launcher = None
    if job_type == 'local':
        launcher = launchers.LocalLauncher(launcher_config, options)
    elif job_type == 'cluster':
        launcher = launchers.ClusterLauncher(launcher_config, options)

    # Signals
    signal.signal(signal.SIGQUIT, launcher.quit)
    signal.signal(signal.SIGHUP, launcher.quit)
    signal.signal(signal.SIGTERM, launcher.kill)
    signal.signal(signal.SIGINT, launcher.kill)

    # Start
    launcher.start()
    # End
    launcher.end()


if __name__ == '__main__':
    main(sys.argv[1:])
