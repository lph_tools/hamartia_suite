# Copyright 2015, The University of Texas at Austin 
# All rights reserved.
# 
# THIS FILE IS PART OF THE HAMARTIA FAULT/ERROR ANALYSIS SUITE
# 
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met: 
# 
# 1. Redistributions of source code must retain the above copyright
# notice, this list of conditions and the following disclaimer. 
# 
# 2. Redistributions in binary form must reproduce the above copyright
# notice, this list of conditions and the following disclaimer in the
# documentation and/or other materials provided with the distribution. 
# 
# 3. Neither the name of the copyright holder nor the names of its
# contributors may be used to endorse or promote products derived from
# this software without specific prior written permission. 
# 
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
# FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
# COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
# INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
# BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
# ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

import os, sys, template_helper
from config import Config
from mako.template import Template
import vfs

class Instrument():
    def __init__(self, iconfig, fs=vfs.LocalFS(), file_tracker=None):
        # FS
        self.fs = fs

        # Defaults
        self.config = {}
        self.target_args = None
        self.output_file = "inst.out"
        self.error_file = "inst.err"
        self.args = {}
        self.config_templates = []
        self.config_names = []

        # Load config
        if 'config' in iconfig:
            self.config = Config(iconfig['config'])
            self.cmd = self.config.value('cmd', required=True)
            self.target_args = self.config.value('target_args', default=None)

            self.name = self.config.value('name', required=True)
            self.output_file = self.config.value('output', default=self.output_file)
            self.error_file = self.config.value('error', default=self.error_file)
            self.args = self.config.value('args', default={})

            # Templates
            self.config_templates = []
            if 'config_paths' in self.config:
                for cp in self.config['config_paths']:
                    with open(cp, 'r') as cf:
                        self.config_templates.append(Template(cf.read()))
            elif self.config:
                self.config_templates = [Template(tt) for tt in self.config.templates]

            self.config_names = self.config.value('config_names', default=[])
            if not self.config_names:
                if 'config_paths' in self.config:
                    self.config_names = [os.path.basename(cp) for cp in self.config['config_paths']]
                elif self.config:
                    self.config_names = ['inst_config'+str(i)+'.txt' for i in range(len(self.config_templates))]


        # Override cmd
        if 'cmd' in iconfig:
            self.cmd = iconfig['cmd']
        elif 'config' not in iconfig:
            raise Exception("Instruction cmd required!")
        self.cmd_template = Template(self.cmd)

        if 'target_args' in iconfig:
            self.target_args = iconfig['target_args']
        if self.target_args:
            self.target_arg_template = Template(self.target_args)

        # Setup overrides
        if 'output' in iconfig:
            self.output_file = iconfig['output']
        if 'error' in iconfig:
            self.error_file = iconfig['error']

        # Parse
        self.parse = []
        if 'parse' in iconfig:
            self.parse = iconfig['parse']

        # File-tracking
        self.file_tracker = file_tracker


    def mako_args(self, trial, iter, total):
        inst = {
            'output': self.output_file,
            'error': self.error_file,
            'trial': trial,
            'iter': iter,
            'total_iter': total,
            'config': self.config_names
        }
        return template_helper.to_mako(inst)


    def gen_configs(self, trial, iter, total, pre_args, iter_args, env_args, dir='.'):
        config_paths = []
        inst = self.mako_args(trial, iter, total)

        m_iter_args = template_helper.to_mako(self.args)
        template_helper.merge_mako(m_iter_args, iter_args)

        for name, template in zip(self.config_names, self.config_templates):
            cp = os.path.join(dir, name)
            ctxt = template.render(inst=inst, pre=pre_args, iter=m_iter_args, env=env_args)

            # File tracking
            if self.file_tracker:
                cpt = self.file_tracker.file_exists(ctxt)
                if cpt:
                    config_paths.append(cpt)
                    self.file_tracker.inc(cpt)
                    continue

            # Save file
            self.fs.write_file(cp, ctxt)
            if self.file_tracker:
                self.file_tracker.add_file(cp)
            config_paths.append(cp)

        return config_paths


    def gen_target_args(self, target, trial, iter, total, pre_args, iter_args, env_args):
        if self.target_args:
            tgt = target.mako_args(trial, iter, total)

            m_iter_args = template_helper.to_mako(target.args)
            template_helper.merge_mako(m_iter_args, iter_args)

            return self.target_arg_template.render(target=tgt, pre=pre_args, iter=m_iter_args, env=env_args)
        return ""


    def gen_cmd(self, trial, iter, total, pre_args, iter_args, env_args):
        inst = self.mako_args(trial, iter, total)

        m_iter_args = template_helper.to_mako(self.args)
        template_helper.merge_mako(m_iter_args, iter_args)

        return self.cmd_template.render(inst=inst, pre=pre_args, iter=m_iter_args, env=env_args)
