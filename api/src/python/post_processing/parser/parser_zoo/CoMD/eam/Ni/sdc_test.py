import os, sys, re
import numpy

'''
    Analyze injection outcome based on stdout file
    The final potential energy and total energy are used to test SDC.
    Quality Codes:
        - 0: Masked (allow potential energy to be off up to two decimal points)
        - 1: incomplete result string (DDC)
        - 2: atom lost (DDC)
        - 3: abnormal final energy (DDC) 
        - 4: abnormal potential energy (DDC)
        - 5: soft SDC (treated as Masked for this app) (i.e., potential energy is good enough)
        - 6: hard SDC
'''
def sdc_test(ofp):
             #  Loop   Time(fs) Total Energy   Potential Energy     Kinetic Energy  Temperature   (us/atom)     # Atoms
    GOLDEN = ['100', '100.00', '-4.369828677395', '-4.414865444762', '0.045036767367', '348.4200', '2.7698', '32000']
     
    field_map = {
        'total_energy': 2,
        'potential_energy': 3,
        'kinetic_energy': 4,
        'temperature': 5, 
        'atoms': 7,
    }   

    if not os.path.exists(ofp):
        out = {'code': 1, 'reason': 'no output'}
        return ("DDC", out)

    with open(ofp) as out_f:
        ofc = out_f.read()

        #                loop    Time    Tot.E     P.E     K.E    Temp    Perf    Atoms 
        result_str = "\s+100\s+(100.00)\s+(\S+)\s+(\S+)\s+(\S+)\s+(\S+)\s+(\S+)\s+(\S+)"
        result = ""
        try:
            result = re.search(result_str, ofc).group()
            result_list = result.split() 
            tot_energy = result_list[field_map['total_energy']]
            pot_energy = result_list[field_map['potential_energy']]
            num_atoms = result_list[field_map['atoms']]
        except Exception as e:
            print str(e) + " in " + ofp
            out = {'code': 1, 'reason': str(e)}
            return ("DDC", out)

        tot_energy_gold = GOLDEN[field_map['total_energy']] 
        pot_energy_gold = GOLDEN[field_map['potential_energy']] 
        num_atoms_gold = GOLDEN[field_map['atoms']] 
        
        if tot_energy_gold == tot_energy and pot_energy_gold == pot_energy and num_atoms_gold == num_atoms:
            return ("Masked", None)
        else:
            # check atom lost
            if num_atoms_gold != num_atoms:
                out = {'code': 2, 'reason': "Atom lost"}
                return ("DDC", out)
            # check final energy
            if not numpy.isfinite(float(tot_energy)) or float(tot_energy) > 0.0:
                out = {'code': 3, 'reason': "Abnormal total energy: {}".format(tot_energy)}
                return ("DDC", out)
            # compare potential energy
            if len(pot_energy_gold) != len(pot_energy):
                out = {'code': 4, 'reason': "Abnormal potential energy: {}".format(pot_energy)}
                return ("DDC", out)
            # the difference location
            diff_locs = [i for i in xrange(len(pot_energy_gold)) if pot_energy_gold[i] != pot_energy[i]]
            if not diff_locs:
                out = {'code': 5, 'pot_energy': pot_energy, 'tot_energy': tot_energy}
                return ("Masked", out)
            else:
                sig_diff_loc = min(diff_locs) # the most significant different location
                decimal_point_loc = pot_energy.find('.')
                if sig_diff_loc < decimal_point_loc:
                    out = {'code': 4, 'reason': "Abnormal potential energy: {}".format(pot_energy)}
                    return ("DDC", out)
                else:
                    # check masked
                    str_len = len(pot_energy_gold)
                    loc_after_point = sig_diff_loc - decimal_point_loc

                    if sig_diff_loc == str_len - 1 or sig_diff_loc == str_len - 2:
                        out = {'code': 5, 'loc': loc_after_point, 'pot_energy': pot_energy}
                        return ("Masked", out)
                    else:
                        out = {'code': 6, 'loc': loc_after_point, 'pot_energy': pot_energy}
                        return ("SDC", out)
