import numpy, re, os
'''
    Quality Code:
        - 0 : Masked
        - 1 : incomplete output (DDC)
        - 2 : abnormal norm (DDC)
        - 3 : SDC
'''
# Golden norm is based on '-n 24 24 24'
def sdc_test(out_f_path):
    norm_str ='Final Relative Residual Norm = (\S+)'
    golden_norm =  '6.538225e-09'
    if not os.path.exists(out_f_path):
        out = {'code':1, 'reason': 'incomplete results'}
        return ("DDC", out)

    with open(out_f_path) as out_f:
        ofc = out_f.read()
        inj_norm = ""
        success = None
        try:
            inj_norm = re.search(norm_str, ofc).group(1)
        except Exception as e:
            print str(e) + " at " + out_f_path
            out = {'code':1, 'reason': 'incomplete results'}
            return ("DDC", out)

        if not numpy.isfinite(float(inj_norm)) or float(inj_norm) > 1.0:
            out = {'code':2, 'relative_resid_norm': inj_norm}
            return ("DDC", out)
       
        if float(inj_norm) < 1e-6:
            out = {'code':0, 'relative_resid_norm': inj_norm}
            return ("Masked", out)
        else:
            out = {'code':3, 'relative_resid_norm': inj_norm}
            return ("SDC", out)
           
