import re
import numpy
'''
    Quality Codes:
        - 0: Masked (allow energy to be off up to integer)
        - 1: incomplete result string (DDC)
        - 2: abnormal values (DDC)
        - 3: SDC
'''
def sdc_test(ofp):
    energy_gold = "2.025075e+05"
    with open(ofp) as out_f:
        ofc = out_f.read()

        energy_str = "\s+Final Origin Energy\s=\s+(\S+)"
        diff_str = "\s+MaxAbsDiff\s+=\s+(\S+)"
        try:
            energy = re.search(energy_str, ofc).group(1)
            max_diff = re.search(diff_str, ofc).group(1)
        except Exception as e:
            #print "[DDC] "+str(e) + " in " + ofp
            out = {'code': 1, 'reason': str(e)}
            return ("DDC", out)

        if not numpy.isfinite(float(energy)):
            out = {'code': 2, 'reason': "Abnormal value", 'energy': energy, 'MaxAbsDiff':max_diff}
            return ("DDC", out)

        good_energy = int(float(energy)) == int(float(energy_gold))

        if good_energy and float(max_diff) < 1e-08:
            out = {'code': 0, 'energy': energy, 'MaxAbsDiff':max_diff}
            return ("Masked", out)
        else:
            if not good_energy:
                out = {'code': 2, 'energy': energy, 'MaxAbsDiff':max_diff}
                return ("DDC", out)
            try:
                if (not numpy.isfinite(float(max_diff))) or float(max_diff) > 1.0:
                    out = {'code': 2, 'reason': "Abnormal value", 'energy': energy, 'MaxAbsDiff':max_diff}
                    return ("DDC", out)
                else:
                    diff_loc = (-1)*int(max_diff.split('e')[1])
                    out = {'code': 3, 'loc': diff_loc, 'MaxAbsDiff':max_diff}
                    return ("SDC", out)
            except Exception as e:
                print "FATAL! " + str(e) + " in " + ofp
