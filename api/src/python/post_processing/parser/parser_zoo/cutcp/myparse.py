import os
import sys
sys.path.append(os.environ['HAMARTIA_PARSER_PATH'])
from inject_parser import InjectParser

# application-specific verification function
from sdc_test import sdc_test

class CutcpInjectParser(InjectParser):

    def __init__(self):
        self.outfile = "inst.out"
        self.extra_outfile = "output.dat"
        super(CutcpInjectParser, self).__init__()

    def parse_non_DUE(self, cur_dir):
        out_f_path = os.path.join(cur_dir, self.outfile)
        extra_out_f_path = os.path.join(cur_dir, self.extra_outfile)
        return sdc_test(out_f_path, extra_out_f_path)

parser = CutcpInjectParser()

parser.run()
