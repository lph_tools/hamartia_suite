import numpy, re, os
'''
    Quality Code:
        - 0 : Masked
        - 1 : incomplete output (DDC)
        - 2 : SDC
'''
# Golden norm is based on nx=18 ny=16 nz=16
def sdc_test(out_f_path):
    verify_str = 'Detected'
    if not os.path.exists(out_f_path):
        out = {'code':1, 'reason': 'incomplete results'}
        return ("DDC", out)

    with open(out_f_path) as out_f:
        ofc = out_f.read()
        sdc = None
        try:
            sdc = re.search(verify_str, ofc)
        except Exception as e:
            print str(e) + " at " + out_f_path
            out = {'code':1, 'reason': 'incomplete results'}
            return ("DDC", out)
    
        if not sdc:
            out = {'code':0}
            return ("Masked", out)
        else:
            out = {'code':2}
            return ("SDC", out)
          
