import os
import sys
sys.path.append(os.environ['HAMARTIA_PARSER_PATH'])
from inject_parser import InjectParser

# application-specific verification function
from sdc_test import sdc_test

class LaghosInjectParser(InjectParser):

    def __init__(self):
        self.outfile = "inst.out"
        super(LaghosInjectParser, self).__init__()

    def parse_non_DUE(self, cur_dir):
        out_f_path = os.path.join(cur_dir, self.outfile)
        return sdc_test(out_f_path)

parser = LaghosInjectParser()

parser.run()
