import os, sys, re
import numpy

'''
    Analyze injection outcome based on stdout file
    The final energy is used to test SDC.
    Quality Codes:
        - 0: Masked (allow potential energy to be off up to two decimal points)
        - 1: incomplete result string (DDC)
        - 2: SDC
'''
golden_outputs = { # input mesh -> golden output
    'square01_quad.mesh' : '49.6955373491',  
    'cube01_hex.mesh' : '3390.1888816744',
    'box01_hex.mesh' : '5495.3710659424',
}        


def sdc_test(ofp):
    if not os.path.exists(ofp):
        out = {'code': 1, 'reason': 'no output'}
        return ("DDC", out)

    with open(ofp) as out_f:
        ofc = out_f.read()
        
        mesh_str = "\s+--mesh\s+(\S+)"
        # ste t dt |e|
        result_str = "step\s+\S+,\s+t\s+=\s+\S+,\s+dt\s+=\s+\S+,\s+\|e\|\s+=\s+(\S+)"
        try:
            mesh = os.path.basename(re.search(mesh_str, ofc).group(1).strip())
            energy = re.findall(result_str, ofc)[-1]
        except Exception as e:
            print str(e) + " in " + ofp
            out = {'code': 1, 'reason': str(e)}
            return ("DDC", out)
        
        golden_energy = golden_outputs.get(mesh, None)
        if golden_energy == None: 
            out = {'code': 1, 'reason': 'no output'}
            return ("DDC", out)

        elif golden_energy == energy:
            return ("Masked", None)

        else:
            abs_diff = abs(float(golden_energy) - float(energy))
            return ("SDC", abs_diff)
