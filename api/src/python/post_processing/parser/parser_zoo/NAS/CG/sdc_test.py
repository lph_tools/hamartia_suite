'''
    analyze injection outcome based on stdout file
'''
import re, numpy

golden_outputs = { # class -> golden output
    'A' : 0.1713023505403E+02,  
    'B' : 0.2271274548263E+02,
}        

class_str = "\s+Class\s+=\s+(\S+)"
success_str="VERIFICATION SUCCESSFUL"
out_str="\s+Zeta\s+(\S+)"

def sdc_test(ofp):
    with open(ofp) as out_f:
        ofc = out_f.read()
        data_class = ""
        try:
            data_class = re.search(class_str, ofc).group(1).strip()
            success = re.search(success_str, ofc)
            if success:
                return ("Masked", None)
        except Exception as e:
            print str(e) + " in " + ofp 
            return ("DDC", None)
        try:
            golden_out = golden_outputs.get(data_class, None)
            out= float(re.search(out_str, ofc).group(1))
            abs_diff = abs(out - golden_out)
            try:
                if (not numpy.isfinite(float(abs_diff))) or (not numpy.isfinite(out)):
                    print "[DDC] in " + ofp
                    return ("DDC", None)
                else:
                    return ("SDC", float(abs_diff))
            except Exception as e:
                print "FATAL! " + str(e) + " in " + ofp 
                return ("DDC", None)
        except Exception as e:
            print str(e) + " in " + ofp 
            return ("DDC", None)
 
