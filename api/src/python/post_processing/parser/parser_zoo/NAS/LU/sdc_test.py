import numpy, re
'''
    Analyze injection outcome using stdout file
'''

class_str = "\s+Class\s+=\s+(\S+)"
success_str = "\s+Verification\s+=\s+(\S+)"

def sdc_test(ofp):
    with open(ofp) as out_f:
        ofc = out_f.read()

        data_class = ""
        success = None
        try:
            success = re.search(success_str, ofc).group(1)
            data_class = re.search(class_str, ofc).group(1).strip()
        except Exception as e:
            print "[DDC]" + str(e) + " in " + ofp 
            return ("DDC", None)
        
        if success == 'SUCCESSFUL':
            return ("Masked", None)
        else:
            return ("SDC", None)

