import re
'''
    IS-specific test

    Note that we use a modified version of IS, which also checks
    if the sum of keys is the same as an error-free run.
    If not, mark as SDC. 
'''
golden_key_sum = {
        "A" :156860112,
        "B" :699490480,
}
success_str = "\s+Verification\s+=\s+(\S+)"
class_str = "\s+Class\s+=\s+(\S+)"

def sdc_test(ofp, line_num):

    with open(ofp) as out_f:
        ofc = out_f.read()

        data_class = ""
        try:
            success = re.search(success_str, ofc).group(1)
            data_class = re.search(class_str, ofc).group(1).strip()
        except Exception as e:
            print str(e) + " in " + ofp
            return ("DDC", "Missing output")

        if success == 'SUCCESSFUL':
            key_sum_str = "Key Sum:\s+(\S+)"
            try:
                keysum = int(re.search(key_sum_str, ofc).group(1))
                if keysum != golden_key_sum[data_class] and line_num != 416: # exclude the detector line
                    return ("SDC", "Wrong key sum")
                else:
                    return ("Masked", None)
            except Exception as e:
                print str(e) + " in " + ofp
                return ("DDC", "No key sum")
        else:
            return ("DDC", "Failed verification")
