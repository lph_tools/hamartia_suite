import numpy, re
'''
    Analyze injection outcome using stdout file
'''

golden_norms = { # class -> golden L2 norm
    'A' : 0.2433365309069E-05,  
    'B' : 0.1800564401355E-05,
}        

class_str = "\s+Class\s+=\s+(\S+)"
success_str="VERIFICATION SUCCESSFUL"
l2_norm_str="\s+L2 Norm is\s+(\S+)"

def sdc_test(ofp):
    with open(ofp) as out_f:
        ofc = out_f.read()

        data_class = ""
        try:
            success = re.search(success_str, ofc)
            l2_norm = float(re.search(l2_norm_str, ofc).group(1))
            data_class = re.search(class_str, ofc).group(1).strip()
        except Exception as e:
            print "[DDC]" + str(e) + " in " + ofp 
            return ("DDC", None)
        
        if success:
            return ("Masked", None)
        else:
            golden_l2_norm = golden_norms.get(data_class, None)
            abs_diff = abs(l2_norm - golden_l2_norm)
            try:
                if (not numpy.isfinite(float(abs_diff))) or (not numpy.isfinite(l2_norm)) or (l2_norm > 1.0):
                    #print "[DDC] in " + ofp
                    return ("DDC", None)
                else:
                    #print "[SDC] in " + ofp
                    return ("SDC", float(abs_diff))
            except Exception as e:
                print "FATAL! " + str(e) + " in " + ofp 
                return ("DDC", None)
 
