import numpy, re, os
'''
    Quality Code:
        - 0 : Masked
        - 1 : incomplete output (DDC)
        - 2 : software detected (DUE)
        - 3 : SDC
'''
def sdc_test(out_f_path, out_f_path2):
    detected_str ='===Error Detected==='
    golden_out =  'out_4.txt'
    if not os.path.exists(out_f_path):
        out = {'code':1, 'reason': 'incomplete results'}
        return ("DDC", out)

    with open(out_f_path) as out_f:
        ofc = out_f.read()
        detected = re.search(detected_str, ofc)
        if detected:
            out = {'code':2, 'reason': 'software detected'}
            return ("DUE", out)

    if not os.path.exists(out_f_path2):
        out = {'code':1, 'reason': 'incomplete results'}
        return ("DDC", out)

    with open(out_f_path2) as out_f, open(golden_out) as golden_f:
        if out_f.read() == golden_f.read():
            out = {'code':0}
            return ("Masked", out)
        else:
            out = {'code':3}
            #print out_f_path2
            return ("SDC", out)
