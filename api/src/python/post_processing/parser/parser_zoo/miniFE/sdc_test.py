import numpy, re, os
'''
    Quality Code:
        - 0 : Masked
        - 1 : incomplete output (DDC)
        - 2 : abnormal norm (DDC)
        - 3 : SDC
'''
# Golden norm is based on nx=64 ny=64 nz=64 verify_solution=1
def sdc_test(out_f_path):
    norm_str ='Final Resid Norm: (\S+)'
    verify_str = 'solution matches'
    if not os.path.exists(out_f_path):
        out = {'code':1, 'reason': 'incomplete results'}
        return ("DDC", out)

    with open(out_f_path) as out_f:
        ofc = out_f.read()
        inj_norm = ""
        success = None
        try:
            success = re.search(verify_str, ofc)
            inj_norm = re.search(norm_str, ofc).group(1)
        except Exception as e:
            print str(e) + " at " + out_f_path
            out = {'code':1, 'reason': 'incomplete results'}
            return ("DDC", out)

        if not numpy.isfinite(float(inj_norm)) or float(inj_norm) > 1.0:
            out = {'code':2, 'resid_norm': inj_norm}
            return ("DDC", out)

        # original test in miniFE
        if success:
            out = {'code':0, 'resid_norm': inj_norm}
            return ("Masked", out)
        else:
            out = {'code':3, 'resid_norm': inj_norm}
            return ("SDC", out)

        """ 
        # test based on final norm
        golden_norm =  '4.15881e-15'
        abs_diff = abs(float(inj_norm) - float(golden_norm))
        rel_err = abs_diff / float(golden_norm)

        if rel_err < 1e-6:
            out = {'code':0, 'resid_norm': inj_norm}
            return ("Masked", out)
        else:
            out = {'code':3, 'resid_norm': inj_norm}
            return ("SDC", out)
        """   
