import numpy, re, os
'''
    Quality Code:
        - 0 : Masked
        - 1 : incomplete output (DDC)
        - 2 : software detected (DUE)
        - 3 : SDC
'''
def sdc_test(out_f_path, out_f_path2):
    if not os.path.exists(out_f_path):
        out = {'code':1, 'reason': 'incomplete results'}
        return ("DDC", out)

    if not os.path.exists(out_f_path2):
        out = {'code':1, 'reason': 'incomplete results'}
        return ("DDC", out)

    with open(out_f_path2) as out_f:
        lines = out_f.readlines()
        result_line = lines[-1] # result line is the last line
        results = []
        golden_results = [5.000000e-01, -2.14915340551713546e-01,  6.31777967063682766e+00, -1.77662823905605149e+00]
        try:
            results = [float(x) for x in result_line.split()]
        except:
            out = {'code':1, 'reason': 'invalid output'}
            return ("DDC", out)

        if len(results) != len(golden_results):
            out = {'code':1, 'reason': 'incomplete results'}
            return ("DDC", out)

        for x in results:
            if not numpy.isfinite(x):
                out = {'code':1, 'reason': 'invalid output'}
                return ("DDC", out)

        max_err = 0.0
        success = True
        for i, r in enumerate(results):
            g = golden_results[i]
            rel_err = abs(r - g) / abs(g)
            if rel_err > 1e-10:
                success = False 
                max_err = rel_err if rel_err > max_err else max_err
        
        if success:
            out = {'code':0}
            return ("Masked", out)
        else:
            out = {'code':3, 'rel_diff': max_err}
            return ("SDC", out)
