import os
import sys
sys.path.append(os.environ['HAMARTIA_PARSER_PATH'])
from inject_parser import InjectParser

# application-specific verification function
from sdc_test import sdc_test

class SW4InjectParser(InjectParser):

    def __init__(self):
        self.outfile = "inst.out"
        self.outfile2 = "pointsource-h0p04/PointSourceErr.txt"
        super(SW4InjectParser, self).__init__()

    def parse_non_DUE(self, cur_dir):
        out_f_path = os.path.join(cur_dir, self.outfile)
        out_f_path2 = os.path.join(cur_dir, self.outfile2)
        return sdc_test(out_f_path, out_f_path2)

parser = SW4InjectParser()

parser.run()
