import numpy, re, os
'''
    Quality Code:
        - 0 : Masked
        - 1 : incomplete output (DDC)
        - 2 : software detected (DUE)
        - 3 : SDC
'''
def sdc_test(stdout_f_path, out_f_path):
    detected_str = '===Error Detected==='
    golden_out =  'golden_output.dat'

    if not os.path.exists(stdout_f_path):
        out = {'code':1, 'reason': 'incomplete results'}
        return ("DDC", out)

    with open(stdout_f_path) as std_out_f, open(golden_out) as golden_f:
        sfc = std_out_f.read() 
        detected = re.search(detected_str, sfc)
        if detected:
            out = {'code':2, 'reason': 'software detected'}
            return ("DUE", out)

        if not os.path.exists(out_f_path):
            out = {'code':1, 'reason': 'incomplete results'}
            return ("DDC", out)

        with open(out_f_path) as out_f:
            # somehow detected message can go into the output file instead stdout
            sfc = out_f.read() 
            detected = re.search(detected_str, sfc)
            if detected:
                out = {'code':2, 'reason': 'software detected'}
                return ("DUE", out)

            if sfc == golden_f.read():
                out = {'code':0}
                return ("Masked", out)
            else:
                out = {'code':3}
                return ("SDC", out)
