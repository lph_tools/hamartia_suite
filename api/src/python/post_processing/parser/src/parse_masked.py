import yaml

def parse_masked(log_path):
    with open(log_path) as log_f:
        try:
            doc = yaml.load(log_f)
        except:
            print "Broken 'masked.yaml' at {}".format(log_path)
            return 0, 0

        if not doc: 
            return 0, 0

        num_masked = 0
        num_same_val_pairs = 0
        if 'next_point' in doc:
            num_masked = doc['next_point'] - 1
        if 'num_same_val_pairs' in doc:
            num_same_val_pairs = int(doc['num_same_val_pairs'])

        return num_masked, num_same_val_pairs
