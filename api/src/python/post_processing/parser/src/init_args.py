import argparse

def init_args():
    parser = argparse.ArgumentParser()
    # these are common arguments 
    # app-specific parser might want to add more arguments
    parser.add_argument('-a', dest='app', required=True, help='Application name')
    parser.add_argument('-d', dest='rootdir', required=True, help='Root directory of all data (the "iter" folder)')
    parser.add_argument('-e', dest='emodel', required=True, help='Error model name')
    parser.add_argument('-p', dest='prof_yaml', help='YAML file of profiling phase')
    parser.add_argument('-s', dest='target_samples', default=3000, help='Number of target samples')
    parser.add_argument('-b', dest='bus_error', default=False, help='Ignore samples with bus errors')
    parser.add_argument('-o', dest='o_name', default='out.json', help='Output file name')
    return parser
