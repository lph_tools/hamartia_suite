import os
import json
from abc import ABCMeta, abstractmethod # for creating abstract classes

from static import *
from init_args import init_args
from parse_stderr import parse_stderr, parse_masked_points, has_bus_error
from parse_errlog import parse_errlog
from parse_masked import parse_masked
from parse_info import parse_duration
'''
    Basic parser that walks through directory of injection results
    and then dumps a JSON file

'''
class InjectParser():
    __metaclass__ = ABCMeta
    '''
        This is an abstract class, the user must implement the following methods before instantiation:
            - parse_non_DUE()
            - (optional) parse_sw_detect()
        See below for spec of each method 

        Once the above methods are implemented, the only method that should be called is run()
        which takes an optional function object to insert extra command line arguments
            
    '''
    def __init__(self):
        self.parser = init_args()
        self.args = None
        self.dirCount = 0 # counter of how many directories we have already traversed
        self.num_masked = 0 # point injected but maksed
        self.num_same_val_pairs = 0 # number of value pairs with identical operands
        self.masked_points = [] # list to store all points that are masked
        self.db = [] # database to store all parsed injection samples
        self.exceptions = []
        self.tmpSample = {} # temporary sample info
        self.parseLog = "parse.log"

    def run(self, extra_args=None):
        if extra_args:
            extra_args(self.parser)
        self.set_args()
        self.dir_walk()
        self.dump_results()

    @abstractmethod 
    def parse_non_DUE(self, cur_dir):
        '''
            This app-specific method further differentiates non-DUE cases
            and returns a tuple (outcome, quality) where
                outcome is the outcome string
                quality is the application output quality (None for non-SDC cases)
        '''
        pass

    def parse_sw_detect(self, cur_dir):
        '''
            This app-specific method checks other DUE cases
            e.g., some may generate a failure log
            It should returns None if non-DUE; otherwise, a str to explain why it is a DUE
        '''    
        return None

    ####################
    # End of Interface #
    ####################


    def set_args(self):
        # this is called after app-specific parsing arguments are appended (if any)
        self.args = self.parser.parse_args()

    def dir_walk(self):
        assert self.args, "Did you call set_args() before calling dir_walk()?"

        for dirPath, subdirList, fileList in os.walk(self.args.rootdir):
            self.dirCount += 1
            if self.dirCount == 1: #skip root dir
                continue
            # Skip if log file does not exist (failed injection sample)
            if not os.path.exists(os.path.join(".", dirPath + "/" + logfile_str)):
                self.dirCount -= 1
                continue
            # Early termination if we have enough samples
            if self.dirCount - 1 >= self.args.target_samples:
                break

            sample_dict = self.parse_files(dirPath)
            if not sample_dict: # discard this sample
                self.dirCount -= 1
                continue
            self.db.append(sample_dict) 
  
    def dump_results(self):
        final_db = {}
        #TODO: add input data information and compiler info?
        final_db['application'] = self.args.app
        final_db['model'] = self.args.emodel
        final_db['samples'] = self.db
        final_db['masked_points'] = self.masked_points
        final_db['total_num_masked'] = self.num_masked
        final_db['num_same_val_pairs'] = self.num_same_val_pairs

        with open(self.args.o_name, "w") as f:
            json.dump(final_db, f, indent=4)    
            print "{} samples dumped to {}".format(len(self.db), self.args.o_name)
        if self.exceptions:
            with open(self.parseLog, "w") as f:
                for exp in self.exceptions:
                    f.write(exp)
            print "Exceptions detected. Check {} for details".format(self.parseLog)        

    def parse_files(self, cur_dir):
        sample_dict = {}
        sample_dict["dir"] = cur_dir

        # parse duration
        info_path = os.path.join(".", cur_dir + "/" + infofile_str)
        if os.path.exists(info_path):
            sample_dict["duration"] = parse_duration(info_path)
        
        # parse maked error file (overprovisioning mode)
        masked_path = os.path.join(".", cur_dir + "/" + maskedfile_str)
        if os.path.exists(masked_path):
            num_masked, num_same_val_pairs = parse_masked(masked_path)
            sample_dict["num_masked"] = num_masked
            self.num_masked += num_masked
            self.num_same_val_pairs += num_same_val_pairs

        # parse error log file
        is_rtl = True if self.args.emodel.startswith("RTL") else False
        log_path = os.path.join(".", cur_dir + "/" + logfile_str) 
        ignored = parse_errlog(cur_dir, log_path, sample_dict, is_rtl) # in-place update sample_dict
        if ignored:
            stderr_path = os.path.join(".", cur_dir + "/" + errfile_str) 
            self.num_masked += 1 # add 1 to count the injected but masked fault
            # Log the exception and discard the sample
            exp_str = "Igonred at {}\n".format(log_path)
            self.exceptions.append(exp_str)
            return None
       
        self.tmpSample = sample_dict # stash parsed sample info because some app-specific tests might need them

        # parse outcome
        try:
            stderr_path = os.path.join(".", cur_dir + "/" + errfile_str) 
            if self.args.bus_error and has_bus_error(stderr_path):
                return None
            inj_point, DUE_reason = parse_stderr(stderr_path)
            sample_dict["dyn_inst"] = int(inj_point)
            sw_detected_reason = self.parse_sw_detect(cur_dir)
            if sw_detected_reason:
                DUE_reason = sw_detected_reason

            if DUE_reason: # DUE cases
                sample_dict["outcome"] = "DUE"
                sample_dict["outcome_reason"] = DUE_reason
            else:    
                outcome, quality = self.parse_non_DUE(cur_dir)
                sample_dict["outcome"] = outcome
                if quality:
                    sample_dict["quality"] = quality
        except Exception as e:
            # Log the exception and discard the sample
            exp_str = "{} at {}\n".format(str(e), cur_dir)
            self.exceptions.append(exp_str)
            return []

        return sample_dict    
 
