import yaml

def parse_duration(info_path):
    with open(info_path) as f:
        try:
            doc = yaml.load(f)
        except:
            print "Broken 'info.yaml' at {}".format(info_path)
            return 0

        if not doc: 
            return 0
        try:
            return doc['duration']
        except:
            print "Broken 'info.yaml' at {}".format(info_path)
            return 0
