import re

inst_str = "at filtered instr # (\S)+"
crash_checks = [
"Caught Fatal Signal...",
"Segmentation",
"Bus error",
"TIME LIMIT",
"Aborted"
]
all_detect_str = "All Detected"
'''
    Parse stderr file

    Return (inj_point, DUE_reason) 

    where inj_point is a int
          DUE_reason is a str (None means not a DUE)
'''
def parse_stderr(err_path):
    inj_point = 0
    DUE_reason = None
    with open(err_path) as err_f:
        efc = err_f.read()
        assert (len(efc) > 0), "Empty inst.err file!"
        #parse injection point
        matches = re.finditer(inst_str, efc)
        insts = [m.group().split("#")[1].strip() for m in matches]
        inj_point = int(insts[-1])
        #parse outcome
        all_detect = re.search(all_detect_str, efc)
        crash = False
        for c in crash_checks:
            if re.search(c, efc):
                crash = True
                break
        if all_detect:
            DUE_reason = "All_detected"
            #print "[DUE] All detected at " + err_path
        elif crash:
            DUE_reason = "OS/SW_detected"
            #print "[DUE] SW/OS detected at " + err_path
        return (inj_point, DUE_reason)

def parse_masked_points(err_path):
    inj_point = 0
    with open(err_path) as err_f:
        efc = err_f.read()
        assert (len(efc) > 0), "Empty inst.err file!"
        #parse injection point
        matches = re.finditer(inst_str, efc)
        insts = [m.group().split("#")[1].strip() for m in matches]
        return [int(i) for i in insts]

def has_bus_error(err_path):
    sigbus_str = "SIGBUS"
    with open(err_path) as err_f:
        efc = err_f.read()
        assert (len(efc) > 0), "Empty inst.err file!"
        sigbus = re.search(sigbus_str, efc)
        if sigbus:
            return True
        return False

