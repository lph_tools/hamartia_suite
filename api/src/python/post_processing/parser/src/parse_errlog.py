import yaml
import os
import re

from fp_convert import fp2uint
'''
    Parse Error Log File

    Apply *In-place* update to sample_dict

    In some cases, the error log file may be corrupted (mostly when ran on LS5)
    This function also tries to fix the problem

    On normal runs, this function has no return value.
    If it returns True, client should discard this sample.

'''
def parse_errlog(cur_dir, log_path, sample_dict, is_rtl):

    last_field = "recorded_injections:"
    ending_str = "  []\n"
    ignored = True


    def parse_operands(op_list):
        tmp = []
        for i in op_list:
            op = {}
            op["width"] = i["width"]
            op["value"] = i["value"][0]
            tmp.append(op)
        return tmp

    # parse log file (error statistics)
    has_yaml_issue = False
    tmp = os.path.join(".", cur_dir + "/" + "tmp.out")
    with open(log_path) as log_f:
        try:
            doc = yaml.load(log_f)
        except:
            has_yaml_issue = True
            print "try to fix trailing data problem at {}".format(log_path)
            # keep reading until we reach the last field and then truncate the rest
            # FIXME: this assume "recorded_injections" field is empty
            log_f.seek(0) # rewind to begin
            d = log_f.readlines()
            with open(tmp, "w") as of:
                for l in d:
                    of.write(l)
                    if re.search(last_field, l):
                        of.write(ending_str)
                        break
            # parse again
            with open(tmp) as tmp_f:
                doc = yaml.load(tmp_f)
                
        if not doc:
            #print "Empty error log at {}".format(log_path)
            return ignored
        # parse assembly line
        asm = doc["instruction"]["asm_line"]
        op = asm.split(" ")[0] #dump operands
        sample_dict["op_type"] = op
        sample_dict["data_type"] = doc["instruction"]["data_type"]
        sample_dict["operation"] = doc["instruction"]["instruction"]["operation"]
        sample_dict["pc"] = doc["instruction"]["address"]["virtual"]
        sample_dict["asm"] = asm

        # parse location

        try:
            sample_dict["image"] = doc["location"].get("image", None)
            ## if app is not compiled with debug flag, we won't know source code info
            sample_dict["file"] = doc["location"].get("file", None)
            sample_dict["line"] = doc["location"].get("line", None)
            rtn = doc["location"]["routine"]
            func_name = rtn.split("(")[0] #dump function parameters
            sample_dict["function"] = func_name
        except Exception as e:
            print "[Error] parsing location at {}".format(log_path)
            assert 0

        # parse operands
        init_operands = doc["initial_operands"]
        inputs = init_operands["inputs"]
        sample_dict["inputs"] = parse_operands(inputs)

        outputs = init_operands["outputs"]
        sample_dict["outputs"] = parse_operands(outputs)

        # in-place floating-point conversion (from human readable to bits)
        # human readable format of each op is stored in "human_value" instead
        fp2uint(sample_dict)

        # parse rtl iteration (for backward compatability)
        if is_rtl:
            # an older version has a different log layout
            try:
                sample_dict["rtl_module"] = doc["misc"]["rtl"]["module"]
                sample_dict["rtl_iter"] = int(doc["misc"]["rtl"]["iterations"])
                ##possibly > 0 only if detector is used
                sample_dict["rtl_filtered"] = int(doc["misc"]["rtl"]["filtered"])
            except:
                sample_dict["rtl_module"] = doc["models"]["rtl"]["module"]
                sample_dict["rtl_iter"] = int(doc["models"]["rtl"]["iterations"])
                ##possibly > 0 only if detector is used
                sample_dict["rtl_filtered"] = int(doc["models"]["rtl"]["filtered"])

        # model-specific logs
        if doc.get("misc", None):
            sample_dict["misc"] = doc["misc"]

        # parse output difference in bits (xor)
        try:
            sample_dict["diff_mask"] = [str(lane["xor"]) for lane in doc["difference"]["outputs"][0]["diff"]]
            sample_dict["affected_structure"] = doc["difference"]["outputs"][0]["name"]
        except Exception as e:
            try:
                sample_dict["diff_mask"] = str(doc["difference"]["outputs"][0]["xor"])
                sample_dict["affected_structure"] = doc["difference"]["outputs"][0]["name"]
            except Exception as e:
                # Special case
                print "[Ignored]" + str(e)+" in " + log_path
                return ignored

        # parse detector coverage
        try:
            detector = doc["models"]["detector"]
            if detector:
                sample_dict["detector"] = detector
                sample_dict["num_iter"] = doc["statistics"]["iterations"]
                sample_dict["num_detected"] = doc["statistics"]["tp_detected"]
        except:
            pass

        # parse multiple injection per run
        if doc["recorded_injections"]:
            sample_dict["num_errors"] = len(doc["recorded_injections"]) + 1
        else:
            sample_dict["num_errors"] = 1

        #TODO: get address if the inst uses memory?

    if has_yaml_issue :
        # replace fixed log file
        os.rename(tmp, log_path)
