# Generic Parser for Error Injection Results

This serves as a generic script for parsing error injections experiments
initiated by the automatic launcher in Hamartia Suite (*launcher.py*).

The common operations are already implemented within a base parser,
including directory-tree walking and checks for DUE. 
The user only has to implement an application-specific method to
differentiate the non-DUE cases.

## How to Use
1. Run `source set_env.sh` to set a env variable that points to this directory
2. Implement a derived parser (See `example.py`)
3. Run the parser (`python example.py <args>`)

Note that there are some required CLI arguments (i.e., `<args>` above) that 
the user has to pass to the script. The list is in `src/init_args.py`.

See `parser_zoo` for more examples.
