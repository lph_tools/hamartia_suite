'''
    example implementation of application-specific checks for SDC

    In this example, we check SDC based on some output file.
    Some applications may need to parse multiple files to determine SDC.

    See 'parser_zoo' directory for more examples.
'''

def sdc_test(out_file_path):
    # implementation goes here
    pass
