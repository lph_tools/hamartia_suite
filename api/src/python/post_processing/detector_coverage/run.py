#! /usr/bin/env python

import os, sys, argparse, tempfile, yaml, json
from scipy import stats
import numpy as np
import subprocess
import multiprocessing
import time

TASK_SCRIPT = 'subsampling.py'

RESIDUE_CHECKER_FILTER = {
    'data_type' : ['DT_INT', 'DT_UINT'],
    'operation' : ['OP_ADD','OP_SUB','OP_MUL'],
    'all_detect': ['RANDBIT1'],
}

FILTERS = {
    'MODULO3' : RESIDUE_CHECKER_FILTER,
    'MODULO3_5' : RESIDUE_CHECKER_FILTER,
}

def sample_filter(samples, config):
    '''
        Obtain samples able to evaluate the specified detector
    '''
    err_model = str(config['error_model'])
    detector = str(config['detector_model'])
    dfilter = FILTERS.get(detector, None)

    if err_model == 'RTL' and not dfilter: # only RTL can go without any detector (no filtering)
        return samples

    assert dfilter, 'Invalid detector!'
    
    if err_model in dfilter['all_detect']:
        print "{} has 100% coverage for {}.".format(detector, err_model)
        sys.exit(0)

    supported_datatype = dfilter['data_type']
    filtered_samples = [s for s in samples if s['data_type'] in supported_datatype]
    supported_operation = dfilter['operation']
    filtered_samples = [s for s in filtered_samples if s['operation'] in supported_operation]
        
    print "Collect {} samples for reinjection".format(len(filtered_samples))
    return filtered_samples

def sample_index_generator(num_samples, num_workers):
    if num_samples < num_workers:
        batch_size = 1
    else:
        batch_size =  int(num_samples / num_workers)
    print "Sample size: {}".format(num_samples)
    print "Sample batch size: {}".format(batch_size)

    start = 0
    while start < num_samples:
        yield (str(start), str(start + batch_size))
        start += batch_size

def _launch_new_process(args, argument_generator):
    begin_idx, end_idx = next(argument_generator)
    
    process_args = args + ['-b', begin_idx, '-e', end_idx]
    print "Launching '%s'" % (" ".join(process_args))
    #child_process = subprocess.Popen(process_args, stdout=subprocess.PIPE, stderr=subprocess.PIPE, cwd=tempfile.mkdtemp())
    child_process = subprocess.Popen(process_args, cwd=tempfile.mkdtemp())
    #out, err = child_process.communicate()
    #print out
    #print err
    return child_process

def launch_fixed_processes(process_count, base_args, argument_generator):
    active_child_processes = []

    while True:
        active_child_processes = _filter_processes_still_active(active_child_processes)
        if len(active_child_processes) < process_count:
            try:
                child_process = _launch_new_process(base_args, argument_generator)
            except StopIteration:
                break
            active_child_processes.append(child_process)

    return_codes = [process.poll() for process in active_child_processes]
    while None in return_codes:
        time.sleep(1) # sleep for 1 second and then check again
        return_codes = [process.poll() for process in active_child_processes]
    

def _filter_processes_still_active(processes):
    return [process for process in processes if process.poll() is None] # None means the process hasn't terminated

def compute_mean_and_confidence(samples, prob=0.95):
    mean = np.mean(samples)
    std = np.std(samples)
    num_samples = len(samples)
    ci_interval = stats.norm.interval(prob, loc=mean, scale=std/np.sqrt(num_samples))
    return (mean, ci_interval)

def compute_per_op_coverage(inj_count_dict):
    print "Dector Coverage"
    print "{:<12}{:<12}{}".format("Operation","Mean","95% Confidence Interval")
    # Per operation statistics
    total_counts = []
    for op, op_counts in inj_count_dict.iteritems():
        # 0 means infinity here, because float('inf') is not a valid JSON value 
        coverages = [1.0 if oc==0 else (1 - 1./oc) for oc in op_counts]
        if np.average(coverages) == 1.0:
            print "{:<12}{:<12.3f}({:.3f}, {:.3f})".format(op, 1.0, 1.0, 1.0)
        else:
            mean, ci_interval = compute_mean_and_confidence(coverages)
            print "{:<12}{:<12.3f}({:.3f}, {:.3f})".format(op, mean, ci_interval[0], ci_interval[1])
        total_counts.extend(op_counts)
    # Overall statistics
    coverages = [1.0 if oc==0 else (1 - 1./oc) for oc in total_counts]
    if np.average(coverages) == 1.0:
        print "{:<12}{:<12.3f}({:.3f}, {:.3f})".format('All', 1.0, 1.0, 1.0)
    else:
        mean, ci_interval = compute_mean_and_confidence(coverages)
        print "{:<12}{:<12.3f}({:.3f}, {:.3f})".format('All', mean, ci_interval[0], ci_interval[1])
 
def compute_rtl_masking_factor(rtl_iter_dict):

    def calc_and_dump(counts, group_name):
        if not counts:
            return
        # 0 means infinity here, because float('inf') is not a valid JSON value 
        coverages = [1.0 if oc==0 else (1 - 1./oc) for oc in counts]
        if np.average(coverages) == 1.0:
            print "{:<12}{:<12.3f}({:.3f}, {:.3f})".format(group_name, 1.0, 1.0, 1.0)
        else:
            mean, ci_interval = compute_mean_and_confidence(coverages)
            print "{:<12}{:<12.3f}({:.3f}, {:.3f})".format(group_name, mean, ci_interval[0], ci_interval[1])
 
    if not rtl_iter_dict:
        return
    print "RTL Logical Masking Factor"
    print "{:<12}{:<12}{}".format("Circuits","Mean","95% Confidence Interval")

    total_counts = []
    int_counts = []
    fp_counts = []
    # Per circuit statistics
    for op, op_counts in rtl_iter_dict.iteritems():
        calc_and_dump(op_counts, op)
        total_counts.extend(op_counts)
        if op.startswith('fp'):
            fp_counts.extend(op_counts)
        else:
            int_counts.extend(op_counts)

    # Per data type statistics (INT and FP)
    calc_and_dump(int_counts, 'INT')
    calc_and_dump(fp_counts, 'FP')

    # Overall statistics
    calc_and_dump(total_counts, 'All')


def dump_output(inj_count_dict, rtl_iter_dict, inj_out_val, out_path):
    output = {
        'num_error_till_undetected' : inj_count_dict,
        'num_rtl_iter_till_masked': rtl_iter_dict,
        'inj_out_loc_and_value': inj_out_val,
    }
    with open(out_path, "w") as of:
       json.dump(output, of) 

def reduction(path):
    reduced_inj_dict = {}
    reduced_rtl_iter_dict = {}
    reduced_inj_out = []

    with open(path) as f:
        for line in f:
             tmp_dict = json.loads(str(line))
             # reduce injection count dict
             for k, v in tmp_dict['inj_count'].iteritems():
                if k in reduced_inj_dict:
                    reduced_inj_dict[k].extend(v)
                else:
                    reduced_inj_dict[k] = v
             # reduce rtl iter dict
             if tmp_dict['rtl_iter']:
                for k, v in tmp_dict['rtl_iter'].iteritems():
                   if k in reduced_rtl_iter_dict:
                       reduced_rtl_iter_dict[k].extend(v)
                   else:
                       reduced_rtl_iter_dict[k] = v
             # reduce injection output value
             if tmp_dict['inj_out']:
                reduced_inj_out.extend(tmp_dict['inj_out'])


    return (reduced_inj_dict, reduced_rtl_iter_dict, reduced_inj_out)

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('-c', dest='config_file', required=True, help='YAML file for configuring the reinjection')
    parser.add_argument('-o', dest='out_file', default='', help='Output file path')
    args = parser.parse_args()
    
    with open(args.config_file) as cf:
        config = yaml.load(cf)
        input_path = config['input_path']
        
        # Initialize default output file
        if not args.out_file:
            args.out_file = "{}_{}_{}_coverage.json".format(config['app_name'], config['error_model'], config['detector_model'])
        if os.path.exists(args.out_file):
            os.remove(args.out_file)

        # Get the number processes to run in parallel 
        num_parallel = config.get('num_in_parallel', 1)
        max_cpu_count = multiprocessing.cpu_count()
        if num_parallel == 'MAX':
            num_parallel = max_cpu_count
        assert num_parallel <= max_cpu_count, "Asking {} cores but you only have {} cores".format(num_parallel, max_cpu_count)

        with open(input_path) as f_json:
            # Get samples with supported detection methods
            result = json.loads(f_json.read())
            samples = result['samples']
            target_samples = sample_filter(samples, config)
            samples_json = 'tmp.json'
            with open(samples_json, "w") as tmp_f:
                json.dump(target_samples, tmp_f)

            # Init sample generator
            sample_idx_generator = sample_index_generator(len(target_samples), num_parallel)

            # Start multiprocessing
            def make_abs(this_path):
                """ Update the path to be absolulte """
                return os.path.abspath(this_path) if this_path is not None else None

            # Make file paths absolute since we will run tasks in parallel
            base_args = ['python', make_abs(TASK_SCRIPT), 
                         '-c', make_abs(args.config_file), 
                         '-s', make_abs(samples_json), 
                         '-p', os.path.dirname(os.path.realpath(__file__)),
                         '-o', make_abs(args.out_file)
                        ]

            start_time = time.time()

            # Master-Slave pattern
            # Assign tasks to slaves (blocked until all tasks are finished)
            launch_fixed_processes(num_parallel, base_args, sample_idx_generator)
            print "Experiments done"
        
            # Master reduces the results
            inj_count, rtl_iter, inj_out = reduction(make_abs(args.out_file))
            compute_per_op_coverage(inj_count)
            compute_rtl_masking_factor(rtl_iter)
            dump_output(inj_count, rtl_iter, inj_out, make_abs(args.out_file))

            # Cleanup 
            os.remove(samples_json)

            end_time = time.time()
            print time.strftime("%H:%M:%S", time.gmtime(end_time - start_time))
if __name__ == '__main__':
    main()
