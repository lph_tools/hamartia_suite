#! /usr/bin/env python
'''
    Collect Operands from Error Injection Results
    
    Output of this script can be later used to re-inject errors 
    for sampling detector coverage and masking factor of RTL
    gate-level model.
'''

import os, sys, argparse
import yaml, json
from fp_convert import *

def dump_json(args, db):
    final_db = {}
    final_db['application'] = args.app
    final_db['samples'] = db
    with open(args.o_name, "w") as f:
        json.dump(final_db, f, indent=4)  
 
def parse_operands(op_list):
    tmp = []
    for i in op_list:
        op = {}
        op["width"] = i["width"]
        op["value"] = i["value"][0]
        tmp.append(op)
    return tmp


parser = argparse.ArgumentParser()
parser.add_argument('-a', dest='app', required=True, help='Application name')
parser.add_argument('-d', dest='rootdir', required=True, help='Root directory of all data(target "iter")')
parser.add_argument('-o', dest='o_name', default='operands.json', help='Output file name')
args = parser.parse_args()
    

logfile_str = "err_prof_inj.out"

#walk through the directory tree
dirCount = 0
db = []
for dirPath, subdirList, fileList in os.walk(args.rootdir):
    dirCount += 1
    if dirCount == 1: #skip root dir
        continue
    # Skip if log file does not exist (failed injection sample)
    logfile_path = os.path.join(".", dirPath + "/" + logfile_str)
    if not os.path.exists(logfile_path):
        dirCount -= 1
        continue
   
    sample_dict = {}
    # parse log file (error statistics)
    with open(logfile_path) as log_f:
        doc = yaml.load(log_f)
        if not doc:
            dirCount -= 1
            continue
        # parse instruction static info
        instr = doc["instruction"]
        sample_dict["asm_line"] = instr["asm_line"]
        sample_dict["operation"] = instr["instruction"]["operation"]
        sample_dict["data_type"] = doc["instruction"]["data_type"]
        sample_dict["file"] = doc["location"].get("file", None)
        sample_dict["line"] = doc["location"].get("line", None)
    
        # parse operands
        init_operands = doc["initial_operands"]
        inputs = init_operands["inputs"]
        sample_dict["inputs"] = parse_operands(inputs)
           
        outputs = init_operands["outputs"]
        sample_dict["outputs"] = parse_operands(outputs)

        # in-place floating-point conversion (from human readable to bits)
        fp2uint(sample_dict)

        #parse rtl iteration
        if doc["models"]["error"].startswith("RTL"):
            # I update the yaml output recently so the RTL info may be in models portion in older runs
            try:
                sample_dict["rtl_module"] = doc["misc"]["rtl"]["module"]
                sample_dict["rtl_iter"] = int(doc["misc"]["rtl"]["iterations"])
            except:
                sample_dict["rtl_module"] = doc["models"]["rtl"]["module"]
                sample_dict["rtl_iter"] = int(doc["models"]["rtl"]["iterations"])

        #parse detector coverage
        try:
            detector = doc["models"]["detector"]
            if detector:
                sample_dict["detector"] = detector
                sample_dict["num_detected"] = doc["statistics"]["tp_detected"]
        except:
            pass


    db.append(sample_dict)

#dump results
print "Parsed {} samples".format(len(db))
dump_json(args, db)
