import os, sys, argparse
import json
import numpy as np
def get_injector(err_model, inj_config, src_path):
    if err_model.startswith('RANDBIT'): # RANDBIT{x} where x is num bits to flip
        num_bit_flip = int(err_model.split('RANDBIT')[1])
        return RandbitInjector(num_bit_flip)
    elif err_model == 'RANDOM':
        return RandomInjector()
    elif err_model == 'RTL':
        model_args = inj_config['error_model_args']
        injector_path = model_args.get('injector_path', None)
        config_path = os.path.join(src_path, model_args.get('config_path', None))
        module_list_path = os.path.join(src_path, model_args.get('module_list_path', None))

        assert os.path.exists(injector_path), "Invalid RTL injector path"
        assert os.path.exists(config_path), "Invalid error config path"
        assert os.path.exists(module_list_path), "Invalid module list path"

        return RTLInjector(injector_path, config_path, module_list_path)

    else:
        print "error_model {} is not supported.".format(err_model)
        sys.exit(1)

def get_detector(detector_model):

    if detector_model.startswith('MODULO'): # MODULO{x}_{y} where x and y are moduli
        print "Use residue checker"
        try:
            modulo = int(detector_model.split('MODULO')[1])
            return ResidueChecker([modulo])
        except:
            moduli = detector_model.split('MODULO')[1].split('_')
            moduli = [int(m) for m in moduli]
            return ResidueChecker(moduli)
    else:
        print "Use dummy checker (i.e., no error dection)"
        return DummyChecker()

def init_inj_count_dict(samples):
    my_dict = {}
    ops = [s['operation'] for s in samples]
    op_set = set(ops)
    for op in op_set:
        my_dict[op] = []
    return my_dict

def subsampling(samples, config, src_path, worker_id):
    err_model = str(config['error_model'])
    injector = get_injector(err_model, config, src_path)
    detector_model = str(config['detector_model'])
    detector = get_detector(detector_model)
    sample_size = config['sample_size']    

    # Init a dict to store injection iterations for each type of operation
    all_inj_count_dict = init_inj_count_dict(samples)
    all_inj_out = []

    # For each sample, we keep injecting until we get `sample_size` undetected errors eventually
    # Iteration count for each opteration type is recorded in `all_inj_count_dict`
    num_processed = 0 # as a heartbeat
    for s in samples:
        undetected_count = 0
        inj_count_list = []
        inj_count = 0
        output_width = s['outputs'][0]['width']
        max_inj_count = output_width * 4
        inj_out_dict = {} # the injected value
        inj_out_dict['line'] = s.get('line', None)
        inj_out_dict['file'] = s.get('file', None)
        inj_out_dict['asm_line'] = s.get('asm_line', None)
        inj_out_dict['data_type'] = s.get('data_type', None)
        inj_out_dict['golden_output'] = s['outputs'][0]['value']
        inj_out_dict['err_output'] = []

        while undetected_count < sample_size:
            # inject
            inj_out = injector.inject_error(s)
            inj_count += 1

            # log injected output value
            inj_out_dict['err_output'].append(inj_out)

            # detect
            detected = detector.detect_error(s, inj_out)
            if not detected:
                inj_count_list.append(inj_count)
                undetected_count += 1
                inj_count = 0

            # some error models are highly-covered by some detectors
            # thus, exit if injection iteration reaches a limit (op_width * 4)
            # so infinite iteration is needed to get an undetected error
            # However, JSON dumps inf as Infinity which is not a valid JSON value
            # Therefore, I dummps 0's instead and make 0's back to inf again
            if inj_count > max_inj_count:
                inj_count_list = sample_size * [0]
                break
        all_inj_count_dict[s['operation']].extend(inj_count_list)
        all_inj_out.append(inj_out_dict)

        # Heartbeat    
        num_processed += 1
        if num_processed % 100 == 0:
            print "{} samples have been processed by worker {}".format(num_processed, worker_id) 

    rtl_masking_dict = None
    if injector.__class__.__name__ == 'RTLInjector':
        rtl_masking_dict = injector.get_rtl_iter_by_module()

    injector.cleanup()
    
    results = {
        'inj_count' : all_inj_count_dict,
        'rtl_iter' : rtl_masking_dict,
        'inj_out' : all_inj_out,
    }

    return results

def main(args):

    #print "My Begin: {}, My End:{}".format(args.begin_idx, args.end_idx) 

    # Get my samples
    with open(args.sample_json) as f_json:
        # Get samples with supported detection methods
        samples = json.loads(f_json.read())
        begin, end = int(args.begin_idx), int(args.end_idx)
        my_samples = samples[begin : end]
        worker_id = "Sample({},{})".format(args.begin_idx, args.end_idx)

        with open(args.config_yaml) as cf:
            config = yaml.load(cf)

            results = subsampling(my_samples, config, args.src_path, worker_id)
            with open(args.out_file, "a") as out_f:
                json_str = json.dumps(results)
                out_f.write("{}\n".format(json_str))

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-c', dest='config_yaml', required=True, help='YAML file for configuring the reinjection')
    parser.add_argument('-s', dest='sample_json', required=True, help='JSON file containing assigned samples')
    parser.add_argument('-p', dest='src_path', required=True, help='Source path')
    parser.add_argument('-b', dest='begin_idx', required=True, help='Beginning index of assigned samples')
    parser.add_argument('-e', dest='end_idx', required=True, help='Ending index of assigned samples')
    parser.add_argument('-o', dest='out_file', required=True, help='Output file path')
    args = parser.parse_args()
    
    sys.path.append(os.path.join(args.src_path, './injectors'))
    sys.path.append(os.path.join(args.src_path, './checkers'))
    from randbit_inj import *
    from random_inj import *
    from rtl_inj import *
    from dummy_checkers import *
    from residue_checkers import *
    
    print "Start a subsampling in {}".format(os.getcwd())
    main(args)
