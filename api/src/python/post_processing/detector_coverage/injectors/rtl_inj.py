import os
import yaml, csv, json
import subprocess

def join_cwd(this_path):
    cwd = os.getcwd()
    return os.path.join(cwd, this_path)

class RTLInjector():

    def __init__(self, injector_path, config_path, module_list_path, err_batch_size=10):
        self.injector_path = injector_path
        self.launcher_path = os.path.join(injector_path,'src/stand_alone.py')
        self.config_path = config_path
        self.module_list = self.build_module_list(module_list_path)
        self.tmp_config = join_cwd('rtl_tmp.yaml')
        self.tmp_inputs = join_cwd('rtl_tmp_inputs.csv')
        self.tmp_output = join_cwd('out.json')
        # To save time, we generate errors in batch (batch size is determined by sample size)
        self.err_batch_size = err_batch_size
        self.error_cache = []
        self.prev_sample = None
        self.rtl_iter_by_module = {}
        
    def inject_error(self, sample):
        # If the same sample (i.e., same input vector) is supplied, we return an error (if any) from cache
        # Otherwise, the next batch of error is performed
        same = self.check_same_sample(sample)
        if same and self.error_cache:
            injected_out = self.pop_error_cache()
            return injected_out

        # build error context from this sample
        cxt = self.build_cxt(sample)

        # find RTL module for simulation
        module_name, module = self.find_module(cxt)
        assert module, "Failed to find the corresponding module from the provided list"

        # render RTL injection config
        self.render_inj_config(module)

        # prepare input file for RTL injector
        self.render_input_csv(sample, module)

        # call RTL injector
        self.launch_injector()

        # parse injection results
        self.parse_inj_results(module_name)

        # return injection result
        injected_out = self.pop_error_cache()
      
        return injected_out

    def get_rtl_iter_by_module(self):
        return self.rtl_iter_by_module

    def cleanup(self):
        try:
            os.remove(self.tmp_config)
            os.remove(self.tmp_inputs)
            os.remove(self.tmp_output)
        except:
            print "Error removing temp files"

    ############################   
    #   End of public methods
    ############################

    def build_module_list(self, module_list_path):
        with open(module_list_path) as f:
            modules = yaml.load(f)
            if not 'modules' in modules:
                raise Exception("RTL module settings needed!")
            return modules['modules']

    def build_cxt(self, sample):
        cxt = {}
        cxt['width'] = sample['outputs'][0]['width']
        cxt['operation'] = sample['operation']
        cxt['datatype'] = sample['data_type']
        return cxt

    def find_module(self, inst_cxt):
        # Find matching RTL module for instruction
        for name, module in self.module_list.iteritems():
            match = False
            for inst in module['instructions']:
                match = True
                for k, v in inst_cxt.iteritems():
                    if k in inst and inst[k] != v:
                        match = False
                        break
                if match:
                    break

            if match:
                # Remove the *instructions* field (unused by real injection)
                #module.pop('instructions', None)
                return (name, module)

        return (None, None)

    def make_abs(self, path):
        return os.path.join(self.injector_path, path)

    def render_inj_config(self, module):
        with open(self.config_path) as cf:
            config = yaml.load(cf)
            with open(self.tmp_config, "w") as tf:
                # modify relative paths to absolute ones
                config['cache'] = self.make_abs(config['cache'])
                config['cell_libs']['nangate']['dir'] = self.make_abs(config['cell_libs']['nangate']['dir'])
                config['dirs']['rtl_comp'] = self.make_abs(config['dirs']['rtl_comp'])
                # custom fields
                config['module'] = module
                config['experiment']['errors'] = self.err_batch_size
                yaml.dump(config, tf, default_flow_style=False)
            
    def render_input_csv(self, sample, module):
        with open(self.tmp_inputs, "w") as f:
            writer = csv.writer(f)
            # wrtie input names
            inputs = module['inputs']
            variable_inputs = sorted([i for i in inputs if not 'value' in inputs[i]])
            writer.writerow(variable_inputs)

            # write input values
            inputs = sample['inputs']
            input_vals = [i['value'] for i in inputs]
            writer.writerow(input_vals)

    def launch_injector(self):
        cmd = [self.launcher_path, '-c', self.tmp_config, '-i', self.tmp_inputs, '-o', self.tmp_output, '-p', '1']
        child_process = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        out, err = child_process.communicate()
        #subprocess.call(cmd)
   
    def parse_inj_results(self, module_name):
        with open(self.tmp_output) as f:
            results = json.load(f)
            # update error cache
            self.error_cache = results['errors']

            # log rtl iteration to compute logic masking factor
            rtl_iters = [e['iterations'] for e in results['errors']]
            if module_name in self.rtl_iter_by_module:
                self.rtl_iter_by_module[module_name].extend(rtl_iters)
            else:
                self.rtl_iter_by_module[module_name] = rtl_iters
                
    def pop_error_cache(self):  
        error = self.error_cache.pop(0)
        err_outputs = error['errorneous_outputs']
        #assert len(err_outputs) == 1, "Multiple output case is not supported yet"
        return err_outputs[0]

    def check_same_sample(self, sample):
        if self.prev_sample is sample:
            return True
        else:
            # log sample 
            self.prev_sample = sample
            return False
      
