import random
class RandomInjector():
    def inject_error(self, sample):
        out_width = sample['outputs'][0]['width'] 
        max_value = (1 << out_width) - 1;
        injected_out = random.randrange(0, max_value)
        return injected_out
        
    def cleanup(self):
        pass
