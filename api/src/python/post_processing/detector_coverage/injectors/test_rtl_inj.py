import pytest
import rtl_inj as tmod

add_sample = {
    "inputs": [
        {
            "width": 64, 
            "value": 15
        }, 
        {
            "width": 8, 
            "value": 1
        }
    ], 
    "data_type": "DT_INT", 
    "outputs": [
        {
            "width": 64, 
            "value": 16
        }, 
        {
            "width": 64, 
            "value": 0
        }
    ],  
    "operation": "OP_ADD", 
}
sub_sample = {
    "inputs": [
        {
            "width": 64, 
            "value": 15
        }, 
        {
            "width": 8, 
            "value": 1
        }
    ], 
    "data_type": "DT_INT", 
    "outputs": [
        {
            "width": 64, 
            "value": 14
        }, 
        {
            "width": 64, 
            "value": 0
        }
    ],  
    "operation": "OP_SUB", 
}

def test_same_sample():

    injector_path = '/home/chunkai/hamartia/rtl_error_injector/src/stand_alone.py'
    config_path = '../configs/rtl_config.yaml'
    module_list_path = '../configs/rtl_modules.yaml'
    injector = tmod.RTLInjector(injector_path, config_path, module_list_path)
    corrupted_out = injector.inject_error(add_sample)
    print corrupted_out
    corrupted_out = injector.inject_error(add_sample)
    print corrupted_out
    
