import random
'''
    Generate a random bit vector of 'word_size' bits and with 'bit_count' 1's set
'''
def random_bits(word_size, bit_count):
    number = 0
    for bit in random.sample(range(word_size), bit_count):
        number |= 1 << bit
    return number


class RandbitInjector():
    def __init__(self, num_bit_flip):
        self.num_bit_flip = num_bit_flip
    
    def inject_error(self, sample):
        golden_out = sample['outputs'][0]['value'] 
        out_width = sample['outputs'][0]['width'] 
        bitmask = random_bits(out_width, self.num_bit_flip) 
        injected_out = golden_out ^ bitmask
        return injected_out
        
    def cleanup(self):
        pass
