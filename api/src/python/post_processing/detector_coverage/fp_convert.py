'''
    In-place floating-point conversion from human-readable format to the format used by the RTL injector,
    and then back to human-readable format

    See README for details.
'''
import binascii
import struct

def fp2uint(sample):
    FP_TYPE = { # keys are data types; values are used to control precision level
               'DT_FLOAT':  ('!f','7f800000'), # second member is the infinity in hex repr
               'DT_DOUBLE': ('!d','7ff0000000000000')
    }
    def _fp2uint(op, aux_args):
        fp_val = float(op['value'])
        try:
            fp_val_in_hex = binascii.hexlify(struct.pack(aux_args[0], fp_val))
        except OverflowError:
            fp_val_in_hex = aux_args[1]

        fp_val_in_uint = int(fp_val_in_hex, 16)
        op['value'] = fp_val_in_uint 
 
    if sample['data_type'] in FP_TYPE:
        aux_args = FP_TYPE.get(sample['data_type'], None)
        assert aux_args

        # convert input
        for op_in in sample['inputs']:
            _fp2uint(op_in, aux_args)
        # convert output
        for op_out in sample['outputs']:
            _fp2uint(op_out, aux_args)
    else:
        pass

def uint2fp(sample):
    FP_TYPE = { # keys are data types; values are used to control precision level
               'DT_FLOAT':  ('f','I'),
               'DT_DOUBLE': ('d','L')
    }

    def _uint2fp(op, aux_args):
        fp_val = struct.unpack(aux_args[0], struct.pack(aux_args[1], op['value']))[0]
        op['value'] = fp_val

    if sample['data_type'] in FP_TYPE:
        aux_args = FP_TYPE.get(sample['data_type'], None)
        assert aux_args

        # convert input
        for op_in in sample['inputs']:
            _uint2fp(op_in, aux_args)
        # convert output
        for op_out in sample['outputs']:
            _uint2fp(op_out, aux_args)
    else:
        pass

def is_fp_close(s1, s2):

    def is_close(a, b, rel_tol=1e-09, abs_tol=0.0):
        return abs(a-b) <= max(rel_tol * max(abs(a), abs(b)), abs_tol)

    for i in xrange(len(s1['inputs'])):
        assert is_close(s1['inputs'][i]['value'], s2['inputs'][i]['value'])

    for i in xrange(len(s1['outputs'])):
        assert is_close(s1['outputs'][i]['value'], s2['outputs'][i]['value'])

def test_fp_converter():
    float_sample = { # values are in human-readable format
                "inputs": [
                    {
                        "width": 32, 
                        "value": 1.2e-01
                    }, 
                    {
                        "width": 32, 
                        "value": 1.2e-01
                    }
                ], 
                "data_type": "DT_FLOAT", 
                "outputs": [
                    {
                        "width": 32, 
                        "value": 2.4e-01
                    }
                ], 
                "operation": "OP_ADD", 
                "asm_line": "addsd xmm0, xmm1"
    }
    float_result = { # values are in the format usable by the RTL injector
                "inputs": [
                    {
                        "width": 32, 
                        "value": 1039516303
                    }, 
                    {
                        "width": 32, 
                        "value": 1039516303
                    }
                ], 
                "data_type": "DT_FLOAT", 
                "outputs": [
                    {
                        "width": 32, 
                        "value": 1047904911
                    }
                ], 
                "operation": "OP_ADD", 
                "asm_line": "addsd xmm0, xmm1"
    }

    double_sample = {
                "inputs": [
                    {
                        "width": 64, 
                        "value": 1.2e-01
                    }, 
                    {
                        "width": 64, 
                        "value": 1.2e-01
                    }
                ], 
                "data_type": "DT_DOUBLE", 
                "outputs": [
                    {
                        "width": 64, 
                        "value": 2.4e-01
                    }
                ], 
                "operation": "OP_ADD", 
                "asm_line": "addpd xmm0, xmm1"
    }
    double_result = {
                "inputs": [
                    {
                        "width": 64, 
                        "value": 4593311331947716280
                    }, 
                    {
                        "width": 64, 
                        "value": 4593311331947716280
                    }
                ], 
                "data_type": "DT_DOUBLE", 
                "outputs": [
                    {
                        "width": 64, 
                        "value": 4597814931575086776
                    }
                ], 
                "operation": "OP_ADD", 
                "asm_line": "addpd xmm0, xmm1"
    }

    original_sample = dict(float_sample)
    fp2uint(float_sample)
    assert float_sample == float_result
    uint2fp(float_sample)
    is_fp_close(float_sample, original_sample)
    print "Pass 32-bit FP case!"

    original_sample = dict(double_sample)
    fp2uint(double_sample)
    assert double_sample == double_result
    uint2fp(double_sample)
    is_fp_close(double_sample, original_sample)
    print "Pass 64-bit FP case!"

if __name__ == '__main__':
    test_fp_converter()
    
