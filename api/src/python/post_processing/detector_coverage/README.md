Detector Coverage Estimator
===========================

This script does post-processing on injection results to obtain coverage of certain detector.
Recall that we keep injecting errors until one error was not detected when user specify both
error model and detector model. Although the original injection result contains the number of 
iterations to get an undetected error, it is not enough to estimate coverage of the applied detector 
if the number of injection is not large enough.

To estimate the coverage accurately, this script performs more injections on the collected
samples and estimates the mean coverage of the detector along with the 95% confidence interval.

Dependencies
------------
- Python packages (pip)
    - pyyaml
    - scipy
    - numpy
    - pytest (for unit testing)

- Tested under Python 2.7

Usage
-----
Before using this estimator, you need to have some injection results. We provide a script to parse 
the data values from the injection results (*assume injection was done by our automatic launcher*.) 
A config file is needed to specify the experiments (e.g. error model, detector model, etc). 

### User-level Steps:
1. Parse injection results to format samples into JSON 
    - `python analyze_operands.py -a <APP_NAME> -d <RESULT_DIR> [-o <OUT_NAME>]`
        - APP_NAME: the application's name
        - RESULT_DIR: the *iter* directory created by the launcher to store raw injection results
        - OUT_NAME (optional): output file name of the parsed results; default is `operands.json`
    - An example parsed file `toy_operands.json` is included in the `inputs` directory for fast testing
2. Prepare a config file (see `configs/mod3_coverage.yaml` for example) 
3. Estimate detector coverage 
    - `python run.py -c <config>`

The estimator will print the coverage for each operation type and also dump a JSON file with the number
of injected errors to obtain each undetected error. The output file format is encoded using the 
application name, error model, and detector model provided by the config file. 

Error/Detector Models
---------------------
We implemented some error models and detector models in this estimator. 
The following are currently supported models:

- Error models:
    - RANDBIT*X* : flip *X* bit of the output value randomly
    - RANDOM: replace the original value with a random value of the same width
    - RTL

- Detector models:
    - MODULO*X* : residue checker with one modulus, *X*
    - MODULO*X_Y* : residue checker with two moduli, *X* and *Y*

TODO
----
1. Integration with the automatic laucher 

