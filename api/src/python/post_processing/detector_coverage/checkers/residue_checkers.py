
class ResidueChecker():
    def __init__(self, moduli):
        assert type(moduli) == list
        self.moduli = moduli

    def get_in_residue(self, sample):
        inputs = sample['inputs']
        assert len(inputs) == 2, "Inputs must be two values"
        op = sample['operation']

        input1_mods = [inputs[0]['value'] % m for m in self.moduli]
        input2_mods = []
        if op == 'OP_SUB':
            input2_mods = [ (-inputs[1]['value']) % m for m in self.moduli]
        else:
            input2_mods = [inputs[1]['value'] % m for m in self.moduli]
        return (input1_mods, input2_mods)
    
    def get_inj_out_residue(self, inj_out):
        inj_out_mods = [inj_out % m for m in self.moduli]
        return inj_out_mods

    def check(self, op, in1, in2, inj_out_mods):

        def decode_op(this_op):
            operations = {
                'OP_ADD': lambda x,y: x + y,
                'OP_SUB': lambda x,y: x + y,
                'OP_MUL': lambda x,y: x * y,
            } 
            return operations.get(this_op, None)

        op_func = decode_op(op) 

        out_mods = [op_func(in1[i], in2[i]) % m for i, m in enumerate(self.moduli)]
        detected = [int(om != inj_out_mods[i]) for i, om in enumerate(out_mods)]
        detected_count = detected.count(1)
        if detected_count > 0:
            return True
        else:
            return False

    def detect_error(self, sample, inj_out):
        in1_mods, in2_mods = self.get_in_residue(sample)
        op = sample['operation']
        inj_out_mods = self.get_inj_out_residue(inj_out)    
        detected = self.check(op, in1_mods, in2_mods, inj_out_mods)
        return detected

