'''
    A dummy checker class which always fails checking errors.
    This is used to estimate masking factors of RTL error model for now.
'''
class DummyChecker():
    def detect_error(self, sample, inj_out):
        return False 
