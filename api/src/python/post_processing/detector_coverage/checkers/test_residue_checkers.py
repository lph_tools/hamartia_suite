import pytest
import residue_checkers as tmod

add_sample = {
    "inputs": [
        {
            "width": 64, 
            "value": 15
        }, 
        {
            "width": 8, 
            "value": 1
        }
    ], 
    "data_type": "DT_INT", 
    "outputs": [
        {
            "width": 64, 
            "value": 16
        }, 
        {
            "width": 64, 
            "value": 0
        }
    ],  
    "operation": "OP_ADD", 
}
sub_sample = {
    "inputs": [
        {
            "width": 64, 
            "value": 15
        }, 
        {
            "width": 8, 
            "value": 1
        }
    ], 
    "data_type": "DT_INT", 
    "outputs": [
        {
            "width": 64, 
            "value": 14
        }, 
        {
            "width": 64, 
            "value": 0
        }
    ],  
    "operation": "OP_SUB", 
}
mul_sample = {
    "inputs": [
        {
            "width": 64, 
            "value": 15
        }, 
        {
            "width": 8, 
            "value": 2
        }
    ], 
    "data_type": "DT_INT", 
    "outputs": [
        {
            "width": 64, 
            "value": 30
        }, 
        {
            "width": 64, 
            "value": 0
        }
    ],  
    "operation": "OP_MUL", 
}
add_sample_answer = 16 
sub_sample_answer = 14
mul_sample_answer = 30


def test_no_error():
    detector = tmod.ResidueChecker([3])
    assert detector.detect_error(add_sample, add_sample_answer) == False
    assert detector.detect_error(sub_sample, sub_sample_answer) == False
    assert detector.detect_error(mul_sample, mul_sample_answer) == False

def test_1_mod_1_bit_error():
    detector = tmod.ResidueChecker([3])
    assert detector.detect_error(add_sample, add_sample_answer^1)
    assert detector.detect_error(sub_sample, sub_sample_answer^1)
    assert detector.detect_error(mul_sample, mul_sample_answer^1)
    assert detector.detect_error(add_sample, add_sample_answer^2)
    assert detector.detect_error(sub_sample, sub_sample_answer^2)
    assert detector.detect_error(mul_sample, mul_sample_answer^2)

def test_1_mod_undetected_error():
    detector = tmod.ResidueChecker([3])
    assert detector.detect_error(add_sample, add_sample_answer+3) == False
    assert detector.detect_error(sub_sample, sub_sample_answer+3) == False
    assert detector.detect_error(mul_sample, mul_sample_answer+3) == False

def test_2_mod_1_bit_error():
    detector = tmod.ResidueChecker([3,5])
    assert detector.detect_error(add_sample, add_sample_answer^1)
    assert detector.detect_error(sub_sample, sub_sample_answer^1)
    assert detector.detect_error(mul_sample, mul_sample_answer^1)

def test_2_mod_undetected_error():
    detector = tmod.ResidueChecker([3,5])
    assert detector.detect_error(add_sample, 31) == False
    assert detector.detect_error(sub_sample, 29) == False
    assert detector.detect_error(mul_sample, 15) == False

