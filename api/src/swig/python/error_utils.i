%include "stdint.i"

%module error_utils
%{
#include <hamartia_api/utils/error.h>
using namespace hamartia_api;
%}

%include <hamartia_api/utils/error.h>
