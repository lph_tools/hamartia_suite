%include "stdint.i"

%module dtypes
%{
#include <hamartia_api/types.h>
using namespace hamartia_api;
%}

%ignore hamartia_api::DataValue::operator=;
%rename(__getitem__) hamartia_api::DataValue::operator[](std::size_t);

%include <hamartia_api/types.h>
