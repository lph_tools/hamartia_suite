%include "stdint.i"

%module utils
%{
#include <hamartia_api/utils/error.h>
using namespace hamartia_api;
%}

%include <hamartia_api/utils/error.h>
