%include "stdint.i"
%include "std_string.i"
%include "std_vector.i"
%include "std_map.i"

%module elogging
%{
#include <hamartia_api/logging.h>
using namespace hamartia_api;
%}

%import "dtypes.i"
%import "interface.i"

%include <hamartia_api/logging.h>

%extend hamartia_api::Log {
    %template(ModelAddLogString) ModelAddLogEntry<std::string>;
};
