%include "stdint.i"
%include "std_string.i"
%include "std_vector.i"

%module(directors="1") detector_model
%{
#include <hamartia_api/detector_model.h>
using namespace hamartia_api;
%}

%import "interface.i"
%import "elogging.i"

%feature("director") hamartia_api::DetectorModel;
%feature("director") hamartia_api::DetectorModelLibrary;

%include <hamartia_api/detector_model.h>

%template(vector_dmp) std::vector<hamartia_api::DetectorModel*>;

%ignore hamartia_api::DetectorModel::Setup(hamartia_api::ErrorContext *, YAML::Node *, hamartia_api::Log *);
%ignore hamartia_api::DetectorModel::Cleanup(hamartia_api::ErrorContext *, YAML::Node *, hamartia_api::Log *);
%ignore hamartia_api::DetectorModel::CheckError(hamartia_api::ErrorContext *, YAML::Node *, hamartia_api::Log *);
