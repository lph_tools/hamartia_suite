%include "stdint.i"
%include "std_string.i"

%module(directors="1") injector
%{
#include <hamartia_api/injector.h>
using namespace hamartia_api;
%}

%import "interface.i"
%import "error_model.i"
%import "detector_model.i"

%feature("director") hamartia_api::Injector;

%include <hamartia_api/injector.h>
