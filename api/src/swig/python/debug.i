%include "std_string.i"

%module debug
%{
#include <hamartia_api/debug.h>

bool DebugEnabled() {
#ifdef DEBUG
    return true;
#else
    return false;
#endif
}

void DebugTag(std::string tag, std::string msg) {
    DEBUG_TAG(tag, msg)
}

void FatalError(std::string msg) {
    FATAL_ERROR(msg)
}

void DebugPrint(std::string msg) {
    DEBUG_PRINT(msg)
}

void InfoPrint(std::string msg) {
    INFO_PRINT(msg)
}

void WarnPrint(std::string msg) {
    WARN_PRINT(msg)
}

void ErrorPrint(std::string msg) {
    ERROR_PRINT(msg)
}

void EmergPrint(std::string msg) {
    EMERG_PRINT(msg)
}

%}

%include <hamartia_api/debug.h>

bool DebugEnabled();
void DebugTag(std::string tag, std::string msg);
void FatalError(std::string msg);
void DebugPrint(std::string msg);
void InfoPrint(std::string msg);
void WarnPrint(std::string msg);
void ErrorPrint(std::string msg);
void EmergPrint(std::string msg);
