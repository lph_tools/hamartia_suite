%include "stdint.i"
%include "std_string.i"
%include "std_vector.i"

%module(directors="1") error_model
%{
#include <hamartia_api/error_model.h>
using namespace hamartia_api;
%}

%import "interface.i"
%import "elogging.i"

%feature("director") hamartia_api::ErrorModel;
%feature("director") hamartia_api::ErrorModelLibrary;

%include <hamartia_api/error_model.h>

%template(vector_emp) std::vector<hamartia_api::ErrorModel*>;

%ignore hamartia_api::ErrorModel::Setup(hamartia_api::ErrorContext *, YAML::Node *, hamartia_api::Log *);
%ignore hamartia_api::ErrorModel::Cleanup(hamartia_api::ErrorContext *, YAML::Node *, hamartia_api::Log *);
%ignore hamartia_api::ErrorModel::PreInjectError(hamartia_api::ErrorContext *, YAML::Node *, hamartia_api::Log *);
%ignore hamartia_api::ErrorModel::InjectError(hamartia_api::ErrorContext *, YAML::Node *, hamartia_api::Log *);
