%include "stdint.i"
%include "std_string.i"
%include "std_vector.i"

%module interface
%{
#include <hamartia_api/interface/instruction_types.h>
#include <hamartia_api/interface/instruction_data.h>
#include <hamartia_api/interface/injection_stats.h>
#include <hamartia_api/interface/injection_trigger.h>
#include <hamartia_api/interface/error_context.h>
using namespace hamartia_api;
%}

%import "dtypes.i"

// Rename templates
%template(vector_dv) std::vector<hamartia_api::DataValue>;
%template(vector_op) std::vector<hamartia_api::Operand>;
%template(vector_flt) std::vector<float>;
%template(vector_dbl) std::vector<double>;

// Rename operators
// TODO: get working...
%rename("%(regex:/operator\\= *\\(/set\\(/)s", %$isfunction) "";

%include <hamartia_api/interface/instruction_types.h>
%include <hamartia_api/interface/instruction_data.h>
%include <hamartia_api/interface/injection_stats.h>
%include <hamartia_api/interface/injection_trigger.h>
%include <hamartia_api/interface/error_context.h>
