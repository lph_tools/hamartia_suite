SWIG Generation
===============

Overview
--------
SWIG is used to create Python (currently) bindings for the API. It uses the existing C++ source code
and the Python C API to create interface files/libraries which allow the API functions to be called
from Python. Additionally, by using the Python C API for a handler, Python models can be run within
a C++ injector (no forking/communication necessary).

SWIG uses interface files which specify how to generate the Python interfaces to the C++ code.
Thankfully, mostly all you have to do is include the `*.h` files; however, there are certain
situations where C++ doesn't translate perfectly to Python and additional commands/code are needed
in the interface files or source code. 

Status
------
Currently, most cases have been covered for initial functionality with Python models. However, there
are still some sections of the C++ code which create warnings and are not generated in Python.

Building
--------
Currently building the SWIG files is integrated into the scons build system. There were some initial
issues with shared library name generation and the build not being able to find them, but that
*should* be fixed now.
