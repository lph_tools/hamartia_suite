# /bin/bash

if [ -n "$1" ]; then
	echo "Make docs..."
	scons -c docs build_docs=1
	scons docs build_docs=1
	echo "Install docs at ${1}..."
	mkdir -p ${1}
	mkdir -p ${1}/cpp
	rm -r ${1}/cpp/*
	cp -r docs/cpp/images ${1}/cpp/images
	cp -r docs/cpp/_build/html/* ${1}/cpp

	mkdir -p ${1}/python
	rm -r ${1}/python/*
	cp -r docs/python/images ${1}/python/images
	cp -r docs/python/_build/html/* ${1}/python

else
	echo "No destination!"
	echo "Usage: ./install_docs.sh <docs_destination>"
fi
