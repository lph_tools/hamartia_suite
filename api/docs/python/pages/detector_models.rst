.. Error Injector documentation master file, created by
   sphinx-quickstart on Thu Mar 26 14:27:53 2015.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. _detector_models:

Detector Models
===============

Features
--------

The Detector model API focuses on detecting unmasked errors. Currently the features
offered are:

- C++/Python Detector model API/execution available (this discusses the Python API)
- Instruction context forwarded (old values, operands, instruction information)
- Logging constructs (for model specific output)


Interface
---------

The error interface is defined as the inputs/outputs to the injection entry-point
(:meth:`~.DetectorModel.CheckError`). The most important aspects for creating an 
detector model are:

- **ErrorContext.error_info.respose** : Response of detector model (or operation)
- **ErrorContext.instruction_data** : Contains instruction data such as:

	- Assembly information
	- Data type and width/size
	- SIMD width

- **ErrorContext.instruction_data.instruction** : Instruction definition (x86-based)
- **ErrorContext.instruction_data.inputs** : Input operands to instruction, containing:

	- Data type, width/size, SIMD lanes
	- Value(s)
	- Type (e.g. Register, Memory, etc)
	- Address (memory), Reg (register)

- **ErrorContext.instruction_data.outputs** : Output operands to instruction

	- Note, the data currently in these are the *correct* values (after the instruction is executed)
    - Also, ``old_value`` contains the previous values (before the instruction was run)

While you can interact with the ErrorContext directly, you can also use helper
functions (getters/setters) to easily extract and save information into the context.


Essential API Calls/Types
-------------------------

While there are many calls, the ones below are some of the essentials:

- :class:`~.DataValue` : A unified container for data values, including representations for:
	- Integers
	- Floats/Doubles (and their sign, fraction, exponent segments)
	- RFLAGS
    - Arbitrary bit operations
- :func:`~.getInputValue` : Get Input value from context (useful for getting
  instruction operands)
- :func:`~.getOutputValue` : Get Output value from context (useful for getting
  injected output values)
- :func:`~.getOutputOldValue` : Get the old Output value from context (useful
  for looking at update distance or other metrics)
- :meth:`~.DetectorModel.SetResponse` : Set the response (of the context) for
  the operation


Creating an Detector Model
--------------------------

All detector models extend the :class:`~.DetectorModel` interface which provides
interface definitions and entry-points for error-detection. ``CheckError()`` is
the entry-point from an injector into your detector model. It includes:

- Error Context, information about the injected instruction (including operands, opcode, etc)
- Configuration, a decoded YAML file of configuration options (model specific)
- Log, a way for inserting specific logging information for your model

Otherwise, the main injection/fault flow is as follows:

1. Gather instruction information and operand data from the context
	- Note that the context includes inputs and outputs (injected and old)
2. Inspect the context to detect any errors (may depend on the operation)
3. Set a response indicating if the error is invalid (``RESP_INVALID_ERROR``);
   otherwise no response needed
4. Return if successful (no runtime errors)

To get a better understanding of how to create an detector model, and the
interfaces/utilities used, an example of an detector model is shown below:

.. _detector-model-example:

Example
^^^^^^^
.. code-block:: python

    import os, sys

    if 'HAMARTIA_DIR' in os.environ:
        sys.path.append(os.path.join(os.environ['HAMARTIA_DIR'], 'src/python'))
    import hamartia_api

    # My Detector Model
    class MyDetectorModel(hamartia_api.error_model.DetectorModel):
        """
        MyDetectorModel definition
        """

        def __init__(self, _name, _my_setting):
            """
            Create MyDetectorModel instance

            Args:
                _name       (str): Detector model name
                _my_setting (int): A setting for parameterized/customizable detector models
            """

            super(MyDetectorModel, self).__init__(_name)
            self.my_setting = _my_setting


        # Currently unused...
        def Prepare(self):
            pass


        # Currently unused...
        def Setup(self, config):
            pass


        # Currently unused...
        def SaveOutput(self):
            pass


        def CheckError(self, cxt, config, log):
            """
            Detect an error

            Args:
                cxt    (object): error context of injected error
                config   (dict): configuration info
                log    (object): logging info

            Returns:
                bool: Successful
            """

            # Old result
            old_result = cxt.getOutputOldValue()
            # Injected result
            result = cxt.getOutputValue()

            if cxt.instruction_data.data_type == hamartia_api.interface.DT_UINT and
               cxt.instruction_data.outputs[0].type == hamartia_api.interface.OT_MEMORY:
                
                if abs(old_result.uint - result.uint) > my_setting:
                    # Detected error
                    DetectorModel.SetResponse(cxt, hamartia_api.interface.RESP_INVALID_ERROR)

            # Otherwise, valid (undetected) error

            return True


Let's discuss the operation of this detector model. First, we include the API.
Then, we define our own detector model ``MyDetectorModel`` which extends the
``DetectorModel`` interface. Next, we should think how the detector model
should be used/instantiated. Detector models are registered as an instance;
thus, it is completely possible to parameterize detector models. This is
accomplished by defining members (such as ``my_setting``) in the class. Then,
in the constructor for the model, along with the ``name`` other arguments which
set up parameters can be included. In this case, we included ``my_setting``.

The main portion of the model is the ``CheckError`` definition. In here we
gather the previous and injected outputs as well as the datatype and type of
operation (memory). We then simply calculate the distance between the two
addresses to check for large differences (obviously not the best detector, but
an example). If a invalid error is detected, ``SetResponse()`` is used with
``RESP_INVALID_ERROR`` to specify an invalid error. Otherwise the error is
valid. Finally, the return value indicates if there was a runtime error.

Essentially what happens during execution is:

1. The injector traps on a target instruction
2. Context information is gathered
3. The specified error model and detector model are looked up by names (not class)
4. The error model is called with context, config, and logging information
5. The error model generates and injects a fault into the context
6. The system (error manager) verifies if it is an unmasked error
7. The detector model is called with context, config, and logging information
8. The detector model checks whether the injected error is detectable
9. The context is saved and program execution continues


Registering a Detector Model
----------------------------

In order for your detector model to get called, it needs to be registered first
with the API.  Registering detector models is done through
:class:`~.DetectorModelLibrary`.  To register models, a library is defined
which extends :class:`~.DetectorModelLibrary`. Then, instances of detector
models are registered within the constructor of the library. These libraries
then can be compiled in with the injector and then initialized with
:meth:`~.ErrorManager.InitDetectorModelLibrary`.  Additionally, they can be
compiled as their own separate shared-library (.so) and be loaded in
dynamically either explicitly or through the ``HAMARTIA_DETECTOR_PATH``.

An example for creating an ``DetectorModelLibrary`` is shown below using 
the detector model class we constructed previously (see :ref:`detector-model-example`).

**Example - Error Model Library**

.. code-block:: python

    # Assume this piece of code follows the error model we just created
    # Thus, we have include the API we need

    class DetectorLibraryExample(hamartia_api.detector_model.DetectorModelLibrary):
        """
        DetectorLibraryExample
        """
        
        def __init__(self):
            """
            Initialize basic detector model library, register all detector models
            """

            # Init
            super(DetectorLibraryExample, self).__init__()

            # Model 1 (Use the default setting)
            model1 = MyDetectorModel('MODEL1')
            super(DetectorLibraryExample, self).AddModel(model1, 'MODEL1')

            # Model 2 (Parameterized setting)
            model2_1 = MyDetectorModel('MODEL2_1', 1)
            model2_2 = MyDetectorModel('MODEL2_2', 2)
            model2_4 = MyDetectorModel('MODEL2_4', 4)
            model2_8 = MyDetectorModel('MODEL2_8', 8)
            super(DetectorLibraryExample, self).AddModel(model2_1)
            super(DetectorLibraryExample, self).AddModel(model2_2)
            super(DetectorLibraryExample, self).AddModel(model2_4)
            super(DetectorLibraryExample, self).AddModel(model2_8)

        def GetName(self):
            """
            Get name of library

            Returns:
                str: Name of library
            """

            return "detector_library_example"


    # Register library
    register = DetectorLibraryExample


First, the error model API is included, along with any detector model that you wish to include. Then, a
class for the library is created which extends ``DetectorModelLibrary``. For this class, you mainly
just need to define the constructor and ``GetName()``. The constructor will contain the
"registration" for each of the detector models. The first argument is the name of the detector model (a 
string without quotes), which is what is used for calling the detector model. The second is the setting 
unique to that detector model. ``AddModel`` is then used to add the detector models.

Note, ``register`` **must** be defined to allow for dynamic loading.


Using a Detector Model
----------------------

As explained in the last section, detector models are registered using
``DetectorModelLibrary`` which can then be initialized/included with the
injector. If you want to compile in the library with the injector and then
initialize it, you would create an instance of the library and then use
:meth:`~.ErrorManager.InitDetectorModelLibrary` in order to register it with
the error manager.

In terms of usage, the detector model is defined within the ``ErrorContext``
(cxt.error_info.detector_model), which can be set by the ``Injector()``
constructor. However, these are usually up to the injector on how they populate
these values (usually through the command-line).
