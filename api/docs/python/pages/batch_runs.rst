.. Error Injector documentation master file, created by
   sphinx-quickstart on Thu Mar 26 14:27:53 2015.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. _batch_runs:

Batch Running
=============

We provide a launcher script for launching error injectors (or any other instrumentation application) 
in a templated manner. The launcher is located at ``$HAMARTIA_DIR/src/python/launcher``. 
It allows user to launch experiments in an automatic and parallel manner. 
Injection result analysis can also be pipelined if needed. 
See `here <http://lph.ece.utexas.edu/users/hamartia/docs/cpp/md_pages_batch_runs.html>`_ for usage.
