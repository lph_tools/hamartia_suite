.. Error Injector documentation master file, created by
   sphinx-quickstart on Thu Mar 26 14:27:53 2015.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. _injectors:

Injectors
=========

Features
--------

The Injector interface provides an easy way to interface with the Error and Detector models. It
implements common actions for instruction-level injectors. However, functions related to
loading/unloading the internal state of the injector need to be implemented by the user. Currently
the features offered are:

- C++/Python Error/Detector model execution
- Instruction (or Instruction-like) state forwarding (to Error/Detector models)
- Logging constructs


Essential API Calls/Types
-------------------------

The interface is defined in the :class:`~.Injector` class. The most important
aspects for creating an injector are:

- :meth:`~.Injector.PreStateToContext` : Used for downloading the state
  into the ErrorContext before an instruction is run (instruction inputs and previous output values).
- :meth:`~.Injector.PostStateToContext` : Used for downloading the state
  into the ErrorContext after an instruction is run (correct instruction outputs).
- :meth:`~.Injector.InjectError` : Injects an error using the current ErrorContext.
- :meth:`~.Injector.RollbackState` : Used for uploading the old values of the ErrorContext
  into the state of the injector (allows register/address destinations to change).
- :meth:`~.Injector.InjectionContextToState` : Used for uploading the injected
  (incorrect) ErrorContext into the state of the injector.


Creating an Injector
--------------------

The Injector class is essentially the interface to using Error/Detector models with your injector.
Since injectors keep track of different state, the implementation of some of the above functions
will vary depending on how the state is kept. However, implementing is as easy as extending your
injector with the class or creating an instance of an extended class and using that in your
injector. We will focus on the latter, since it is applicable in all cases.

**Example - Injector class definition**

.. code-block:: python

    # Hamartia API
    from hamartia_api. import injector
    from hamartia_api. import interface
    import hamartia_api

    # Operand type
    REG = 1
    MEM = 2

    # MyState (highly application dependent)
    class MyState:
        def __init__(self):
            self.opcode = 0x00 # Instruction opcode
            self.in_type = []  # Input types
            self.in_ops = []   # Input operands
            self.out_type = 0  # Output type
            self.out_op = 0    # Output operand
            self.regs = []     # Registers
            self.mem = []      # Memory


    # MyInjector definition
    class MyInjector(injector.Injector):

        def __init__(self, error_model=None, error_config=None, detector_model=None, detector_config=None):
            """
            Initialize the injector with error/detector model info/configs

            Args:
                error_model     (str): The error model to use for injection
                error_config    (str): The YAML config to pass to the error model (path)
                detector_model  (str): The detector model to use for injection
                detector_config (str): The YAML config to pass to the detector model (path)
            """

            super(MyInjector, self).__init__(error_model, error_config, detector_model, detector_config)

        # Note: InjectError() does not necessarily need to be modified


        def PreStateToContext(injector_state):
            """
            Save initial instruction state

            Args:
                injector_state (object): MyState (architectural state)
            """

            # Set instruction opcode
            self.current_context.instruction_data.instruction.opcode = injector_state.opcode
            # Set inputs
            for i, (op, op_type) in enumerate(zip(injector_state.in_ops, injector_state.in_types)):
                dv = hamartia_api.DataValue()

                if op_type == REG:
                    # Register
                    dv.uint = injector_state.regs[op]
                    self.current_context.instruction_data.inputs[i].reg = op
                else:
                    # Memory
                    dv.uint = injector_state.mem[op]
                    self.current_context.instruction_data.inputs[i].address.virt = op

                self.current_context.setInput(dv, i)

            # Set old output
            dv = hamartia_api.DataValue()
            op = injector_state.out_op

            if injector_state.out_type == REG:
                # Register
                dv.uint = injector_state.regs[op]
                self.current_context.instruction_data.outputs[0].reg = op
            else:
                # Memory
                dv.uint = injector_state.mem[op]
                self.current_context.instruction_data.outputs[0].address.virt = op

            self.current_context.instruction_data.outputs[0].setOldValue(dv)


       def PostStateToContext(injector_state):
            """
            Save correct instruction state
            """

            # Set output
            dv = hamartia_api.DataValue()
            op = injector_state.out_op
            if injector_state.out_type == REG:
                # Register
                dv.uint = injector_state.regs[op]
                self.current_context.instruction_data.outputs[0].reg = op
            else:
                # Memory
                dv.uint = injector_state.mem[op]
                self.current_context.instruction_data.outputs[0].address.virt = op

            self.current_context.setOutput(dv)


        def RollbackState(injector_state):
            """
            Rollback state
            """

            # Upload output
            dv = self.current_context.getOutputOldValue()
            if self.current_context.instruction_data.outputs[0].reg is not None:
                # Register
                reg = self.current_context.instruction_data.outputs[0].reg
                injector_state.regs[reg] = dv.uint
            else:
                # Memory
                addr = self.current_context.instruction_data.outputs[0].address.virt
                injector_state.mem[addr] = dv.uint


        def InjectionContextToState(injector_state):
            """
            Apply injected state
            """

            # Upload output
            dv = self.injection_context.getOutputValue()
            if self.injection_context.instruction_data.outputs[0].reg is not None:
                # Register
                reg = self.injection_context.instruction_data.outputs[0].reg
                injector_state.regs[reg] = dv.uint
            else:
                # Memory
                addr = injection_context.instruction_data.outputs[0].address.virt
                injector_state.mem[addr] = dv.uint

            # Override current context
            current_context = injection_context


Let's discuss the operation of this injector. First, we include the API and some shorcuts.
Then, we define our own injector ``MyInjector`` which extends the
:class:`~.Injector` interface. We forward all the constructors to the default
functionality. Then we start overriding the functions that manage state. Note
that we do not have to override :meth:`~.Injector.InjectError`, since the default functionality is
usually fine.

For ``PreStateToContext()`` we include the ``injector_state`` in the arguments,
which is used to forward specific instruction state to the Error model (using
``current_context``). Note, ``current_context`` is the current (initially
un-injected values) and ``injected_context`` is the injected values. Thus, we
set the instruction opcode and then set the input operands (value and type) and
output operands (old values for rollback). Please see the :class:`~.ErrorContext` 
for more information about what values can be set (currently) in
``error_context.instruction_data``. There are some methods ease the use of setting
ErrorContext.  For ``PostStateToContext()`` we similarly save the outputs in the
``current_context`` before injection.

For ``RollbackState()``, a mutable ``MyState`` is passed instead to allow 
modification. Since we want to revert the old values back into the state, we look 
at the metadata for the output instruction and inject the value into the state. 
This should be called before ``InjectionContextToState()`` if registers or addresses 
change (unless there is no state change after the target instruction is run).

Similarly, for ``InjectionContextToState()`` a mutable ``MyState`` is passed
in, allowing for modification.  Since we want to inject the erroneous values
back into the state, we look at the metadata for the output instruction and
inject the value into the state. Also, note that the ``current_context``
becomes the ``injected_context`` since the values have now been "committed."

Using the Injector
------------------

**Example - Injector Usage**

.. code-block:: python

    # ...

    injector = None

    def Init(args):
        """
        Program/injector initialization
        """

        # Setup injector with error model and config
        injector = MyInjector(args[0], args[1])


    def PreInstructionEvent(state, inject):
        """
        Pre-instruction event
        """

        if inject:
            # Download state to context
            injector.PreStateToContext(state)


    def PostInstructionEvent(state, inject):
        """
        Post-instruction event
        """

        if inject:
            # Download state to context
            injector.PostStateToContext(state)
            # Run injection
            injector.InjectError()
            # Rollback values (if needed)
            injector.RollbackToOldState(state)
            # Upload injected value(s)
            injector.InjectionContextToState(state)

    # ...

Injector usage is very simple. First, an instance of ``MyInjector`` is created.
This is initialized with the error model and config path. In this example,
``PreInstructionEvent()`` indicates a function that is called before each
instruction. However, if we want to inject into an instruction, first we call
``PreStateToContext()`` to save the initial state. Then after the instruction
is executed, in ``PostInstructionEvent()`` we save the correct results of the
instruction with ``PostStateToContext()``.  Then we inject an error into the
context with ``InjectError()``. If needed (e.g. error is detected), we 
rollback values with ``RollbackToOldState()`` to get back to a know/unchanged 
state. Finally, we save the corrupted value(s) to the state with 
``InjectionContextToState()``.
