.. Error Injector documentation master file, created by
   sphinx-quickstart on Thu Mar 26 14:27:53 2015.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. _error_models:

Error Models
============

Features
--------

Currently, the Error model API focuses on injecting instruction-level, transient (one-time) faults.
However, there are some additions that are in progress to allow for multiple injections (persistent
faults) and different state/context information. Currently the features offered are:

- C++/Python Error model API/execution available (this discusses the Python API)
- Instruction context forwarded (operands, instruction information)
- Logging constructs (for model specific output)


Interface
---------

The error interface is defined as the inputs/outputs to the injection
entry-point (:meth:`~.ErrorModel.InjectError`). The most important aspects 
for creating an error model are:

- **ErrorContext.error_info.respose** : Response of error model (or operation)
- **ErrorContext.instruction_data** : Contains instruction data such as:

    - Assembly information
    - Data type and width/size
    - SIMD width

- **ErrorContext.instruction_data.instruction** : Instruction definition (x86-based)
- **ErrorContext.instruction_data.inputs** : Input operands to instruction, containing:

    - Data type, width/size, SIMD lanes
    - Value(s)
    - Type (e.g. Register, Memory, etc)
    - Address (memory), Reg (register)

- **ErrorContext.instruction_data.outputs** : Output operands to instruction

    - Note, the data currently in these are the *correct* values (after the instruction is executed)
    - Also, ``old_value`` contains the previous values (before the instruction was run)


While you can interact with the interface (ErrorContext) directly, you can also use helper
functions to easily extract and save information into the context.


Essential API Calls/Types
-------------------------

While there are many calls, the ones below are some of the essentials:

- :class:`~.DataValue` : A unified container for data values, including representations for:
    - Integers
    - Floats/Doubles (and their sign, fraction, exponent segments)
    - RFLAGS
    - Arbitrary bit operations
- :func:`~.getInputValue` : Get Input value from context (useful for getting
  instruction operands)
- :func:`~.getOutputValue` : Get Output value from context (useful for getting
  correct output values)
- :func:`~.getOutputOldValue` : Get the old Output value from context
- :func:`~.setOutput` : Set Output value for context (useful for injecting incorrect output values)
- :meth:`~.ErrorModel.SetResponse` : Set the response (of the context) for the
  operation (injection)


Creating an Error Model
-----------------------

All Error models extend the :class:`~.ErrorModel` interface which provides
interface definitions and entry-points for fault injection.
:meth:`~.ErrorModel.InjectError()` is the entry-point from an injector into
your error model. It includes:

- Error Context, information about the injected instruction (including operands, opcode, etc)
- Configuration, a decoded YAML file of configuration options (model specific)
- Log, a way for inserting specific logging information for your model

Otherwise, the main injection/fault flow is as follows:

1. Gather instruction information and operand data from the context

    - Note that the context includes inputs and outputs (correct)

2. Based upon the instruction and its operands, generate an fault based off of some model
3. Inject the fault back into the context (i.e. one of the outputs)
4. Optionally set a response indicating future operations (e.g. ``RESP_UPDATE``)
5. Return if successful (no runtime errors)

Also, included in the API are :mod:`~.utils.error` (e.g. random bit flipping functions, etc) 
and helper functions (e.g. getting/setting operands, etc) which can be used from the (insert libs here).

To get a better understanding of how to create an error model, and the interfaces/utilities used, an
example of an error model is shown below:

.. _error-model-example:

Example
^^^^^^^
.. code-block:: python

    import os, sys

    if 'HAMARTIA_DIR' in os.environ:
        sys.path.append(os.path.join(os.environ['HAMARTIA_DIR'], 'src/python'))
    import hamartia_api

    # My Error Model
    class MyErrorModel(hamartia_api.error_model.ErrorModel):
        """
        MyErrorModel definition
        """

        def __init__(self, _name, _my_setting):
            """
            Create MyErrorModel instance

            Args:
                _name       (str): Error model name
                _my_setting (int): A setting for parameterized/customizable error models
            """

            super(MyErrorModel, self).__init__(_name)
            self.my_setting = _my_setting


        # Currently unused...
        def Prepare(self):
            pass


        # Currently unused...
        def Setup(self, config):
            pass


        # Currently unused...
        def SaveOutput(self):
            pass


        def InjectError(self, cxt, config, log):
            """
            Inject random error

            Args:
                cxt    (object): error context to inject into
                config   (dict): configuration info
                log    (object): logging info

            Returns:
                bool: Successful
            """

            # Old result
            old_result = cxt.getOutputValue()

            # Generate error
            mod_result = hamartia_api.DataValue()
            mod_result.uint = hamartia_api.utils.MR_inject(old_result.uint, self.my_setting)

            # Inject error
            cxt.setOutput(mod_result)

            return True


Let's discuss the operation of this fault model. First, we include the API.
Then, we define our own error model ``MyErrorModel`` which extends the ``ErrorModel`` interface. Next,
we should think how the error model should be used/instantiated. Error models are registered as an
instance; thus, it is completely possible to parameterize error models. This is accomplished by
defining members (such as ``my_setting``) in the class. Then, in the constructor for the model, along
with the ``name`` other arguments which set up parameters can be included. In this case, we included
``my_setting``.

The main portion of the model is the ``InjectError`` definition. In here, we gather the
correct output of the instruction using a helper function. This way, we can directly modify the
correct output without having to replay the instruction. The fault generation itself uses a random
fault function (see utils) depending on the width of the operation. It also uses ``my_setting`` as a
mask. Finally, the fault is injected back into the context using a helper function to set one of the
outputs of the context. Finally, the return value indicates if there was a runtime error.

Essentially what happens during execution is:

1. The injector traps on a target instruction
2. Context information is gathered
3. The specified error model is looked up by name (not class)
4. The error model is called with context, config, and logging information
5. The error model generates and injects a fault into the context
6. The system (error manager) verifies it is an unmasked/undetected error
7. The context is saved and program execution continues
            

Registering an Error Model
--------------------------

In order for your error model to get called, it needs to be registered first
with the API.  Registering Error models is done through
:class:`~.ErrorModelLibrary`.  To register models, a library is defined which
extends :class:`~.ErrorModelLibrary`. Then, instances of error models are
registered within the constructor of the library. These libraries then can be
compiled in with the injector and then initialized with
:meth:`~.ErrorManager.InitErrorModelLibrary`.  Additionally, they can be
compiled as their own separate shared-library (.so) and be loaded in
dynamically either explicitly or through the ``HAMARTIA_ERROR_PATH``.

An example for creating an ``ErrorModelLibrary`` is shown below using the error
model class we constructed previously (see :ref:`error-model-example`).

**Example - Error Model Library**

.. code-block:: python

    # Assume this piece of code follows the error model we just created
    # Thus, we have include the API we need

    class ErrorLibraryExample(hamartia.error_model.ErrorModelLibrary):
        """
        ErrorLibraryExample
        """
        
        def __init__(self):
            """
            Initialize basic error model library, register all error models
            """

            # Init
            super(ErrorLibraryExample, self).__init__()

            # Model 1 (Use the default setting)
            model1 = MyErrorModel('MODEL1')
            super(ErrorLibraryExample, self).AddModel(model1)

            # Model 2 (Parameterized setting)
            model2_1 = MyErrorModel('MODEL2_1', 1)
            model2_2 = MyErrorModel('MODEL2_2', 2)
            model2_4 = MyErrorModel('MODEL2_4', 4)
            model2_8 = MyErrorModel('MODEL2_8', 8)
            super(ErrorLibraryExample, self).AddModel(model2_1)
            super(ErrorLibraryExample, self).AddModel(model2_2)
            super(ErrorLibraryExample, self).AddModel(model2_4)
            super(ErrorLibraryExample, self).AddModel(model2_8)


        def GetName(self):
            """
            Get name of library

            Returns:
                str: Name of library
            """

            return "error_library_example"


    # Register library
    register = ErrorLibraryExample
    

First, the error model API is included, along with any error model that you wish to include. Then, a
class for the library is created which extends ``ErrorModelLibrary``. For this class, you mainly
just need to define the constructor and ``GetName()``. The constructor will contain the
"registration" for each of the error models. The first argument is the name of the error model (a 
string without quotes), which is what is used for calling the error model. The second is the setting 
unique to that error model. ``AddModel`` is then used to add the error models.

Note, ``register`` **must** be defined to allow for dynamic loading.


Using an Error Model
--------------------

As explained in the last section, Error models are registered using ``ErrorModelLibrary`` which can
then be initialized/included with the injector. If you want to compile in the library with the
injector and then initialize it, you would create an instance of the library and then use
:meth:`~.ErrorManager.InitErrorModelLibrary` in order to register it with the error manager.

In terms of usage, the error model is defined within the ``ErrorContext``
(cxt.error_info.error_model), which can be set by the ``Injector()`` constructor. However, these are
usually up to the injector on how they populate these values (usually through the command-line).

Our RTL gate-level injector is implemented as a Python error model. Thus, we refer user to 
the source code of it for a detailed example of using error models. The entry point in source 
code is ``model.py``.
