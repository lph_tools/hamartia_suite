.. Error Injector documentation master file, created by
   sphinx-quickstart on Thu Aug 27 19:44:48 2015.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Hamartia Error API Documentation
================================

Overview
--------
The Hamartia Error and Detector API provides a common interface for fault injection and detection.
While the current implementation is currently focused on *instruction-level* injection, the overall
interface could be extended in the future to support different levels of injection. In general this
library provides:

- Cross-language interfaces (SWIG)

	- Instruction information
	- Injection statistics

- Generic Injector
- Error managers (for handling C++ or Python error/detector models)
- Unified error/detector model interface
- Basic fault utilities
- Helper functions

Error Injection Flow
--------------------
.. image:: images/API_Flows.png

Indices and Modules
===================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`


Topics
======

.. toctree::
   :titlesonly:
   :maxdepth: 3

   pages/design
   pages/batch_runs
   pages/error_models
   pages/detector_models
   pages/injectors
