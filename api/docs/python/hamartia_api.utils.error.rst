hamartia_api.utils.error module
===============================

.. automodule:: hamartia_api.utils.error
    :members:
    :undoc-members:
    :show-inheritance:
