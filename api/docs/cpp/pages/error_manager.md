Error Managers
==============

Features
--------

Error Managers allow for errors to be injected and possibly detected. Additionally, they allow for
error models instantiated in a different language to be run by launching an external process running
that (language's) error manager. The interface between the error managers is implemented in C++ 
and connected to script languages like Python using SWIG. Currently the features offered are:

- C++/Python Error/Detector model execution (more lanaguages could be added in the future)
- Instruction (or Instruction-like) state forwarding (to Error/Detector models)
- Statistics tracking
- Logging constructs


Operation
---------

![Injection Flow](images/API_Flows.png)

Error managers are responsible executing error/detector models, whether local or external. A local
error/detector model is one which matches the language of the "main_manager", the manager which is
directly associated with the injector (executable). A remote/external error/detector model is one
which does not match the language of the "main_manager" and must fork a separate process (external
error manager) in order to process the model.

Since the error manager itself runs the error/detector models, it is responsible for the
registration of the models (seen in the error/detector model sections). This can either be done
statically (at compile time) or dynamically (during runtime).

The interface is defined in the [error_manager](@ref hamartia_api::error_manager) namespace (since
it is effectively a singleton). The most important aspects for creating an error manager are:

- [Init](@ref hamartia_api::error_manager::Init) : Used for initializing the error manager (with
  arguments from the command-line optionally)
- [RegisterHandlers](@ref hamartia_api::error_manager::RegisterHandlers) : Used for registering error/detector
  libraries with the error manager.
- [LoadAllErrorModelsFromEnv/LoadAllDetectorModelsFromEnv](@ref hamartia_api::error_manager::LoadAllErrorModelsFromEnv) : 
  Used for loading error/detector libraries dynamically.
- [InjectError](@ref hamartia_api::error_manager::InjectError) : Used for injecting an
  unmasked/undetected error (only the injector calls this).
- [CheckError](@ref hamartia_api::error_manager::CheckError) : Used for detecting an
  unmasked/undetected error (only the injector calls this).

Creating an Error Manager
-------------------------

Creating an Error manager is relatively straight forward. As long as your intended language is
supported by SWIG and you follow the interface for the error manager (look at the C++/Python code 
for the error managers for examples), it should work.
