Detector Models
===============

Features
--------

The Detector model API focuses on detecting unmasked errors. Currently the features
offered are:

- C++/Python Detector model API/execution available (this discusses the C++ API)
- Instruction context forwarded (old values, operands, instruction information)
- Logging constructs (for model specific output)


Interface
---------

The error interface is defined as the inputs/outputs to the injection entry-point
([DetectorModel::CheckError()](@ref hamartia_api::DetectorModel::CheckError)). 
The most important aspects for creating an detector model are:

- **ErrorContext.error_info.respose** : Response of detector model (or operation)
- **ErrorContext.instruction_data** : Contains instruction data such as:
	- Assembly information
	- Data type and width/size
	- SIMD width
- **ErrorContext.instruction_data.instruction** : Instruction definition (x86-based)
- **ErrorContext.instruction_data.inputs** : Input operands to instruction, containing:
	- Data type, width/size, SIMD lanes
	- Value(s)
	- Type (e.g. Register, Memory, etc)
	- Address (memory), Reg (register)
- **ErrorContext.instruction_data.outputs** : Output operands to instruction
	- Note, the data currently in these are the *correct* values (after the instruction is executed)
    - Also, `old_value` contains the previous values (before the instruction was run)

While you can interact with the interface (ErrorContext pointer) directly, you can also use helper
functions to easily extract and save information into the context.


Essential API Calls/Types
-------------------------

While there are many calls, the ones below are some of the essentials:

- [DataValue](@ref hamartia_api::DataValue) : A unified container for data values, including representations for:
	- Integers
	- Floats/Doubles (and their sign, fraction, exponent segments)
	- RFLAGS
    - Arbitrary bit operations
- [ErrorContext::getInputValue()](@ref hamartia_api::ErrorContext::getInputValue) : Get Input
  value from context (useful for getting instruction operands)
- [ErrorContext::getOutputValue()](@ref hamartia_api::ErrorContext::getOutputValue) : Get Output
  value from context (useful for getting correct output values)
- [ErrorContext::getOutputOldValue()](@ref hamartia_api::ErrorContext::getOutputOldValue) : Get the
  old Output value from context
- [DetectorModel::SetResponse()](@ref hamartia_api::DetectorModel::SetResponse) : Set the response (of the
  context) for the operation (injection)


Creating an Detector Model
--------------------------

All Detector models extend the [DetectorModel](@ref hamartia_api::DetectorModel) interface which provides
interface definitions and entry-points for error-detection. `CheckError()` is the entry-point from
an injector into your detector model. It includes:

- Error Context, information about the injected instruction (including operands, opcode, etc)
- Configuration, a decoded YAML file of configuration options (model specific)
- Log, a way for inserting specific logging information for your model

Otherwise, the main injection/fault flow is as follows:

1. Gather instruction information and operand data from the context
	- Note that the context includes inputs and outputs (injected and old)
2. Inspect the context to detect any errors (may depend on the operation)
3. Set a response indicating if the error is invalid (`RESP_INVALID_ERROR`), otherwise no response
   needed
4. Return if successful (no runtime errors)

To get a better understanding of how to create an detector model, and the interfaces/utilities used, an
example of an detector model is shown below:

### Example

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.cpp}
#ifndef _MY_DETECTORMODEL_H
#define _MY_DETECTORMODEL_H

#include <hamartia_api/types.h>
#include <hamartia_api/detector_model.h>
#include <cmath>

class MyDetectorModel : public hamartia_api::DetectorModel
{

	private:
	// Detector model setting
	uint64_t my_setting;
		 
	public:
	/**
	 * Create MyDetectorModel instance
	 *
	 * \param _name    	  Detector model name
	 * \param _my_setting A setting for parameterized/customizable detector models
	 */
	MyDetectorModel(std::string _name, uint64_t _my_setting) : DetectorModel(_name)
	{
		my_setting = _my_setting;
	}

	/**
	 * Detect an error
	 *
	 * \param cxt    Error context of injected error
	 * \param config YAML config file
	 * \param log    Log
	 *
	 * \return Successful
	 */
	bool CheckError(hamartia_api::ErrorContext *cxt, YAML::Node *config, hamartia_api::Log *log)
	{
		// Old result
		hamartia_api::DataValue old_result = cxt->GetOutputOldValue();
		// Injected result
		hamartia_api::DataValue result = cxt->GetOutputValue();

		if (cxt->instruction_data().data_type() == hamartia_api::DT_UINT &&
            cxt->instruction_data().outputs(0).type() == OT_MEMORY) {

            // Check distance
            if (abs(((int64_t) old_result.uint) - ((int64_t) result.uint)) > my_setting) {
                // Invalid (detected) error
                SetResponse(cxt, RESP_INVALID_ERROR);
            }
        }

        // Otherwise, valid error

		return true;
	}

};

#endif
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Let's discuss the operation of this detector model. First, we include the necessary include files:

- *types.h* : Datatypes for dealing with operands (i.e. DataValue)
- *detector_model.h* : The DetectorModel interface definition

Then, we define our own detector model `MyDetectorModel` which extends the `DetectorModel` interface. Next,
we should think how the detector model should be used/instantiated. Detector models are registered as an
instance; thus, it is completely possible to parameterize detector models. This is accomplished by
defining members (such as `my_setting`) in the class. Then, in the constructor for the model, along
with the `name` other arguments which set up parameters can be included. In this case, we included
`my_setting`.

The main portion of the model is the `CheckError` definition. In here we gather the previous and
injected outputs as well as the datatype and type of operation (memory). We then simply calculate
the distance between the two addresses to check for large differences (obviously not the best
detector, but an example). If a invalid error is detected, `SetResponse()` is used with
`RESP_INVALID_ERROR` to specify an invalid error. Otherwise the error is valid. Finally, the return
value indicates if there was a runtime error.

Essentially what happens during execution is:

1. The injector traps on a target instruction
2. Context information is gathered
3. The specified error model and detector model are looked up by names (not class)
4. The error model is called with context, config, and logging information
5. The error model generates and injects a fault into the context
6. The system (error manager) verifies if it is an unmasked error
7. The detector model is called with context, config, and logging information
8. The detector model checks whether the injected error is detectable
9. The context is saved and program execution continues



Registering a Detector Model
----------------------------

In order for your detector model to get called, it needs to be registered first with the API.
Registering Detector models is done through [DetectorModelLibrary(s)](@ref hamartia_api::DetectorModelLibrary). 
To register models, a library is defined which extends
[DetectorModelLibrary](@ref hamartia_api::DetectorModelLibrary). Then, instances of detector models are
registered within the constructor of the library. These libraries then can be compiled in with the
injector and then initialized with [RegisterHandlers()](@ref hamartia_api::error_manager::RegisterHandlers).
Additionally, they can be compiled as their own separate shared-library (.so) and be loaded in
dynamically either explicitly or through the `HAMARTIA_DETECTOR_PATH`.

An example for creating an `DetectorModelLibrary` is shown below using 3 different detector models.

### Example - Error Model Library

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.cpp}
// Libs
#include <string>
#include <hamartia_api/detector_model.h>

// Detector models
#include "model1.h"
#include "model2.h"
#include "model3.h"

class DetectorLibraryExample : public hamartia_api::DetectorModelLibrary
{
	public:
	DetectorLibraryExample()
    {
        // Model 1 
        REGISTER_DETECTOR_MODEL(DetectorModel1, MODEL1)

        // Model 2
        REGISTER_DETECTOR_MODEL_ARGS(DetectorModel2, MODEL2_1, 1)
        REGISTER_DETECTOR_MODEL_ARGS(DetectorModel2, MODEL2_2, 2)
        REGISTER_DETECTOR_MODEL_ARGS(DetectorModel2, MODEL2_4, 4)
        REGISTER_DETECTOR_MODEL_ARGS(DetectorModel2, MODEL2_8, 8)

        // Model 3
        REGISTER_DETECTOR_MODEL_ARGS(DetectorModel3, MODEL3_32, 32)
        REGISTER_DETECTOR_MODEL_ARGS(DetectorModel3, MODEL3_64, 64)
    }

	std::string GetName()
    {
        return "detector_library_example";
    }
};
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

First, the detector model API is included, along with any detector model that you wish to include. Then, a
class for the library is created which extends `DetectorModelLibrary`. For this class, you mainly just
need to define the constructor and `GetName()`. The constructor will contain the "registration" for
each of the detector models. `REGISTER_DETECTOR_MODEL` and `REGISTER_DETECTOR_MODEL_ARGS` are two
macros that aid in adding the detector models. The first argument is the class name of the detector
model and the second is the name of the detector model (without quotes). Additionally,
`REGISTER_DETECTOR_MODEL_ARGS` can be used to initialize parameterized detector models where the
additional arguments are passed to the constructor. Thus, multiple parameterized detector models can
be registered with the same error model class. Note, the name specified at registration (second
argument) is what is used for calling the detector model.


Using a Detector Model
----------------------

As explained in the last section, Detector models are registered using `DetectorModelLibrary` which can
then be initialized/included with the injector. If you want to compile in the library with the
injector and then initialize it, you would create an instance of the library and then use
[RegisterHandlers()](@ref hamartia_api::error_manager::RegisterHandlers) in order to register it 
with the error manager.

In terms of usage, the detector model is defined within the `ErrorContext`
(cxt.error_info.detector_model), which can be set by the `Injector()` constructor. However, these are
usually up to the injector on how they populate these values (usually through the command-line).
