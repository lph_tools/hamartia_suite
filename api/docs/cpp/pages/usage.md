Building and Usage
==================

Building
--------
The Hamartia API uses the Scons build system, so please install that before building. Additionally,
make sure YAML-CPP and other dependencies are installed (http://lph.ece.utexas.edu/users/hamartia/install.htm). 
Then simply run:

	scons

Cleaning can be done with:

	scons -c

To build with debug (messages):

    scons -Q debug=1

And documentation can be compiled with (requires doxygen and sphinx to be installed):

	scons docs

### API Library

The build process creates a static-library (./build/libhamartia_api.so) which can be loaded into
programs when compiling. This will allow for your program to utilize the Error/Detector API in your
custom injector (or models). A typical compile/link command may be:

	g++ -I $HAMARTIA_DIR/include/cpp -L $HAMARTIA_DIR/build -lhamartia_api <source_files> -o <output>

### Error/Detector Libraries

The build process also creates a couple shared-libraries for the default/basic error/detector models
which can be loaded dynamically. The same workflow should be used for creating your own
error/detector model libraries which then can be loaded into the error manager or injector. The
command is very similar to above:

	g++ -shared -I $HAMARTIA_DIR/include/cpp -L $HAMARTIA_DIR/build -lhamartia_api <source_files> -o <output.so>

### Error Manager Executable

The build process also creates a standalone error manager executable which is used for executing C++
Error/Detector models from other languages (e.g. Python).

Running
-------
Running is completely dependent on the injector that is used. Please refer to the Pin-Injector and 
RTL-Injector documentation for more detail.
