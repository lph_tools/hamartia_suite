Batch Running
=============

We provide a launcher script for launching error injectors (or any other instrumentation application) in a templated manner.
The launcher is located at $HAMARTIA_DIR/src/python/launcher. It allows user to launch experiments in an automatic and parallel 
manner. Injection result analysis can also be pipelined if needed.

Python Package Required
--------
- mako
- pyyaml
- futures

Usage
--------
Suppose `$LAUNCHER_DIR` is set to `$HAMARTIA_DIR/src/python/launcher`

Basic usage:

	$LAUNCHER_DIR/launcher.py -c <config_file> -d <output_dir>

For timeout usage (detecting hang):

	$LAUNCHER_DIR/launcher.py -c <config_file> -d <output_dir> -t <timeout>

There are several examples of *config_file* within `$LAUNCHER_DIR/configs` directory.
We refer user to README.md in the launcher directory for details.
