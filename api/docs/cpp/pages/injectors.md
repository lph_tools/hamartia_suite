Injectors
=========

Features
--------

The Injector interface provides an easy way to interface with the Error and Detector models. It
implements common actions for instruction-level injectors. However, functions relating to
loading/unloading the internal state of the injector need to be implemented by the user. Currently
the features offered are:

- C++/Python Error/Detector model execution
- Instruction (or Instruction-like) state forwarding (to Error/Detector models)
- Logging constructs


Essential API Calls/Types
-------------------------

The interface is defined in the [Injector](@ref hamartia_api::Injector) class. The most important
aspects for creating an injector are:

- [PreStateToContext](@ref hamartia_api::Injector::PreStateToContext) : Used for downloading the state
  into the [ErrorContext](@ref hamartia_api::ErrorContext) before an instruction is run (instruction 
  inputs and previous output values).
- [PostStateToContext](@ref hamartia_api::Injector::PostStateToContext) : Used for downloading the state
  into the [ErrorContext](@ref hamartia_api::ErrorContext) after an instruction is run (correct instruction outputs).
- [InjectError](@ref hamartia_api::Injector::InjectError) : Injects an error using the current
  [ErrorContext](@ref hamartia_api::ErrorContext).
- [RollbackState](@ref hamartia_api::Injector::RollbackState) : Used for uploading the old
  values of the [ErrorContext](@ref hamartia_api::ErrorContext) back into the state of the injector
  (allows register/address destinations to change).
- [InjectionContextToState](@ref hamartia_api::Injector::InjectionContextToState) : Used for uploading the injected
  (incorrect) [ErrorContext](@ref hamartia_api::ErrorContext) into the state of the injector.


Creating an Injector
--------------------

The Injector class is essentially the interface to using Error/Detector models with your injector.
Since injectors keep track of different state, the implementation of some of the above functions
will vary depending on how the state is kept. However, implementation is as easy as extending your
injector with the class or creating an instance of an extended class and using that in your
injector. We will focus on the latter, since it is applicable in all cases.

### Example - Injector class definition

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.cpp}

#ifndef _MY_INJECTOR_H
#define _MY_INJECTOR_H

// Error API
#include <hamartia_api/types.h>
#include <hamartia_api/interface.h>
#include <hamartia_api/injector.h>

// Operand type
typedef enum {
	REG,
	MEM
} op_t;

// MyState (highly application dependent)
struct {
	int opcode;            // Instruction opcode
	op_t in_type[NUM_OPS]; // Input types
	int in_ops[NUM_OPS];   // Input operands (register id or memory address)
	op_t out_type;         // Output type
	int out_op;            // Output operand (register id or memory address)
	int regs[NUM_REGS];    // Registers
	int mem[MEM_SIZE];     // Memory
} MyState;

// MyInjector definition
class MyInjector : public hamartia_api::Injector {
	public:
    // Constructors
	MyInjector() : Injector() {}
	MyInjector(std::string error_model, std::string error_config) : Injector(error_model, error_config) {}
	MyInjector(std::string error_model, std::string error_config, std::string detector_model, std::string detector_config) : Injector(error_model, error_config, detector_model, detector_config) {}
    // Deconstructor
	~MyInjector() {}

	// State functions
	// Note: InjectError() does not necessarily need to be modified
	void PreStateToContext(MyState injector_state);
	void PostStateToContext(MyState injector_state);
	void RollbackState(MyState *injector_state);
	void InjectionContextToState(MyState *injector_state);
};


// Save initial instruction state
void MyInjector::PreStateToContext(MyState injector_state)
{
    // Set instruction opcode
	current_context.instruction_data.instruction.opcode = injector_state.opcode;
	// Set inputs
	for (int i = 0; i < NUM_OPS; ++i) {
		hamartia_api::DataValue dv;
		int op = injector_state.in_ops[i];

		if (injector_state.in_type[i] == REG) {
			// Register (op is register id)
			dv.uint = injector_state.regs[op];
		} else {
			// Memory (op is address)
			dv.uint = injector_state.mem[op];
			current_context.instruction_data.inputs[i].address.virt = op;
		}
		current_context.setInput(dv, i);
	}
    // Set old output
    hamartia_api::DataValue dv;
    int op = injector_state.out_op;

    if (injector_state.out_type == REG) {
        // Register
        dv.uint = injector_state.regs[op];
        current_context.instruction_data.outputs[0].reg_id = op;
        current_context.instruction_data.outputs[0].type = OT_REGISTER;
    } else {
        // Memory
        dv.uint = injector_state.mem[op];
        current_context.instruction_data.outputs[0].address.virt = op;
        current_context.instruction_data.outputs[0].type = OT_MEMORY;
    }
    current_context.instruction_data.outputs[0].setOldValue(dv);
}

// Save correct instruction state
void MyInjector::PostStateToContext(MyState injector_state)
{
	// Set output
	hamartia_api::DataValue dv;
	int op = injector_state.out_op;
	if (injector_state.out_type == REG) {
		// Register
		dv.uint = injector_state.regs[op];
	} else {
		// Memory
		dv.uint = injector_state.mem[op];
		current_context.instruction_data.outputs[0].address.virt = op;
	}
    current_context.instruction_data.outputs[0].setValue(dv);
}

// Rollback state
void MyInjector::RollbackState(MyState *injector_state)
{
	// Upload output
	hamartia_api::DataValue dv = current_context.getOutputOldValue();
	if (current_context.instruction_data.outputs[0].type == OT_REGISTER) {
		// Register
		int reg_idx = current_context.instruction_data.outputs[0].reg_id;
		*injector_state.reg[reg_idx] = dv.uint;
	} else {
		// Memory
		int addr = current_context.instruction_data.outputs[0].address.virt;
		*injector_state.mem[addr] = dv.uint;
	}
}

// Save injected state
void MyInjector::InjectionContextToState(MyState *injector_state)
{
	// Upload output
	hamartia_api::DataValue dv = injection_context.GetOutputValue();
	if (injection_context.instruction_data.outputs[0].type == OT_REGISTER) {
		// Register
		int reg_idx = injection_context.instruction_data.outputs[0].reg_id;
		*injector_state.reg[reg_idx] = dv.uint;
	} else {
		// Memory
		int addr = injection_context.instruction_data.outputs[0].address.virt;
		*injector_state.mem[addr] = dv.uint;
	}

    // Copy to current context
    current_context = injection_context;
}

#endif
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Let's discuss the operation of this injector. First, we include the necessary include files:

- *types.h* : Datatypes for dealing with operands and injection (i.e. DataValue)
- *interface.h* : Interface definitions for error_context, instruction_dat, etc.
- *injector.h* : The Injector interface definition

Then, we define our own injector `MyInjector` which extends the [Injector](@ref hamartia_api::Injector)
interface. We forward all the constructors to the default functionality. Then we start overriding
the functions that manage state. Note that we do not have to override 
[InjectError](@ref hamartia_api::Injector::InjectError), since the default functionality is usually fine.

For `PreStateToContext()` we include the `injector_state` in the arguments, which is used to forward
specific instruction state to the Error model (using `current_context`). Note, `current_context` is
the current (initially un-injected values) and `injected_context` is the injected values. Thus, we
set the instruction opcode and then set the input operands (value and type) and output operands (old
values for rollback) to `current_context`. Please see the [InstructionData Interface](@ref hamartia_api::InstructionData) 
for more information about what values can be set (currently) in `error_context.instruction_data`.
Also, there are some helper methods in [ErrorContext](@ref hamartia_api::ErrorContext) that ease 
the use of the ErrorContext. For `PostStateToContext()` we similarly save the outputs in the
`current_context` before injection.

For `RollbackState()` a pointer to `MyState` is passed instead, which allows for modification.
Since we want to inject the old values back into the state, we look at the metadata for the
output instruction and inject the value into the state. This should be called before
`InjectionContextToState()` if registers or addresses change (unless there is no state change after
the target instruction is run).

Similarly, for `InjectionContextToState()` a pointer to `MyState` is passed in, allowing for modification.
Since we want to inject the erroneous values back into the state, we look at the metadata for the
output instruction and inject the value into the state. Also, note that the `current_context`
becomes the `injected_context` since the values have now been "committed."

Using the Injector
------------------

### Example - Injector Usage

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.cpp}
// ...

MyInjector * injector;

// Program/injector initialization
void Init(std::vector<std::string> args)
{
	// Setup injector with error model and config
	injector = new MyInjector(args[0], args[1]);
}

// Pre-instruction event
void PreInstructionEvent(MyState *state, bool inject)
{
	if (inject) {
		// Download state to context
		injector->PreStateToContext(*state);
	}
}

// Post-instruction event
void PostInstructionEvent(MyState *state, bool inject)
{
	if (inject) {
		// Download state to context
		injector->PostStateToContext(*state);
		// Run injection
		injector->InjectError();
		// Rollback values (if needed)
		injector->RollbackState(state);
		// Upload injected value(s)
		injector->InjectionContextToState(state);
	}
}

// ...
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Injector usage is very simple. First, an instance of `MyInjector` is created. This is initialized
with the error model and config path. In this exampled, `PreInstructionEvent()` indicates a function
that is called before each instruction. However, if we want to inject into an instruction, first we
call `PreStateToContext()` to save the initial state. Then after the instruction is executed, in
`PostInstructionEvent()` we save the correct results of the instruction with `PostStateToContext()`.
Then we inject an error into the context with `InjectError()`. If needed, we rollback values with
`RollbackToOldState()` to get back to a known/unchanged state. Finally, we save the corrupted
value(s) to the state with `InjectionContextToState()`.

We refer user to our PIN-injector for detailed usage of the injector API.
