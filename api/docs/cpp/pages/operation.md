Operation
=========

Overview
--------
The Hamartia Error and Detector API provides a common interface for fault injection and detection.
While the current implementation is currently focused on *instruction-level* injection, the overall
interface could be extended in the future to support different levels of injection. In general this
library provides:

- Cross-language interfaces (SWIG)
	- Instruction information
	- Injection statistics
- Generic Injector
- Error managers (for handling C++ or Python error/detector models)
- Unified error/detector model interface
- Helper functions
- Basic fault utilities
    - RANDOM - Replace the value with a random number.
    - RANDOM_LH - Replace the value with a random number (limited to the lower half-word).
    - BURST* - Inject a bursty error (burst length of * bits)
    - RANDBIT* - Inject random bit flips (\* erroneous bits)
    - MANTB* - Inject a bursty error into the mantissa (FP values only)
    - MANTR* - Inject random bit flips into the mantissa (FP values only)
    - \note * is a number in {1,2,4,8}


Error Injection Flow
--------------------
![Injection Flow](images/API_Flows.png)
