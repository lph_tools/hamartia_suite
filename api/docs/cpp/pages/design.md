Design
======

Overview
--------
The Hamartia Error and Detector API was created in order to gain a better understanding of how
errors manifest using a Monte-carlo simulation/injection approach. However, the API can be used for
other error/detector-related things as well since it is a general API.

![Injection Flow](images/API_Flows.png)


Motivation
----------
- Simple fault models are currently used
    - Such as flipping a bit in the result
    - More precise models should be used/evaluated
- Fault models have to be reimplemented each time
    - There should be a common, shared interface
    - Can be used across projects and injectors
- Injection should be fast and have low-overhead
    - Separate levels of abstraction
    - Faults should be guaranteed (at some level), focus on the error manifestation
- Allow for large-scale experiments and simulations


Design Decisions
----------------

### Guaranteed Errors

All error models will create a guaranteed error (unmasked). This is verified and generated through
an iterative approach (random). The main idea for only injecting unmasked errors is that errors
which are masked will not cause any effects on the program. However, we do track statistics on how
many iterations it took to get a unmasked error.

### Unified and Cross-language Interface

If there is an easy to implement interface for all error/detector models, researchers can share
models and run them with different injectors. We currently support instruction-level injection and
RTL gate-level injection with C++ and Python; however, the API is fairly generic enough where 
it could be extended to more circuit-level injection (or other targets) in the future.

### Monte-Carlo Injection

We adopt a Monte-Carlo approach to injection since the search space is very vast (in most cases).  A
Monte-Carlo method will help to approximate the effects of different errors in this space and
hopefully lead to a better understanding of what errors look like so that they can be better
detected and corrected. However, there are built-in settings for narrowing the injection space and
targeting certain parts of a program if needed.
