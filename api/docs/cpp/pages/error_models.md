Error Models
============

Features
--------

Currently, the Error model API focuses on injecting instruction-level, transient (one-time) faults.
However, there are some additions that are in progress to allow for multiple injections (persistent
faults) and different state/context information. Currently the features offered are:

- C++/Python Error model API/execution available (this site discusses the C++ API)
- Instruction context forwarded (operands, instruction information)
- Logging constructs (for model specific output)


Interface
---------

The error interface is defined as the inputs/outputs to the injection entry-point
([ErrorModel::InjectError()](@ref hamartia_api::ErrorModel::InjectError)).
The most important aspects for creating an error model are:

- **ErrorContext.error_info.respose** : Response of error model (or operation)
- **ErrorContext.instruction_data** : Contains instruction data such as:
	- Assembly information
	- Data type and width/size
	- SIMD width
- **ErrorContext.instruction_data.instruction** : Instruction definition (x86-based)
- **ErrorContext.instruction_data.inputs** : Input operands to instruction, containing:
	- Data type, width/size, SIMD lanes
	- Value(s)
	- Type (e.g. Register, Memory, etc)
	- Address (memory), Reg (register)
- **ErrorContext.instruction_data.outputs** : Output operands to instruction
	- Note, the data currently in these are the *correct* values (after the instruction is executed)
    - Also, `old_value` contains the previous values (before the instruction was run)

While you can interact with the interface (ErrorContext pointer) directly, you can also use helper
functions to easily extract and save information into the context.


Essential API Calls/Types
-------------------------

While there are many calls, the ones below are some of the essentials:

- [DataValue](@ref hamartia_api::DataValue) : A unified container for data values, including representations for:
	- Integers
	- Floats/Doubles (and their sign, fraction, exponent segments)
	- RFLAGS
    - Arbitrary bit operations
- [ErrorContext::getInputValue()](@ref hamartia_api::ErrorContext::getInputValue) : Get Input
  value from context (useful for getting instruction operands)
- [ErrorContext::getOutputValue()](@ref hamartia_api::ErrorContext::getOutputValue) : Get Output
  value from context (useful for getting correct output values)
- [ErrorContext::getOutputOldValue()](@ref hamartia_api::ErrorContext::getOutputOldValue) : Get the
  old Output value from context
- [ErrorContext::setOutput()](@ref hamartia_api::ErrorContext::setOutput) : Set Output
  value for context (useful for injecting incorrect output values)
- [ErrorModel::SetResponse()](@ref hamartia_api::ErrorModel::SetResponse) : Set the response (of the
  context) for the operation (injection)


Creating an Error Model
-----------------------

All Error models extend the [ErrorModel](@ref hamartia_api::ErrorModel) interface which provides
interface definitions and entry-points for fault injection. `InjectError()` is the entry-point from
an injector into your error model. It includes:

- Error Context, information about the injected instruction (including operands, opcode, etc)
- Configuration, a decoded YAML file of configuration options (model specific)
- Log, a way for inserting specific logging information for your model

Otherwise, the main injection/fault flow is as follows:

1. Gather instruction information and operand data from the context
	- Note that the context includes inputs and outputs (correct)
2. Based upon the instruction and its operands, generate a fault based on some model
3. Inject the fault back into the context (i.e. either input or output)
4. Optionally set a response indicating future operations (e.g. `RESP_UPDATE`)
5. Return if successful (no runtime errors)

Also, included in the API are [basic fault utilities](@ref hamartia_api::utils) (e.g. random bit
flipping functions, etc) and helper functions (e.g. getting/setting operands, etc) which can be used
in a custom error model.

To get a better understanding of how to create an error model, and the interfaces/utilities used, an
example of an error model is shown below:

### Example

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.cpp}
#ifndef _MY_ERRORMODEL_H
#define _MY_ERRORMODEL_H

#include <hamartia_api/types.h>
#include <hamartia_api/utils/error.h>
#include <hamartia_api/error_model.h>

class MyErrorModel : public hamartia_api::ErrorModel
{

	private:
	// Error model setting
	uint64_t my_setting;
		 
	public:
	/**
	 * Create MyErrorModel instance
	 *
	 * \param _name    	  Error model name
	 * \param _my_setting A setting for parameterized/customizable error models
	 */
	MyErrorModel(std::string _name, uint64_t _my_setting) : ErrorModel(_name)
	{
		my_setting = _my_setting;
	}

	/**
	 * Inject random, masked error
	 *
	 * \param cxt    Error context to inject error into
	 * \param config YAML config file
	 * \param log    Log
	 *
	 * \return Successful
	 */
	bool InjectError(hamartia_api::ErrorContext *cxt, YAML::Node *config, hamartia_api::Log *log)
	{
		// Operand width
		uint32_t op_width   = cxt->instruction_data.width;
		// SIMD width
		uint32_t num_packed = cxt->instruction_data.simd_width;
		// Old result
		hamartia_api::DataValue old_result = cxt->getOutputValue();

		// Generate error
		hamartia_api::DataValue mod_result;
		if (op_width == 8) {
			// Inject into byte
			mod_result.uint(hamartia_api::utils::MR_inject <uint8_t> (old_result.uint, my_setting));

		} else if (op_width == 16) {
			// Inject into word (16bit)
			mod_result.uint(hamartia_api::utils::MR_inject <uint16_t> (old_result.uint, my_setting));

		} else if (op_width == 32 || num_packed == 4) {
			// Inject into double word (32bit)
			mod_result.uint(hamartia_api::utils::MR_inject <uint32_t> (old_result.uint, my_setting));

		} else if (op_width == 64 || num_packed == 2) {
			// Inject into quad word (64bit)
			mod_result.uint(hamartia_api::utils::MR_inject <uint64_t> (old_result.uint, my_setting));

		} else {
			mod_result.uint(hamartia_api::utils::MR_inject <uint64_t> (old_result.uint, my_setting));
		}

		// Inject error
		cxt->setOutput(mod_result);

		return true;
	}

};

#endif
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Let's discuss the operation of this fault model. First, we include the necessary include files:

- *types.h* : Datatypes for dealing with operands and injection (i.e. DataValue)
- *utils/error.h* : Fault utilities which implement some of the basics for injection
- *error_model.h* : The ErrorModel interface definition

Then, we define our own error model `MyErrorModel` which extends the `ErrorModel` interface. Next,
we should think how the error model should be used/instantiated. Error models are registered as an
instance; thus, it is completely possible to parameterize error models. This is accomplished by
defining members (such as `my_setting`) in the class. Then, in the constructor for the model, along
with the `name`, other arguments which set up parameters can be included. In this case, we included
`my_setting`.

The main portion of the model is the `InjectError` definition. In here, we gather the width of the
operation and the SIMD width (see interface definition and functions). Additionally, we gather the
correct output of the instruction using a helper function. This way, we can directly modify the
correct output without having to replay the instruction. The fault generation itself uses a random
fault function (see utils) depending on the width of the operation. It also uses `my_setting` as a
mask. Finally, the fault is injected back into the context using a helper function to set one of the
outputs of the context. Finally, the return value indicates if there was a runtime error. We refer
user to implemented error models in $HAMARTIA_DIR/include/cpp/hamartia_api/error_models/ for other
examples. 

Essentially what happens during execution is:

1. The injector traps on a target instruction
2. Context information is gathered
3. The specified error model is looked up by name (not class)
4. The error model is called with context, config, and logging information
5. The error model generates and injects a fault into the context
6. The system (error manager) verifies it is an unmasked/undetected error
7. The context is saved and program execution continues


Registering an Error Model
--------------------------

In order for your error model to get called, it needs to be registered first with the API.
Registering Error models is done through [ErrorModelLibrary(s)](@ref hamartia_api::ErrorModelLibrary). 
To register models, a library is defined which extends
[ErrorModelLibrary](@ref hamartia_api::ErrorModelLibrary). Then, instances of error models are
registered within the constructor of the library. These libraries then can be compiled in with the
injector and then initialized with [RegisterHandlers()](@ref hamartia_api::error_manager::RegisterHandlers).
Additionally, they can be compiled as their own separate shared-library (.so) and be loaded in
dynamically either explicitly or through the `HAMARTIA_ERROR_PATH`.

An example for creating an `ErrorModelLibrary` is shown below using 3 different error models.

### Example - Error Model Library

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.cpp}
// Libs
#include <string>
#include <hamartia_api/error_model.h>

// Error models
#include "model1.h"
#include "model2.h"
#include "model3.h"

class ErrorLibraryExample : public hamartia_api::ErrorModelLibrary
{
	public:
	ErrorLibraryExample()
    {
        // Model 1 
        REGISTER_ERROR_MODEL(ErrorModel1, MODEL1)

        // Model 2
        REGISTER_ERROR_MODEL_ARGS(ErrorModel2, MODEL2_1, 1)
        REGISTER_ERROR_MODEL_ARGS(ErrorModel2, MODEL2_2, 2)
        REGISTER_ERROR_MODEL_ARGS(ErrorModel2, MODEL2_4, 4)
        REGISTER_ERROR_MODEL_ARGS(ErrorModel2, MODEL2_8, 8)

        // Model 3
        REGISTER_ERROR_MODEL_ARGS(ErrorModel3, MODEL3_32, 32)
        REGISTER_ERROR_MODEL_ARGS(ErrorModel3, MODEL3_64, 64)
    }

	std::string GetName()
    {
        return "error_library_example";
    }
};
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

First, the error model API is included, along with any error model that you wish to include. Then, a
class for the library is created which extends `ErrorModelLibrary`. For this class, you mainly just
need to define the constructor and `GetName()`. The constructor will contain the "registration" for
each of the error models. `REGISTER_ERROR_MODEL` and `REGISTER_ERROR_MODEL_ARGS` are two macros that
aid in adding the error models. The first argument is the class name of the error model and the
second is the name of the error model (without quotes). Additionally, `REGISTER_ERROR_MODEL_ARGS`
can be used to initialize parameterized error models where the additional arguments are passed to
the constructor. Thus, multiple parameterized error models can be registered with the same error
model class. Note, the name specified at registration (second argument) is what is used for calling
the error model.

User can refer to $HAMARTIA_DIR/src/cpp/error_models/basic_models.cpp for another model registration 
example.

Using an Error Model
--------------------

As explained in the last section, error models are registered using `ErrorModelLibrary` which can
then be initialized/included with the injector. If you want to compile the library with the
injector and then initialize it, you would create an instance of the library and then use 
[RegisterHandlers()](@ref hamartia_api::error_manager::RegisterHandlers) in order to register it 
with the error manager (see [error_manager::Init()](@ref hamartia_api::error_manager)).

In terms of usage, the error model is defined within the `ErrorContext`
(cxt.error_info.error_model), which can be set by the `Injector()` constructor. However, these are
usually up to the injector on how they populate these values (usually through the command-line).
